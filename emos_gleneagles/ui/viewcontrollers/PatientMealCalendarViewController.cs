﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public partial class PatientMealCalendarViewController : BaseViewController, IDailyPatientMealCallback, IPatientDayOrderCallback, ILock
	{
		private const float TablesHeight = 315f;
		private LocationModelSimple _location;
		private RegistrationModelSimple _registration;
		private DynamicTableSource<NurseDashboardHeaderModel, MealOrderPeriodViewCell> _mealPeriodDataSource;
		private DailyPatientMealCalendarViewCellHandler _orderHandler;
		private CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> _orderSource;
		private CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> _companionOrderSource;
		private List<MealCalendar> _patientMealCalendars;
		private List<MealCalendar> _companionMealCalendars;
		private PatientOrderCommand _selectedPatientMealOrderCommand;
		private ProfileLanguageModelSimple _profileLanguageModel;

		public PatientMealCalendarViewController (IApplicationContext appContext, LocationModelSimple location, RegistrationModelSimple registration, bool isCompanionMode, bool isPatient = false) : base (appContext, "PatientMealCalendarViewController", null)
		{
			_location = location;
			_registration = registration;
			var setting = new BaseViewCellSetting { CellWidth = 150 };
			var mealPeriodHandler = new NoCallbackViewCellHandler<NurseDashboardHeaderModel> (AppContext, MealOrderPeriodViewCell.CellIdentifier, setting);
			_mealPeriodDataSource = new DynamicTableSource<NurseDashboardHeaderModel, MealOrderPeriodViewCell> (mealPeriodHandler);
			var orderViewCellSetting = new DailyPatientMealCalendarViewCellSetting {
				CellWidth = setting.CellWidth,
				ChildMealViewCellHeight = VerticalMealOrderViewCell.CellHeight,
				HeaderHeight = 30f
			};
			_orderHandler = new DailyPatientMealCalendarViewCellHandler (DailyMealOrderCollectionViewCell.CellIdentifier, this, orderViewCellSetting);
			_orderSource = new CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> (_orderHandler);
			_companionOrderSource = new CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> (_orderHandler);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			Title = AppContext.LanguageHandler.GetLocalizedString ("PatientMealOrder");
			NavigationItem.SetLeftBarButtonItems (new List<UIBarButtonItem> ().ToArray (), false);
			SetLayout ();
			BodyView.BackgroundColor = Colors.ViewCellBackground;
			CompanionLabel.BackgroundColor = AppContext.ColorHandler.ButtonBackground;
			CompanionLabel.Font = Fonts.Header;
			PatientLabel.BackgroundColor = AppContext.ColorHandler.ButtonBackground;
			PatientLabel.Font = Fonts.Header;
			MealPeriodTable.RowHeight = MealOrderPeriodViewCell.CellHeight;
			MealPeriodTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			CompanionMealPeriodTable.RowHeight = MealOrderPeriodViewCell.CellHeight;
			CompanionMealPeriodTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			PatientOrderCollectionView.RegisterNibForCell(DailyMealOrderCollectionViewCell.Nib, new NSString (DailyMealOrderCollectionViewCell.CellIdentifier));
			CompanionOrderCollectionView.RegisterNibForCell(DailyMealOrderCollectionViewCell.Nib, new NSString (DailyMealOrderCollectionViewCell.CellIdentifier));
			var patientSummaryView = new PageTitleAndPatientSummaryView (AppContext, _location, _registration);
			patientSummaryView.NoTitleMode ();
			HeaderView.AddSubview (patientSummaryView);
			// Perform any additional setup after loading the view, typically from a nib.
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear(animated);
			UnlockRegistration ();
			SendRequest ();
			//_requestHandler.SendRequest (View, RequestPatientMealCalendar);
		}
		private void SendRequest(){
			if (AppContext.CurrentUser.Authorized ("ACCESS_PATIENT_LANGUAGE") && 
				_registration.profile_id != null) {
				_requestHandler.SendRequest (View, RequestPatientSelectedLanguage);
			} else {
				_requestHandler.SendRequest (View, RequestPatientMealCalendar);
			}
		}

		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var refreshButton = new UIBarButtonItem (UIBarButtonSystemItem.Refresh, (sender, args) => {
				Refresh();
			});
			var doneButton = new UIBarButtonItem ("Done", UIBarButtonItemStyle.Done, (sender, args) => {
				AppContext.SignOut ();
			});

			var buttons = new UIBarButtonItem[] {
				doneButton, refreshButton
			};
			if (AppContext.CurrentUser.Authorized ("ACCESS_PATIENT_LANGUAGE") &&
				_registration.profile_id != null) {

				var imagePath = "en";
				if (_profileLanguageModel != null) {
					if (AppContext.Languages != null) {
						if(_profileLanguageModel.emlanguage_id != null)
							imagePath = AppContext.Languages.Where (r => r.id == _profileLanguageModel.emlanguage_id).FirstOrDefault ().code;

//						UIImage languageImage = UIImage.FromFile ("Images/" + imagePath + "_small.png");
						var languageButton = new UIBarButtonItem ("Language", UIBarButtonItemStyle.Plain, (sender, args) => {
							LoadLanguageViewController ();
						});

						buttons = new UIBarButtonItem[] {
							doneButton, refreshButton, languageButton
						};
					}

				}
				
			}
			return buttons;
		}
		private void LoadLanguageViewController(){
			AppContext.LoadPatientLanguage(false, false, _profileLanguageModel, HandleOnLanguageSave);
		}
		private void HandleOnLanguageSave (object sender, ProfileLanguageEventArgs e)
		{
			_profileLanguageModel = e.ProfileLanguage;
			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
		}
		private const float LabelsHeight = 37f;
		private void SetLayout ()
		{
			HeaderView.SetFrame (y: Measurements.TopY, height: Measurements.CompanionMealOrderHeaderHeight);
			BodyView.Hidden = true;
			BodyView.SetFrameBelowTo (HeaderView, height: Measurements.PageHeight - Measurements.CompanionMealOrderHeaderHeight, distanceToAbove: 0);
			BodyView.Hidden = true;
			PatientLabel.SetFrame (height: LabelsHeight);
			MealPeriodTable.SetFrame (0, 30 + LabelsHeight, 100, TablesHeight - 50);
			OrderCollectionViewFlowLayout.ItemSize = new SizeF (DailyMealOrderCollectionViewCell.CellWidth, TablesHeight);
			PatientOrderCollectionView.SetFrame (MealPeriodTable.Frame.Width, LabelsHeight, 768 - MealPeriodTable.Frame.Width, TablesHeight);
			CompanionLabel.SetFrameBelowTo (PatientOrderCollectionView, height: 32, distanceToAbove: 0);
			CompanionOrderCollectionView.SetFrameBelowTo (CompanionLabel, PatientOrderCollectionView.Frame.X, PatientOrderCollectionView.Frame.Width, TablesHeight, distanceToAbove: 0);
			CompanionMealPeriodTable.SetFrame (0, CompanionOrderCollectionView.Frame.Top + 30, MealPeriodTable.Frame.Width, TablesHeight);
			CompanionOrderCollectionViewFlowLayout.ItemSize = new SizeF (DailyMealOrderCollectionViewCell.CellWidth, TablesHeight);
		}
		private async void RequestPatientMealCalendar()
		{
			if (AppContext.WardId == null) {
				_requestHandler.StopRequest ();
				return;
			}
			var param = KnownUrls.GetPatientModeNurseDashboardQueryString (AppContext.MealPeriodGroupCode, AppContext.InstitutionId, AppContext.WardId, AppContext.OperationDate, AppContext.HospitalId, _registration.registration_id);
			var url = KnownUrls.GetPatientModeNurseDashboardModel;
			await AppContext.HttpSender.Request<NurseDashboardModel> ()
				.From (url)
				.WithQueryString (param)
				.WithContent (AppContext.OperationDate)
				.WhenSuccess (result => OnRequestPatientMealOrderSuccessful(result))
				.WhenFail (result => OnRequestCompleted (false, AppContext.LanguageHandler.GetLocalizedString ("Requestdashboard")))
				.Go ();
		}
		private void OnRequestPatientMealOrderSuccessful(NurseDashboardModel result)
		{
			OnRequestCompleted (true);
			var mock = new PatientMealCalendarDashboard { 
				header = result.header,
				bed = result.beds.FirstOrDefault (b => b.id == _location.id && b.registrations.FirstOrDefault().profile_id == _registration.profile_id)
			};
			OnRequestPatientMealOrderSuccessful (mock);
		}
		private void OnRequestPatientMealOrderSuccessful(PatientMealCalendarDashboard result)
		{
			_mealPeriodDataSource.Items = result.header;
			BodyView.Hidden = false;
			MealPeriodTable.Source = _mealPeriodDataSource;
			MealPeriodTable.RowHeight = 55f;
			MealPeriodTable.ReloadData ();
			_patientMealCalendars = new List<MealCalendar> ();
			_patientMealCalendars.Add (CreateMealCalendar (result, ConsumerTypes.Patient));
			_orderSource.Items = _patientMealCalendars;
			PatientOrderCollectionView.Source = _orderSource;
			PatientOrderCollectionView.ReloadData ();

			_requestHandler.SendRequest (View, RequestCompanionCalendar);
		}
		private MealCalendar CreateMealCalendar (PatientMealCalendarDashboard result, ConsumerTypes consumerType)
		{
			var patientOrderCommands = PatientOrderCommandViewCellHandler.CreateOneDayPatientOrderCommands (AppContext, result.bed, result.bed.registrations.First (), AppContext.OperationDate, consumerType);
			var patientMealCalendar = new MealCalendar {
				OrderDate = AppContext.OperationDate,
				PatientOrderCommands = patientOrderCommands
			};
			return patientMealCalendar;
		}
		public async void RequestCompanionCalendar()
		{
			if (AppContext.WardId == null) {
				_requestHandler.StopRequest ();
				return;
			}

			var queryString = KnownUrls.GetPatientModeNurseDashboardQueryString (AppContext.MealPeriodGroupCode, AppContext.InstitutionId, AppContext.WardId, AppContext.OperationDate, AppContext.HospitalId, _registration.registration_id);
			var url = KnownUrls.GetPatientModeLodgerDashboardModel;

			await AppContext.HttpSender.Request<NurseDashboardModel> ()
				.From (url)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestCompanionDashboardSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestCompanionDashboardSuccessful(NurseDashboardModel result)
		{
			OnRequestCompleted (true);
			var mock = new PatientMealCalendarDashboard { 
				header = result.header,
				bed = result.beds.FirstOrDefault (b => b.id == _location.id && b.registrations.FirstOrDefault().profile_id == _registration.profile_id)
			};
			OnRequestCompanionMealOrderSuccessful (mock);
		}
		private void OnRequestCompanionMealOrderSuccessful (PatientMealCalendarDashboard result)
		{
			CompanionMealPeriodTable.Source = _mealPeriodDataSource;
			_companionMealCalendars = new List<MealCalendar> ();
			_companionMealCalendars.Add (CreateMealCalendar (result, ConsumerTypes.Companion));
			_orderSource.Items = _companionMealCalendars;
			_companionOrderSource.Items = _companionMealCalendars;
			CompanionMealPeriodTable.RowHeight = 55f;
			CompanionMealPeriodTable.ReloadData ();
			CompanionOrderCollectionView.Source = _companionOrderSource;
			CompanionOrderCollectionView.ReloadData ();

			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
		}
		public void OrderButtonClicked (PatientOrderCommand item)
		{
			if (!OperationCodeHandler.IsClickableOnDashboard (item.OperationCode))
				return;
			if (!item.Registration.canDoMealOrder) {
				this.ShowAlert (AppContext, AppContext.LanguageHandler.GetLocalizedString(LanguageKeys.DietOrderNotSet), null, false);
				return;
			}
			AppContext.SelectedLocation = item.Location;
			AppContext.SelectedRegistration = item.Registration;
			AppContext.SelectedMealOrderPeriod = item.MealOrderPeriod;
			AppContext.SelectedOperationCode = item.OperationCode;
			_selectedPatientMealOrderCommand = item;
			if (item.ConsumerType == ConsumerTypes.Companion) {
				AppContext.LoadCompanionMealOrder (false, false, true, item);
			} else {
				if (Settings.CheckLock && !AppContext.LockHandler.LockResult.free_to_access) {
					CheckLock (LockRequestCode.MealOrder);
				} else {
					LoadMealOrderController ();
				}
			}
		}
		private void LoadMealOrderController ()
		{
			AppContext.LoadNurseMealOrder (false, false, Dashboard.DashboardMenu.PatientMealOrder, true, _selectedPatientMealOrderCommand, HandleOnMealOrderClose, HandleOnMealOrderSave, this, true);
		}
		private void HandleOnMealOrderClose (object sender, EventArgs e)
		{
			PopToViewControllerAndUnlock ();
		}
		private void HandleOnMealOrderSave (object sender, PatientOrderCommandEventArgs e)
		{
			PopToViewControllerAndUnlock ();
		}
		public void PopToViewControllerAndUnlock(bool unlock = true, bool animated = true)
		{
			UnlockRegistration ();
			NavigationController.PopToViewController (NavigationController.ViewControllers[0], true);
		}
		public void ItemSelected (MealCalendar selected, int selectedIndex)
		{
		}
		public void PatientNameClicked (LocationWithRegistrationSimple location)
		{
			throw new NotImplementedException ();
		}
		public void IdClicked (LocationWithRegistrationSimple location)
		{
			throw new NotImplementedException ();
		}
		public void Refresh ()
		{
			_requestHandler.SendRequest (View, RequestPatientMealCalendar);
		}
		public void OnDashboardChanged (Dashboard.OrderTabs tab, bool refresh = true)
		{
			throw new NotImplementedException ();
		}
		public void ShowWardSelectionAlert ()
		{
			throw new NotImplementedException ();
		}
		public void OpenBirthdayCakeForm (LocationWithRegistrationSimple locationWithRegistration)
		{
			throw new NotImplementedException ();
		}
		public void CloseBirthdayCakeForm ()
		{
			throw new NotImplementedException ();
		}
		public void ItemSelected(LocationWithRegistrationSimple selected, int selectedIndex)
		{
			throw new NotImplementedException ();
		}
		public override void SetTextsByLanguage ()
		{
			CompanionLabel.Text = String.Format("{0} Meal Order",AppContext.LanguageHandler.GetLocalizedString (LanguageKeys.Companion));
		}
		private void UnlockRegistration ()
		{
			AppContext.LockHandler.UnlockRegistration ();
		}
		private void CheckLock (LockRequestCode lockRequestCode)
		{
			AppContext.LockHandler.LockRegistration (this, _registration.registration_id.Value, lockRequestCode, _requestHandler, View);
		}
		public void PatientMenuLockSuccessful (LockResultIpad result)
		{
		}
		public void PatientMenuLockFailed (LockResultIpad result)
		{
		}
		public void MealOrderLockSuccessful (LockResultIpad result)
		{
			LoadMealOrderController ();
		}
		public void MealOrderLockFailed (LockResultIpad result)
		{
			var message = String.Format ("Patient {0} has been locked by [{1}] on {2}.", _registration.profile_name, result.locked_by, result.lock_start_at_verbose);
			this.ShowAlert (AppContext, message, null, false);
		}
		public void OnLockRequestCompleted(bool success, string failedTitle = "", bool showFailedTitle = true)
		{
			base.OnRequestCompleted (success, failedTitle, showFailedTitle);
		}
		public void UnlockSuccessful ()
		{
		}
		public void UnlockFailed ()
		{
		}
		public void OnUnlockRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
		}
		public void OnLockNotExtended ()
		{
			AppContext.LoadMenu (core_emos.Menu.EmosMenu.RestrictedPatientMealOrder, showMenu: false);
		}
		public void OnUnlockLocksByUserRequestCompleted(bool success)
		{
			base.OnRequestCompleted (success);
		}
		private async void RequestPatientSelectedLanguage()
		{
			if (AppContext.WardId == null) {
				_requestHandler.StopRequest ();
				return;
			}

			var profileId = _registration.profile_id;
			if (profileId == null) {
				return;
			}

			var param = KnownUrls.GetPatientSelectedLanguageQuery (profileId);
			await AppContext.HttpSender.Request<ProfileLanguageModelSimple> ()
				.From (KnownUrls.GetPatientSelectedLanguage)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestPatientSelectedLanguageSuccessful(result))
				.WhenFail (result => OnRequestCompleted (false, AppContext.LanguageHandler.GetLocalizedString ("Requestdashboard")))
				.Go ();
		}
		private void OnRequestPatientSelectedLanguageSuccessful(ProfileLanguageModelSimple result)
		{
			OnRequestCompleted (true);
			_profileLanguageModel = result;
			_requestHandler.SendRequest (View, RequestPatientMealCalendar);
		}
		public void SetOrderBy (string orderBy)
		{
		}
	}
}