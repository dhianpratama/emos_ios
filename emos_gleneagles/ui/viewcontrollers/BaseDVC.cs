﻿using System;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.ObjCRuntime;
using emos_ios.tools;
using ios;
using System.Drawing;
using System.Globalization;

namespace emos_ios
{
	public abstract class BaseDVC : DialogViewController, IBaseDVC
	{
		protected IApplicationContext AppContext;
		protected RequestHandler _requestHandler;
		protected bool HiddenLogo;
		protected UIView LogoView;
		protected float LogoY;

		public BaseDVC (IApplicationContext appContext, bool hiddenLogo = false) : base (UITableViewStyle.Grouped, null, true)
		{
			AppContext = appContext;
			LogoY = 0;
			HiddenLogo = hiddenLogo;
			_requestHandler = new RequestHandler (AppContext, this);
		}
		public override void LoadView ()
		{
			base.LoadView ();
			if (HiddenLogo)
				return;
			var LogoView = new LogoView (AppContext.ServerConfig.Version, false, AppContext.OperationDate.ToString (Formats.OperationDateFormat, new CultureInfo (AppContext.LanguageHandler.LanguageCode)));
			LogoView.Frame = new RectangleF (0, LogoY, 768, Measurements.LogoHeight);
			LogoView.ClipsToBounds = true;
			this.View.AutosizesSubviews = false;
			this.View.AddSubview (LogoView);
		}
		public virtual void CreateRoot(string title = "")
		{
			if (HiddenLogo) {
				Root = new RootElement (title);
				return;
			}
			Root = new RootElement (title) {
				new Section (),
				new Section (),
				new Section ()
			};
		}
		public virtual void GetData ()
		{
		}
		public UIView GetDvcView()
		{
			return View;
		}
		protected virtual void OnRequestCompleted(bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			_requestHandler.CompletedRequest (success, failedTitleCode, showFailedTitle);
		}
		public IBaseContext GetBaseContext()
		{
			return AppContext;
		}
	}
}

