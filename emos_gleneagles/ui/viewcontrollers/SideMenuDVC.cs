﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using core_emos;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class SideMenuDVC : BaseDVC
	{
		public SideMenuDVC (IApplicationContext applicationContext) : base (applicationContext, true)
		{
			var emosMenuText = AppContext.LanguageHandler.GetLocalizedString ("EmosMenu");
			var othersText = AppContext.LanguageHandler.GetLocalizedString ("Others");
			var settingText = AppContext.LanguageHandler.GetLocalizedString ("Setting");
			var signoutText = AppContext.LanguageHandler.GetLocalizedString ("Signout");

			var rootElement = new RootElement (emosMenuText) {
				new Section() {
				},
				new Section() {
				},
				new Section (AppContext.LanguageHandler.GetLocalizedString ("MealOrder")) {
					new StringElement (AppContext.LanguageHandler.GetLocalizedString ("NurseDashboard"), () => {
						AppContext.LoadMenu (Menu.EmosMenu.NurseDashboard);
					}),
					new StringElement (AppContext.LanguageHandler.GetLocalizedString ("LodgerMealOrder"), () => {
						AppContext.LoadMenu (Menu.EmosMenu.CompanionMealOrder);
					}),
				}
			};
			if (AppContext.CurrentUser.IsAdmin) {
				rootElement.Add (new Section (othersText) {
					new StringElement (settingText, () => {
						AppContext.LoadMenu (Menu.EmosMenu.Setting);
					})
				});
			}
			/*
			var languageText = AppContext.LanguageHandler.GetLocalizedString ("DishLanguage");
			if (AppContext.Languages != null && AppContext.Languages.Count > 0) {
				rootElement.Add (new Section (languageText) {
					new StringElement (languageText, () => {
						AppContext.LoadMenu (Menu.EmosMenu.DishLanguage);
					}),
				});
			}*/
			rootElement.Add (new Section () {
				new StringElement (AppContext.LanguageHandler.GetLocalizedString ("AboutEmos"), () => {
					AppContext.PushDVC (new AboutDVC(AppContext));
				}),
				new StringElement (signoutText, () => {
					AppContext.SignOut (AppContext.CurrentUser.IsAdmin);
				})
			});
			Root = rootElement;
		}
	}
}
