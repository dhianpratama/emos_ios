﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;

namespace emos_ios
{
	public partial class AboutDVC : BaseDVC
	{
		public AboutDVC (IApplicationContext appContext) : base (appContext)
		{
			CreateRoot (AppContext.LanguageHandler.GetLocalizedString ("Admin"));
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var versionText = AppContext.LanguageHandler.GetLocalizedString ("Version");
			Root.Add (new Section () {
				(new EntryElement (versionText, "", AppContext.ServerConfig.Version)),
			});
		}
	}
}