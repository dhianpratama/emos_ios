﻿using System;

namespace emos_ios
{
	//Gleneagles
	public static class Settings
	{
		public static bool UseDateAsDashboardTitle = false;
		public static bool HideNurseDashboardTab = true;
		public static bool ShowCompanionLabelOnDashboard = false;
		public static bool HideMealOnHold = false;
		public static bool HideStayForLunch = false;
		public static bool HideEarlyDelivery = false;
		public static long DefaultNewExtraOrderQuantity = 0;
		public static bool IgnoreIncompleteOrder = true;
		public static bool UseMealPeriodAsBirthdayCakeSegment = false;
		public static bool ChangeCompanionLabelToLodger = true;
		public static bool SimpleTrialOrder = true;
		public static bool DoubleTapNurseDashboardTableView = false;
		public static bool UseDietOrderText = false;
		public static bool UseExtraOrdersText = false;
		public static bool MultipleCompanionOrder = true;
		public static bool MandatoryMainDish = false;
		public static bool HideMealPeriodGroupInSetting = true;
		public static bool EnableDeclineMealOrder = true;
		public static bool HideSaveDraft = true;
		public static bool FoodAllergiesRemarkSpecialQuery = false;
		public static bool ShowOtherDietsCommentField = false;
		public static bool ShowActiveDietInformation = false;
		public static bool ShowPatientPreference = true;
		public static bool CheckLock = true;
		public static bool ShowDVCTitle = false;
		public static bool ShowAdditionalPatientMenuLinks = true;
		public static bool UseRegistrationNumberInsteadOfId = true;
		public static int StartDateToEndDateDefaultDays = 90;
		public static bool DisableExtraOrderEdit = false;
		public static bool OneDishMeal = false;
		public static bool LoadExistingOrderOnEdit = true;
		public static bool AllowDashboardCutoffToggle = true;
		public static bool PatientDailyOrderMode = true;
		public static bool QueryMealTypeWithFeeds = true;
		public static bool FoodTextureIsCompulsory = true;
		public static bool UseSmallCap = true;

		// turn the following on for MEH uat
		public static bool EnableTheme = false;
		public static bool ShowOnGoingRemark = true;
		public static bool ShowDetailOnImageView = true;

	}
}