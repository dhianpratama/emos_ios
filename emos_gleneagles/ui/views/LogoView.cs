﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.IO;

namespace emos_ios
{
	[Register("LogoView")]
	public partial class LogoView : UIView
	{
		public LogoView (IntPtr h) : base(h)
		{
		}
		public LogoView (string version, bool isDemo, string operationDate)
		{
			var arr = NSBundle.MainBundle.LoadNib("LogoView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
			VersionLabel.Text = version;
			LogoImage.ContentMode = UIViewContentMode.ScaleAspectFit;
			SetImage ();
			OperationDateLabel.Text = operationDate;
		}
		public void SetImage ()
		{
			var documentsFolderPath = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			var logoFile = Path.Combine (documentsFolderPath, "logo.png");
			if (!File.Exists (logoFile))
				logoFile = "Images/logo.png";
			LogoImage.Image = UIImage.FromFile (logoFile);
		}
		public void SetDefaultLogoImage()
		{
			LogoImage.Image = UIImage.FromFile ("Images/logo.png");
		}
		public void ChangeOperationDateLabel(string label)
		{
			OperationDateLabel.Text = label;
		}
	}
}