﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Drawing;
using System.Linq;
using MonoTouch.ObjCRuntime;
using core_emos;

namespace emos_ios
{
	[Register("MealOrderHeaderView")]
	public partial class MealOrderHeaderView : UIView
	{
		private IApplicationContext _appContext;
		private const float CategoryTabHeight = 50f;
		private List<MealCategoryTabButtonView> _categoryTabButtons;
		public List<MealOrderStepView> StepButtons { get; set; }
		private MealOrderModel _mealOrderModel;
		public event EventHandler<MealOrderHeaderEventArgs> OnStepSelected;
		private int _currentTag;
		private int _currentStep;
		public DishTypeModelSimple CurrentDishType;
		public TagModelSimple CurrentMealTag;
		public int CurrentStep { get { return _currentStep; } private set { } }
		public IMealOrderCallback Callback { get; set; }
		public bool IsPatient { get; set; }
		public bool IsCompanion { get; set; }

		public MealOrderHeaderView(IntPtr h): base(h)
		{
		}			
		public MealOrderHeaderView (IApplicationContext appContext)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib("MealOrderHeaderView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
		}
		public void Load (MealOrderModel mealOrderModel, bool viewOnly, int selectedTag)
		{
			_mealOrderModel = mealOrderModel;
			_currentTag = selectedTag;
			mealCategoryTabView.Frame = new RectangleF (0, Measurements.HeaderHeight - CategoryTabHeight, 768, CategoryTabHeight);
			SetTexts (mealOrderModel);
			CategoryImage.Image = ImageLibrary.GetMealPeriodImage (false, _appContext.SelectedMealOrderPeriod.label);
			if (viewOnly) {
				ShowOrderInfoView ();
				return;
			}
			HideOrderInfoView ();
			if (_appContext.SelectedOperationCode != 5) {
				GenerateStepButtons ();
				GenerateCategoryTabButtons ();
			}
			SetTextsByLanguage ();
		}

		public void SkipStepButtons(MealOrderModel oneDishMealOrderModel, int step, bool next){
			ViewHandler.ClearSubviews (StepsView);
			_mealOrderModel = oneDishMealOrderModel;
			StepButtons = new List<MealOrderStepView> ();
			int allTypesCount = _mealOrderModel.tagsMealOrder [_currentTag].types.Where(e => e.skip == false).ToList().Count ();

			for (int i = 0; i < allTypesCount; i++) {
				var buttonTitle = (i + 1).ToString ();
				var mealStepView = CreateStep (buttonTitle, i);
				StepButtons.Add (mealStepView);
			}
			var summaryStep = CreateStep ((allTypesCount + 1).ToString (), allTypesCount);
			StepButtons.Add (summaryStep);
			SelectStep (step, true);
		}

		private void SetTexts (MealOrderModel mealOrderModel)
		{
			IdLabel.Text = _appContext.SelectedRegistration.profile_identifier;
			NameLabel.Text = _appContext.SelectedRegistration.profile_name;
			WardLabel.Text = _appContext.SelectedLocation.label;
			var patientText = _appContext.LanguageHandler.GetLocalizedString ("Patient");
			if (mealOrderModel.order_by_patient)
				OrderByValueLabel.Text = IsPatient ? patientText : String.Format ("{0} ({1})", patientText,  mealOrderModel.order_by);
			else
				OrderByValueLabel.Text = mealOrderModel.order_by;
			OrderTimeValueLabel.Text = mealOrderModel.order_time;
		}
		private void GenerateCategoryTabButtons ()
		{
			var mealCategories = _mealOrderModel.tagsMealOrder.Select (e => e.tag.label).ToList ();
			var tabWidth = (float) Math.Ceiling(768f / mealCategories.Count());
			_categoryTabButtons = new List<MealCategoryTabButtonView> ();
			for (int i = 0; i < mealCategories.Count (); i++) {
				var x = tabWidth * i;
				var mealCategoryTabButtonView = new MealCategoryTabButtonView (_appContext, mealCategories[i], i);
				mealCategoryTabButtonView.Frame = new RectangleF (x, 0, tabWidth, CategoryTabHeight);
				mealCategoryTabButtonView.SetLayout (x, tabWidth, CategoryTabHeight);
				mealCategoryTabButtonView.OnCategoryClicked += OnCategoryClicked;
				mealCategoryTabView.AddSubview (mealCategoryTabButtonView);
				_categoryTabButtons.Add (mealCategoryTabButtonView);
			}
		}
		private void OnCategoryClicked (object sender, EventArgs e)
		{
			var button = (MealCategoryTabButtonView)sender;
			SelectCategory (button.CategoryIndex);
		}
		public void SelectCategory (int selected, bool filterDish = true)
		{
			if (_categoryTabButtons.Count == 0)
				return;
			for (int i = 0; i < _categoryTabButtons.Count (); i++) {
				if (i == selected)
					_categoryTabButtons [i].Pressed = true;
				else
					_categoryTabButtons [i].Pressed = false;
			}
			_currentTag = selected;
			CurrentMealTag = _mealOrderModel.tagsMealOrder [selected].tag;
			if (filterDish)
				Callback.FilterDish (_currentStep, _currentTag);
			GenerateStepButtons ();
		}
		public void SelectStep (int selected, bool filterDish = true)
		{
			if (StepButtons.Count == 0)
				return;
			_currentStep = selected;
			List<DishTypeModelSimple> dishTypes = new List<DishTypeModelSimple> ();
			dishTypes = _mealOrderModel.tagsMealOrder [_currentTag].types.Where (r => r.skip == false).ToList();

			if (selected != dishTypes.Count())
				CurrentDishType = dishTypes[selected];
			else
				CurrentDishType = null;
			for (int i = 0; i < StepButtons.Count (); i++) {
				StepButtons [i].Pressed = i == selected;
			}
			if (selected > 0) {
				for (int i = 0; i < _categoryTabButtons.Count (); i++) {
					if (i != _currentTag)
						_categoryTabButtons [i].Deactive ();
				}
			} else {
				if (_categoryTabButtons != null) {
					_categoryTabButtons.ForEach (c => {
						c.Hidden = false;
					});
				}
			}
			if (filterDish)
				Callback.FilterDish (_currentStep, _currentTag);
		}
		private void GenerateStepButtons ()
		{
			ViewHandler.ClearSubviews (StepsView);
			StepButtons = new List<MealOrderStepView> ();
			for (int i = 0; i < _mealOrderModel.tagsMealOrder [_currentTag].types.Count (); i++) {
				var buttonTitle = (i + 1).ToString ();
				var mealStepView = CreateStep (buttonTitle, i);
				StepButtons.Add (mealStepView);
			}
			var summaryStep = CreateStep ((_mealOrderModel.tagsMealOrder [_currentTag].types.Count + 1).ToString (), _mealOrderModel.tagsMealOrder [_currentTag].types.Count);
			StepButtons.Add (summaryStep);
			SelectStep (0, false);
		}

		private MealOrderStepView CreateStep(string title, int step)
		{
			var mealStepView = new MealOrderStepView (Callback.GetBaseContext (), title, step);
			mealStepView.SetButtonTitle (title);
			mealStepView.OnStepClicked += OnStepClicked;
			StepsView.AddSubview (mealStepView);
			mealStepView.Frame = new RectangleF(50 * step, 0, 50, 50);
			return mealStepView;
		}

		private void OnStepClicked (object sender, EventArgs e)
		{
			var step = (MealOrderStepView)sender;
			if (step.Step != _currentStep)
				OnStepSelected (this, new MealOrderHeaderEventArgs (step.Step));
		}

		public void DeactiveOtherTags()
		{
			//_categoryTabButtons[1].
		}

		public void HideOrderInfoView()
		{
			OrderInfoView.Hidden = true;
			StepsToOrderLabel.Hidden = false;
			StepsView.Hidden = false;
			mealCategoryTabView.Hidden = false;
		}
		public void ShowOrderInfoView()
		{
			OrderInfoView.Hidden = false;
			StepsToOrderLabel.Hidden = true;
			StepsView.Hidden = true;
			mealCategoryTabView.Hidden = true;
		}
		public void UpdateMealOrderModel()
		{

		}
		private void SetTextsByLanguage()
		{
			NameTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Name") + ":";
			IDTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("ID") + ":";
			WardTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("WardBed") + ":";
			StepsToOrderLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("StepsToOrder");
			OrderByTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Orderby") + ":";
			OrderTimeTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Ordertime") + ":";
		}
		public void Order (int _step)
		{
		}
		public void CancelOrder (int _step)
		{
		}
		public void SetTitle (List<DishModelWithTags> availableDishes, List<DishModelWithTags> suitableDishes)
		{
		}
	}
}

