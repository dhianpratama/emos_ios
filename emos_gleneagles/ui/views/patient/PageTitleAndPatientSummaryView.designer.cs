// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class PageTitleAndPatientSummaryView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ComsumptionChartingLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView DateAndMealView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel IdLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel IDTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WardBedLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WardTitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ComsumptionChartingLabel != null) {
				ComsumptionChartingLabel.Dispose ();
				ComsumptionChartingLabel = null;
			}

			if (DateAndMealView != null) {
				DateAndMealView.Dispose ();
				DateAndMealView = null;
			}

			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (DateTitleLabel != null) {
				DateTitleLabel.Dispose ();
				DateTitleLabel = null;
			}

			if (IdLabel != null) {
				IdLabel.Dispose ();
				IdLabel = null;
			}

			if (IDTitleLabel != null) {
				IDTitleLabel.Dispose ();
				IDTitleLabel = null;
			}

			if (MealLabel != null) {
				MealLabel.Dispose ();
				MealLabel = null;
			}

			if (MealTitleLabel != null) {
				MealTitleLabel.Dispose ();
				MealTitleLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (NameTitleLabel != null) {
				NameTitleLabel.Dispose ();
				NameTitleLabel = null;
			}

			if (WardBedLabel != null) {
				WardBedLabel.Dispose ();
				WardBedLabel = null;
			}

			if (WardTitleLabel != null) {
				WardTitleLabel.Dispose ();
				WardTitleLabel = null;
			}
		}
	}
}
