﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using MonoTouch.ObjCRuntime;
using System.Drawing;

namespace emos_ios
{
	[Register("PageTitleAndPatientSummaryView")]
	public partial class PageTitleAndPatientSummaryView : UIView
	{
		private IApplicationContext _appContext;

		public PageTitleAndPatientSummaryView (IntPtr h) : base(h)
		{
		}
		public PageTitleAndPatientSummaryView (IApplicationContext appContext, LocationModelSimple location, RegistrationModelSimple registration)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib ("PageTitleAndPatientSummaryView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.BackgroundColor = UIColor.Clear;
			AddSubview (v);

			if (location.label == null)
				WardBedLabel.Text = "";
			else
				WardBedLabel.Text = location.label;
			if (registration == null) {
				IdLabel.Text = "";
				NameLabel.Text = "";
				return;
			}
			IdLabel.Text = registration.profile_identifier;
			NameLabel.Text = registration.profile_name;
			DateAndMealView.BackgroundColor = _appContext.ColorHandler.ViewCellBackground;
			DateAndMealView.Hidden = true;

			ComsumptionChartingLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("ConsumptionCharting");
			NameTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Name") + ":";
			IDTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("ID") + ":";
			WardTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("WardBed") + ":";
			MealTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Meal") + ":";
			DateTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Date") + ":";
		}
		public void NoTitleMode()
		{
			ComsumptionChartingLabel.Hidden = true;
			NameTitleLabel.SetFrame (y: 10);
			IDTitleLabel.SetFrameBelowTo (NameTitleLabel, distanceToAbove: 0);
			WardTitleLabel.SetFrameBelowTo (IDTitleLabel, distanceToAbove: 0);
			NameLabel.SetFrame (y: NameTitleLabel.Frame.Top);
			IdLabel.SetFrame (y: IDTitleLabel.Frame.Top);
			WardBedLabel.SetFrame (y: WardTitleLabel.Frame.Top);
		}
		public void SetHeaderImage(UIImage image)
		{
		}
	}
}

