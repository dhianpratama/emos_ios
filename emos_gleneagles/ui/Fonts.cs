﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public static class Fonts
	{
		public static readonly UIFont PageHeader = UIFont.BoldSystemFontOfSize (36f);
		public static readonly UIFont Header = UIFont.BoldSystemFontOfSize (20f);
		public static readonly UIFont CutoffHeader = UIFont.BoldSystemFontOfSize (15f);
		public static readonly String ValueFontName = "HelveticaNeue";
		public static readonly String KeyFontName = "HelveticaNeue-Bold";
		public static readonly UIFont NurseDashboardPatientInfo = UIFont.SystemFontOfSize (14f);
		public static readonly UIFont NurseDashboardBedInfo = UIFont.BoldSystemFontOfSize (14f);	
	}
}
