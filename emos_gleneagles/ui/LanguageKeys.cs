﻿using System;

namespace emos_ios
{
	public static class LanguageKeys
	{
		public static string UserName = "UserId";
		public static string UsernamePasswordIncorrect = "UserIdPasswordIncorrect";
		public static string ExtraOrder = "SpecialInstruction";
		public static string ExtraOrders = "SpecialInstructions";
		public static string Companion = "Lodger";
		public static string NoExtraOrderSelected = "Nospecialinstructionselected";
		public static string NoExtraOrderIsFound = "Nospecialinstructionselected";
		public static string DeleteExtraOrderConfirmation = "Areyousurewanttodeletethisspecialinstruction";
		public static string DeletedExtraOrderInformation = "SpecialInstruction'{0}'hasbeendeleted";
		public static string Therapeutic = "Therapeutic";
		public static string ExtraOrderIconText = "SI";
		public static string TrialOrderIconText = "TO";
		public static string FullFeedsTitle = "FullFeeds";
		public static string ClearFeedsTitle = "ClearFeeds";
		public static string NilByMouthTitle = "NilbyMouth";
		public static string NoDietOnTherapeuticFound = "NodietorderunderTherapeuticisfound";
		public static string CompanionOrder = "LodgerOrder";
		public static string DietOrderNotSet = "Pleasesetfoodtexturebeforeorderingmeal";
	}
}

