﻿using System;

namespace emos_ios
{
	public static class Measurements
	{
		public static float FrameTop = 64f;
		public static float LogoHeight = 104f;
		public static float KeyboardHeight = 260f;
		public static float TopLogoY = 64f;
		public static float TopLogoHeight = LogoHeight;
		public static float HeaderHeight = 290f;
		public static float MealOrderHeaderHeight = HeaderHeight;
		public static float HeaderImageHeight = 0f;

		public static float BottomLogoY = 1024f;
		public static float BottomLogoHeight = 0f;

		public static float NurseDashboardRowHeight = 64f;
		public static float NurseDashboardTopBarHeight = 0f;
		public static float CompanionMealOrderHeaderHeight = 155f;
		public static float PatientMealOrderHeaderHeight = 105f;
		public static float NurseDashboardCutoffTitleHeight = 45f;
		public static float NurseDashboardOperationContainerLeft = 300f;

		public static float NurseLoginLabelHeight = 0f;

		public static float BottomY = 1024f;

		public static float LogoY = FrameTop;

		public static float TopY = TopLogoHeight + FrameTop;
		public static float PageHeight = 1024 - FrameTop - LogoHeight;
		public static float BodyHeight = PageHeight - HeaderHeight;
		public static float TherapeuticListTableHeight = 360;
		public static float TrialOrderTableHeight = 490;

		public static float LoginImageHeight = 550;
		public static float LoginImageDistanceToNurseLogin = 20;
	}
}