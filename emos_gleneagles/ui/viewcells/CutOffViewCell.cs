﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class CutOffViewCell : BaseViewCell<CutOffModel>
	{
		public const string CellIdentifier = "CutOffViewCell";
		public const float RowHeight = 75;
		private NoCallbackViewCellHandler<CutOffModel> _handler;

		public CutOffViewCell (IntPtr handle) : base (handle)
		{
			BackgroundColor = UIColor.LightGray;
		}
		protected override void SetCellContent (CutOffModel item)
		{
			TimeLabel.Text = item.time1!="" ? item.time1 : item.time2;
			DayDescriptionLabel.Text = item.time1_day!="" ? item.time1_day : item.time2_day;
			CutoffLabel.Text = item.operation_label;
		}
		protected override void SetViewCellHandler(IViewCellHandler<CutOffModel> handler)
		{
			_handler = (NoCallbackViewCellHandler<CutOffModel>) handler;
		}
		protected override void SetLayout()
		{
			Frame = new RectangleF (Frame.Location.X, Frame.Location.Y, _handler.BaseViewCellSetting.CellWidth, RowHeight);
			CutoffLabel.Frame = new RectangleF (0, 0, _handler.BaseViewCellSetting.CellWidth, 25);
			TimeLabel.Frame = new RectangleF (0, CutoffLabel.Frame.Height, _handler.BaseViewCellSetting.CellWidth, 25);
			DayDescriptionLabel.Frame = new RectangleF (0, (TimeLabel.Frame.Height+CutoffLabel.Frame.Height), _handler.BaseViewCellSetting.CellWidth, 25);
		}
	}
}

