// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("MealOrderViewCell")]
	partial class MealOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton MealImageButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView MealOrderStaticTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView NameTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton OrderButon { get; set; }

		[Action ("MealImageButton_TouchDown:")]
		partial void MealImageButton_TouchDown (MonoTouch.Foundation.NSObject sender);

		[Action ("OrderButtonClicked:")]
		partial void OrderButtonClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (MealImageButton != null) {
				MealImageButton.Dispose ();
				MealImageButton = null;
			}

			if (MealOrderStaticTextView != null) {
				MealOrderStaticTextView.Dispose ();
				MealOrderStaticTextView = null;
			}

			if (NameTextView != null) {
				NameTextView.Dispose ();
				NameTextView = null;
			}

			if (OrderButon != null) {
				OrderButon.Dispose ();
				OrderButon = null;
			}
		}
	}
}
