﻿using System;
using ios;

namespace emos_ios
{
	public interface ICallback<T>
	{
		IBaseContext GetBaseContext();
		void ItemSelected(T selected, int selectedIndex);
	}

//	public class BaseCallback<T> : ICallback<T>
//	{
//		public IBaseContext AppContext { get; set; }
//		public BaseCallback (IBaseContext appContext)
//		{
//			AppContext = appContext;
//		}
//		public IBaseContext GetBaseContext()
//		{
//			return AppContext;
//		}
//		public virtual void ItemSelected (T selected, int selectedIndex)
//		{
//		}
//	}
}

