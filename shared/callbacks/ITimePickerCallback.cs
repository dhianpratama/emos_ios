﻿using System;

namespace emos_ios
{
	public interface ITimePickerCallback
	{
		void DoneTimePicker(TimePickerSelection selection, DateTime? date, int? index);
	}
}

