﻿using System;

namespace emos_ios
{
	public interface IDatePickerCallback
	{
		void DoneDatePicker(DateTime? date, DateType dateType);
	}
	public enum DateType
	{
		StartDate, EndDate
	}
}

