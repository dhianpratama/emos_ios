﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public class RequestEventArgs : EventArgs
	{
		public bool Success { get; set; }
		public RequestEventArgs (bool success)
		{
			Success = success;
		}
	}
	public class RequestLoadingEventArgs : EventArgs
	{
		public string LoadingText { get; set; }
		public UIView View { get; set; }
		public RequestLoadingEventArgs (UIView view, string loadingText = "Loading")
		{
			LoadingText = loadingText;
			View = view;
		}
	}
}

