﻿using System;
using System.IO;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class ServerConfig
	{
		public string BaseUrl { get; set; }
		public string User { get; private set; }
		public string Password { get; private set; }
		public long? InsitutionId { get; private set; }
		public string Version { get; private set; }
		public DateTime OperationDate { get; private set; }
		public bool IsDemo { get; private set; }
		public string Dev { get; set; }
		private string _configBaseUrl;

		public ServerConfig ()
		{
			#if DEBUG
			LoadConfig ("content/server.config.dev");
			#else
			LoadConfig ("content/server.config.prod");
			this.BaseUrl = NSUserDefaults.StandardUserDefaults.StringForKey ("BaseUrlKey");
			if (String.IsNullOrEmpty(BaseUrl))
				BaseUrl = _configBaseUrl;
			#endif
		}
		private void LoadConfig (string fileName)
		{
			var lines = File.ReadLines (fileName);
			var separator = new[] {
				'='
			};
			foreach (var line in lines) {
				var splits = line.Split (separator, 2);
				var key = splits [0];
				var value = splits [1];
				switch (key) {
				case "url":
					this.BaseUrl = value;
					_configBaseUrl = value;
					break;
				case "user":
					this.User = value;
					break;
				case "password":
					this.Password = value;
					break;
				case "institution_id":
					this.InsitutionId = Convert.ToInt32 (value);
					break;
				case "add_days_to_operation_date":
					this.OperationDate = DateTime.Today.AddDays (Convert.ToInt32 (value));
					break;
//				case "version":
//					this.Version = value;
//					break;
				case "demo":
					this.IsDemo = Convert.ToBoolean (value);
					break;
				case "dev":
					this.Dev = value;
					break;
				}
			}

			this.Version = getBundleVersionNo ();
		}
		private String getBundleVersionNo(){
			return NSBundle.MainBundle.InfoDictionary ["CFBundleShortVersionString"].ToString();
		}
		private String getBundleBuildNo(){
			return NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
		}
	}
}

