using MonoTouch.UIKit;
using System.Collections.Generic;
using System;
using emos_ios;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class CollectionSource<T, TCell> : UICollectionViewSource where TCell: BaseCollectionViewCell<T>
	{
		protected IViewCellHandler<T> CellHandler;
		protected readonly UINib Nib;
		public List<T> Items { get; set; }
		public CollectionSource(IViewCellHandler<T> cellHandler)
	    {
	        Items = new List<T>();
			CellHandler = cellHandler;
			Nib = UINib.FromName (CellHandler.CellIdentifier, NSBundle.MainBundle);
	    }
	    public override Int32 NumberOfSections(UICollectionView collectionView)
	    {
	        return 1;
	    }
	    public override Int32 GetItemsCount(UICollectionView collectionView, Int32 section)
	    {
	        return Items.Count;
	    }
	    public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
	    {
	        return true;
	    }
	//    public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
	//    {
	//        var cell = (UserCell) collectionView.CellForItem(indexPath);
	//        cell.ImageView.Alpha = 0.5f;
	//    }
	//
	//    public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
	//    {
	//        var cell = (UserCell) collectionView.CellForItem(indexPath);
	//        cell.ImageView.Alpha = 1;
	//
	//        T row = Rows[indexPath.Row];
	//        row.Tapped.Invoke();
	//    }
	//
	    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
	    {
			var cell = (TCell)collectionView.DequeueReusableCell (new NSString (CellHandler.CellIdentifier), indexPath);
			cell.BackgroundColor = CellHandler.BaseViewCellSetting.BackgroundColor;
			cell.Initialize (CellHandler, Items [indexPath.Row]);
			return cell;
	    }
	}
}