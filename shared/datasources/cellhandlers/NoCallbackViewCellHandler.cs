﻿using System;

namespace emos_ios
{
	public class NoCallbackViewCellHandler<T> : BaseViewCellHandler<T>
	{
		public NoCallbackViewCellHandler (string cellIdentifier, BaseViewCellSetting setting) : base(cellIdentifier, null, setting)
		{
		}
	}
}

