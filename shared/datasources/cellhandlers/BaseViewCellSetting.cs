﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public class BaseViewCellSetting
	{
		public bool CustomRowSize { get; set; }
		public float CellWidth { get; set;}
		public float CellHeight { get; set; }
		public UIColor BackgroundColor { get; set; }
		public BaseViewCellSetting()
		{
			BackgroundColor = Colors.ViewCellBackground;
		}
	}
}

