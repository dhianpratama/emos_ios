﻿using System;

namespace emos_ios
{
	public class StringViewCellHandler : BaseViewCellHandler<string>
	{
		public StringViewCellHandler (ICallback<string> callback, BaseViewCellSetting setting) : base(StringViewCell.CellIdentifier, callback, setting)
		{
		}
	}
}

