﻿using System;
using MonoTouch.UIKit;
using System.Collections.Generic;

namespace emos_ios
{
	public interface IViewCellHandler<T>
	{
		string CellIdentifier { get; set; }
		ICallback<T> Callback { get; set; }
		BaseViewCellSetting BaseViewCellSetting { get; set; }
		Func<T, float> GetHeightForRow { get; set; }
	}
	public class BaseViewCellHandler<T> : IViewCellHandler<T>
	{
		public string CellIdentifier { get; set; }
		public ICallback<T> Callback { get; set; }
		public BaseViewCellSetting BaseViewCellSetting { get; set; }
		public Func<T, float> GetHeightForRow { get; set; }
		public bool EpicIsUp { get; set; }
		public BaseViewCellHandler (string cellIdentifier, ICallback<T> callback, BaseViewCellSetting baseViewCellSetting)
		{
			CellIdentifier = cellIdentifier;
			Callback = callback;
			BaseViewCellSetting = baseViewCellSetting;
		}
	}
}

