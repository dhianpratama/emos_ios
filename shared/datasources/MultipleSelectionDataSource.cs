﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Linq;
using System.Collections.Generic;

namespace emos_ios
{
	public class MultipleSelectionDataSource<T, TCell> : DynamicTableSource<T, TCell> where TCell : BaseViewCell<T>
	{
		public List<NSIndexPath> SelectedIndexPaths { get; set; }
		public MultipleSelectionDataSource (IViewCellHandler<T> cellHandler) : base(cellHandler)
		{
			SelectedIndexPaths = new List<NSIndexPath> ();
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			SelectedIndexPaths.Add (indexPath);
			base.RowSelected (tableView, indexPath);
			var cell = (TCell)tableView.CellAt (indexPath);
			if (cell == null)
				return;
			cell.Accessory = UITableViewCellAccessory.Checkmark;
			cell.HandleRowSelected (Items [indexPath.Row]);
			Reload (tableView, indexPath);
		}
		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			SelectedIndexPaths.Remove (indexPath);
			var cell = (TCell)tableView.CellAt (indexPath);
			if (cell == null)
				return;
			cell.Accessory = UITableViewCellAccessory.None;
			cell.HandleRowDeselected (Items [indexPath.Row]);
			base.RowDeselected (tableView, indexPath);
		}
		private void Reload (UITableView tableView, NSIndexPath indexPath)
		{
			if (ReloadOnSelectionChanged) {
				tableView.ReloadRows (new NSIndexPath[] {
					indexPath
				}, UITableViewRowAnimation.Automatic);
				SelectedIndexPaths.ForEach (i =>  {
					tableView.SelectRow (i, false, UITableViewScrollPosition.None);
				});
			}
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = base.GetCell (tableView, indexPath);
			if (tableView.IndexPathsForSelectedRows != null && tableView.IndexPathsForSelectedRows.Any (i => i == indexPath))
				cell.Accessory = UITableViewCellAccessory.Checkmark;
			else
				cell.Accessory = UITableViewCellAccessory.None;
			return cell;
		}
		public void SelectRows(List<int> indexes, UITableView tableView, int section = 0)
		{
//			SelectedIndexPaths = new List<NSIndexPath>();
			indexes.ForEach (i => {
				var indexPath = NSIndexPath.FromRowSection (i, section);
				tableView.SelectRow(indexPath, false, UITableViewScrollPosition.None);
				RowSelected(tableView, indexPath);
			});
		}
		public void DeselectRows(List<int> indexes, UITableView tableView, int section = 0)
		{
//			SelectedIndexPaths = new List<NSIndexPath>();
			indexes.ForEach (i => {
				var indexPath = NSIndexPath.FromRowSection (i, section);
				tableView.DeselectRow(indexPath, false);
				RowDeselected(tableView, indexPath);
			});
		}
	}
}

