﻿using System;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class LanguageHandler
	{
		public string LanguageCode { get; set; }
		public LanguageHandler ()
		{

		}
		public string GetLocalizedString (string key)
		{
			var path = NSBundle.MainBundle.PathForResource (String.Format("languages/{0}",LanguageCode), "lproj");
			var languageBundle = NSBundle.FromPath (path);
			return languageBundle.LocalizedString (key, "");
		}
	}
}

