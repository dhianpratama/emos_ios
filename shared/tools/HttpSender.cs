using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Threading;
using emos_ios.tools;
using System.Collections.Specialized;
using ios;
//using System.Reflection;
using Newtonsoft.Json;

namespace emos_ios
{
	public static class QueryStringBuilder
	{
		public static string BuildUrl(string baseUrl, KnownUrl knownUrl, string queryParams = "")
		{
			return String.Format("{0}/{1}?{2}", baseUrl, knownUrl.Url, queryParams);
		}
	}
	public interface IHttpSenderContext
	{
		CookieContainer CurrentAspxAuth { get; set; }
		string BaseUrl { get; }
		void OnServerUnavailable();
		void OnConnectionProblem ();
	}

	public class HttpSender
	{
		public IHttpSenderContext Context { get; private set; }
		public SynchronizationContext SyncContext { get; private set; }

		public HttpSender(IHttpSenderContext context, IJsonSerializer jsonSerializer, SynchronizationContext syncContext)
		{
			Context = context;
			JsonSerializer = jsonSerializer;
			SyncContext = syncContext;
		}

		public HttpSender<T> Request<T>() where T : class
		{
			return new HttpSender<T>(this);
		}
		public HttpSender<object> Request()
		{
			return new HttpSenderNoResult(this);
		}

		public HttpSenderByteArray RequestRawBytes()
		{
			return new HttpSenderByteArray(this);
		}

		public DoNothingHttpSender<T> DoNothing<T>() where T : class
		{
			return new DoNothingHttpSender<T>(this);
		}

		public DoNothingHttpSender<T> StaticResult<T>(T result) where T : class
		{
			return new DoNothingHttpSender<T>(this, result);
		}

		public string BaseUrl
		{
			get { return Context.BaseUrl; }
		}
		public IJsonSerializer JsonSerializer
		{
			get; private set;
		}

		public void OnServerUnavailable()
		{
			Context.OnServerUnavailable();
		}

		public void OnConnectionProblem()
		{
			Context.OnConnectionProblem ();
		}
	}

	public class HttpSender<T> where T : class 
	{
		private const string AspxAuthCookie = ".ASPXAUTH";
		private HttpSender _httpSender;
		private SynchronizationContext _syncContext;

		public HttpSender(HttpSender httpSender)
		{
			_httpSender = httpSender;
			_transform = Transform;
			_syncContext = httpSender.SyncContext;
		}

		private KnownUrl _knownUrl;
		public HttpSender<T> From(KnownUrl knownUrl)
		{
			_knownUrl = knownUrl;
			return this;
		}

//		private object _queryParams;
		public HttpSender<T> WithQueryStringObject(object queryParams)
		{
//			_queryParams = queryParams;
			return this;
		}

		private string _queryString;
		public HttpSender<T> WithQueryString(string value)
		{
			_queryString = value;
			return this;
		}

		private object _content;
		public virtual HttpSender<T> WithContent(object content)
		{
			_content = content;
			return this;
		}
		private Action<T> _onSuccess;
		public HttpSender<T> WhenSuccess(Action<T> onSuccess)
		{
			_onSuccess = onSuccess;
			return this;
		}
		public HttpSender<T> WhenSuccess(Action onSuccess)
		{
			_onSuccess = o => onSuccess();
			return this;
		}

		private Action<HttpStatusCode> _onFail;
		public HttpSender<T> WhenFail(Action<HttpStatusCode> onFail)
		{
			_onFail = onFail;
			return this;
		}
		public HttpSender<T> WhenFail(Action onFail)
		{
			_onFail = (s) => onFail();
			return this;
		}

		public HttpSender<T> NoTransform()
		{
			_transform = null;
			return this;
		}

		private Func<HttpResponseMessage, T> _transform;
		public HttpSender<T> WithTransform(Func<HttpResponseMessage, T> transform)
		{
			_transform = transform;
			return this;
		}

		private bool _stayOnBackground;
		public HttpSender<T> StayOnBackground(bool stayOnBackground)
		{
			_stayOnBackground = stayOnBackground;
			return this;
		}

		private bool _getAuthenticationCookie;
		public HttpSender<T> GetAuthenticationCookie()
		{
			_getAuthenticationCookie = true;
			return this;
		}

		public virtual T GoSync()
		{
			return Go(false);
		}
		public class NoKeepAlivesWebClient : WebClient
		{
			protected override WebRequest GetWebRequest(Uri address)
			{
				var request = base.GetWebRequest(address);
				if (request is HttpWebRequest)
				{
					((HttpWebRequest)request).KeepAlive = false;
				}

				return request;
			}
		}	
		private T Go(bool useSyncContext)
		{
			using (var client = new HttpClient(AttachAuthentication ()))
			{
				var task = ExecuteRequest(client, null);
				HttpResponseMessage response = null;
				try 
				{
					response = task.Result;
					if (response.StatusCode != HttpStatusCode.OK)
					{
						var errorMessage = String.Format("Failed to send request to {0}. HttpResponse: {1}",
							response.RequestMessage.RequestUri,
							response.StatusCode);
						throw new Exception(errorMessage);
					}
					ReadAuthenticationCookie (response);
					var responseContent = _transform != null ? _transform(response) : default(T);
					_syncContext.Post(c => NotifyOnSuccess((T)c), responseContent, useSyncContext);

					return responseContent;
				}
				catch (WebException ex)
				{
					if (ex.Status == WebExceptionStatus.ConnectFailure)
					{
						_syncContext.Post (_ => _httpSender.OnConnectionProblem (), null, useSyncContext);
					} 
					else
					{
						_syncContext.Post (r => NotifyOnFail ((HttpResponseMessage)r), response, useSyncContext);
					}
					return default(T);
				}
				catch (Exception)
				{
					_syncContext.Post (r => NotifyOnFail ((HttpResponseMessage)r), response, useSyncContext);
					return default(T);
				}
			}
		}
		private void ReadAuthenticationCookie (HttpResponseMessage response)
		{
			if (_getAuthenticationCookie) {
				IEnumerable<string> cookies;
				if (response.Headers.TryGetValues ("set-cookie", out cookies)) {
					var auth = cookies.Single (c => c.StartsWith (AspxAuthCookie));
					var uri = response.RequestMessage.RequestUri;
					var cookieContainer = new CookieContainer ();
					cookieContainer.SetCookies (uri, auth);
					_httpSender.Context.CurrentAspxAuth = cookieContainer;
				}
			}
		}
		private HttpClientHandler AttachAuthentication ()
		{
			var handler = new HttpClientHandler ();
			if (!_getAuthenticationCookie)
				handler.CookieContainer = _httpSender.Context.CurrentAspxAuth;
			return handler;
		}
		public virtual async Task<T> Go()
		{
			return await Task.Factory.StartNew<T>(() => Go(!_stayOnBackground)).ConfigureAwait(!_stayOnBackground);
		}
			
		private Task<HttpResponseMessage> ExecuteRequest(HttpClient client, AuthenticationHeaderValue authHeader)
		{
			var url = QueryStringBuilder.BuildUrl(_httpSender.BaseUrl, _knownUrl, _queryString);
			client.DefaultRequestHeaders.ConnectionClose = true;
			return _knownUrl.Method == HttpMethod.Get ? client.GetAsync(url) : PostAsJsonAsync(client, url);
		}
		private Task<HttpResponseMessage> PostAsJsonAsync(HttpClient httpClient, string url)
		{
			var body = _httpSender.JsonSerializer.Serialize (_content);
			var httpContent = new StringContent (body);
			httpContent.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
			return httpClient.PostAsync (url, httpContent);
		}
		private static T Transform(HttpResponseMessage response)
		{
			return response.GetContent<T> ();
		}

		protected void NotifyOnSuccess(T content)
		{
			if (_onSuccess != null)
				_onSuccess (content);
		}

		protected void NotifyOnFail(HttpResponseMessage response)
		{
			if (response != null)
			{
				if (_onFail != null)
					_onFail (response.StatusCode);
			} 
			else
			{
				if (_onFail != null)
					_onFail (HttpStatusCode.InternalServerError);
			}
		}

	}

	public class HttpSenderByteArray : HttpSender<byte[]>
	{
		public HttpSenderByteArray(HttpSender httpSender) : base(httpSender)
		{
			WithTransform(TransformFunc);
		}

		private byte[] TransformFunc (HttpResponseMessage response)
		{
			var readTask = response.Content.ReadAsByteArrayAsync();
			readTask.Wait();
			return readTask.Result;
		}
	}

	public class HttpSenderNoResult : HttpSender<object>
	{
		public HttpSenderNoResult(HttpSender httpSender) : base(httpSender)
		{
			NoTransform();
		}
	}

	public class DoNothingHttpSender<T> : HttpSender<T> where T : class
	{
		private T _result;

		public DoNothingHttpSender(HttpSender httpSender, T result = default(T)) : base(httpSender)
		{
			_result = result;

		}
		public override Task<T> Go ()
		{
			if (_result != default(T))
				NotifyOnSuccess(_result);
			return Task.Run(() => _result);
		}
	}

	public static class SyncContextExtensions
	{
		public static void Post(this SynchronizationContext syncContext, SendOrPostCallback callback, object arg, bool useSyncContext)
		{
			if (useSyncContext)
				syncContext.Post (callback, arg);
			else
				callback (arg);
		}
	}
}
