using System;
using MonoTouch.Foundation;
using System.Drawing;
using MonoTouch.UIKit;

namespace emos_ios
{
	public class KeyboardHandler
	{
		public UIView ActiveView;             // Controller that activated the keyboard
		public float ScrollAmount;
		private NSObject _keyboardUpObserver;
		private NSObject _keyboardDownObserver;

		private float _scrollAmount = 0.0f;    // amount to scroll 
		private float _originYPoint = 0.0f;    // amount to scroll 

//		private float bottom = 0.0f;           // bottom point
//		private float offset = 10.0f;          // extra offset
		private bool _moveViewUp = false;       // which direction are we moving

		public KeyboardHandler() {}
		public void AddKeyboardObservers (UIView view)
		{
			_moveViewUp = false;
			RemoveKeyboardObservers ();
			ActiveView = view;

			_originYPoint = view.Frame.Y;
			_keyboardDownObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyBoardDownNotification);
			_keyboardUpObserver = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyBoardUpNotification);
		}
		public void RemoveKeyboardObservers ()
		{
			if (_keyboardUpObserver != null)
				NSNotificationCenter.DefaultCenter.RemoveObserver (_keyboardUpObserver);
			if (_keyboardDownObserver != null)
				NSNotificationCenter.DefaultCenter.RemoveObserver (_keyboardDownObserver);
		}
		public void KeyboardAnimate(NSNotification notification){
			// get the keyboard size
			var val = new NSValue(notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey).Handle);
			RectangleF r = val.RectangleFValue;

			_moveViewUp = notification.Name == UIKeyboard.WillShowNotification;

			var keyboardFrame = _moveViewUp
				? UIKeyboard.FrameEndFromNotification(notification)
				: UIKeyboard.FrameBeginFromNotification(notification);

			if (_moveViewUp)
				_scrollAmount = -keyboardFrame.Height;
			else
				_scrollAmount = _originYPoint;

			ScrollTheView (_moveViewUp);
		}
		public void KeyBoardUpNotification(NSNotification notification)
		{
			KeyboardAnimate (notification);
		}
		public void KeyBoardDownNotification(NSNotification notification)
		{
			KeyboardAnimate (notification);
		}
		private void ScrollTheView(bool move)
		{
			// scroll the view up or down
			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration (0.3);

			RectangleF frame = ActiveView.Frame;

			if (move) {
				frame.Y = _scrollAmount;
			} else {
				frame.Y = _scrollAmount;
				_scrollAmount = 0;
			}

			ActiveView.Frame = frame;
			UIView.CommitAnimations();
		}
	}
}

