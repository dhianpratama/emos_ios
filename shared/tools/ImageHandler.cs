﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using MonoTouch.Dialog.Utilities;
using emos_ios.tools;
using ios;

namespace emos_ios
{
	public class ImageHandler
	{
		public const string ImageNotAvailable = "Images/image-not-available.png";
		public ImageHandler (IBaseContext appContext)
		{
		}
		public async Task<UIImage> FromUrl(string uri, bool isRelative=false)
		{
			var httpClient = new HttpClient();
			Task<byte[]> contentsTask = httpClient.GetByteArrayAsync (uri);
			// await! control returns to the caller and the task continues to run on another thread
			var contents = await contentsTask;
			// load from bytes
			return UIImage.LoadFromData (NSData.FromArray (contents));
		}
	}
}

