﻿using System;

namespace emos_ios
{
	public static class OperationCodeHandler
	{
		public static OperationCodeStatus GetStatus (long? operationCode)
		{
			switch(operationCode)
			{
			case 1:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Order"), 
					ImagePath = "Images/btn_order.png"
				};
			case 2:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("View"), 
					ImagePath = "Images/btn_edit.png"
				};
			case 3:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Edit"), 
					ImagePath = "Images/btn_edit.png"
				};
			case 4:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Reorder"), 
					ImagePath = "Images/btn_reorder.png"
				};
			case 5:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Ordered"), 
					ImagePath = "Images/btn_ordered.png"};
			case 6:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("NoOrder"), 
					ImagePath = "Images/btn_no_order.png"};
			case 7:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("NoOrder"), 
					ImagePath = "Images/btn_nbm.png"
				};
			case 11:
				return new OperationCodeStatus { 
					Status = AppDelegate.Instance.LanguageHandler.GetLocalizedString("ViewDraft"), 
					ImagePath = "Images/btn_edit.png"};
			default:
				return new OperationCodeStatus ();
			};
		}

		public static OperationCodeStatus GetConsumptionStatus(long? operationCode)
		{
			switch(operationCode)
			{
			case 2:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_edit.png"};
			case 3:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_no_order.png"};
			case 4:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_nbm.png"};
			case 99:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_no_order.png"};
			case 100:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/cumulative_btn.png"};
			default:
				return new OperationCodeStatus ();
			};
		}

		public static OperationCodeStatus GetConsumptionStatus(long? operationCode, bool ordered)
		{
			if (operationCode == 4)
				return new OperationCodeStatus { Status = "NBM", ImagePath = "Images/btn_nbm.png"};
			if (ordered)
				return new OperationCodeStatus { Status = "Order", ImagePath = "Images/btn_edit.png" };
			else
				return new OperationCodeStatus { Status = "Edit", ImagePath = "Images/btn_no_order.png"};
		}
	}
}

