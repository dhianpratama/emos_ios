﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public static class CommonTools
	{
		public static string ConvertToAmPm(string time)
		{
			var result = "N/A";
			if (!String.IsNullOrEmpty(time)) {
				var times = time.Split (':');
				if (times.Count() > 1) {
					var hour = Convert.ToInt16 (times [0]);
					var ampm = "AM";

					if (hour >= 12) {
						hour -= 12;
						ampm = "PM";
					} else {
						if (hour == 0)
							hour = 12;
					}

					result = hour.ToString () + ":" + times [1] + " " + ampm;
				}
			}
			return result;
		}
	}
}

