﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public static class ViewHandler
	{
		public static void ClearSubviews(UIView parentView)
		{
			foreach (var view in parentView.Subviews)
				view.RemoveFromSuperview ();
		}
		public static UIView DrawVerticalLine(RectangleF frame, UIColor color)
		{
			var verticalLineView = new UIView {
				Frame = frame,
				BackgroundColor = color
			};
			return verticalLineView;
		}

	}
}

