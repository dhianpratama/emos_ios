﻿using System;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace emos_ios
{

	//custom class to get the Tapped event to work in a RadioElement
	public class OptionsRadioElement: RadioElement
	{
		public OptionsRadioElement(string caption, NSAction tapped): base(caption)
		{
			Tapped += tapped;
		}
	}

	public class DebugRadioElement : RadioElement {
		Action<DebugRadioElement, EventArgs> onCLick;

		public DebugRadioElement (string s, Action<DebugRadioElement, EventArgs> onCLick) : base (s) {
			this.onCLick = onCLick;
		}

		public override void Selected (DialogViewController dvc, UITableView tableView, NSIndexPath path)
		{
			base.Selected (dvc, tableView, path);
			var selected = onCLick;
			if (selected != null)
				selected (this, EventArgs.Empty);
		}
	}
}

