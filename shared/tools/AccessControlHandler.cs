﻿using System;

using System.Linq;

namespace emos_ios
{
	public static class AccessControlHandler
	{
		public static bool Authorized(string aclIdentifier)
		{
			if (AppDelegate.Instance.UserAccessControl.Any (e => e == aclIdentifier))
				return true;
			else
				return false;
		}
	}
}

