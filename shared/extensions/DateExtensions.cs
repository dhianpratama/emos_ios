﻿using System;
using System.Web;

namespace emos_ios
{
	public static class DateExtensions
	{
		public const string DisplayDateFormat = "dd-MMM-yyyy hh:mm tt";
		public static string ToMvcDateString(this DateTime dateTime)
		{
			return  (dateTime.ToString ("yyyy-MM-dd HH:mm:ss"));
		}
		public static DateTime ToDisplayDatetime(this DateTime dateTime)
		{
			return dateTime.ToLocalTime ();
		}
		public static string ToDisplayDateString(this DateTime dateTime)
		{
			return dateTime.ToLocalTime ().ToString ("dd-MMM-yyyy hh:mm tt");
		}
		public static string ToDisplayDateFormatInString(this DateTime dateTime)
		{
			return dateTime.ToLocalTime ().ToString ("dd-MMM-yyyy");
		}
	}
}

