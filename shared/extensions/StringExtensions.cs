﻿using System;

namespace emos_ios
{
	public static class StringExtensions
	{
		public static bool IsNumeric(this String value)
		{
			if (String.IsNullOrEmpty (value))
				return false;
			bool isNum;
			double retNum;
			isNum = Double.TryParse(value, System.Globalization.NumberStyles.Any,System.Globalization.NumberFormatInfo.InvariantInfo, out retNum );
			return isNum;
		}
	}
}

