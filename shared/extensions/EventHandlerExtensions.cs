﻿using System;

namespace emos_ios
{
	public static class EventHandlerExtensions
	{
		public static void SafeInvoke (this EventHandler eventHandler, Object invoker)
		{
			var handler = eventHandler;
			if (handler != null)
				handler.Invoke (invoker, new EventArgs ());
		}
		public static void SafeInvoke<T> (this EventHandler<T> eventHandler, Object invoker, T args) where T : EventArgs
		{
			var handler = eventHandler;
			if (handler != null)
				handler.Invoke (invoker, args);
		}
	}
}

