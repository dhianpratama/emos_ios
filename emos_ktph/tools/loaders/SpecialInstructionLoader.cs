﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios;

namespace emos_ktph
{
	public class SpecialInstructionLoader : ISpecialInstructionLoader
	{
		public void LoadSpecialInstruction (IApplicationContext appContext, LocationWithRegistrationSimple location, IPatientMenu callback, InterfaceStatusModel _interfaceStatus)
		{
			var controller = new SpecialInstructionViewController (appContext, callback, location, _interfaceStatus) {
				
			};
			appContext.Nav.PushViewController(controller, true);
		}
	}
}

