﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios;

namespace emos_ktph
{
	public class AdmissionLoader : IAdmissionLoader
	{
		public void LoadEditAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location, EventHandler onSave = null)
		{
			var controller = new AdmissionViewController (appContext, location);
			if (onSave != null)
				controller.OnSave += onSave;
			appContext.LoadController (false, true, controller);
		}
		public void LoadViewAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location)
		{
			var controller = new AdmissionViewController (appContext, location, true);
			appContext.LoadController (false, true, controller);
		}
	}
}

