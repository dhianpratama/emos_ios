﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class BulkOrderSummaryLoader : IBulkOrderSummaryLoader
	{
		public void Load (IApplicationContext appContext, KeyValuePair<string,string> location)
		{
			var controller = new BulkOrderSummaryViewController (appContext, location);
			appContext.LoadController (true, false, controller);
		}
	}
}

