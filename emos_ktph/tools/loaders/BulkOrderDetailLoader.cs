﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios
{
	public class BulkOrderDetailLoader : IBulkOrderDetailLoader
	{
		public void Load (IApplicationContext appContext, KeyValuePair<string, string> location, BulkMealRestrictionGroupInfo model, EventHandler onSave, MealOrderPeriodModel mealPeriod)
		{
			var controller = new BulkOrderViewController (appContext, location, model, mealPeriod);
			controller.OnSave += onSave;
			appContext.LoadController (false, true, controller);
		}
	}
}

