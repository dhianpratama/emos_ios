﻿using System;
using ios;
using emos_ktph;
using feedback;

namespace emos_ios
{
	public class ControllerLoader : IControllerLoader
	{
		public IAdmissionLoader AdmissionLoader { get; set; }
		public ISpecialInstructionLoader SpecialInstructionLoader { get; set;}
		public IFeedbackLoader FeedbackLoader { get; set;}
		public ILoader PatientInfoLoader { get; set;}
		public ISettingLoader SettingLoader { get; set;}
		public IBulkOrderSummaryLoader BulkOrderSummaryLoader { get; set;}
		public IBulkOrderDetailLoader BulkOrderDetailLoader { get; set;}

		public ControllerLoader()
		{
			AdmissionLoader = new AdmissionLoader ();
			SpecialInstructionLoader = new SpecialInstructionLoader();
			FeedbackLoader = new FeedbackLoader ();
			SettingLoader = new SettingLoader ();
			BulkOrderSummaryLoader = new BulkOrderSummaryLoader ();
			BulkOrderDetailLoader = new BulkOrderDetailLoader ();
		}
	}
}

