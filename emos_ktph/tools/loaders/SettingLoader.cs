﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class SettingLoader : ISettingLoader
	{
		public void Load (IApplicationContext appContext, bool hideDateSetting = false, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDone = null, EventHandler OnCancel = null, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDateChanged = null)
		{
			var controller = new SettingV2ViewController (appContext, hideDateSetting);
			controller.OnDone += OnDone;
			controller.OnCancel += OnCancel;
			appContext.LoadModalView (controller.View);
		}
	}
}

