﻿using System;

namespace emos_ios
{
	//Jurong
	public static class Settings
	{
		public static bool UseDateAsDashboardTitle = true;
		public static bool HideNurseDashboardTab = false;
		public static bool ShowCompanionLabelOnDashboard = false;
		public static bool ShowBedNameOnDashboard = false;
		public static bool HideMealOnHold = true;
		public static bool HideStayForMeal = true;
		public static bool HideEarlyDelivery = true;
		public static long DefaultNewExtraOrderQuantity = 1;
		public static bool IgnoreIncompleteOrder = false;
		public static bool UseMealPeriodAsBirthdayCakeSegment = true;
		public static bool ChangeCompanionLabelToLodger = false;
		public static bool SimpleTrialOrder = false;
		public static bool DoubleTapNurseDashboardTableView = false;
		public static bool UseDietOrderText = true;
		public static bool UseExtraOrdersText = true;
		public static bool MultipleCompanionOrder = true;
		public static bool MandatoryMainDish = true;
		public static bool HideMealPeriodGroupInSetting = false;
		public static bool EnableDeclineMealOrder = false;
		public static bool HideSaveDraft = false;
		public static bool FoodAllergiesRemarkSpecialQuery = true;
		public static bool ShowOtherDietsCommentField = false;
		public static bool ShowActiveDietInformation = true;
		public static bool ShowOnGoingRemark = true;
		public static bool ShowPatientPreference = false;
		public static bool CheckLock = false;
		public static bool ShowDVCTitle = true;
		public static bool ShowAdditionalPatientMenuLinks = false;
		public static bool UseRegistrationNumberInsteadOfId = false;
		public static int StartDateToEndDateDefaultDays = 7;
		public static bool DisableExtraOrderEdit = true;
		public static bool OneDishMeal = true;
		public static bool LoadExistingOrderOnEdit = true;
		public static bool AllowDashboardCutoffToggle = false;
		public static bool PatientDailyOrderMode = false;
		public static bool QueryMealTypeWithFeeds = false;
		public static bool EnableTheme = false;
		public static bool FoodTextureIsCompulsory = false;
		public static bool ShowDetailOnImageView = false;
		public static bool ShowYesNoImageButtonOnMealOrderDetail = true;
		public static bool UseSmallCap = false;
		public static bool ShowOrderRoleSelection = true;
		public static bool ShowMealOrderTitle = false;
		public static bool ShowGotoPatientModeButtonOnMealOrder = true;
		public static bool ShowImageAsHeader = true;
		public static bool DietsEndToday = false;
		public static bool CommonDietsVersion2 = false;
		public static bool HideLoginNavigationBar = true;
		public static bool ShowWardSelectionInLogin = true;
		public static bool ReplaceMealOrderBackButton = true;
		public static bool UseDateForDiets = false;
		public static bool HospitalBasedWards = false;
		public static bool GradientMenuView = true;
		public static bool ShowInfoMsgOnMealOrdering = false;
		public static bool MealOrderBasedPatientInfo = false;
		public static bool HideNurseDashboardIcons = true;
		public static bool HideAvailability = true;
		public static bool HideDateSettingOnPatientMeal = true;
		public static bool ShowTherapeuticComment = true;
		public static bool CompanionMealChargeableOption = true;
		public static bool CurrentAndNextMealPeriodExtraOrder = true;
		public static bool ExtraOrderQuantityInMealOrderHidden = false;
		public static int ExtraOrderMinimumQuantity = 0;
		public static int ExtraOrderMaximumQuantity = 3;
		public static int MaximumRemarkLengthInMealOrder = 200;
		public static bool CheckLoginAcl = true;
		public static bool HideSkippedStep = false;
		public static bool ShowAvailabilityInMealOrderTabs = true;
		public static bool AllowToAddOnlyOneTherapeutic = false;
		public static bool HideMenuOfTheDay = false;
		public static bool HideManualAdmissionOnSapUp = true;
	}
}
