﻿using System;

namespace emos_ios
{
	public static class LanguageKeys
	{
		public static string UserName = "Username";
		public static string UsernamePasswordIncorrect = "UsernamePasswordIncorrect";
		public static string ExtraOrder = "SpecialInstruction";
		public static string ExtraOrders = "SpecialInstructions";
		public static string Companion = "Companion";
		public static string NoExtraOrderSelected = "Noextraorderselected.";
		public static string NoExtraOrderIsFound = "Noextraorderisfound";
		public static string DeleteExtraOrderConfirmation = "Areyousurewanttodeletethisextraorder?";
		public static string DeletedExtraOrderInformation = "ExtraOrder'{0}'hasbeendeleted";
		public static string Therapeutic = "Diet Order";
		public static string FullFeedsTitle = "FullFeeds[DIET135]";
		public static string ClearFeedsTitle = "ClearFeeds[DIET136]";
		public static string NilByMouthTitle = "NilbyMouth[DIET41]";
		public static string NoDietOnTherapeuticFound = "NodietorderunderTherapeutic(Diet24)isfound";
		public static string CompanionOrder = "CompanionOrder";
		public static string DietOrderNotSet = "Pleasesetdietorderforpatientbeforeorderingmeals";
	}
}

