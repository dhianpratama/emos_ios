﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class PatientOrderCommandCollectionViewCell : BaseCollectionViewCell<PatientOrderCommand>
	{
		public const string CellIdentifier = "PatientOrderCommandCollectionViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private PatientDayOrderViewCellHandler _handler;
		private IPatientDayOrderCallback _callback;
		private PatientOrderCommand _item;
		public long? OperationCode;
		public RegistrationModelSimple Registration;
		public LocationModelSimple Location;
		public MealOrderPeriodModelSimple MealOrderPeriod;

		public PatientOrderCommandCollectionViewCell (IntPtr handle) : base (handle)
		{
		}
		public static PatientOrderCommandCollectionViewCell Create ()
		{
			return (PatientOrderCommandCollectionViewCell)Nib.Instantiate (null, null) [0];
		}
		public void SetViewCellHandler(IViewCellHandler<LocationWithRegistrationSimple> handler)
		{
			_handler = (PatientDayOrderViewCellHandler) handler;
			_callback = (IPatientDayOrderCallback) _handler.Callback;
		}
		protected override void SetCellContent (PatientOrderCommand item)
		{
			_item = item;
			OrderButton.SetImage (UIImage.FromBundle(item.ImagePath), UIControlState.Normal);
		}
		public void SetContent (PatientOrderCommand item)
		{
			SetCellContent(item);
		}
		partial void ButtonOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OrderButtonClicked(_item);
		}
		public void SetOrderByLabel()
		{

		}
	}
}

