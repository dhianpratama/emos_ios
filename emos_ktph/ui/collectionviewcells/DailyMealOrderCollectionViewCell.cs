﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using core_emos;
using System.Linq;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class DailyMealOrderCollectionViewCell : BaseCollectionViewCell<MealCalendar>, IPatientOrderCommandCallback
	{
		public const string CellIdentifier = "DailyMealOrderCollectionViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private MealCalendar _item;
		private DailyPatientMealCalendarViewCellHandler _handler;
		private DynamicTableSource<PatientOrderCommand, VerticalMealOrderViewCell> _orderDataSource;
		private IDailyPatientMealCallback _callback;

		public DailyMealOrderCollectionViewCell (IntPtr handle) : base (handle)
		{
		}
		public static DailyMealOrderCollectionViewCell Create ()
		{
			return (DailyMealOrderCollectionViewCell)Nib.Instantiate (null, null) [0];
		}

		protected override void SetViewCellHandler(IViewCellHandler<MealCalendar> handler)
		{
			_handler = (DailyPatientMealCalendarViewCellHandler)handler;
			_callback = (IDailyPatientMealCallback)_handler.Callback;
			var orderViewCellSetting = new BaseViewCellSetting {
				CellWidth = handler.BaseViewCellSetting.CellWidth,
				CellHeight = _handler.Setting.ChildMealViewCellHeight
			};
			var verticalHandler = new DailyPatientMealViewCellHandler (VerticalMealOrderViewCell.CellIdentifier, this, orderViewCellSetting);
			_orderDataSource = new DynamicTableSource<PatientOrderCommand, VerticalMealOrderViewCell> (verticalHandler);
		}
		protected override void SetCellContent (MealCalendar item)
		{
			_item = item;
			DateLabel.Text = item.OrderDate.ToString("dd MMM");
			DayLabel.Text = item.OrderDate.ToString ("ddd");
			_orderDataSource.Items = item.PatientOrderCommands;
			OrderTable.SetFrame (height: _handler.Setting.ChildMealViewCellHeight * _item.PatientOrderCommands.Count);
			OrderTable.RowHeight = _handler.Setting.ChildMealViewCellHeight;
			OrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			OrderTable.Source = _orderDataSource;
			OrderTable.ReloadData ();
		}
		#region IPatientOrderCommandCallback implementation

		public void OrderButtonClicked (PatientOrderCommand item)
		{
			_callback.OrderButtonClicked (item);
		}

		#endregion

		#region ICallback implementation

		public ios.IBaseContext GetBaseContext ()
		{
			return _callback.GetBaseContext ();
		}

		public void ItemSelected (PatientOrderCommand selected, int selectedIndex)
		{
			_callback.OrderButtonClicked (selected);
		}

		#endregion
	}
}


