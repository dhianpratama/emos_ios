﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class OrderCollectionViewCell : BaseCollectionViewCell<PatientOrderCommand>
	{
		public const string CellIdentifier = "OrderCollectionViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private PatientOrderCommandViewCellHandler _handler;
		private IPatientOrderCommandCallback _callback;
		private PatientOrderCommand _item;

		public OrderCollectionViewCell (IntPtr handle) : base (handle)
		{
		}

		public static OrderCollectionViewCell Create ()
		{
			return (OrderCollectionViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetViewCellHandler(IViewCellHandler<PatientOrderCommand> handler)
		{
			_handler = (PatientOrderCommandViewCellHandler) handler;
			_callback = (IPatientOrderCommandCallback) _handler.Callback;
		}
		protected override void SetCellContent (PatientOrderCommand item)
		{
			_item = item;
			//OrderButton.SetFrame (width: 60f, height: 60f);
			OrderButton.SetImage (UIImage.FromBundle(item.ImagePath), UIControlState.Normal);
			OrderButton.SetTitle ("", UIControlState.Normal);
			OrderByLabel.Text = item.OrderBy;
			this.SetFrame (x: _handler.BaseViewCellSetting.CellWidth * item.Index, width: _handler.BaseViewCellSetting.CellWidth, height: Measurements.NurseDashboardRowHeight);
		}
		partial void ButtonOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OrderButtonClicked(_item);
		}
	}
}

