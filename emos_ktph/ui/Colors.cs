﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public static class Colors
	{
		public static UIColor DarkViewCellBackground = UIColor.LightGray;
		public static UIColor ViewCellBackground = UIColor.FromRGB(230, 230, 230);
		public static UIColor MealOrderBackground = UIColor.FromRGB(238, 238, 238);
		public static UIColor HeaderBackground = UIColor.FromRGB(54, 147, 180);
//		public static UIColor MainThemeColor = UIColor.FromRGB(78, 154, 19);
		public static UIColor MainThemeColor = UIColor.FromRGB (23, 105, 178);
		public static UIColor ButtonBackground = UIColor.FromRGB(0, 107, 179);
		public static UIColor MealOrderStepButtonBackground = UIColor.DarkGray;
		public static UIColor PlannedDischargeBackground = RegularAdmissionBackground;
		public static UIColor RegularAdmissionBackground = UIColor.FromRGBA (49, 136, 204, 48);
		public static UIColor ManualAdmissionBackground = UIColor.FromRGBA (147, 118, 32, 30);
		public static UIColor ManualAdmissionButtonColor = UIColor.FromRGBA (149, 194, 101, 255);
		public static UIColor ManualAdmissionButtonFontColor = UIColor.White;
		public static UIColor NurseDashboardPatientTableSeparatorColor = UIColor.White;
		public static UIColor NurseDashboardHeaderBackgroundColor = UIColor.FromRGB (54, 147, 180);
		public static UIColor NurseDashboardHeaderButtonColor = UIColor.FromRGBA(11, 37, 61, 200);
		public static UIColor NurseDashboardHeaderFontColor = UIColor.White;
		public static UIColor NurseDashboardTopBarColor = UIColor.FromRGB (23, 105, 178);
	}
}

