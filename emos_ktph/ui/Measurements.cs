﻿using System;

namespace emos_ios
{
	public static class Measurements
	{
		public static float FrameTop = 64f;
		public static float LogoHeight = 0f;
		public static float KeyboardHeight = 270f;
		public static float TopLogoY = 0f;
		public static float TopLogoHeight = 0f;
		public static float HeaderHeight = 240f;
		public static float MealOrderHeaderHeight = 290f;
		public static float HeaderImageHeight = 205f;

		public static float BottomLogoY = 920f;
		public static float BottomLogoHeight = 104;

		public static float NurseDashboardRowHeight = 80f;
		public static float NurseDashboardTopBarHeight = 60f;
		public static float CompanionMealOrderHeaderHeight = 290f;
		public static float PatientMealOrderHeaderHeight = HeaderHeight;
		public static float NurseDashboardCutoffTitleHeight = 50f;
		public static float NurseLoginLabelHeight = 40f;
		public static float NurseDashboardOperationContainerLeft = 408f;

		public static float BottomY = 1024f;
		public static float LogoY = 920f;

		public static float TopY = TopLogoHeight + FrameTop;
		public static float PageHeight = 1024 - FrameTop - LogoHeight;
		public static float BodyHeight = PageHeight - HeaderHeight;
		public static float TherapeuticListTableHeight = 800;
		public static float TrialOrderTableHeight = 800;
		public static float LoginImageHeight = 480;
		public static float LoginImageDistanceToNurseLogin = 140;
	}
}

