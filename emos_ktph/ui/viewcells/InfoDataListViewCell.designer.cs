// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ktph
{
	[Register ("InfoDataListViewCell")]
	partial class InfoDataListViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView SeparatorImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SeparatorView { get; set; }

		[Action ("DeleteButton_TouchUpInside:")]
		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("EditButton_TouchUpInside:")]
		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (EditButton != null) {
				EditButton.Dispose ();
				EditButton = null;
			}

			if (SeparatorImage != null) {
				SeparatorImage.Dispose ();
				SeparatorImage = null;
			}

			if (SeparatorView != null) {
				SeparatorView.Dispose ();
				SeparatorView = null;
			}
		}
	}
}
