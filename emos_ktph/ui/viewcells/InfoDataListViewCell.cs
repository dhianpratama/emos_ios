﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;

namespace emos_ktph
{
	public partial class InfoDataListViewCell : BaseViewCell<List<KeyValuePair<string,string>>>
	{
		public static readonly UINib Nib = UINib.FromName ("InfoDataListViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("InfoDataListViewCell");

		public const string CellIdentifier = "InfoDataListViewCell";
		private InfoDataListHandler _handler;
		private ICommonDataListCallback _callback;

		private static float RowHeight = 25;
		private static float FontSize = 14f;

		private List<KeyValuePair<string,string>> _items;

		public InfoDataListViewCell (IntPtr handle) : base (handle)
		{
		}

		public static InfoDataListViewCell Create ()
		{
			return (InfoDataListViewCell)Nib.Instantiate (null, null) [0];
		}

		protected override void SetViewCellHandler(IViewCellHandler<List<KeyValuePair<string,string>>> handler)
		{
			_handler = (InfoDataListHandler) handler;
			_callback = (ICommonDataListCallback) _handler.Callback;
		}

		protected override void SetCellContent (List<KeyValuePair<string,string>> items)
		{
			_items = items;
			InitializeButtons ();
			GenerateDishList (items);
			CheckButtonDisability ();
		}
		private void InitializeButtons ()
		{
			if (!_callback.EditEnabled) {
				DeleteButton.Hidden = true;
				EditButton.Hidden = true;
				return;
			}
			DeleteButton.SetImage (UIImage.FromBundle ("Images/delete_icon.png"), UIControlState.Normal);
			EditButton.SetImage (UIImage.FromBundle ("Images/edit_icon.png"), UIControlState.Normal);
			EditButton.ImageEdgeInsets = new UIEdgeInsets (10, 5, 10, 5);
			DeleteButton.ImageEdgeInsets = new UIEdgeInsets (10, 5, 10, 5);

		}
		public void GenerateDishList(List<KeyValuePair<string,string>> items)
		{
			_items = items;
			var tempItems = items.Where(e=> e.Key!="Id").ToList();
			SizeF previousSize = new SizeF ();
			for(int i=0; i < tempItems.Count; i++) {
				var keyLabel = new UILabel();
				keyLabel.Font = UIFont.FromName (Fonts.KeyFontName, 16f);
				keyLabel.Text = String.IsNullOrEmpty (tempItems [i].Key) ? "" : String.Format ("{0} {1} ", tempItems [i].Key, ":");
				keyLabel.Frame = new RectangleF (130, (i * RowHeight) + 25, GetLabelWidth(tempItems[i].Key), RowHeight);
				AddSubview(keyLabel);

				var size = keyLabel.Text.Length == 0 ? previousSize : keyLabel.StringSize(keyLabel.Text, keyLabel.Font, new SizeF(keyLabel.Frame.Width, 20));
				var valueLabel = new UILabel();
				valueLabel.Font = UIFont.FromName(Fonts.ValueFontName, FontSize);
				valueLabel.Text = tempItems [i].Value;
				valueLabel.Frame = new RectangleF (135+size.Width, (i * RowHeight) + 25, GetLabelWidth(tempItems[i].Value), RowHeight);
				valueLabel.LineBreakMode = UILineBreakMode.WordWrap;
				AddSubview(valueLabel);

				this.Frame = new RectangleF (0, 0, this.Frame.Width, tempItems.Count * (FontSize + 20));

				//SeparatorView.Frame = new RectangleF (SeparatorView.Frame.X, SeparatorView.Frame.Y, 2, GetRowHeight (items));
				previousSize = size;
			};
		}

		private int GetLabelWidth(string text)
		{
			return !String.IsNullOrEmpty(text) ? (5 + text.Length) * Convert.ToInt32 (FontSize) : 0;
		}

		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OnDelete(GetId());
		}

		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OnEdit(GetId());
		}

		public static float GetRowHeight(List<KeyValuePair<string,string>> item)
		{
			if(item!=null)
				return ((item.Count-1) * RowHeight) + 50;

			return RowHeight;
		}

		private long GetId()
		{
			return Convert.ToInt32 (_items.First (e => e.Key == "Id").Value);
		}

		private void CheckButtonDisability()
		{
			if (_handler.EpicIsUp) {
				EditButton.Enabled = false;
				EditButton.TintColor = UIColor.LightGray;

				DeleteButton.Enabled = false;
				DeleteButton.TintColor = UIColor.LightGray;
			} else {
				EditButton.Enabled = true;
				EditButton.TintColor = UIColor.Black;

				DeleteButton.Enabled = true;
				DeleteButton.TintColor = UIColor.Black;
			}

			if (_handler.DisableEditButton) {
				EditButton.Enabled = false;
				EditButton.TintColor = UIColor.LightGray;
			}
			if (_handler.DisableDeleteButton) {
				DeleteButton.Enabled = false;
				DeleteButton.TintColor = UIColor.LightGray;
			}

		}
	}
}

