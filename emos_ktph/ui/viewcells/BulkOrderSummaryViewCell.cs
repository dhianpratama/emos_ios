﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class BulkOrderSummaryViewCell : BaseViewCell<BulkMealRestrictionGroupInfo>
	{
		public const float RowHeight = 60;
		public const string CellIdentifier = "BulkOrderSummaryViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private CrudViewCellHandler<BulkMealRestrictionGroupInfo> _handler;
		private ICrud<BulkMealRestrictionGroupInfo> _callback;
		private BulkMealRestrictionGroupInfo _item;

		public BulkOrderSummaryViewCell (IntPtr handle) : base (handle)
		{
		}
		public static BulkOrderSummaryViewCell Create ()
		{
			return (BulkOrderSummaryViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (BulkMealRestrictionGroupInfo item)
		{
			_item = item;
			DescriptionLabel.Text = String.Format ("Consolidated order by - {0} ({1})", item.orderyby, item.order_time);
			if (item.late_order)
				LateOrderImage.Image = UIImage.FromBundle ("Images/late.png");
			else
				LateOrderImage.Hidden = true;
			if (item.processed_order)
				EditButton.Hidden = true;
			else
				EditButton.Layer.CornerRadius = 10f;
		}
		protected override void SetViewCellHandler(IViewCellHandler<BulkMealRestrictionGroupInfo> handler)
		{
			_handler = (CrudViewCellHandler<BulkMealRestrictionGroupInfo>) handler;
			_callback = (ICrud<BulkMealRestrictionGroupInfo>) _handler.Callback;
		}
		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.Edit(_item);
		}
	}
}

