﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Collections.Generic;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using System.Linq;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class MealOrderViewCell : BaseViewCell<List<DishModelWithTags>>, IImageUpdated
	{
		public const string CellIdentifier = "MealOrderViewCell";
		public static UITableViewCellSeparatorStyle SeparatorStyle = UITableViewCellSeparatorStyle.None;
		private MealOrdersViewCellHandler _handler;
		private IMealOrdersCallback _callback;
		private List<DishModelWithTags> _item;
		public bool IsSelected;
		private ImageDownloader _imageDownloader = new ImageDownloader ();

		public MealOrderViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (List<DishModelWithTags> item)
		{
			_item = item;
			OrderButton.Layer.CornerRadius = 10;
			BackgroundView.Layer.CornerRadius = 10;
			DishDetails.Layer.CornerRadius = 10;
			if (IsSpecialButton ()) {
				ShowSpecialButton ();
				return;
			}
			DownloadImage (_item.First ().picture_url);
			NameTextView.Text = item [0].label;
			Select (item [0].active_data);
			if (item [0].isAdditionalDish) {
				AddNonSuitableSign ();
				if (!_callback.GetBaseContext ().GetBaseUser ().Authorized ("ACCESS_MORE_DISHES")) {
					OrderButton.UserInteractionEnabled = false;
					DishDetails.UserInteractionEnabled = false;
				}
			}
			if (item [0].dishTags.Any (t => t.mark_as_halal))
				AddHalalSign ();
			BackgroundView.SendSubviewToBack (BackgroundImage);
		}
		private void AddNonSuitableSign ()
		{
			var notSuitableImage = new UIImageView (BackgroundView.Frame);
			notSuitableImage.Opaque = false;
			notSuitableImage.Image = UIImage.FromBundle ("Images/div_more_dish_v5.png");
			BackgroundView.AddSubview (notSuitableImage);
			BackgroundView.SendSubviewToBack (notSuitableImage);
		}
		private void AddHalalSign ()
		{
			var halalImage = new UIImageView (BackgroundView.Frame);
			halalImage.Opaque = false;
			halalImage.Image = UIImage.FromBundle ("Images/div_halal_dish_v5.png");
			BackgroundView.AddSubview (halalImage);
			BackgroundView.SendSubviewToBack (halalImage);
		}
		protected override void SetViewCellHandler(IViewCellHandler<List<DishModelWithTags>> handler)
		{
			_handler = (MealOrdersViewCellHandler) handler;
			_callback = (IMealOrdersCallback) _handler.Callback;
		}
		partial void OrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if (!IsSpecialButton())
				OrderDish ();
			else if (_callback.OnlySuitable) 
				_callback.ShowAllAvailableDish();
			else 
				_callback.ShowSuitableDishesOnly();
		}
		private void OrderDish ()
		{
			Select (!IsSelected);
			_callback.OrderDish (_item);
		}
		private void Select (bool isSelected)
		{
			IsSelected = isSelected;
			BackgroundImage.Hidden = !isSelected;
			if (isSelected)
				BackgroundImage.Image = UIImage.FromBundle ("Images/div_blue_checked.png");
			else
				BackgroundImage.Image = null;
		}
		private bool IsSpecialButton ()
		{
			return _item.First ().id == null;
		}
		partial void DishDetails_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.ShowDetail(_item);
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			_imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				OrderButton.SetImage (ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this), UIControlState.Normal);
			});
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
					OrderButton.SetImage (ImageDownloader.LazyLoad (uri.ToString (), this), UIControlState.Normal);
			});
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				OrderButton.SetImage (ImageDownloader.GetNotAvailableImage (), UIControlState.Normal);
			});
		}
		private void ShowSpecialButton ()
		{
			if (_callback.OnlySuitable)
				ShowPlusButton ();
			else
				ShowMinusButton ();
			DishDetails.Hidden = true;
		}
		private void ShowPlusButton ()
		{
			NameTextView.Text = "View all available dishes";
			OrderButton.SetImage (UIImage.FromBundle ("Images/btn_increase.png"), UIControlState.Normal);
		}
		private void ShowMinusButton ()
		{
			NameTextView.Text = "View only suitable dishes";
			OrderButton.SetImage (UIImage.FromBundle ("Images/btn_decrease.png"), UIControlState.Normal);
		}
		private bool IsPlusButton ()
		{
			return _item.First ().id == null;
		}
	}
}

