// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("MealOrderViewCell")]
	partial class MealOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView BackgroundImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DishDetails { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView NameTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton OrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView OrderStatusImage { get; set; }

		[Action ("DishDetails_TouchUpInside:")]
		partial void DishDetails_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("OrderButton_TouchUpInside:")]
		partial void OrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundImage != null) {
				BackgroundImage.Dispose ();
				BackgroundImage = null;
			}

			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (DishDetails != null) {
				DishDetails.Dispose ();
				DishDetails = null;
			}

			if (NameTextView != null) {
				NameTextView.Dispose ();
				NameTextView = null;
			}

			if (OrderButton != null) {
				OrderButton.Dispose ();
				OrderButton = null;
			}

			if (OrderStatusImage != null) {
				OrderStatusImage.Dispose ();
				OrderStatusImage = null;
			}
		}
	}
}
