﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;

namespace core_emos
{
	public partial class VerticalMealOrderViewCell : BaseViewCell<PatientOrderCommand>
	{
		public const string CellIdentifier = "VerticalMealOrderViewCell";
		public const float CellHeight = 55f;
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private IPatientOrderCommandCallback _callback;
		private PatientOrderCommand _item;

		public VerticalMealOrderViewCell (IntPtr handle) : base (handle)
		{
		}

		public static VerticalMealOrderViewCell Create ()
		{
			return (VerticalMealOrderViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (PatientOrderCommand item)
		{
			_item = item;
			BackgroundColor = SharedColors.DarkViewCellBackground;
			if (String.IsNullOrEmpty (item.ImagePath))
				return;
			OrderButton.SetImage (UIImage.FromBundle(item.ImagePath), UIControlState.Normal);
			CompanionOrderStatusLabel.Text = item.CompanionStatus;
		}
		protected override void SetViewCellHandler(IViewCellHandler<PatientOrderCommand> handler)
		{
			_callback = (IPatientOrderCommandCallback)handler.Callback;
		}
		partial void OrderButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			_callback.ItemSelected(_item, 0);
		}
	}
}

