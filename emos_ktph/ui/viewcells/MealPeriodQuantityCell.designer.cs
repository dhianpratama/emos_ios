// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ktph
{
	[Register ("MealPeriodQuantityCell")]
	partial class MealPeriodQuantityCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel QuantityLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField QuantityTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIStepper Stepper { get; set; }

		[Action ("Quantity_EditingChanged:")]
		partial void Quantity_EditingChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("Stepper_ValueChanged:")]
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (QuantityLabel != null) {
				QuantityLabel.Dispose ();
				QuantityLabel = null;
			}

			if (Stepper != null) {
				Stepper.Dispose ();
				Stepper = null;
			}

			if (QuantityTextField != null) {
				QuantityTextField.Dispose ();
				QuantityTextField = null;
			}
		}
	}
}
