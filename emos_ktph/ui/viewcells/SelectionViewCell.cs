﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ktph
{
	public partial class SelectionViewCell : BaseViewCell<SpecialInstructionV2Detail>
	{
		public const string CellIdentifier = "SelectionViewCell";
		public static readonly UINib Nib = UINib.FromName ("SelectionViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("SelectionViewCell");
		public SpecialInstructionV2Detail _item;
		private ISpecialInstructionrEntryCallback _callback;

		public SelectionViewCell (IntPtr handle) : base (handle)
		{
		}

		public static SelectionViewCell Create ()
		{
			return (SelectionViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetViewCellHandler(IViewCellHandler<SpecialInstructionV2Detail> handler)
		{
			_callback = (ISpecialInstructionrEntryCallback) handler.Callback;
		}
		protected override void SetCellContent (SpecialInstructionV2Detail item)
		{
			_item = item;
			this.Layer.BorderColor = UIColor.Black.CGColor;
			this.Layer.BorderWidth = 0.5f;
			this.contentLabel.Text = item.specialInstruction.code;
		}
		public override void HandleRowSelected(SpecialInstructionV2Detail item)
		{
			if (item.specialInstruction.id != _item.specialInstruction.id)
				return;
			_callback.DetailSelected (_item);
		}
		public override void HandleRowDeselected(SpecialInstructionV2Detail item)
		{
			if (item.specialInstruction.id != _item.specialInstruction.id)
				return;
			_callback.DetailDeselected (_item);
		}
	}
}

