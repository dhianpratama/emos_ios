// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ktph
{
	[Register ("SpecialInstructionEntryViewCell")]
	partial class SpecialInstructionEntryViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton closeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView DetailsTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopView { get; set; }

		[Action ("closeButtonTapped:")]
		partial void closeButtonTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DetailsTable != null) {
				DetailsTable.Dispose ();
				DetailsTable = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (TopView != null) {
				TopView.Dispose ();
				TopView = null;
			}

			if (closeButton != null) {
				closeButton.Dispose ();
				closeButton = null;
			}
		}
	}
}
