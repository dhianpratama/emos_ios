// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("BulkOrderSummaryViewCell")]
	partial class BulkOrderSummaryViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel DescriptionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView LateOrderImage { get; set; }

		[Action ("EditButton_TouchUpInside:")]
		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DescriptionLabel != null) {
				DescriptionLabel.Dispose ();
				DescriptionLabel = null;
			}

			if (LateOrderImage != null) {
				LateOrderImage.Dispose ();
				LateOrderImage = null;
			}

			if (EditButton != null) {
				EditButton.Dispose ();
				EditButton = null;
			}
		}
	}
}
