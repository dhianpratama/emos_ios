﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;
using System.Collections.Generic;
using System.Linq;

namespace emos_ktph
{
	public partial class SpecialInstructionEntryViewCell : BaseViewCell<SpecialInstructionV2Detail>, ISpecialInstructionDetailV2Callback
	{
		private const float ChildCellHeight = 44;
		private const float TopViewHeight = 50;
		public const string CellIdentifier = "SpecialInstructionEntryViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private SpecialInstructionV2Detail _item;
		private ISpecialInstructionrEntryCallback _callback;
		private ExtraOrderMealPeriodV2CellSetting _childSetting;
		private ExtraOrderMealPeriodV2CellHandler _childHandler;
		private MultipleSelectionDataSource<ExtraOrderMealPeriodModelSimple, MealPeriodQuantityCell> _childSource;

		public SpecialInstructionEntryViewCell (IntPtr handle) : base (handle)
		{
			InitiateChild ();
		}
		public static SpecialInstructionEntryViewCell Create ()
		{
			return (SpecialInstructionEntryViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (SpecialInstructionV2Detail item)
		{
			this.Layer.BorderColor = UIColor.Black.CGColor;
			this.Layer.BorderWidth = 1.0f;
			TopView.BackgroundColor = UIColor.LightGray;
			this.BackgroundColor = UIColor.Green;
			NameLabel.TextColor = Colors.MainThemeColor;
			_item = item;

			NameLabel.Text = item.specialInstruction.code;
			NameLabel.Font = Fonts.Header;

			ShowDetails ();
			ShowSelection ();
		}
		private void ShowSelection ()
		{
			ShowChildSelections (_item.mealPeriods);
			this.SetFrame (height: CalculateRowHeight (_item));
			DetailsTable.SetFrameBelowTo (TopView, height: CalculateDetailsHeight (_item), distanceToAbove: 0);
		}
		private void ShowDetails ()
		{
			SetChildSource ();
			DetailsTable.AllowsMultipleSelection = !_item.quantifiable;
			DetailsTable.Source = _childSource;
		}
		private void SetChildSource ()
		{
			_childSource.Items = _item.mealPeriods;
		}

		private void InitiateChild ()
		{
			_childSetting = new ExtraOrderMealPeriodV2CellSetting {
				CellHeight = ChildCellHeight,
				MaximumQuantity = Settings.ExtraOrderMaximumQuantity,
				MinimumQuantity = Settings.ExtraOrderMinimumQuantity
			};

			_childHandler = new ExtraOrderMealPeriodV2CellHandler (MealPeriodQuantityCell.CellIdentifier, this, _childSetting) {
				GetHeightForRow = GetDetailHeightForRow
			};
			_childSource = new MultipleSelectionDataSource<ExtraOrderMealPeriodModelSimple, MealPeriodQuantityCell> (_childHandler);
			_childSource.OnItemDeselected += OnDetailDeselected;
			_childSource.OnItemSelected += OnDetailSelected;
		}
		private void OnDetailSelected (object sender, RowSelectionEventArgs<ExtraOrderMealPeriodModelSimple> e)
		{
			_callback.DetailSelected (e.Item);
		}
		private void OnDetailDeselected (object sender, RowSelectionEventArgs<ExtraOrderMealPeriodModelSimple> e)
		{
			_callback.DetailDeselected (e.Item);
		}
		private void ShowChildSelections (List<ExtraOrderMealPeriodModelSimple> details)
		{
			if (details.FirstOrDefault ().quantifiable)
				return;

			var selectedRows = new List<int> ();
			var deselectedRows = new List<int> ();
			for (int i = 0; i < details.Count; i++) {
				if (details [i].selected) {
					selectedRows.Add (i);
				} else {
					deselectedRows.Add (i);
				}
			}
			if (selectedRows.Count > 0)
				_childSource.SelectRows (selectedRows, DetailsTable);
			if (deselectedRows.Count > 0)
				_childSource.DeselectRows (deselectedRows, DetailsTable);
		}
		protected override void SetViewCellHandler(IViewCellHandler<SpecialInstructionV2Detail> handler)
		{
			_callback = (ISpecialInstructionrEntryCallback) handler.Callback;
		}

		public static float CalculateRowHeight(SpecialInstructionV2Detail item)
		{
			return CalculateDetailsHeight (item) + TopViewHeight;
		}
		public static float CalculateDetailsHeight (SpecialInstructionV2Detail item)
		{
			if (item.mealPeriods.Count() == 0)
				return 0;
			return item.mealPeriods.Count() * ChildCellHeight;
		}
		private float GetDetailHeightForRow (ExtraOrderMealPeriodModelSimple item)
		{
			return ChildCellHeight;
		}
		public override void HandleRowSelected(SpecialInstructionV2Detail item)
		{
		}
		public override void HandleRowDeselected(SpecialInstructionV2Detail item)
		{
		}
		public void DetailQuantityChanged (ExtraOrderMealPeriodModelSimple item)
		{
			var detail = _item.mealPeriods.First (d => d.id == item.id);
			detail.selected = item.selected;
			detail.quantities = item.quantities;
			_callback.ValueChanged (_item);
		}
		partial void closeButtonTapped (MonoTouch.Foundation.NSObject sender){
			_callback.SelectionDeleted(_item);
		}
		private void DeselectDetails (long? id)
		{
		}
		public void DetailSelectionChanged(ExtraOrderMealPeriodModelSimple item)
		{
		}

		private long ConvertToLong (string value)
		{
			long quantity;
			if (long.TryParse (value, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out quantity))
				return quantity;
			else
				return 0;
		}
		public void ItemSelected (ExtraOrderMealPeriodModelSimple selected, int selectedIndex)
		{
		}
		public bool GetAllowChangeQuantity ()
		{
			return true;
		}
		public ios.IBaseContext GetBaseContext ()
		{
			return _callback.GetBaseContext ();
		}
	}
}

