﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;

namespace emos_ktph
{
	public partial class MealPeriodQuantityCell : BaseViewCell<ExtraOrderMealPeriodModelSimple>
	{
		public const string CellIdentifier = "MealPeriodQuantityCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private ExtraOrderMealPeriodModelSimple _item;
		private ExtraOrderMealPeriodV2CellHandler _handler;
		private ISpecialInstructionDetailV2Callback _callback;

		public MealPeriodQuantityCell (IntPtr handle) : base (handle)
		{
		}
		public static MealPeriodQuantityCell Create ()
		{
			return (MealPeriodQuantityCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (ExtraOrderMealPeriodModelSimple item)
		{
			_item = item;
			NameLabel.Text = item.label;
			QuantityLabel.Text = "Quantity";
			QuantityTextField.Text = item.quantities.ToString();

			QuantityLabel.Hidden = !item.quantifiable;
			QuantityTextField.Hidden = !item.quantifiable;
			QuantityTextField.Enabled = false;
			Stepper.Hidden = !item.quantifiable;
			Stepper.MaximumValue = _handler.setting.MaximumQuantity;
			Stepper.MinimumValue = _handler.setting.MinimumQuantity;

			Stepper.Value = item.quantities;
		}
		public override void HandleRowSelected(ExtraOrderMealPeriodModelSimple item)
		{
			if (item.id != _item.id)
				return;
			_item.selected = true;
			_item.quantities = item.quantities;
			_callback.DetailQuantityChanged(_item);
		}
		public override void HandleRowDeselected(ExtraOrderMealPeriodModelSimple item)
		{
			if (item.id != _item.id)
				return;
			_item.selected = false;
			_item.quantities = 0;
			QuantityTextField.Text = _item.quantities.ToString ();
			Stepper.Value = 0;
			_callback.DetailQuantityChanged(_item);
		}
		partial void Quantity_EditingChanged (MonoTouch.Foundation.NSObject sender)
		{
			var value = ((UITextField) sender).Text;
			if (string.IsNullOrEmpty(value))
				return;
			long quantity = 0;
			long.TryParse(value, System.Globalization.NumberStyles.Any,System.Globalization.NumberFormatInfo.InvariantInfo, out quantity );
			if (quantity == 0 || (quantity == 1 && _item.quantities == 0))
				_callback.DetailSelectionChanged(_item);
			_item.quantities = (int)quantity;
			_callback.DetailQuantityChanged(_item);
		}
		protected override void SetViewCellHandler(IViewCellHandler<ExtraOrderMealPeriodModelSimple> handler)
		{
			_handler = (ExtraOrderMealPeriodV2CellHandler) handler;
			_callback = (ISpecialInstructionDetailV2Callback) _handler.Callback;
		}
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			QuantityTextField.Text = Stepper.Value.ToString();
			if (Stepper.Value == 0) {
				_item.selected = false;
				_callback.DetailSelectionChanged(_item);
			} else if ((Stepper.Value == 1 && _item.quantities == 0)) {
				_item.selected = true;
				_callback.DetailSelectionChanged(_item);
			}
			_item.quantities = (int) Stepper.Value;
			_callback.DetailQuantityChanged(_item);
		}
	}
}

