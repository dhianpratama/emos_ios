﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Drawing;
using System.Linq;
using MonoTouch.ObjCRuntime;
using core_emos;

namespace emos_ios
{
	[Register("MealOrderHeaderView")]
	public partial class MealOrderHeaderView : UIView
	{
		private IApplicationContext _appContext;
		private const float CategoryTabHeight = 50f;
		private const float StepHeight = 100f;
		private const float StepWidth = 100f;
		private List<MealCategoryTabButtonView> _categoryTabButtons;
		public List<MealOrderStepView> StepButtons { get; set; }
		private MealOrderModel _mealOrderModel;
		public event EventHandler<MealOrderHeaderEventArgs> OnStepSelected;
		private int _currentTag;
		private int _currentStep;
		public DishTypeModelSimple CurrentDishType;
		public TagModelSimple CurrentMealTag;
		public int CurrentStep { get { return _currentStep; } private set { } }
		public IMealOrderCallback Callback { get; set; }
		public bool IsPatient { get; set; }
		public bool IsCompanion { get; set; }

		public MealOrderHeaderView(IntPtr h): base(h)
		{
		}			
		public MealOrderHeaderView (IApplicationContext appContext)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib ("MealOrderHeaderView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			AddSubview (v);
		}
		public void Load (MealOrderModel mealOrderModel, bool viewOnly, int selectedTag, bool patientInfoOnly = false)
		{
			_mealOrderModel = mealOrderModel;
			_currentTag = selectedTag;
			mealCategoryTabView.Frame = new RectangleF (0, Measurements.MealOrderHeaderHeight - CategoryTabHeight, 768, CategoryTabHeight);
			SetTexts (mealOrderModel);
			SetTextsByLanguage ();
			HideOrderInfoView ();
			if (patientInfoOnly)
				return;
			CategoryImage.Image = ImageLibrary.GetMealPeriodImage (IsCompanion, _appContext.SelectedMealOrderPeriod.label);
			if (viewOnly) {
				ShowOrderInfoView ();
				return;
			}
			if (_appContext.SelectedOperationCode != 5)
				GenerateCategoryTabButtons ();
		}

		public void SkipStepButtons(MealOrderModel oneDishMealOrderModel, int step, bool nextStep){
			ViewHandler.ClearSubviews (StepsView);
			var previousSteps = StepButtons;
			_mealOrderModel = oneDishMealOrderModel;
			StepButtons = new List<MealOrderStepView> ();
			int allTypesCount = _mealOrderModel.tagsMealOrder [_currentTag].types.ToList().Count ();

			for (int i = 0; i < allTypesCount; i++) {
				var buttonTitle = (i + 1).ToString ();
				var skip = _mealOrderModel.tagsMealOrder [_currentTag].types [i].skip;
				var mealStepView = CreateStep (buttonTitle, i, skip, previousSteps);
				StepButtons.Add (mealStepView);
			}
			var summaryStep = CreateStep ((allTypesCount + 1).ToString (), allTypesCount, false);
			StepButtons.Add (summaryStep);
			SelectStep (nextStep? step + 1 : step, true);
		}
		private void SetTexts (MealOrderModel mealOrderModel)
		{
			IdLabel.Text = _appContext.SelectedRegistration.profile_identifier;
			NameLabel.Text = _appContext.SelectedRegistration.profile_name;
			WardLabel.Text = _appContext.SelectedLocation.code;
		}
		private void GenerateCategoryTabButtons ()
		{
			var mealCategories = _mealOrderModel.tagsMealOrder.Select (e => e.tag.label).ToList ();
			var tabWidth = (float) Math.Ceiling(768f / mealCategories.Count());
			_categoryTabButtons = new List<MealCategoryTabButtonView> ();
			for (int i = 0; i < mealCategories.Count (); i++) {
				var x = tabWidth * i;
				var mealCategoryTabButtonView = new MealCategoryTabButtonView (_appContext, GenerateTabTitle (i), i, UITextAlignment.Center);
				mealCategoryTabButtonView.Frame = new RectangleF (x, 0, tabWidth, CategoryTabHeight);
				mealCategoryTabButtonView.SetLayout (x, tabWidth, CategoryTabHeight);
				mealCategoryTabButtonView.OnCategoryClicked += OnCategoryClicked;
				mealCategoryTabView.AddSubview (mealCategoryTabButtonView);
				_categoryTabButtons.Add (mealCategoryTabButtonView);
			}
		}
		private string GenerateTabTitle (int i)
		{
			var availableDishes = new List<DishModelWithTags> ();
			foreach (var dishType in _mealOrderModel.tagsMealOrder [i].types.Where(t => !t.skip)) {
				availableDishes.AddRange (_mealOrderModel.dishes.Where (d => d.dish_type_id == dishType.id && d.dishTags.Any (dt => dt.id == _mealOrderModel.tagsMealOrder [i].id)).ToList ());
			}
			var suitableDishes = availableDishes.Where (e => !e.isAdditionalDish).ToList ();
			var availabilityText = GetAvailabilityText (availableDishes, suitableDishes);
			return String.Format ("{0}\n{1}", _mealOrderModel.tagsMealOrder [i].label, availabilityText);
		}
		private string GetAvailabilityText (List<DishModelWithTags> availableDishes, List<DishModelWithTags> suitableDishes)
		{
			var availableTitleText = _appContext.LanguageHandler.GetLocalizedString ("available");
			var suitableTitleText = _appContext.LanguageHandler.GetLocalizedString ("suitable");
			return String.Format ("({0} {1}, {2} {3})", availableDishes.Count (), availableTitleText, suitableDishes.Count (), suitableTitleText);
		}
		private void OnCategoryClicked (object sender, EventArgs e)
		{
			var button = (MealCategoryTabButtonView)sender;
			SelectCategory (button.CategoryIndex);
		}
		public void SelectCategory (int selected, bool filterDish = true)
		{
			if (_categoryTabButtons.Count == 0)
				return;
			if (selected == _currentTag && _currentTag > 0)
				return;
			for (int i = 0; i < _categoryTabButtons.Count (); i++) {
				if (i == selected)
					_categoryTabButtons [i].Pressed = true;
				else
					_categoryTabButtons [i].Pressed = false;
			}
			_currentTag = selected;
			CurrentMealTag = _mealOrderModel.tagsMealOrder [selected].tag;
			GenerateStepButtons ();
			if (filterDish)
				Callback.FilterDish (_currentStep, _currentTag);
		}
		public void SelectStep (int selected, bool filterDish = true)
		{
			if (StepButtons.Count == 0)
				return;
			for (int i = 0; i < StepButtons.Count (); i++) {
				StepButtons [i].Pressed = i == selected;
			}
			_currentStep = selected;
			List<DishTypeModelSimple> dishTypes = new List<DishTypeModelSimple> ();
			dishTypes = _mealOrderModel.tagsMealOrder [_currentTag].types.ToList();

			if (selected != dishTypes.Count())
				CurrentDishType = dishTypes[selected];
			else
				CurrentDishType = null;
			for (int i = 0; i < StepButtons.Count (); i++) {
				StepButtons [i].Pressed = i == selected;
			}
			if (selected > 0) {
				for (int i = 0; i < _categoryTabButtons.Count (); i++) {
					if (i != _currentTag)
						_categoryTabButtons [i].Deactive ();
				}
			} else {
				if (_categoryTabButtons != null) {
					_categoryTabButtons.ForEach (c => {
						c.Hidden = false;
					});
				}
			}
			if (filterDish)
				Callback.FilterDish (_currentStep, _currentTag);
		}
		private void GenerateStepButtons ()
		{
			ViewHandler.ClearSubviews (StepsView);
			var previousSteps = StepButtons;
			StepButtons = new List<MealOrderStepView> ();
			for (int i = 0; i < _mealOrderModel.tagsMealOrder [_currentTag].types.Count (); i++) {
				var dishType = _mealOrderModel.tagsMealOrder [_currentTag].types [i];
				var buttonTitle = (i + 1).ToString ();
				var skip = dishType.skip;
				var mealStepView = CreateStep (buttonTitle, i, skip, dishType: dishType);
				StepButtons.Add (mealStepView);
			}
			var summaryStep = CreateStep ((_mealOrderModel.tagsMealOrder [_currentTag].types.Count + 1).ToString (), _mealOrderModel.tagsMealOrder [_currentTag].types.Count, !IsSummaryEnable());
			StepButtons.Add (summaryStep);
			SelectStep (0, false);
		}

		private MealOrderStepView CreateStep(string title, int step, bool disabled, List<MealOrderStepView> previousSteps = null, DishTypeModelSimple dishType=null)
		{
			var mealStepView = new MealOrderStepView (Callback.GetBaseContext (), title, step, StepWidth, StepHeight);
			mealStepView.SetButtonTitle (title);
			if (previousSteps != null && previousSteps.Any ()) {
//				mealStepView.IsDisabled = previousSteps [step].IsDisabled;
//				mealStepView.IsClickable = previousSteps [step].IsClickable;
				mealStepView.Pressed = previousSteps [step].Pressed;
				mealStepView.IsOrdered = previousSteps [step].IsOrdered;
			} else {
				mealStepView.IsDisabled = disabled;
				mealStepView.IsClickable = !disabled;

				if (dishType != null) {
					if (DishTypeAlreadySelectedOnFirstLoad (dishType.id)) {
						mealStepView.IsClickable = true;
						mealStepView.Pressed = true;
						mealStepView.IsOrdered = true;
					}
				}
			}
			mealStepView.IsDisabled = disabled;
			mealStepView.IsClickable = !disabled;
			mealStepView.OnStepClicked += OnStepClicked;
			StepsView.AddSubview (mealStepView);
			mealStepView.Frame = new RectangleF (StepWidth * step, 0, StepWidth, StepHeight);
			return mealStepView;
		}

		private void OnStepClicked (object sender, EventArgs e)
		{
			var step = (MealOrderStepView)sender;
			if (step.Step != _currentStep)
				OnStepSelected (this, new MealOrderHeaderEventArgs (step.Step));
		}

		public void DeactiveOtherTags()
		{
			//_categoryTabButtons[1].
		}

		public void HideOrderInfoView()
		{
			StepsView.Hidden = false;
			mealCategoryTabView.Hidden = false;
		}
		public void ShowOrderInfoView()
		{
			StepsView.Hidden = true;
			mealCategoryTabView.Hidden = true;
		}
		public void UpdateMealOrderModel()
		{

		}
		private void SetTextsByLanguage()
		{
			NameTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Name");
			IDTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("ID");
			WardTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("WardBed");
		}
		public void Order (int _step)
		{
			StepButtons [_step].IsOrdered = true;
		}
		public void CancelOrder (int _step)
		{
			StepButtons [_step].IsOrdered = false;
		}
		public void SetTitle (List<DishModelWithTags> availableDishes, List<DishModelWithTags> suitableDishes)
		{
			var availabilityText = GetAvailabilityText (availableDishes, suitableDishes);
			_categoryTabButtons [_currentTag].SetTitle (String.Format ("{0}\n{1}", _mealOrderModel.tagsMealOrder [_currentTag].label, availabilityText));
		}
		private bool DishTypeAlreadySelectedOnFirstLoad(long? dishTypeId)
		{
			return _mealOrderModel.orderedDishes != null
			&& _mealOrderModel.orderedDishes.Any (e => e.dish_type_id == dishTypeId);
		}
		private bool IsSummaryEnable()
		{
			var types = _mealOrderModel.tagsMealOrder [_currentTag].types
				.Where(e=> !e.skip)
				.ToList();
			var ordered = _mealOrderModel.orderedDishes
				.Where (e => !String.IsNullOrEmpty (e.dish_type_label))
				.ToList ();
			return ordered.Count >= types.Count;
		}
	}
}

