// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealPeriodHeaderView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel MealPeriodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TimeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MealPeriodLabel != null) {
				MealPeriodLabel.Dispose ();
				MealPeriodLabel = null;
			}

			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
		}
	}
}
