﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace emos_ios
{
	[Register("MealPeriodHeaderView")]
	public partial class MealPeriodHeaderView : UIView
	{
		private IApplicationContext _appContext;
		public string MealPeriod { 
			get { return MealPeriodLabel == null ? "" : MealPeriodLabel.Text; }
			set { 
				if (MealPeriodLabel != null)
					MealPeriodLabel.Text = value;
			}
		}
		public string HeaderTime { 
			get { return TimeLabel == null ? "" : TimeLabel.Text; }
			set { 
				if (TimeLabel != null)
					TimeLabel.Text = value;
			}
		}

		public MealPeriodHeaderView (IApplicationContext appContext)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib ("MealPeriodHeaderView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			AddSubview (v);
		}
	}
}