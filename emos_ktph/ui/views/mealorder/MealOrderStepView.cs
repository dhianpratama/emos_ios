﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using ios;

namespace emos_ios
{
	[Register("MealOrderStepView")]
	public partial class MealOrderStepView : UIView
	{
		public int Step { get; private set; }
		private bool _pressed;
		public bool Pressed {
			get { return _pressed; }
			set {
				_pressed = value;
				if (value)
					Activate ();
				ChangeState ();
			}
		}
		private bool _isOrdered;
		public bool IsOrdered {
			get { return _isOrdered; }
			set {
				_isOrdered = value;
				ChangeState ();
			}
		}
		private bool _isDisabled;
		public bool IsDisabled {
			get { return _isDisabled; }
			set {
				_isDisabled = value;
				if (value)
					Disable ();
				else
					Enable ();
			}
		}
		private bool _isClickable;
		public bool IsClickable {
			get { return _isClickable; }
			set {
				_isDisabled = !value;
				_isClickable = value;
				ChangeState ();
			}
		}

		public EventHandler OnStepClicked;
		public MealOrderStepView(IntPtr h): base(h)
		{
		}
		public MealOrderStepView (IBaseContext appContext, string buttonTitle, int step, float width, float height)
		{
			var arr = NSBundle.MainBundle.LoadNib("MealOrderStepView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
			MealOrderButton.Layer.CornerRadius = 10;
			v.SetFrame (width: width, height: height);
			MealOrderButton.SetFrame (width: width, height: height);
			Step = step;
		}
		public void SetButtonTitle(string buttonTitle)
		{
			MealOrderButton.SetTitle (buttonTitle, UIControlState.Normal);
		}
		partial void ChangeState (MonoTouch.Foundation.NSObject sender)
		{
			if (_isDisabled)
				return;
			var handler = OnStepClicked;
			if (handler != null)
				handler(this, new EventArgs());
		}
		private	void Activate ()
		{
			_isClickable = true;
			_isDisabled = false;
		}
		public void ChangeState ()
		{
			if (_pressed) {
				var imageFile = String.Format ("Images/step{0}_icon_active.png", Step + 1);
				MealOrderButton.SetImage (UIImage.FromBundle (imageFile), UIControlState.Normal);
			} else if (_isClickable) {
				var imageFile = String.Format ("Images/step{0}_icon.png", Step + 1);
				MealOrderButton.SetImage (UIImage.FromBundle (imageFile), UIControlState.Normal);
			} else {
				var imageFile = String.Format ("Images/step{0}_icon_disabled.png", Step + 1);
				MealOrderButton.SetImage (UIImage.FromBundle (imageFile), UIControlState.Normal);
			}
		}
		private void Disable ()
		{
			_pressed = false;
			_isClickable = false;
			ChangeState ();
		}
		private void Enable ()
		{
			_isClickable = true;
			ChangeState ();
		}
	}
}

