// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealOrderHeaderView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView CategoryImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel IdLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel IDTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView mealCategoryTabView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView StepsView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WardLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WardTitleLabel { get; set; }

		[Action ("Save:")]
		partial void Save (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CategoryImage != null) {
				CategoryImage.Dispose ();
				CategoryImage = null;
			}

			if (IdLabel != null) {
				IdLabel.Dispose ();
				IdLabel = null;
			}

			if (IDTitleLabel != null) {
				IDTitleLabel.Dispose ();
				IDTitleLabel = null;
			}

			if (mealCategoryTabView != null) {
				mealCategoryTabView.Dispose ();
				mealCategoryTabView = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (NameTitleLabel != null) {
				NameTitleLabel.Dispose ();
				NameTitleLabel = null;
			}

			if (StepsView != null) {
				StepsView.Dispose ();
				StepsView = null;
			}

			if (WardLabel != null) {
				WardLabel.Dispose ();
				WardLabel = null;
			}

			if (WardTitleLabel != null) {
				WardTitleLabel.Dispose ();
				WardTitleLabel = null;
			}
		}
	}
}
