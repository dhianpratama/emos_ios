﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using System.Collections.Generic;

namespace emos_ios
{
	[Register("PageTitleAndPatientSummaryView")]
	public partial class PageTitleAndPatientSummaryView : UIView
	{
		private IApplicationContext _appContext;

		public PageTitleAndPatientSummaryView (IntPtr h) : base(h)
		{
		}
		public PageTitleAndPatientSummaryView (IApplicationContext appContext, LocationModelSimple location, RegistrationModelSimple registration)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib ("PageTitleAndPatientSummaryView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			v.BackgroundColor = UIColor.Clear;
			AddSubview (v);

			ShowWardBed (appContext.Ward, location);
			if (registration == null) {
				IdLabel.Text = "";
				NameLabel.Text = "";
				return;
			}
			IdLabel.Text = registration.profile_identifier;
			NameLabel.Text = registration.profile_name;
			DateAndMealView.BackgroundColor = _appContext.ColorHandler.ViewCellBackground;
			DateAndMealView.Hidden = true;
			ContainerView.Layer.CornerRadius = 5.0f;

			NameTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Name");
			IDTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("ID");
			WardTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("WardBed");
			MealTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Meal");
			DateTitleLabel.Text = _appContext.LanguageHandler.GetLocalizedString ("Date");
		}
		private void ShowWardBed (KeyValuePair<string, string> Ward, LocationModelSimple location)
		{
			WardBedLabel.Text = location.code ?? "";
			/*
			var bed = location.label ?? "";
			var ward = Ward.Value ?? "";
			WardBedLabel.Text = String.Format ("{0}/{1}", ward, bed);
			*/
		}
		public void SetHeaderImage(UIImage image)
		{
			HeaderImage.Image = image;
		}
	}
}

