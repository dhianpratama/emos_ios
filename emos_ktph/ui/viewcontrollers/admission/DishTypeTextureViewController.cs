﻿using System.Linq;
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class DishTypeTextureViewController : BaseViewController, IDishTypeTextureCallback
	{
		private const float MaxHeight = 644f;
		public event EventHandler<BaseEventArgs<List<CustomDietTexture>>> OnDone;
		public event EventHandler OnCancel;
		private const float ElementLeft = 30f;
		private const float ElementHeight = 40f;
		private UIBarButtonItem _doneButton;
		private UIBarButtonItem _backButton;
		private List<CustomDietTexture> _customDietTextures = new List<CustomDietTexture>();
		private List<RestrictionModelSimple> _dietTextures = new List<RestrictionModelSimple>();
		private List<BaseIpadModel> _dishTypes = new List<BaseIpadModel>();
		private List<DishTypeTexture> _dishTypeTextures = new List<DishTypeTexture>();
		private DishTypeTextureViewCellHandler _viewCellHandler;
		private DynamicTableSource<DishTypeTexture, DishTypeTextureViewCell> _tableSource;

		public DishTypeTextureViewController (IApplicationContext appContext, List<CustomDietTexture> customDietTextures, List<RestrictionModelSimple> dietTextures, List<BaseIpadModel> dishTypes) : base (appContext, "DishTypeTextureViewController", null)
		{
			_customDietTextures = customDietTextures;
			_dietTextures = dietTextures;
			_dishTypes = dishTypes;
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			SetBackground ();
			SetNavigationBar ();
			Load ();
		}
		private void SetBackground ()
		{
			var left = 134f;
			var width = UIScreen.MainScreen.Bounds.Right - (left * 2);
			var contentHeight = NavigationBar.Frame.Height + (_dishTypes.Count * DishTypeTextureViewCell.CellHeight);
			var height = contentHeight > MaxHeight ? MaxHeight : contentHeight;
			var top = (UIScreen.MainScreen.Bounds.Bottom / 2) - (height / 2);
			BackgroundView.SetFrame (left, top, width, height);
			BackgroundView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
			BackgroundView.Layer.BorderWidth = 1.0f;
			BackgroundView.BackgroundColor = UIColor.White;
		}
		private void InitiateTable ()
		{
			_viewCellHandler = new DishTypeTextureViewCellHandler (DishTypeTextureViewCell.CellIdentifier, this, new BaseViewCellSetting ());
			_tableSource = new DynamicTableSource<DishTypeTexture, DishTypeTextureViewCell> (_viewCellHandler);
			_tableSource.Items = _dishTypeTextures;
			TableView.SetFrameBelowTo (NavigationBar, 0, BackgroundView.Frame.Width, BackgroundView.Frame.Height - NavigationBar.Frame.Height, 0f);
			TableView.Source = _tableSource;
			TableView.RowHeight = DishTypeTextureViewCell.CellHeight;
		}
		private void Load ()
		{
			foreach (var dishType in _dishTypes) {
				var dishTypeLabel = _dishTypes.First (d => d.id == dishType.id).label;
				var texture = _customDietTextures.FirstOrDefault (c => c.dish_type_id == dishType.id);
				var textureId = texture == null ? 0 : texture.restriction_id;
				var dishTypeTexture = new DishTypeTexture {
					DishType = new KeyValuePair<long, string> (dishType.id.Value, dishTypeLabel),
					TextureId = textureId ?? 0
				};
				_dishTypeTextures.Add (dishTypeTexture);
			}
			InitiateTable ();
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			AnimateMe (this, BackgroundView);
		}
		private void SetNavigationBar()
		{
			NavigationBar.TopItem.Title = "Advanced Options";
			NavigationBar.TopItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			NavigationBar.TopItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
			NavigationBar.SetFrame (width: BackgroundView.Frame.Width);
			AppContext.ColorHandler.PaintNavigationBar (NavigationBar);
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var doneText = AppContext.LanguageHandler.GetLocalizedString ("Done");
			_doneButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Done, (sender, args) => {
				DoneButton_Clicked ();
			});
			var buttons = new List<UIBarButtonItem> {
				_doneButton
			};
			return buttons;
		}
		private void DoneButton_Clicked ()
		{
			OnDone.SafeInvoke (this, new BaseEventArgs<List<CustomDietTexture>> ().Initiate (_customDietTextures));
		}
		private bool Validate ()
		{
			return true;
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			var backText = AppContext.LanguageHandler.GetLocalizedString ("Back");
			_backButton = new UIBarButtonItem (backText, UIBarButtonItemStyle.Done, (sender, args) => {
				OnCancel.SafeInvoke (this);
			});
			var buttons = new List<UIBarButtonItem> {
				_backButton
			};
			return buttons;
		}
		private static void AnimateMe(UIViewController viewController, UIView backgroundView)
		{
			var left = (UIScreen.MainScreen.Bounds.Right - backgroundView.Frame.Width) / 2;
			backgroundView.Frame = new RectangleF (left, UIScreen.MainScreen.Bounds.Bottom, backgroundView.Frame.Width, backgroundView.Frame.Height);
			UIView.BeginAnimations ("slideAnimation");
			UIView.SetAnimationDuration (0.5);
			UIView.SetAnimationDelegate (viewController);
			backgroundView.Center = new PointF (UIScreen.MainScreen.Bounds.Right / 2, UIScreen.MainScreen.Bounds.Bottom / 2);
			UIView.CommitAnimations ();
		}
		public void ValueChanged(DishTypeTexture model)
		{
			var customDietTexture = _customDietTextures.First (c => c.dish_type_id == model.DishType.Key);
			customDietTexture.restriction_id = model.TextureId;
		}
		public List<KeyValuePair<string, string>> GetTextures()
		{
			var dietTextures = new List<KeyValuePair<string, string>> ();
			if (_dietTextures != null)
				dietTextures = _dietTextures.Select (d => new KeyValuePair<string, string> (d.id.ToString (), d.label)).ToList ();
			return dietTextures;
		}
		public void ItemSelected(DishTypeTexture selected, int index)
		{
		}
		public void HandleOnTextureSelected (DishTypeTexture selected)
		{
			var dietTexture = _customDietTextures.FirstOrDefault (d => d.dish_type_id == selected.DishType.Key);
			if (dietTexture != null)
				dietTexture.restriction_id = selected.TextureId;
			else {
				dietTexture = new CustomDietTexture { dish_type_id = selected.DishType.Key, restriction_id = selected.TextureId };
				_customDietTextures.Add (dietTexture);
			}
		}
	}
}

