// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("AddSpecialConditionViewController")]
	partial class AddSpecialConditionViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationBar NavigationBar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SpecialConditionButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (NavigationBar != null) {
				NavigationBar.Dispose ();
				NavigationBar = null;
			}

			if (SpecialConditionButton != null) {
				SpecialConditionButton.Dispose ();
				SpecialConditionButton = null;
			}
		}
	}
}
