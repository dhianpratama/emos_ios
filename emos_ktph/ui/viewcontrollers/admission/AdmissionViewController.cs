﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Dialog;
using ios;

namespace emos_ios
{
	public partial class AdmissionViewController : BaseViewController, ICrud<SpecialCondition>
	{
		public event EventHandler OnSave;
		private const float BorderSize = 1f;
		private const float HorizontalMargin = 10f;
		private const float ElementLeft = 300f;
		private const float RowHeight = 50f;
		private float ElementWidth;
		private const float CaptionWidth = 300;
		private const float HeaderLeft = 10;
		private UIColor DefaultBackgroundColor = UIColor.FromRGB(240, 239, 247);
		private RegistrationModelWithMealPeriodsAndOperationCode _registration;
		private LocationWithRegistrationSimple _locationWithRegistration;
		private AdmissionViewModel _model;
		private bool _isPrompting = false;
		private float _totalPageHeight;
		private RoundedDropDown _idTypeDropDown;
		private RoundedEntryElement _idNumber;
		private RoundedEntryElement _idNumberSample;
		private RoundedEntryElement _patientNameElement;
		private RoundedDropDown _genderDropDown;
		private RoundedDropDown _raceDropDown;
		private RoundedEntryElement _caseNumberElement;
		private RoundedDatePickerButton _admissionTimePickerButton;
		private RoundedDropDown _treatmentCategoryDropDown;
		private RoundedDropDown _bedDropDown;
		private RoundedDropDown _fluidThicknessDropDown;
		private RoundedDatePickerButton _dateOfBirthPickerButton;
		private RoundedMultiLineEntryElement _remarkElement;
		private UIButton _addSpecialConditionButton;
		private UIButton _addDateSpecificRestrictionButton;
		private UIButton _addDishTypeTextureButton;
		private OptionsModel<RestrictionModelSimple> _allergyOptionsModel;
		private OptionsModel<RestrictionModelSimple> _therapeuticOptionsModel;
		private OptionsModel<RestrictionModelSimple> _dietTextureOptionsModel;
		private OptionsModel<RestrictionModelSimple> _mealTypeOptionsModel;
		private AddSpecialConditionViewController _addSpecialConditionViewController;
		private AddSpecialConditionViewController _addDateSpecificRestrictionViewController;
		private DishTypeTextureViewController _addDishTypeTextureViewController;
		private UILabel _additionalInformationLabel;
		private UILabel _dateSpecificRestrictionLabel;
		private UILabel _decoyLabel;
		private UIBarButtonItem _saveButton;
		private List<SpecialCondition> _selectedSpecialConditions = new List<SpecialCondition>();
		private List<SpecialCondition> _selectedDateSpecificRestrictions = new List<SpecialCondition>();
		private List<CustomDietTexture> _customDietTextures = new List<CustomDietTexture>();
		private UITableView _specialConditionTable;
		private CrudViewCellHandler<SpecialCondition> _specialConditionViewCellHandler;
		private DynamicTableSource<SpecialCondition, SpecialConditionViewCell> _specialConditionSource;
		private UITableView _dateSpecificRestrictionTable;
		private CrudViewCellHandler<SpecialCondition> _dateSpecificRestrictionViewCellHandler;
		private DynamicTableSource<SpecialCondition, SpecialConditionViewCell> _dateSpecificRestrictionSource;
		private bool _isView;
		private long? _malayId;
		private long? _halalId;
		private bool _isLoading;

		// Keyboard Handler
		private UIView activeview;             // Controller that activated the keyboard
		private float scroll_amount = 0.0f;    // amount to scroll 
		private bool moveViewUp = false;       // which direction are we moving

		public AdmissionViewController (IApplicationContext AppContext, LocationWithRegistrationSimple locationWithRegistration, bool isView = false) : base (AppContext, "AdmissionViewController", null)
		{
			try {
				_isView = isView;
				_locationWithRegistration = locationWithRegistration;
				if (locationWithRegistration.registrations.Any ())
					_registration = locationWithRegistration.registrations.First ();
				InitiateSpecialConditionTable ();
				InitiateDateSpecificRestrictionTable ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				ElementWidth = UIScreen.MainScreen.Bounds.Right - (HorizontalMargin * 2);
				ScrollView.BackgroundColor = DefaultBackgroundColor;
				BackgroundView.BackgroundColor = DefaultBackgroundColor;
				SetNavigationBar ();

				// Keyboard popup
				NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyBoardUpNotification);
				// Keyboard Down
				NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyBoardDownNotification);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewWillAppear (bool animated)
		{
			try {
				base.ViewWillAppear (animated);
				BackgroundView.SetFrame (0, 0, UIScreen.MainScreen.Bounds.Right, Measurements.PageHeight + Measurements.FrameTop);
				BackgroundView.UserInteractionEnabled = !_isView;
				Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void KeyBoardUpNotification(NSNotification notification)
		{
			RectangleF r = UIKeyboard.FrameEndFromNotification(notification);
			activeview = _remarkElement;
			if (activeview != null) {
				scroll_amount = r.Height;
				if (scroll_amount > 0) {
					moveViewUp = true;
					ScrollTheView (moveViewUp);
				} else {
					moveViewUp = false;
				}
			}
		}
		private void KeyBoardDownNotification(NSNotification notification)
		{
			if (moveViewUp)
				ScrollTheView(false);
		}
		private void ScrollTheView(bool move)
		{
			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration (0.1);

			RectangleF frame = View.Frame;

			if (move) {
				frame.Y -= scroll_amount;
			} else {
				frame.Y += scroll_amount;
				scroll_amount = 0;
			}

			View.Frame = frame;
			UIView.CommitAnimations();
		}
		private void InitiateSpecialConditionTable ()
		{
			try {
				_specialConditionViewCellHandler = new CrudViewCellHandler<SpecialCondition> (SpecialConditionViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_specialConditionSource = new DynamicTableSource<SpecialCondition, SpecialConditionViewCell> (_specialConditionViewCellHandler);
				_specialConditionTable = new UITableView ();
				_specialConditionTable.Source = _specialConditionSource;
				_specialConditionTable.RowHeight = SpecialConditionViewCell.CellHeight;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void InitiateDateSpecificRestrictionTable ()
		{
			try {
				_dateSpecificRestrictionViewCellHandler = new CrudViewCellHandler<SpecialCondition> (SpecialConditionViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_dateSpecificRestrictionSource = new DynamicTableSource<SpecialCondition, SpecialConditionViewCell> (_dateSpecificRestrictionViewCellHandler);
				_dateSpecificRestrictionTable = new UITableView ();
				_dateSpecificRestrictionTable.Source = _dateSpecificRestrictionSource;
				_dateSpecificRestrictionTable.RowHeight = SpecialConditionViewCell.CellHeight;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetNavigationBar()
		{
			try {
				if (_isView)
					NavigationItem.Title = "View Admission";
				else if (_registration == null)
					NavigationItem.Title = "Add Admission";
				else
					NavigationItem.Title = "Edit Admission";
				NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			try {
				var doneText = AppContext.LanguageHandler.GetLocalizedString ("Save");
				_saveButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Done, (sender, args) => {
					SaveButton_Click();
				});
				var buttons = new List<UIBarButtonItem> ();
				if (!_isView)
					buttons.Add (_saveButton);
				return buttons;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void SaveButton_Click()
		{
			try {
				if (!Validate ()) {
					this.ShowAlert (AppContext, "MissingRequiredInformation");
					return;
				}
				if (CheckNric () && !NricHandler.IsValidNric (_idNumber.TextField.Text)) {
					this.ShowAlert (AppContext, "PleaseEnterValidNric");
					return;
				}
				var saveModel = CreateSaveModel();
				_requestHandler.SendRequest (View, () => Save (saveModel));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private bool CheckNric ()
		{
			try {
				var document = _model.documents.FirstOrDefault (d => d.id.ToString () == _idTypeDropDown.SelectedKey);
				if (document != null && String.Equals (document.label, "NRIC", StringComparison.InvariantCultureIgnoreCase))
					return true;
				return false;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		private bool Validate ()
		{
			try {
				if (String.IsNullOrEmpty (_idTypeDropDown.SelectedKey))
					return false;
				if (String.IsNullOrEmpty (_patientNameElement.TextField.Text))
					return false;
				if (String.IsNullOrEmpty (_genderDropDown.SelectedKey))
					return false;
				if (String.IsNullOrEmpty (_raceDropDown.SelectedKey))
					return false;
				if (_dateOfBirthPickerButton.SelectedDate == default(DateTime))
					return false;
				if (_admissionTimePickerButton.SelectedDate == default(DateTime))
					return false;
				if (String.IsNullOrEmpty (_bedDropDown.SelectedKey))
					return false;
				if (String.IsNullOrEmpty (_treatmentCategoryDropDown.SelectedKey))
					return false;
				return true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		private AdmissionSaveModel CreateSaveModel ()
		{
			try {
				var admission = _model.SaveModel.Admission ?? new AdmissionModel ();
				if (admission.registration_id == null) {
					admission.institution_id = AppContext.InstitutionId;
				}
				admission.identifier_id = long.Parse(_idTypeDropDown.SelectedKey);
				admission.identifier_number = _idNumber.TextField.Text;
				admission.profile_name = _patientNameElement.TextField.Text;
				admission.gender_id = long.Parse (_genderDropDown.SelectedKey);
				admission.race_id = long.Parse (_raceDropDown.SelectedKey);
				admission.birthday = _dateOfBirthPickerButton.SelectedDate;
				admission.case_number = _caseNumberElement.TextField.Text;
				admission.admission_time = _admissionTimePickerButton.SelectedDate;
				admission.location_id = long.Parse (_bedDropDown.SelectedKey);
				admission.treatment_category_id = long.Parse (_treatmentCategoryDropDown.SelectedKey);
				admission.additional_remarks_1 = _remarkElement.Value;
				if (IsNewAdmission()) {
					admission.manual_registration = true;
				} else {
					admission.manual_registration = _registration.manual_registration;
				}
				var admissionSaveModel = new AdmissionSaveModel {
					Admission = admission,
					Allergies = GetSelections (_allergyOptionsModel),
					DietTextures = GetSelections (_dietTextureOptionsModel),
					MealTypes = GetSelections (_mealTypeOptionsModel),
					Therapeutics = GetSelections (_therapeuticOptionsModel),
					FluidThickness = GetFluidThickness (),
					CustomDietTextures = _customDietTextures
				};
				admissionSaveModel.GroupedDietOrder.Nbms = CreateDietOrders (_selectedSpecialConditions, true);
				admissionSaveModel.GroupedDietOrder.ClearFeeds = CreateDietOrders (_selectedDateSpecificRestrictions, false);
				return admissionSaveModel;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private List<DietOrderCompactModel> CreateDietOrders (List<SpecialCondition> specialConditions, bool isNBM)
		{
			try {
				var result = new List<DietOrderCompactModel> ();
				foreach (var specialCondition in specialConditions) {
					result.Add(new DietOrderCompactModel {
						diet_order_id = specialCondition.DietOrderId,
						meal_order_period_id = specialCondition.StartMealPeriod.Key,
						end_meal_order_period_id = specialCondition.EndMealPeriod.Key,
						profile_diet_order_id = specialCondition.ProfileDietOrderId ?? 0,
						start_effective_time = specialCondition.StartTime,
						end_effective_time = specialCondition.EndTime
					});
				}

				if (isNBM)
					result.AddRange (_model.SaveModel.GroupedDietOrder.Nbms.Where (n => n.cancelled));
				else
					result.AddRange (_model.SaveModel.GroupedDietOrder.ClearFeeds.Where (n => n.cancelled));

				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private List<RestrictionModelSimple> GetFluidThickness ()
		{
			try {
				if (String.IsNullOrEmpty (_fluidThicknessDropDown.SelectedKey))
					return new List<RestrictionModelSimple> ();
				var id = long.Parse(_fluidThicknessDropDown.SelectedKey);
				return _model.GroupedRestriction.FluidThickness.Where (f => f.id == id).ToList ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private async void Save(AdmissionSaveModel model)
		{
			try {
				await AppContext.HttpSender.Request<OperationInfoIpad> ()
					.From (KnownUrls.SaveFullAdmission)
					.WithContent (model)
					.WhenSuccess (result => OnSaveSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnSaveSuccessful (OperationInfoIpad result)
		{
			try {
				if (result.success) {
					OnRequestCompleted (true);
					this.ShowAlert (AppContext, "Savedsuccessful");
				} else {
					OnRequestCompleted (false);
				}
				OnSave.SafeInvoke (this);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private List<RestrictionModelSimple> GetSelections (OptionsModel<RestrictionModelSimple> optionModel)
		{
			try {
				var result = new List<RestrictionModelSimple> ();
				//			optionModel.TypeGroup.ForEach (tg => result.AddRange (tg.Result));
				result.AddRange (optionModel.TypeGroup.Result);
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void Refresh ()
		{
			try {
				_requestHandler.SendRequest (View, RequestViewModel);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestViewModel()
		{
			try {
				var registration_id = _registration == null ? null : _registration.registration_id;
				var param = KnownUrls.GetAdmissionFullQueryString (registration_id, AppContext.InstitutionId);
				await AppContext.HttpSender.Request<AdmissionViewModel> ()
					.From (KnownUrls.GetEditAdmissionFull)
					.WithQueryString (param)
					.WhenSuccess (result => OnRequestAdmissionSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestAdmissionSuccessful (AdmissionViewModel result)
		{
			try {
				if (result == null) {
					OnRequestCompleted (false);
					return;
				}
				_model = result;
				OnRequestCompleted (true);
				GenerateUI (result);
				ShowValues (result);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private UILabel CreateHeader(string title, RectangleF frame, float distanceToAbove)
		{
			try {
				var label = UiGenerator.CreateHeader (title, frame, Fonts.Header);
				AttachToBackground (label, distanceToAbove);
				return label;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void AttachToBackground (UIView view, float distanceToAbove)
		{
			try {
				if (view.Superview == null)
					BackgroundView.Add (view);
				_totalPageHeight += view.Frame.Height + distanceToAbove;
				AdjustBackground ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AdjustBackground ()
		{
			try {
				var scrollViewHeight = _totalPageHeight > Measurements.PageHeight ? _totalPageHeight + 40f : Measurements.PageHeight;
				BackgroundView.SetFrame (height: scrollViewHeight);
				ScrollView.ContentSize = new SizeF (UIScreen.MainScreen.Bounds.Right, scrollViewHeight);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateUI (AdmissionViewModel result)
		{
			try {
				GeneratePatientIdentification ();
				GeneratePatientInformation ();
				GenerateAdmissionInformation ();
				CreateFluidThicknessDropDown ();
				GenerateDietTexture ();
				GenerateAdvancedTexture ();
				GenerateAddSpecialCondition ();
				_specialConditionTable.SetFrameBelowTo (_addSpecialConditionButton, 0, UIScreen.MainScreen.Bounds.Right, 0);
				GenerateAddDateSpecificRestriction ();
				_dateSpecificRestrictionTable.SetFrameBelowTo (_addDateSpecificRestrictionButton, 0, UIScreen.MainScreen.Bounds.Right, 0);
				GenerateAllergy ();
				GenerateTherapeutic ();
				GenerateMealType ();
				GenerateAdditionalInformation ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AdjustLayoutBelowSpecialCondition ()
		{
			try {
				_dateSpecificRestrictionLabel.SetFrameBelowTo (_specialConditionTable);
				_addDateSpecificRestrictionButton.SetFrameBelowTo (_dateSpecificRestrictionLabel);
				_dateSpecificRestrictionTable.SetFrameBelowTo (_addDateSpecificRestrictionButton);
				AdjustLayoutBelowDateSpecificRestriction ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AdjustLayoutBelowDateSpecificRestriction ()
		{
			try {
				_allergyOptionsModel.Dvc.View.SetFrameBelowTo (_dateSpecificRestrictionTable);
				_therapeuticOptionsModel.Dvc.View.SetFrameBelowTo (_allergyOptionsModel.Dvc.View, distanceToAbove: 0f);
				_mealTypeOptionsModel.Dvc.View.SetFrameBelowTo (_therapeuticOptionsModel.Dvc.View, distanceToAbove: 0f);
				_additionalInformationLabel.SetFrameBelowTo (_mealTypeOptionsModel.Dvc.View);
				_remarkElement.SetFrameBelowTo (_additionalInformationLabel, distanceToAbove: BorderSize);
				_decoyLabel.SetFrameBelowTo (_remarkElement);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		void ShowValues (AdmissionViewModel result)
		{
			try {
				_isLoading = true;
				if (IsNewAdmission ()) {
					_dateOfBirthPickerButton.SelectedDate = DateTime.Now;
					_admissionTimePickerButton.SelectedDate = DateTime.Now;
					AdjustLayoutBelowSpecialCondition ();
					_bedDropDown.SelectedKey = _locationWithRegistration.id.ToString ();
					return;
				} else {
					_bedDropDown.SelectedKey = result.SaveModel.Admission.location_id.ToString ();
				}
				_idTypeDropDown.SelectedKey = result.SaveModel.Admission.identifier_id.ToString ();
				_idNumber.TextField.Text = result.SaveModel.Admission.identifier_number;
				_patientNameElement.TextField.Text = result.SaveModel.Admission.profile_name;
				_genderDropDown.SelectedKey = result.SaveModel.Admission.gender_id.ToString ();
				_raceDropDown.SelectedKey = result.SaveModel.Admission.race_id.ToString ();
				_dateOfBirthPickerButton.SelectedDate = result.SaveModel.Admission.birthday;
				_caseNumberElement.TextField.Text = result.SaveModel.Admission.case_number;
				_admissionTimePickerButton.SelectedDate = result.SaveModel.Admission.admission_time;
				_treatmentCategoryDropDown.SelectedKey = result.SaveModel.Admission.treatment_category_id.ToString ();
				if (result.SaveModel.FluidThickness.Any ())
					_fluidThicknessDropDown.SelectedKey = result.SaveModel.FluidThickness.First ().id.ToString ();
				_selectedSpecialConditions = GenerateSpecialConditions (result.SaveModel.GroupedDietOrder.Nbms);
				_selectedDateSpecificRestrictions = GenerateDateSpecificRestrictions (result.SaveModel.GroupedDietOrder.ClearFeeds);
				_customDietTextures = result.SaveModel.CustomDietTextures;
				_remarkElement.Value = result.SaveModel.Admission.additional_remarks_1;
				ShowSpecialConditions ();
				ShowDateSpecificRestrictions ();
				_isLoading = false;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private bool IsNewAdmission ()
		{
			try {
				return _registration == null;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		private void GeneratePatientIdentification ()
		{
			try {
				var distanceToAbove = BorderSize;
				var patientIdentificationLabel = CreateHeader ("Patient Identification", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 0f);
				CreateIdTypeDropDown (distanceToAbove, patientIdentificationLabel);
				/*
			if (_isView || !IsNewAdmission ())
				_idNumber = new RoundedEntryElement (new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight), CaptionWidth, "ID Number*", false, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
			else
				_idNumber = new RoundedEntryElement (new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight), CaptionWidth, "ID Number*", true);
			*/
				_idNumber = new RoundedEntryElement (new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight), CaptionWidth, "ID Number*", !_locationWithRegistration.isSapUp);
				_idNumber.SetFrameBelowTo (_idTypeDropDown, distanceToAbove: distanceToAbove);
				_idNumber.TextField.ReturnKeyType = UIReturnKeyType.Next;
				AttachToBackground (_idNumber, distanceToAbove);
				//if (_isView || !IsNewAdmission ())
				//	return;
				_idNumberSample = new RoundedEntryElement (new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight), CaptionWidth, "", !_locationWithRegistration.isSapUp, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
				_idNumberSample.SetFrameBelowTo (_idNumber, distanceToAbove: distanceToAbove);
				_idNumberSample.TextField.ReturnKeyType = UIReturnKeyType.Next;
				_idNumberSample.TextField.Text = "(e.g. S1234567D)";
				_idNumberSample.TextField.TextColor = UIColor.LightGray;
				_idNumberSample.UserInteractionEnabled = false;
				_idNumberSample.TextField.ClearButtonMode = UITextFieldViewMode.Never;
				AttachToBackground (_idNumberSample, distanceToAbove);
				} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateIdTypeDropDown (float distanceToAbove, UILabel patientIdentificationLabel)
		{
			try {
				var idTypeFrame = new RectangleF (HorizontalMargin, BorderSize, ElementWidth, RowHeight);
				var idTypes = _model.documents.Select (i => new KeyValuePair<string, string> (i.id.ToString (), i.label)).ToList ();
				_idTypeDropDown = CreateRoundedDrown ("ID Type", idTypeFrame, idTypes, patientIdentificationLabel, !IsNewAdmission (), distanceToAbove, UIRectCorner.TopLeft, UIRectCorner.TopRight);
				_idTypeDropDown.UserInteractionEnabled = IsNewAdmission ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GeneratePatientInformation ()
		{
			try {
				var patientInformationLabel = CreateHeader ("Patient Information", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 20f);
				/*
			if (_isView || !IsNewAdmission())
				patientInformationLabel.SetFrameBelowTo (_idNumber);
			else
				patientInformationLabel.SetFrameBelowTo (_idNumberSample);
			*/
				patientInformationLabel.SetFrameBelowTo (_idNumberSample);
				_patientNameElement = new RoundedEntryElement (new RectangleF (HorizontalMargin, BorderSize, ElementWidth, RowHeight), CaptionWidth, "Patient Name*", !_locationWithRegistration.isSapUp, UIRectCorner.TopLeft, UIRectCorner.TopRight);
				_patientNameElement.SetFrameBelowTo (patientInformationLabel, distanceToAbove: BorderSize);
				_patientNameElement.TextField.ReturnKeyType = UIReturnKeyType.Next;
				AttachToBackground (_patientNameElement, BorderSize);
				CreateGenderDropDown ();
				CreateRacesDropDown ();
				var dobFrame = new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight);
				_dateOfBirthPickerButton = UiGenerator.CreateRoundedDatePickerButton (AppContext, BackgroundView, dobFrame, _raceDropDown, CaptionWidth, "Date of Birth*", !_locationWithRegistration.isSapUp, BorderSize, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
				_dateOfBirthPickerButton.DatePickerMode = UIDatePickerMode.Date;
				AttachToBackground (_dateOfBirthPickerButton, BorderSize);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAdmissionInformation ()
		{
			try {
				var admissionInformationLabel = CreateHeader ("Admission Information", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 20f);
				admissionInformationLabel.SetFrameBelowTo (_dateOfBirthPickerButton);
				_caseNumberElement = new RoundedEntryElement (new RectangleF (HorizontalMargin, BorderSize, ElementWidth, RowHeight), CaptionWidth, "Case Number", !_locationWithRegistration.isSapUp, UIRectCorner.TopLeft, UIRectCorner.TopRight);
				_caseNumberElement.SetFrameBelowTo (admissionInformationLabel, distanceToAbove: BorderSize);
				_caseNumberElement.TextField.ReturnKeyType = UIReturnKeyType.Next;
				AttachToBackground (_caseNumberElement, BorderSize);
				var admissionTimeFrame = new RectangleF (HorizontalMargin, 0, ElementWidth, RowHeight);
				_admissionTimePickerButton = UiGenerator.CreateRoundedDatePickerButton (AppContext, BackgroundView, admissionTimeFrame, _caseNumberElement, CaptionWidth, "Admission Time*", !_locationWithRegistration.isSapUp, BorderSize);
				_admissionTimePickerButton.DatePickerMode = UIDatePickerMode.DateAndTime;
				AttachToBackground (_admissionTimePickerButton, BorderSize);
				CreateBedDropDown ();
				CreateTreatmentCategoryDropDown ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateBedDropDown ()
		{
			try {
				var bedFrame = new RectangleF (HorizontalMargin, 0, ElementWidth, RowHeight);
				var beds = _model.Beds.Select (g => new KeyValuePair<string, string> (g.id.Value.ToString (), g.label)).ToList ();
				var enabled = !_isView && (AppContext.CurrentUser.Authorized ("TRANSFER_PATIENT"));
				_bedDropDown = CreateRoundedDrown ("Bed*", bedFrame, beds, _admissionTimePickerButton, !enabled, BorderSize);
				_bedDropDown.UserInteractionEnabled = enabled;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateTreatmentCategoryDropDown ()
		{
			try {
				var treatmentCategoryFrame = new RectangleF (HorizontalMargin, 0, ElementWidth, RowHeight);
				var treatmentCategories = _model.treatmentCategories.Select (g => new KeyValuePair<string, string> (g.id.Value.ToString (), String.Format("{0}-{1}", g.code, g.label))).ToList ();
				_treatmentCategoryDropDown = CreateRoundedDrown ("Treatment Category*", treatmentCategoryFrame, treatmentCategories, _bedDropDown, _isView, BorderSize);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateGenderDropDown ()
		{
			try {
				var genderFrame = new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight);
				var genders = _model.genders.Select (g => new KeyValuePair<string, string> (g.id.Value.ToString (), g.label)).ToList ();
				_genderDropDown = CreateRoundedDrown ("Gender*", genderFrame, genders, _patientNameElement, _isView, BorderSize);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateRacesDropDown ()
		{
			try {
				var races = _model.races.Select (i => new KeyValuePair<string, string> (i.id.ToString (), i.label)).ToList ();
				var raceFrame = new RectangleF (HorizontalMargin, BorderSize, ElementWidth, RowHeight);
				_raceDropDown = CreateRoundedDrown ("Race*", raceFrame, races, _genderDropDown, _isView, BorderSize);
				_raceDropDown.OnDone += HandleOnRaceDone;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnRaceDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
			try {
				if (_isLoading)
					return;
				if (_malayId == null) {
					var malayRace = _model.races.FirstOrDefault (r => r.code == "M");
					if (malayRace == null)
						return;
					_malayId = malayRace.id;
				}
				if (_malayId == null)
					return;
				if (_malayId != long.Parse (e.Value.Key))
					return;
				if (_halalId == null) {
					var halal = _mealTypeOptionsModel.Items.FirstOrDefault (m => m.code == "H");
					if (halal == null)
						return;
					_halalId = halal.id;
				}
				_mealTypeOptionsModel.TypeGroup.Select (_halalId.Value);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateFluidThicknessDropDown ()
		{
			try {
				var fluidThicknessFrame = new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth, RowHeight);
				var fluidThicknesss = _model.GroupedRestriction.FluidThickness.Select (g => new KeyValuePair<string, string> (g.id.Value.ToString (), g.label)).ToList ();
				_fluidThicknessDropDown = CreateRoundedDrown ("Fluid Thickness", fluidThicknessFrame, fluidThicknesss, _treatmentCategoryDropDown, _isView, BorderSize, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAdditionalInformation ()
		{
			try {
				_additionalInformationLabel = CreateHeader ("Additional Information", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 20f);
				_remarkElement = new RoundedMultiLineEntryElement (new RectangleF (HorizontalMargin, _idTypeDropDown.Frame.Top + RowHeight + BorderSize, ElementWidth
					, RowHeight * 3), CaptionWidth, "On-Going Remarks\n(Max Length: 100 chars)", true, 100, UIRectCorner.BottomLeft | UIRectCorner.TopLeft, UIRectCorner.BottomRight | UIRectCorner.TopRight);
				AttachToBackground (_remarkElement, BorderSize);
				_decoyLabel = CreateHeader ("", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 10f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateDietTexture ()
		{
			try {
				_dietTextureOptionsModel = new OptionsModel<RestrictionModelSimple> {
					Title = "Diet Type - Diet Texture",
					SelectedItems = _model.SaveModel.DietTextures,
					Items = _model.GroupedRestriction.DietTextures
				};
				AttachOptionList (_dietTextureOptionsModel, OnDietTextureSelected, 20f);
				_dietTextureOptionsModel.Dvc.View.SetFrameBelowTo (_fluidThicknessDropDown);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAdvancedTexture ()
		{
			try {
				_addDishTypeTextureButton = new UIButton ();
				_addDishTypeTextureButton.SetTitle ("Advanced Settings for Texture Restrictions", UIControlState.Normal);
				_addDishTypeTextureButton.BackgroundColor = Colors.ButtonBackground;
				_addDishTypeTextureButton.Layer.CornerRadius = 10;
				var left = 10f;
				var width = UIScreen.MainScreen.Bounds.Right - (2 * left);
				if (_isView)
					_addDishTypeTextureButton.SetFrameBelowTo (_dietTextureOptionsModel.Dvc.View, left, width, 0f, 0f);
				else
					_addDishTypeTextureButton.SetFrameBelowTo (_dietTextureOptionsModel.Dvc.View, left, width, RowHeight);
				_addDishTypeTextureButton.Hidden = _isView;
				_addDishTypeTextureButton.TouchUpInside += AddDishTypeTextureButton_TouchUpInside;
				AttachToBackground (_addDishTypeTextureButton, 20f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAddSpecialCondition ()
		{
			try {
				var specialConditionLabel = CreateHeader ("Special Condition", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 20f);
				specialConditionLabel.SetFrameBelowTo (_addDishTypeTextureButton);
				_addSpecialConditionButton = new UIButton ();
				_addSpecialConditionButton.SetTitle ("Add Special Condition", UIControlState.Normal);
				_addSpecialConditionButton.BackgroundColor = Colors.ButtonBackground;
				_addSpecialConditionButton.Layer.CornerRadius = 10;
				var left = 10f;
				var width = UIScreen.MainScreen.Bounds.Right - (2 * left);
				if (_isView)
					_addSpecialConditionButton.SetFrameBelowTo (specialConditionLabel, left, width, 0f, 0f);
				else
					_addSpecialConditionButton.SetFrameBelowTo (specialConditionLabel, left, width, RowHeight);
				_addSpecialConditionButton.Hidden = _isView;
				_addSpecialConditionButton.TouchUpInside += AddSpecialConditionButton_TouchUpInside;
				AttachToBackground (_addSpecialConditionButton, 20f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AddSpecialConditionButton_TouchUpInside (object sender, EventArgs e)
		{
			try {
				ShowAddSpecialCondition ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowAddSpecialCondition (SpecialCondition model = null)
		{
			try {
				_addSpecialConditionViewController = new AddSpecialConditionViewController (AppContext, _selectedSpecialConditions, model);
				_addSpecialConditionViewController.IsDateSpecific = false;
				_addSpecialConditionViewController.ViewTitle = "Special Condition";
				_addSpecialConditionViewController.DietOrders = _model.DietOrders;
				_addSpecialConditionViewController.MealOrderPeriodsWithSequences = _model.MealOrderPeriodsWithSequences;
				_addSpecialConditionViewController.MealOrderPeriods = _model.MealOrderPeriods;
				_addSpecialConditionViewController.OnDone += HandleOnAddSpecialConditionDone;
				_addSpecialConditionViewController.OnCancel += HandleOnAddSpecialConditionCancel;
				View.AddSubview (_addSpecialConditionViewController.View);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAddDateSpecificRestriction ()
		{
			try {
				_dateSpecificRestrictionLabel = CreateHeader ("Date Specific Restriction", new RectangleF (HeaderLeft, 0, UIScreen.MainScreen.Bounds.Right, RowHeight), 20f);
				_dateSpecificRestrictionLabel.SetFrameBelowTo (_specialConditionTable);
				_addDateSpecificRestrictionButton = new UIButton ();
				_addDateSpecificRestrictionButton.SetTitle ("Add Date Specific Restriction", UIControlState.Normal);
				_addDateSpecificRestrictionButton.BackgroundColor = Colors.ButtonBackground;
				_addDateSpecificRestrictionButton.Layer.CornerRadius = 10;
				var left = 10f;
				var width = UIScreen.MainScreen.Bounds.Right - (2 * left);
				if (_isView)
					_addDateSpecificRestrictionButton.SetFrameBelowTo (_dateSpecificRestrictionLabel, left, width, 0f, 0f);
				else
					_addDateSpecificRestrictionButton.SetFrameBelowTo (_dateSpecificRestrictionLabel, left, width, RowHeight);
				_addDateSpecificRestrictionButton.Hidden = _isView;
				_addDateSpecificRestrictionButton.TouchUpInside += AddDateSpecificRestrictionButton_TouchUpInside;
				AttachToBackground (_addDateSpecificRestrictionButton, 20f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AddDateSpecificRestrictionButton_TouchUpInside (object sender, EventArgs e)
		{
			try {
				ShowAddDateSpecificRestriction ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowAddDateSpecificRestriction (SpecialCondition model = null)
		{
			try {
				_addDateSpecificRestrictionViewController = new AddSpecialConditionViewController (AppContext, _selectedDateSpecificRestrictions, model);
				_addDateSpecificRestrictionViewController.IsDateSpecific = true;
				_addDateSpecificRestrictionViewController.ViewTitle = "Date Specific Restriction";
				_addDateSpecificRestrictionViewController.DietOrders = _model.dateSpecificRestrictions;
				_addDateSpecificRestrictionViewController.MealOrderPeriodsWithSequences = _model.MealOrderPeriodsWithSequences;
				_addDateSpecificRestrictionViewController.MealOrderPeriods = _model.MealOrderPeriods;
				_addDateSpecificRestrictionViewController.OnDone += HandleOnAddDateSpecificRestrictionDone;
				_addDateSpecificRestrictionViewController.OnCancel += HandleOnAddDateSpecificRestrictionCancel;
				View.AddSubview (_addDateSpecificRestrictionViewController.View);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AddDishTypeTextureButton_TouchUpInside (object sender, EventArgs e)
		{
			try {
				ShowAddDishTypeTexture ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowAddDishTypeTexture (DishTypeTexture model = null)
		{
			try {
				_addDishTypeTextureViewController = new DishTypeTextureViewController (AppContext, _customDietTextures, _model.GroupedRestriction.DietTextures, _model.DishTypes);
				_addDishTypeTextureViewController.OnDone += HandleOnDishTypeTextureDone;
				_addDishTypeTextureViewController.OnCancel += HandleOnDishTypeTextureCancel;
				View.AddSubview (_addDishTypeTextureViewController.View);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnDishTypeTextureDone (object sender, BaseEventArgs<List<CustomDietTexture>> e)
		{
			try {
				_customDietTextures = e.Value;
				HandleOnDishTypeTextureCancel (sender, e);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		void HandleOnDishTypeTextureCancel (object sender, EventArgs e)
		{
			try {
				_addDishTypeTextureViewController.View.RemoveFromSuperview ();
				_addDishTypeTextureViewController.Dispose ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateAllergy ()
		{
			try {
				_allergyOptionsModel = new OptionsModel<RestrictionModelSimple> {
					Title = "Diet Type - Allergy",
					SelectedItems = _model.SaveModel.Allergies,
					Items = _model.GroupedRestriction.Allergies
				};
				AttachOptionList (_allergyOptionsModel, OnAllergySelected, 10f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnAddSpecialConditionDone (object sender, BaseEventArgs<List<SpecialCondition>> e)
		{
			try {
				_selectedSpecialConditions = e.Value;
				HandleOnAddSpecialConditionCancel (sender, e);
				ShowSpecialConditions ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnAddSpecialConditionCancel (object sender, EventArgs e)
		{
			try {
				_addSpecialConditionViewController.View.RemoveFromSuperview ();
				_addSpecialConditionViewController.Dispose ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnAddDateSpecificRestrictionDone (object sender, BaseEventArgs<List<SpecialCondition>> e)
		{
			try {
				_selectedDateSpecificRestrictions = e.Value;
				HandleOnAddDateSpecificRestrictionCancel (sender, e);
				ShowDateSpecificRestrictions ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnAddDateSpecificRestrictionCancel (object sender, EventArgs e)
		{
			try {
				_addDateSpecificRestrictionViewController.View.RemoveFromSuperview ();
				_addDateSpecificRestrictionViewController.Dispose ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateTherapeutic ()
		{
			try {
				_therapeuticOptionsModel = new OptionsModel<RestrictionModelSimple> {
					Title = "Diet Type - Therapeutic Diet",
					SelectedItems = _model.SaveModel.Therapeutics,
					Items = _model.GroupedRestriction.Therapeutics
				};
				AttachOptionList (_therapeuticOptionsModel, OnTherapeuticSelected, 0f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnRestrictionSelected (object sender, BaseEventArgs<RestrictionModelSimple> e)
		{
			try {
				var restriction = e.Value;
				foreach (var collidedId in restriction.collided_ids) {
					_therapeuticOptionsModel.TypeGroup.Deselect (collidedId);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateMealType ()
		{
			try {
				_mealTypeOptionsModel = new OptionsModel<RestrictionModelSimple> {
					Title = "Diet Type - Meal Type",
					SelectedItems = _model.SaveModel.MealTypes,
					Items = _model.GroupedRestriction.MealTypes
				};
				AttachOptionList (_mealTypeOptionsModel, OnMealTypeSelected, 0f);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AttachOptionList(OptionsModel<RestrictionModelSimple> option, EventHandler<IdEventArgs> onItemSelected, float distanceToAbove)
		{
			try {
				option.Checkboxes = new List<Section> ();
				var checkboxGroup = new CheckboxGroupDVC<RestrictionModelSimple> (option.Title, option.SelectedItems, option.Items);
				option.TypeGroup = checkboxGroup;
				option.TypeGroup.OnItemSelected += HandleOnRestrictionSelected;
				option.Checkboxes.Add (checkboxGroup.Create ());
				checkboxGroup.OnCheckboxSelected += onItemSelected;
				option.Dvc = new DialogViewController (new RootElement (option.Title));
				option.Dvc.Root.Add (option.Checkboxes);
				CustomizeTableView (option);
				AttachToBackground (option.Dvc.View, distanceToAbove);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private static void CustomizeTableView (OptionsModel<RestrictionModelSimple> option)
		{
			try {
				var tableView = option.Dvc.TableView;
				tableView.ScrollEnabled = false;
				tableView.RowHeight = RowHeight;
				tableView.SetFrame (height: 60 + (RowHeight * option.Items.Count));
				tableView.SectionHeaderHeight = 0.1f;
			} catch (Exception) {
				return;
			}
		}
		private void OnAllergySelected (object sender, IdEventArgs e)
		{
			try {
				if (_isPrompting)
					return;
				var booleanElement = (BooleanElement)sender;
				if (!booleanElement.Value)
					return;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnTherapeuticSelected (object sender, IdEventArgs e)
		{
			try {
				if (_isPrompting)
					return;
				var booleanElement = (BooleanElement)sender;
				if (!booleanElement.Value)
					return;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnDietTextureSelected (object sender, IdEventArgs e)
		{
			try {
				if (_isPrompting)
					return;
				var booleanElement = (BooleanElement)sender;
				if (!booleanElement.Value)
					return;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnMealTypeSelected (object sender, IdEventArgs e)
		{
			try {
				if (_isPrompting)
					return;
				var booleanElement = (BooleanElement)sender;
				if (!booleanElement.Value)
					return;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private RoundedDropDown CreateRoundedDrown (string title, RectangleF frame, List<KeyValuePair<string, string>> items, UIView above, bool hideArrow, float distanceToAbove = 20f, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone)
		{
			try {
				var result = new RoundedDropDown (AppContext, BackgroundView, frame, CaptionWidth, title, items, !_locationWithRegistration.isSapUp, captionRectCorner, textFieldRectCorner, hideArrow);
				result.SetElementFrameBelowTo (above, distanceToAbove: distanceToAbove);
				AttachToBackground (result, distanceToAbove);
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		public void Delete (SpecialCondition item)
		{
			try {
				if (!item.IsDateSpecific) {
					if (item.ProfileDietOrderId != null) {
						var nbm = _model.SaveModel.GroupedDietOrder.Nbms.First (n => n.profile_diet_order_id == item.ProfileDietOrderId);
						nbm.cancelled = true;
					}
					_selectedSpecialConditions.RemoveAll (s => s.StartTime == item.StartTime && s.StartMealPeriod.Key == item.StartMealPeriod.Key);
					ShowSpecialConditions ();
				} else {
					if (item.ProfileDietOrderId != null) {
						var feeds = _model.SaveModel.GroupedDietOrder.ClearFeeds.First (n => n.profile_diet_order_id == item.ProfileDietOrderId);
						feeds.cancelled = true;
					}
					_selectedDateSpecificRestrictions.RemoveAll (s => s.StartTime == item.StartTime && s.StartMealPeriod.Key == item.StartMealPeriod.Key && s.DietOrderId == item.DietOrderId);
					ShowDateSpecificRestrictions ();
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void Edit (SpecialCondition item)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void ItemSelected (SpecialCondition item, int selected)
		{
			try {
				if (!item.IsDateSpecific) {
					ShowAddSpecialCondition(item);
				} else {
					ShowAddDateSpecificRestriction (item);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowSpecialConditions ()
		{
			try {
				var distanceToAbove = _isView ? 0f : 20f;
				if (_selectedSpecialConditions.Any ()) {
					_specialConditionTable.Hidden = false;
					_specialConditionTable.SetFrameBelowTo (_addSpecialConditionButton, 0, UIScreen.MainScreen.Bounds.Right, _selectedSpecialConditions.Count * SpecialConditionViewCell.CellHeight);
					_specialConditionSource.Items = _selectedSpecialConditions;
					_specialConditionTable.ReloadData ();
					AttachToBackground (_specialConditionTable, distanceToAbove);
				} else {
					_specialConditionTable.Hidden = true;
				}
				if (_specialConditionTable.Frame.Height > 0) {
					_totalPageHeight -= (_specialConditionTable.Frame.Height + distanceToAbove);
					AdjustBackground ();
				}
				AdjustLayoutBelowSpecialCondition ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private List<SpecialCondition> GenerateSpecialConditions (List<DietOrderCompactModel> dietOrders)
		{
			try {
				var result = new List<SpecialCondition> ();
				foreach (var dietOrder in dietOrders) {
					var endMealPeriodId = dietOrder.end_meal_order_period_id == null ? dietOrder.meal_order_period_id : dietOrder.end_meal_order_period_id;
					var specialCondition = new SpecialCondition {
						ProfileDietOrderId = dietOrder.profile_diet_order_id,
						DietOrderId = dietOrder.diet_order_id,
						DietOrder = KeyValueHelper.GenerateKeyValue (_model.DietOrders, dietOrder.restriction_id.Value),
						StartTime = dietOrder.start_effective_time.Value,
						EndTime = dietOrder.end_effective_time,
						StartMealPeriod = KeyValueHelper.GenerateKeyValue (_model.MealOrderPeriods, dietOrder.meal_order_period_id.Value),
						EndMealPeriod = KeyValueHelper.GenerateKeyValue (_model.MealOrderPeriods, endMealPeriodId.Value),
						IsDateSpecific = false
					};
					result.Add (specialCondition);
				}
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void ShowDateSpecificRestrictions ()
		{
			try {
				var distanceToAbove = _isView ? 0f : 20f;
				if (_selectedDateSpecificRestrictions.Any ()) {
					_dateSpecificRestrictionTable.Hidden = false;
					_dateSpecificRestrictionTable.SetFrameBelowTo (_addDateSpecificRestrictionButton, 0, UIScreen.MainScreen.Bounds.Right, _selectedDateSpecificRestrictions.Count * SpecialConditionViewCell.CellHeight);
					_dateSpecificRestrictionSource.Items = _selectedDateSpecificRestrictions;
					_dateSpecificRestrictionTable.ReloadData ();
					AttachToBackground (_dateSpecificRestrictionTable, distanceToAbove);
				} else {
					_dateSpecificRestrictionTable.Hidden = true;
				}
				if (_dateSpecificRestrictionTable.Frame.Height > 0) {
					_totalPageHeight -= (_dateSpecificRestrictionTable.Frame.Height + distanceToAbove);
					AdjustBackground ();
				}
				AdjustLayoutBelowDateSpecificRestriction ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private List<SpecialCondition> GenerateDateSpecificRestrictions (List<DietOrderCompactModel> dietOrders)
		{
			try {
				var result = new List<SpecialCondition> ();
				foreach (var dietOrder in dietOrders) {
					var endMealPeriodId = dietOrder.end_meal_order_period_id == null ? dietOrder.meal_order_period_id : dietOrder.end_meal_order_period_id;
					var specialCondition = new SpecialCondition {
						ProfileDietOrderId = dietOrder.profile_diet_order_id,
						DietOrderId = dietOrder.diet_order_id,
						DietOrder = KeyValueHelper.GenerateKeyValue (_model.dateSpecificRestrictions, dietOrder.restriction_id.Value),
						StartTime = dietOrder.start_effective_time.Value,
						EndTime = dietOrder.end_effective_time,
						StartMealPeriod = KeyValueHelper.GenerateKeyValue (_model.MealOrderPeriods, dietOrder.meal_order_period_id.Value),
						EndMealPeriod = KeyValueHelper.GenerateKeyValue (_model.MealOrderPeriods, endMealPeriodId.Value),
						IsDateSpecific = true
					};
					result.Add (specialCondition);
				}
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
	}
}

