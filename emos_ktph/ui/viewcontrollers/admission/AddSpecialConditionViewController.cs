﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class AddSpecialConditionViewController : BaseViewController
	{
		public event EventHandler<BaseEventArgs<List<SpecialCondition>>> OnDone;
		public event EventHandler OnCancel;
		private List<BaseIpadModel> _dietOrders = new List<BaseIpadModel> ();
		public List<BaseIpadModel> DietOrders {
			get { return _dietOrders; }
			set { _dietOrders = value; }
		}
		private List<MealOrderPeriodModelSimple> _mealOrderPeriodsWithSequences = new List<MealOrderPeriodModelSimple> ();
		public List<MealOrderPeriodModelSimple> MealOrderPeriodsWithSequences {
			get { return _mealOrderPeriodsWithSequences; }
			set { _mealOrderPeriodsWithSequences = value; }
		}
		private List<BaseIpadModel> _mealOrderPeriods;
		public List<BaseIpadModel> MealOrderPeriods {
			get { return _mealOrderPeriods; }
			set { _mealOrderPeriods = value; }
		}
		private int _editedIndex { get; set; }
		private List<SpecialCondition> _selectedItems { get; set; }
		private SpecialCondition _model { get; set; }
		private const float ElementLeft = 30f;
		private const float ElementHeight = 40f;
		private float ElementWidth;
		private UIBarButtonItem _doneButton;
		private UIBarButtonItem _backButton;
		private DropDownButton _addConditionDropDown;
		private UILabel _specialConditionLabel;
		private UILabel _startTimeLabel;
		private UIDatePickerButton _startDatePickerButton;
		private UILabel _endTimeLabel;
		private UIDatePickerButton _endDatePickerButton;
		private DropDownButton _startMealPeriodDropDown;
		private DropDownButton _endMealPeriodDropDown;
		private string _viewTitle = "Special Condition";
		public string ViewTitle {
			get { return _viewTitle; }
			set { _viewTitle = value; }
		}
		private bool _isDatespecific = false;
		public bool IsDateSpecific {
			get { return _isDatespecific; }
			set { _isDatespecific = value; }
		}

		public AddSpecialConditionViewController (IApplicationContext appContext, List<SpecialCondition> selectedItems, SpecialCondition edited = null) : base (appContext, "AddSpecialConditionViewController", null)
		{
			_dietOrders = new List<BaseIpadModel> ();
			_selectedItems = selectedItems;
			_model = edited;
			_editedIndex = GetEditedIndex (selectedItems, edited);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			SetBackground ();
			ElementWidth = BackgroundView.Frame.Width - (2 * ElementLeft);
			SetNavigationBar ();
			GenerateUI ();
			ShowValues ();
		}
		private int GetEditedIndex (List<SpecialCondition> selectedItems, SpecialCondition edited)
		{
			if (edited != null) {
				for (int i = 0; i < selectedItems.Count; i++) {
					if (selectedItems [i].StartTime == edited.StartTime && selectedItems [i].StartMealPeriod.Key == edited.StartMealPeriod.Key && selectedItems [i].DietOrderId == edited.DietOrderId)
						return i;
				}
			}
			return -1;
		}
		private void GenerateUI ()
		{
			var labelFrame = new RectangleF (ElementLeft, 50, ElementWidth, ElementHeight);
			_specialConditionLabel = UiGenerator.CreateHeader (_viewTitle, labelFrame, Fonts.Header);
			BackgroundView.Add (_specialConditionLabel);
			_addConditionDropDown = CreateDropDown ("Add " + _viewTitle, _specialConditionLabel);
			CreateStartTimeGroupUI (labelFrame);
			CreateEndTimeGroupUI (labelFrame);
		}
		private void ShowValues ()
		{
			if (_model == null)
				return;
			_startDatePickerButton.SelectedDate = _model.StartTime;
			_endDatePickerButton.SelectedDate = _model.EndTime ?? default(DateTime);
			if (_model.EndTime == null)
				_endDatePickerButton.SetTitle (SpecialConditionViewCell.DefaultEndDateTitle, UIControlState.Normal);
			if (_model.DietOrderId == null)
				return;
			_addConditionDropDown.SelectedKey = _model.DietOrder.Key.ToString ();
			_startMealPeriodDropDown.SelectedKey = _model.StartMealPeriod.Key.ToString ();
			if (!String.IsNullOrEmpty(_model.EndMealPeriod.Value))
				_endMealPeriodDropDown.SelectedKey = _model.EndMealPeriod.Key.ToString ();
			else
				_endMealPeriodDropDown.SelectedKey = _model.StartMealPeriod.Key.ToString ();
		}
		private void CreateStartTimeGroupUI (RectangleF labelFrame)
		{
			_startTimeLabel = UiGenerator.CreateHeader ("Start Time", labelFrame, Fonts.Header);
			_startTimeLabel.SetFrameBelowTo (_addConditionDropDown);
			BackgroundView.Add (_startTimeLabel);
			_startDatePickerButton = UiGenerator.CreateDatePickerButton (AppContext, View, Colors.ViewCellBackground, "Start time", labelFrame, _startTimeLabel);
			_startDatePickerButton.DatePickerMode = UIDatePickerMode.Date;
			_startDatePickerButton.SelectedDate = DateTime.Today;
			BackgroundView.Add (_startDatePickerButton);
			_startMealPeriodDropDown = CreateDropDown ("Start Meal Period", _startDatePickerButton, 10);
			var mealPeriods = _mealOrderPeriods.Select (m => new KeyValuePair<string,string> (m.id.ToString (), m.label)).ToList ();
			_startMealPeriodDropDown.Load (AppContext, BackgroundView, "Start Meal Period", mealPeriods);
		}
		private void CreateEndTimeGroupUI (RectangleF labelFrame)
		{
			_endTimeLabel = UiGenerator.CreateHeader ("End Time", labelFrame, Fonts.Header);
			_endTimeLabel.SetFrameBelowTo (_startMealPeriodDropDown);
			BackgroundView.Add (_endTimeLabel);
			_endDatePickerButton = UiGenerator.CreateDatePickerButton (AppContext, View, Colors.ViewCellBackground, "End time", labelFrame, _endTimeLabel);
			_endDatePickerButton.DatePickerMode = UIDatePickerMode.Date;
			_endDatePickerButton.SetTitle (SpecialConditionViewCell.DefaultEndDateTitle, UIControlState.Normal);
			BackgroundView.Add (_endDatePickerButton);
			_endMealPeriodDropDown = CreateDropDown ("End Meal Period", _endDatePickerButton, 10);
			var mealPeriods = _mealOrderPeriods.Select (m => new KeyValuePair<string,string> (m.id.ToString (), m.label)).ToList ();
			_endMealPeriodDropDown.Load (AppContext, BackgroundView, "End Meal Period", mealPeriods);
		}
		private void SetBackground ()
		{
			var left = 134f;
			var width = UIScreen.MainScreen.Bounds.Right - (left * 2);
			BackgroundView.SetFrame (left, 100, width, 800f);
			BackgroundView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
			BackgroundView.Layer.BorderWidth = 1.0f;
			BackgroundView.BackgroundColor = UIColor.White;
			BackgroundView.AutosizesSubviews = false;
			BackgroundView.ClipsToBounds = false;
		}
		private DropDownButton CreateDropDown (string title, UIView aboveView, float distanceToAbove = 0f)
		{
			var result = new DropDownButton ("Select", true, true);
			var items = _dietOrders.Select (i => new KeyValuePair<string, string> (i.id.ToString (), i.label)).ToList ();
			result.Load (AppContext, BackgroundView, title, items);
			result.SelectedKey = _model == null ? "" : _model.DietOrderId.ToString ();;
			result.OnDone += HandleOnConditionDropDownDone;
			result.SetFrameBelowTo (aboveView, ElementLeft, ElementWidth, distanceToAbove: distanceToAbove);
			result.AddToParentView (BackgroundView);
			return result;
		}
		private void HandleOnConditionDropDownDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			AnimateMe (this, BackgroundView);
		}
		private void SetNavigationBar()
		{
			NavigationBar.TopItem.Title = "Add " + _viewTitle;
			NavigationBar.TopItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			NavigationBar.TopItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
			NavigationBar.SetFrame (width: BackgroundView.Frame.Width);
			AppContext.ColorHandler.PaintNavigationBar (NavigationBar);
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var doneText = AppContext.LanguageHandler.GetLocalizedString ("Done");
			_doneButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Done, (sender, args) => {
				DoneButton_Clicked ();
			});
			var buttons = new List<UIBarButtonItem> {
				_doneButton
			};
			return buttons;
		}
		private void DoneButton_Clicked ()
		{
			var specialCondition = _model ?? new SpecialCondition ();

			// Validate required fields.
			if (!ValidateRequired ()) {
				this.ShowAlert (AppContext, "MissingRequiredInformation");
				return;
			}

			// Validate input date before today.
			if (!ValidatePastDate ()) {
				this.ShowAlert (AppContext, "DateIsInThePast");
				return;
			}

			// Validate start time bigger than end time.
			if (!ValidateStartTimeBigger ()) {
				this.ShowAlert (AppContext, "EndTimeShouldBeBiggerThanTimeDate");
				return;
			}

			// Validate same start & end date.
			if (!ValidateSameDay ()) {
				this.ShowAlert (AppContext, "EndMealPeriodShouldBeBiggerThanStartMealPeriod");
				return;
			}
				
			// Validate different start & end date.
			if (!ValidateDifferentDay ()) {
				this.ShowAlert (AppContext, "ConflictWithExistingPeriod");
				return;
			}

			specialCondition.IsDateSpecific = _isDatespecific;
			specialCondition.DietOrderId = long.Parse (_addConditionDropDown.SelectedKey);
			specialCondition.DietOrder = KeyValueHelper.GenerateKeyValue (_dietOrders, long.Parse (_addConditionDropDown.SelectedKey));
			specialCondition.StartTime = _startDatePickerButton.SelectedDate;
			specialCondition.StartMealPeriod = KeyValueHelper.GenerateKeyValue (_mealOrderPeriods, long.Parse (_startMealPeriodDropDown.SelectedKey));
			specialCondition.EndMealPeriod = KeyValueHelper.GenerateKeyValue (_mealOrderPeriods, long.Parse (_endMealPeriodDropDown.SelectedKey));
			specialCondition.EndTime = _endDatePickerButton.SelectedDate == default(DateTime) ? (DateTime?) null : _endDatePickerButton.SelectedDate;

			if (_editedIndex == -1)
				_selectedItems.Add (specialCondition);
			else
				_selectedItems [_editedIndex] = specialCondition;
			OnDone.SafeInvoke (this, new BaseEventArgs<List<SpecialCondition>> ().Initiate (_selectedItems));
		}

		private int ParseValidationDate (DateTime date, int mealOrderPeriodSequence, bool isInfinite) 
		{
			if (isInfinite) {
				return int.MaxValue;
			} else {
				return ((date.Year * 1000000) + (date.Month * 10000) + (date.Day * 100) + mealOrderPeriodSequence);
			}
		}
		private bool ValidateRequired ()
		{
			if (String.IsNullOrEmpty(_addConditionDropDown.SelectedKey))
				return false;
			if (_startDatePickerButton.SelectedDate == default(DateTime))
				return false;
			if (String.IsNullOrEmpty (_startMealPeriodDropDown.SelectedKey))
				return false;
			if (String.IsNullOrEmpty (_endMealPeriodDropDown.SelectedKey))
				return false;
			return true;
		}
		private bool ValidatePastDate ()
		{
			var currentStart = ParseValidationDate (_startDatePickerButton.SelectedDate, 0, false);
			var currentEnd = ParseValidationDate (_endDatePickerButton.SelectedDate, 0, (_endDatePickerButton.SelectedDate == default(DateTime)));
			var today = ParseValidationDate (DateTime.Today.Date, 0, false);
			if (currentStart < today || currentEnd < today)
				return false;
			return true;
		}
		private bool ValidateStartTimeBigger ()
		{
			var currentStart = ParseValidationDate (_startDatePickerButton.SelectedDate, 0, false);
			var currentEnd = ParseValidationDate (_endDatePickerButton.SelectedDate, 0, (_endDatePickerButton.SelectedDate == default(DateTime)));
			if (currentStart > currentEnd)
				return false;
			return true;
		}
		private bool ValidateSameDay ()
		{
			if (_startDatePickerButton.SelectedDate == _endDatePickerButton.SelectedDate) {
				var startMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == long.Parse(_startMealPeriodDropDown.SelectedKey)).order;
				var endMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == long.Parse(_endMealPeriodDropDown.SelectedKey)).order;
				var currentStart = ParseValidationDate (_startDatePickerButton.SelectedDate, startMealPeriodSequence, false);
				var currentEnd = ParseValidationDate (_endDatePickerButton.SelectedDate, endMealPeriodSequence, (_endDatePickerButton.SelectedDate == default(DateTime)));

				if (currentStart > currentEnd)
					return false;
			}
			return true;
		}
		private bool ValidateDifferentDay ()
		{
			for (int i = 0; i < _selectedItems.Count; i++) {
				if (i == _editedIndex)
					continue;

				var startMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == long.Parse(_startMealPeriodDropDown.SelectedKey)).order;
				var endMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == long.Parse(_endMealPeriodDropDown.SelectedKey)).order;
				var currentStart = ParseValidationDate (_startDatePickerButton.SelectedDate, startMealPeriodSequence, false);
				var currentEnd = ParseValidationDate (_endDatePickerButton.SelectedDate, endMealPeriodSequence, (_endDatePickerButton.SelectedDate == default(DateTime)));

				var comparer = _selectedItems [i];
				var comparerStartMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == comparer.StartMealPeriod.Key).order;
				var comparerEndMealPeriodSequence = _mealOrderPeriodsWithSequences.Find (e => e.id == comparer.EndMealPeriod.Key).order;
				var comparerStart = ParseValidationDate (comparer.StartTime, comparerStartMealPeriodSequence, false);
				var comparerEnd = 0;
				if (comparer.EndTime == null) {
					comparerEnd = ParseValidationDate (DateTime.Today, comparerEndMealPeriodSequence, (comparer.EndTime == null));
				} else {
					comparerEnd = ParseValidationDate ((DateTime) comparer.EndTime, comparerEndMealPeriodSequence, (comparer.EndTime == null));
				}

				if ((currentStart < comparerStart && currentEnd < comparerStart) || (currentStart > comparerEnd && currentEnd > comparerEnd)) {
					continue;
				} else {
					return false;
				}
			}
			return true;
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			var backText = AppContext.LanguageHandler.GetLocalizedString ("Back");
			_backButton = new UIBarButtonItem (backText, UIBarButtonItemStyle.Done, (sender, args) => {
				OnCancel.SafeInvoke (this);
			});
			var buttons = new List<UIBarButtonItem> {
				_backButton
			};
			return buttons;
		}
		private static void AnimateMe(UIViewController viewController, UIView backgroundView)
		{
			var left = (UIScreen.MainScreen.Bounds.Right - backgroundView.Frame.Width) / 2;
			backgroundView.Frame = new RectangleF (left, UIScreen.MainScreen.Bounds.Bottom, backgroundView.Frame.Width, backgroundView.Frame.Height);
			UIView.BeginAnimations ("slideAnimation");
			UIView.SetAnimationDuration (0.5);
			UIView.SetAnimationDelegate (viewController);
			backgroundView.Center = new PointF (UIScreen.MainScreen.Bounds.Right / 2, UIScreen.MainScreen.Bounds.Bottom / 2);
			UIView.CommitAnimations ();
		}
	}
}
