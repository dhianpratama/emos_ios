// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("PatientMealCalendarViewController")]
	partial class PatientMealCalendarViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BodyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView MealPeriodTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UICollectionViewFlowLayout OrderCollectionViewFlowLayout { get; set; }

		[Outlet]
		MonoTouch.UIKit.UICollectionView PatientOrderCollectionView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BodyView != null) {
				BodyView.Dispose ();
				BodyView = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (MealPeriodTable != null) {
				MealPeriodTable.Dispose ();
				MealPeriodTable = null;
			}

			if (OrderCollectionViewFlowLayout != null) {
				OrderCollectionViewFlowLayout.Dispose ();
				OrderCollectionViewFlowLayout = null;
			}

			if (PatientOrderCollectionView != null) {
				PatientOrderCollectionView.Dispose ();
				PatientOrderCollectionView = null;
			}
		}
	}
}
