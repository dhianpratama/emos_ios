﻿
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.IO;
using MonoTouch.Dialog.Utilities;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios.tools;
using core_emos;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;

namespace emos_ios
{
	public partial class PatientMenuViewController : BaseViewController
	{
		private IPatientMenu _controller;
		private LocationWithRegistrationSimple _locationWithRegistrationSimple;
		private InterfaceStatusModel _interfaceStatus;
		private List<KeyValuePair<string,NSAction>> _menus;
		private int _heightView;
		private int _buttonGap = 8;
		private int _buttonHeight = 50;

		public PatientMenuViewController (IApplicationContext appContext, IPatientMenu controller, LocationWithRegistrationSimple location, InterfaceStatusModel interfaceStatus) : base (appContext, "PatientMenuViewController", null)
		{
			_controller = controller;
			_locationWithRegistrationSimple = location;
			_interfaceStatus = interfaceStatus;
			HiddenLogo = true;
			RegisterMenu ();
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			MenuView.BackgroundColor = Colors.MainThemeColor;
			GenerateMenuToView ();
			CalculateHeightView ();

			MenuView.SetFrame (y: 1024);
		}
		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear (animated);
			UIView.BeginAnimations ("slideAnimation");
			UIView.SetAnimationDuration (0.2);
			UIView.SetAnimationDelegate (this);
			MenuView.SetFrame (y : 1024 - _heightView);
			UIView.CommitAnimations ();
		}


		private void RegisterMenu()
		{
			_menus = new List<KeyValuePair<string,NSAction>> ();
			_menus.Add (new KeyValuePair<string,NSAction> ("View Admission", () => _controller.ViewAdmission (_locationWithRegistrationSimple)));
			_menus.Add (new KeyValuePair<string,NSAction> ("Edit Admission", () => _controller.EditAdmission (_locationWithRegistrationSimple)));
			//if (!_interfaceStatus.ADT) {
			//	_menus.Add (new KeyValuePair<string,NSAction> ("Edit Admission", () => _controller.EditAdmission (_locationWithRegistrationSimple)));
			//}
			if (AppContext.CurrentUser.Authorized ("MANAGE_SPECIAL_INSTRUCTION")) {
				_menus.Add (new KeyValuePair<string,NSAction> ("Special Instruction", () => _controller.SpecialInstruction (_locationWithRegistrationSimple)));
			}

			var registration = _locationWithRegistrationSimple.registrations.FirstOrDefault ();
			if (registration != null && registration.web_portal_enabled)
				_menus.Add (new KeyValuePair<string,NSAction> ("Web Portal Registration", () => _controller.WebPortalRegistration(_locationWithRegistrationSimple, _locationWithRegistrationSimple.registrations.First ())));

			_menus.Add (new KeyValuePair<string,NSAction> ("Companion Meal Order", () => _controller.CompanionMealCalendar(_locationWithRegistrationSimple, _locationWithRegistrationSimple.registrations.First ())));
			_menus.Add (new KeyValuePair<string,NSAction> ("Patient Calendar", () => _controller.PatientMealCalendar(_locationWithRegistrationSimple, _locationWithRegistrationSimple.registrations.First ())));

			//_menus.Add (new KeyValuePair<string,NSAction> ("Meal Feedback", () => _controller.Feedback (_locationWithRegistrationSimple)));

		}

		private void GenerateMenuToView()
		{
			
			int i = 0;
			int y = 1;
			_menus.ForEach (m => {

				var button = new UIButton();
				button.Frame = new RectangleF(10, (i*_buttonHeight)+(_buttonGap*y),748,_buttonHeight);
				button.SetTitleColor(UIColor.White,UIControlState.Normal);
				button.Font = UIFont.BoldSystemFontOfSize(21f);
				button.BackgroundColor = UIColor.FromRGB(0f/255f, 121f/255f, 206f/255f);

				var gradient = new CAGradientLayer();

				gradient.Colors = new MonoTouch.CoreGraphics.CGColor[]
				{
					UIColor.FromRGB (115, 181, 216).CGColor,
					UIColor.FromRGB (35, 101, 136).CGColor
				};

				gradient.Locations = new NSNumber[]
				{
						.5f,
						1f
				};

				gradient.Frame = button.Layer.Bounds;

				button.Layer.AddSublayer(gradient);

				button.Layer.MasksToBounds = true;
				button.SetTitle(m.Key,UIControlState.Normal);

				button.Layer.CornerRadius = 5.0f;
				button.Layer.MasksToBounds = true;
				button.TouchUpInside += delegate {
					DismissViewController(true, m.Value);
				};
				MenuView.AddSubview(button);
				i++;
				y++;
			});
		}

		private void CalculateHeightView()
		{
			if (_menus != null) {
				_heightView = (_menus.Count * _buttonHeight) + (_buttonGap + _menus.Count) + _buttonHeight;
			}
		}

		private void DisableButton (UIButton button, string message="")
		{
			button.Enabled = false;
			button.SetTitleColor (UIColor.LightGray, UIControlState.Disabled);
			button.SetTitle (String.Format ("{0} {1}", button.Title (UIControlState.Normal), message), UIControlState.Disabled);
		}
		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			//MenuView.SetFrame (height:_heightView, y: 1024 - _heightView);
			//MenuView.Frame.Height = (float)_heightView;
			//this.View.Superview.Bounds = new RectangleF (0, 0, 540, _heightView);
		}
		public override void SetTextsByLanguage()
		{
		}
		partial void exitButtonTouched (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, null);
		}

	}
}

