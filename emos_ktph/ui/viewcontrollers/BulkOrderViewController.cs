﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using VMS_IRIS.Areas.EmosIpad.Models;
using ios;
using System.Globalization;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios
{
	public partial class BulkOrderViewController : BaseViewController, INameQuantityCallback<BulkMealOrderRestrictionGrouping>
	{
		public event EventHandler OnSave;
		private UIBarButtonItem _saveButton;
		private DynamicTableSource<BulkMealOrderRestrictionGrouping, BulkOrderDetailViewCell> _bulkOrderSource;
		private NameQuantityViewCellHandler<BulkMealOrderRestrictionGrouping> _bulkOrderViewCellHandler;
		private BulkMealRestrictionGroupInfo _model;
		private MealPeriodHeaderView _mealPeriodHeaderView;
		private KeyValuePair<string, string> _location;
		private MealOrderPeriodModel _mealPeriod;

		public BulkOrderViewController (IApplicationContext appContext, KeyValuePair<string, string> location, BulkMealRestrictionGroupInfo item, MealOrderPeriodModel mealPeriod) : base (appContext, "BulkOrderViewController", null)
		{
			try {
				_model = item;
				_mealPeriod = mealPeriod;
				_location = location;
				InitiateBulkOrderTable ();
				Title = String.Format ("{0} - Order", location.Value);
				_mealPeriodHeaderView = new MealPeriodHeaderView (appContext);
				ShowOrderTime (item);
				_mealPeriodHeaderView.MealPeriod = mealPeriod.meal_order_period_label;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowOrderTime (BulkMealRestrictionGroupInfo item)
		{
			try {
				var format = "dd/MM/yyyy h:mm tt";
				DateTime dateTime;
				if (!DateTime.TryParseExact (item.order_time, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
					_mealPeriodHeaderView.HeaderTime = DateTime.Today.ToLongDateString ();
				else
					_mealPeriodHeaderView.HeaderTime = dateTime.ToLongDateString ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				SetNavigationBar ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewWillAppear (bool animated)
		{
			try {
				base.ViewWillAppear (animated);
				SetLayout ();
				SetHeader ();
				if (IsNewAdmission())
					_requestHandler.SendRequest (View, RequestStructure);
				else
					ShowBulkOrderTable (_model);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestStructure ()
		{
			try {
				var param = KnownUrls.GetBulkOrderStructureQueryString (_location.Key);
				await AppContext.HttpSender.Request<BulkMealRestrictionGroupInfo> ()
					.From (KnownUrls.GetBulkOrderStructure)
					.WithQueryString (param)
					.WhenSuccess (result => OnRequestBulkOrderStructureSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestBulkOrderStructureSuccessful (BulkMealRestrictionGroupInfo result)
		{
			try {
				OnRequestCompleted (true);
				_model = result;
				ShowBulkOrderTable (result);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetLayout ()
		{
			try {
				HeaderView.AddSubview (_mealPeriodHeaderView);
				_mealPeriodHeaderView.SetFrame (0, 0);
				HeaderView.SetFrame (10, Measurements.TopY + 10);
				var height = UIScreen.MainScreen.Bounds.Bottom - (HeaderView.Frame.Top + HeaderView.Frame.Height + 20) - 10;
				BulkOrderTable.SetFrameBelowTo (HeaderView, 10, HeaderView.Frame.Width, height);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetHeader ()
		{
			try {
				HeaderView.Layer.BorderColor = UIColor.Black.CGColor;
				HeaderView.Layer.BorderWidth = 2.0f;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void InitiateBulkOrderTable ()
		{
			try {
				var setting = new NameQuantityViewCellSetting {
					CellHeight = BulkOrderDetailViewCell.RowHeight,
					AlternateBackground = true,
					QuantityTextHidden = false,
					QuantityLabelHidden = true
				};
				_bulkOrderViewCellHandler = new NameQuantityViewCellHandler<BulkMealOrderRestrictionGrouping> (BulkOrderDetailViewCell.CellIdentifier, this, setting);
				_bulkOrderSource = new DynamicTableSource<BulkMealOrderRestrictionGrouping, BulkOrderDetailViewCell> (_bulkOrderViewCellHandler);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetNavigationBar()
		{
			try {
				NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var buttons = new List<UIBarButtonItem> ();
			try {
				CreateSaveButton ();
				buttons.Add (_saveButton);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
			}
			return buttons;
		}
		private void CreateSaveButton ()
		{
			try {
				var addText = AppContext.LanguageHandler.GetLocalizedString ("Save");
				_saveButton = new UIBarButtonItem (addText, UIBarButtonItemStyle.Done, (sender, args) =>  {
					SaveButton_Click ();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SaveButton_Click ()
		{
			try {
				var saveModel = CreateSaveModel ();
				_requestHandler.SendRequest (View, () => RequestSave (saveModel));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestSave (ConsolidatedMealOrderSaveData model)
		{
			try {
				await AppContext.HttpSender.Request<OperationInfoIpad> ()
					.From (KnownUrls.SaveBulkOrder)
					.WithContent (model)
					.WhenSuccess (result => OnRequestSaveSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestSaveSuccessful (OperationInfoIpad result)
		{
			try {
				var success = result.success && result.executed;
				base.OnRequestCompleted (success);
				if (!success)
					return;
				OnSave.SafeInvoke (this);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private ConsolidatedMealOrderSaveData CreateSaveModel()
		{
			try {
				var result = new ConsolidatedMealOrderSaveData {
					bulk_meal_order_restriction_groups = _model.bulk_meal_restriction_groups,
					order_performed_by = AppContext.CurrentUser.Id,
				};
				if (!IsNewAdmission ())
					result.bulk_meal_order_id = _model.bulk_meal_order_id;
				result.institution_id = AppContext.ServerConfig.InsitutionId;
				result.location_id = long.Parse (_location.Key);
				result.meal_order_period_id = _mealPeriod.meal_order_period_id;
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private bool IsNewAdmission()
		{
			try {
				return _model == null || _model.bulk_meal_order_id == 0;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		private void ShowBulkOrderTable (BulkMealRestrictionGroupInfo bulkOrderInfo)
		{
			try {
				BulkOrderTable.Layer.BorderColor = UIColor.Black.CGColor;
				BulkOrderTable.Layer.BorderWidth = 2.0f;
				BulkOrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				BulkOrderTable.RowHeight = BulkOrderDetailViewCell.RowHeight;
				_bulkOrderSource.Items = bulkOrderInfo.bulk_meal_restriction_groups;
				BulkOrderTable.Source = _bulkOrderSource;
				BulkOrderTable.ReloadData ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		#region INameQuantityCallback<BulkMealOrderRestrictionGrouping> implementation
		public void DetailQuantityChanged (BulkMealOrderRestrictionGrouping item)
		{
			try {
				var detail = _model.bulk_meal_restriction_groups.FirstOrDefault (g => g.bulk_meal_order_id == item.bulk_meal_order_id);
				detail.quantity = item.quantity;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void DetailSelectionChanged (BulkMealOrderRestrictionGrouping item)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public bool GetAllowChangeQuantity ()
		{
			try {
				return true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		#endregion
		#region ICallback implementation
		public void ItemSelected (BulkMealOrderRestrictionGrouping selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		#endregion
	}


}

