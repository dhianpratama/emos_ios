﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using VMS_IRIS.Areas.EmosIpad.Models;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;

namespace emos_ios
{
	public partial class BulkOrderSummaryViewController : BaseViewController, ICrud<BulkMealRestrictionGroupInfo>
	{
		private UIBarButtonItem _addButton;
		private UIBarButtonItem _changeWardButton;
		private BulkOrderSummary _model;
		private KeyValuePair<string,string> _location;
		private CrudViewCellHandler<BulkMealRestrictionGroupInfo> _handler;
		private DynamicTableSource<BulkMealRestrictionGroupInfo, BulkOrderSummaryViewCell> _source;
		private AlternatingTableViewDelegate _tableViewDelegate;
		private MealPeriodHeaderView _mealPeriodHeaderView;
		private CAGradientLayer _gradient;

		public BulkOrderSummaryViewController (IApplicationContext appContext, KeyValuePair<string, string> location) : base (appContext, "BulkOrderSummaryViewController", null)
		{
			try {
				_location = location;
				InitiateBulkOrderTable ();
				Title = String.Format ("{0} - Order", location.Value);
				_mealPeriodHeaderView = new MealPeriodHeaderView (appContext);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				SetNavigationBar ();
				_tableViewDelegate = new AlternatingTableViewDelegate ();
				OrderTable.Delegate = _tableViewDelegate;
				SetLayout ();
				SetHeader ();
				SetTable ();
				AddGradient ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AddGradient ()
		{
			try {
				var container = new UIView (LateOrderLabel.Frame);
				LateOrderLabel.SetFrame (0, 0);
				LateOrderLabel.Opaque = true;
				LateOrderLabel.TextColor = UIColor.White;
				container.AddSubview (LateOrderLabel);
				HeaderView.AddSubview (container);

				_gradient = new CAGradientLayer ();
				_gradient.Frame = container.Bounds;
				_gradient.NeedsDisplayOnBoundsChange = true;
				_gradient.MasksToBounds = true;
				_gradient.Colors = new CGColor[]{ UIColor.FromRGB(122, 170, 6).CGColor, UIColor.DarkGray.CGColor };
				container.Layer.InsertSublayer (_gradient, 0);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewWillAppear (bool animated)
		{
			try {
				base.ViewWillAppear (animated);
				Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetLayout ()
		{
			try {
				HeaderView.AddSubview (_mealPeriodHeaderView);
				_mealPeriodHeaderView.SetFrame (0, 0);
				HeaderView.SetFrame (10, Measurements.TopY + 10);
				var height = UIScreen.MainScreen.Bounds.Bottom - (HeaderView.Frame.Top + HeaderView.Frame.Height + 20) - 10;
				OrderTable.SetFrameBelowTo (HeaderView, 10, HeaderView.Frame.Width, height);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetHeader ()
		{
			try {
				HeaderView.Layer.BorderColor = UIColor.Black.CGColor;
				HeaderView.Layer.BorderWidth = 2.0f;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetTable ()
		{
			try {
				OrderTable.Layer.BorderColor = UIColor.Black.CGColor;
				OrderTable.Layer.BorderWidth = 2.0f;
				OrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				OrderTable.RowHeight = BulkOrderSummaryViewCell.RowHeight;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void InitiateBulkOrderTable ()
		{
			try {
				var setting = new BaseViewCellSetting {
					CellHeight = BulkOrderSummaryViewCell.RowHeight,
					AlternateBackground = true,
				};
				_handler = new CrudViewCellHandler<BulkMealRestrictionGroupInfo> (BulkOrderSummaryViewCell.CellIdentifier, this, setting);
				_source = new DynamicTableSource<BulkMealRestrictionGroupInfo, BulkOrderSummaryViewCell> (_handler);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void Refresh ()
		{
			try {
				_requestHandler.SendRequest (View, RequestViewModel);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetNavigationBar()
		{
			try {
				NavigationItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			return new UIBarButtonItem[1] { CreateBackButton () };
		}
		private UIBarButtonItem CreateBackButton()
		{
			try {
				return new UIBarButtonItem ("Logout", UIBarButtonItemStyle.Done, (sender, args) => {
					AppContext.SignOut ();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons (bool showAdd)
		{
			var buttons = new List<UIBarButtonItem> ();
			try {
				if (showAdd) {
					CreateAddButton ();
					buttons.Add (_addButton);
				}
				CreateChangeWardButton ();
				buttons.Add (_changeWardButton);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
			}
			return buttons;
		}
		private void CreateAddButton ()
		{
			try {
				var addText = AppContext.LanguageHandler.GetLocalizedString ("Add");
				_addButton = new UIBarButtonItem (addText, UIBarButtonItemStyle.Done, (sender, args) =>  {
					AddButton_Click ();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void AddButton_Click ()
		{
			try {
				AppContext.ControllerLoader.BulkOrderDetailLoader.Load (AppContext, _location, new BulkMealRestrictionGroupInfo (), HandleBulkOrderSave, CreateMealPeriod ());
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private MealOrderPeriodModel CreateMealPeriod ()
		{
			try {
				var mealPeriod = new MealOrderPeriodModel {
					meal_order_period_id = long.Parse (_model.MealPeriod.Key),
					meal_order_period_label = _model.MealPeriod.Value,
					for_tomorrow = _model.ForTomorrow
				};
				return mealPeriod;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void HandleOnBulkOrderSave (object sender, EventArgs e)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void CreateChangeWardButton ()
		{
			try {
				var text = AppContext.LanguageHandler.GetLocalizedString ("ChangeWard");
				_changeWardButton = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, (sender, args) =>  {
					ChangeWardButton_Click ();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ChangeWardButton_Click ()
		{
			try {
				var bulkWards = AppContext.WardViewHandler.Wards.Where (w => !w.bulkMealOrder);
				if (!bulkWards.Any ())
					return;
				var items = bulkWards.Select (w => new KeyValuePair<string,string> (w.id.ToString (), w.label)).ToList ();
				ShowTableViewController (items, "Wards");
				TableViewController.DoneClicked += Wards_DoneClicked;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void Wards_DoneClicked (object sender, EventArgs e)
		{
			try {
				if (SelectedKeyValue.Value == null)
					return;
				AppContext.SetGlobalWard (SelectedKeyValue);
				AppContext.LoadMenu (core_emos.Menu.EmosMenu.NurseDashboard);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestViewModel()
		{
			try {
				var param = KnownUrls.GetBulkOrderSummaryQueryString (AppContext.InstitutionId, _location.Key);
				await AppContext.HttpSender.Request<BulkOrderSummary> ()
					.From (KnownUrls.GetBulkOrderSummary)
					.WithQueryString (param)
					.WhenSuccess (result => OnRequestBulkOrderSummarySuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestBulkOrderSummarySuccessful (BulkOrderSummary result)
		{
			try {
				if (result == null) {
					OnRequestCompleted (false);
					return;
				}
				_model = result;
				OnRequestCompleted (true);
				ShowValues (result);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowValues (BulkOrderSummary result)
		{
			try {
				var orderExist = result.MealGroups.Any ();
				var showAddButton = !orderExist || (orderExist && result.MealGroups.All (g => g.processed_order));
				NoOrderLabel.Hidden = orderExist;
				NavigationItem.SetRightBarButtonItems (RightButtons (showAddButton).ToArray (), true);
				_mealPeriodHeaderView.HeaderTime = result.OrderDate.ToLongDateString ();
				LateOrderLabel.Text = String.Format ("Any order made after {0} {1} will be considered as late orders.", result.OrderDate.ToShortDateString (), result.LateOrderTime);
				_mealPeriodHeaderView.MealPeriod = result.MealPeriod.Value;
				OrderTable.Hidden = !result.MealGroups.Any ();
				if (!orderExist)
					return;
				_source.Items = result.MealGroups;
				OrderTable.Source = _source;
				OrderTable.ReloadData ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void ShowNoOrder ()
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void Delete (BulkMealRestrictionGroupInfo item)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void Edit (BulkMealRestrictionGroupInfo item)
		{
			try {
				AppContext.ControllerLoader.BulkOrderDetailLoader.Load (AppContext, _location, item, HandleBulkOrderSave, CreateMealPeriod ());
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void ItemSelected (BulkMealRestrictionGroupInfo item, int index)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleBulkOrderSave (object sender, EventArgs e)
		{
			try {
				this.ShowAlert (AppContext, "ConsolidatedOrderSavedSuccessfully");
				NavigationController.PopToViewController (NavigationController.ViewControllers[0], true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
	}
}

