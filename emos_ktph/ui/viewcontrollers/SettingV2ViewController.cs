﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Linq;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class SettingV2ViewController : BaseViewController
	{
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OnDone;
		public event EventHandler OnCancel;
		private const float RowHeight = 40f;
		private float ElementWidth;
		private const float HeaderLeft = 10;
		private bool _hideDateSetting;
		private UIBarButtonItem _doneButton;
		private UIBarButtonItem _backButton;
		private UIDatePickerButton _datePickerButton;
		private UILabel _switchDayLabel;
		private UILabel _filterWardLabel;
		private DropDownButton _wardDropDown;
		private KeyValuePair<string, string> _previousWard;
		private DateTime _previousOperationDate;

		public SettingV2ViewController (IApplicationContext appContext, bool hideDateSetting = false) : base (appContext, "SettingV2ViewController", null)
		{
			try {
				_hideDateSetting = hideDateSetting;
				TransparentBackground = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				base.DidReceiveMemoryWarning ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				ElementWidth = BackgroundView.Frame.Width - (HeaderLeft * 2);
				TransparentBorderView.Layer.BorderWidth = 1.0f;
				TransparentBorderView.Layer.BorderColor = UIColor.LightGray.CGColor;
				SetNavigationBar ();
				GenerateUI ();
				ShowValues ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public override void ViewWillAppear (bool animated)
		{
			try {
				base.ViewWillAppear (animated);
				AnimateMe (this, TransparentBorderView);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public static void AnimateMe(UIViewController viewController, UIView backgroundView)
		{
			try {
				backgroundView.Frame = new RectangleF (768, Measurements.FrameTop, 406, UIScreen.MainScreen.Bounds.Bottom - Measurements.FrameTop);
				UIView.BeginAnimations ("slideAnimation");
				UIView.SetAnimationDuration (0.5);
				UIView.SetAnimationDelegate (viewController);
				backgroundView.Center = new PointF (UIScreen.MainScreen.Bounds.Right - backgroundView.Frame.Width / 2, Measurements.FrameTop + backgroundView.Frame.Height / 2);
				UIView.CommitAnimations ();
			} catch (Exception) {
				return;
			}
		}
		private void ShowValues ()
		{
			try {
				_previousWard = AppContext.Ward;
				if (!String.IsNullOrEmpty (AppContext.Ward.Key))
					_wardDropDown.SelectedKey = AppContext.Ward.Key;
				if (_hideDateSetting)
					return;
				_previousOperationDate = AppContext.OperationDate;
				_datePickerButton.SelectedDate = AppContext.OperationDate;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateUI()
		{
			try {
				GenerateSwitchDayUI ();
				_filterWardLabel = CreateHeader ("Filter Ward", new RectangleF (HeaderLeft, 0, ElementWidth, RowHeight), 0f);
				if (_hideDateSetting)
					_filterWardLabel.SetFrameBelowTo (NavigationBar);
				else
					_filterWardLabel.SetFrameBelowTo (_datePickerButton);
				_wardDropDown = CreateDropDown ("Filter Ward", _filterWardLabel);
				_wardDropDown.OnListNotInititated += HandleOnWardNotInitiated;
				_wardDropDown._dropDownHandler.ModalParent = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateSwitchDayUI ()
		{
			try {
				if (_hideDateSetting)
					return;
				_switchDayLabel = CreateHeader ("Switch Day", new RectangleF (HeaderLeft, HeaderLeft, ElementWidth, RowHeight), 0f);
				_switchDayLabel.SetFrameBelowTo (NavigationBar);
				GenerateDatePicker ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void GenerateDatePicker ()
		{
			try {
				if (_hideDateSetting)
					return;
				var frame = new RectangleF (HeaderLeft, HeaderLeft, ElementWidth, RowHeight);
				_datePickerButton = UiGenerator.CreateDatePickerButton (AppContext, View, Colors.ViewCellBackground, "Operation Date", frame, _switchDayLabel, limitToSevenDays:true);
				_datePickerButton.DatePickerMode = UIDatePickerMode.Date;
				_datePickerButton.BackgroundColor = UIColor.White;
				_datePickerButton.Layer.BorderColor = UIColor.LightGray.CGColor;
				_datePickerButton.Layer.BorderWidth = 1.0f;
				_datePickerButton.Layer.CornerRadius = 13.0f;
				BackgroundView.AddSubview (_datePickerButton);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private UILabel CreateHeader(string title, RectangleF frame, float distanceToAbove)
		{
			try {
				var label = UiGenerator.CreateHeader (title, frame, Fonts.Header);
				BackgroundView.AddSubview (label);
				return label;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private DropDownButton CreateDropDown (string title, UIView aboveView, float distanceToAbove = 0f)
		{
			try {
				var result = new DropDownButton ("Select", true, true);
				var items = AppContext.WardViewHandler.Wards.Select (w => new KeyValuePair<string,string> (w.id.ToString (), w.label)).ToList ();
				result.Load (AppContext, BackgroundView, title, items);
				result.SelectedKey = AppContext.Ward.Key;
				result.SetFrameBelowTo (aboveView, HeaderLeft, ElementWidth, distanceToAbove: distanceToAbove);
				result.AddToParentView (BackgroundView);
				return result;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		private void HandleOnRequestWardsSuccessful (object sender, EventArgs e)
		{
			try {
				base.OnRequestCompleted (true);
				var items = AppContext.WardViewHandler.Wards.Select (w => new KeyValuePair<string, string> (w.id.ToString (), w.label)).ToList ();
				_wardDropDown.Load (AppContext, BackgroundView, "Select", items);
				_wardDropDown.ShowDropDown ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void HandleOnWardNotInitiated (object sender, EventArgs e)
		{
			try {
				AppContext.WardViewHandler.OnRequestSuccessful += HandleOnRequestWardsSuccessful;
				AppContext.WardViewHandler.OnRequestFailed += (ob, ev) => base.OnRequestCompleted (false);
				_requestHandler.SendRequest (View, () => AppContext.WardViewHandler.RequestInstitutionWards (AppContext.ServerConfig.InsitutionId.Value));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void SetNavigationBar()
		{
			try {
				NavigationBar.TopItem.Title = "Settings";
				NavigationBar.TopItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
				NavigationBar.TopItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
				NavigationBar.SetFrame (width: BackgroundView.Frame.Width);
				AppContext.ColorHandler.PaintNavigationBar (NavigationBar);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var doneText = AppContext.LanguageHandler.GetLocalizedString ("Done");
			_doneButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Done, (sender, args) => {
				DoneButton_Clicked ();
			});
			var buttons = new List<UIBarButtonItem> {
				_doneButton
			};
			return buttons;
		}
		private void DoneButton_Clicked ()
		{
			try {
				if (!Validate ()) {
					this.ShowAlert (AppContext, "MissingRequiredInformation");
					return;
				}
				if (_previousWard.Key == _wardDropDown.SelectedKey && (_hideDateSetting || _previousOperationDate == _datePickerButton.SelectedDate)) {
					OnCancel.SafeInvoke (this);
					Close ();
					return;
				}
				AppContext.SetGlobalWard (_wardDropDown.Selected);
				if (!_hideDateSetting)
					AppContext.OperationDate = _datePickerButton.SelectedDate;
				OnDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string,string>> ().Initiate (_wardDropDown.Selected));
				Close ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private bool Validate ()
		{
			try {
				if (String.IsNullOrEmpty(_wardDropDown.SelectedKey))
					return false;
				if (!_hideDateSetting && _datePickerButton.SelectedDate == default(DateTime))
					return false;
				return true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return false;
			}
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			var backText = AppContext.LanguageHandler.GetLocalizedString ("Back");
			_backButton = new UIBarButtonItem (backText, UIBarButtonItemStyle.Done, (sender, args) => {
				OnCancel.SafeInvoke (this);
				Close ();
			});
			var buttons = new List<UIBarButtonItem> {
				_backButton
			};
			return buttons;
		}
		private void Close()
		{
			try {
				View.RemoveFromSuperview ();
				DismissViewController (false, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
	}
}

