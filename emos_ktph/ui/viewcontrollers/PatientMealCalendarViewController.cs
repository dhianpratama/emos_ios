﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using System.Collections.Generic;
using System.Linq;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios
{
	public partial class PatientMealCalendarViewController : BaseViewController, IDailyPatientMealCallback, IPatientDayOrderCallback, ILock
	{
		private const int DaysCount = 21;
		private const float MealOrderCellHeight = 85f;
		private const float CellWidth = 96f;
		private const float DateHeaderHeight = 66f;
		private DateTime _startDate;
		private LocationModelSimple _location;
		private RegistrationModelSimple _registration;
		private DynamicTableSource<MealOrderPeriodModel, MealOrderPeriodWithImageViewCell> _mealPeriodDataSource;
		private DailyPatientMealCalendarViewCellHandler _orderHandler;
		private CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> _orderSource;
		private List<MealCalendar> _patientMealCalendars;
		private PatientOrderCommand _selectedPatientMealOrderCommand;
		private PatientMealOrderAllPeriod _item;
		private bool _isCompanionMode;
		private bool _isPatient = false;
		private PageTitleAndPatientSummaryView _patientSummaryView;

		public PatientMealCalendarViewController (IApplicationContext appContext, LocationModelSimple location, RegistrationModelSimple registration, bool isCompanionMode, bool isPatient = false) : base (appContext, "PatientMealCalendarViewController", null)
		{
			_location = location;
			_registration = registration;
			_isCompanionMode = isCompanionMode;
			_isPatient = isPatient;

			var setting = new BaseViewCellSetting { CellWidth = CellWidth, CellHeight = MealOrderCellHeight };
			var mealPeriodHandler = new NoCallbackViewCellHandler<MealOrderPeriodModel> (AppContext, MealOrderPeriodWithImageViewCell.CellIdentifier, setting);
			_mealPeriodDataSource = new DynamicTableSource<MealOrderPeriodModel, MealOrderPeriodWithImageViewCell> (mealPeriodHandler);
			var dailyPatientMealCalendarViewCellSetting = new DailyPatientMealCalendarViewCellSetting {
				CellWidth = setting.CellWidth,
				ChildMealViewCellHeight = MealOrderCellHeight,
				HeaderHeight = DateHeaderHeight
			};
			_orderHandler = new DailyPatientMealCalendarViewCellHandler (DailyMealOrderCollectionViewCell.CellIdentifier, this, dailyPatientMealCalendarViewCellSetting);
			_orderSource = new CollectionSource<MealCalendar, DailyMealOrderCollectionViewCell> (_orderHandler);
			_startDate = GetLastWeekMonday ();
		}
		private DateTime GetLastWeekMonday ()
		{
			var lastWeek = AppContext.OperationDate.AddDays (-7);
			int offset = lastWeek.DayOfWeek - DayOfWeek.Monday;
			return lastWeek.AddDays (-offset);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationItem.SetLeftBarButtonItems (LeftButtons().ToArray(), false);
			SetLayout ();
			BodyView.BackgroundColor = Colors.ViewCellBackground;
			MealPeriodTable.RowHeight = MealOrderPeriodWithImageViewCell.CellHeight;
			MealPeriodTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			PatientOrderCollectionView.RegisterNibForCell(DailyMealOrderCollectionViewCell.Nib, new NSString (DailyMealOrderCollectionViewCell.CellIdentifier));
			SetHeader ();
			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear(animated);
			UnlockRegistration ();
			Refresh ();
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			var doneButton = new UIBarButtonItem ("Back", UIBarButtonItemStyle.Done, (sender, args) => {
				AppContext.LoadMenu(Menu.EmosMenu.NurseDashboard);
			});
			var buttons = new UIBarButtonItem[] {
				 new UIBarButtonItem (" ", UIBarButtonItemStyle.Done, (sender, args) => {
				})
			};

			if (_isPatient)
				return buttons;

			buttons = new UIBarButtonItem[] {
				doneButton
			};

			return buttons;
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var refreshButton = new UIBarButtonItem (UIBarButtonSystemItem.Refresh, (sender, args) => {
				Refresh();
			});
			var mode = _isCompanionMode ? "GoToPatientOrder" : "GoToCompanionOrder";
			var modeButton = new UIBarButtonItem (AppContext.LanguageHandler.GetLocalizedString (mode), UIBarButtonItemStyle.Plain, (sender, args) => {
				_isCompanionMode = !_isCompanionMode;
				SetHeaderImage ();
				NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
				Refresh ();
			});
			var infoButton = CreateInfoButton ();
			if (_isPatient) {
				var gotoNurse = AppContext.LanguageHandler.GetLocalizedString ("GoToNurseMode");
				var gotoNurseModeButton = new UIBarButtonItem (gotoNurse, UIBarButtonItemStyle.Done, (sender, args) => {
					AppContext.LoadAuthentication(HandleOnAuthenticationSuccess);
				});
				return new UIBarButtonItem[] {
					infoButton, gotoNurseModeButton, NavigationBarHelper.CreateSeparatorBarButton (), refreshButton, NavigationBarHelper.CreateSeparatorBarButton (), modeButton
				};
			} else {
				var gotoPatient = AppContext.LanguageHandler.GetLocalizedString ("GoToPatientMode");
				var gotoPatientModeButton = new UIBarButtonItem (gotoPatient, UIBarButtonItemStyle.Done, (sender, args) => {
					ApplyPatientMode (true);
				});
				return new UIBarButtonItem[] {
					infoButton, gotoPatientModeButton, NavigationBarHelper.CreateSeparatorBarButton (), refreshButton, NavigationBarHelper.CreateSeparatorBarButton (), modeButton
				};
			}
		}
		private UIBarButtonItem CreateInfoButton()
		{
			return new UIBarButtonItem (UIImage.FromFile ("Images/info-icon-white-22x22.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
				AppContext.ControllerLoader.PatientInfoLoader.Load (AppContext);
			});
		}
		private void HandleOnAuthenticationSuccess (object sender, EventArgs e)
		{
			ApplyPatientMode (false);
			AppContext.LoadMenu(Menu.EmosMenu.NurseDashboard);
		}
		private void ApplyPatientMode (bool patientMode)
		{
			_isPatient = patientMode;
			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			NavigationItem.SetLeftBarButtonItems (LeftButtons().ToArray(), true);

		}
		private void SetHeader ()
		{
			_patientSummaryView = new PageTitleAndPatientSummaryView (AppContext, _location, _registration);
			SetHeaderImage ();
			HeaderView.AddSubview (_patientSummaryView);

		}
		void SetHeaderImage ()
		{
			if (_isCompanionMode)
				_patientSummaryView.SetHeaderImage (UIImage.FromFile ("Images/companion_meal_order_header.png"));
			else
				_patientSummaryView.SetHeaderImage (UIImage.FromFile ("Images/patient_meal_order_header.png"));
		}
		private const float LabelsHeight = 37f;
		private void SetLayout ()
		{
			HeaderView.SetFrame (y: Measurements.TopY, height: Measurements.CompanionMealOrderHeaderHeight);
			BodyView.Hidden = true;
			BodyView.SetFrameBelowTo (HeaderView, height: Measurements.PageHeight - Measurements.CompanionMealOrderHeaderHeight, distanceToAbove: 0);
			MealPeriodTable.SetFrame (0, DateHeaderHeight, CellWidth, 0);
		}
		private float CalculateTableHeight ()
		{
			if (_item == null)
				return 0;
			return _item.mealPeriods.Count * MealOrderCellHeight;
		}
		private float CalculateTableWidth ()
		{
			return DaysCount * _orderHandler.BaseViewCellSetting.CellWidth;
		}
		private async void RequestPatientMealCalendar()
		{
			var param = KnownUrls.GetPatientCalendarQueryString (AppContext.InstitutionId, AppContext.HospitalId, _registration.registration_id, AppContext.MealPeriodGroupCode, _startDate, DaysCount);
			await AppContext.HttpSender.Request<PatientMealOrderAllPeriod> ()
				.From (KnownUrls.GetPatientCalendar)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestPatientMealOrderSuccessful(result))
				.WhenFail (result => OnRequestCompleted (false, AppContext.LanguageHandler.GetLocalizedString ("Requestdashboard")))
				.Go ();
		}
		private async void RequestCompanionMealCalendar()
		{
			var param = KnownUrls.GetPatientCalendarQueryString (AppContext.InstitutionId, AppContext.HospitalId, _registration.registration_id, AppContext.MealPeriodGroupCode, _startDate, DaysCount);
			await AppContext.HttpSender.Request<PatientMealOrderAllPeriod> ()
				.From (KnownUrls.GetCompanionCalendar)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestPatientMealOrderSuccessful(result))
				.WhenFail (result => OnRequestCompleted (false, AppContext.LanguageHandler.GetLocalizedString ("Requestdashboard")))
				.Go ();
		}
		private void OnRequestPatientMealOrderSuccessful(PatientMealOrderAllPeriod result)
		{
			_item = result;
			OnRequestCompleted (true);
			var tableHeight = CalculateTableHeight ();
			ShowMealPeriods (result, tableHeight);
			ShowOrders (result, tableHeight + DateHeaderHeight);
			BodyView.Hidden = false;
		}
		private void ShowMealPeriods (PatientMealOrderAllPeriod result, float tableHeight)
		{
			_mealPeriodDataSource.Items = result.mealPeriods;
			MealPeriodTable.Source = _mealPeriodDataSource;
			MealPeriodTable.RowHeight = MealOrderCellHeight;
			MealPeriodTable.SetFrame (height: tableHeight);
			MealPeriodTable.ReloadData ();
		}
		private void ShowOrders (PatientMealOrderAllPeriod result, float tableHeight)
		{
			PatientOrderCollectionView.SetFrame (MealPeriodTable.Frame.Width, 0, 768 - MealPeriodTable.Frame.Width, height: tableHeight);
			OrderCollectionViewFlowLayout.ItemSize = new SizeF (CellWidth, tableHeight);
			_patientMealCalendars = CreateMealCalendars (result, ConsumerTypes.Patient);
			_orderSource.Items = _patientMealCalendars;
			PatientOrderCollectionView.Source = _orderSource;
			PatientOrderCollectionView.ReloadData ();
			PatientOrderCollectionView.ScrollToItem (NSIndexPath.FromItemSection (7, 0), UICollectionViewScrollPosition.Left, false);
		}
		private List<MealCalendar> CreateMealCalendars (PatientMealOrderAllPeriod patientMealOrders, ConsumerTypes consumerType)
		{
			var results = new List<MealCalendar> ();
			for (int i = 0; i < DaysCount; i++) {
				var operationDate = _startDate.AddDays (i);
				var patientOrderCommands = new List<PatientOrderCommand> ();
				for (int mealPeriodIndex = 0; mealPeriodIndex < patientMealOrders.mealPeriods.Count; mealPeriodIndex++) {
					var mealPeriodIndexInList = (DaysCount * mealPeriodIndex) + i;
					var mealOrder = patientMealOrders.bed.registrations.First ().mealOrderPeriods [mealPeriodIndexInList];
					patientOrderCommands.Add (PatientOrderCommandViewCellHandler.CreatePatientOrderCommand (AppContext, mealPeriodIndex, _location, _registration, consumerType, mealOrder));
				}
				var patientMealCalendar = new MealCalendar {
					OrderDate = operationDate,
					PatientOrderCommands = patientOrderCommands
				};
				results.Add (patientMealCalendar);
			}
			return results;
		}
		public void OrderButtonClicked (PatientOrderCommand item)
		{
			if (!OperationCodeHandler.IsClickableOnDashboard (item.OperationCode))
				return;
			if (!item.Registration.canDoMealOrder) {
				this.ShowAlert (AppContext, AppContext.LanguageHandler.GetLocalizedString(LanguageKeys.DietOrderNotSet), null, false);
				return;
			}
			AppContext.SelectedLocation = item.Location;
			AppContext.SelectedRegistration = item.Registration;
			AppContext.SelectedMealOrderPeriod = item.MealOrderPeriod;
			AppContext.SelectedOperationCode = item.OperationCode;
			_selectedPatientMealOrderCommand = item;
			if (Settings.CheckLock && !AppContext.LockHandler.LockResult.free_to_access) {
				CheckLock (LockRequestCode.MealOrder);
			} else {
				LoadMealOrderController ();
			}
		}
		private void LoadMealOrderController ()
		{
			if (_isCompanionMode)
				AppContext.LoadCompanionMealOrder (false, false, _isPatient, _selectedPatientMealOrderCommand);
			else {
				if (_isPatient)
					AppContext.LoadPatientMealOrder (false, false, Dashboard.DashboardMenu.PatientMealOrder, _isPatient, _selectedPatientMealOrderCommand, HandleOnMealOrderClose, HandleOnMealOrderSave, this, true);
				else
					AppContext.LoadNurseMealOrder (false, false, Dashboard.DashboardMenu.PatientMealOrder, _isPatient, _selectedPatientMealOrderCommand, HandleOnMealOrderClose, HandleOnMealOrderSave, this, true);
			}
		}
		private void HandleOnMealOrderClose (object sender, EventArgs e)
		{
			PopToViewControllerAndUnlock ();
		}
		private void HandleOnMealOrderSave (object sender, PatientOrderCommandEventArgs e)
		{
			PopToViewControllerAndUnlock ();
		}
		public void PopToViewControllerAndUnlock(bool unlock = true, bool animated = true)
		{
			UnlockRegistration ();
			if (_isPatient) {
				NavigationController.PopToViewController (NavigationController.ViewControllers[2], true);
			} else {
				NavigationController.PopToViewController (NavigationController.ViewControllers[0], true);
			}
		}
		public void ItemSelected (MealCalendar selected, int selectedIndex)
		{
		}
		public void PatientNameClicked (LocationWithRegistrationSimple location)
		{
			throw new NotImplementedException ();
		}
		public void IdClicked (LocationWithRegistrationSimple location)
		{
			throw new NotImplementedException ();
		}
		public void Refresh ()
		{

			if (_isCompanionMode)
				_requestHandler.SendRequest (View, RequestCompanionMealCalendar);
			else
				_requestHandler.SendRequest (View, RequestPatientMealCalendar);
		}
		public void OnDashboardChanged (Dashboard.OrderTabs tab, bool refresh = true)
		{
			throw new NotImplementedException ();
		}
		public void ShowWardSelectionAlert ()
		{
			throw new NotImplementedException ();
		}
		public void OpenBirthdayCakeForm (LocationWithRegistrationSimple locationWithRegistration)
		{
			throw new NotImplementedException ();
		}
		public void CloseBirthdayCakeForm ()
		{
			throw new NotImplementedException ();
		}
		public void ItemSelected(LocationWithRegistrationSimple selected, int selectedIndex)
		{
			throw new NotImplementedException ();
		}
		private void UnlockRegistration ()
		{
			AppContext.LockHandler.UnlockRegistration ();
		}
		private void CheckLock (LockRequestCode lockRequestCode)
		{
			AppContext.LockHandler.LockRegistration (this, _registration.registration_id.Value, lockRequestCode, _requestHandler, View);
		}
		public void PatientMenuLockSuccessful (LockResultIpad result)
		{
		}
		public void PatientMenuLockFailed (LockResultIpad result)
		{
		}
		public void MealOrderLockSuccessful (LockResultIpad result)
		{
			LoadMealOrderController ();
		}
		public void MealOrderLockFailed (LockResultIpad result)
		{
			var message = String.Format ("Patient {0} has been locked by [{1}] on {2}.", _registration.profile_name, result.locked_by, result.lock_start_at_verbose);
			this.ShowAlert (AppContext, message, null, false);
		}
		public void OnLockRequestCompleted(bool success, string failedTitle = "", bool showFailedTitle = true)
		{
			base.OnRequestCompleted (success, failedTitle, showFailedTitle);
		}
		public void UnlockSuccessful ()
		{
		}
		public void UnlockFailed ()
		{
		}
		public void OnUnlockRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
		}
		public void OnLockNotExtended ()
		{
			AppContext.LoadMenu (core_emos.Menu.EmosMenu.RestrictedPatientMealOrder, showMenu: false);
		}
		public void OnUnlockLocksByUserRequestCompleted(bool success)
		{
			base.OnRequestCompleted (success);
		}
		public void SetOrderBy (string orderBy)
		{
		}
	}
}