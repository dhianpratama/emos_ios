// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("BulkOrderSummaryViewController")]
	partial class BulkOrderSummaryViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LateOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LineView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealPeriodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView OrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TimeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (LateOrderLabel != null) {
				LateOrderLabel.Dispose ();
				LateOrderLabel = null;
			}

			if (LineView != null) {
				LineView.Dispose ();
				LineView = null;
			}

			if (MealPeriodLabel != null) {
				MealPeriodLabel.Dispose ();
				MealPeriodLabel = null;
			}

			if (NoOrderLabel != null) {
				NoOrderLabel.Dispose ();
				NoOrderLabel = null;
			}

			if (OrderTable != null) {
				OrderTable.Dispose ();
				OrderTable = null;
			}

			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
		}
	}
}
