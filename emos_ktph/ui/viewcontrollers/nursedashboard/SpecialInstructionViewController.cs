﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Dialog;

namespace emos_ktph
{
	public partial class SpecialInstructionViewController : BaseViewController, ISpecialInstructionCallback, ISpecialInstructionSelectionCallback, ICallback<SpecialInstructionV2Detail>, ISpecialInstructionrEntryCallback
	{
		public IPatientMenu PatientMenuCallback { get; set; }
		private InterfaceStatusModel _interfaceStatus;
		private LocationWithRegistrationSimple _location;
		private IApplicationContext _appContext;
		private ExtraOrderV2Model _items;
		public RootElement _patientClassOption;
		public SpecialInstructionSelectionViewController _selectionView;
		private MultipleSelectionDataSource<SpecialInstructionV2Detail, SpecialInstructionEntryViewCell> _dataSource;

		public SpecialInstructionViewController (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location, InterfaceStatusModel status) : base (appContext, "SpecialInstructionViewController", null)
		{
			try {
				PatientMenuCallback = callback;
				_location = location;
				_interfaceStatus = status;
				_appContext = appContext;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				// Perform any additional setup after loading the view, typically from a nib.
				InitiateSpecialInstructionDataSource();
				_requestHandler.SendRequest (View, RequestExtraOrderV2Model);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void ItemSelected (SpecialInstructionV2Detail selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestExtraOrderV2Model()
		{
			try {
				var queryString = KnownUrls.RegistrationIdQueryString ((long)_location.registrations.FirstOrDefault().registration_id);
				await _appContext.HttpSender.Request<ExtraOrderV2Model>()
					.From (KnownUrls.GetExtraOrderV2Model)
					.WithQueryString(queryString)
					.WhenSuccess (result => OnRequestSpecialInstructionSuccesful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestSpecialInstructionSuccesful(ExtraOrderV2Model result)
		{
			try {
				OnRequestCompleted (true);
				_items = result;
				if (_items.allSpecialInstructions.Count > 0)
					setupHeaderView();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void setupSelectionView(){
			try {
				_selectionView = new SpecialInstructionSelectionViewController(_appContext, this, _items);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void InitiateSpecialInstructionDataSource ()
		{
			try {
				var setting = new BaseViewCellSetting ();
				var handler = new BaseViewCellHandler<SpecialInstructionV2Detail> (SpecialInstructionEntryViewCell.CellIdentifier, this, setting) {
					GetHeightForRow = GetHeightForRow,
					EpicIsUp = _interfaceStatus.DIET_ORDERS
				};
				_dataSource = new MultipleSelectionDataSource<SpecialInstructionV2Detail, SpecialInstructionEntryViewCell> (handler) {
					ReloadOnSelectionChanged = true
				};
				_dataSource.OnItemDeselected += OnExtraOrderDeselected;
				_dataSource.OnItemSelected += OnExtraOrderSelected;
				SpecialInstructionTableView.Source = _dataSource;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnExtraOrderSelected (object sender, RowSelectionEventArgs<SpecialInstructionV2Detail> e)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnExtraOrderDeselected (object sender, RowSelectionEventArgs<SpecialInstructionV2Detail> e)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private float GetHeightForRow (SpecialInstructionV2Detail item)
		{
			try {
				return SpecialInstructionEntryViewCell.CalculateRowHeight (item);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return 0;
			}
		}
		private void setupHeaderView(){
			try {
				SpecialInstructionTitleLabel.SetFrame (y: (Settings.ShowAdditionalPatientMenuLinks) ? Measurements.TopY : Measurements.TopY);
				SpecialInstructionTitleLabel.SetFrame (height: 0);
				var headerDVC = new SpecialInstructionHeaderDVC (AppContext, PatientMenuCallback, _location, Title, this, _items);
				headerDVC.View.SetFrameBelowTo (SpecialInstructionTitleLabel, 0, 768, 380, 0);
				headerDVC.Root.Add (new Section (_appContext.LanguageHandler.GetLocalizedString ("Special Instruction")) {
					_patientClassOption
				});
				View.AddSubview (headerDVC.View);
				SpecialInstructionTableView.SetFrameBelowTo (headerDVC.View, height: 1024-380-Measurements.TopY, distanceToAbove: 0);

				_dataSource.Items = _items.specialInstructions;
				SpecialInstructionTableView.ReloadData ();

				if (_items.allSpecialInstructions.Count () > 0)
					setNavigationRightButton ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void setNavigationRightButton(){
			try {
				var saveButton = new UIBarButtonItem("Save", UIBarButtonItemStyle.Done, delegate {
					OnSaveButtonClicked();
				});
				saveButton.TintColor = UIColor.White;
				NavigationItem.SetRightBarButtonItem (saveButton, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnSaveButtonClicked(){
			try {
				var saveModel = GenerateSaveModel ();
				//if (!ValidateInput (saveModel))
				//return;

				_requestHandler.SendRequest (View, () => RequestSave (saveModel));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private async void RequestSave(SaveExtraOrderV2ModelIpad saveModel)
		{
			try {
				await AppContext.HttpSender.Request<OperationInfoIpad>()
					.From (KnownUrls.SaveExtraOrderV2)
					.WithContent(saveModel)
					.WhenSuccess (result => OnRequestSaveSuccesful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnRequestSaveSuccesful(OperationInfoIpad result)
		{
			try {
				if (result.success) {
					OnRequestCompleted (true);
					this.ShowAlert (AppContext, "Savedsuccessful");
				} else
					OnRequestCompleted (false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private SaveExtraOrderV2ModelIpad GenerateSaveModel()
		{
			try {
				var saveModel = new SaveExtraOrderV2ModelIpad () {
					registrationId = (long)_location.registrations.FirstOrDefault().registration_id,
					extraOrder = _items.specialInstructions 
				};
				return saveModel;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return null;
			}
		}
		//callback
		public void AddButtonClicked(){
			try {
				setupSelectionView();
				UIApplication.SharedApplication.KeyWindow.Add(_selectionView.View);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void ExitButtonClicked(){
			try {
				_selectionView.View.RemoveFromSuperview ();
				_selectionView.View = null;
				_selectionView = null;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void UpdateTableView(List<SpecialInstructionV2Detail> detail) {
			try {
				ExitButtonClicked();
				var oldSpecialInstructions = _items.specialInstructions;

				foreach(var r in detail){
					if (r.specialInstruction.active_data && !oldSpecialInstructions
						.Any (e => e.specialInstruction.id == r.specialInstruction.id)) {
						r.mealPeriods.ForEach (mp => {
							mp.selected = false;
							mp.quantities = 0;
						});
						oldSpecialInstructions.Add (r);
					}else if(!r.specialInstruction.active_data && oldSpecialInstructions
						.Any(e => e.specialInstruction.id == r.specialInstruction.id)){
						var toBeDeleted = oldSpecialInstructions.FirstOrDefault(a => a.specialInstruction.id == r.specialInstruction.id);
						oldSpecialInstructions.Remove(toBeDeleted);
					}
				}

				_items.specialInstructions = oldSpecialInstructions.ToList();
				_dataSource.Items = _items.specialInstructions;
				SpecialInstructionTableView.ReloadData ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		#region ISpecialInstructionEntryCallback
		public void DetailSelected (ExtraOrderMealPeriodModelSimple selected){}
		public void DetailDeselected (ExtraOrderMealPeriodModelSimple deselected){}
		public void DetailSelected (SpecialInstructionV2Detail selected){}
		public void DetailDeselected (SpecialInstructionV2Detail deselected){}
		public void ValueChanged (SpecialInstructionV2Detail model){
			try {
				var update = _items.specialInstructions.FirstOrDefault (r => r.specialInstruction.id == model.specialInstruction.id);
				update.mealPeriods = model.mealPeriods;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void SelectionChanged (SpecialInstructionV2Detail model, bool Reload = false){}

		public void SelectionDeleted(SpecialInstructionV2Detail model){
			try {
				var toBeDeleted = _items.specialInstructions.FirstOrDefault(r => r.specialInstruction.id == model.specialInstruction.id);
				_items.specialInstructions.Remove (toBeDeleted);
				_dataSource.Items = _items.specialInstructions;
				SpecialInstructionTableView.ReloadData ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		#endregion
	}
}

