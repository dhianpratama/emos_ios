// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ktph
{
	[Register ("SpecialInstructionViewController")]
	partial class SpecialInstructionViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView SpecialInstructionTableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SpecialInstructionTitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (SpecialInstructionTitleLabel != null) {
				SpecialInstructionTitleLabel.Dispose ();
				SpecialInstructionTitleLabel = null;
			}

			if (SpecialInstructionTableView != null) {
				SpecialInstructionTableView.Dispose ();
				SpecialInstructionTableView = null;
			}
		}
	}
}
