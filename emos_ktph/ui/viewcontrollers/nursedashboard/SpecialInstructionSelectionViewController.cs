﻿
using System;
using System.Drawing;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;

namespace emos_ktph
{
	public partial class SpecialInstructionSelectionViewController : BaseViewController, ICallback<SpecialInstructionV2Detail>, ISpecialInstructionrEntryCallback
	{		
		private ISpecialInstructionSelectionCallback _callback;
		private ExtraOrderV2Model _extraOrder;
		private MultipleSelectionDataSource<SpecialInstructionV2Detail, SelectionViewCell> _dataSource;

		public SpecialInstructionSelectionViewController (IApplicationContext appContext, ISpecialInstructionSelectionCallback callback, ExtraOrderV2Model model) : base (appContext, "SpecialInstructionSelectionViewController", null)
		{
			try {
				_callback = callback;
				_extraOrder = model;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				InitiateExtraOrderDataSource ();
				siTableView.AllowsMultipleSelection = true;
				siHeaderView.BackgroundColor = Colors.MainThemeColor;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void InitiateExtraOrderDataSource ()
		{
			try {
				var setting = new BaseViewCellSetting ();
				var handler = new BaseViewCellHandler<SpecialInstructionV2Detail> (SelectionViewCell.CellIdentifier, this, setting) {
					GetHeightForRow = GetHeightForRow,
					EpicIsUp = false
				};
				_dataSource = new MultipleSelectionDataSource<SpecialInstructionV2Detail, SelectionViewCell> (handler) {
					ReloadOnSelectionChanged = true
				};
				_dataSource.OnItemDeselected += OnExtraOrderDeselected;
				_dataSource.OnItemSelected += OnExtraOrderSelected;
				_dataSource.Items = _extraOrder.allSpecialInstructions.ToList() ;

				siTableView.Source = _dataSource;

				int i = 0;
				var selectedRows = new List<int> ();
				_extraOrder.allSpecialInstructions.ForEach(r =>{
					r.specialInstruction.active_data = false;
					if(_extraOrder.specialInstructions
						.Any(e => e.specialInstruction.id == r.specialInstruction.id)){
						selectedRows.Add(i);
						r.specialInstruction.active_data = true;
					}
					i++;
				}
				);

				_dataSource.SelectRows (selectedRows, siTableView);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void DetailSelectionChanged(SpecialInstructionV2Detail item)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		#region MultipleSelectionDataSource Callback
		private void OnExtraOrderSelected (object sender, RowSelectionEventArgs<SpecialInstructionV2Detail> e)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private void OnExtraOrderDeselected (object sender, RowSelectionEventArgs<SpecialInstructionV2Detail> e)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		private float GetHeightForRow (SpecialInstructionV2Detail item)
		{
			return 40.0f;
		}
		#endregion
		#region ICallback implementation
		public void ItemSelected (SpecialInstructionV2Detail selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		#endregion
		#region ISpecialInstructionEntryCallback 
		public void DetailSelected (SpecialInstructionV2Detail selected){
			try {
				_extraOrder.allSpecialInstructions.FirstOrDefault(r => r.specialInstruction.id == selected.specialInstruction.id).specialInstruction.active_data = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void DetailDeselected (SpecialInstructionV2Detail deselected){
			try {
				_extraOrder.allSpecialInstructions.FirstOrDefault(r => r.specialInstruction.id == deselected.specialInstruction.id).specialInstruction.active_data = false;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void DetailSelected (ExtraOrderMealPeriodModelSimple selected){
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void DetailDeselected (ExtraOrderMealPeriodModelSimple deselected){
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void ValueChanged (SpecialInstructionV2Detail model){
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void SelectionChanged (SpecialInstructionV2Detail model, bool Reload = false){
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		public void SelectionDeleted (SpecialInstructionV2Detail model){
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
		#endregion

		partial void siExitButtonClicked (MonoTouch.Foundation.NSObject sender){
			try {
				_callback.ExitButtonClicked();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}

		partial void submitButtonClicked (MonoTouch.Foundation.NSObject sender){
			try {
				_callback.UpdateTableView(_extraOrder.allSpecialInstructions);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ErrorOccured");
				return;
			}
		}
	}
}

