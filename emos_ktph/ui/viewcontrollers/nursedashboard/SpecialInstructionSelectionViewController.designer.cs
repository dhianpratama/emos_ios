// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ktph
{
	[Register ("SpecialInstructionSelectionViewController")]
	partial class SpecialInstructionSelectionViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView siBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView siContentView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView siHeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView siTableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton submitButton { get; set; }

		[Action ("siExitButtonClicked:")]
		partial void siExitButtonClicked (MonoTouch.Foundation.NSObject sender);

		[Action ("submitButtonClicked:")]
		partial void submitButtonClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (siBackgroundView != null) {
				siBackgroundView.Dispose ();
				siBackgroundView = null;
			}

			if (siContentView != null) {
				siContentView.Dispose ();
				siContentView = null;
			}

			if (siHeaderView != null) {
				siHeaderView.Dispose ();
				siHeaderView = null;
			}

			if (siTableView != null) {
				siTableView.Dispose ();
				siTableView = null;
			}

			if (submitButton != null) {
				submitButton.Dispose ();
				submitButton = null;
			}
		}
	}
}
