﻿using System;

namespace core_emos
{
	public class Menu
	{
		public enum EmosMenu
		{
			NurseDashboard, 
			PatientMealOrder, 
			CompanionMealOrder, 
			Setting, 
			Login, 
			IoCharting, 
			About, 
			RestrictedPatientMealOrder, 
			DishLanguage, 
			RestrictedCompanionMealOrder,
			FeedbackDashboard
		}
	}
}

