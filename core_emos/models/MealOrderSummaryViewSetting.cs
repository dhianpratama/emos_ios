﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class MealOrderSummaryViewSetting
	{
		public IMealOrderCallback Controller { get; set; }
		public MealOrderModel MealOrderModel  { get; set; }
		public IEnumerable<DishModelSimple> SelectedDishes  { get; set; }
		public bool IsViewOnly  { get; set; }
		public bool IsPatient   { get; set; }
		public bool IsCompanionMeal   { get; set; }
		public bool CurrentAndNextMealPeriodExtraOrder  { get; set; }
		public bool ExtraOrderQuantityInMealOrderHidden  { get; set; }
		public int MaximumRemarkLength { get; set; }
		public long SelectedTagId { get; set; }
	}
}

