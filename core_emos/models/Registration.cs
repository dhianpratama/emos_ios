﻿using System;
using System.Collections.Generic;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios.models
{
	public class Registration
	{
		public Registration ()
		{
		}
	}

	public class LocationWithRegistration
	{
		public long location_id { get; set; }
		public long? institution_id { get; set; }
		//public HierarchyId node { get; set; }
		public string node_string { get; set; }
		public short level { get; set; }
		public string code { get; set; }
		public string label { get; set; }
		public LocationCompactModel ancestor_location { get; set; }
	}

	public class RegistrationEMOSModel : RegistrationCompactModel
	{
		public ProfileModel profile { get; set; }
		public LocationCompactModel location { get; set; }
		public ClassModel _class { get; set; }
		public List<ProfileAllergyDisplayModel> profileAllergies { get; set; }
		public List<RestrictionModel> therapeuticRestrictions { get; set; }
		public List<RestrictionModel> nbmRestrictions { get; set; }
		public List<RestrictionModel> clearFeedRestrictions { get; set; }
		public List<RestrictionModel> fullFeedRestrictions { get; set; }
		public List<MealOrderPeriodModel> mealPeriods { get; set; }
		public List<ProfileTrialDisplayModel> trialRestrictions { get; set; }
	}

	public class RegistrationCompactModel
	{
		public RegistrationCompactModel()
		{
			extension_properties = new Dictionary<string, string>();
		}

		public long registration_id { get; set; }
		public long? profile_id { get; set; }
		public long? institution_id { get; set; }
		public string registration_number { get; set; }
		public bool active_registration { get; set; }
		public DateTime? registration_time { get; set; }
		public string registration_time_verbose { get; set; }
		public bool finished_registration { get; set; }
		public DateTime? finished_registration_time { get; set; }
		public bool cancelled_registration { get; set; }
		public DateTime? cancelled_registration_time { get; set; }
		public bool planned_registration { get; set; }
		public DateTime? planned_registration_create_time { get; set; }
		public DateTime? planned_arrival_time { get; set; }
		public bool planned_finished_registration { get; set; }
		public DateTime? planned_finished_registration_create_time { get; set; }
		public DateTime? planned_finish_registration_time { get; set; }
		public bool deprecated { get; set; }
		public bool manual_registration { get; set; }
		public bool sap_registration { get; set; }
		public Dictionary<string, string> extension_properties { get; set; }
		public long? location_id { get; set; }
		//public long? location_structure_id { get; set; }
		public long? class_id { get; set; }

//		public long? GetLocationWardGroupId()
//		{
//			var locationStructure = extension_properties.Get("ASSIGNED_WARD_GROUP", string.Empty);
//			long locationStructureId;
//			var locationStructureSuccess = Int64.TryParse(locationStructure, out locationStructureId);
//			if (locationStructureSuccess)
//				return locationStructureId;
//			return null;
//		}
	}

	public class RegistrationModel : RegistrationCompactModel
	{
		public ProfileModel profile { get; set; }
//		public InstitutionModel institution { get; set; }
		public LocationCompactModel location { get; set; }
		//public LocationStructureCompactModel locationStructure { get; set; }
		public ClassModel _class { get; set; }
	}

	public class RegistrationSaveModel : RegistrationCompactModel
	{
		public OperationInfo operationInfo { get; set; }
	}
}

