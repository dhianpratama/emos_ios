﻿using System;

namespace emos_ios.models
{
	public class SalutationModel
	{
		public long salutation_id { get; set; }
		public string salutation_code { get; set; }
		public string salutation_label { get; set; }
	}
}

