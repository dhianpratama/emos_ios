﻿using System;

namespace emos_ios
{
	public class OperationCodeStatus
	{
		public enum OperationCodes : long
		{
			Order = 1,
			ViewOrder = 2,
			ChangeOrder = 4,
			Ordered = 5,
			ViewDraft = 11,
			Decline = 14
		}

		public string Status { get; set; }
		public string ImagePath { get; set; }
	}
}
