namespace emos_ios.models
{
	public class Operation
	{
		public long operation_id { get; set; }
		public string operation_code { get; set; }
		public string operation_label { get; set; }
	}
}

