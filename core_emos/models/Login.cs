﻿using System;

namespace emos_ios.models
{
	public class Login
	{
		public bool executed { get; set; }
		public bool logged_in { get; set; }
		public bool success { get; set; }
	}
	public class LoginContent
	{
		public string user_name { get; set; }
		public string password { get; set; }
	}
}

