﻿using System;
using MonoTouch.UIKit;

namespace core_emos
{
	public class RectangleBorder
	{
		public UIView Left { get; set; }
		public UIView Right { get; set; }
		public UIView Top { get; set; }
		public UIView Bottom { get; set; }

		public RectangleBorder (UIView left, UIView right, UIView top, UIView bottom)
		{
			Left = left;
			Right = right;
			Top = top;
			Bottom = bottom;
		}
	}
}

