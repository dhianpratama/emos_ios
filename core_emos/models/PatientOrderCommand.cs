using VMS_IRIS.Areas.EmosIpad.Models;
using System;

public class PatientOrderCommand
{
	public ConsumerTypes ConsumerType { get; set; }
	public float Index { get; set; }
	public string Value { get; set; }
	public string ImagePath { get; set; }
	public string OrderBy { get; set; }
	public long? OperationCode { get; set; }
	public DateTime OperationDate { get; set; }
	public string CompanionStatus { get; set; }
	public RegistrationModelSimple Registration { get; set; }
	public MealOrderPeriodModelSimple MealOrderPeriod { get; set; }
	public LocationModelSimple Location { get; set; }
	public bool chargeable { get; set;}
}
public enum ConsumerTypes
{
	Patient, Companion
}