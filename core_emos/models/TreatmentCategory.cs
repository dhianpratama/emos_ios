﻿using System;

namespace emos_ios.models
{
	public class TreatmentCategoryCompactModel
	{
		public long treatment_category_id { get; set; }
		public string treatment_category_code { get; set; }
		public string treatment_category_label { get; set; }
		public long? class_id { get; set; }
		public long? institution_id { get; set; }
	}

	public class TreatmentCategoryModel : TreatmentCategoryCompactModel
	{
		public ClassModel _class { get; set; }
	}
}

