﻿namespace emos_ios.models
{
	public class LocationStructureCompactModel
	{
		public long location_structure_id { get; set; }
		public string location_structure_code { get; set; }
		public string location_structure_label { get; set; }
		public long? institution_id { get; set; }
		public long? location_structure_extension_id { get; set; }
		public long? parent_location_structure_id { get; set; }
	}
}
