﻿using System;

namespace emos_ios.models
{
	public class RestrictionModel
	{
		public long restriction_id { get; set; }
		public string restriction_label { get; set; }
		public string restriction_code { get; set; }
		public long? restriction_type_id { get; set; }
		public string restriction_type_label { get; set; }
	}

	public class ProfileTrialDisplayModel
	{
		public long? restriction_id { get; set; }
		public string restriction_label { get; set; }
		public string restriction_code { get; set; }
		public long? restriction_type_id { get; set; }
		public string restriction_type_label { get; set; }
		public long? dish_type_id { get; set; }
	}
}

