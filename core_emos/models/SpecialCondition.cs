﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class SpecialCondition
	{
		public long? DietOrderId { get; set; }
		public long? ProfileDietOrderId { get; set; }
		public KeyValuePair<long?, string> DietOrder { get; set;}
		public DateTime StartTime { get; set; }
		public KeyValuePair<long?, string> StartMealPeriod { get; set;}
		public DateTime? EndTime { get; set; }
		public KeyValuePair<long?, string> EndMealPeriod { get; set;}
		public bool IsDateSpecific { get; set; }
	}
}