﻿using System;

namespace emos_ios.models
{
	public class DietOrderTypeModel
	{
		public long diet_order_id { get; set; }
		public string diet_order_code { get; set; }
		public string diet_order_label { get; set; }
	}
}

