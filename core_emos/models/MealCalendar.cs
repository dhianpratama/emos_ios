﻿using System;
using System.Collections.Generic;

namespace core_emos
{
	public class MealCalendar
	{
		public DateTime OrderDate { get; set; }
		public List<PatientOrderCommand> PatientOrderCommands { get; set; }
	}
}

