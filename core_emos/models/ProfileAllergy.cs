﻿using System;

namespace emos_ios.models
{
	public class ProfileAllergyDisplayModel
	{
		public long profile_allergy_id { get; set; }
		public long? profile_id { get; set; }
		public long? restriction_id { get; set; }
		public string restriction_label { get; set; }
		public string restriction_type_label { get; set; }
	}
}

