﻿using System;

namespace emos_ios.models
{
	public class GenderModel
	{
		public long gender_id { get; set; }
		public string gender_code { get; set; }
		public string gender_label { get; set; }
	}
}

