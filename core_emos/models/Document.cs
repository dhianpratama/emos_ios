﻿using System;

namespace emos_ios.models
{
	public class DocumentModel
	{
		public long document_id { get; set; }
		public string document_code { get; set; }
		public string document_label { get; set; }
		public long? document_type_id { get; set; }
	}
}

