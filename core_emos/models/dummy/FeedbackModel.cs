﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_IRIS.BusinessLogic.Feedback
{
	public class FeedbackModel
	{
		public QuestionSetModel questionSet { get; set; }
		public List<QuestionModel> questionCollection { get; set; }
	}

	public class AnswerModel
	{
		public long question_set_id { get; set; }
		public long question_id { get; set; }
		public string question_type_label { get; set; }
		public string freetext { get; set; }
		public string answer { get; set; }
	}

	public class SaveFeedbackModel
	{
		public long? profile_id { get; set; }
		public long? registration_id { get; set; }
		public List<AnswerModel> answers { get; set; }

		public SaveFeedbackModel()
		{
			answers = new List<AnswerModel>();
		}
	}

	public class FeedbackParams
	{
		public long? institution_id { get; set; }
		public string searchText { get; set; }

		public List<FeedbackOrderByState> sort { get; set; }
	}

	public class FeedbackOrderByState
	{
		public bool active { get; set; }
		public string identifier { get; set; }
		public bool asc { get; set; }
	}
}