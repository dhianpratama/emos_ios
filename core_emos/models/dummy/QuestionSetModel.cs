﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_IRIS.BusinessLogic.Feedback
{
    public class QuestionSetModel
    {
        public long question_set_id { get; set; }
        public long? institution_id { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public bool finalized { get; set; }
        public bool use_for_emos_ipad { get; set; }
        public bool set_to_finalized { get; set; } // temporary
    }
}