﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class DishTypeTexture
	{
		public KeyValuePair<long, string> DishType {get;set;}
		public long TextureId {get;set;}
	}
}

