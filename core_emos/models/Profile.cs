﻿using System;
using System.Collections.Generic;

namespace emos_ios.models
{
	public class ProfileCompactModel
	{
		public ProfileCompactModel()
		{
			extension_properties = new Dictionary<string, string>();
		}
		public long profile_id { get; set; }
		public string profile_name { get; set; }
		public string profile_identifier { get; set; }
		public string profile_internal_id { get; set; }
		public DateTime? birthday { get; set; }
		public string birthday_verbose { get; set; }
		public long? document_type_id { get; set; }
		public long? document_id { get; set; }
		public long? language_id { get; set; }
		public long? gender_id { get; set; }
		public long? race_id { get; set; }
		public long? salutation_id { get; set; } 
		public bool tomorrow_is_birthday { get; set; }
		public Dictionary<string, string> extension_properties { get; set; }
	}

	public class ProfileModel : ProfileCompactModel
	{
		public LanguageModel language { get; set; }
		public GenderModel gender { get; set; }
		public RaceModel race { get; set; }
		public SalutationModel salutation { get; set; }
		public DocumentModel document { get; set; }
		public DocumentTypeModel document_type { get; set; }
	}

	public class ProfileSaveModel : ProfileCompactModel
	{
		public OperationInfo operationInfo { get; set; }    
	}
}

