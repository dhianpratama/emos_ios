﻿using System;

namespace emos_ios.models
{
	public class LanguageModel
	{
		public long language_id { get; set; }
		public string language_code { get; set; }
		public string language_label { get; set; }
	}
}

