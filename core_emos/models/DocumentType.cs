﻿using System;

namespace emos_ios.models
{
	public class DocumentTypeModel
	{
		public long document_type_id { get; set; }
		public string document_type_code { get; set; }
		public string document_type_label { get; set; }
	}
}

