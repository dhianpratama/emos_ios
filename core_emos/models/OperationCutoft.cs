﻿using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios.models
{
	public class OperationCutoffCompactModel
	{
		public long? operation_id { get; set; }
		public long? meal_order_period_id { get; set; }
		public long? location_id { get; set; }
		public string time_reference { get; set; }
		public string time1 { get; set; }
		public string time2 { get; set; }
		public long? time1_day_ref { get; set; }
		public long? time2_day_ref { get; set; }
	}
	public class OperationCutoff : OperationCutoffCompactModel
	{
		public Operation operation { get; set; }
		public MealOrderPeriodModel meal_order_period { get; set; }
		public LocationStructureCompactModel location_structure { get; set; }
	}

}

