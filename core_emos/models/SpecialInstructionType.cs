﻿using System;
using System.Collections.Generic;

namespace emos_ios.models
{
	public class SpecialInstructionTypeModel
	{
		public long special_instruction_type_id { get; set; }
		public string special_instruction_type_label { get; set; }
		public string special_instruction_type_code { get; set; }
		public long? institution_id { get; set; }

		//For diet order retrieve
		public long? profile_diet_order_special_instruction_id { get; set; }
		public int quantity { get; set; }
		public List<ProfileDietOrderSpecialInstructionDetailModel> instruction_details { get; set; }
	}

	public class ProfileDietOrderSpecialInstructionDetailModel
	{
		public long profile_diet_order_special_instruction_detail_id { get; set; }
		public long? profile_diet_order_special_instruction_id { get; set; }
		public long? special_instruction_id { get; set; }
		public string special_instruction_label { get; set; }
		public int quantity { get; set; }
		public long? dish_id { get; set; }
		public DishSimpleModel Dish { get; set; }
	}
}

