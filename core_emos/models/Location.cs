﻿using System;

namespace emos_ios.models
{
	public class LocationCompactModel
	{
		public long location_id { get; set; }
		public long? institution_id { get; set; }
		//public HierarchyId node { get; set; }
		public string node_string { get; set; }
		public short level { get; set; }
		public string code { get; set; }
		public string label { get; set; }
		public LocationCompactModel ancestor_location { get; set; }
	}
}

