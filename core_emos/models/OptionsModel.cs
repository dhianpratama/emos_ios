﻿using System;
using MonoTouch.Dialog;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class OptionsModel<T>
	{
		public DialogViewController Dvc { get; set; }
		public CheckboxGroupDVC<RestrictionModelSimple> TypeGroup { get; set; }
		public List<Section> Checkboxes { get; set; }
		public string Title;
		public List<T> Items { get; set; }
		public List<T> SelectedItems { get; set; }

		public OptionsModel ()
		{
//			TypeGroup = new List<CheckboxGroupDVC<RestrictionModelSimple>> ();
			Checkboxes = new List<Section> ();
		}
	}
}

