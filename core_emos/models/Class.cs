﻿using System;

namespace emos_ios.models
{
	public class ClassModel
	{
		public long class_id { get; set; }
		public string class_code { get; set; }
		public string class_label { get; set; }
		public int grade_sequence { get; set; }
	}
}

