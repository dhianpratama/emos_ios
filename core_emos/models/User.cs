﻿using System;
using System.Net;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public class User : BaseUser
	{
		public UserAssignedLocationModel AssignedLocation { get; set; }
		public UserLanguageModelSimple SelectedLanguage { get; set;}

		public User () : base ()
		{
			AssignedLocation = new UserAssignedLocationModel ();
			SelectedLanguage = new UserLanguageModelSimple ();
		}
	}
}

