﻿using System;

namespace core_emos
{
	public class PatientOrderCommandEventArgs : EventArgs
	{
		public PatientOrderCommand PatientOrderCommand {get; set;}
		public PatientOrderCommandEventArgs(PatientOrderCommand patientOrderCommand)
		{
			PatientOrderCommand = patientOrderCommand;
		}
	}
}

