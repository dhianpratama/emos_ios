﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public class ProfileLanguageEventArgs : EventArgs
	{
		public ProfileLanguageModelSimple ProfileLanguage { get; private set; }
		public ProfileLanguageEventArgs (ProfileLanguageModelSimple profileLanguage)
		{
			ProfileLanguage = profileLanguage;
		}
	}
}

