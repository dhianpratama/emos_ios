﻿using System;

namespace core_emos
{
	public class MealOrderHeaderEventArgs : EventArgs
	{
		public int SelectedStep { get; set; }
		public MealOrderHeaderEventArgs(int selectedStep)
		{
			SelectedStep = selectedStep;
		}
	}
}

