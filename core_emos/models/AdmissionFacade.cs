﻿using System;
using System.Collections.Generic;

namespace emos_ios.models
{
	public class AdmissionFacadeModel : RegistrationCompactModel
	{
		public AdmissionFacadeModel()
		{
			diet_orders = new List<DietOrderModel>();
			profile_allergies = new List<RestrictionModel>();
			restriction_diet_orders= new List<DietOrderModel>();
			trial_diet_orders = new List<DietOrderModel>();
			snack_and_beverages_diet_orders = new List<DietOrderModel>();
			snack_pack_diet_orders = new List<DietOrderModel>();
			full_feed_diet_orders = new List<DietOrderModel>();
			clear_feed_diet_orders = new List<DietOrderModel>();
			nbm_diet_orders = new List<DietOrderModel>();
		}

		public List<DietOrderModel> diet_orders { get; set; }
		public List<RestrictionModel> profile_allergies { get; set; }
		public List<DietOrderModel> restriction_diet_orders { get; set; }
		public List<DietOrderModel> trial_diet_orders { get; set; }
		public List<DietOrderModel> snack_and_beverages_diet_orders { get; set; }
		public List<DietOrderModel> snack_pack_diet_orders { get; set; }
		public List<DietOrderModel> full_feed_diet_orders { get; set; }
		public List<DietOrderModel> clear_feed_diet_orders { get; set; }
		public List<DietOrderModel> nbm_diet_orders { get; set; }

		public ProfileCompactModel profile { get; set; }
	}
}

