﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class PatientMealCalendarDashboard
	{
		public long? wardGroupId { get; set; }
		public List<NurseDashboardHeaderModel> header { get; set; }
		public LocationWithRegistrationSimple bed { get; set; }
		public List<DailyPatientMeal> dailyPatientMeals { get; set; }
		public PatientMealCalendarDashboard ()
		{
			this.header = new List<NurseDashboardHeaderModel> ();
			this.dailyPatientMeals = new List<DailyPatientMeal> ();
		}
	}
	public class DailyPatientMeal
	{
		public DateTime dateTime { get; set; }
		public List<MealOrderPeriodWithOperationCode> mealOrderPeriods { get; set; }
	}
}

