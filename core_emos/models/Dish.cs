﻿using System;

namespace emos_ios.models
{
	public class DishSimpleModel
	{
		public long dish_id { get; set; }
		public long? parent_dish_id { get; set; }
		public string dish_code { get; set; }
		public string dish_label { get; set; }
		public long? dish_type_id { get; set; }
		public string picture_url { get; set; }
		public bool available_for_companion { get; set; }
		public long? kitchen_id { get; set; }
		public string description { get; set; }
		public bool one_dish_meal { get; set; }
		public bool enable_high_priority_restriction { get; set; }
		public bool is_composite_dish { get; set; }
		public bool override_description { get; set; }
		public bool override_picture { get; set; }
		public bool dish_deactivated { get; set; }
		public int? default_standby_quantity { get; set; }
		public int sequence { get; set; }
		public string foodworks_name { get; set; }
		public Single weight_gram { get; set; }
		public int number_of_serves { get; set; }
	}
}

