﻿using System;
using System.Collections.Generic;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios.models
{
	public class DietOrderModel : DietOrderCompactModel
	{
		public DietOrderTypeModel dietOrderType { get; set; }
		public RegistrationModel registration { get; set; }
		public ProfileModel profile { get; set; }
		public List<RestrictionModel> restrictions { get; set; }
		public List<SpecialInstructionTypeModel> specialInstructions { get; set; }
		public MealOrderPeriodModel mealOrderPeriod { get; set; }
	}
}

