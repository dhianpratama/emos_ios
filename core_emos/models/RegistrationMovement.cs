﻿using System;
using System.Collections.Generic;

namespace emos_ios.models
{
	public class RegistrationMovementCompactModel
	{
		public RegistrationMovementCompactModel()
		{
			extension_properties = new Dictionary<string, string>();
		}

		public long registration_movement_id { get; set; }
		public long? institution_id { get; set; }
		public long? registration_id { get; set; }
		public string registration_number { get; set; }
		public long? prior_location_id { get; set; }
		public string prior_location_label { get; set; }
		public long? assigned_location_id { get; set; }
		public string assigned_location_label { get; set; }
		public long? prior_location_structure_id { get; set; }
		public string prior_location_structure_label { get; set; }
		public long? assigned_location_structure_id { get; set; }
		public string assigned_location_structure_label { get; set; }
		public long? class_id { get; set; }
		public string class_verbose { get; set; }
		public bool active_registration { get; set; }
		public DateTime? registration_time { get; set; }
		public string registration_time_verbose { get; set; }
		public bool finished_registration { get; set; }
		public DateTime? finished_registration_time { get; set; }
		public bool cancelled_registration { get; set; }
		public DateTime? cancelled_registration_time { get; set; }
		public bool planned_registration { get; set; }
		public DateTime? planned_registration_create_time { get; set; }
		public DateTime? planned_arrival_time { get; set; }
		public bool planned_finished_registration { get; set; }
		public DateTime? planned_finished_registration_create_time { get; set; }
		public DateTime? planned_finish_registration_time { get; set; }
		public bool deprecated { get; set; }
		public int movement_sequence { get; set; }
		public string movement_sequence_verbose { get; set; }
		public DateTime? movement_time { get; set; }
		public string movement_time_verbose { get; set; }
		public bool planned_movement { get; set; }
		public bool actual_movement { get; set; }
		public Dictionary<string, string> extension_properties { get; set; }
	}
}

