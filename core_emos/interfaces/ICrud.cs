﻿using System;

namespace emos_ios
{
	public interface ICrud<T> : ICallback<T>
	{
		void Delete(T item);
		void Edit(T item);
	}
}

