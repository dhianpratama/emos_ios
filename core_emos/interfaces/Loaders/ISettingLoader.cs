﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public interface ISettingLoader
	{
		void Load (IApplicationContext appContext, bool hideDateSetting = false, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDone = null, EventHandler OnCancel = null, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDateChanged = null);
	}
}

