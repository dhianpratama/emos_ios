﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public interface IBulkOrderSummaryLoader
	{
		void Load (IApplicationContext appContext, KeyValuePair<string,string> location);
	}
}

