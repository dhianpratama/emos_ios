﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IAdmissionLoader
	{
		void LoadEditAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location, EventHandler onSave = null);
		void LoadViewAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location);
	}
}

