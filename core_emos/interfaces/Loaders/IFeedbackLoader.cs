﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IFeedbackLoader
	{
		void LoadFeedback (IApplicationContext appContext);
	}
}

