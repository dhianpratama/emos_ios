﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;

namespace emos_ios
{
	public interface IBulkOrderDetailLoader
	{
		void Load (IApplicationContext appContext, KeyValuePair<string, string> location, BulkMealRestrictionGroupInfo item, EventHandler onSave, MealOrderPeriodModel mealPeriod);
	}
}

