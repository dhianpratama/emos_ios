﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ISpecialInstructionLoader
	{
		void LoadSpecialInstruction (IApplicationContext appContext, LocationWithRegistrationSimple location, IPatientMenu callback, InterfaceStatusModel _interfaceStatus);
	}
}

