﻿using System;

namespace emos_ios
{
	public interface IControllerLoader
	{
		IAdmissionLoader AdmissionLoader { get; set; }
		ISpecialInstructionLoader SpecialInstructionLoader { get; set;}
		IFeedbackLoader FeedbackLoader { get; set;}
		ILoader PatientInfoLoader { get; set;}
		ISettingLoader SettingLoader { get; set;}
		IBulkOrderSummaryLoader BulkOrderSummaryLoader { get; set;}
		IBulkOrderDetailLoader BulkOrderDetailLoader { get; set;}
	}

	public interface ILoader
	{
		void Load(IApplicationContext appContext);
	}
}

