﻿using System;
using ios;
using core_emos;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace emos_ios
{
	public interface IApplicationContext : IBaseContext
	{
		User CurrentUser { get; set; }
		OperationCodeHandler OperationCodeHandler { get; set; }
		IControllerLoader ControllerLoader { get; set; }
		WardViewHandler WardViewHandler { get; set; }
		void PopViewController(bool animated=true);
		LockHandler LockHandler { get; set; }
		List<LanguageModelSimple> Languages { get; set;}
		long? WardId { get; set; }
		string MealPeriodGroupCode { get; set; }
		long? InstitutionId { get; set; }
		long? HospitalId { get; set; }
		KeyValuePair<string, string> Ward { get; set; }
		RegistrationModelSimple SelectedRegistration { get; set; }
		LocationModelSimple SelectedLocation { get; set; }
		MealOrderPeriodModelSimple SelectedMealOrderPeriod { get; set; }
		void SetHospitalByWard ();
		long? SelectedOperationCode { get; set; }
		void LoadMenu(core_emos.Menu.EmosMenu emosMenu, bool isRoot = true, bool showMenu = true);
		void LoadController (bool isRoot, bool showLeftMenu, UIViewController controller);
		void LoadNurseMealOrder (bool isRoot, bool showLeftMenu, Dashboard.DashboardMenu dashboardMenu, bool IsPatient, PatientOrderCommand item, EventHandler<PatientOrderCommandEventArgs> onClose, EventHandler<PatientOrderCommandEventArgs> onSave, IPatientDayOrderCallback callback, bool isPatientCalendar);
		void LoadPatientMealOrder (bool isRoot, bool showLeftMenu, Dashboard.DashboardMenu dashboardMenu, bool IsPatient, PatientOrderCommand item, EventHandler<PatientOrderCommandEventArgs> onClose, EventHandler<PatientOrderCommandEventArgs> onSave, IPatientDayOrderCallback callback, bool isPatientCalendar);
		void LoadCompanionMealOrder (bool isRoot, bool showLeftMenu, bool IsPatient, PatientOrderCommand item);
		void LoadPatientLanguage (bool isRoot, bool showLeftMenu, ProfileLanguageModelSimple profileLanguage, EventHandler<ProfileLanguageEventArgs> onSaved);
		void LoadAuthentication (EventHandler onSuccess);
		void LoadModalView (UIView view);
		void SetGlobalWard(KeyValuePair<string,string> selected);
		string WardLabel{ get; set;}
	}
}

