﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IPatientMenu
	{
		void ClosePatientMenu ();
		void PatientMealOrder (LocationModelSimple location, RegistrationModelSimple registration);
		void EditAdmission(LocationWithRegistrationSimple locationWithRegistrationSimple);
		void DischargePatient(LocationWithRegistrationSimple locationWithRegistrationSimple);
		void TransferPatient(LocationWithRegistrationSimple locationWithRegistrationSimple);
		void TherapeuticDietOrder(LocationWithRegistrationSimple locationWithRegistrationSimple, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed, bool extendLock = false);
		void FoodAllergies(LocationWithRegistrationSimple locationWithRegistrationSimple, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed, bool extendLock = false);
		void SnackPack(LocationWithRegistrationSimple locationWithRegistrationSimple);
		void NillByMouth(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false);
		void FullFeeds(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false);
		void ClearFeeds(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false);
		void ConsumptionChecklist(LocationModelSimple location, RegistrationModelSimple registration);
		void MealType(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false);
		void SpecialInstruction(LocationWithRegistrationSimple locationWithRegistrationSimple);
		void ViewAdmission (LocationWithRegistrationSimple locationWithRegistrationSimple);
		void TrialOrder (LocationWithRegistrationSimple locationWithRegistrationSimple);
		void FluidRestriction (LocationWithRegistrationSimple locationWithRegistrationSimple);
		void Feedback (LocationWithRegistrationSimple locationWithRegistrationSimple);
		void PopToViewControllerAndUnlock (bool unlock = true, bool animated = true);
		void BackToCompanion();
		void PatientMealCalendar(LocationModelSimple location, RegistrationModelSimple registration);
		void CompanionMealCalendar(LocationModelSimple location, RegistrationModelSimple registration);
		void WebPortalRegistration(LocationModelSimple location, RegistrationModelSimple registration);
		LockResultIpad LockResult { get; }
		bool ShouldRunViewWillAppear { get; set; }
	}
}

