﻿using System;
using MonoTouch.UIKit;

namespace core_emos
{
	public static class NavigationBarHelper
	{
		public static UIBarButtonItem CreateSeparatorBarButton()
		{
			return new UIBarButtonItem ("|", UIBarButtonItemStyle.Plain,(sender, args) =>  {});
		}
	}
}

