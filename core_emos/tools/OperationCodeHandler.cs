﻿using System;
using ios;

namespace emos_ios
{
	public class OperationCodeHandler
	{
		private IApplicationContext _appContext;
		public OperationCodeHandler (IApplicationContext appContext)
		{
			_appContext = appContext;
		}
		public OperationCodeStatus GetStatus (long? operationCode)
		{
			switch(operationCode)
			{
			case 0:
				return new OperationCodeStatus ();
			case 1:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("Order"), 
					ImagePath = "Images/btn_order.png"
				};
			case 2:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("View"), 
					ImagePath = "Images/btn_edit.png"
				};
			case 3:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("Edit"), 
					ImagePath = "Images/btn_edit.png"
				};
			case 4:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("Reorder"), 
					ImagePath = "Images/btn_reorder.png"
				};
			case 5:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("Ordered"), 
					ImagePath = "Images/btn_ordered.png"};
			case 6:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("NoOrder"), 
					ImagePath = "Images/btn_no_order.png"};
			case 7:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("NoOrder"), 
					ImagePath = "Images/btn_nbm.png"
				};
			case 8:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("View"), 
					ImagePath = "Images/btn_edit.png"
				};
			case 9:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("Reorder"), 
					ImagePath = "Images/reorder_grey.png"
				};
			case 11:
				return new OperationCodeStatus { 
					Status = _appContext.LanguageHandler.GetLocalizedString("ViewDraft"), 
					ImagePath = "Images/btn_draft.png"};
			case 12:
				return new OperationCodeStatus { 
					Status = "",
					ImagePath = "Images/stay_for_lunch.png"};
			case 13:
				return new OperationCodeStatus { 
					Status = "",
					ImagePath = "Images/put_on_hold.png"};
			case 14:
				return new OperationCodeStatus { 
					Status = "",
					ImagePath = "Images/decline.png"};
			default:
				return new OperationCodeStatus ();
			};
		}

		public static OperationCodeStatus GetConsumptionStatus(long? operationCode)
		{
			switch(operationCode)
			{
			case 2:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_edit.png"};
			case 3:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_no_order.png"};
			case 4:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_nbm.png"};
			case 99:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/btn_no_order.png"};
			case 100:
				return new OperationCodeStatus { Status = "", ImagePath = "Images/cumulative_btn.png"};
			default:
				return new OperationCodeStatus ();
			};
		}

		public OperationCodeStatus GetConsumptionStatus(long? operationCode, bool ordered)
		{
			if (operationCode == 4)
				return new OperationCodeStatus { Status = "NBM", ImagePath = "Images/btn_nbm.png"};
			if (ordered)
				return new OperationCodeStatus { Status = _appContext.LanguageHandler.GetLocalizedString("Order"), ImagePath = "Images/btn_edit.png" };
			else
				return new OperationCodeStatus { Status = _appContext.LanguageHandler.GetLocalizedString("Edit"), ImagePath = "Images/btn_no_order.png"};
		}
		public static bool IsDraft (long? operationCode)
		{
			return operationCode == 11;
		}
		public static bool IsViewExistingOrder (long? operationCode)
		{
			return operationCode == 2
				|| operationCode == 5
				|| operationCode == 8
				|| operationCode == 12
				|| operationCode == 13
				|| operationCode == 14;
		}
		public static bool IsClickableOnDashboard (long? operationCode)
		{
			return operationCode == 1
			|| operationCode == 4
			|| operationCode == 2
			|| operationCode == 5
			|| operationCode == 8
			|| operationCode == 11
			|| operationCode == 12
			|| operationCode == 13
			|| operationCode == 14;
		}
		public static bool HasOrderBy (long? operationCode)
		{
			return operationCode == 2
			|| operationCode == 3
			|| operationCode == 4
			|| operationCode == 5
			|| operationCode == 12
			|| operationCode == 13;
			}
		public static bool IsOrdered (long? operationCode)
		{
			return operationCode == 5;
		}

		public static bool ShouldGoToMealOrderWithoutCheckingRole(long? operationCode)
		{
			return operationCode == 2
				|| operationCode == 5
				|| operationCode == 11;
		}

		public static bool EditModeAndValidForOneDishMeal(long? operationCode)
		{
			return operationCode == 4
			|| operationCode == 11;
		}
	}
}

