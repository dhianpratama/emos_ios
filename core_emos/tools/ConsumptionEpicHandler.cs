﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using ios;
using Newtonsoft.Json;

namespace emos_ios
{
	public class ConsumptionEpicHandler
	{
		public LocationModelSimple Location { get; set; }
		public RegistrationModelSimple Registration { get; set; }
		public PatientConsumptionInfo PatientConsumptionInfo { get; set; }
		public event EventHandler<RequestEventArgs> HandleRequestCompleted;
		private IApplicationContext _appContext;

		public ConsumptionEpicHandler (IApplicationContext appContext)
		{
			_appContext = appContext;
		}
		private ConsumptionChecklistEpicModel GenerateConsumptionChecklistEpicModel(NutrientForConsumptionChecklistWithCreateTime result)
		{
			var nutrients = new List<NutrientForConsumptionChecklist> ();
			nutrients = result.nutrients;
			PatientConsumptionInfo.consumption_checklist_id = result.consumption_checklist_id;
			PatientConsumptionInfo.outside_food_taken = result.outside_food_taken;

			var fluidML = GetNutrientForEpic ("FLUIDCONSUMED", nutrients).value ?? (float)0;
			foreach (var freetext in result.Freetexts) {
				fluidML += int.Parse (freetext.freetext_value);
			}

			var epicDataModel = new ConsumptionChecklistEpicModel();
			epicDataModel.pid = new Pid()
			{
				nric = Registration.profile_identifier,
				name = Registration.profile_name,
				dob = Registration.birthday.Value.ToString("yyyyMMdd"),
				gender = Registration.gender_code,
				caseNumber = Registration.registration_number
			};
			epicDataModel.pv1 = new Pv1();
			epicDataModel.obr = new Obr()
			{
				time = result.formatted_create_time
			};
			epicDataModel.obx = new List<Obx>();
			epicDataModel.obx.Add(new Obx()
				{
					seq = 1,
					dataType = "ST",
					key = "OUTSIDEFOODTAKEN",
					value = PatientConsumptionInfo.outside_food_taken ? "Yes" : "No",
					unit = "",
					observer = _appContext.CurrentUser.Id
				});
			epicDataModel.obx.Add(new Obx()
				{
					seq = 2,
					dataType = "NM",
					key = "CALORIES",
					value = GetNutrientForEpic("CALORIES", nutrients).value.ToString(),
					unit = "kCal",
					observer = _appContext.CurrentUser.Id
				});
			epicDataModel.obx.Add(new Obx()
				{
					seq = 3,
					dataType = "NM",
					key = "CARBOHYDRATE",
					value = GetNutrientForEpic("CARBOHYDRATE", nutrients).value.ToString(),
					unit = GetNutrientForEpic("CARBOHYDRATE", nutrients).unit,
					observer = _appContext.CurrentUser.Id
				});
			epicDataModel.obx.Add(new Obx()
				{
					seq = 4,
					dataType = "NM",
					key = "FAT",
					value = GetNutrientForEpic("FAT", nutrients).value.ToString(),
					unit = GetNutrientForEpic("FAT", nutrients).unit,
					observer = _appContext.CurrentUser.Id
				});
			epicDataModel.obx.Add(new Obx()
				{
					seq = 5,
					dataType = "NM",
					key = "FLUIDCONSUMED",
					value = fluidML.ToString(),
					unit = "mL",
					observer = _appContext.CurrentUser.Id
				});
			epicDataModel.obx.Add(new Obx()
				{
					seq = 6,
					dataType = "NM",
					key = "PROTEIN",
					value = GetNutrientForEpic("PROTEIN", nutrients).value.ToString(),
					unit = GetNutrientForEpic("PROTEIN", nutrients).unit,
					observer = _appContext.CurrentUser.Id
				});

			return epicDataModel;
		}
		private NutrientForConsumptionChecklist GetNutrientForEpic(string key, List<NutrientForConsumptionChecklist> nutrients)
		{
			var result = new NutrientForConsumptionChecklist ();
			NutrientForConsumptionChecklist nut = null;
			switch (key) {
			case "CARBOHYDRATE":
				nut = nutrients.FirstOrDefault (e => e.label.Contains("Carbohydrate"));
				if(nut!=null){
					result.label = nut.label;
					result.value = nut.value;
					result.unit = nut.unit;
				}
				break;
			case "FAT":
				nut = nutrients.FirstOrDefault (e => e.label.Contains("Total fat"));
				if(nut!=null){
					result.label = nut.label;
					result.value = nut.value;
					result.unit = nut.unit;
				}
				break;
			case "FLUIDCONSUMED":
				nut = nutrients.FirstOrDefault (e => e.label.Contains("Water"));
				if(nut!=null){
					result.label = nut.label;
					result.value = nut.value;
					result.unit = nut.unit;

					if (PatientConsumptionInfo.extraBeverages != null) {
						PatientConsumptionInfo.extraBeverages
							.ForEach (e => {
								float val = Convert.ToInt32(e.Value);
								result.value += val;
							});
					}
				}
				break;
			case "PROTEIN":
				nut = nutrients.FirstOrDefault (e => e.label.Contains("Protein"));
				if(nut!=null){
					result.label = nut.label;
					result.value = nut.value;
					result.unit = nut.unit;
				}
				break;
			case "CALORIES":
				nut = nutrients.FirstOrDefault (e => e.label.Contains("Calories"));
				if(nut!=null){
					result.label = nut.label;
					result.value = nut.value;
					result.unit = nut.unit;
				}
				break;
			}

			return result;
		}
		public async void RequestSendToEpic(NutrientForConsumptionChecklistWithCreateTime nutrients)
		{
			var epicModel = GenerateConsumptionChecklistEpicModel (nutrients);
			var queryString = KnownUrls.SendConsumptionToEPICQuerString (JsonConvert.SerializeObject (epicModel));
			await _appContext.HttpSender.Request<OperationInfo> ()
				.From (KnownUrls.SendConsumptionToEPIC)
				.WithQueryString (queryString)
				.WhenSuccess (result => SentToEpicSuccessful (result))
				.WhenFail (result => HandleRequestCompleted.SafeInvoke (this, new RequestEventArgs (false)))
				.Go ();
		}
		private void SentToEpicSuccessful(OperationInfo result)
		{
			if (result.executed && result.success) {
				RequestDataSentToEpic (PatientConsumptionInfo.consumption_checklist_id);
			} else {
				HandleSuccessfulRequest (result);
			}
		}
		public async void RequestDataSentToEpic(long? consumption_id)
		{
			var queryString = KnownUrls.DataSentToEpicWithChecklistId(consumption_id);
			await _appContext.HttpSender.Request<OperationInfo> ()
				.From (KnownUrls.DataSentToEpic)
				.WithQueryString (queryString)
				.WhenSuccess (result => HandleSuccessfulRequest (result))
				.WhenFail (result => HandleRequestCompleted.SafeInvoke (this, new RequestEventArgs (false)))
				.Go ();
		}
		private void HandleSuccessfulRequest (OperationInfo result)
		{
			if (result.executed)
				HandleRequestCompleted.SafeInvoke (this, new RequestEventArgs(true));
			else
				HandleRequestCompleted.SafeInvoke (this, new RequestEventArgs(false));
		}
	}
}

