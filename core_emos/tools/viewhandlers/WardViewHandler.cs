﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using System.Linq;

namespace emos_ios
{
	public class WardViewHandler
	{
		private IApplicationContext _appContext;
		public List<LocationModelSimple> Wards { get; set; }
		public event EventHandler OnRequestSuccessful;
		public event EventHandler OnRequestFailed;

		public WardViewHandler (IApplicationContext appContext)
		{
			Wards = new List<LocationModelSimple> ();
			_appContext = appContext;
		}
		public void Request()
		{
			if (Wards != null && Wards.Any ()) {
				OnRequestSuccessful.SafeInvoke (this);
				return;
			}				
			RequestWards ();
		}
		public void RequestInstitutionWards(long institution_id)
		{
			if (Wards != null && Wards.Any ()) {
				OnRequestSuccessful.SafeInvoke (this);
				return;
			}				
			SendRequestInstitutionWards (institution_id);
		}
		public bool CheckBulkOrder (string wardId)
		{
			if (String.IsNullOrEmpty (wardId))
				return false;
			if (Wards == null || !Wards.Any ())
				return false;
			var ward = Wards.First (w => w.id == long.Parse (wardId));
			if (ward.bulkMealOrder)
				return true;
			return false;
		}
		private async void RequestWards()
		{
			var param = KnownUrls.InstitutionIdQueryString (_appContext.InstitutionId);
			await _appContext.HttpSender.Request<List<LocationModelSimple>> ()
				.From (KnownUrls.GetWards)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestWardsSuccessful(result))
				.WhenFail (result=> OnRequestFailed.SafeInvoke (this))
				.Go ();
		}
		private void OnRequestWardsSuccessful(List<LocationModelSimple> wards)
		{
			Wards = wards;
			OnRequestSuccessful.SafeInvoke (this);
		}
		private async void SendRequestInstitutionWards(long institution_id)
		{
			var param = KnownUrls.InstitutionIdQueryString (institution_id);
			await _appContext.HttpSender.Request<List<LocationModelSimple>> ()
				.From (KnownUrls.GetInstitutionWards)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestWardsSuccessful(result))
				.WhenFail (result=> OnRequestFailed.SafeInvoke (this))
				.Go ();
		}
	}
}

