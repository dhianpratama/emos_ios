﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;

namespace core_emos
{
	public class PatientDayOrderBorderHandler
	{
		public RectangleBorder RectangleBorder { get; set; }

		public void HideAllBorder()
		{
			RectangleBorder.Left.Hidden = true;
			RectangleBorder.Right.Hidden = true;
			RectangleBorder.Top.Hidden = true;
			RectangleBorder.Bottom.Hidden = true;
		}
		public void ShowAllBorder()
		{
			RectangleBorder.Left.Hidden = false;
			RectangleBorder.Right.Hidden = false;
			RectangleBorder.Top.Hidden = false;
			RectangleBorder.Bottom.Hidden = false;
		}
		public void GenerateBorder(bool isPatientMealOrder, BedStatus bedStatus)
		{
			if (isPatientMealOrder) {
				HideAllBorder ();
				return;
			}

			switch (bedStatus) {
			case BedStatus.Single:
				HideAllBorder ();
				break;
			case BedStatus.Top:
				ShowAllBorder ();
				RectangleBorder.Bottom.Hidden = true;
				break;
			case BedStatus.Middle:
				ShowAllBorder ();
				RectangleBorder.Top.Hidden = true;
				RectangleBorder.Bottom.Hidden = true;
				break;
			case BedStatus.Bottom:
				ShowAllBorder ();
				RectangleBorder.Top.Hidden = true;
				break;
			}
		}
		public void SetBordersLayout (float height)
		{
			RectangleBorder.Left.SetFrame (height: height);
			RectangleBorder.Right.SetFrame (height: height);
			RectangleBorder.Bottom.SetFrame (y: height);
		}
	}
}

