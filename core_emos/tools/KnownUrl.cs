﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Web;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class KnownUrls
	{
		public static string InstitutionIdQueryString(long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString["institution_id"] = institution_id.ToString();
			return queryString.ToString ();
		}
		public static string InstitutionIdParam(long? institution_id)
		{
			return String.Format ("?institution_id={0}", institution_id);
		}

		public static string RegistrationIdQueryString(long registrationId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString["registrationId"] = registrationId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl Login = new KnownUrl ("System/Auth/Login", HttpMethod.Post);
		public static string LoginQueryString(string user_name, string password)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["user_name"] = user_name;
			queryString ["password"] = password;
			return queryString.ToString ();
		}
		public static readonly KnownUrl Logout = new KnownUrl ("System/Auth/Logout", HttpMethod.Post);
		public static string LogoutQueryString(string user_name)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["user_name"] = user_name;
			return queryString.ToString ();
		}

		public static string LoginParam(string user_name, string password)
		{
			return String.Format ("?user_name={0}&password={1}", user_name, password);
		}

		public static readonly KnownUrl GetEditAdmissionModel = new KnownUrl ("EmosIpad/EditAdmission/GetEditAdmissionModel", HttpMethod.Post);
		public static string GetAdmissionQueryString(long? registration_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetEditAdmissionFull = new KnownUrl ("EmosIpad/Admission/GetFullAdmission", HttpMethod.Get);
		public static string GetAdmissionFullQueryString(long? registration_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl SaveFullAdmission = new KnownUrl ("EmosIpad/SaveFullAdmission/Save", HttpMethod.Post);
		public static string GetAdmissionParam(long? registration_id, long? institution_id)
		{
			return String.Format ("?registration_id={0}&institution_id={1}", registration_id, institution_id);
		}

		public static readonly KnownUrl GetExistingProfile = new KnownUrl ("EmosIpad/EditAdmission/GetEditAdmissionModelByIdentifierNumber", HttpMethod.Post);
		public static string GetExistingProfileQueryString(string identifier, long? document_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["identifier"] = identifier;
			queryString ["document_id"] = document_id.ToString();
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}

		public static readonly KnownUrl SaveAdmission = new KnownUrl ("EmosIpad/EditAdmission/SaveAdmission", HttpMethod.Post);

		public static readonly KnownUrl DischargeAdmission = new KnownUrl ("EmosIpad/EditAdmission/DischargeAdmission", HttpMethod.Post);
		public static string DischargeAdmissionQueryString(long? registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static string DischargeAdmissionParam(long? registration_id)
		{
			return String.Format ("?registration_id={0}", registration_id);
		}

		public static readonly KnownUrl GetNurseDashboardModel = new KnownUrl("EmosIpad/NurseDashboard/GetNurseDashboardModel", HttpMethod.Post);
		public static string GetNurseDashboardQueryString(string meal_period_group_code, long? institution_id, long? ward_id, DateTime date, long? hospital_id)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			queryString ["meal_period_group_code"] = meal_period_group_code;
			queryString ["institution_id"] = institution_id.ToString ();
			if (ward_id != null)
				queryString ["ward_id"] = ward_id.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["hospital_id"] = hospital_id.ToString ();
			return queryString.ToString ();
		}
		public static string GetNurseDashboardParam(string meal_period_group_code, long? institution_id, long? ward_id, DateTime date)
		{
			return String.Format ("?meal_period_group_code={0}&institution_id={1}&ward_id={2}&date={3}", meal_period_group_code, institution_id, ward_id, date);
		}
		public static readonly KnownUrl GetPatientCalendar = new KnownUrl("EmosIpad/PatientCalendar/GetPatientCalendar", HttpMethod.Post);
		public static string GetPatientCalendarQueryString(long? institution_id, long? hospital_id, long? registration_id, string meal_period_group_code, DateTime startDate, int daysNumber)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			queryString ["institutionId"] = institution_id.ToString ();
			queryString ["hospitalId"] = hospital_id.ToString ();
			queryString ["registrationId"] = registration_id.ToString ();
			queryString ["mealOrderPeriodGroupFilter"] = meal_period_group_code;
			queryString ["startDate"] = startDate.ToMvcDateString ();
			queryString ["daysNumber"] = daysNumber.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetCompanionCalendar = new KnownUrl("EmosIpad/PatientCalendar/GetCompanionCalendar", HttpMethod.Post);
		public static readonly KnownUrl GetAvailableBedsByWard = new KnownUrl("EmosIpad/NurseDashboard/GetAvailableBedsByWard", HttpMethod.Post);
		public static string GetAvailbaleBedsByWardQueryString(long? ward_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["ward_id"] = ward_id.ToString ();
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}
		public static string GetAvailbaleBedsByWardParam(long? ward_id, long? institution_id)
		{
			return String.Format ("?ward_id={0}&institution_id={1}", ward_id, institution_id);
		}

		public static readonly KnownUrl GetWards = new KnownUrl("EmosIpad/NurseDashboard/GetWards", HttpMethod.Post);
		public static string GetWardsQueryString(long hospital_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["hospital_id"] = hospital_id.ToString ();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetInstitutionWards = new KnownUrl("EmosIpad/NurseDashboard/GetInstitutionWards", HttpMethod.Post);

		public static readonly KnownUrl GetBedsByWard = new KnownUrl ("EmosIpad/NurseDashboard/GetAvailableBedsByWard", HttpMethod.Post);
		public static string GetBedsByWardQueryString(long? ward_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["ward_id"] = ward_id.ToString ();
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}
		public static string GetBedsByWardParam(long? ward_id, long? institution_id)
		{
			return String.Format ("?ward_id={0}&institution_id={1}", ward_id, institution_id);
		}

		public static readonly KnownUrl GetMealOrderModel = new KnownUrl("EmosIpad/MealOrder/GetMealOrderModel", HttpMethod.Post);
		public static string GetMealOrderModelQueryString(long? registration_id, long? meal_order_period_id, long? operation_code, DateTime date, bool patientMode = false)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["meal_order_period_id"] = meal_order_period_id.ToString ();
			queryString ["operation_code"] = operation_code.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["is_patient_mode"] = patientMode.ToString();
			return queryString.ToString ();
		}
		public static string GetMealOrderModelParam(long? registration_id, long? meal_order_period_id, long? operation_code, DateTime date)
		{
			return String.Format ("?registration_id={0}&meal_order_period_id={1}&operation_code={2}&date={3}", registration_id, meal_order_period_id, operation_code, date);
		}

		public static readonly KnownUrl SaveMealOrder = new KnownUrl ("EmosIpad/MealOrder/SaveOrder", HttpMethod.Post);

		public static readonly KnownUrl GetFluidRestrictionModel = new KnownUrl("EmosIpad/FluidRestriction/GetFluidRestrictionModel", HttpMethod.Post);

		public static readonly KnownUrl GetTherapeuticModel = new KnownUrl("EmosIpad/TherapeuticDietOrder/GetTherapeuticDietOrderModel", HttpMethod.Post);
		public static readonly KnownUrl AddTherapeutic = new KnownUrl("EmosIpad/TherapeuticDietOrder/AddTherapeutic", HttpMethod.Post);
		public static string InstitutionAndRegistrationQueryString(long? institution_id, long? registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetTherapeuticModelById = new KnownUrl("EmosIpad/TherapeuticDietOrder/GetTherapeuticDietOrderModelById", HttpMethod.Post);
		public static string GetTherapeuticModelByIdQueryString(long? institution_id, long? registration_id, long? profileDietOrderId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["profileDietOrderId"] = profileDietOrderId.ToString ();
			return queryString.ToString ();
		}
		public static string InstitutionAndRegistrationParam(long? institution_id, long? registration_id)
		{
			return String.Format ("?institution_id={0}&registration_id={1}", institution_id, registration_id);
		}

		public static readonly KnownUrl SaveTherapeuticDietOrder = new KnownUrl("EmosIpad/TherapeuticDietOrder/Save", HttpMethod.Post);

		public static readonly KnownUrl SaveFluidRestriction = new KnownUrl("EmosIpad/FluidRestriction/Save", HttpMethod.Post);

		public static readonly KnownUrl GetFoodAllergiesModel = new KnownUrl("EmosIpad/FoodAllergies/GetFoodAllergiesModel", HttpMethod.Post);

		public static readonly KnownUrl SaveFoodAllergies = new KnownUrl("EmosIpad/FoodAllergies/Save", HttpMethod.Post);

		public static readonly KnownUrl GetSnackPackModel = new KnownUrl("EmosIpad/SnackPack/GetSnackPackModel", HttpMethod.Post);

		public static readonly KnownUrl SaveSnackPack = new KnownUrl("EmosIpad/SnackPack/Save", HttpMethod.Post);

		public static readonly KnownUrl GetCommonDietOrderModel = new KnownUrl ("EmosIpad/CommonDietOrder/GetCommonDietOrderModel", HttpMethod.Post);

		public static string GetCommonDietOrderQueryString(string diet_order_code, long? institution_id, long? registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["diet_order_code"] = diet_order_code;
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static string GetCommonDietOrderParam(string diet_order_code, long? institution_id, long? registration_id)
		{
			return String.Format ("?diet_order_code={0},institution_id={1}&registration_id={2}", diet_order_code, institution_id, registration_id);
		}

		public static readonly KnownUrl SaveCommonDietOrder = new KnownUrl("EmosIpad/CommonDietOrder/Save", HttpMethod.Post);
		public static readonly KnownUrl GetAllMealPeriodGroups = new KnownUrl("eMOS/MealOrderPeriodGroup/GetAllMealPeriodGroupsIpad", HttpMethod.Post);

		public static readonly KnownUrl TransferAdmission = new KnownUrl("EmosIpad/EditAdmission/TransferAdmission", HttpMethod.Post);
		public static string TransferAdmissionQueryString(long? registration_id, long? old_location_id, long? new_location_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["old_location_id"] = old_location_id.ToString ();
			queryString ["new_location_id"] = new_location_id.ToString ();
			return queryString.ToString ();
		}
		public static string TransferAdmissionParam(long? registration_id, long? old_location_id, long? new_location_id)
		{
			return String.Format ("?registration_id={0}&old_location)id={1}&new_location_id={2}", registration_id, old_location_id, new_location_id);
		}

		public static readonly KnownUrl GetPatientCalenderForConsumptionChecklist = new KnownUrl("EmosIpad/ConsumptionChecklist/GetConsumptionChecklistInfo", HttpMethod.Post);
		public static string PatientCalenderConsumptionQueryString(DateTime calendar_start_date, long institution_id, long registration_id, long hospital_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["calendar_start_date"] = calendar_start_date.ToMvcDateString ();
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["hospital_id"] = hospital_id.ToString ();
			return queryString.ToString ();
		}
		public static string PatientCalenderConsumptionParam(DateTime calendar_start_date, long institution_id, long registration_id, long ward_group_id)
		{
			return String.Format ("?calendar_start_date={0}&institution_id={1}&registration_id={2}&ward_group_id={3}", calendar_start_date, institution_id, registration_id, ward_group_id);
		}


		public static readonly KnownUrl GetPatientConsumptionInfo = new KnownUrl("EmosIpad/ConsumptionChecklist/GetPatientConsumptionInfo", HttpMethod.Post);
		public static object PatientConsumptionInfoContent(ConsumptionChecklistCalender consumptionChecklistCalender)
		{
			return new { consumptionChecklistCalender };
		}
		public static string PatientConsumptionInfoQueryString(long? registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static string PatientConsumptionInfoParam(long? registration_id, ConsumptionChecklistCalender consumptionChecklistCalender)
		{
			return String.Format ("?registration_id={0}&meal_period_id={1}&operation_code={2}&date={3}",registration_id,consumptionChecklistCalender.meal_period_id,consumptionChecklistCalender.operation_code,consumptionChecklistCalender.Date);
		}

		public static readonly KnownUrl GetPatientInfo = new KnownUrl("EmosIpad/Patient/GetPatientInfo", HttpMethod.Post);
		public static string PatientInfoQueryString(long registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static string PatientInfoParam(long registration_id)
		{
			return String.Format ("?registration_id={0}", registration_id);
		}

		public static readonly KnownUrl GetPatientMealOrderModel = new KnownUrl("EmosIpad/PatientMealOrder/GetPatientMealOrderModel", HttpMethod.Post);
		public static string GetPatientMealOrderModelQueryString(long? locationId, long? institutionId, DateTime date, long? hospitalId, string mealPeriodGroup)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["locationId"] = locationId.ToString ();
			queryString ["institutionId"] = institutionId.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["hospitalId"] = hospitalId.ToString ();
			queryString ["mealPeriodGroup"] = mealPeriodGroup;
			return queryString.ToString ();
		}
		public static string GetPatientMealOrderModelParam(long? locationId, long? institutionId, DateTime date, long? wardGroupId, string mealPeriodGroup)
		{
			return String.Format ("?locationId={0}&institutionId={1}&date{2}&wardGroupId={3}&mealPeriodGroup={4}", locationId, institutionId, date,wardGroupId,mealPeriodGroup);
		}

		public static readonly KnownUrl SaveConsumptionChecklist = new KnownUrl("EmosIpad/ConsumptionChecklist/SaveConsumptionChecklist", HttpMethod.Post);

		public static readonly KnownUrl GetMealTypeModel = new KnownUrl("EmosIpad/MealType/GetMealTypeModel", HttpMethod.Post);
		public static string GetMealTypeModelQueryString(long? registration_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registration_id"] = registration_id.ToString();
			queryString ["institution_id"] = institution_id.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetMealTypeWithFeed = new KnownUrl("EmosIpad/MealType/GetMealTypeWithFeed", HttpMethod.Post);
		public static string GetMealTypeWithFeedQueryString(long? registration_id, long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registration_id"] = registration_id.ToString();
			queryString ["institution_id"] = institution_id.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl SaveMealType = new KnownUrl("EmosIpad/MealType/Save", HttpMethod.Post);

		public static readonly KnownUrl GetUserAccessControl = new KnownUrl ("EmosIpad/User/GetUserAccessControl", HttpMethod.Post);
		public static string GetUserAccessControlQueryString(string username)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["username"] = username;
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetNurseWard = new KnownUrl ("EmosIpad/User/GetNurseWard", HttpMethod.Post);
		public static string GetNurseWardQueryString(string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetInterfaceStatus = new KnownUrl("EmosIpad/Integration/GetInterfaceStatus", HttpMethod.Post);

		public static readonly KnownUrl SendConsumptionToEPIC = new KnownUrl("eMOS/ConsumptionChecklist/SendDataToEPIC", HttpMethod.Post);
		public static string SendConsumptionToEPICQuerString(string data)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["data"] = data;
			return queryString.ToString ();
		}
		public static readonly KnownUrl DataSentToEpic = new KnownUrl("eMOS/ConsumptionChecklist/DataSentToEPIC", HttpMethod.Post);
		public static string DataSentToEpicWithChecklistId(long? consumptionChecklistid)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["consumption_checklist_id"] = consumptionChecklistid.ToString();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetNutrients = new KnownUrl("EmosIpad/ConsumptionChecklist/GetNutrients", HttpMethod.Post);
		public static string GetNutrientsQueryString(long? consumptionChecklistid)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["consumptionChecklistId"] = consumptionChecklistid.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetViewAdmission = new KnownUrl("EmosIpad/ViewAdmission/GetPatientInfo", HttpMethod.Post);
		public static string GetViewAdmissionQueryString(long registrationId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registrationId"] = registrationId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetPatientAllergies = new KnownUrl("EmosIpad/FoodAllergies/GetPatientFoodAllergies", HttpMethod.Post);
		public static string GetPatientAllergiesQueryString(long registrationId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registrationId"] = registrationId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetCompanionDashboard = new KnownUrl("EmosIpad/CompanionMealOrder/GetDashboardModel", HttpMethod.Post);
		public static string GetCompanionDashboardQueryString(string meal_period_group_code, long? institution_id, long? ward_id, DateTime date, long? hospital_id)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			queryString ["meal_period_group_code"] = meal_period_group_code;
			queryString ["institution_id"] = institution_id.ToString ();
			if (ward_id != null)
				queryString ["ward_id"] = ward_id.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["hospital_id"] = hospital_id.ToString ();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetCompanionMealOrderModel = new KnownUrl("EmosIpad/CompanionMealOrder/GetCompanionMealOrderModel", HttpMethod.Post);
		public static string GetCompanionMealOrderModelQueryString(long? companion_order_id, long? registration_id, long? meal_order_period_id, long? operation_code, DateTime date, bool patient_mode = false)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["companion_order_id"] = companion_order_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["meal_order_period_id"] = meal_order_period_id.ToString ();
			queryString ["operation_code"] = operation_code.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["is_patient_mode"] = patient_mode.ToString ();
			return queryString.ToString ();
		}

		public static readonly KnownUrl SaveCompanionOrder = new KnownUrl ("EmosIpad/CompanionMealOrder/SaveCompanionOrder", HttpMethod.Post);

		public static readonly KnownUrl GetBirthdayCakeModel = new KnownUrl("EmosIpad/BirthdayCake/GetBirthdayCakeModel", HttpMethod.Post);
		public static string GetBirthdayCakeModelQueryString(long registrationId, long institutionId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registrationId"] = registrationId.ToString();
			queryString ["institutionId"] = institutionId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl CancelBirthdayCake = new KnownUrl ("eMOS/BirthdayCake/CancelBirthdayCake", HttpMethod.Post);
		public static string CancelBirthdayCakeQueryString(int RegistrationId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["RegistrationId"] = RegistrationId.ToString();
			return queryString.ToString ();
		}

		public static string ProfileDietOrderIdQueryString(long profileDietOrderId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["profileDietOrderId"] = profileDietOrderId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl SaveBirthdayCake = new KnownUrl("EmosIpad/BirthdayCake/SaveBirthdayCake", HttpMethod.Post);
	
		public static readonly KnownUrl GetMealOrderDraftModel = new KnownUrl ("EmosIpad/MealOrderDraft/GetDraftModel", HttpMethod.Post);

		public static readonly KnownUrl SaveMealOrderDraft = new KnownUrl("EmosIpad/MealOrderDraft/SaveDraft", HttpMethod.Post);

		public static readonly KnownUrl GetExtraOrderViewModel = new KnownUrl("EmosIpad/ExtraOrder/GetExtraOrderViewModel", HttpMethod.Post);
	
		public static readonly KnownUrl GetTherapeuticViewModel = new KnownUrl("EmosIpad/TherapeuticDietOrder/GetTherapeuticViewModel", HttpMethod.Post);

		public static readonly KnownUrl GetFluidRestrictionViewModel = new KnownUrl("EmosIpad/FluidRestriction/GetFluidRestrictionViewModel", HttpMethod.Post);

		public static readonly KnownUrl DeleteProfileDietOrderRestrictions = new KnownUrl("eMOS/DietOrderAPI/DeactivateDietOrderRestrictionsAt",HttpMethod.Post);

		public static readonly KnownUrl GetExtraOrderEditModel = new KnownUrl("EmosIpad/ExtraOrder/GetExtraOrderEditModel", HttpMethod.Post);
		public static string GetExtraOrderEditModelBasedOnDateQueryString(long profileDietOrderId, long registrationId, DateTime date)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["profileDietOrderId"] = profileDietOrderId.ToString();
			queryString ["registrationId"] = registrationId.ToString();
			queryString ["date"] = date.ToMvcDateString ();
			return queryString.ToString ();
		}
		public static string GetExtraOrderEditModelQueryString(long profileDietOrderId, long registrationId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["profileDietOrderId"] = profileDietOrderId.ToString();
			queryString ["registrationId"] = registrationId.ToString();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetSnackPackViewModel = new KnownUrl ("EmosIpad/SnackPack/GetSnackPackViewModel",HttpMethod.Post);
	
		public static readonly KnownUrl SaveExtraOrder = new KnownUrl("EmosIpad/ExtraOrder/SaveExtraOrder", HttpMethod.Post);
	
		public static readonly KnownUrl GetTrialOrderViewModel = new KnownUrl("EmosIpad/TrialOrder/GetTrialOrderViewModel", HttpMethod.Post);

		public static readonly KnownUrl GetTrialOrderEditModel = new KnownUrl("EmosIpad/TrialOrder/GetTrialOrderEditModel", HttpMethod.Post);
		public static string GetTrialOrderEditModelQueryString(long registrationId, long institutionId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registrationId"] = registrationId.ToString();
			queryString ["institutionId"] = institutionId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl SaveTrialOrder = new KnownUrl ("EmosIpad/TrialOrder/Save", HttpMethod.Post);

		public static readonly KnownUrl GetCompanionOrders = new KnownUrl("EmosIpad/CompanionMealOrder/GetCompanionOrders", HttpMethod.Post);
		public static string GetCompanionOrdersQueryString(long registrationId, long institutionId, long mealPeriodId, long operationCode, DateTime date, bool patientMode = false)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["registrationId"] = registrationId.ToString();
			queryString ["institutionId"] = institutionId.ToString();
			queryString ["mealPeriodId"] = mealPeriodId.ToString();
			queryString ["operationCode"] = operationCode.ToString();
			queryString ["date"] = date.ToString("MM/dd/yyyy");
			queryString ["is_patient_mode"] = patientMode.ToString ();
			return queryString.ToString ();
		}

		public static readonly KnownUrl DeleteCompanionOrder = new KnownUrl("eMOS/MealOrderAPI/cancelOrder", HttpMethod.Post);
		public static string DeleteCompanionOrderQueryString(long companionOrderId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["companionOrderId"] = companionOrderId.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetUserAssignedLocations = new KnownUrl("EmosIpad/User/GetUserAssignedLocations", HttpMethod.Post);
		public static string GetUserAssignedLocationsQueryString(string username)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["username"] = username.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetFeedback = new KnownUrl("Feedback/Feedback/GetFeedback", HttpMethod.Post);
		public static string GetFeedbackQueryString(long question_set_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["question_set_id"] = question_set_id.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl GetEmosFeedback = new KnownUrl ("Feedback/Feedback/GetEmosFeedback", HttpMethod.Post);

		public static readonly KnownUrl SaveAnswerFromUser = new KnownUrl("Feedback/Feedback/SaveAnswers", HttpMethod.Post);

		public static readonly KnownUrl DeclineMealOrder = new KnownUrl("eMOS/MealOrderAPI/DeclineMealOrder", HttpMethod.Post);
		public static string DeclineMealOrderQueryString(long? institutionId, long? locationId, long? mealOrderId, long? menuCycleMenuId, long? mealOrderPeriodId, long? registrationId, DateTime? date)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["institutionId"] = institutionId.ToString();
			queryString ["locationId"] = locationId.ToString();
			queryString ["mealOrderId"] = mealOrderId.ToString();
			queryString ["menuCycleMenuId"] = menuCycleMenuId.ToString();
			queryString ["mealOrderPeriodId"] = mealOrderPeriodId.ToString();
			queryString ["registrationId"] = registrationId.ToString();
			queryString ["date"] = date.Value.ToMvcDateString();
			return queryString.ToString ();
		}
		public static readonly KnownUrl LockRegistration = new KnownUrl("EmosIpad/LockIpad/LockRegistration", HttpMethod.Post);
		public static string LockRegistrationQueryString(long id, string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["id"] = id.ToString();
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}
		public static readonly KnownUrl UnlockRegistration = new KnownUrl("EmosIpad/LockIpad/UnlockRegistration", HttpMethod.Post);
		public static string UnlockRegistrationQueryString(long id, string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["id"] = id.ToString();
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}
		public static readonly KnownUrl UnlockRegistrationsBy = new KnownUrl("EmosIpad/LockIpad/UnlockRegistrationsBy", HttpMethod.Post);
		public static string UnlockRegistrationsByQueryString(string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}
		public static readonly KnownUrl ExtendRegistrationLock = new KnownUrl("EmosIpad/LockIpad/ExtendRegistrationLock", HttpMethod.Post);
		public static string ExtendRegistrationLockQueryString(long id, string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["id"] = id.ToString();
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}
		public static readonly KnownUrl UnlockRegistrationsByUser = new KnownUrl("EmosIpad/LockIpad/UnlockRegistrationsByUser", HttpMethod.Post);
		public static string UnlockRegistrationsByUserQueryString(string userName)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString (string.Empty);
			queryString ["userName"] = userName;
			return queryString.ToString ();
		}
		public static readonly KnownUrl CheckUpdate = new KnownUrl("EmosIpad/EmosTheme/CheckUpdate", HttpMethod.Get);
		public static readonly KnownUrl GetLanguages = new KnownUrl("EmosIpad/Language/GetUserLanguages", HttpMethod.Post);
		public static readonly KnownUrl SavePatientLanguage = new KnownUrl("EmosIpad/Language/Save", HttpMethod.Post);
		public static readonly KnownUrl GetPatientSelectedLanguage = new KnownUrl("EmosIpad/Language/GetPatientSelectedLanguage", HttpMethod.Post);
		public static string GetPatientSelectedLanguageQuery(long? profile_id)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			if (profile_id != null)
				queryString ["profileId"] = profile_id.ToString ();
			return queryString.ToString ();
		}
		public static string GetThemeQueryString(long? institution_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["institution_id"] = institution_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetPatientModeNurseDashboardModel = new KnownUrl("EmosIpad/PatientDashboard/GetPatientModeDashboardModel", HttpMethod.Post);
		public static string GetPatientModeNurseDashboardQueryString(string meal_period_group_code, long? institution_id, long? ward_id, DateTime date, long? hospital_id, long? registration_id)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			queryString ["meal_period_group_code"] = meal_period_group_code;
			queryString ["institution_id"] = institution_id.ToString ();
			if (ward_id != null)
				queryString ["ward_id"] = ward_id.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["hospital_id"] = hospital_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetPatientModeLodgerDashboardModel = new KnownUrl("EmosIpad/PatientDashboard/GetPatientLodgerDashboardModel", HttpMethod.Post);
		public static readonly KnownUrl GetCommonDietOrderV2Model = new KnownUrl ("EmosIpad/CommonDietOrder/GetCommonDietOrderModelV2", HttpMethod.Post);
		public static readonly KnownUrl SaveCommonDietOrderV2 = new KnownUrl("EmosIpad/CommonDietOrder/SaveV2", HttpMethod.Post);
		public static readonly KnownUrl GetCommonDietOrderListViewModel = new KnownUrl("EmosIpad/CommonDietOrder/GetCommonDietOrderListViewModel", HttpMethod.Post);
		public static string GetCommonDietOrderQueryV2String(string diet_order_code, long? institution_id, long? registration_id, long? profileDietId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["diet_order_code"] = diet_order_code;
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["profile_diet_order_id"] = profileDietId.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetExtraOrderV2Model = new KnownUrl("EmosIpad/ExtraOrder/GetExtraOrderEditV2Model", HttpMethod.Post);
		public static readonly KnownUrl SaveExtraOrderV2 = new KnownUrl("EmosIpad/ExtraOrder/SaveExtraOrderV2", HttpMethod.Post);

		public static readonly KnownUrl GetBulkOrderSummary = new KnownUrl("EmosIpad/BulkOrder/GetBulkOrderSummary", HttpMethod.Get);
		public static string GetBulkOrderSummaryQueryString(long? institution_id, string location_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["institution_id"] = institution_id.ToString ();
			queryString ["location_id"] = location_id;
			queryString ["filter"] = "MEAL";
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetBulkOrderStructure = new KnownUrl("EmosIpad/BulkOrder/GetBulkOrderStructure", HttpMethod.Get);
		public static string GetBulkOrderStructureQueryString(string location_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["location_id"] = location_id;
			return queryString.ToString ();
		}
		public static readonly KnownUrl SaveBulkOrder = new KnownUrl("EmosIpad/BulkOrder/Save", HttpMethod.Post);

		#region Feedback
		public static string GetFeedbackDashboardQueryString
		(long? institution_id, long? ward_id, DateTime date, long? hospital_id)
		{
			var queryString = HttpUtility.ParseQueryString(string.Empty);
			queryString ["institution_id"] = institution_id.ToString ();
			if (ward_id != null)
				queryString ["ward_id"] = ward_id.ToString ();
			queryString ["date"] = date.ToMvcDateString ();
			queryString ["hospital_id"] = hospital_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl GetFeedbackDashboardModel = 
			new KnownUrl("EmosIpad/NurseDashboard/GetFeedbackDashboardModel", HttpMethod.Post);

		public static readonly KnownUrl GetFeedbackQuestionModel = 
			new KnownUrl("EmosIpad/Feedback/GetFeedback", HttpMethod.Post);
		public static string GetFeedbackQuestionByRegistration(long? registration_id, bool startOver = false)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			queryString ["startOver"] = startOver.ToString ();
			return queryString.ToString ();
		}
		public static string GetFeedbackDraftByRegistration(long? registration_id)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["registration_id"] = registration_id.ToString ();
			return queryString.ToString ();
		}
		public static readonly KnownUrl SaveFeedbackAnswerV2 = new KnownUrl("EmosIpad/Feedback/SaveFeedbackAnswerV2", HttpMethod.Post);
		public static readonly KnownUrl SaveFeedbackDraft = new KnownUrl("EmosIpad/Feedback/SaveFeedbackDraft", HttpMethod.Post);
		public static readonly KnownUrl CheckIfDraftExist = new KnownUrl("EmosIpad/Feedback/CheckIfFeedbackDraftExist", HttpMethod.Post);

		public static readonly KnownUrl GetCareGiverURL = new KnownUrl("EmosIpad/WebPortalRegistration/GetCareGiver", HttpMethod.Post);
		public static readonly KnownUrl SaveCaregiver = new KnownUrl("EmosIpad/WebPortalRegistration/SaveCaregiver", HttpMethod.Post);

		public static readonly KnownUrl GetUpdateChargeableStatusURL = new KnownUrl("EmosIpad/CompanionMealOrder/ChangeChargeableStatus", HttpMethod.Post);
		public static string GetCompanionChargeableQuery(long? companionMealOrdeId)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString ["companionMealOrderId"] = companionMealOrdeId.ToString ();
			return queryString.ToString ();
		}
		#endregion
	}
}

