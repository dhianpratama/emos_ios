﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public static class UiGenerator
	{
		public static UILabel CreateHeader (string title, RectangleF frame, UIFont font)
		{
			var label = new UILabel (frame);
			label.Text = title;
			label.Font = font;
			return label;
		}
		public static UIDatePickerButton CreateDatePickerButton(IApplicationContext appContext, UIView parentView, UIColor borderColor, string title, RectangleF defaultFrame, UIView aboveView, float distanceToAbove = 0, bool limitToSevenDays=false)
		{
			var result = new UIDatePickerButton (appContext, parentView, title, defaultFrame, limitToSevenDays);
			result.SetTitle (title, UIControlState.Normal);
			result.SetTitleColor (UIColor.Black, UIControlState.Normal);
			result.SetFrameBelowTo (aboveView, distanceToAbove: distanceToAbove);
			result.Layer.BorderColor = borderColor.CGColor;
			result.Layer.BorderWidth = 2.0f;
			result.Layer.CornerRadius = 10;
			return result;
		}

		public static RoundedDatePickerButton CreateRoundedDatePickerButton(IApplicationContext appContext, UIView parentView, RectangleF defaultFrame, UIView aboveView, float captionWidth, string title, bool editable, float distanceToAbove = 0, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone, bool limitToSevenDays=false)
		{
			var result = new RoundedDatePickerButton (appContext, parentView, defaultFrame, captionWidth, title, editable, captionRectCorner, textFieldRectCorner);
			result.LimitToSevenDays = limitToSevenDays;
			result.SetFrameBelowTo (aboveView, distanceToAbove: distanceToAbove);
			return result;
		}
	}
}

