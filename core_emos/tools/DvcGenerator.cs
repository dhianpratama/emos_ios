﻿using System;
using MonoTouch.Dialog;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public class DvcGenerator
	{
		public static Section PatientInfoShort(IApplicationContext appContext, RegistrationModelSimple registration, bool showCaseNumber = true)
		{
			var patientInfoText = appContext.LanguageHandler.GetLocalizedString ("Patientinfo");
			var nameText = appContext.LanguageHandler.GetLocalizedString ("Name");
			var idText = appContext.LanguageHandler.GetLocalizedString ("Identifier");
			var section = new Section (patientInfoText) {
				new EntryElementDVC (nameText, "", registration.profile_name, true),
				new EntryElementDVC (idText, "", registration.profile_identifier, true),
			};
			if (showCaseNumber) {
				var caseNumberText = appContext.LanguageHandler.GetLocalizedString ("CaseNumber");
				var caseNumber = new EntryElementDVC (caseNumberText, "", registration.registration_number, true);
				section.Add (caseNumber);
			}
			return section;
		}
		public static Section PatientInfoWithFeed(IApplicationContext appContext, RegistrationModelSimple registration, bool showFeed, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed)
		{
			var result = PatientInfoShort (appContext, registration);
			if (!showFeed)
				return result;
			var clearFeedTitle = appContext.LanguageHandler.GetLocalizedString ("ClearFeeds");
			var fullFeedTitle = appContext.LanguageHandler.GetLocalizedString ("FullFeeds");
			result.Add(new EntryElementDVC(clearFeedTitle, "", GetDietOrderDate(appContext, clearFeed), true));
			result.Add(new EntryElementDVC(fullFeedTitle, "", GetDietOrderDate(appContext, fullFeed), true));
			return result;
		}
		private static string GetDietOrderDate (IApplicationContext appContext, CommonDietOrderModel dietOrder)
		{
			var notSpecified = appContext.LanguageHandler.GetLocalizedString ("Notspecified");
			var startDate = dietOrder == null || dietOrder.start_date == null ? "" : dietOrder.start_date.Value.ToDisplayDateString ();
			var endDate = dietOrder == null || dietOrder.end_date == null ? "" : dietOrder.end_date.Value.ToDisplayDateString ();
			return String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate) ? notSpecified : String.Format ("{0} - {1}", startDate, endDate);
		}
	}
}

