﻿using System;
using MonoTouch.UIKit;

namespace core_emos
{
	public class ImageLibrary
	{
		public static UIImage GetMealPeriodImage(bool isCompanion, string mealOrderPeriod)
		{
			if (isCompanion)
				return GetCompanionMealPeriodImage (mealOrderPeriod);
			else
				return GetPatientMealPeriodImage (mealOrderPeriod);
		}
		private static UIImage GetPatientMealPeriodImage (string mealOrderPeriod)
		{
			switch (mealOrderPeriod) {
			case "Breakfast":
				return UIImage.FromFile ("Images/header_breakfast.png");
			case "Lunch":
				return UIImage.FromFile ("Images/header_lunch.png");
			case "Dinner":
				return UIImage.FromFile ("Images/header_dinner.png");
			case "Afternoon Tea Break":
				return UIImage.FromFile ("Images/header_afternoon_tea.png");
			case "Morning Tea Break":
				return UIImage.FromFile ("Images/header_morning_tea.png");
			case "Tea Break":
				return UIImage.FromFile ("Images/header_tea_break.png");
			case "Supper":
				return UIImage.FromFile ("Images/header_supper.png");
			};
			return UIImage.FromFile ("Images/img-not-found.png");
		}
		private static UIImage GetCompanionMealPeriodImage (string mealOrderPeriod)
		{
			switch (mealOrderPeriod) {
			case "Breakfast":
				return UIImage.FromFile ("Images/header_breakfast_companion.png");
			case "Lunch":
				return UIImage.FromFile ("Images/header_lunch_companion.png");
			case "Dinner":
				return UIImage.FromFile ("Images/header_dinner_companion.png");
			};
			return UIImage.FromFile ("Images/img-not-found.png");
		}
	}
}

