﻿using System;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using ios;
using MonoTouch.UIKit;

namespace core_emos
{
	public interface ILock
	{
		void PatientMenuLockSuccessful (LockResultIpad result);
		void PatientMenuLockFailed (LockResultIpad result);
		void MealOrderLockSuccessful (LockResultIpad result);
		void MealOrderLockFailed (LockResultIpad result);
		void OnLockRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true);
		void UnlockSuccessful ();
		void UnlockFailed ();
		void OnUnlockRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true);
		void OnLockNotExtended ();
		void OnUnlockLocksByUserRequestCompleted (bool success);
	}
	public class LockHandler
	{
		private const long NoLock = -9999;
		private const int LockTimeOutSeconds = 300;
		private const int ConfirmationTimeoutSeconds = 60;
		private const int ThresholdSeconds = 15;
		private IApplicationContext _appContext;
		private ILock _callback;
		public LockResultIpad LockResult { get; set; }
		public long RegistrationId { get; private set; }
		private bool _checkLock { get; set; }
		private TimerHandler _lockTimerHandler;
		private TimerHandler _confirmationTimerHandler;
		private UIView _view;
		private RequestHandler _requestHandler;
		private DateTime _lockStartTime;
		private UIAlertView _confirmationAlert;

		public LockHandler (IApplicationContext appContext, bool checkLock)
		{
			_appContext = appContext;
			_checkLock = checkLock;
			LockResult = new LockResultIpad { free_to_access = !checkLock };
			RegistrationId = NoLock;
			InitializeLockTimer ();
			InitializeConfirmationTimer ();
		}
		private void InitializeLockTimer ()
		{
			_lockTimerHandler = new TimerHandler ();
			_lockTimerHandler.OnTick += HandleOnLockTimerTick;
		}
		private void HandleOnLockTimerTick (object sender, EventArgs e)
		{
			_lockTimerHandler.Stop ();
			_confirmationAlert = _view.ShowAlert (_appContext, "Information", "Your session is about to expire. Press OK to extend", "OK", ExtendRegistrationLock, false);
		}
		private int GetLockExpirySeconds()
		{
			return LockTimeOutSeconds + ConfirmationTimeoutSeconds - ThresholdSeconds;
		}
		private void InitializeConfirmationTimer ()
		{
			_confirmationTimerHandler = new TimerHandler ();
			_confirmationTimerHandler.OnTick += HandleOnConfirmationTimerTick;
		}
		void HandleOnConfirmationTimerTick (object sender, EventArgs e)
		{
			DoNotExtendLock ();
			if (_confirmationAlert != null)
				_confirmationAlert.DismissWithClickedButtonIndex (0, false);
			_view.ShowAlert (_appContext, "Your lock is expired", translateMessage: false);
		}
		public void ExtendRegistrationLock ()
		{
			StopTimers ();
			_requestHandler.SendRequest (_view, () => SendExtendRegistrationLockRequest (RegistrationId));
		}
		private void DoNotExtendLock ()
		{
			StopTimers ();
			UnlockRegistration ();
			_callback.OnLockNotExtended ();
		}
		public void LockRegistration(ILock callback, long id, LockRequestCode lockRequestCode, RequestHandler requestHandler, UIView view)
		{
			_callback = callback;
			_view = view;
			_requestHandler = requestHandler;
			LockResult = new LockResultIpad { free_to_access = !_checkLock };
			if (!_checkLock) {
				RegistrationId = NoLock;
				return;
			}
			RegistrationId = id;
			requestHandler.SendRequest (view, () => SendLockRequest (id, lockRequestCode));
		}
		private async void SendLockRequest (long id, LockRequestCode lockRequestCode)
		{
			var param = emos_ios.KnownUrls.LockRegistrationQueryString (id, _appContext.CurrentUser.Id);
			await _appContext.HttpSender.Request<LockResultIpad> ()
				.From (emos_ios.KnownUrls.LockRegistration)
				.WithQueryString (param)
				.WhenSuccess (result => OnLockRequestSuccessful (result, lockRequestCode))
				.WhenFail (result => OnLockRequestFailed ())
				.Go ();
		}
		private async void SendExtendRegistrationLockRequest (long id)
		{
			var param = emos_ios.KnownUrls.ExtendRegistrationLockQueryString (id, _appContext.CurrentUser.Id);
			await _appContext.HttpSender.Request<LockResultIpad> ()
				.From (emos_ios.KnownUrls.ExtendRegistrationLock)
				.WithQueryString (param)
				.WhenSuccess (result => OnExtendLockRequestSuccessful (result))
				.WhenFail (result => OnLockRequestFailed ())
				.Go ();
		}
		private void OnLockRequestSuccessful (LockResultIpad result, LockRequestCode lockRequestCode)
		{
			LockResult = result;
			if (result.OperationInfoIpad.success && result.OperationInfoIpad.executed && result.free_to_access) {
				_lockStartTime = DateTime.UtcNow;
				_lockTimerHandler.Restart (_lockStartTime, LockTimeOutSeconds - ThresholdSeconds);
				_confirmationTimerHandler.Restart (_lockStartTime, GetLockExpirySeconds ());
				switch (lockRequestCode) {
				case LockRequestCode.PatientMenu:
					_callback.PatientMenuLockSuccessful (result);
					break;
				case LockRequestCode.MealOrder:
					_callback.MealOrderLockSuccessful (result);
					break;
				}
			} else {
				switch (lockRequestCode) {
				case LockRequestCode.PatientMenu:
					_callback.PatientMenuLockFailed (result);
					break;
				case LockRequestCode.MealOrder:
					_callback.MealOrderLockFailed (result);
					break;
				}
			}
			_callback.OnLockRequestCompleted (true);
		}
		private void OnExtendLockRequestSuccessful (LockResultIpad result)
		{
			LockResult = result;
			if (result.OperationInfoIpad.success && result.OperationInfoIpad.executed && result.free_to_access) {
				_lockStartTime = DateTime.UtcNow;
				_lockTimerHandler.Restart (_lockStartTime, LockTimeOutSeconds - ThresholdSeconds);
				_confirmationTimerHandler.Restart (_lockStartTime, GetLockExpirySeconds ());
			} else {
				_callback.OnLockNotExtended ();
			}
			_callback.OnLockRequestCompleted (true);
		}
		private void OnLockRequestFailed ()
		{
			_callback.OnLockRequestCompleted (false, "Lock");
		}
		public async void UnlockRegistration ()
		{
			if (RegistrationId == NoLock || !LockResult.free_to_access || !_checkLock)
				return;
			LockResult.free_to_access = false;
			StopTimers ();
			var param = emos_ios.KnownUrls.UnlockRegistrationQueryString (RegistrationId, _appContext.CurrentUser.Id);
			await _appContext.HttpSender.Request<OperationInfoIpad> ()
				.From (emos_ios.KnownUrls.UnlockRegistration)
				.WithQueryString (param)
				.WhenSuccess (result => OnUnlockRequestSuccessful (result))
				.WhenFail (result=> OnUnlockRequestFailed())
				.Go ();
		}
		private void StopTimers ()
		{
			_lockTimerHandler.Stop ();
			_confirmationTimerHandler.Stop ();
		}
		private void OnUnlockRequestSuccessful (OperationInfoIpad result)
		{
			if (result.success && result.executed) {
				RegistrationId = NoLock;
				_callback.UnlockSuccessful ();
			} else
				_callback.UnlockFailed ();
			_callback.OnUnlockRequestCompleted (true);
		}
		private void OnUnlockRequestFailed()
		{
			_callback.OnUnlockRequestCompleted (false);
		}
		public async void UnlockRegistrationsByUser ()
		{
			StopTimers ();
			if (RegistrationId == NoLock || !LockResult.free_to_access || !_checkLock)
				return;
			LockResult.free_to_access = false;
			var param = emos_ios.KnownUrls.UnlockRegistrationsByQueryString (_appContext.CurrentUser.Id);
			await _appContext.HttpSender.Request<OperationInfoIpad> ()
				.From (emos_ios.KnownUrls.UnlockRegistrationsBy)
				.WithQueryString (param)
				.WhenSuccess (result => OnUnlockRegistrationsByUserSuccessful (result))
				.WhenFail (result=> OnUnlockRegistrationsByUserRequestFailed())
				.Go ();
		}
		private void OnUnlockRegistrationsByUserSuccessful (OperationInfoIpad result)
		{
			if (result.success && result.executed) {
				RegistrationId = NoLock;
				_callback.UnlockSuccessful ();
			} else
				_callback.UnlockFailed ();
			_callback.OnUnlockLocksByUserRequestCompleted (true);
		}
		private void OnUnlockRegistrationsByUserRequestFailed()
		{
			_callback.OnUnlockLocksByUserRequestCompleted (false);
		}
	}
	public enum LockRequestCode
	{
		MealOrder, PatientMenu
	}
}

