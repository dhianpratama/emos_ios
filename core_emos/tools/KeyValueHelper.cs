﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public static class KeyValueHelper
	{
		public static KeyValuePair<long?, string> GenerateKeyValue (List<BaseIpadModel> list, long id)
		{
			var entity = list.First(d => d.id == id);
			return new KeyValuePair<long?, string>(entity.id, entity.label);
		}
	}
}

