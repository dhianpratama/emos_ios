using System.Collections.Generic;
using MonoTouch.Dialog;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using System;
using emos_ios;
using ios;

public class CheckboxGroupDVC<T> : Section where T : BaseIpadModel
{
	public event EventHandler<BaseEventArgs<T>> OnItemSelected;
	private string _caption;
	private List<BooleanElement> _allCheckboxElements;
	public List<T> Items { get; private set; }
	public event EventHandler<IdEventArgs> OnCheckboxSelected;
	public List<T> Result = new List<T>();

	public CheckboxGroupDVC(string caption, List<T> selectedItems, List<T> allItems) : base(caption)
	{
		_caption = caption;
		Items = allItems ?? new List<T> ();
		Items.ForEach (a => {
			if (selectedItems.Any (e => e.id == a.id))
				Result.Add (a);
		});
	}

	public Section Create()
	{
		Section _section = new Section (_caption) {

		};

		_markCurrentItems ();
		_allCheckboxElements = new List<BooleanElement> ();
		Items.ForEach (a => {
//				CheckboxDVC c = new CheckboxDVC (a.label);
//				c.Tapped += delegate {
//					a.active_data = !a.active_data;
//					OnTapped();
//				};
			BooleanElement c = new BooleanElement (a.label, a.active_data);
			SetValueChangedDelegate (c, a);
			_allCheckboxElements.Add (c);
		});

		_section.AddAll (_allCheckboxElements);

		return _section;
	}
	private void SetValueChangedDelegate (BooleanElement booleanElement, T item)
	{
		booleanElement.ValueChanged += delegate {
			item.active_data = !item.active_data;
			OnTapped (item);
			OnCheckboxSelected.SafeInvoke (booleanElement, new IdEventArgs (item.id));
		};
	}
	private void _markCurrentItems()
	{
		Items
			.ForEach (item => {
				if(Result.Any(r=> r.id==item.id))
					item.active_data = true;
		});
	}
	public void Deselect()
	{
		_allCheckboxElements.ForEach (e => e.Value = false);
	}
	public void Select(long id)
	{
		var index = Items.FindIndex (i => i.id == id);
		_allCheckboxElements [index].Value = true;
	}
	public void Deselect (long? collidedId)
	{
		var index = Items.FindIndex (i => i.id == collidedId);
		if (index < 0)
			return;
		_allCheckboxElements [index].Value = false;
	}
	private void OnTapped(T item)
	{
		if (item.active_data) {
			var a = Result.FirstOrDefault (e => e.id == item.id);
			if (a == null)
				Result.Add (item);
			OnItemSelected.SafeInvoke (this, new BaseEventArgs<T>().Initiate(item));
		} else {
			var a = Result.FirstOrDefault (e => e.id == item.id);
			if (a != null) {
				Result.Remove (a);
			}
		}
		Result = Result.Distinct ().ToList ();
	}
	public void ChangeState (int index, bool state)
	{
		var element = _allCheckboxElements [index];
		element.ValueChanged += null;
		_allCheckboxElements [index].Value = state;
		SetValueChangedDelegate (element, Items [index]);
	}
	public string GetLabel (int index)
	{
		try{
			var element = Items [index];
			return element.label;
		} catch(Exception){
			return "";
		}
	}
	public bool GetOnOff (int index)
	{
		try{
			var element = _allCheckboxElements [index];
			return element.Value;
		} catch(Exception){
			return false;
		}
	}

}