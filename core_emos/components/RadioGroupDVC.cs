using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class RadioGroupDVC<T>  where T : BaseIpadModel
	{
		public RadioGroupDVC()
		{

		}

		public RootElement CreateRadioGroup(string caption, long? model_id, List<T> list)
		{
			var section = new Section ();
			list.ForEach (r => section.Add(new MyRadioElement (r.label)));
			RootElement result = new RootElement (caption, new RadioGroup (GetSequence(model_id, list))) {
				section
			};

			return result;
		}


		private int GetSequence(long? _id, List<T> list)
		{
			int result = 0;
			if (list.Count > 0) {
				for (int i = 0; i < list.Count; i++) {
					if (list [i].id == _id) {
						result = i;
						break;
					}
				}
			}
			return result;
		}
	}
}

