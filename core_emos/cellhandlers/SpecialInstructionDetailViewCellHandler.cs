﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class ExtraOrderMealPeriodV2CellHandler : BaseViewCellHandler<ExtraOrderMealPeriodModelSimple>
	{
		public ExtraOrderMealPeriodV2CellSetting setting;
		public ExtraOrderMealPeriodV2CellHandler (string cellIdentifier, ISpecialInstructionDetailV2Callback callback, ExtraOrderMealPeriodV2CellSetting setting) : base(cellIdentifier, callback, setting)
		{
			this.setting = setting;
		}
	}

	public class ExtraOrderMealPeriodV2CellSetting : BaseViewCellSetting
	{
		public int MaximumQuantity { get; set; }
		public int MinimumQuantity { get; set; }

		public ExtraOrderMealPeriodV2CellSetting() {
			MaximumQuantity = 100;
			MinimumQuantity = 0;
		}
	}
}

