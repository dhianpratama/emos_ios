using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public class PatientOrderCommandViewCellHandler : BaseViewCellHandler<PatientOrderCommand>
	{
		public PatientDayOrderViewCellSetting Setting { get; set; }
		public PatientOrderCommandViewCellHandler (string cellIdentifier, IPatientOrderCommandCallback callback, PatientDayOrderViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
			Setting = setting;
		}
		public static List<PatientOrderCommand> CreateOneDayPatientOrderCommands (IApplicationContext appContext, LocationWithRegistrationSimple location, RegistrationModelSimple registration, DateTime operationDate, ConsumerTypes consumerType = ConsumerTypes.Patient)
		{
			int index = 0;
			var patientOrderCommands = new List<PatientOrderCommand> ();
			location.registrations [0].mealOrderPeriods.ForEach (m =>  {
				var patientOrderCommand = CreatePatientOrderCommand (appContext, index++, location, registration, consumerType, m);
				patientOrderCommands.Add (patientOrderCommand);
			});

			return patientOrderCommands;
		}
		public static PatientOrderCommand CreatePatientOrderCommand (IApplicationContext appContext, int index, LocationModelSimple location, RegistrationModelSimple registration, ConsumerTypes consumerType, MealOrderPeriodWithOperationCode mealOrder)
		{
			var operationCodeStatus = appContext.OperationCodeHandler.GetStatus (mealOrder.operation_code);
			var orderBy = "";
			if (OperationCodeHandler.HasOrderBy (mealOrder.operation_code)) {
				orderBy = mealOrder.order_by;
			}
			var operationDate = mealOrder.operation_date == DateTime.MinValue ? appContext.OperationDate : DateTime.SpecifyKind (mealOrder.operation_date, DateTimeKind.Utc);
			return new PatientOrderCommand {
				Index = index,
				ConsumerType = consumerType,
				Value = operationCodeStatus.Status,
				ImagePath = operationCodeStatus.ImagePath,
				OrderBy = orderBy,
				OperationCode = mealOrder.operation_code,
				OperationDate = operationDate,
				Registration = registration,
				Location = location,
				MealOrderPeriod = (MealOrderPeriodModelSimple)mealOrder,
				CompanionStatus = mealOrder.companion_order_status
			};
		}
	}
}

