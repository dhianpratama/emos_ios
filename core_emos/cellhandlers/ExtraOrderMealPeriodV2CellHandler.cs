﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class SpecialInstructionDetailViewCellHandler : BaseViewCellHandler<SpecialInstructionDetail>
	{
		public SpecialInstructionDetailViewCellHandler (string cellIdentifier, ISpecialInstructionDetailCallback callback, NameQuantityViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

