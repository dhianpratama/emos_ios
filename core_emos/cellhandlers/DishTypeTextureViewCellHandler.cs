﻿using System;

namespace emos_ios
{
	public class DishTypeTextureViewCellHandler : BaseViewCellHandler<DishTypeTexture>
	{
		public DishTypeTextureViewCellHandler (string cellIdentifier, IDishTypeTextureCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

