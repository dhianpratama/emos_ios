﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class ConsumptionChecklistCalenderViewCellHandler : BaseViewCellHandler<ConsumptionChecklistCalender>
	{
		public ConsumptionChecklistCalenderViewCellHandler (string cellIdentifier, IConsumptionChecklistCalenderCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

