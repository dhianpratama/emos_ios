﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class ConsumptionChecklistCalenderRowViewCellHandler: BaseViewCellHandler<MealPeriodForCalender>
	{
		public ConsumptionChecklistCalenderRowViewCellSetting Setting;
		public ConsumptionChecklistCalenderRowViewCellHandler (string cellIdentifier, IConsumptionChecklistCalenderRowCallback callback, ConsumptionChecklistCalenderRowViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
			Setting = setting;
		}
	}

	public class ConsumptionChecklistCalenderRowViewCellSetting : BaseViewCellSetting
	{
		public int ColumnCount { get; set; }
		public float CalendarLeft { get; set; }
	}
}

