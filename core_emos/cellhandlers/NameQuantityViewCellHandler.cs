﻿using System;

namespace emos_ios
{
	public class NameQuantityViewCellHandler<T> : BaseViewCellHandler<T>
	{
		public NameQuantityViewCellSetting Setting { get; set;}
		public NameQuantityViewCellHandler(string cellIdentifier, INameQuantityCallback<T> callback, NameQuantityViewCellSetting viewCellSetting) : base(cellIdentifier, callback, viewCellSetting)
		{
			Setting = viewCellSetting;
		}
	}
	public class NameQuantityViewCellSetting : BaseViewCellSetting
	{
		public bool QuantityLabelHidden {get; set;}
		public bool QuantityTextHidden {get; set;}
		public float QuantityLabelX {get; set;}
		public bool ShowQuantityOnly { get; set;}
		public int MaximumQuantity { get; set; }
		public int MinimumQuantity { get; set; }

		public NameQuantityViewCellSetting() {
			MaximumQuantity = 100;
			MinimumQuantity = 0;
		}
	}

}

