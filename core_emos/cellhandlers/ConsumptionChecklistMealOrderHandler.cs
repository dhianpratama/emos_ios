﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class ConsumptionChecklistMealOrderHandler : BaseViewCellHandler<DishModelSimple>
	{
		public ConsumptionChecklistMealOrderHandler (string cellIdentifier, IConsumptionChecklistMealOrderCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

