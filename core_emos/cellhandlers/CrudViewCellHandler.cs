﻿using System;

namespace emos_ios
{
	public class CrudViewCellHandler<T> : BaseViewCellHandler<T>
	{
		public CrudViewCellHandler (string cellIdentifier, ICrud<T> callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

