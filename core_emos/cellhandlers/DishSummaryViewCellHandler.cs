﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class DishSummaryViewCellHandler : BaseViewCellHandler<DishModelSimple>
	{
		public DishSummaryViewCellHandler (string cellIdentifier, BaseViewCellSetting setting) : base(cellIdentifier, null, setting)
		{
		}
	}
}

