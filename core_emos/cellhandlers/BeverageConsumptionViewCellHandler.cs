﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class BeverageConsumptionViewCellHandler : BaseViewCellHandler<ExtraBeverage>
	{
		public BeverageConsumptionViewCellHandler (string cellIdentifier, IBeverageConsumptionCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

