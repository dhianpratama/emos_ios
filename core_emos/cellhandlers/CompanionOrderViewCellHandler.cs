﻿using System;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class CompanionOrderViewCellHandler : BaseViewCellHandler<CompanionOrder>
	{
		public CompanionOrderViewCellHandler (string cellIdentifier, ICompanionOrderListCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

