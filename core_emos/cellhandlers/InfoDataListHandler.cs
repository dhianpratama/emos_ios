﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class InfoDataListHandler : BaseViewCellHandler<List<KeyValuePair<string, string>>>
	{
		public InfoDataListHandler (string cellIdentifier, ICommonDataListCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

