﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class MealOrderViewCellHandler : BaseViewCellHandler<DishModelWithTags>
	{
		public MealOrderViewCellHandler (string cellIdentifier, IMealOrderCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
	public class MealOrdersViewCellHandler : BaseViewCellHandler<List<DishModelWithTags>>
	{
		public MealOrdersViewCellHandler (string cellIdentifier, IMealOrdersCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

