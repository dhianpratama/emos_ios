﻿using System;
using System.Drawing;
using VMS_IRIS.Areas.EmosIpad.Models;
using MonoTouch.UIKit;
using System.Collections.Generic;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class PatientDayOrderViewCellHandler : BaseViewCellHandler<LocationWithRegistrationSimple>
	{
		public PatientDayOrderViewCellSetting Setting { get; set; }
		public PatientDayOrderViewCellHandler (string cellIdentifier, IPatientDayOrderCallback callback, PatientDayOrderViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
			Setting = setting;
		}
	}

	public class PatientDayOrderViewCellSetting : BaseViewCellSetting
	{
		public RectangleF OrderContainerFrame { get; set; }

		public PatientDayOrderViewCellSetting ()
		{
		}
	}
}

