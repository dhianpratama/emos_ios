﻿using System;
using emos_ios;
using core_emos;

namespace emos_ios
{
	public class DailyPatientMealViewCellHandler : BaseViewCellHandler<PatientOrderCommand>
	{
		public DailyPatientMealViewCellHandler (string cellIdentifier, IPatientOrderCommandCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
	public class DailyPatientMealCalendarViewCellHandler : BaseViewCellHandler<MealCalendar>
	{
		public DailyPatientMealCalendarViewCellSetting Setting {get; set;}
		public DailyPatientMealCalendarViewCellHandler (string cellIdentifier, IDailyPatientMealCallback callback, DailyPatientMealCalendarViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
			Setting = setting;
		}
	}
	public class DailyPatientMealCalendarViewCellSetting : BaseViewCellSetting
	{
		public float ChildMealViewCellHeight { get; set; }
		public float HeaderHeight { get; set; }
		public DailyPatientMealCalendarViewCellSetting ()
		{
		}
	}

}
