﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public static class BaseIpadModelExtensions
	{
		public static T FillBaseIpadModel<T>(this T model, BaseIpadModel value) where T : BaseIpadModel
		{
			model.id = value.id;
			model.code = value.code;
			model.label = value.label;
			model.active_data = value.active_data;
			return model;
		}
	}
}

