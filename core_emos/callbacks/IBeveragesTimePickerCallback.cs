﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IBeveragesTimePickerCallback
	{
		void OpenBeveragesTimePicker(ExtraBeverage item, int? index);
	}
}

