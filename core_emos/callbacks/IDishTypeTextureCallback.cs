﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public interface IDishTypeTextureCallback : ICallback<DishTypeTexture>
	{
		void ValueChanged(DishTypeTexture model);
		List<KeyValuePair<string, string>> GetTextures();
		void HandleOnTextureSelected (DishTypeTexture _item);
	}
}

