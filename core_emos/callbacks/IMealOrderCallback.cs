﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IMealOrderCallback : ICallback<DishModelWithTags>
	{
		void FilterDish(int dishTypeIndex, int tagIndex);
		void ShowAllAvailableDish();
		void ShowSuitableDishesOnly();
		bool NonSuitableDishSelected ();
		void NextStep ();
		bool IsPatient { get; set; }
		bool CanOrder ();
		bool ShowAvailability ();
		RegistrationModelSimple GetRegistration ();
		MealOrderPeriodModelSimple GetMealOrderPeriod ();
	}
	public interface IMealOrdersCallback : ICallback<List<DishModelWithTags>>
	{
		void OrderDish (List<DishModelWithTags> dish);
		void ShowDetail (List<DishModelWithTags> dish);
		void CancelDishOrder (List<DishModelWithTags> dish);
		void ShowAllAvailableDish ();
		void ShowSuitableDishesOnly ();
		bool OnlySuitable { get; }
	}
}

