﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public interface IDailyPatientMealCallback : ICallback<MealCalendar>
	{
		void OrderButtonClicked(PatientOrderCommand item);
	}
}

