﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ICommonDataListCallback : ICallback<List<KeyValuePair<string,string>>>
	{
		void OnEdit(long id);
		void OnDelete(long id);
		void Refresh();
		bool EditEnabled { get; }
		InterfaceStatusModel GetInterfaceStatus ();
	}
}

