﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ISpecialInstructionDetailCallback : ICallback<SpecialInstructionDetail>
	{
		void DetailQuantityChanged(SpecialInstructionDetail item);
		void DetailSelectionChanged(SpecialInstructionDetail item);
		bool GetAllowChangeQuantity ();
	}
	public interface ISpecialInstructionDetailV2Callback : ICallback<ExtraOrderMealPeriodModelSimple>
	{
		void DetailQuantityChanged(ExtraOrderMealPeriodModelSimple item);
		void DetailSelectionChanged(ExtraOrderMealPeriodModelSimple item);
		bool GetAllowChangeQuantity ();
	}
}

