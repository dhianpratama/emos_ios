﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ILocationSelectionCallback : ICallback<LocationModelSimple>
	{
		void OpenModal();
	}
}

