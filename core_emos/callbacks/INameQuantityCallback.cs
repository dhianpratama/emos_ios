﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface INameQuantityCallback<T> : ICallback<T>
	{
		void DetailQuantityChanged(T item);
		void DetailSelectionChanged(T item);
		bool GetAllowChangeQuantity ();
	}
}

