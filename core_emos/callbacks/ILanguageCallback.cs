﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public interface ILanguageCallback 
	{
		void Close();
		void CloseAndReloadNavigation(ProfileLanguageModelSimple profileLanguage);
	}
}

