using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IPatientOrderCommandCallback : ICallback<PatientOrderCommand>
	{
		void OrderButtonClicked(PatientOrderCommand item);
	}
}