﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ICompanionOrderListCallback : ICallback<CompanionOrder>
	{
		void OnEdit(long id, bool chargeable = true);
		void OnDelete(long id);
		void OnChargeable(long id, bool chargeable);
		void Refresh();
		InterfaceStatusModel GetInterfaceStatus ();
	}
}

