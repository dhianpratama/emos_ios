﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IConsumptionChecklistCalenderCallback : ICallback<ConsumptionChecklistCalender>
	{
		void LoadConsumptionList(ConsumptionChecklistCalender model);
		void Refresh();
	}
	public interface IConsumptionChecklistCalenderRowCallback : ICallback<MealPeriodForCalender>
	{
	}
}

