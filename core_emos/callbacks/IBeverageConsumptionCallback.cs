﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IBeverageConsumptionCallback : ICallback<ExtraBeverage>
	{
		void RemoveBeverage (ExtraBeverage item);
		void OpenBeveragesTimePicker(ExtraBeverage item);
	}
}

