﻿using System;

namespace emos_ios
{
	public interface IDashboardCallback
	{
		void OnDashboardChange(Dashboard.DashboardMenu menu);
	}
}

