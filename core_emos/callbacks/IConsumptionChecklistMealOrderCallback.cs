﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IConsumptionChecklistMealOrderCallback : ICallback<DishModelSimple>
	{
		void OnDecreaseQuantity(double quantity, long dishId);
		void OnIncreaseQuantity(double quantity, long dishId);
		void OnMealsConsumptionTimeClicked(long dishId);
	}
}

