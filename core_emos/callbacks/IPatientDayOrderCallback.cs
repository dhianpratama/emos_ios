﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IPatientDayOrderCallback : ICallback<LocationWithRegistrationSimple>
	{
		void PatientNameClicked(LocationWithRegistrationSimple location);
		void IdClicked(LocationWithRegistrationSimple location);
		void OrderButtonClicked(PatientOrderCommand item);
		void Refresh();
		void OnDashboardChanged(Dashboard.OrderTabs tab, bool refresh = true);
		void PopToViewControllerAndUnlock(bool unlock = true, bool animated = true);
		void ShowWardSelectionAlert ();
		void OpenBirthdayCakeForm(LocationWithRegistrationSimple locationWithRegistration);
		void CloseBirthdayCakeForm();
		void SetOrderBy (string orderBy);
	}
}

