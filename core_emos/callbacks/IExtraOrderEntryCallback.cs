﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IExtraOrderEntryCallback : ICallback<ExtraOrderModelSimple>
	{
		void DetailSelected (SpecialInstructionDetail selected);
		void DetailDeselected (SpecialInstructionDetail deselected);
		void ValueChanged (ExtraOrderModelSimple model);
		void SelectionChanged (ExtraOrderModelSimple model, bool Reload = false);
		void SuitableChanged (ExtraOrderModelSimple model, bool IsSuitableOnly);
		bool GetAllowMultipleSelection ();
	}
}

