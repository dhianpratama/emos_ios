using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface ISpecialInstructionCallback 
	{
		void AddButtonClicked();
	}
	public interface ISpecialInstructionSelectionCallback
	{
		void ExitButtonClicked();
		void UpdateTableView(List<SpecialInstructionV2Detail> detail);
	}
	public interface ISpecialInstructionrEntryCallback : ICallback<SpecialInstructionV2Detail>
	{
		void DetailSelected (ExtraOrderMealPeriodModelSimple selected);
		void DetailDeselected (ExtraOrderMealPeriodModelSimple deselected);
		void DetailSelected (SpecialInstructionV2Detail selected);
		void DetailDeselected (SpecialInstructionV2Detail deselected);
		void ValueChanged (SpecialInstructionV2Detail model);
		void SelectionChanged (SpecialInstructionV2Detail model, bool Reload = false);
		void SelectionDeleted (SpecialInstructionV2Detail model);
	}

}