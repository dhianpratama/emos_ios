﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public partial class TherapeuticHeaderDVC : DialogViewController
	{
		private CommonDietOrderModel _fullFeed;
		private CommonDietOrderModel _clearFeed;
		public TherapeuticHeaderDVC (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location, string dietOrderText, bool showFeed, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed) : base (UITableViewStyle.Grouped, null)
		{
			_clearFeed = clearFeed;
			_fullFeed = fullFeed;
			Root = new RootElement ("TherapeuticHeaderDVC") {
				DvcGenerator.PatientInfoWithFeed (appContext, location.registrations.First (), showFeed, _clearFeed, _fullFeed)
			};
			CreateOthersSection (appContext, callback, location);
			Root.Add (new Section (dietOrderText));
		}
		private void CreateOthersSection (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location)
		{
			Root.Add (new Section (appContext.LanguageHandler.GetLocalizedString ("Others")) {
				new StyledStringElement ("Allergies", () => {
					callback.FoodAllergies (location, _clearFeed, _fullFeed, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
				new StyledStringElement ("Meal Type", () =>  {
					callback.MealType (location, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				}
			});
		}
	}
}
