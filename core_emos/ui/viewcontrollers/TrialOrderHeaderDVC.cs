﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public partial class TrialOrderHeaderDVC : DialogViewController
	{
		public TrialOrderHeaderDVC (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location, String dietOrderText, bool isTrialOrder = false) : base (UITableViewStyle.Grouped, null)
		{
			Root = new RootElement ("TrialOrderHeaderDVC") {
				DvcGenerator.PatientInfoShort (appContext, location.registrations.First ())
			};
			CreateOthersSection (appContext, callback, location, isTrialOrder);
			Root.Add (new Section (dietOrderText));
		}
		private void CreateOthersSection (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location, bool isTrialOrder)
		{
			if (isTrialOrder) {
				Root.Add (new Section (appContext.LanguageHandler.GetLocalizedString ("Others")) {
					new StyledStringElement ("Special Instruction", () => {
						callback.SpecialInstruction (location);
					}) {
						Image = UIImage.FromBundle ("Images/next_button_thumb.png")
					},
				});
			} else {
				Root.Add (new Section (appContext.LanguageHandler.GetLocalizedString ("Others")) {
					new StyledStringElement ("Trial Order", () => {
						callback.TrialOrder (location);
					}) {
						Image = UIImage.FromBundle ("Images/next_button_thumb.png")
					},
				});
			}


		}
	}
}
