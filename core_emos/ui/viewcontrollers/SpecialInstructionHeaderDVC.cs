﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public partial class SpecialInstructionHeaderDVC : DialogViewController
	{
		public String _addedInstruction;
		public StringElement _stringElement;
		public ISpecialInstructionCallback _specialInstructionCallback;
		public RootElement _patientClassOption;
		private ExtraOrderV2Model _model;

		public SpecialInstructionHeaderDVC (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location, String dietOrderText, ISpecialInstructionCallback specialCallback, ExtraOrderV2Model model) : base (UITableViewStyle.Grouped, null)
		{
			_specialInstructionCallback = specialCallback;
			_model = model;

			Root = new RootElement ("SpecialInstructionHeaderDVC") {
				DvcGenerator.PatientInfoShort (appContext, location.registrations.First ())
			};
			CreateOthersSection (appContext, callback, location);
			Root.Add (new Section (dietOrderText));
		}
		private void CreateOthersSection (IApplicationContext appContext, IPatientMenu callback, LocationWithRegistrationSimple location)
		{
			Root.Add (new Section (appContext.LanguageHandler.GetLocalizedString ("Special Instruction")) {
				new StyledStringElement ("Add", () => {
					_specialInstructionCallback.AddButtonClicked();
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
			});

		}
	}
}
