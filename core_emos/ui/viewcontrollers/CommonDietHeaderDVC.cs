﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace core_emos
{
	public partial class CommonDietHeaderDVC : DialogViewController
	{
		public CommonDietHeaderDVC (IApplicationContext appContext, LocationWithRegistrationSimple location, string dietOrderText, bool showCaseNumber = true) : base (UITableViewStyle.Grouped, null)
		{
			Root = new RootElement ("CommonDietHeaderDVC") {
				DvcGenerator.PatientInfoShort (appContext, location.registrations.First (), showCaseNumber)
			};
			Root.Add (new Section (dietOrderText));
		}
	}
}
