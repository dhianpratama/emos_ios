﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class MealOrderPeriodViewCell : BaseViewCell<NurseDashboardHeaderModel>
	{
		public const float CellHeight = 80f;
		public const string CellIdentifier = "MealOrderPeriodViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private BaseViewCellHandler<NurseDashboardHeaderModel> _handler;

		public MealOrderPeriodViewCell (IntPtr handle) : base (handle)
		{
		}
		public static MealOrderPeriodViewCell Create ()
		{
			return (MealOrderPeriodViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (NurseDashboardHeaderModel item)
		{
			MealPeriodLabel.Text = item.meal_order_period_label;
			this.SetFrame (height: _handler.BaseViewCellSetting.CellHeight);
		}
		protected override void SetViewCellHandler(IViewCellHandler<NurseDashboardHeaderModel> handler)
		{
			_handler = (BaseViewCellHandler<NurseDashboardHeaderModel>)handler;
		}

	}
}

