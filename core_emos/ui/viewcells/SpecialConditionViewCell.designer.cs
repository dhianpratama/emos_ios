// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("SpecialConditionViewCell")]
	partial class SpecialConditionViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EndDateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel EndMealPeriodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView SpecialConditionTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel StartDateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel StartMealPeriodLabel { get; set; }

		[Action ("DeleteButton_TouchUpInside:")]
		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (SpecialConditionTextView != null) {
				SpecialConditionTextView.Dispose ();
				SpecialConditionTextView = null;
			}

			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (EndDateLabel != null) {
				EndDateLabel.Dispose ();
				EndDateLabel = null;
			}

			if (EndMealPeriodLabel != null) {
				EndMealPeriodLabel.Dispose ();
				EndMealPeriodLabel = null;
			}

			if (StartDateLabel != null) {
				StartDateLabel.Dispose ();
				StartDateLabel = null;
			}

			if (StartMealPeriodLabel != null) {
				StartMealPeriodLabel.Dispose ();
				StartMealPeriodLabel = null;
			}
		}
	}
}
