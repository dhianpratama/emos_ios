// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace core_emos
{
	[Register ("MealOrderPeriodWithImageViewCell")]
	partial class MealOrderPeriodWithImageViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView ItemImage { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ItemImage != null) {
				ItemImage.Dispose ();
				ItemImage = null;
			}
		}
	}
}
