﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.BusinessLogic.EMOS.MealOrderPeriod;
using emos_ios;

namespace core_emos
{
	public partial class MealOrderPeriodWithImageViewCell : BaseViewCell<MealOrderPeriodModel>
	{
		public const float CellHeight = 230f;
		public const string CellIdentifier = "MealOrderPeriodWithImageViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private BaseViewCellHandler<MealOrderPeriodModel> _handler;

		public MealOrderPeriodWithImageViewCell (IntPtr handle) : base (handle)
		{
		}
		public static MealOrderPeriodWithImageViewCell Create ()
		{
			return (MealOrderPeriodWithImageViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (MealOrderPeriodModel item)
		{
			switch (item.meal_order_period_code) {
			case "B":
				ItemImage.Image = UIImage.FromFile("Images/breakfast_icon.png");
				break;
			case "L":
				ItemImage.Image = UIImage.FromFile("Images/lunch_icon.png");
				break;
			case "D":
				ItemImage.Image = UIImage.FromFile("Images/dinner_icon.png");
				break;
			}

			ItemImage.SetFrame (width: _handler.BaseViewCellSetting.CellWidth, height: _handler.BaseViewCellSetting.CellHeight);
			this.SetFrame (width: _handler.BaseViewCellSetting.CellWidth, height: _handler.BaseViewCellSetting.CellHeight);
		}
		protected override void SetViewCellHandler(IViewCellHandler<MealOrderPeriodModel> handler)
		{
			_handler = (BaseViewCellHandler<MealOrderPeriodModel>)handler;
		}
	}
}

