﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class NameQuantityViewCell : BaseViewCell<SpecialInstructionDetail>
	{
		public const string CellIdentifier = "NameQuantityViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private SpecialInstructionDetail _item;
		private NameQuantityViewCellHandler<SpecialInstructionDetail> _handler;
		private INameQuantityCallback<SpecialInstructionDetail> _callback;

		public NameQuantityViewCell (IntPtr handle) : base (handle)
		{
		}
		public static NameQuantityViewCell Create ()
		{
			return (NameQuantityViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (SpecialInstructionDetail item)
		{
			_item = item;
			NameLabel.Text = item.label;
			QuantityLabel.Text = _callback.GetBaseContext ().LanguageHandler.GetLocalizedString ("Serving");
			QuantityTextField.Text = item.quantity.ToString ();
			QuantityTextField.UserInteractionEnabled = false;
			Stepper.Value = item.quantity ?? 0;
			QuantityLabel.Hidden = !_callback.GetAllowChangeQuantity ();
			QuantityTextField.Hidden = !_callback.GetAllowChangeQuantity ();
			Stepper.Hidden = !_callback.GetAllowChangeQuantity ();
		}
		protected override void SetLayout ()
		{
			QuantityLabel.Hidden = _handler.Setting.QuantityLabelHidden;
			QuantityTextField.Hidden = _handler.Setting.QuantityTextHidden;
			Stepper.Hidden = _handler.Setting.QuantityTextHidden;
			var distance = Stepper.Frame.Left - QuantityTextField.Frame.Left;
			float xPoint = _handler.Setting.QuantityLabelX == 0 ? QuantityTextField.Frame.X : _handler.Setting.QuantityLabelX;
			Stepper.SetFrame (xPoint+ distance);
			Stepper.MinimumValue = _handler.Setting.MinimumQuantity;
			Stepper.MaximumValue = _handler.Setting.MaximumQuantity;
			NameLabel.SetFrame (width: xPoint - 40);

			if (_handler.Setting.QuantityTextHidden)
				return;
			QuantityTextField.SetFrame (xPoint);
			if (_item.id == null) {
				Stepper.Hidden = true;
			}
		}
		public override void HandleRowSelected(SpecialInstructionDetail item)
		{
			if (item.id != _item.id)
				return;
			_item.selected = true;
			_item.quantity = item.quantity;
			_callback.DetailQuantityChanged(_item);
		}
		public override void HandleRowDeselected(SpecialInstructionDetail item)
		{
			if (item.id != _item.id)
				return;
			_item.selected = false;
			_item.quantity = 0;
			QuantityTextField.Text = _item.quantity.ToString ();
			Stepper.Value = 0;
			_callback.DetailQuantityChanged(_item);
		}
		partial void QuantityTextField_EditingChanged (MonoTouch.Foundation.NSObject sender)
		{
			var value = ((UITextField) sender).Text;
			if (string.IsNullOrEmpty(value))
				return;
			long quantity = 0;
			long.TryParse(value, System.Globalization.NumberStyles.Any,System.Globalization.NumberFormatInfo.InvariantInfo, out quantity );
			if (quantity == 0 || (quantity == 1 && _item.quantity == 0))
				_callback.DetailSelectionChanged(_item);
			_item.quantity = quantity;
			_callback.DetailQuantityChanged(_item);
		}
		protected override void SetViewCellHandler(IViewCellHandler<SpecialInstructionDetail> handler)
		{
			_handler = (NameQuantityViewCellHandler<SpecialInstructionDetail>) handler;
			_callback = (INameQuantityCallback<SpecialInstructionDetail>) _handler.Callback;
		}
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			QuantityTextField.Text = Stepper.Value.ToString();
			if (Stepper.Value == 0) {
				_item.selected = false;
				_callback.DetailSelectionChanged(_item);
			} else if ((Stepper.Value == 1 && _item.quantity == 0)) {
				_item.selected = true;
				_callback.DetailSelectionChanged(_item);
			}
			_item.quantity = (long) Stepper.Value;
			_callback.DetailQuantityChanged(_item);
		}
	}
}

