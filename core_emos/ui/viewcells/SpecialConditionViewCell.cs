﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;

namespace emos_ios
{
	public partial class SpecialConditionViewCell : BaseViewCell<SpecialCondition>
	{
		public const string DefaultEndDateTitle = "Unlimited";
		public const float CellHeight = 60f;
		public const string CellIdentifier = "SpecialConditionViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private SpecialCondition _item;
		//private CrudViewCellHandler<SpecialCondition> _handler;
		private ICrud<SpecialCondition> _callback;

		public SpecialConditionViewCell (IntPtr handle) : base (handle)
		{
		}

		public static SpecialConditionViewCell Create ()
		{
			return (SpecialConditionViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (SpecialCondition item)
		{
			_item = item;
			SpecialConditionTextView.Text = item.DietOrder.Value;
			StartDateLabel.Text = item.StartTime.ToDisplayDateFormatInString ();
			EndDateLabel.Text = item.EndTime == null ? DefaultEndDateTitle : item.EndTime.Value.ToDisplayDateFormatInString ();
			StartMealPeriodLabel.Text = item.StartMealPeriod.Value;
			EndMealPeriodLabel.Text = item.EndMealPeriod.Value;
			DeleteButton.SetImage (UIImage.FromBundle ("Images/decline.png"), UIControlState.Normal);
		}
		protected override void SetViewCellHandler(IViewCellHandler<SpecialCondition> handler)
		{
			//_handler = (CrudViewCellHandler<SpecialCondition>)handler;
			_callback = (ICrud<SpecialCondition>) handler.Callback;
		}
		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.Delete(_item);
		}
	}
}

