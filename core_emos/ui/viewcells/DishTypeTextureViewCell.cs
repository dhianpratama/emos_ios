﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public partial class DishTypeTextureViewCell : BaseViewCell<DishTypeTexture>
	{
		public const float CellHeight = 44f;
		public const string CellIdentifier = "DishTypeTextureViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private IDishTypeTextureCallback _callback;
		private RoundedDropDown _dropDown;
		private DishTypeTexture _item;

		public DishTypeTextureViewCell (IntPtr handle) : base (handle)
		{
		}
		public static DishTypeTextureViewCell Create ()
		{
			return (DishTypeTextureViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (DishTypeTexture item)
		{
			_item = item;
			var frame = new RectangleF (0, 0, Frame.Width, Frame.Height);
			_dropDown = new RoundedDropDown (_callback.GetBaseContext (), this, frame, 200f, item.DishType.Value, _callback.GetTextures (), true);
			_dropDown.OnDone += HandleOnItemSelected;
			_dropDown.SelectedKey = item.TextureId.ToString ();
			AddSubview (_dropDown);
		}
		private void HandleOnItemSelected (object sender, BaseEventArgs<System.Collections.Generic.KeyValuePair<string, string>> e)
		{
			_item.TextureId = long.Parse (e.Value.Key);
			_callback.HandleOnTextureSelected (_item);
		}
		protected override void SetViewCellHandler(IViewCellHandler<DishTypeTexture> handler)
		{
			_callback = (IDishTypeTextureCallback) handler.Callback;
		}
	}
}

