﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class BulkOrderDetailViewCell : BaseViewCell<BulkMealOrderRestrictionGrouping>
	{
		public const float RowHeight = 60;
		public const string CellIdentifier = "BulkOrderDetailViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private BulkMealOrderRestrictionGrouping _item;
		private NameQuantityViewCellHandler<BulkMealOrderRestrictionGrouping> _handler;
		private INameQuantityCallback<BulkMealOrderRestrictionGrouping> _callback;

		public BulkOrderDetailViewCell (IntPtr handle) : base (handle)
		{
		}
		public static NameQuantityViewCell Create ()
		{
			return (NameQuantityViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (BulkMealOrderRestrictionGrouping item)
		{
			_item = item;
			NameLabel.Text = item.label;
			QuantityTextField.Text = item.quantity.ToString ();
			QuantityTextField.UserInteractionEnabled = false;
			Stepper.Value = item.quantity;
			if (item.mark_as_halal)
				NameLabel.TextColor = UIColor.FromRGB (7, 130, 13);
		}
		protected override void SetViewCellHandler(IViewCellHandler<BulkMealOrderRestrictionGrouping> handler)
		{
			_handler = (NameQuantityViewCellHandler<BulkMealOrderRestrictionGrouping>) handler;
			_callback = (INameQuantityCallback<BulkMealOrderRestrictionGrouping>) _handler.Callback;
		}
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			QuantityTextField.Text = Stepper.Value.ToString();
			_item.quantity = (int) Stepper.Value;
			_callback.DetailQuantityChanged(_item);
		}
	}
}