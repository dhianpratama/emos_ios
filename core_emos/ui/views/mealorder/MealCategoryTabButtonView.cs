﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Drawing;

namespace emos_ios
{
	[Register("MealCategoryTabButtonView")]
	public partial class MealCategoryTabButtonView : UIView
	{
		private IApplicationContext _appContext;
		private bool _pressed;
		public bool Pressed {
			get { return _pressed; }
			set { _pressed = value; Select (value); }
		}
		public int CategoryIndex;
		public EventHandler OnCategoryClicked;

		public MealCategoryTabButtonView (IntPtr h) : base(h)
		{
		}
		public MealCategoryTabButtonView (IApplicationContext appContext, string buttonTitle, int index, UITextAlignment textAlignment = UITextAlignment.Left)
		{
			var arr = NSBundle.MainBundle.LoadNib("MealCategoryTabButtonView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
			_appContext = appContext;
			TabButton.LineBreakMode = UILineBreakMode.WordWrap;
			TabButton.TitleLabel.TextAlignment = textAlignment;
			TabButton.SetTitle (buttonTitle, UIControlState.Normal);
			CategoryIndex = index;
		}
		private	void Select (bool selected)
		{
			if (selected) {
				TabButton.BackgroundColor = _appContext.ColorHandler.MealOrderBackground;
			} else {
				TabButton.BackgroundColor = _appContext.ColorHandler.ViewCellBackground;
			}
		}
		public void Deactive()
		{
			Hidden = true;
		}
		public void SetLayout (float x, float tabWidth, float tabHeight)
		{
			TabButton.Frame = new RectangleF (0, 0, tabWidth, tabHeight);
			TabButton.UpdateMask (RoundedRectangle.RoundedTopCorners);
		}
		partial void CategoryClicked (MonoTouch.Foundation.NSObject sender)
		{
			EventHandler handler = OnCategoryClicked;
			if (handler != null)
				handler(this, new EventArgs());
		}
		public void SetTitle (string title)
		{
			TabButton.SetTitle (title, UIControlState.Normal);
		}
	}
}

