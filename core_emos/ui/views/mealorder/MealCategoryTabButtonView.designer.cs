// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealCategoryTabButtonView
	{
		[Outlet]
		MonoTouch.UIKit.UIView ButtonBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TabButton { get; set; }

		[Action ("CategoryClicked:")]
		partial void CategoryClicked (MonoTouch.Foundation.NSObject sender);

		[Action ("CategorySelected:")]
		partial void CategorySelected (MonoTouch.Foundation.NSObject sender);

		[Action ("PickCategory:")]
		partial void PickCategory (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ButtonBackgroundView != null) {
				ButtonBackgroundView.Dispose ();
				ButtonBackgroundView = null;
			}

			if (TabButton != null) {
				TabButton.Dispose ();
				TabButton = null;
			}
		}
	}
}
