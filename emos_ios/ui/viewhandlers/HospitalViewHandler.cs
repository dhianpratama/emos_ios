﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using emos_ios.tools;
using System.Linq;

namespace emos_ios
{
	public class HospitalViewHandler
	{
		public List<LocationModelSimple> Models { get; set; }
		public event EventHandler OnRequestSuccessful;
		public event EventHandler OnRequestFailed;

		public HospitalViewHandler ()
		{
		}
		public void Request()
		{
			if (Models != null && Models.Any ()) {
				OnRequestSuccessful.SafeInvoke (this);
				return;
			}				
			RequestModels ();
		}
		private async void RequestModels()
		{
			var param = KnownUrls.InstitutionIdQueryString (AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<List<LocationModelSimple>> ()
				.From (KnownUrls.GetWards)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestModelsSuccessful(result))
				.WhenFail (result=> OnRequestFailed.SafeInvoke (this))
				.Go ();
		}
		private void OnRequestModelsSuccessful(List<LocationModelSimple> models)
		{
			Models = models;
			OnRequestSuccessful.SafeInvoke (this);
		}
	}
}

