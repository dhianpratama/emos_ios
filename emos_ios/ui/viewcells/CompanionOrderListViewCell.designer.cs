// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CompanionOrderListViewCell")]
	partial class CompanionOrderListViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton ChargeableButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CompanionOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditButton { get; set; }

		[Action ("ChargeableButtonTapped:")]
		partial void ChargeableButtonTapped (MonoTouch.Foundation.NSObject sender);

		[Action ("DeleteButton_TouchUpInside:")]
		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("EditButton_TouchUpInside:")]
		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CompanionOrderLabel != null) {
				CompanionOrderLabel.Dispose ();
				CompanionOrderLabel = null;
			}

			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (EditButton != null) {
				EditButton.Dispose ();
				EditButton = null;
			}

			if (ChargeableButton != null) {
				ChargeableButton.Dispose ();
				ChargeableButton = null;
			}
		}
	}
}
