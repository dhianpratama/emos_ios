﻿
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog.Utilities;

using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class CompanionOrderListViewCell : BaseViewCell<CompanionOrder>, IImageUpdated
	{
		public const string CellIdentifier = "CompanionOrderListViewCell";
		public static readonly UINib Nib = UINib.FromName ("CompanionOrderListViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("CompanionOrderListViewCell");
		private List<ImageDownloader> _imageDownloaders;
		private CompanionOrderViewCellHandler _handler;
		private ICompanionOrderListCallback _callback;
		private static float RowHeight = 60;
		private static float FontSize = 14f;
		private List<UILabel> _labelList;
		private CompanionOrder _item;
		private List<UIImageView> _imageViews;

		public CompanionOrderListViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetViewCellHandler(IViewCellHandler<CompanionOrder> handler)
		{
			_handler = (CompanionOrderViewCellHandler)handler;
			_callback = (ICompanionOrderListCallback) _handler.Callback;
		}
		protected override void SetCellContent (CompanionOrder item)
		{
			_item = item;
			var companionOrderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.CompanionOrder);
			var addSpaceCompanionText = companionOrderText + " ";
			CompanionOrderLabel.AttributedText = new NSAttributedString (
				addSpaceCompanionText + item.sequence.ToString(), 
				underlineStyle: NSUnderlineStyle.Single);

			CompanionOrderLabel.Frame = new RectangleF (150, CompanionOrderLabel.Frame.Y, CompanionOrderLabel.Frame.Width, CompanionOrderLabel.Frame.Height);

			SetIconImage ();

			if (AppDelegate.Instance.SelectedOperationCode == (long?)OperationCodeStatus.OperationCodes.Ordered) {
				EditButton.Enabled = false;
				DeleteButton.Enabled = false;
				EditButton.Hidden = true;
				DeleteButton.Hidden = true;
				ChargeableButton.Hidden = true;
				ChargeableButton.Enabled = false;
			} else {
				EditButton.Enabled = true;
				DeleteButton.Enabled = true;
				EditButton.Hidden = false;
				DeleteButton.Hidden = false;
				if (Settings.CompanionMealChargeableOption) {
					ChargeableButton.Hidden = false;
					ChargeableButton.Enabled = true;
				} else {
					ChargeableButton.Hidden = true;
					ChargeableButton.Enabled = false;
				}
			}

			GenerateOrderList ();
			DownloadNextImage (-1);
		}
		private void GenerateOrderList()
		{
			_imageDownloaders = new List<ImageDownloader> ();
			_imageViews = new List<UIImageView> ();
			_labelList = new List<UILabel> ();
			for (int i = 0; i < _item.dishes.Count; i++) {
				var image = new UIImageView (new RectangleF (150, (i * RowHeight) + RowHeight, 50, 50));
				var label = new UILabel();
				label.Font = UIFont.FromName(Fonts.ValueFontName, FontSize);
				label.Text = _item.dishes [i].dishLabel;
				label.Frame = new RectangleF (220, (i * RowHeight) + RowHeight, 450, 50);
				label.LineBreakMode = UILineBreakMode.WordWrap;
				_labelList.Add (label);
				if (String.IsNullOrEmpty (_item.dishes [i].pictureUrl))
					image.Image = ImageDownloader.GetNotAvailableImage ();
				_imageViews.Add (image);
			}
			AddSubviews (_imageViews.ToArray ());
			AddSubviews (_labelList.ToArray ());
		}
		private void DownloadNextImage (int index)
		{
			var nextIndex = index + 1;
			if (nextIndex >= _item.dishes.Count)
				return;
			DownloadImage (nextIndex, _item.dishes [nextIndex].pictureUrl, 50, 50);
		}
		public static float GetRowHeight(CompanionOrder item)
		{
			return (item.dishes.Count * RowHeight) + 90;
		}
		private void SetIconImage()
		{
			EditButton.SetImage (UIImage.FromBundle ("Images/edit_icon.png"), UIControlState.Normal);
			DeleteButton.SetImage (UIImage.FromBundle ("Images/delete_icon.png"), UIControlState.Normal);

			ChargeableButton.SetTitle ("$", UIControlState.Normal);
		}
		partial void DeleteButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OnDelete((long)_item.companionMealOrderId);
		}
		partial void EditButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			AppDelegate.Instance.SelectedOperationCode = 2;
			_callback.OnEdit((long)_item.companionMealOrderId, _item.chargable);
		}
		private void DownloadImage (int index, string imagePath, int width = 154, int height = 154)
		{
			var imageDownloader = new ImageDownloader ();
			imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloaders.Add (imageDownloader);
			if (String.IsNullOrEmpty (imagePath))
				DownloadNextImage (index++);
			else
				imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				var index = GetImageIndex(e.Text);
				if (index > -1)
					_imageViews [index].Image = ImageDownloader.LazyLoad (_imageDownloaders[index].ImageUrl, this);
				DownloadNextImage (index);
			});
		}
		private int GetImageIndex(string imagePath)
		{
			if (String.IsNullOrEmpty (imagePath))
				return -1;
			for (int i = 0; i < _item.dishes.Count; i++) {
				if (!String.IsNullOrEmpty(_item.dishes [i].pictureUrl) && _item.dishes [i].pictureUrl.Equals (imagePath))
					return i;
			}
			return -1;
		}
		private int GetImageIndex(Uri uri)
		{
			if (String.IsNullOrEmpty (uri.ToString ()))
				return -1;
			for (int i = 0; i < _item.dishes.Count; i++) {
				if (String.Equals(uri.ToString (), _imageDownloaders [i].ImageUrl, StringComparison.OrdinalIgnoreCase))
					return i;
			}
			return -1;
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				var index = GetImageIndex(uri);
				if (index > -1)
					_imageViews [index].Image = ImageDownloader.LazyLoad (uri.ToString (), this);
			});
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				var index = GetImageIndex (e.Text);
				if (index > -1)
					_imageViews [index].Image = ImageDownloader.GetNotAvailableImage ();
				DownloadNextImage (index);
			});
		}
		partial void ChargeableButtonTapped (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OnChargeable((long)_item.companionMealOrderId, _item.chargable);
		}
	}
}

