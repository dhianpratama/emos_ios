﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public partial class MealOrdersViewCell : BaseViewCell<List<DishModelWithTags>>
	{
		public const string CellIdentifier = "MealOrdersViewCell";
		private List<DishModelWithTags> _item;
		private readonly UINib _childNib;
		private MealOrdersViewCellHandler _handler;

		public MealOrdersViewCell (IntPtr handle) : base (handle)
		{
			_childNib = UINib.FromName (MealOrderViewCell.CellIdentifier, NSBundle.MainBundle);
		}
		protected override void SetCellContent (List<DishModelWithTags> item)
		{
			_item = item;
			AddItem (0);
			AddItem (1);
		}
		protected override void SetViewCellHandler(IViewCellHandler<List<DishModelWithTags>> handler)
		{
			_handler = (MealOrdersViewCellHandler) handler;
		}
		private void AddItem (int index)
		{
			if (index >= _item.Count ())
				return;
			var mealOrderViewCell = (MealOrderViewCell)_childNib.Instantiate (null, null) [0];		
			mealOrderViewCell.Initialize (_handler, new List<DishModelWithTags> { _item [index] });
			mealOrderViewCell.Frame = new RectangleF (index * _handler.BaseViewCellSetting.CellWidth, 0, _handler.BaseViewCellSetting.CellWidth, _handler.BaseViewCellSetting.CellHeight);
			mealOrderViewCell.BackgroundColor = _handler.BaseViewCellSetting.BackgroundColor;
			AddSubview (mealOrderViewCell);
		}
	}
}

