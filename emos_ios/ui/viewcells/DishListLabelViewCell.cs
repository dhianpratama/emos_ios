﻿
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class DishListLabelViewCell : UITableViewCell
	{
		public static float RowHeight = 20;
		public static readonly UINib Nib = UINib.FromName ("DishListLabelViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("DishListLabelViewCell");

		private List<DishModelSimple> _dishes;

		public DishListLabelViewCell (IntPtr handle) : base (handle)
		{
		}

		public static DishListLabelViewCell Create ()
		{
			return (DishListLabelViewCell)Nib.Instantiate (null, null) [0];
		}

		public void GenerateDishList(List<DishModelSimple> dishes, float width)
		{
			_dishes = dishes;
			for(int i=0; i < _dishes.Count; i++) {
				var dishLabel = new UILabel();
				dishLabel.Font = UIFont.FromName(Fonts.ValueFontName, 14f);
				dishLabel.Text = String.Format ("-  {0}", _dishes [i].label);
				dishLabel.Frame = new RectangleF (0, (i * RowHeight) + 21, width, RowHeight);
				AddSubview(dishLabel);
			};
			MealOrderLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealOrder");
		}
	}
}

