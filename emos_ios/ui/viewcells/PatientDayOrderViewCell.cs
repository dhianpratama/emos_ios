
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class PatientDayOrderViewCell : BaseViewCell<LocationWithRegistrationSimple>, IPatientOrderCommandCallback
	{
		public const string CellIdentifier = "PatientDayOrderViewCell";
		private PatientDayOrderViewCellHandler _handler;
		private IPatientDayOrderCallback _callback;
		private LocationWithRegistrationSimple _item;
		private UICollectionViewController _collectionViewController;
		private CollectionSource<PatientOrderCommand, OrderCollectionViewCell> _patientOrderCommandSource;
		private PatientDayOrderBorderHandler _borderHandler;

		public PatientDayOrderViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (LocationWithRegistrationSimple item)
		{
			_item = item;
			_borderHandler = new PatientDayOrderBorderHandler {
				RectangleBorder = new RectangleBorder (LeftBorder, RightBorder, TopBorder, BottomBorder)
			};
			_borderHandler.HideAllBorder ();

			OrderContainerView.Frame = _handler.Setting.OrderContainerFrame;
			ViewHandler.ClearSubviews (OrderContainerView);
			SetIconsVisibility (item);
			if (item.registrations.Count > 0) {
				var registration = item.registrations.First ();
				NameButton.SetTitle (registration.profile_name, UIControlState.Normal);
				if (Settings.UseRegistrationNumberInsteadOfId)
					IdButton.SetTitle(registration.registration_number, UIControlState.Normal);
				else
					IdButton.SetTitle(registration.profile_identifier, UIControlState.Normal);
				GenerateOrderButtons (item);
				if (!Settings.HideStayForMeal && registration.planned_discharged_patient)
					BackgroundColor = Colors.PlannedDischargeBackground;
				else if (registration.manual_registration)
					BackgroundColor = Colors.ManualAdmissionBackground;
				else
					BackgroundColor = Colors.RegularAdmissionBackground;

				BirthdayButton.SetImage (UIImage.FromBundle ("Images/birthday_cake.png"), UIControlState.Normal);
				SnackPackImage.Image = UIImage.FromBundle ("Images/sandwich2.png");
				BirthdayCakeChecklistIcon.Image = UIImage.FromBundle ("Images/checklist_icon.png");
				ExtraOrderChecklistImage.Image = UIImage.FromBundle ("Images/checklist_icon.png");
				TrialOrderChecklistImage.Image = UIImage.FromBundle ("Images/checklist_icon.png");

				if (registration.hasTrialOrder) {
					IdButton.SetTitleColor (UIColor.Purple, UIControlState.Normal);
					NameButton.SetTitleColor (UIColor.Purple, UIControlState.Normal);
				}
				if (!TrialOrderButton.Hidden)
					TrialOrderButton.SetTitle (IconTexts.TrialOrderIconText, UIControlState.Normal);
				if (!ExtraOrderButton.Hidden)
					ExtraOrderButton.SetTitle (IconTexts.ExtraOrderIconText, UIControlState.Normal);
			} else {
				NameButton.SetTitle ("", UIControlState.Normal);
				IdButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ManualAdmission"), UIControlState.Normal);
				IdButton.BackgroundColor = Colors.ManualAdmissionButtonColor;
				IdButton.SetTitleColor (Colors.ManualAdmissionButtonFontColor, UIControlState.Normal);
				IdButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
				NameButton.Hidden = (_item.isSapUp && Settings.HideManualAdmissionOnSapUp);
				IdButton.Hidden = (_item.isSapUp && Settings.HideManualAdmissionOnSapUp);
			}

			if (Settings.DoubleTapNurseDashboardTableView) {
				var idTapGestureRecognizer = new UITapGestureRecognizer();
				idTapGestureRecognizer.AddTarget(() => idButtonTapped(idTapGestureRecognizer));
				idTapGestureRecognizer.NumberOfTapsRequired = 2;
				IdButton.AddGestureRecognizer(idTapGestureRecognizer);

				var nameTapGestureRecognizer = new UITapGestureRecognizer();
				nameTapGestureRecognizer.AddTarget(() => nameButtonTapped(nameTapGestureRecognizer));
				nameTapGestureRecognizer.NumberOfTapsRequired = 2;
				NameButton.AddGestureRecognizer(nameTapGestureRecognizer);
			}

			BedLabel.Text = item.code;
			_borderHandler.GenerateBorder (AppDelegate.Instance.CurrentTab == Dashboard.OrderTabs.PatientMealOrder, _item.bedStatus);
			_borderHandler.SetBordersLayout (GetRowHeight (_item));
			var height = GetRowHeight (_item);
			OrderContainerView.SetFrame (Measurements.NurseDashboardOperationContainerLeft, width: UIScreen.MainScreen.Bounds.Right - Measurements.NurseDashboardOperationContainerLeft, height: height);
			NameButton.SetFrame (width: Measurements.NurseDashboardOperationContainerLeft - IdButton.Frame.Left - 10);
			//IdButton.Font = Fonts.NurseDashboardPatientInfo;
			//NameButton.Font = Fonts.NurseDashboardPatientInfo;
			ShowBedName ();
			if (Settings.HideNurseDashboardIcons)
				HideNurseDashboardIcons ();
			this.SetFrame (height: height);
		}
		private void HideNurseDashboardIcons ()
		{
			BirthdayButton.Hidden = true;
			BirthdayCakeChecklistIcon.Hidden = true;
			ExtraOrderButton.Hidden = true;
			ExtraOrderChecklistImage.Hidden = true;
			SnackPackImage.Hidden = true;
			TrialOrderButton.Hidden = true;
			TrialOrderChecklistImage.Hidden = true;
		}
		public void ShowBedName ()
		{
			if (!Settings.ShowBedNameOnDashboard)
				return;
			BedLabel.Font = Fonts.NurseDashboardBedInfo;
			var bedNameLabel = new UILabel () {
				Text = _item.label,
				Font = Fonts.NurseDashboardBedInfo
			};
			bedNameLabel.SetFrameBelowTo (BedLabel, BedLabel.Frame.Left, BedLabel.Frame.Width, BedLabel.Frame.Height, 3);
			AddSubview (bedNameLabel);
			BirthdayButton.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			BirthdayCakeChecklistIcon.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			ExtraOrderButton.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			ExtraOrderChecklistImage.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			SnackPackImage.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			TrialOrderChecklistImage.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
			TrialOrderButton.SetFrameBelowTo (bedNameLabel, distanceToAbove: 3);
		}
		private void InitializeViewCollection ()
		{
			var uiCollectionFlowLayout = new UICollectionViewFlowLayout {
				MinimumInteritemSpacing = 0,
				ScrollDirection = UICollectionViewScrollDirection.Horizontal
			};
			var rowHeight = GetRowHeight (_item);
			uiCollectionFlowLayout.ItemSize = new SizeF (_handler.BaseViewCellSetting.CellWidth, rowHeight);
			_collectionViewController = new UICollectionViewController (uiCollectionFlowLayout);
			_collectionViewController.CollectionView.RegisterNibForCell(OrderCollectionViewCell.Nib, new NSString (OrderCollectionViewCell.CellIdentifier));
			_collectionViewController.CollectionView.SetFrame (height: rowHeight);
			_collectionViewController.CollectionView.BackgroundColor = UIColor.Clear;
			_collectionViewController.View.BackgroundColor = UIColor.Clear;
			_collectionViewController.CollectionView.Opaque = false;
			OrderContainerView.AddSubview (_collectionViewController.CollectionView);
		}
		private void SetIconsVisibility (LocationWithRegistrationSimple item)
		{
			var isRegistered = item.registrations.Count == 0;
			var registration = item.registrations.FirstOrDefault ();
			SnackPackImage.Hidden = isRegistered || !registration.hasSnackPack;
			ExtraOrderButton.Hidden = isRegistered || !registration.hasExtraOrder;
			TrialOrderButton.Hidden = isRegistered || !registration.hasTrialOrder;
			BirthdayButton.Hidden = isRegistered || !registration.is_tomorrow_birthday;
			BirthdayCakeChecklistIcon.Hidden = isRegistered || !registration.birthday_cake_ordered;
			TrialOrderChecklistImage.Hidden = isRegistered || !registration.hasOrderedTrialOrder;
			ExtraOrderChecklistImage.Hidden = isRegistered || !registration.hasOrderedExtraOrder;
		}
		private void GenerateOrderButtons(LocationWithRegistrationSimple location)
		{
			if (location.registrations [0].mealOrderPeriods == null) {
				GeneratePatientMealOrder (location);
				return;
			}

			InitializeViewCollection ();
			var handler = new PatientOrderCommandViewCellHandler (OrderCollectionViewCell.CellIdentifier, this, _handler.Setting);
			_patientOrderCommandSource = new CollectionSource<PatientOrderCommand, OrderCollectionViewCell> (handler);
			var patientOrderCommands = PatientOrderCommandViewCellHandler.CreateOneDayPatientOrderCommands (AppDelegate.Instance, location, location.registrations.First (), AppDelegate.Instance.OperationDate);
			_patientOrderCommandSource.Items = patientOrderCommands;
			_collectionViewController.CollectionView.Source = _patientOrderCommandSource;
		}			
		private void GeneratePatientMealOrder (LocationWithRegistrationSimple location)
		{
			if (location.registrations [0].currentMealPeriod == null)
				return;
			var orderedDishes = location.registrations [0].currentMealPeriod.orderedDishes;
			if (orderedDishes != null && orderedDishes.Count () > 0)
				GeneratePatientMealOrderList (location);
			else
				GeneratePatientMealOrderButton (location);
		}
		private void GeneratePatientMealOrderList (LocationWithRegistrationSimple location)
		{
			var dishListViewCell = DishListLabelViewCell.Create ();
			dishListViewCell.BackgroundColor = UIColor.Clear;
			var height = GetRowHeight (location);
			var width = 768 - OrderContainerView.Frame.Left;
			dishListViewCell.GenerateDishList (location.registrations [0].currentMealPeriod.orderedDishes, width);
			dishListViewCell.Frame = new RectangleF (0, 0, width, height);
			OrderContainerView.AddSubview (dishListViewCell);
		}
		private void GeneratePatientMealOrderButton (LocationWithRegistrationSimple location)
		{
			var mealPeriod = new MealOrderPeriodWithOperationCode () {
				id = location.registrations [0].currentMealPeriod.id,
				label = location.registrations [0].currentMealPeriod.label,
				code = location.registrations [0].currentMealPeriod.code,
				operation_code = location.registrations[0].currentMealPeriod.operationCode ?? 1
			};
			OrderContainerView.AddSubview (CreateOrderButton (0, location, mealPeriod));
		}
		public static float GetRowHeight(LocationWithRegistrationSimple location)
		{
			if (location.registrations.Count > 0 && location.registrations [0].currentMealPeriod != null) {
				return (location.registrations [0].currentMealPeriod.orderedDishes.Count () * DishListLabelViewCell.RowHeight) + Measurements.NurseDashboardRowHeight;
			}
			return Measurements.NurseDashboardRowHeight;
		}
		private PatientOrderCommandCollectionViewCell CreateOrderButton (int x, LocationWithRegistrationSimple location, MealOrderPeriodWithOperationCode mealOrder)
		{
			var cell = PatientOrderCommandCollectionViewCell.Create ();
			cell.OperationCode = mealOrder.operation_code;
			cell.Registration = (RegistrationModelSimple)location.registrations.First();
			cell.Location = (LocationModelSimple)location;
			cell.MealOrderPeriod = (MealOrderPeriodModelSimple)mealOrder;
			var operationCodeStatus = AppDelegate.Instance.OperationCodeHandler.GetStatus (mealOrder.operation_code);
			var orderBy = "";
			if (mealOrder.operation_code == 2 || mealOrder.operation_code == 3 || mealOrder.operation_code == 4 || mealOrder.operation_code == 5)
				orderBy = mealOrder.order_by;
			var item = new PatientOrderCommand () {
				Index = 0,
				Value = operationCodeStatus.Status,
				ImagePath = operationCodeStatus.ImagePath,
				OrderBy = orderBy,
				OperationCode = mealOrder.operation_code,
				Registration = (RegistrationModelSimple)location.registrations.First(),
				Location = (LocationModelSimple)location,
				MealOrderPeriod = (MealOrderPeriodModelSimple)mealOrder,
			};
			cell.SetContent (item);
			cell.SetViewCellHandler (_handler);
			cell.Frame = new RectangleF ((x * _handler.Setting.CellWidth), 0, _handler.Setting.CellWidth, _handler.Setting.CellHeight);
			cell.BackgroundColor = UIColor.Clear;
			return cell;
		}
		protected override void SetViewCellHandler(IViewCellHandler<LocationWithRegistrationSimple> handler)
		{
			_handler = (PatientDayOrderViewCellHandler) handler;
			_callback = (IPatientDayOrderCallback) _handler.Callback;
		}
		private void idButtonTapped(UITapGestureRecognizer recognizer){
			_callback.IdClicked(_item);
		}

		private void nameButtonTapped(UITapGestureRecognizer recognizer){
			_callback.IdClicked(_item);
		}
		partial void NameButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(!Settings.DoubleTapNurseDashboardTableView)
				_callback.PatientNameClicked(_item);
		}
		partial void IdButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(!Settings.DoubleTapNurseDashboardTableView)
				_callback.IdClicked(_item);
		}

		partial void BirthdayButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OpenBirthdayCakeForm(_item);
		}
		partial void ExtraOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{

		}

		#region IPatientOrderCommandCallback implementation

		public void OrderButtonClicked (PatientOrderCommand item)
		{
			_callback.OrderButtonClicked (item);
		}

		#endregion

		#region ICallback implementation

		public IBaseContext GetBaseContext ()
		{
			return _callback.GetBaseContext ();
		}

		public void ItemSelected (PatientOrderCommand selected, int selectedIndex)
		{
			_callback.OrderButtonClicked (selected);
		}

		#endregion
	}
}

