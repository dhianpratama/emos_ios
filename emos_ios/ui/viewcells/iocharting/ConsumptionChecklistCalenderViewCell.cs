﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class ConsumptionChecklistCalenderViewCell : BaseViewCell<ConsumptionChecklistCalender>
	{
		public const string CellIdentifier = "ConsumptionChecklistCalenderViewCell";
		public static readonly UINib Nib = UINib.FromName ("ConsumptionChecklistCalenderViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ConsumptionChecklistCalenderViewCell");
		private ConsumptionChecklistCalender _item;
		private IConsumptionChecklistCalenderCallback _callback;
		public ConsumptionChecklistCalenderViewCellHandler _handler;

		public ConsumptionChecklistCalenderViewCell (IntPtr handle) : base (handle)
		{
		}

		public static ConsumptionChecklistCalenderViewCell Create ()
		{
			return (ConsumptionChecklistCalenderViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (ConsumptionChecklistCalender item)
		{
			_item = item;
			if(_item.has_checklist){
				if (_item.data_sent_to_epic)
					ChecklistButton.Image = UIImage.FromBundle ("Images/checklist_red.png");
				else
					ChecklistButton.Image = UIImage.FromBundle ("Images/checklist_black.png");
			}

			StatusButton.SetImage(UIImage.FromFile(OperationCodeHandler.GetConsumptionStatus (item.operation_code).ImagePath), UIControlState.Normal);
			BackgroundColor = Colors.ViewCellBackground;
		}
		protected override void SetViewCellHandler(IViewCellHandler<ConsumptionChecklistCalender> handler)
		{
			_handler = (ConsumptionChecklistCalenderViewCellHandler) handler;
			_callback = (IConsumptionChecklistCalenderCallback) _handler.Callback;
		}
		partial void StatusButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(_item.operation_code==2 || _item.operation_code == 3 || _item.operation_code==100)
				_callback.LoadConsumptionList(_item);
		}
	}
}

