﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class BeverageConsumptionDisplayViewCell : BaseViewCell<ExtraBeverage>
	{
		public const string CellIdentifier = "BeverageConsumptionDisplayViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);

		public BeverageConsumptionDisplayViewCell (IntPtr handle) : base (handle)
		{
		}

		public static BeverageConsumptionDisplayViewCell Create ()
		{
			return (BeverageConsumptionDisplayViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent(ExtraBeverage item)
		{
			BeverageNameLabel.Text = item.item;
			BeverageWeightLabel.Text = item.value.ToString ();
			BeverageTimeLabel.Text = CommonTools.ConvertToAmPm(item.consumptionTime);
			BeveragemLLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("mL");
		}
		protected override void SetViewCellHandler(IViewCellHandler<ExtraBeverage> handler)
		{
		}
	}
}

