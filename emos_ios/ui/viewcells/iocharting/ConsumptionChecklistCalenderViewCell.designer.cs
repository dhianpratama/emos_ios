// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ConsumptionChecklistCalenderViewCell")]
	partial class ConsumptionChecklistCalenderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView ChecklistButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView ConsumptionStaticTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton StatusButton { get; set; }

		[Action ("StatusButton_TouchUpInside:")]
		partial void StatusButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ChecklistButton != null) {
				ChecklistButton.Dispose ();
				ChecklistButton = null;
			}

			if (ConsumptionStaticTextView != null) {
				ConsumptionStaticTextView.Dispose ();
				ConsumptionStaticTextView = null;
			}

			if (StatusButton != null) {
				StatusButton.Dispose ();
				StatusButton = null;
			}
		}
	}
}
