// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ConsumptionChecklistMealOrderViewCell")]
	partial class ConsumptionChecklistMealOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton DecreaseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView DescriptionTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DishImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishTypeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton IncreaseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField QuantityText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TimeButton { get; set; }

		[Action ("DecreaseButton_TouchUpInside:")]
		partial void DecreaseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("IncreaseButton_TouchUpInside:")]
		partial void IncreaseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("TimeButton_TouchUpInside:")]
		partial void TimeButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DecreaseButton != null) {
				DecreaseButton.Dispose ();
				DecreaseButton = null;
			}

			if (DescriptionTextView != null) {
				DescriptionTextView.Dispose ();
				DescriptionTextView = null;
			}

			if (DishImage != null) {
				DishImage.Dispose ();
				DishImage = null;
			}

			if (DishNameLabel != null) {
				DishNameLabel.Dispose ();
				DishNameLabel = null;
			}

			if (DishTypeLabel != null) {
				DishTypeLabel.Dispose ();
				DishTypeLabel = null;
			}

			if (IncreaseButton != null) {
				IncreaseButton.Dispose ();
				IncreaseButton = null;
			}

			if (QuantityText != null) {
				QuantityText.Dispose ();
				QuantityText = null;
			}

			if (TimeButton != null) {
				TimeButton.Dispose ();
				TimeButton = null;
			}
		}
	}
}
