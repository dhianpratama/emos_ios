﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class ConsumptionChecklistMealOrderDisplayViewCell : BaseViewCell<DishModelSimple>, IImageUpdated
	{
		public const string CellIdentifier = "ConsumptionChecklistMealOrderDisplayViewCell";
		public static readonly UINib Nib = UINib.FromName ("ConsumptionChecklistMealOrderDisplayViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ConsumptionChecklistMealOrderDisplayViewCell");

		private ImageDownloader _imageDownloader = new ImageDownloader ();
		private DishModelSimple _item;
		public double Quantity;
		private ConsumptionChecklistMealOrderHandler _handler;

		public ConsumptionChecklistMealOrderDisplayViewCell (IntPtr handle) : base (handle)
		{
		}

		public static ConsumptionChecklistMealOrderDisplayViewCell Create ()
		{
			return (ConsumptionChecklistMealOrderDisplayViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (DishModelSimple value)
		{
			_item = value;
			DishNameLabel.Text = String.Format(" - {0}", value.label);
			DishTypeLabel.Text = value.dish_type_label;
			DishDescLabel.Text = value.description;
			DishDescLabel.UserInteractionEnabled = false;
			DownloadImage (_item.picture_url);
			Quantity = (double)value.consumptionQuantity;
			ConvertQuantityAndShow ();
			DishTypeLabel.SizeToFit ();
			DishNameLabel.SizeToFit ();
			DishNameLabel.Frame = new RectangleF (DishTypeLabel.Frame.Left + DishTypeLabel.Frame.Width, DishTypeLabel.Frame.Top, DishNameLabel.Frame.Width, DishTypeLabel.Frame.Height);
			ConsumptionTimeLabel.Text =  CommonTools.ConvertToAmPm(value.consumptionUpdateTime);
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}
		private void ConvertQuantityAndShow ()
		{
			var fractionNum = Quantity % 1;
			string fraction = "";
			if (fractionNum == 0)
				fraction = "0";
			else if (fractionNum == 0.25)
				fraction = "1/4";
			else if (fractionNum == 0.5)
				fraction = "1/2";
			else if (fractionNum == 0.75)
				fraction = "3/4";
			else if (fractionNum == 1)
				fraction = "Full";
			var dec = Math.Floor (Quantity).ToString ();
			ConsumptionDetailLabel.Text = dec == "1" ? "Full" : String.Format ("{0}", fraction);
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			_imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		protected override void SetViewCellHandler(IViewCellHandler<DishModelSimple> handler)
		{
			_handler = (ConsumptionChecklistMealOrderHandler) handler;
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImageView.Image = ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this);
			});
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
					DishImageView.Image = ImageDownloader.LazyLoad (uri.ToString (), this);
			});
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImageView.Image = ImageDownloader.GetNotAvailableImage ();
			});
		}
	}
}

