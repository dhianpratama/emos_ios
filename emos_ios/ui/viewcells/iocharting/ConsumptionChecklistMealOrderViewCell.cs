﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class ConsumptionChecklistMealOrderViewCell : BaseViewCell<DishModelSimple>, IImageUpdated
	{
		public const string CellIdentifier = "ConsumptionChecklistMealOrderViewCell";
		public static readonly UINib Nib = UINib.FromName ("ConsumptionChecklistMealOrderViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ConsumptionChecklistMealOrderViewCell");
		private ImageDownloader _imageDownloader = new ImageDownloader ();
		private ConsumptionChecklistMealOrderHandler _handler;
		private IConsumptionChecklistMealOrderCallback _callback;
		private DishModelSimple _item;
		public double Quantity;

		public ConsumptionChecklistMealOrderViewCell (IntPtr handle) : base (handle)
		{
		}
		public static ConsumptionChecklistMealOrderViewCell Create ()
		{
			return (ConsumptionChecklistMealOrderViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetViewCellHandler(IViewCellHandler<DishModelSimple> handler)
		{
			_handler = (ConsumptionChecklistMealOrderHandler) handler;
			_callback = (IConsumptionChecklistMealOrderCallback) _handler.Callback;
		}
		protected override void SetCellContent (DishModelSimple value)
		{
			_item = value;
			DishNameLabel.Text = String.Format(" - {0}", value.label);
			DishTypeLabel.Text = value.dish_type_label;
			DescriptionTextView.Text = value.description;
			DescriptionTextView.UserInteractionEnabled = false;
			DownloadImage (_item.picture_url);
			IncreaseButton.SetImage(UIImage.FromFile ("Images/btn_increase.png"), UIControlState.Normal);
			DecreaseButton.SetImage(UIImage.FromFile ("Images/btn_decrease.png"), UIControlState.Normal);
			Quantity = (double)value.consumptionQuantity;
			ConvertQuantityAndShow ();
			DishTypeLabel.SizeToFit ();
			DishNameLabel.SizeToFit ();
			DishNameLabel.Frame = new RectangleF (DishTypeLabel.Frame.Left + DishTypeLabel.Frame.Width, DishTypeLabel.Frame.Top, DishNameLabel.Frame.Width, DishTypeLabel.Frame.Height);
			TimeButton.SetTitle (CommonTools.ConvertToAmPm(value.consumptionUpdateTime), UIControlState.Normal);
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}
		partial void DecreaseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if (Quantity == 0) return;
			Quantity-=0.25;
			ConvertQuantityAndShow ();
			_callback.OnDecreaseQuantity(Quantity,(long)_item.id);
		}
		partial void IncreaseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if (Quantity==1) return;
			Quantity+=0.25;
			ConvertQuantityAndShow ();
			_callback.OnIncreaseQuantity(Quantity,(long)_item.id);
		}

		private void ConvertQuantityAndShow ()
		{
			var fractionNum = Quantity % 1;
			string fraction = "";
			if (fractionNum == 0)
				fraction = "0";
			else if (fractionNum == 0.25)
				fraction = "1/4";
			else if (fractionNum == 0.5)
				fraction = "1/2";
			else if (fractionNum == 0.75)
				fraction = "3/4";
			else if (fractionNum == 1)
				fraction = "Full";
			var dec = Math.Floor (Quantity).ToString ();
			QuantityText.Text = dec == "1" ? "Full" : String.Format ("{0}", fraction);
		}
		partial void TimeButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OnMealsConsumptionTimeClicked((long)_item.id);
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			_imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImage.Image = ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this);
			});
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
					DishImage.Image = ImageDownloader.LazyLoad (uri.ToString (), this);
			});
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImage.Image = ImageDownloader.GetNotAvailableImage ();
			});
		}
	}
}

