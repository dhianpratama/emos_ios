// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ConsumptionChecklistMealOrderDisplayViewCell")]
	partial class ConsumptionChecklistMealOrderDisplayViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ConsumptionDetailLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ConsumptionTimeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView DishDescLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DishImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishTypeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DishNameLabel != null) {
				DishNameLabel.Dispose ();
				DishNameLabel = null;
			}

			if (DishDescLabel != null) {
				DishDescLabel.Dispose ();
				DishDescLabel = null;
			}

			if (ConsumptionTimeLabel != null) {
				ConsumptionTimeLabel.Dispose ();
				ConsumptionTimeLabel = null;
			}

			if (ConsumptionDetailLabel != null) {
				ConsumptionDetailLabel.Dispose ();
				ConsumptionDetailLabel = null;
			}

			if (DishTypeLabel != null) {
				DishTypeLabel.Dispose ();
				DishTypeLabel = null;
			}

			if (DishImageView != null) {
				DishImageView.Dispose ();
				DishImageView = null;
			}
		}
	}
}
