﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public partial class ConsumptionChecklistCalenderRowViewCell : BaseViewCell<MealPeriodForCalender>
	{
		private const int VerticalSeparatorWidth = 1;
		public const string CellIdentifier = "ConsumptionChecklistCalenderRowViewCell";
//		private MealPeriodForCalender _item;
//		private readonly UINib _nib;
		private ConsumptionChecklistCalenderRowViewCellHandler _handler;
		private IConsumptionChecklistCalenderRowCallback _callback;

		public ConsumptionChecklistCalenderRowViewCell (IntPtr handle) : base (handle)
		{
//			_nib = UINib.FromName (ConsumptionChecklistCalenderRowViewCell.CellIdentifier, NSBundle.MainBundle);
		}
		protected override void SetCellContent (MealPeriodForCalender item)
		{
//			_item = item;
			//MealTypeLabel.Text = item.label;
			var consumptionChecklistCalendarViewCellHandler = new ConsumptionChecklistCalenderViewCellHandler (ConsumptionChecklistCalenderViewCell.CellIdentifier, (IConsumptionChecklistCalenderCallback) _callback, new BaseViewCellSetting ());
			MealTypeTextView.Text = item.label;
			MealTypeTextView.BackgroundColor = Colors.ViewCellBackground;
			var columnCount = _handler.Setting.ColumnCount;
			var cellHeight = _handler.BaseViewCellSetting.CellHeight;
			var cellWidth = _handler.BaseViewCellSetting.CellWidth;
			var childCellWidth = _handler.BaseViewCellSetting.CellWidth / columnCount;
			ConsumptionChecklistBackgroundView.Frame = new RectangleF (_handler.Setting.CalendarLeft, 0, _handler.BaseViewCellSetting.CellWidth, _handler.BaseViewCellSetting.CellHeight);
			for (int x = 0; x < item.operationCodes.Count (); x++) {
				if (x > columnCount)
					return;
				if (x > 0)
					ConsumptionChecklistBackgroundView.AddSubview(ViewHandler.DrawVerticalLine(new RectangleF (x * childCellWidth, 0, VerticalSeparatorWidth, cellHeight), UIColor.Black));
				var consumptionCell = ConsumptionChecklistCalenderViewCell.Create ();
				consumptionCell.Initialize (consumptionChecklistCalendarViewCellHandler, item.ConsumptionChecklistCalenders[x]);
				consumptionCell.Frame = new RectangleF ((x * childCellWidth) + VerticalSeparatorWidth, 0, childCellWidth, cellHeight);
				ConsumptionChecklistBackgroundView.AddSubview (consumptionCell);
			}
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}
		protected override void SetViewCellHandler(IViewCellHandler<MealPeriodForCalender> handler)
		{
			_handler = (ConsumptionChecklistCalenderRowViewCellHandler) handler;
			_callback = (IConsumptionChecklistCalenderRowCallback) _handler.Callback;
		}
	}
}
