// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ConsumptionChecklistCalenderRowViewCell")]
	partial class ConsumptionChecklistCalenderRowViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIView ConsumptionChecklistBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView MealTypeTextView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ConsumptionChecklistBackgroundView != null) {
				ConsumptionChecklistBackgroundView.Dispose ();
				ConsumptionChecklistBackgroundView = null;
			}

			if (MealTypeTextView != null) {
				MealTypeTextView.Dispose ();
				MealTypeTextView = null;
			}
		}
	}
}
