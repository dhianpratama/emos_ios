// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("BeverageConsumptionDisplayViewCell")]
	partial class BeverageConsumptionDisplayViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BeveragemLLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BeverageNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BeverageTimeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BeverageWeightLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BeverageNameLabel != null) {
				BeverageNameLabel.Dispose ();
				BeverageNameLabel = null;
			}

			if (BeverageWeightLabel != null) {
				BeverageWeightLabel.Dispose ();
				BeverageWeightLabel = null;
			}

			if (BeveragemLLabel != null) {
				BeveragemLLabel.Dispose ();
				BeveragemLLabel = null;
			}

			if (BeverageTimeLabel != null) {
				BeverageTimeLabel.Dispose ();
				BeverageTimeLabel = null;
			}
		}
	}
}
