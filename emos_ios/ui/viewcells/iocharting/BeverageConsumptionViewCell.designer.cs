// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("BeverageConsumptionViewCell")]
	partial class BeverageConsumptionViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel mlLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton RemoveButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TimePickerButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel WeightLabel { get; set; }

		[Action ("RemoveButton_TouchDown:")]
		partial void RemoveButton_TouchDown (MonoTouch.Foundation.NSObject sender);

		[Action ("TimePickerButton_TouchUpInside:")]
		partial void TimePickerButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (mlLabel != null) {
				mlLabel.Dispose ();
				mlLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (RemoveButton != null) {
				RemoveButton.Dispose ();
				RemoveButton = null;
			}

			if (TimePickerButton != null) {
				TimePickerButton.Dispose ();
				TimePickerButton = null;
			}

			if (WeightLabel != null) {
				WeightLabel.Dispose ();
				WeightLabel = null;
			}
		}
	}
}
