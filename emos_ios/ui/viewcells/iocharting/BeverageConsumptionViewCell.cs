﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class BeverageConsumptionViewCell : BaseViewCell<ExtraBeverage>
	{
		public const string CellIdentifier = "BeverageConsumptionViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private BeverageConsumptionViewCellHandler _handler;
		private IBeverageConsumptionCallback _callback;
//		private KeyValuePair<string, string> _item;
		private ExtraBeverage _extraBeverage;

		public BeverageConsumptionViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent(ExtraBeverage item)
		{
			_extraBeverage = item;
			NameLabel.Text = item.item;
			WeightLabel.Text = item.value.ToString ();
			TimePickerButton.SetTitle (CommonTools.ConvertToAmPm(item.consumptionTime), UIControlState.Normal);
			RemoveButton.Layer.CornerRadius = 10;
			mlLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("mL");
		}
		public static BeverageConsumptionViewCell Create ()
		{
			return (BeverageConsumptionViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetViewCellHandler(IViewCellHandler<ExtraBeverage> handler)
		{
			_handler = (BeverageConsumptionViewCellHandler) handler;
			_callback = (IBeverageConsumptionCallback) _handler.Callback;
		}
		partial void RemoveButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			_callback.RemoveBeverage(_extraBeverage);
		}

		partial void TimePickerButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.OpenBeveragesTimePicker(_extraBeverage);
		}
	}
}

