﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using VMS_IRIS.Areas.EmosIpad.Models;
using MonoTouch.Dialog.Utilities;
using emos_ios.tools;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class DishSummaryViewCell : BaseViewCell<DishModelSimple>, IImageUpdated
	{
		public const string CellIdentifier = "DishSummaryViewCell";
		private DishModelSimple _item;
		private ImageDownloader _imageDownloader = new ImageDownloader ();
		private BaseViewCellHandler<DishModelSimple> _handler;

		public DishSummaryViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (DishModelSimple item)
		{
			_item = item;
			DownloadImage (_item.picture_url);
			DishTypeLabel.Text = item.dish_type_label;
			DishTypeLabel.SizeToFit ();
			DishNameLabel.Text = String.Format(" - {0}", item.label);
			DishNameLabel.Frame = new RectangleF (DishTypeLabel.Frame.Left + DishTypeLabel.Frame.Width, DishTypeLabel.Frame.Top, DishNameLabel.Frame.Width, DishTypeLabel.Frame.Height);
			DishNameLabel.SizeToFit ();
			DescriptionTextView.Text = item.description;
		}
		protected override void SetViewCellHandler(IViewCellHandler<DishModelSimple> handler)
		{
			_handler = (BaseViewCellHandler<DishModelSimple>) handler;
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			_imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImage.Image = ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this);
			});
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
					DishImage.Image = ImageDownloader.LazyLoad (uri.ToString (), this);
			});
		}
		private void HandleOnImageNotFound (object sender, EventArgs e)
		{
			InvokeOnMainThread (() => {
				DishImage.Image = ImageDownloader.GetNotAvailableImage ();
			});
		}
	}
}