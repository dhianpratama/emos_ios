// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("DishListLabelViewCell")]
	partial class DishListLabelViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel MealOrderLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MealOrderLabel != null) {
				MealOrderLabel.Dispose ();
				MealOrderLabel = null;
			}
		}
	}
}
