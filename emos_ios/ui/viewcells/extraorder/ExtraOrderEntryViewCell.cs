﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public partial class ExtraOrderEntryViewCell : BaseViewCell<ExtraOrderModelSimple>, INameQuantityCallback<SpecialInstructionDetail>
	{
		private const float ChildCellHeight = 44;
		private const float TopViewHeight = 50;
		public const string CellIdentifier = "ExtraOrderEntryViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		private ExtraOrderModelSimple _item;
		private IExtraOrderEntryCallback _callback;
		private NameQuantityViewCellSetting _childSetting;
		private NameQuantityViewCellHandler<SpecialInstructionDetail> _childHandler;
		private MultipleSelectionDataSource<SpecialInstructionDetail, NameQuantityViewCell> _childSource;

		public ExtraOrderEntryViewCell (IntPtr handle) : base (handle)
		{
			InitiateChild ();
		}
		public static ExtraOrderEntryViewCell Create ()
		{
			return (ExtraOrderEntryViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (ExtraOrderModelSimple item)
		{
			this.Layer.BorderColor = UIColor.Black.CGColor;
			this.Layer.BorderWidth = 1.0f;
			TopView.BackgroundColor = UIColor.DarkGray;
			this.BackgroundColor = UIColor.Green;
			NameLabel.TextColor = UIColor.Red;
			_item = item;

			NameLabel.Text = item.label;
			NameLabel.Font = Fonts.Header;

			var servingText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Serving");
			QuantityLabel.Text = servingText;
			QuantityTextField.Text = item.quantity.ToString();
			QuantityTextField.UserInteractionEnabled = false;
			Stepper.Value = (double)item.quantity;
			if (AppDelegate.Instance.CurrentUser.Authorized ("MANAGE_EXTRA_ORDER_TYPE")) {
				Stepper.UserInteractionEnabled = true;
			} else
				Stepper.UserInteractionEnabled = false;
			ShowDetails ();
			QuantityLabel.Text = (Settings.UseExtraOrdersText) ? AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Serving") : 
				AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Quantity");

			ShowSelection (item.selected);
		}
		private void ShowDetails ()
		{
			SuitableView.Hidden = !(_callback.GetBaseContext ().GetBaseUser ().Authorized ("MANAGE_EXTRA_ORDER_TYPE")
				&& _item.details.Any (r => !r.suitable));
			SuitableSwitch.On = !_item.IsSuitableOnly;
			SuitableSwitch.OnTintColor = _callback.GetBaseContext ().ColorHandler.MainThemeColor;
			SetChildSource ();
			DetailsTable.AllowsMultipleSelection = _callback.GetAllowMultipleSelection ();
//			DetailsTable.RowHeight = ChildCellHeight;
			DetailsTable.Source = _childSource;
		}
		private void SetChildSource ()
		{
			_childSource.Items = _item.details;
		}
		private void ShowSelection (bool selected)
		{
			_item.selected = selected;
			ShowChildSelections (_item.details);
			this.SetFrame (height: CalculateRowHeight (_item));
			DetailsTable.SetFrameBelowTo (TopView, height: CalculateDetailsHeight (_item), distanceToAbove: 0);
		}
		private void InitiateChild ()
		{
			_childSetting = new NameQuantityViewCellSetting {
				CellHeight = ChildCellHeight
			};
			_childHandler = new NameQuantityViewCellHandler<SpecialInstructionDetail> (NameQuantityViewCell.CellIdentifier, this, _childSetting) {
				GetHeightForRow = GetDetailHeightForRow
			};
			_childSource = new MultipleSelectionDataSource<SpecialInstructionDetail, NameQuantityViewCell> (_childHandler);
			_childSource.OnItemDeselected += OnDetailDeselected;
			_childSource.OnItemSelected += OnDetailSelected;
		}
		private void OnDetailSelected (object sender, RowSelectionEventArgs<SpecialInstructionDetail> e)
		{
			_callback.DetailSelected (e.Item);
		}
		private void OnDetailDeselected (object sender, RowSelectionEventArgs<SpecialInstructionDetail> e)
		{
			_callback.DetailDeselected (e.Item);
		}
		private void ShowChildSelections (List<SpecialInstructionDetail> details)
		{
			var selectedRows = new List<int> ();
			var deselectedRows = new List<int> ();
			for (int i = 0; i < details.Count; i++) {
				if (details [i].selected) {
					selectedRows.Add (i);
				} else {
					deselectedRows.Add (i);
				}
			}
			if (selectedRows.Count > 0)
				_childSource.SelectRows (selectedRows, DetailsTable);
			if (deselectedRows.Count > 0)
				_childSource.DeselectRows (deselectedRows, DetailsTable);
		}
		protected override void SetViewCellHandler(IViewCellHandler<ExtraOrderModelSimple> handler)
		{
			_callback = (IExtraOrderEntryCallback) handler.Callback;
		}
		public static float CalculateRowHeight(ExtraOrderModelSimple item)
		{
			return CalculateDetailsHeight (item) + TopViewHeight;
		}
		public static float CalculateDetailsHeight (ExtraOrderModelSimple item)
		{
			if (item.quantity == 0)
				return 0;
			var count = item.IsSuitableOnly ? item.details.Where (d => d.suitable).Count () : item.details.Count;
			return count * ChildCellHeight;
		}
		private float GetDetailHeightForRow (SpecialInstructionDetail item)
		{
			if (item.suitable || !_item.IsSuitableOnly)
				return ChildCellHeight;
			else
				return 0;
		}
		public override void HandleRowSelected(ExtraOrderModelSimple item)
		{
			if (item.id != _item.id)
				return;
			ShowSelection (true);
			_callback.ValueChanged (_item);
		}
		public override void HandleRowDeselected(ExtraOrderModelSimple item)
		{
			if (item.id != _item.id)
				return;
			_item.quantity = 0;
			ShowSelection (false);
			_callback.ValueChanged (_item);
		}
		public void DetailQuantityChanged (SpecialInstructionDetail item)
		{
			var detail = _item.details.First (d => d.id == item.id);
			detail.selected = item.selected;
			detail.quantity = item.quantity;
			_callback.ValueChanged (_item);
		}
		private void DeselectDetails (long? id)
		{
			var deselectedRows = new List<int> ();
			for (int i = 0; i < _item.details.Count; i++) {
				if (!_item.details[i].selected || _item.details[i].id == id)
					continue;
				deselectedRows.Add(i);
			}
			_childSource.DeselectRows (deselectedRows, DetailsTable);
		}
		public void DetailSelectionChanged(SpecialInstructionDetail item)
		{
			for (int i = 0; i < _item.details.Count; i++) {
				var detail = _item.details [i];
				if (_item.details [i].id != item.id)
					continue;
				_item.details [i].selected = item.selected;
				if (item.selected) {
					_childSource.SelectRow (i, DetailsTable);
				} else {
					_childSource.DeselectRow (i, DetailsTable);
				}
			}
			if (!_callback.GetAllowMultipleSelection ())
				DeselectDetails (item.id);
		}
		partial void QuantityTextField_EditingChanged (MonoTouch.Foundation.NSObject sender)
		{
		}
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			var previousQuantity = _item.quantity;
			QuantityTextField.Text = Stepper.Value.ToString();
			_item.quantity = (long) Stepper.Value;
			if (Stepper.Value == 0) {
				_item.selected = false;
				_item.details.ForEach(d => d.quantity = 0);
				DeselectDetails(-1);
				SetChildSource();
				DetailsTable.ReloadData();
				_callback.SelectionChanged(_item, true);
			} else if (Stepper.Value == 1 && previousQuantity == 0) {
				_item.selected = true;
				SetChildSource();
				DetailsTable.ReloadData();
				_callback.SelectionChanged(_item, true);
			} else {
				_callback.ValueChanged(_item);
			}
		}
		private long ConvertToLong (string value)
		{
			long quantity;
			if (long.TryParse (value, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out quantity))
				return quantity;
			else
				return 0;
		}
		public void ItemSelected (SpecialInstructionDetail selected, int selectedIndex)
		{
		}
		public bool GetAllowChangeQuantity ()
		{
			return _callback.GetAllowMultipleSelection ();
		}
		public ios.IBaseContext GetBaseContext ()
		{
			return _callback.GetBaseContext ();
		}
		partial void SuitableSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_callback.SuitableChanged(_item, !SuitableSwitch.On);
		}
	}
}

