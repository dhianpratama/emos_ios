// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ExtraOrderEntryViewCell")]
	partial class ExtraOrderEntryViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITableView DetailsTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel QuantityLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField QuantityTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIStepper Stepper { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch SuitableSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView SuitableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopView { get; set; }

		[Action ("QuantityTextField_EditingChanged:")]
		partial void QuantityTextField_EditingChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("Stepper_ValueChanged:")]
		partial void Stepper_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("SuitableSwitch_ValueChanged:")]
		partial void SuitableSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DetailsTable != null) {
				DetailsTable.Dispose ();
				DetailsTable = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (QuantityLabel != null) {
				QuantityLabel.Dispose ();
				QuantityLabel = null;
			}

			if (QuantityTextField != null) {
				QuantityTextField.Dispose ();
				QuantityTextField = null;
			}

			if (Stepper != null) {
				Stepper.Dispose ();
				Stepper = null;
			}

			if (TopView != null) {
				TopView.Dispose ();
				TopView = null;
			}

			if (SuitableView != null) {
				SuitableView.Dispose ();
				SuitableView = null;
			}

			if (SuitableSwitch != null) {
				SuitableSwitch.Dispose ();
				SuitableSwitch = null;
			}
		}
	}
}
