// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ExtraOrderViewCell")]
	partial class ExtraOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITableView ExtraOrderDetailsTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealPeriodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MealPeriodView { get; set; }

		[Action ("Decrease:")]
		partial void Decrease (MonoTouch.Foundation.NSObject sender);

		[Action ("Increase:")]
		partial void Increase (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ExtraOrderDetailsTable != null) {
				ExtraOrderDetailsTable.Dispose ();
				ExtraOrderDetailsTable = null;
			}

			if (MealPeriodLabel != null) {
				MealPeriodLabel.Dispose ();
				MealPeriodLabel = null;
			}

			if (MealPeriodView != null) {
				MealPeriodView.Dispose ();
				MealPeriodView = null;
			}
		}
	}
}
