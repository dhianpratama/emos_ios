﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class ExtraOrderDetailsViewCell  : BaseViewCell<SpecialInstructionDetail>
	{
		public const string CellIdentifier = "ExtraOrderDetailsViewCell";
//		private NoCallbackViewCellHandler<SpecialInstructionDetail> _handler;
//		private SpecialInstructionDetail _item;
//		private int _quantity;
		private const int NoQuantitySet = -1;

		public ExtraOrderDetailsViewCell (IntPtr handle) : base (handle)
		{
//			_quantity = NoQuantitySet;
		}
		protected override void SetCellContent (SpecialInstructionDetail item)
		{
//			_item = item;
			if (String.IsNullOrEmpty (item.label))
				ExtraOrderLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString("NoSpecialInstructionassignedbydietitian.");
			else
				ExtraOrderLabel.Text = item.label;
		}
		protected override void SetViewCellHandler(IViewCellHandler<SpecialInstructionDetail> handler)
		{
//			_handler = (NoCallbackViewCellHandler<SpecialInstructionDetail>) handler;
		}
	}
}


