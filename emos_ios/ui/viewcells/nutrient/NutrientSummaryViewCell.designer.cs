// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("NutrientSummaryViewCell")]
	partial class NutrientSummaryViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel CumulativeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel UnitLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ValueLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CumulativeLabel != null) {
				CumulativeLabel.Dispose ();
				CumulativeLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (UnitLabel != null) {
				UnitLabel.Dispose ();
				UnitLabel = null;
			}

			if (ValueLabel != null) {
				ValueLabel.Dispose ();
				ValueLabel = null;
			}
		}
	}
}
