﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public partial class NutrientBreakdownViewCell : BaseViewCell<DishConsumption>
	{
		public const string CellIdentifier = "NutrientBreakdownViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
//		private DishConsumption _item;
		private DynamicTableSource<NutrientForConsumptionChecklist, NutrientSummaryViewCell> _nutrientSource;

		public NutrientBreakdownViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (DishConsumption item)
		{
//			_item = item;
			NameLabel.Text = item.Dish.label;
			NameLabel.Font = Fonts.Header;
			NutrientTable.RowHeight = NutrientSummaryViewCell.CellHeight;
			NutrientTable.ScrollEnabled = true;
			//NutrientTable.SetFrame (width: 768, height: CalculateDetailsRowHeight (item) - 200f);
			//NutrientTable.ClipsToBounds = true;

			_nutrientSource.Items = item.nutrients;
			NutrientTable.Source = _nutrientSource;
			var heightForView =  CalculateRowHeight (item);
			this.SetFrame (height:heightForView);
		}
		protected override void SetViewCellHandler(IViewCellHandler<DishConsumption> handler)
		{
			var setting = new BaseViewCellSetting { CellHeight = NutrientSummaryViewCell.CellHeight };
			var nutrientHandler = new NoCallbackViewCellHandler<NutrientForConsumptionChecklist> (handler.Callback.GetBaseContext (), NutrientSummaryViewCell.CellIdentifier, setting);
			_nutrientSource = new DynamicTableSource<NutrientForConsumptionChecklist, NutrientSummaryViewCell> (nutrientHandler);
		}
		public static NutrientBreakdownViewCell Create ()
		{
			return (NutrientBreakdownViewCell)Nib.Instantiate (null, null) [0];
		}
		public static float CalculateRowHeight(DishConsumption item)
		{
			return CalculateDetailsRowHeight (item) + 40;
		}
		public static float CalculateDetailsRowHeight(DishConsumption item)
		{
			return (item.nutrients.Count * NutrientSummaryViewCell.CellHeight);
		}

	}
}

