﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class NutrientSummaryViewCell : BaseViewCell<NutrientForConsumptionChecklist>
	{
		public const float CellHeight = 44;
		public const string CellIdentifier = "NutrientSummaryViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
//		private NutrientForConsumptionChecklist _item;

		public NutrientSummaryViewCell (IntPtr handle) : base (handle)
		{
		}

		public static NutrientSummaryViewCell Create ()
		{
			return (NutrientSummaryViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (NutrientForConsumptionChecklist item)
		{
//			_item = item;
			NameLabel.Text = item.label;
			ValueLabel.Text = Math.Round ((decimal) item.value, 1).ToString ();
			CumulativeLabel.Text = Math.Round ((decimal) (item.cumulativeValue ?? 0), 1).ToString ();

			if (CumulativeLabel.Text == "0.0")
				CumulativeLabel.Text = "0";

			if (ValueLabel.Text == "0.0")
				ValueLabel.Text = "0";

			if (item.cumulativeValue == null)
				CumulativeLabel.Text = "";

			UnitLabel.Text = "";
			this.SetFrame (x:0, y:0, width: 534, height: CellHeight);
		}
	}
}

