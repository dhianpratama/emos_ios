// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("DishSummaryViewCell")]
	partial class DishSummaryViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITextView DescriptionTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView DishImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DishTypeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (DescriptionTextView != null) {
				DescriptionTextView.Dispose ();
				DescriptionTextView = null;
			}

			if (DishImage != null) {
				DishImage.Dispose ();
				DishImage = null;
			}

			if (DishNameLabel != null) {
				DishNameLabel.Dispose ();
				DishNameLabel = null;
			}

			if (DishTypeLabel != null) {
				DishTypeLabel.Dispose ();
				DishTypeLabel = null;
			}
		}
	}
}
