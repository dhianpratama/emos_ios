﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class ExtraOrderViewCell : BaseViewCell<ExtraOrderModelSimple>
	{
		public const string CellIdentifier = "ExtraOrderViewCell";
		private const int ExtraOrderDetailsRowHeight = 40;
//		private ExtraOrderModelSimple _item;
		private DynamicTableSource<SpecialInstructionDetail, ExtraOrderDetailsViewCell> _extraOrderDetailsSource;

		public ExtraOrderViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (ExtraOrderModelSimple item)
		{
//			_item = item;
			MealPeriodView.Layer.CornerRadius = 10;
			MealPeriodLabel.Text = item.label;
			_extraOrderDetailsSource.Items = item.details ?? new List<SpecialInstructionDetail>();
			ExtraOrderDetailsTable.Source = _extraOrderDetailsSource;
			ExtraOrderDetailsTable.RowHeight = ExtraOrderDetailsRowHeight;
			ExtraOrderDetailsTable.Frame = new RectangleF (0, MealPeriodView.Frame.Height, 534, ExtraOrderDetailsRowHeight * item.details.Count ());
			ExtraOrderDetailsTable.BackgroundColor = Colors.DarkViewCellBackground;
		}
		protected override void SetViewCellHandler(IViewCellHandler<ExtraOrderModelSimple> handler)
		{
			var setting = new BaseViewCellSetting { CellHeight = ExtraOrderDetailsRowHeight };
			var cellHandler = new NoCallbackViewCellHandler<SpecialInstructionDetail> (handler.Callback.GetBaseContext (), ExtraOrderDetailsViewCell.CellIdentifier, setting);
			_extraOrderDetailsSource = new DynamicTableSource<SpecialInstructionDetail, ExtraOrderDetailsViewCell> (cellHandler);
		}
	}
}

