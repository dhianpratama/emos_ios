// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("PatientDayOrderViewCell")]
	partial class PatientDayOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BedLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton BirthdayButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView BirthdayCakeChecklistIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView BottomBorder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ExtraOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView ExtraOrderChecklistImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton IdButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LeftBorder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NameButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView OrderContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView RightBorder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView SnackPackImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopBorder { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TrialOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView TrialOrderChecklistImage { get; set; }

		[Action ("BirthdayButtonClicked:")]
		partial void BirthdayButtonClicked (MonoTouch.Foundation.NSObject sender);

		[Action ("ExtraOrderButton_TouchUpInside:")]
		partial void ExtraOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("IdButton_TouchUpInside:")]
		partial void IdButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("NameButton_TouchUpInside:")]
		partial void NameButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("TrialButton_TouchUpInside:")]
		partial void TrialButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BedLabel != null) {
				BedLabel.Dispose ();
				BedLabel = null;
			}

			if (BirthdayButton != null) {
				BirthdayButton.Dispose ();
				BirthdayButton = null;
			}

			if (BirthdayCakeChecklistIcon != null) {
				BirthdayCakeChecklistIcon.Dispose ();
				BirthdayCakeChecklistIcon = null;
			}

			if (BottomBorder != null) {
				BottomBorder.Dispose ();
				BottomBorder = null;
			}

			if (ExtraOrderButton != null) {
				ExtraOrderButton.Dispose ();
				ExtraOrderButton = null;
			}

			if (ExtraOrderChecklistImage != null) {
				ExtraOrderChecklistImage.Dispose ();
				ExtraOrderChecklistImage = null;
			}

			if (IdButton != null) {
				IdButton.Dispose ();
				IdButton = null;
			}

			if (LeftBorder != null) {
				LeftBorder.Dispose ();
				LeftBorder = null;
			}

			if (NameButton != null) {
				NameButton.Dispose ();
				NameButton = null;
			}

			if (OrderContainerView != null) {
				OrderContainerView.Dispose ();
				OrderContainerView = null;
			}

			if (RightBorder != null) {
				RightBorder.Dispose ();
				RightBorder = null;
			}

			if (SnackPackImage != null) {
				SnackPackImage.Dispose ();
				SnackPackImage = null;
			}

			if (TopBorder != null) {
				TopBorder.Dispose ();
				TopBorder = null;
			}

			if (TrialOrderButton != null) {
				TrialOrderButton.Dispose ();
				TrialOrderButton = null;
			}

			if (TrialOrderChecklistImage != null) {
				TrialOrderChecklistImage.Dispose ();
				TrialOrderChecklistImage = null;
			}
		}
	}
}
