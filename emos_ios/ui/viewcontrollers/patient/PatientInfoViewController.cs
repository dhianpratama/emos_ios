﻿
using System;
using System.Drawing;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using System.Collections.Generic;
using System.Text;

namespace emos_ios
{
	public partial class PatientInfoViewController : BaseViewController
	{
		private const float EntryElementHeight = 50;
		private const float EntryElementWidth = 766;
		private const float CaptionWidth = 250;
		private const float BorderSize = 1.0f;
		private UIView _controlBackgroundView = new UIView {
			BackgroundColor = Colors.DarkViewCellBackground,
			Frame = new RectangleF(0, 0, 768, 0),
		};

		private PatientInfoModel _model;

		public PatientInfoViewController () : base (AppDelegate.Instance, "PatientInfoViewController", null)
		{
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			AnimateMe ();
		}
		private void AnimateMe()
		{
			BackgroundView.Frame = new RectangleF (0, UIScreen.MainScreen.Bounds.Bottom, UIScreen.MainScreen.Bounds.Right, 700);
			UIView.BeginAnimations ("slideAnimation");
			UIView.SetAnimationDuration (0.5);
			UIView.SetAnimationDelegate (this);
			BackgroundView.Center = new PointF (UIScreen.MainScreen.Bounds.Right - BackgroundView.Frame.Width / 2, Measurements.FrameTop + BackgroundView.Frame.Height / 2);
			UIView.CommitAnimations ();
		}
		public void Load ()
		{
			if (_model == null)
				_requestHandler.SendRequest (View, RequestPatientInfo);
			else
				OnRequestCompleted (_model);
		}
		private async void RequestPatientInfo()
		{
			string param;
			if (Settings.MealOrderBasedPatientInfo)
				param = KnownUrls.GetMealOrderModelQueryString (AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, AppDelegate.Instance.OperationDate);
			else
				param = KnownUrls.RegistrationIdQueryString (AppDelegate.Instance.SelectedRegistration.registration_id.Value);

			await AppDelegate.Instance.HttpSender.Request<PatientInfoModel> ()
				.From (KnownUrls.GetPatientInfo)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestCompleted (result))
				.WhenFail (result => base.OnRequestCompleted (false))
				.Go ();
		}
		protected void OnRequestCompleted(PatientInfoModel result)
		{
			if (result == null || String.IsNullOrEmpty (result.patientName)) {
				base.OnRequestCompleted (false);
				return;
			}
			base.OnRequestCompleted (true);
			_model = result;
			BackgroundView.BackgroundColor = UIColor.Clear;

			var scrollView = new UIScrollView {
				BackgroundColor = UIColor.Clear,
				Frame = new RectangleF(0, TopView.Frame.Height, 768, 1024 - BackgroundView.Frame.Top - TopView.Frame.Height),
				ContentSize = new SizeF (768, 1024 - BackgroundView.Frame.Top - TopView.Frame.Height),
				AutoresizingMask = UIViewAutoresizing.FlexibleHeight,
			};

			_controlBackgroundView.Layer.BorderColor = Colors.DarkViewCellBackground.CGColor;
			_controlBackgroundView.Layer.BorderWidth = 1.0f;
			var y = 0;

			var genderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Gender");
			var raceText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Race");
			var patientNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientName");
			var admissionTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionTime");
			var IDNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("IDNumber");
			var WardBedText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("WardBed");
			var dietsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Diets");
			var specialInstructionText = (Settings.UseExtraOrdersText)? "Extra Orders" : 
				AppDelegate.Instance.LanguageHandler.GetLocalizedString("SpecialInstructions");

			var dietCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("DietComment");
			var clearFeedCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("ClearFeedComment");
			var fullFeedCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("FullFeedComment");
			var fluidCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("FluidComment");
			var extraOrderCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("ExtraOrderComment");
			var stTrialCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("STTrialComment");
			_controlBackgroundView.AddSubview(CreateElement (y++, patientNameText, _model.patientName));
			_controlBackgroundView.AddSubview(CreateElement (y++, IDNumberText, _model.idNumber));
			_controlBackgroundView.AddSubview(CreateElement (y++, admissionTimeText, _model.admissionTime.ToString("dddd, dd MMMM yyyy")));
			_controlBackgroundView.AddSubview(CreateElement (y++, WardBedText, _model.wardBed));
			_controlBackgroundView.AddSubview(CreateElement (y++, raceText, _model.race));
			_controlBackgroundView.AddSubview(CreateElement (y++, genderText, _model.gender));

			y = AddToNewLine (y, _model.diets, dietsText);

			if(_model.specialInstructions != null && _model.specialInstructions.Length > 0)
				y = AddToNewLine (y, _model.specialInstructions, specialInstructionText);

			if(_model.therapeuticComments != null && _model.therapeuticComments.Length > 0)
				y = AddToNewLine (y, _model.therapeuticComments, dietCommentText);

			if(_model.clearFeedComments != null && _model.clearFeedComments.Length > 0)
				y = AddToNewLine (y, _model.clearFeedComments, clearFeedCommentText);

			if(_model.fullFeedComments != null && _model.fullFeedComments.Length > 0)
				y = AddToNewLine (y, _model.fullFeedComments, fullFeedCommentText);

			if(_model.fluidComments != null && _model.fluidComments.Length > 0)
				y = AddToNewLine (y, _model.fluidComments, fluidCommentText);

			if(_model.extraOrderComments != null && _model.extraOrderComments.Length > 0)
				y = AddToNewLine (y, _model.extraOrderComments, extraOrderCommentText);

			if(_model.trialOrderComments != null && _model.trialOrderComments.Length > 0)
				y = AddToNewLine (y, _model.trialOrderComments, stTrialCommentText);

			AddLineSeparatedStrings (result.nbms, "NBM", ref y);
			var tempY = y;
			AddSeparatedStrings (result.textures, "Texture", ref y);
			AddSeparatedStrings (result.therapeutics, "Therapeutic", ref y, y - tempY - 1);

			scrollView.ContentSize = new SizeF (768, (y * BorderSize) + (y * EntryElementHeight));
			scrollView.Add (_controlBackgroundView);
			BackgroundView.Add (scrollView);
		}
		private void AddLineSeparatedStrings (List<string> values, string titleKey, ref int y)
		{
			if (!values.Any ())
				return;
			var caption = AppDelegate.Instance.LanguageHandler.GetLocalizedString (titleKey);
			for (var i = 0; i < values.Count; i++) {
				var element = CreateMultilineElement (y++, i == 0 ? caption : "", values[i], 0);
				_controlBackgroundView.AddSubview (element);
			}
		}
		private void AddSeparatedStrings (List<string> values, string titleKey, ref int y, float heightAdjustment = 0, string separator = ", ")
		{
			if (!values.Any ())
				return;
			var content = String.Join (separator, values);
			var caption = AppDelegate.Instance.LanguageHandler.GetLocalizedString (titleKey);
			var element = CreateMultilineElement (y, caption, content, heightAdjustment);
			_controlBackgroundView.AddSubview (element);
			y += element.Row;
		}
		private int AddToNewLine(int y, string unsplitText, string caption)
		{
			if (unsplitText.Count() > 0) {
				var str = unsplitText.Split (',').ToList();
				int i = 0;

				str.ForEach(r => 
					{ 
						_controlBackgroundView.AddSubview(CreateElement (y++, (i == 0)? caption : "", r.Trim())); 
						i ++;
					});
			}
			return y;
		}
		private UIView CreateElement (float y, string caption, string text)
		{
			var frame = new RectangleF (BorderSize, (y * BorderSize) + (y * EntryElementHeight), EntryElementWidth, EntryElementHeight);
			var element = new RoundedEntryElement (frame, CaptionWidth, caption, false);
			element.TextField.Text = text;
			element.SetCaptionFont (20, true);
			return element;
		}
		private RoundedMultiLineEntryElement CreateMultilineElement (float y, string caption, string text, float heightAdjustment)
		{
			var frame = new RectangleF (BorderSize, (y * BorderSize) - heightAdjustment + (y * EntryElementHeight), EntryElementWidth, EntryElementHeight);
			var element = new RoundedMultiLineEntryElement (frame, CaptionWidth, caption, false, fitString: true, content: text, multiLineCaption: false);
			element.SetCaptionFont (20, true);
			var texts = text.Split ('\n');
			for (var i = 0; i < texts.Count (); i++) {
				if (i > 0)
					element.TextView.InsertText (new NSString (Environment.NewLine));
				element.TextView.InsertText (texts [i]);
			}
			return element;
		}
		partial void CloseButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			View.RemoveFromSuperview();
		}
		public override void ShowBottomLogo (float x = 0, float y = 920, float width = 768, float height = 124)
		{
		}
	}
}

