using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using System.Web;
using System.Linq;
using System.Globalization;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class MainViewController : BaseViewController, IPatientDayOrderCallback, IPatientMenu, IPatientOrderCommandCallback, ILock
	{
		private NurseDashboardHeaderView _nurseDashboardHeaderView;
		private PatientMealOrderView _patientMealOrderView;
		private long? _selectedWardId;
		private string _selectedMealOrderPeriodGroupCode;
		private List<MealOrderPeriodGroupModel> _mealPeriodGroups;
		private UIBarButtonItem _settingButton, _refreshButton, _cutoffButton;
		private KeyValuePair<string, string> _selectedWard;
		private PatientMenuViewController _patientMenuViewController;
		private InterfaceStatusModel _interfaceStatus;
		private LocationWithRegistrationSimple _patientMenuLocation, _locationForBirthday;
		private BirthdayCakeViewController _birthdayCakeViewController;
		public emos_ios.Dashboard.OrderTabs CurrentTab { get; set; }
		private PatientOrderCommand _selectedPatientMealOrderCommand;
		private LocationWithRegistrationSimple _selectedBed;
		private RegistrationModelSimple _selectedRegistration;
		private List<PatientOrderCommand> _sequentialOrderCommands = new List<PatientOrderCommand>();
		public bool ShouldRunViewWillAppear { get; set; }
		public LockResultIpad LockResult { get { return AppContext.LockHandler.LockResult; } }
		private bool _isOrdering;
		private string _orderBy;

		public MainViewController (emos_ios.Dashboard.OrderTabs tab) : base (AppDelegate.Instance, "MainViewController", null)
		{
			try {
				CurrentTab = tab;
				_selectedMealOrderPeriodGroupCode = "MEAL";
				NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
				SetTitle ();
				InitiateLock ();
				_isOrdering = false;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();
				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();

				BackgroundView.Frame = new RectangleF (0, Measurements.TopY, 768, Measurements.PageHeight);
				if (!AppDelegate.Instance.CurrentUser.IsAdmin)
					OnDashboardChanged (CurrentTab, false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewWillAppear (bool animated)
		{
			try {
				base.ViewWillAppear (animated);
				if (!ShouldRunViewWillAppear) {
					ShouldRunViewWillAppear = true;
					return;
				}
				UnlockRegistration ();
				if (LoadingPatientMealOrder ())
					return;
				Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewDidDisappear (bool animated)
		{
			try {
				base.ViewDidDisappear (animated);
				ShouldRunViewWillAppear = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private bool LoadingPatientMealOrder ()
		{
			try {
				if (_isOrdering) {
					_isOrdering = false;
					if (ShouldLoadPatientMealOrder (_orderBy)) {
						PatientMealOrder (AppDelegate.Instance.SelectedLocation, AppDelegate.Instance.SelectedRegistration);
						return true;
					}
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
			return false;
		}

		private void InitiateLock ()
		{
			try {
				ShouldRunViewWillAppear = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void UnlockRegistration ()
		{
			try {
				AppContext.LockHandler.UnlockRegistration ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void Refresh()
		{
			try {
				_sequentialOrderCommands = new List<PatientOrderCommand> ();
				if (CurrentTab == emos_ios.Dashboard.OrderTabs.NurseDashboard)
					_requestHandler.SendRequest (View, RequestNurseDashboardModel);
				else if (CurrentTab == emos_ios.Dashboard.OrderTabs.PatientMealOrder)
					_requestHandler.SendRequest (View, RequestPatientMealOrderModel);
				else if (CurrentTab == emos_ios.Dashboard.OrderTabs.CompanionMealOrder)
					_requestHandler.SendRequest (View, RequestCompanionDashboard);
				SyncDateLogo ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SyncDateLogo()
		{
			try {
				if (Settings.UseDateAsDashboardTitle) {
					CultureInfo languageCode = new CultureInfo (AppDelegate.Instance.LanguageHandler.LanguageCode);
					NavigationItem.Title = AppDelegate.Instance.OperationDate.ToString (Formats.OperationDateFormat, languageCode);
					return;
				}
				if (LogoView == null)
					return;
				var operationDate = AppContext.OperationDate.ToString (Formats.OperationDateFormat, new CultureInfo (AppContext.LanguageHandler.LanguageCode));
				if (LogoView != null)
					LogoView.ChangeOperationDateLabel (operationDate);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void SetTitle ()
		{
			try {
				if (Settings.UseDateAsDashboardTitle) {
					CultureInfo languageCode = new CultureInfo (AppDelegate.Instance.LanguageHandler.LanguageCode);
					NavigationItem.Title = AppDelegate.Instance.OperationDate.ToString (Formats.OperationDateFormat, languageCode);
					return;
				}
				switch (CurrentTab) {
				case Dashboard.OrderTabs.NurseDashboard:
					NavigationItem.Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NurseDashboard");
					break;
				case Dashboard.OrderTabs.PatientMealOrder:
					NavigationItem.Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientMealOrder");
					break;
				case Dashboard.OrderTabs.CompanionMealOrder:
					if(Settings.ChangeCompanionLabelToLodger)
						NavigationItem.Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("LodgerMealOrder");
					else
						NavigationItem.Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CompanionMealOrder");
					break;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			try {
				_refreshButton = new UIBarButtonItem (UIBarButtonSystemItem.Refresh, (sender, args) => {
					Refresh();
				});
				_settingButton = new UIBarButtonItem (UIImage.FromFile ("Images/gear.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
					ShowSetting ();
				});
				var buttons = new List<UIBarButtonItem> {
					_settingButton, _refreshButton
				};
				if (Settings.AllowDashboardCutoffToggle) {
					_cutoffButton = new UIBarButtonItem (UIImage.FromFile ("Images/clock_thumb.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
						if (_nurseDashboardHeaderView != null)
							_nurseDashboardHeaderView.ToggleCutoffVisibility ();
					});
					buttons.Add (_cutoffButton);
				}
				return buttons;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private void LoadPatientDailyOrderController ()
		{
			try {
				if (!Settings.PatientDailyOrderMode)
					return;
				if (_nurseDashboardHeaderView.OriginalHeaderCount == _sequentialOrderCommands.Count) {
					PopToViewControllerAndUnlock ();
					Refresh ();
					return;
				}
				var controller = new PatientDailyOrderViewController (this, this, _nurseDashboardHeaderView, _selectedBed, _sequentialOrderCommands);
				controller.OnFinish += CloseChild;
				NavigationController.PushViewController (controller, false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void FilterMealPeriodGroup ()
		{
			try {
				if (_mealPeriodGroups == null)
					_requestHandler.SendRequest (View, () => RequestMealPeriodGroups ());
				else
					OnRequestMealPeriodGroupsSuccessful (_mealPeriodGroups);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestNurseDashboardModel()
		{
			try {
				if (AppDelegate.Instance.WardId == null || AppDelegate.Instance.WardId == 0) {
					_requestHandler.StopRequest ();
					return;
				}
				if (_nurseDashboardHeaderView != null)
					_nurseDashboardHeaderView.HidePatientDayOrderTableVisibility ();
				var param = KnownUrls.GetNurseDashboardQueryString (AppDelegate.Instance.MealPeriodGroupCode, AppDelegate.Instance.InstitutionId, AppDelegate.Instance.WardId, AppDelegate.Instance.OperationDate, AppDelegate.Instance.HospitalId);

				var requestdashboardText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Requestdashboard");

				await AppDelegate.Instance.HttpSender.Request<NurseDashboardModel> ()
					.From (KnownUrls.GetNurseDashboardModel)
					.WithQueryString (param)
					.WithContent (AppDelegate.Instance.OperationDate)
					.WhenSuccess (result => OnRequestNurseSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false, requestdashboardText))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnRequestNurseSuccessful (NurseDashboardModel result)
		{
			try {
				OnRequestCompleted (true);
				AppDelegate.Instance.WardGroupId = result.wardGroupId;
				_nurseDashboardHeaderView = new NurseDashboardHeaderView (this, Dashboard.DashboardMenu.NurseDashboard) {
					Frame = new RectangleF (0, 0, 768, BackgroundView.Frame.Height)
				};
				BackgroundView.AddSubview(_nurseDashboardHeaderView);
				_nurseDashboardHeaderView.Load (result);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PatientNameClicked(LocationWithRegistrationSimple location)
		{
			try {
				_patientMenuLocation = location;
				if (location.registrations.Count > 0) {
					_selectedRegistration = location.registrations.First ();
					_requestHandler.SendRequest (View, GetInterfaceStatusForPatientMenu);
				} else {
					_requestHandler.SendRequest (View, GetInterfaceStatus);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void IdClicked(LocationWithRegistrationSimple location)
		{
			try {
				PatientNameClicked (location);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void PatientContextClicked (LocationWithRegistrationSimple location)
		{
			try {
				var registration = location.registrations.FirstOrDefault ();
				if (registration == null || registration.registration_id == 0)
					ManualAdmission (location);
				else {
					PatientNameClicked (location);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OrderButtonClicked(PatientOrderCommand patientOrderCommand)
		{
			try {
				if (!OperationCodeHandler.IsClickableOnDashboard (patientOrderCommand.OperationCode))
					return;

				if (!patientOrderCommand.Registration.canDoMealOrder) {
					this.ShowAlert (AppContext, AppContext.LanguageHandler.GetLocalizedString(LanguageKeys.DietOrderNotSet), null, false);
					return;
				} else if (patientOrderCommand.Registration.has_other_allergies) {
					this.ShowAlert (AppContext, "Mealorderingisnotallowedduetounclassifiedallergies.");
					return;
				}

				AssignDataBeforeOpenMealOrder (patientOrderCommand);
				if (OperationCodeHandler.ShouldGoToMealOrderWithoutCheckingRole (patientOrderCommand.OperationCode)) {
					LoadMealOrderController ();
					return;
				}

				if (Settings.CheckLock)
					CheckLock (LockRequestCode.MealOrder);
				else
					LoadMealOrder ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void AssignDataBeforeOpenMealOrder(PatientOrderCommand patientOrderCommand)
		{
			try {
				if (AppDelegate.Instance.SelectedRegistration == null || AppDelegate.Instance.SelectedRegistration.profile_id != patientOrderCommand.Registration.profile_id)
					_sequentialOrderCommands.Clear ();

				AppDelegate.Instance.SelectedLocation = patientOrderCommand.Location;
				AppDelegate.Instance.SelectedRegistration = patientOrderCommand.Registration;
				AppDelegate.Instance.SelectedMealOrderPeriod = patientOrderCommand.MealOrderPeriod;
				AppDelegate.Instance.SelectedOperationCode = patientOrderCommand.OperationCode;

				var state = GetTabStatus (CurrentTab);
				if (state == Dashboard.DashboardMenu.CompanionMealOrder) {
					var controller = new CompanionOrderListViewController (patientOrderCommand);
					controller.ViewOnly = OperationCodeHandler.IsOrdered (patientOrderCommand.OperationCode);
					NavigationController.PushViewController (controller, true);
					return;
				}
				_selectedRegistration = patientOrderCommand.Registration;
				_selectedPatientMealOrderCommand = patientOrderCommand;
				_selectedBed = _nurseDashboardHeaderView.GetBed (patientOrderCommand);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private void RecordSequentialOrder (PatientOrderCommand patientOrderCommand)
		{
			try {
				_sequentialOrderCommands.Add (patientOrderCommand);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void LoadMealOrder ()
		{
			try {
				if (Settings.ShowOrderRoleSelection) {
					LoadOrderRoleSelectionController ();
					return;
				}
				LoadMealOrderController ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private OrderRoleViewController _orderRoleViewController;
		private void LoadOrderRoleSelectionController ()
		{
			try {
				_orderRoleViewController = new OrderRoleViewController (AppContext);
				_orderRoleViewController.IsPatient = false;
				_orderRoleViewController.IsCompanion = CurrentTab == emos_ios.Dashboard.OrderTabs.CompanionMealOrder;
				_orderRoleViewController.OnRoleSelected += HandleOnRoleSelected;
				NavigationController.PushViewController (_orderRoleViewController, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnRoleSelected (object sender, StringEventArgs e)
		{
			try {
				_orderBy = e.Text;
				ShouldRunViewWillAppear = false;
				PopToViewControllerAndUnlock (false, false);
				bool isPatient = !String.IsNullOrEmpty (e.Text);
				LoadMealOrderController (isPatient, e.Text);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void LoadMealOrderController (bool isPatient = false, string orderBy = "")
		{
			try {
				_isOrdering = true;
				var state = GetTabStatus (CurrentTab);
				_selectedPatientMealOrderCommand.OrderBy = orderBy;
				var controller = new MealOrderViewController (_selectedPatientMealOrderCommand, state, this);
				controller.IsPatient = isPatient;
				controller.OnClose += HandleMealOrderClose;
				controller.OnSave += HandleSuccessfulSave;
				controller.OnDecline += HandleSuccessfulSave;
				NavigationController.PushViewController (controller, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void CloseChild (object sender, EventArgs e)
		{
			try {
				PopToViewControllerAndUnlock ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleMealOrderClose (object sender, PatientOrderCommandEventArgs e)
		{	
			try {
				if (LoadingPatientMealOrder ())
					return;
				PopToViewControllerAndUnlock ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private bool ShouldLoadPatientMealOrder(string orderBy)
		{
			try {
				return Settings.ShowOrderRoleSelection && (orderBy == OrderRole.Patient || orderBy == OrderRole.Caregiver);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		private void HandleSuccessfulSave (object sender, PatientOrderCommandEventArgs e)
		{
			try {
				if (LoadingPatientMealOrder ())
					return;
				var state = GetTabStatus (CurrentTab);
				if (!Settings.PatientDailyOrderMode || state != Dashboard.DashboardMenu.NurseDashboard) {
					CloseChild (sender, e);
					ShouldRunViewWillAppear = true;
					return;
				}
				PopToViewControllerAndUnlock (false, false);
				RecordSequentialOrder (e.PatientOrderCommand);
				LoadPatientDailyOrderController ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ItemSelected(LocationWithRegistrationSimple selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ItemSelected(PatientOrderCommand selected, int selectedIndex)
		{
			try {
				//this.ShowAlert ("Item selected");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ManualAdmission(LocationWithRegistrationSimple location)
		{
			try {
				_patientMenuLocation = location;
				_requestHandler.SendRequest (View, GetInterfaceStatus);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ItemSelected(LocationModelSimple ward, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestMealPeriodGroups()
		{
			try {
				var mealPeriodGroup = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealGroup");
				await AppDelegate.Instance.HttpSender.Request<List<MealOrderPeriodGroupModel>> ()
					.From (KnownUrls.GetAllMealPeriodGroups)
					.WhenSuccess (result => OnRequestMealPeriodGroupsSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false, mealPeriodGroup))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnRequestMealPeriodGroupsSuccessful(List<MealOrderPeriodGroupModel> groups)
		{
			try {
				var mealPeriodGroupText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealPeriodGroups");
				base.OnRequestCompleted (true);
				_mealPeriodGroups = groups;

				var groupsLabel = new List<KeyValuePair<string,string>> ();
				_mealPeriodGroups.ForEach (e => {
					groupsLabel.Add(new KeyValuePair<string,string>(e.code, e.label));
				});
				ShowTableViewController (groupsLabel, mealPeriodGroupText);
				TableViewController.DoneClicked += MealPeriodGroupViewController_DoneClicked;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void MealPeriodGroupViewController_DoneClicked (object sender, EventArgs e)
		{
			try {
				base.TableViewController_DoneClicked ();
				if (SelectedKeyValue.Value == null)
					return;
				var selected = SelectedKeyValue;
				_selectedMealOrderPeriodGroupCode = selected.Key;
				Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void WardTableViewController_DoneClicked (object sender, EventArgs e)
		{
			try {
				base.TableViewController_DoneClicked ();
				if (SelectedKeyValue.Value == null)
					return;
				_selectedWard = SelectedKeyValue;
				_selectedWardId = Convert.ToInt32(_selectedWard.Key);
				AppDelegate.Instance.WardId = _selectedWardId;
				AppDelegate.Instance.Ward = _selectedWard;
				Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnRequestWardsFailed()
		{
			try {
				base.OnRequestCompleted (false);
				this.ShowAlert (AppContext, "GetWardFailed");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowSetting ()
		{
			try {
				var hideDate = Settings.HideDateSettingOnPatientMeal && CurrentTab == emos_ios.Dashboard.OrderTabs.PatientMealOrder;
				AppContext.ControllerLoader.SettingLoader.Load (AppContext, hideDate, HandleOnSettingDone, null, OperationDateSelectionDone);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnSettingDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
			try {
				if (AppContext.WardViewHandler.CheckBulkOrder (e.Value.Key))
					AppContext.ControllerLoader.BulkOrderSummaryLoader.Load (AppContext, e.Value);
				else
					Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OperationDateSelectionDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
			try {
				HandleOnSettingDone (sender, e);
				SetTitle ();
				if (Settings.UseDateAsDashboardTitle)
					Title = AppDelegate.Instance.OperationDate.ToString (Formats.OperationDateFormat);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnDashboardChanged(Dashboard.OrderTabs tab, bool refresh = true)
		{
			try {
				CurrentTab = tab;
				ViewHandler.ClearSubviews (BackgroundView);
				switch (tab) {
				case Dashboard.OrderTabs.PatientMealOrder:
					AppDelegate.Instance.CurrentTab = Dashboard.OrderTabs.PatientMealOrder;
					_patientMealOrderView = new PatientMealOrderView (this) {
						Frame = new RectangleF (0, 0, 768, BackgroundView.Frame.Height)
					};
					BackgroundView.AddSubview (_patientMealOrderView);
					break;

				case Dashboard.OrderTabs.NurseDashboard:
					AppDelegate.Instance.CurrentTab = Dashboard.OrderTabs.NurseDashboard;
					break;

				case Dashboard.OrderTabs.CompanionMealOrder:
					AppDelegate.Instance.CurrentTab = Dashboard.OrderTabs.CompanionMealOrder;
					break;
				}
				if (refresh)
					Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private async void RequestPatientMealOrderModel()
		{
			try {
				var param = KnownUrls.GetPatientMealOrderModelQueryString (AppDelegate.Instance.WardId, AppDelegate.Instance.InstitutionId, AppDelegate.Instance.OperationDate, AppDelegate.Instance.WardGroupId, _selectedMealOrderPeriodGroupCode );
				await AppDelegate.Instance.HttpSender.Request<PatientMealOrderModel> ()
					.From (KnownUrls.GetPatientMealOrderModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnRequestPatientMealOrderSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private void OnRequestPatientMealOrderSuccessful(PatientMealOrderModel result)
		{
			try {
				OnRequestCompleted (true);
				if (result.mealPeriod != null)
					_patientMealOrderView.Load (result);
				else {
					var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Thelastcutofftimehasalreadypassed.");
					var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Pleaseselectanotherday.");
					this.ShowAlert (AppContext, alertMsg1 + " \n " + alertMsg2, translateMessage: false);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public void ClosePatientMenu ()
		{
			try {
				_patientMenuViewController.DismissViewController (true, null);
				UnlockRegistration ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void EditAdmission(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				AppContext.ControllerLoader.AdmissionLoader.LoadEditAdmission (AppContext, locationWithRegistrationSimple, HandleOnAdmissionSave);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void DischargePatient(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var dvc = new DischargePatientDVC (locationWithRegistrationSimple.registrations.First ().registration_id, this);
				AppContext.PushDVC (dvc);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void TransferPatient(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var dvc = new TransferPatientDVC (locationWithRegistrationSimple.registrations.First ().registration_id, this);
				AppContext.PushDVC (dvc);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void TherapeuticDietOrder(LocationWithRegistrationSimple locationWithRegistrationSimple, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed, bool extendLock = false)
		{
			try {
				ShouldRunViewWillAppear = false;
				if (extendLock)
					AppContext.LockHandler.ExtendRegistrationLock ();
				PopToViewControllerAndUnlock (false, false);
				var controller = new TherapeuticListViewController (this, locationWithRegistrationSimple, _interfaceStatus, clearFeed, fullFeed);
				NavigationController.PushViewController (controller, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void FoodAllergies(LocationWithRegistrationSimple locationWithRegistrationSimple, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed, bool extendLock = false)
		{
			try {
				ShouldRunViewWillAppear = false;
				if (extendLock)
					AppContext.LockHandler.ExtendRegistrationLock ();
				PopToViewControllerAndUnlock (false, false);
				var dvc = new FoodAllergiesDVC (this, locationWithRegistrationSimple, _interfaceStatus);
				dvc.ClearFeed = clearFeed;
				dvc.FullFeed = fullFeed;
				AppContext.PushDVC (dvc);
				dvc.Load ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void SnackPack(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var controller = new SnackPackListViewController (locationWithRegistrationSimple, _interfaceStatus) {
					PatientMenuCallback = this
				};
				NavigationController.PushViewController (controller, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void NillByMouth(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false)
		{
			try {
				if (Settings.CommonDietsVersion2) {
					var nbmTitle = AppContext.LanguageHandler.GetLocalizedString ("NilbyMouth");
					ShouldRunViewWillAppear = false;
					if (extendLock)
						AppContext.LockHandler.ExtendRegistrationLock ();
					PopToViewControllerAndUnlock (false, false);
					var controller = new CommonDietOrderListController (this, locationWithRegistrationSimple, _interfaceStatus, nbmTitle, "DIET41", CommonDietsType.NBM);
					NavigationController.PushViewController (controller, true);
				} else {
					var dvc = new NilByMouthDVC (this, locationWithRegistrationSimple.registrations.First ().registration_id,_interfaceStatus);
					AppContext.PushDVC (dvc);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void FullFeeds(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false)
		{
			try {
				if (Settings.CommonDietsVersion2) {
					var fullFeedTitle = AppContext.LanguageHandler.GetLocalizedString ("FullFeeds");
					ShouldRunViewWillAppear = false;
					if (extendLock)
						AppContext.LockHandler.ExtendRegistrationLock ();
					PopToViewControllerAndUnlock (false, false);
					var controller = new CommonDietOrderListController (this, locationWithRegistrationSimple, _interfaceStatus, fullFeedTitle, "DIET135", CommonDietsType.FullFeeds);
					NavigationController.PushViewController (controller, true);
				} else {
					var dvc = new FullFeedsDVC (this, locationWithRegistrationSimple.registrations.First ().registration_id, _interfaceStatus);
					AppContext.PushDVC (dvc);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ClearFeeds(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false)
		{
			try {
				if (Settings.CommonDietsVersion2) {
					var clearFeedTitle = AppContext.LanguageHandler.GetLocalizedString ("ClearFeeds");
					ShouldRunViewWillAppear = false;
					if (extendLock)
						AppContext.LockHandler.ExtendRegistrationLock ();
					PopToViewControllerAndUnlock (false, false);
					var controller = new CommonDietOrderListController (this, locationWithRegistrationSimple, _interfaceStatus, clearFeedTitle, "DIET136", CommonDietsType.ClearFeeds);
					NavigationController.PushViewController (controller, true);
				} else {
					var dvc = new ClearFeedsDVC (this, locationWithRegistrationSimple.registrations.First ().registration_id, _interfaceStatus);
					AppContext.PushDVC (dvc);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ConsumptionChecklist(LocationModelSimple location, RegistrationModelSimple registration)
		{
			try {
				var ioChartingViewController = new IoChartingViewController(location, registration);
				NavigationController.PushViewController(ioChartingViewController, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void MealType(LocationWithRegistrationSimple locationWithRegistrationSimple, bool extendLock = false)
		{
			try {
				ShouldRunViewWillAppear = false;
				if (extendLock)
					AppContext.LockHandler.ExtendRegistrationLock ();
				PopToViewControllerAndUnlock (false, false);
				var dvc = new MealTypeDVC (locationWithRegistrationSimple, this, _interfaceStatus);
				AppContext.PushDVC (dvc); 
				dvc.Load ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void SpecialInstruction(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				AppContext.ControllerLoader.SpecialInstructionLoader.LoadSpecialInstruction (AppContext, locationWithRegistrationSimple, this, _interfaceStatus);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PatientMealOrder(LocationModelSimple location, RegistrationModelSimple registration)
		{
			try {
				UnlockRegistration ();
				AppDelegate.Instance.SelectedLocation = location;
				AppDelegate.Instance.SelectedRegistration = registration;


				var _result = new PatientMealCalendarViewController (AppDelegate.Instance, AppDelegate.Instance.SelectedLocation, AppDelegate.Instance.SelectedRegistration, false, true);
				this.NavigationController.PushViewController (_result, false);

				//AppContext.LoadMenu (core_emos.Menu.EmosMenu.RestrictedPatientMealOrder, showMenu:false);
				this.Dispose ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ViewAdmission(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				AppContext.ControllerLoader.AdmissionLoader.LoadViewAdmission (AppContext, locationWithRegistrationSimple);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void TrialOrder(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var controller = new TrialOrderListViewController (locationWithRegistrationSimple, _interfaceStatus) {
					PatientMenuCallback = this
				};
				NavigationController.PushViewController(controller, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public void FluidRestriction(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var controller = new FluidRestrictionListViewController (locationWithRegistrationSimple, _interfaceStatus) {
					PatientMenuCallback = this
				};
				NavigationController.PushViewController (controller, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public void Feedback(LocationWithRegistrationSimple locationWithRegistrationSimple)
		{
			try {
				var dvc = new FeedbackDVC (locationWithRegistrationSimple, null, null, true, 1);
				AppContext.PushDVC (dvc);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PopToViewControllerAndUnlock(bool unlock = true, bool animated = true)
		{
			try {
				if (unlock)
					UnlockRegistration ();
				NavigationController.PopToViewController (NavigationController.ViewControllers[0], animated);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ShowWardSelectionAlert()
		{
			try {
				this.ShowAlert (AppContext, "Youdon'thaverighttoaccesswardselection");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public async void GetInterfaceStatus()
		{
			try {
				await AppDelegate.Instance.HttpSender.Request<InterfaceStatusModel> ()
					.From (KnownUrls.GetInterfaceStatus)
					.WhenSuccess (result => OnRequestInterfaceStatusSuccesful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnRequestInterfaceStatusSuccesful(InterfaceStatusModel result)
		{
			try {
				OnRequestCompleted (true);
				_interfaceStatus = result;

				if (_interfaceStatus.ADT == false || AppContext.CurrentUser.Authorized ("ACCESS_MANUAL_ADMISSION"))
					AppContext.ControllerLoader.AdmissionLoader.LoadEditAdmission (AppContext, _patientMenuLocation, HandleOnAdmissionSave);
				else
					this.ShowAlert (AppContext, "Youcan'tdomanualadmissionwhenSAPisup.");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnAdmissionSave (object sender, EventArgs e)
		{
			try {
				PopToViewControllerAndUnlock ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public async void GetInterfaceStatusForPatientMenu()
		{
			try {
				await AppDelegate.Instance.HttpSender.Request<InterfaceStatusModel> ()
					.From (KnownUrls.GetInterfaceStatus)
					.WhenSuccess (result => OnGetInterfaceSuccessPatientMenu(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnGetInterfaceSuccessPatientMenu(InterfaceStatusModel result)
		{
			try {
				OnRequestCompleted (true);
				_interfaceStatus = result;
				LoadPatientMenu (false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void CheckLock (LockRequestCode lockRequestCode)
		{
			try {
				AppContext.LockHandler.LockRegistration (this, _selectedRegistration.registration_id.Value, lockRequestCode, _requestHandler, View);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void LoadPatientMenu (bool ignoreLock)
		{
			try {
				if (!ignoreLock && Settings.CheckLock && !AppContext.LockHandler.LockResult.free_to_access) {
					CheckLock (LockRequestCode.PatientMenu);
					return;
				}

				_patientMenuViewController = new PatientMenuViewController (AppDelegate.Instance, this, _patientMenuLocation, _interfaceStatus);
				_patientMenuViewController.ModalPresentationStyle = Settings.GradientMenuView ? 
					UIModalPresentationStyle.OverCurrentContext : 
					UIModalPresentationStyle.FormSheet;
				_patientMenuViewController.ModalTransitionStyle = Settings.GradientMenuView ?
					UIModalTransitionStyle.CrossDissolve:
					UIModalTransitionStyle.CoverVertical;
				NavigationController.PresentViewController (_patientMenuViewController, true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OpenBirthdayCakeForm(LocationWithRegistrationSimple locationWithRegistration)
		{
			try {
				_locationForBirthday = locationWithRegistration;
				_requestHandler.SendRequest (View, ()=>RequestBirthdayCakeModel((long)_locationForBirthday.registrations.First().registration_id));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void CloseBirthdayCakeForm()
		{
			try {
				_birthdayCakeViewController.DismissViewController (true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public async void RequestCompanionDashboard()
		{
			try {
				if (AppDelegate.Instance.WardId == null || AppDelegate.Instance.WardId == 0) {
					_requestHandler.StopRequest ();
					return;
				}
				if (_nurseDashboardHeaderView != null)
					_nurseDashboardHeaderView.HidePatientDayOrderTableVisibility ();
				var queryString = KnownUrls.GetCompanionDashboardQueryString (AppDelegate.Instance.MealPeriodGroupCode, AppDelegate.Instance.InstitutionId, AppDelegate.Instance.WardId, AppDelegate.Instance.OperationDate, AppDelegate.Instance.HospitalId);
				await AppDelegate.Instance.HttpSender.Request<NurseDashboardModel> ()
					.From (KnownUrls.GetCompanionDashboard)
					.WithQueryString (queryString)
					.WhenSuccess (result => OnRequestCompanionDashboardSuccessful (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public void OnRequestCompanionDashboardSuccessful(NurseDashboardModel result)
		{
			try {
				OnRequestCompleted (true);
				AppDelegate.Instance.WardGroupId = result.wardGroupId;
				_nurseDashboardHeaderView = new NurseDashboardHeaderView (this, Dashboard.DashboardMenu.CompanionMealOrder) {
					Frame = new RectangleF (0, 0, 768, BackgroundView.Frame.Height)
				};
				BackgroundView.AddSubview(_nurseDashboardHeaderView);
				_nurseDashboardHeaderView.Load (result);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");

			}
		}

		private Dashboard.DashboardMenu GetTabStatus(emos_ios.Dashboard.OrderTabs tabStatus)
		{
			var state = Dashboard.DashboardMenu.NurseDashboard;
			try {
				switch (tabStatus) 
				{
				case emos_ios.Dashboard.OrderTabs.NurseDashboard:
					state = Dashboard.DashboardMenu.NurseDashboard;
					break;
				case emos_ios.Dashboard.OrderTabs.PatientMealOrder:
					state = Dashboard.DashboardMenu.PatientMealOrder;
					break;

				case emos_ios.Dashboard.OrderTabs.CompanionMealOrder:
					state = Dashboard.DashboardMenu.CompanionMealOrder;
					break;
				}
				return state;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return state;
			}
		}

		private async void RequestBirthdayCakeModel(long registrationId)
		{
			try {
				var queryString = KnownUrls.GetBirthdayCakeModelQueryString (registrationId, (long)AppDelegate.Instance.InstitutionId);
				await AppDelegate.Instance.HttpSender.Request<BirthdayCakeModelIpad> ()
					.From (KnownUrls.GetBirthdayCakeModel)
					.WithQueryString(queryString)
					.WhenSuccess (result => OnGetBirthdayCakeSuccesful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private void OnGetBirthdayCakeSuccesful(BirthdayCakeModelIpad result)
		{
			try {
				OnRequestCompleted (true);
				_birthdayCakeViewController = new BirthdayCakeViewController (_locationForBirthday, result, this);

				_birthdayCakeViewController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				_birthdayCakeViewController.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
				NavigationController.PresentViewController (_birthdayCakeViewController, true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PatientMenuLockSuccessful (LockResultIpad result)
		{
			try {
				LoadPatientMenu (false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PatientMenuLockFailed (LockResultIpad result)
		{
			try {
				LoadPatientMenu (true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void MealOrderLockSuccessful (LockResultIpad result)
		{
			try {
				LoadMealOrder ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void MealOrderLockFailed (LockResultIpad result)
		{
			try {
				var message = String.Format ("Patient {0} has been locked by [{1}] on {2}.", _selectedRegistration.profile_name, result.locked_by, result.lock_start_at_verbose);
				this.ShowAlert (AppContext, message, null, false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnLockRequestCompleted(bool success, string failedTitle = "", bool showFailedTitle = true)
		{
			try {
				base.OnRequestCompleted (success, failedTitle, showFailedTitle);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void UnlockSuccessful ()
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void UnlockFailed ()
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnUnlockRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnLockNotExtended ()
		{
			try {
				AppContext.LoadMenu (Menu.EmosMenu.NurseDashboard);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnUnlockLocksByUserRequestCompleted(bool success)
		{
			try {
				base.OnRequestCompleted (success);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void SetOrderBy (string orderBy)
		{
			try {
				_orderBy = orderBy;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void BackToCompanion() {
			try {
				AppContext.LoadMenu (Menu.EmosMenu.CompanionMealOrder);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void PatientMealCalendar(LocationModelSimple location, RegistrationModelSimple registration)
		{
			try {
				AppContext.SelectedLocation = location;
				AppContext.SelectedRegistration = registration;

				AppContext.LoadMenu (Menu.EmosMenu.RestrictedPatientMealOrder, isRoot:false, showMenu:true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void WebPortalRegistration(LocationModelSimple location, RegistrationModelSimple registration)
		{
			try {
				AppContext.SelectedLocation = location;
				AppContext.SelectedRegistration = registration;
				var vc = new WebPortalRegistrationViewController (location, registration);
				AppContext.Nav.PushViewController (vc, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		public void CompanionMealCalendar(LocationModelSimple location, RegistrationModelSimple registration)
		{
			try {
				AppContext.SelectedLocation = location;
				AppContext.SelectedRegistration = registration;
				AppContext.LoadMenu (Menu.EmosMenu.RestrictedCompanionMealOrder, isRoot:false, showMenu:true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
	}
}

