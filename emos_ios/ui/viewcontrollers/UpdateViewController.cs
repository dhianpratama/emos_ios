﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ios;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class UpdateViewController : BaseViewController
	{
		public const string LoginImageUpdateName = "emos_login.png";
		private const string VersionKey = "Version";
		public event EventHandler OnUpdateComplete;
		public event EventHandler OnNoUpdate;
		public event EventHandler OnColorUpdated;
		public event EventHandler NoThemeFromServer;
		private FileDownloader UpdateChecker { get; set; }
		private List<PatchInfo> _patchInfos = new List<PatchInfo>();
		private int _downloadCount;

		public UpdateViewController (IApplicationContext appContext) : base (appContext, "UpdateViewController", null)
		{
			HiddenLogo = true;
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ShowTitle ();
//			ProgressView.SetProgress (0.0f, false);
		}
		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 400, 150);
		}
		public void Start ()
		{
			if (!Settings.EnableTheme)
				return;
//			SendRequest (View, RequestCheckUpdate, AppContext.LanguageHandler.GetLocalizedString ("CheckingUpdates"));
			RequestCheckUpdate ();
		}
		private async void RequestCheckUpdate()
		{
			var institutionId = AppDelegate.Instance.InstitutionId;
			var param = KnownUrls.GetThemeQueryString (institutionId);

			await AppDelegate.Instance.HttpSender.Request<EmosThemeUpdate> ()
				.From (KnownUrls.CheckUpdate)
				.WithQueryString(param)
				.WhenSuccess (result => CheckUpdateSuccessful (result))
				.WhenFail (result => CheckUpdateFailed())
				.GetAuthenticationCookie ()
				.Go ();
		}
		private void CheckUpdateFailed ()
		{
			DismissViewController (false, null);
			OnRequestCompleted (false);
		}
		private void CheckUpdateSuccessful (EmosThemeUpdate result)
		{
			OnRequestCompleted (true);
			if (result != null) {
				if (CheckIsUpdated (result)) {
					OnNoUpdate.SafeInvoke (this);
					return;
				}
				AppContext.ColorHandler.UpdateTheme (result.MainThemeColor, result.ButtonBackgroundColor, result.MealOrderStepButtonBackgroundColor);
				OnColorUpdated.SafeInvoke (this);
				DownloadImages (result);
			} else {
				AppContext.ColorHandler.UpdateTheme (0, 0, 0);
				NoThemeFromServer.SafeInvoke (this);
			}
		}
		bool CheckIsUpdated (EmosThemeUpdate result)
		{
			var version = NSUserDefaults.StandardUserDefaults.StringForKey (VersionKey);
			if (String.IsNullOrEmpty (version) || version != result.Version) {
				NSUserDefaults.StandardUserDefaults.SetString (result.Version, VersionKey);
				return false;
			}
			return true;
		}
		private void DownloadImages (EmosThemeUpdate result)
		{
			TitleText.Text = AppContext.LanguageHandler.GetLocalizedString ("DownloadingUpdates");
			ProgressLabel.Hidden = false;
			ProgressView.Hidden = false;
			_patchInfos.Add (new PatchInfo {
				Filename = LoginImageUpdateName,
				Url = AppDelegate.Instance.ServerConfig.BaseUrl + result.LoginImageUrl
			});
			_patchInfos.Add (new PatchInfo {
				Filename = "logo.png",
				Url = AppDelegate.Instance.ServerConfig.BaseUrl + result.LogoImageUrl
			});
			UpdateChecker = new FileDownloader ();
			UpdateChecker.OnDownloadCompleted += HandleOnDownloadCompleted;
			DownloadNext ();
		}
		private void ShowTitle ()
		{
			TitleText.Font = Fonts.Header;
			TitleText.Text = AppContext.LanguageHandler.GetLocalizedString ("CheckingUpdates");
			TitleText.BackgroundColor = Colors.HeaderBackground;
		}
		private void UpdateProgress ()
		{
			BeginInvokeOnMainThread (() => {
				ProgressLabel.Text = String.Format ("{0} of {1}", _downloadCount, _patchInfos.Count);
				ProgressView.SetProgress (_downloadCount - 1, true);
			});
		}
		private void HandleOnDownloadCompleted (object sender, EventArgs e)
		{
			DownloadNext ();
		}
		private void DownloadNext ()
		{
			if (_patchInfos.Count == 0)
				return;
			_downloadCount++;
			if (_downloadCount > _patchInfos.Count) {
				OnUpdateComplete.SafeInvoke (this);
				return;
			}
			UpdateProgress ();
			UpdateChecker.Download (_patchInfos [_downloadCount - 1]);
		}
	}
}

