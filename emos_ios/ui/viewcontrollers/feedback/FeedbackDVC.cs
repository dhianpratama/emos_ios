﻿using System;
using System.Linq;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.tools;
using VMS_IRIS.BusinessLogic.Feedback;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class FeedbackDVC : BaseDVC, ICallback<int>
	{
		public bool IsFirstPage { get; set; }
		private FeedbackModel _feedbackModel { get; set; }
		private QuestionModel _questionModel { get; set; }
		private LocationWithRegistrationSimple _location;
		private int _order;

		private CheckboxGroupDVC<QuestionAnswerModel> _multiAnswers;
		private RootElement _singleAnswer;
		private MultilineEntryElement _freetextAnswer;
		private RegistrationModelWithMealPeriodsAndOperationCode _registration;

		private List<AnswerModel> _answers;

		public FeedbackDVC (LocationWithRegistrationSimple location, FeedbackModel feedbackModel, List<AnswerModel> answers, bool isFirstPage=false, int order=1) : base (AppDelegate.Instance)
		{
			_location = location;
			_registration = _location.registrations.First ();
			IsFirstPage = isFirstPage;

			if (IsFirstPage) {
				_order = 1;
				_requestHandler.SendRequest (View, GetFeedbackModel);
			}
			else {
				_order = order;
				_feedbackModel = feedbackModel;
				_answers = answers;
				GetCurrentQuestion ();
				CreateRightButton ();
				CreateRoot (AppContext.LanguageHandler.GetLocalizedString("MealFeedback"));
			}
		}

		private void CreateRightButton()
		{
			var saveButton = new UIBarButtonItem("Save", UIBarButtonItemStyle.Plain, delegate {
				SaveButtonClicked();
			});

			var nextButton = new UIBarButtonItem("Next", UIBarButtonItemStyle.Plain, delegate {
				ProceedToNextQuestion();
			});
			saveButton.TintColor = UIColor.White;
			nextButton.TintColor = UIColor.White;

			if (_order == _feedbackModel.questionCollection.Count)
				NavigationItem.SetRightBarButtonItem (saveButton, true);
			else
				NavigationItem.SetRightBarButtonItem (nextButton, true);
		}

		private void ProceedToNextQuestion()
		{
			if (Valid ()) {

				EmbedAnswerToModel ();

				var dvc = new FeedbackDVC (_location, _feedbackModel, _answers, false, _order + 1);
				AppContext.PushDVC (dvc);
			} else {
				this.ShowAlert (AppContext, "Pleasefilltheanswer");
			}
		}

		private void GetCurrentQuestion()
		{
			_questionModel = _feedbackModel.questionCollection [_order - 1];
		}

		private async void GetFeedbackModel()
		{
			await AppDelegate.Instance.HttpSender.Request<FeedbackModel> ()
				.From (KnownUrls.GetEmosFeedback)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(FeedbackModel result)
		{
			OnRequestCompleted (true);
			if (result != null) {
				_feedbackModel = result;
				_answers = new List<AnswerModel> ();

				bool valid = false;
				if (_feedbackModel != null) {
					if (_feedbackModel.questionCollection != null) {
						if (_feedbackModel.questionCollection.Count > 0) {
							_questionModel = _feedbackModel.questionCollection.First ();
							valid = true;
						}
					}
				}

				CreateRightButton ();
				if (valid) {
					CreateRoot (AppContext.LanguageHandler.GetLocalizedString ("MealFeedback"));
				}
			} else {
				this.ShowAlert (AppContext, "Nofeedbackavailable");
			}
		}

		public override void CreateRoot(string title = "")
		{
			if (!string.IsNullOrEmpty (title))
				base.CreateRoot (title);
			switch (_questionModel.questionType.label) {
			case "Freetext":
				CreateFreetextQuestion ();
				break;
			case "Single Choice":
				CreateSingleChoiceQuestion ();
				break;
			case "Multi Choice":
				CreateMultiChoicesQuestion ();
				break;
			}
		}

		private StyledStringElement CreateQuestionGroupTitle(string title)
		{
			var element = new StyledStringElement (title);
			element.TextColor = UIColor.Black;
			return element;
		}

		private void CreateFreetextQuestion()
		{
			_freetextAnswer = new MultilineEntryElement ("Answer : ", "", "", 0, this, false, false, 90f);
			Root.Add (new Section ("Question Group") {
				CreateQuestionGroupTitle (_questionModel.questionGroup != null ? _questionModel.questionGroup.label : "-")
			});
			Root.Add (new Section ("Question") {
				new StyledStringElement (_questionModel.freetext)
			});
			Root.Add (new Section ("Answer") {
				_freetextAnswer
			});
		}

		private void CreateSingleChoiceQuestion()
		{
			var singleAnsSection = new Section ();
			_questionModel.answers.ForEach (r => singleAnsSection.Add (new MyRadioElement (r.freetext)));

			_singleAnswer = new RootElement ("Choose your answer", new RadioGroup (0)) {
				singleAnsSection
			};
			Root.Add (new Section ("Question Group") {
				CreateQuestionGroupTitle (_questionModel.questionGroup != null ? _questionModel.questionGroup.label : "-")
			});
			Root.Add (new Section ("Question") {
				new StyledStringElement (_questionModel.freetext)
			});
			Root.Add (new Section ("Answer") {
				_singleAnswer
			});
		}

		private void CreateMultiChoicesQuestion()
		{
			_multiAnswers = new CheckboxGroupDVC<QuestionAnswerModel>("Answer", new List<QuestionAnswerModel>(), _questionModel.answers);
		
			Root.Add (new Section ("Question Group") {
				CreateQuestionGroupTitle (_questionModel.questionGroup != null ? _questionModel.questionGroup.label : "-")
			});
			Root.Add (new Section ("Question") {
				new StyledStringElement (_questionModel.freetext)
			});
			Root.Add (_multiAnswers.Create ());
		}

		public void ItemSelected(int index, int idx)
		{

		}

		private void EmbedAnswerToModel()
		{
			switch (_questionModel.questionType.label) {
			case "Freetext":
				if (!_answers.Any (e => e.question_id == _questionModel.question_id)) {
					_answers.Add (new AnswerModel () {
						question_set_id = (long)_questionModel.question_set_id,
						question_id = _questionModel.question_id,
						question_type_label = _questionModel.questionType.label,
						freetext = _questionModel.freetext,
						answer = _freetextAnswer.Value
					});
				} else {
					_answers.ForEach (a => {
						if(a.question_id==_questionModel.question_id)
							a.answer = _freetextAnswer.Value;
					});
				}
				break;
			case "Single Choice":
				if (!_answers.Any (e => e.question_id == _questionModel.question_id)) {
					_answers.Add (new AnswerModel () {
						question_set_id = (long)_questionModel.question_set_id,
						question_id = _questionModel.question_id,
						question_type_label = _questionModel.questionType.label,
						freetext = _questionModel.freetext,
						answer = _questionModel.answers [_singleAnswer.RadioSelected].freetext
					});
				} else {
					_answers.ForEach (a => {
						if(a.question_id==_questionModel.question_id)
							a.answer = _questionModel.answers [_singleAnswer.RadioSelected].freetext;
					});
				}
				break;
			case "Multi Choice":
				_answers.RemoveAll (e => e.question_id == _questionModel.question_id);
				_multiAnswers.Result
					.ForEach (r => {
						_answers.Add(new AnswerModel()
							{
								question_set_id = (long)_questionModel.question_set_id,
								question_id = _questionModel.question_id,
								question_type_label = _questionModel.questionType.label,
								freetext = _questionModel.freetext,
								answer = r.freetext
							});
				});
				break;
			}
		}

		private void SaveButtonClicked()
		{
			EmbedAnswerToModel ();

			var saveModel = new SaveFeedbackModel () {
				profile_id = _location.registrations.First().profile_id,
				registration_id = _registration.registration_id,
				answers = _answers
			};

			_requestHandler.SendRequest (View, () => SaveAnswers (saveModel));
		}

		private async void SaveAnswers(SaveFeedbackModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveAnswerFromUser)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			PopToNurseDashboard ();
			this.ShowAlert (AppContext, "Feedbacksaved");
		}

		private bool Valid()
		{
			bool valid = true;

			switch (_questionModel.questionType.label) {
			case "Freetext":
				if (String.IsNullOrEmpty (_freetextAnswer.Value))
					valid = false;
				break;
			case "Single Choice":
				valid = true;
				break;
			case "Multi Choice":
				if (_multiAnswers.Result.Count == 0)
					valid = false;
				break;
			}

			return valid;
		}

		public void PopToNurseDashboard()
		{
			NavigationController.PopToRootViewController (true);
		}
	}
}

