﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Globalization;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;

namespace emos_ios
{
	public class NilByMouthDVC : BaseDVC
	{
		private FormattedDateTimeElement _startDate, _endDate;
		private CommonDietOrderModel _model;
		private long? _registrationId;
		private IPatientMenu _callback;
		private InterfaceStatusModel _interfaceStatus;
		private UIBarButtonItem _deleteButton = new UIBarButtonItem (UIBarButtonSystemItem.Trash);

		public NilByMouthDVC (IPatientMenu controller, long? registrationId, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance)
		{
			this._interfaceStatus = interfaceStatus;
			this._registrationId = registrationId;
			_callback = controller;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetNilByMouthModel);
		}

		private void CreateRightButtons()
		{
			if (!_callback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var saveButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			saveButton.TintColor = UIColor.White;
			var rightButtons = new UIBarButtonItem[] { saveButton };
			if (!_interfaceStatus.DIET_ORDERS)
				NavigationItem.SetRightBarButtonItems (rightButtons, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var nilByMounthConfigurationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nilbymouthconfiguration");
			_startDate = new FormattedDateTimeElement (startDateText, _model.start_date ?? DateTime.Now.ToDisplayDatetime ());
			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			_endDate = new FormattedDateTimeElement (endDateText, _model.end_date ?? DateTime.Now.AddDays (Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ());
			Root.Add (new Section (nilByMounthConfigurationText) {
				_startDate,
				_endDate,
			});
		}
		private string ConvertDate (DateTime? dateValue)
		{
			CultureInfo languageCode = new CultureInfo (AppDelegate.Instance.LanguageHandler.LanguageCode);
			return dateValue == null ? "Not specified" : dateValue.Value.ToString ("dd/MM/yyyy hh:mm", languageCode);
		}
		private void OnSave()
		{
			_model.start_date = _startDate.DateValue.ToDisplayDatetime ();
			_model.end_date = _endDate.DateValue.ToDisplayDatetime ();
			//_model.meal_order_period_id = _model.allMealPeriods [_mealPeriodOptions.RadioSelected].id;
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetNilByMouthModel()
		{
			var content = KnownUrls.GetCommonDietOrderQueryString ("DIET41", AppDelegate.Instance.InstitutionId, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.GetCommonDietOrderModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnRequestModelCompleted(CommonDietOrderModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.NilByMouthTitle));
			CreateRightButtons ();
			AddDeleteButton (result);
		}
		private void AddDeleteButton (CommonDietOrderModel result)
		{
			if (_interfaceStatus.DIET_ORDERS || result.profile_diet_order_id == null || !_callback.LockResult.free_to_access)
				return;

			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);
			if (!rightButtons.Contains (_deleteButton)) {
				_deleteButton.Clicked+= (sender, e) => {
					View.ShowConfirmAlert(AppContext, "NilbyMouth", "Areyousurewanttodeactivatethis?", OnDeleteConfirmed);
				};
				rightButtons.Add (_deleteButton);
			}
			NavigationItem.SetRightBarButtonItems (rightButtons.ToArray (), false);
		}
		private void OnDeleteConfirmed ()
		{
			_requestHandler.SendRequest (View, () => DeleteModel ());
		}
		private async void Save(CommonDietOrderModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.SaveCommonDietOrder)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			GetData ();
		}
		private async void DeleteModel()
		{
			long profileDietOrderId = Convert.ToInt32 (_model.profile_diet_order_id);
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (result.success);
			if (!result.success)
				return;
			var message = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nbmhasbeendeactivated"));
			this.ShowAlert (AppContext, message);
			_callback.PopToViewControllerAndUnlock ();
		}
	}
}

