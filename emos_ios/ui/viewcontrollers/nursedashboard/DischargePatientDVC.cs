﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.models;
using emos_ios.tools;

using VMS_IRIS.Areas.EmosIpad.Models;


namespace emos_ios
{
	public class DischargePatientDVC : BaseDVC
	{
		private long? registration_id;
		private EntryElement _patientName, _patientIdentifier, _admissionTime, _caseNumber;
		private EditAdmissionModel editAdmissionModel;
		private IPatientMenu _callback;

		public DischargePatientDVC (long? registration_id, IPatientMenu callback) : base (AppDelegate.Instance)
		{
			this.registration_id = registration_id;
			this._callback = callback;
			GetData ();
		}

		private void ShowDischargeButton ()
		{
			var dischargeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Discharge");
			var rightButton = new UIBarButtonItem (dischargeText, UIBarButtonItemStyle.Plain, delegate {
				_requestHandler.SendRequest (View, DischargePatient);
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, RequestAdmissionModel );
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var patientNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientName");
			var identifierText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Identifier");
			var caseNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CaseNumber");
			var AdmissionTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionTime");
			var patientDataText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientData");

			_patientName = new EntryElementDVC (patientNameText, "", editAdmissionModel.admissionModel.profile_name, true);
			_patientIdentifier = new EntryElementDVC (identifierText, "", editAdmissionModel.admissionModel.identifier_number, true);
			_caseNumber = new EntryElementDVC (caseNumberText, "", editAdmissionModel.admissionModel.case_number, true);
			_admissionTime = new EntryElementDVC (AdmissionTimeText, "", editAdmissionModel.admissionModel.admission_time.ToDisplayDateString (), true);

			Root.Add (new Section (patientDataText) {
				_patientName,
				_patientIdentifier,
				_caseNumber,
				_admissionTime
			});
		}

		private async void RequestAdmissionModel()
		{
			//MBHUDView.HudWithBody ("Please Wait...", MBAlertViewHUDType.ActivityIndicator, 10.0f, true);
			var editAdmissionContent = KnownUrls.GetAdmissionQueryString (this.registration_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.GetEditAdmissionModel)
				.WithQueryString (editAdmissionContent)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private async void DischargePatient()
		{
			var queryString = KnownUrls.DischargeAdmissionQueryString (this.registration_id);
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.DischargeAdmission)
				.WithQueryString (queryString)
				.WhenSuccess (result => onDischargeCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(EditAdmissionModel result)
		{
			OnRequestCompleted (true);
			editAdmissionModel = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("DischargeAdmission"));
			ShowDischargeButton ();
		}

		private void onDischargeCompleted(EditAdmissionModel result)
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "DischargeSuccessfully");
			_callback.PopToViewControllerAndUnlock ();
		}
	}
}

