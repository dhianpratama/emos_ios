// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("TherapeuticListViewController")]
	partial class TherapeuticListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TherapeuticDietErrorMsgLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TherapeuticDietTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView TherapeuticListTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}

			if (TherapeuticDietErrorMsgLabel != null) {
				TherapeuticDietErrorMsgLabel.Dispose ();
				TherapeuticDietErrorMsgLabel = null;
			}

			if (TherapeuticDietTitleLabel != null) {
				TherapeuticDietTitleLabel.Dispose ();
				TherapeuticDietTitleLabel = null;
			}

			if (TherapeuticListTable != null) {
				TherapeuticListTable.Dispose ();
				TherapeuticListTable = null;
			}
		}
	}
}
