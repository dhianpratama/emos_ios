﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class SnackPackListViewController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> SnackPackListDataSource;

		private InfoDataListHandler _snackPackListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private RegistrationModelSimple _registration;
		private InterfaceStatusModel _interfaceStatus;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }

		public SnackPackListViewController (LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance, "SnackPackListViewController", null)
		{
//			_locationWithRegistrations = locationWithRegistration;
			_registration = locationWithRegistration.registrations.First ();
			_interfaceStatus = interfaceStatus;
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SnackPack");
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			SnackPackTitleLabel.SetFrame (y: Measurements.TopY + 20);
			if (!Settings.ShowDVCTitle)
				SnackPackTitleLabel.SetFrame (height: 0);
			SnackPackTable.SetFrameBelowTo (SnackPackTitleLabel);
			_requestHandler.SendRequest (View, RequestSnackPackViewModel);
		}

		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			var addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				OnEdit(0);
			});
			addButton.TintColor = UIColor.White;

			if (_items == null) {
				if (!_interfaceStatus.DIET_ORDERS)
					NavigationItem.SetRightBarButtonItem (addButton, false);
			} else {
				if (_items.Count == 0) {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (addButton, false);
				} else {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (null, false);
				}
			}
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_snackPackListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			SnackPackTable.Hidden = false;
			SnackPackListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_snackPackListViewCellHandler);
			SnackPackListDataSource.Items = _items;
			SnackPackTable.Source = SnackPackListDataSource;
			SnackPackTable.ReloadData ();
		}

		public void SetLayout()
		{
			SnackPackTable.ScrollEnabled = true;
			SnackPackTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			var dvc = new SnackPackDVC (this, _registration.registration_id);
			AppContext.PushDVC (dvc);
		}

		public void OnDelete(long id)
		{
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Snackpack");
			var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatethissnackpack?");
			this.ShowConfirmAlert (AppContext, alertMsg1, alertMsg2, OnDeleteConfirmed);
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestSnackPackViewModel()
		{
			var queryString = KnownUrls.RegistrationIdQueryString ((long)_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>>()
				.From (KnownUrls.GetSnackPackViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestSnackPckModelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestSnackPckModelSuccesful(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			_items = result;
			GenerateRightButton ();
			if (_items.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private async void DeleteSnackPack()
		{
			long profileDietOrderId = Convert.ToInt32( _items.First ().First (e => e.Key == "Id").Value );
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var alertMsg = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Snackpack'{0}'hasbeendeactivated"),
					                  _items.First ().First (e => e.Key == "Identifier").Value);
				this.ShowAlert (AppContext, alertMsg, translateMessage: false);
				_requestHandler.SendRequest (View, RequestSnackPackViewModel);
			} else
				OnRequestCompleted (false);
		}

		private void OnDeleteConfirmed()
		{
			_requestHandler.SendRequest (View, DeleteSnackPack);
		}

		private void ShowTable()
		{
			SnackPackTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			SnackPackTable.Hidden = true;
			NotFoundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			SnackPackTable.Hidden = true;
			NotFoundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestSnackPackViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			SnackPackTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SnackPack");
			SnackPackErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nosnackpackisfound");
		}
	}
}

