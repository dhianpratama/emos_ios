﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.models;
using emos_ios.tools;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class TransferPatientDVC : BaseDVC
	{
		private long? registration_id;
		private EntryElement _patientName, _patientIdentifier, _admissionTime, _caseNumber;
		private EditAdmissionModel editAdmissionModel;
		private IPatientMenu _callback;

		private List<LocationModelSimple> _wards;

		public TransferPatientDVC (long? registration_id, IPatientMenu callback) : base (AppDelegate.Instance)
		{
			this.registration_id = registration_id;
			_callback = callback;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, RequestAdmissionModel);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var patientNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientName");
			var identifierText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Identifier");
			var caseNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CaseNumber");
			var admissionTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionTime");
			var patientDataText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientData");
			var currentLocationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CurrentLocation");
			var transferToText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Transferto");
			var selectWardText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Selectward");

			_patientName = new EntryElementDVC (patientNameText, "", editAdmissionModel.admissionModel.profile_name, true);
			_patientIdentifier = new EntryElementDVC (identifierText, "", editAdmissionModel.admissionModel.identifier_number, true);
			_caseNumber = new EntryElementDVC (caseNumberText, "", editAdmissionModel.admissionModel.case_number, true);
			_admissionTime = new EntryElementDVC (admissionTimeText, "", editAdmissionModel.admissionModel.admission_time.ToDisplayDateString (), true);

			Root.Add (new Section (patientDataText) {
				_patientName,
				_patientIdentifier,
				_caseNumber,
				_admissionTime
			});
			Root.Add (new Section (currentLocationText) {
				new EntryElementDVC (editAdmissionModel.admissionModel.location_label, "", "", true)
			});
			Root.Add (new Section (transferToText) {
				new RootElement (selectWardText, (RootElement e) => {
					return CreateWardsMenu (e);
				})
			});
		}

		public DialogViewController CreateWardsMenu(RootElement e)
		{
			var selectWardText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Selectward");
			var wardsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Wards");
			var backText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Back");

			DialogViewController dvc = new DialogViewController (e);

			var wardSection = new Section (wardsText);
			_wards.ForEach (r => wardSection.Add (new RootElement (r.label, (RootElement elm) => {
				return new BedsMenuSelection (elm, r.id, editAdmissionModel.admissionModel, _callback);
			})));

			dvc.Root = new RootElement (selectWardText) {
				wardSection
			};
			var leftButton = new UIBarButtonItem(backText, UIBarButtonItemStyle.Done, delegate {
				dvc.NavigationController.PopViewControllerAnimated(true);
			});
			leftButton.TintColor = UIColor.White;
			dvc.NavigationItem.SetLeftBarButtonItem (leftButton, true);
			return dvc;
		}

		private async void RequestAdmissionModel()
		{
			//MBHUDView.HudWithBody ("Please Wait...", MBAlertViewHUDType.ActivityIndicator, 10.0f, true);
			var editAdmissionContent = KnownUrls.GetAdmissionQueryString (this.registration_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.GetEditAdmissionModel)
				.WithQueryString (editAdmissionContent)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(EditAdmissionModel result)
		{
			OnRequestCompleted (true);
			editAdmissionModel = result;
			OnRequestWards ();
		}

		private async void OnRequestWards()
		{
			var hospitalId = KnownUrls.GetWardsQueryString ((long)AppDelegate.Instance.HospitalId);
			await AppDelegate.Instance.HttpSender.Request<List<LocationModelSimple>> ()
				.From (KnownUrls.GetWards)
				.WithQueryString (hospitalId)
				.WhenSuccess (result => OnRequestWardCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false,"Ward",true))
				.Go ();
		}

		private void OnRequestWardCompleted(List<LocationModelSimple> listWards)
		{
			OnRequestCompleted (true);
			_wards = listWards;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TransferPatient"));
		}
	}

	#region beds class
	public class BedsMenuSelection : BaseDVC
	{
		private List<LocationModelSimple> _beds;
		private AdmissionModel _admissionModel;
		private IPatientMenu _callback;

		public LocationModelSimple BedSelected;

		public BedsMenuSelection(RootElement e, long? wardId, AdmissionModel admissionModel, IPatientMenu callback) : base (AppDelegate.Instance)
		{
			_admissionModel = admissionModel;
			_callback = callback;
			_requestHandler.SendRequest (View, () => OnRequestBeds (wardId));
		}

		private async void OnRequestBeds(long? wardId)
		{
			var param = KnownUrls.GetBedsByWardQueryString (wardId, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<List<LocationModelSimple>> ()
				.From (KnownUrls.GetBedsByWard)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestBedsCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnRequestBedsCompleted(List<LocationModelSimple> listBeds)
		{
			OnRequestCompleted (true);
			_beds = listBeds;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TransferPatient"));
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var availableBedsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Availablebeds");
			var bedSection = new Section (availableBedsText);
			_beds.Where (e => e.occupied == false).ToList ().ForEach(r => 
				bedSection.Add(new StyledStringElement (r.label, delegate {
					BedSelected = r;
					_requestHandler.SendRequest (View, () => OnTransferRequest ());
				})));
			Root.Add (bedSection);
		}

		private async void OnTransferRequest()
		{
			var param = KnownUrls.TransferAdmissionQueryString (_admissionModel.registration_id, _admissionModel.location_id, BedSelected.id);
			await AppDelegate.Instance.HttpSender.Request<LocationModelSimple> ()
				.From (KnownUrls.TransferAdmission)
				.WithQueryString (param)
				.WhenSuccess (result => OnTransferSuccesful())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnTransferSuccesful()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			_callback.PopToViewControllerAndUnlock ();
		}
	}
	#endregion
}

