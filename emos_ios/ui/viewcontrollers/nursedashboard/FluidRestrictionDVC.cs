﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using ios;

namespace emos_ios
{
	public class FluidRestrictionDVC : BaseDVC, ICallback<int>
	{
		public List<RestrictionTypeWithRestriction> RestrictionTypes;

		private List<CheckboxGroupDVC<RestrictionModelSimple>> _restrictionTypeGroups;
		private List<Section> _restrictionCheckboxes;
		private FormattedDateTimeElement _startDate, _endDate;
		private TherapeuticDietOrderModel _model;
		private long? _registrationId;
		private ICommonDataListCallback _callback;

		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;

		private Section _dietCommentSection;

		public FluidRestrictionDVC (ICommonDataListCallback controller, long? registrationId) : base (AppDelegate.Instance)
		{
			this._registrationId = registrationId;
			_callback = controller;

			_dietComments = new List<MultilineEntryElement> ();
			GetData ();
			CreateSaveButton ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetTherapeuticModel);
		}

		private void CreateSaveButton()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");
			var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("DietComments");
			var patientInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Patientinfo");
			var nameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Name");
			var IDText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ID");
			var periodText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Period");

			_restrictionTypeGroups = new List<CheckboxGroupDVC<RestrictionModelSimple>> ();
			_restrictionCheckboxes = new List<Section> ();
			_model.allRestrictionTypes.Where(e=> e.restrictions.Count > 0)
				.ToList()
				.ForEach (rt => {

					var checkboxGroup = new CheckboxGroupDVC<RestrictionModelSimple>(rt.label, _model.currentRestrictions, rt.restrictions);
					_restrictionTypeGroups.Add(checkboxGroup);
					_restrictionCheckboxes.Add(checkboxGroup.Create());
				});
			_startDate = new FormattedDateTimeElement (startDateText, (DateTime)( _model.start_date ?? DateTime.Now));
			_endDate = new FormattedDateTimeElement (endDateText, (DateTime) (_model.end_date??DateTime.Now.AddDays(7)));

			_startDate.DateSelected+= delegate {
				DateSelected();
			};
			_endDate.DateSelected += delegate {
				DateSelected ();
			};
			_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
				AddNewDietComment();
			}){
				BackgroundColor = UIColor.LightGray,
				TextColor = UIColor.DarkGray
			};

			_dietCommentSection = new Section (dietCommentsText) {
				_addCommentButton
			};
			_dietComments.ForEach (r => _dietCommentSection.Add (r));

			Root.Add (new Section (patientInfoText) {
				new EntryElementDVC (nameText, "", _model.registration.profile_name, true),
				new EntryElementDVC (IDText, "", _model.registration.profile_identifier, true)
			});
			Root.Add (new Section (periodText) {
				_startDate,
				_endDate
			});
			Root.Add (_dietCommentSection);
			Root.Add (_restrictionCheckboxes);
		}

		private void AddNewDietComment()
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (commentText + ": " , "", "",_dietComments.Count,this));
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction"));
		}

		private void OnSave()
		{
			_model.comments.Clear ();
			_dietComments.ForEach (e => {
				_model.comments.Add(e.Value);
			});

			var finalResult = new List<RestrictionModelSimple> ();
			_restrictionTypeGroups.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});
			_model.currentRestrictions = finalResult.Distinct()
				.ToList();
			_model.allRestrictionTypes = null;
			_model.start_date = _startDate.DateValue.ToDisplayDatetime ();
			_model.end_date = _endDate.DateValue.ToDisplayDatetime ();
			_model.registration.registration_id = _registrationId;

			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetTherapeuticModel()
		{
			var content = KnownUrls.InstitutionAndRegistrationQueryString (null, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<TherapeuticDietOrderModel> ()
				.From (KnownUrls.GetFluidRestrictionModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(TherapeuticDietOrderModel result)
		{
			OnRequestCompleted (true);
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			this._model = result;
			_model = result;
			int idx = 0;
			_model.comments.ForEach (e => {
				_dietComments.Add (new MultilineEntryElement (commentText + ": ", "", e, idx,this));
				idx++;
			});
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction"));
		}

		private async void Save(TherapeuticDietOrderModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<TherapeuticDietOrderModel> ()
				.From (KnownUrls.SaveFluidRestriction)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}

		public void ItemSelected(int idx, int index)
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			var commentDetailsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Doyouwanttodeletethiscomment");

			this.ShowConfirmAlert (AppContext, commentText, commentDetailsText, ()=>RemoveComment(idx));
		}

		private void RemoveComment(int index)
		{
			if (index < _dietComments.Count) {
				_dietComments.RemoveAt (index);

				for (int i = 0; i < _dietComments.Count; i++) {
					_dietComments [i].Index = i;
				}

				CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction"));
			}
		}

		private void DateSelected()
		{
			_model.start_date = _startDate.DateValue;
			_model.end_date = _endDate.DateValue;
		}
	}
}

