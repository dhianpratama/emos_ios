﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Globalization;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;
using RaEngine.Service;

namespace emos_ios
{
	public class NilByMouthV2DVC : BaseDVC
	{
		private FormattedDateElement _startDate, _endDate;
		private CommonDietOrderV2Model _model;
		private long? _registrationId;
		private long? _profileDietOrderId;
		private ICommonDataListCallback _callback;
		private InterfaceStatusModel _interfaceStatus;
		private UIBarButtonItem _deleteButton = new UIBarButtonItem (UIBarButtonSystemItem.Trash);

		private List<CheckboxGroupDVC<MealOrderPeriodModelSimple>> _mealPeriodsGroup;
		private List<Section> _mealPeriodCheckboxes;

		public NilByMouthV2DVC (ICommonDataListCallback controller, long? registrationId, long? profileDietOrderId, InterfaceStatusModel interfaceStatus,IApplicationContext appContext) : base (AppDelegate.Instance)
		{
			this._interfaceStatus = interfaceStatus;
			this._registrationId = registrationId;
			this._profileDietOrderId = profileDietOrderId;
			_callback = controller;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetNilByMouthModel);
		}

		private void CreateRightButtons()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var saveButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			saveButton.TintColor = UIColor.White;
			var rightButtons = new UIBarButtonItem[] { saveButton };
			if (!_interfaceStatus.DIET_ORDERS)
				NavigationItem.SetRightBarButtonItems (rightButtons, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var nilByMounthConfigurationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nilbymouthconfiguration");
			_startDate = new FormattedDateElement (startDateText, _model.start_date ?? DateTime.Now.ToDisplayDatetime ());
			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			_endDate = new FormattedDateElement (endDateText, _model.end_date ?? DateTime.Now.AddDays (Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ());
			Root.Add (new Section (nilByMounthConfigurationText) {
				_startDate,
				_endDate,
			});
			LoadMealPeriods ();
		}
		private void LoadMealPeriods(){
			_mealPeriodsGroup = new List<CheckboxGroupDVC<MealOrderPeriodModelSimple>> ();

			_mealPeriodCheckboxes = new List<Section> ();

			var checkboxGroup = new CheckboxGroupDVC<MealOrderPeriodModelSimple> ("Meal Periods", _model.orderedMealPeriods, _model.allMealPeriods);
			_mealPeriodsGroup.Add (checkboxGroup);
			_mealPeriodCheckboxes.Add (checkboxGroup.Create ());

			Root.Add (_mealPeriodCheckboxes);
		}
		private string ConvertDate (DateTime? dateValue)
		{
			CultureInfo languageCode = new CultureInfo (AppDelegate.Instance.LanguageHandler.LanguageCode);
			return dateValue == null ? "Not specified" : dateValue.Value.ToString ("dd/MM/yyyy hh:mm", languageCode);
		}
		private void OnSave()
		{
			var finalResult = new List<MealOrderPeriodModelSimple> ();
			_mealPeriodsGroup.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});

			if (finalResult.Count <= 0) {
				this.ShowAlert (AppDelegate.Instance, "Pleaseselectatleastonemealperiod");
				return;
			}

			_model.start_date = _startDate.DateValue.ToDisplayDatetime ();
			_model.end_date = _endDate.DateValue.ToDisplayDatetime ();

			_model.orderedMealPeriods = finalResult.Distinct()
				.ToList();
			
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetNilByMouthModel()
		{
			var content = KnownUrls.GetCommonDietOrderQueryString ("DIET41", AppDelegate.Instance.InstitutionId, this._registrationId);

			if(_profileDietOrderId != null)
				content = KnownUrls.GetCommonDietOrderQueryV2String ("DIET41", AppDelegate.Instance.InstitutionId, this._registrationId, _profileDietOrderId);

			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderV2Model> ()
				.From (KnownUrls.GetCommonDietOrderV2Model)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnRequestModelCompleted(CommonDietOrderV2Model result)
		{
			OnRequestCompleted (true);
			this._model = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.NilByMouthTitle));
			CreateRightButtons ();
			AddDeleteButton (result);
		}
		private void AddDeleteButton (CommonDietOrderV2Model result)
		{
			if (_interfaceStatus.DIET_ORDERS || result.profile_diet_order_id == 0)
				return;

			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);
			if (!rightButtons.Contains (_deleteButton)) {
				_deleteButton.Clicked+= (sender, e) => {
					View.ShowConfirmAlert(AppContext, "NilbyMouth", "Areyousurewanttodeactivatethis?", OnDeleteConfirmed);
				};
				rightButtons.Add (_deleteButton);
			}
			NavigationItem.SetRightBarButtonItems (rightButtons.ToArray (), false);
		}
		private void OnDeleteConfirmed ()
		{
			_requestHandler.SendRequest (View, () => DeleteModel ());
		}
		private async void Save(CommonDietOrderV2Model model)
		{
			string saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SaveNBM");
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveCommonDietOrderV2)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false,saveText,true))
				.Go ();
		}
		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success)
				this.ShowAlert (AppContext, "Savedsuccessful");
			else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
		private async void DeleteModel()
		{
			long profileDietOrderId = Convert.ToInt32 (_model.profile_diet_order_id);
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (result.success);
			if (!result.success)
				return;
			var message = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nbmhasbeendeactivated"));
			this.ShowAlert (AppDelegate.Instance, message);
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
	}
}

