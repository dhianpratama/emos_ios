// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ExtraOrderViewController")]
	partial class ExtraOrderViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton AddCommentButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CommentLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView CommentTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField CommentText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView ExtraOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Action ("AddCommentButton_TouchDown:")]
		partial void AddCommentButton_TouchDown (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (AddCommentButton != null) {
				AddCommentButton.Dispose ();
				AddCommentButton = null;
			}

			if (CommentTable != null) {
				CommentTable.Dispose ();
				CommentTable = null;
			}

			if (CommentText != null) {
				CommentText.Dispose ();
				CommentText = null;
			}

			if (ExtraOrderTable != null) {
				ExtraOrderTable.Dispose ();
				ExtraOrderTable = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (CommentLabel != null) {
				CommentLabel.Dispose ();
				CommentLabel = null;
			}
		}
	}
}
