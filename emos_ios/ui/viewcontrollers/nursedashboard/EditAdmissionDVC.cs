﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.models;
using emos_ios.tools;

using VMS_IRIS.Areas.EmosIpad.Models;
using ios;

namespace emos_ios
{
	public class EditAdmissionDVC : BaseDVC
	{
		RootElement _identifierOption, _patientTitleOption, _genderOption, _raceOption, _languageOption, _treatmentCategoryOption, _patientClassOption ;
		EntryElement _patientName, _identifierNumber, _caseNumber;
		FormattedDateElement _birthday;
		FormattedDateTimeElement _admissionTime;
		List<TreatmentCategoryModelSimple> _treatmentCategories;
		EditAdmissionModel editAdmissionModel;
		List<ClassModelSimple> _mealClasses;

		private long? registration_id;
		private long? bed_id;

		private ServerConfig _serverConfig = new ServerConfig();

		public EditAdmissionDVC(long? registration_id, long? bed_id) : base (AppDelegate.Instance)
		{
			Initialize (registration_id, bed_id);
		}
		public EditAdmissionDVC (long? registration_id) : base (AppDelegate.Instance)
		{
			Initialize (registration_id, 0);
		}
		private void Initialize (long? registration_id, long? bed_id)
		{
			this.registration_id = registration_id;
			this.bed_id = bed_id;
			GetData ();
		}

		private void CreateSaveButton()
		{
			var saveTextButton = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveTextButton, UIBarButtonItemStyle.Plain, delegate {
				onSaveClicked();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void GetData() 
		{
			_requestHandler.SendRequest (View, RequestAdmissionModel);
		}

		private async void RequestAdmissionModel()
		{
			//MBHUDView.HudWithBody ("Please Wait...", MBAlertViewHUDType.ActivityIndicator, 10.0f, true);
			var editAdmissionContent = KnownUrls.GetAdmissionQueryString (this.registration_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.GetEditAdmissionModel)
				.WithQueryString (editAdmissionContent)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private async void SaveAdmission(AdmissionModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.SaveAdmission)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		public void OnRequestModelCompleted(EditAdmissionModel result)
		{
			OnRequestCompleted (true);
			editAdmissionModel = result;
			if (this.bed_id != null)
				editAdmissionModel.admissionModel.location_id = bed_id;
			InitiateTreatmentCategories ();
			InitiateMealClasses ();
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("EditAdmission"));
			CreateSaveButton ();
		}
		private void InitiateTreatmentCategories ()
		{
			_treatmentCategories = new List<TreatmentCategoryModelSimple> {
				new TreatmentCategoryModelSimple {
					label = AppContext.LanguageHandler.GetLocalizedString("None")
				},
			};
			_treatmentCategories.AddRange (editAdmissionModel.treatmentCategories);
		}
		private void InitiateMealClasses ()
		{
			_mealClasses = new List<ClassModelSimple> {
				new ClassModelSimple { label = AppContext.LanguageHandler.GetLocalizedString("Pleaseselect") },
			};
			_mealClasses.AddRange (editAdmissionModel.classes);
		}
		public void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var identifierText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Identifier");
			var titleText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Title");
			var genderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Gender");
			var languageText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Language");
			var treatmentCategoryText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TreatmentCategory");
			var mealClassText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealClass");
			var raceText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Race");
			var patientNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientName");
			var patientNameMsgText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enterpatientname");
			var identifierNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("IdentifierNumber");
			var enterIdentifierNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enteridentifiernumber");
			var birthdayText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Birthday");
			var admissionTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionTime");
			var caseNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CaseNumber");
			var enterCaseNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Entercasenumber");
			var patientDataText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientData");
			var admissionInformationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionInformation");

			_identifierOption = new RadioGroupDVC<DocumentModelSimple> ()
				.CreateRadioGroup(identifierText, editAdmissionModel.admissionModel.identifier_id, editAdmissionModel.documents);

			_patientTitleOption = new RadioGroupDVC<SalutationModelSimple> ()
				.CreateRadioGroup(titleText, editAdmissionModel.admissionModel.salutation_id, editAdmissionModel.salutations);

			_genderOption = new RadioGroupDVC<GenderModelSimple> ()
				.CreateRadioGroup(genderText, editAdmissionModel.admissionModel.gender_id, editAdmissionModel.genders);

			_languageOption = new RadioGroupDVC<LanguageModelSimple> ()
				.CreateRadioGroup(languageText, editAdmissionModel.admissionModel.language_id, editAdmissionModel.languages);

			_treatmentCategoryOption = new RadioGroupDVC<TreatmentCategoryModelSimple> ()
				.CreateRadioGroup(treatmentCategoryText, editAdmissionModel.admissionModel.treatment_category_id ?? 0, _treatmentCategories);

			_patientClassOption = new RadioGroupDVC<ClassModelSimple> ()
				.CreateRadioGroup(mealClassText, editAdmissionModel.admissionModel.patient_class_id ?? 0, _mealClasses);

			_raceOption = new RadioGroupDVC<RaceModelSimple> ()
				.CreateRadioGroup(raceText, editAdmissionModel.admissionModel.race_id, editAdmissionModel.races);

			_patientName = new EntryElement (patientNameText, patientNameMsgText, editAdmissionModel.admissionModel.profile_name);
			_identifierNumber = new EntryElement (identifierNumberText, enterIdentifierNumberText, editAdmissionModel.admissionModel.identifier_number);
			_identifierNumber.ReturnKeyType = UIReturnKeyType.Done;
			_identifierNumber.ShouldReturn += delegate {
				_requestHandler.SendRequest(View, RequestExistingProfile);
				_identifierNumber.ResignFirstResponder(true);
				return true;
			};
			_birthday = new FormattedDateElement (birthdayText, editAdmissionModel.admissionModel.birthday);
			_admissionTime = new FormattedDateTimeElement (admissionTimeText, editAdmissionModel.admissionModel.admission_time.ToDisplayDatetime ());

			_caseNumber = new EntryElementDVC (caseNumberText, enterCaseNumberText, editAdmissionModel.admissionModel.case_number);

			Root.Add (new Section (patientDataText) {
				_identifierOption,
				_identifierNumber,
				_patientTitleOption,
				_patientName,
				_genderOption,
				_raceOption,
				_languageOption,
				_birthday
			});

			Root.Add (new Section (admissionInformationText) {
				new StringElement (editAdmissionModel.admissionModel.location_label),
				_admissionTime,
				_caseNumber,
				_treatmentCategoryOption,
				_patientClassOption
			});
		}

		public void onSaveClicked()
		{
			if (InvalidData ())
				return;
			editAdmissionModel.admissionModel.identifier_id = editAdmissionModel.documents [_identifierOption.RadioSelected].id;
			editAdmissionModel.admissionModel.identifier_number = _identifierNumber.Value;
			editAdmissionModel.admissionModel.salutation_id = editAdmissionModel.salutations [_patientTitleOption.RadioSelected].id;
			editAdmissionModel.admissionModel.profile_name = _patientName.Value;
			editAdmissionModel.admissionModel.gender_id = editAdmissionModel.genders [_genderOption.RadioSelected].id;
			editAdmissionModel.admissionModel.race_id = editAdmissionModel.races [_raceOption.RadioSelected].id;
			editAdmissionModel.admissionModel.language_id = editAdmissionModel.languages [_languageOption.RadioSelected].id;
			editAdmissionModel.admissionModel.birthday = DateTime.SpecifyKind(_birthday.DateValue, DateTimeKind.Utc);
			editAdmissionModel.admissionModel.admission_time = DateTime.SpecifyKind(_admissionTime.DateValue, DateTimeKind.Utc);
			editAdmissionModel.admissionModel.case_number = _caseNumber.Value;
			editAdmissionModel.admissionModel.treatment_category_id = _treatmentCategoryOption.RadioSelected == 0 ? null : _treatmentCategories [_treatmentCategoryOption.RadioSelected].id;
			editAdmissionModel.admissionModel.patient_class_id = _mealClasses [_patientClassOption.RadioSelected].id;
			editAdmissionModel.admissionModel.location_id = editAdmissionModel.admissionModel.location_id;
			editAdmissionModel.admissionModel.institution_id = _serverConfig.InsitutionId;
			_requestHandler.SendRequest (View, ()=> SaveAdmission (editAdmissionModel.admissionModel));
		}
		private bool InvalidData()
		{
			if (_patientClassOption.RadioSelected == 0) {
				this.ShowAlert(AppContext, "Pleaseselectmealclass");
				return true;
			}
			return false;
		}
		private async void RequestExistingProfile()
		{
			var identifier_id = editAdmissionModel.documents [_identifierOption.RadioSelected].id;
			var content = KnownUrls.GetExistingProfileQueryString (_identifierNumber.Value, identifier_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<EditAdmissionModel> ()
				.From (KnownUrls.GetExistingProfile)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestExistingProfileCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestExistingProfileCompleted(EditAdmissionModel result)
		{
			OnRequestCompleted (true);
			editAdmissionModel = result;
			if (this.bed_id != null)
				editAdmissionModel.admissionModel.location_id = bed_id;

			if (editAdmissionModel.admissionModel.profile_id == null)
				this.ShowAlert (AppContext, "Profile not found");

			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("EditAdmission"));
		}
	}
}

