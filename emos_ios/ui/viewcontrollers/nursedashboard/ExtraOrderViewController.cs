﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using emos_ios.tools;
using ios;

namespace emos_ios
{
	public partial class ExtraOrderViewController : BaseViewController, ICallback<ExtraOrderModelSimple>, IExtraOrderEntryCallback, IStringWithButtonCallback
	{
		private const long MealPeriodAll = 0;
		private MultipleSelectionDataSource<ExtraOrderModelSimple, ExtraOrderEntryViewCell> _dataSource;
		private LocationWithRegistrationSimple _locationWithRegistrationSimple;
		private long _registrationId;
		private List<ExtraOrderModelSimple> _extraOrders;
		private ExtraOrderEntryDVC _extraOrderEntryDVC;
		private TableAndHeaderView<KeyValuePair<string, string>, KeyValuePairViewCell> _mealPeriodView;
		private long _profileDietOrderId;
		private ExtraOrderModel _model;
		private ICommonDataListCallback _callback;
		private DynamicTableSource<KeyValuePair<int, string>, StringWithButtonViewCell> _commentSource;
		private InterfaceStatusModel _interfaceStatus;

		public ExtraOrderViewController (ICommonDataListCallback callback, LocationWithRegistrationSimple locationWithRegistrationSimple, long profileDietOrderId, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance, "ExtraOrderViewController", null)
		{
			_registrationId = (long)locationWithRegistrationSimple.registrations.First ().registration_id;
			_locationWithRegistrationSimple = locationWithRegistrationSimple;
			_profileDietOrderId = profileDietOrderId;
			_callback = callback;
			_interfaceStatus = interfaceStatus;
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.ExtraOrder);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			HeaderView.Frame = new RectangleF (0, Measurements.TopY, 768, Settings.ShowOtherDietsCommentField ? 
				Measurements.HeaderHeight : Measurements.HeaderHeight - 40);
			ExtraOrderTable.SetFrameBelowTo  (HeaderView, 0, 768, 
				Settings.ShowOtherDietsCommentField? Measurements.BodyHeight : Measurements.BodyHeight + 40, 
				distanceToAbove: 0);
			InitiateExtraOrderEntryDVC ();
			InitiateComments ();
			InitiateMealPeriodView ();
			InitiateExtraOrderDataSource ();
			ExtraOrderTable.AllowsSelection = false;
			_requestHandler.SendRequest (View, RequestGetModel);
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public void SetRightButtons()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var saveButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Done, delegate {
				OnSaveButtonClicked();
			});
			saveButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (saveButton, true);
		}
		private void InitiateExtraOrderEntryDVC ()
		{
			_extraOrderEntryDVC = new ExtraOrderEntryDVC (this, _locationWithRegistrationSimple, _model);
			_extraOrderEntryDVC.View.Frame = new RectangleF (0, 0, 400, Measurements.HeaderHeight);
			HeaderView.AddSubview (_extraOrderEntryDVC.View);
		}
		private void InitiateComments ()
		{
			if (!Settings.ShowOtherDietsCommentField)
				return;
			AddCommentButton.Hidden = _interfaceStatus.DIET_ORDERS;
			CommentText.Hidden = _interfaceStatus.DIET_ORDERS;
			CommentLabel.Hidden = !_interfaceStatus.DIET_ORDERS;
			_extraOrderEntryDVC.View.SetFrame (height: 143);
			CommentText.SetFrameBelowTo(_extraOrderEntryDVC.View, 10, 230, distanceToAbove: 10);
			CommentLabel.Frame = CommentText.Frame;
			AddCommentButton.SetFrame(CommentText.Frame.Left + CommentText.Frame.Width + 20, CommentText.Frame.Top, 130);
			AddCommentButton.Layer.CornerRadius = 10;
			CommentTable.SetFrameBelowTo(CommentText, 0, 400, 100, 10);
			CommentTable.RowHeight = 38f;
			var setting = new BaseViewCellSetting {
				CellHeight = 38
			};
			var handler = new BaseViewCellHandler<KeyValuePair<int, string>> (StringWithButtonViewCell.CellIdentifier, this, setting);
			_commentSource = new DynamicTableSource<KeyValuePair<int, string>, StringWithButtonViewCell> (handler);
			CommentTable.Source = _commentSource;
		}
		private void InitiateMealPeriodView ()
		{
			var setting = new BaseViewCellSetting ();
			var handler = new NoCallbackViewCellHandler<KeyValuePair<string, string>> (AppContext, KeyValuePairViewCell.CellIdentifier, setting);
			var targetMealPeriodText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TargetMealPeriod");
			_mealPeriodView = new TableAndHeaderView<KeyValuePair<string, string>, KeyValuePairViewCell> (AppContext, handler, targetMealPeriodText, false, false);
			var x = _extraOrderEntryDVC.View.Frame.Width;
			_mealPeriodView.SetLayout (new RectangleF (x, 0, 768 - x, Measurements.HeaderHeight));
			_mealPeriodView.BackgroundColor = Colors.ViewCellBackground;
			HeaderView.AddSubview (_mealPeriodView);
		}
		private void InitiateExtraOrderDataSource ()
		{
			var setting = new BaseViewCellSetting ();
			var handler = new BaseViewCellHandler<ExtraOrderModelSimple> (ExtraOrderEntryViewCell.CellIdentifier, this, setting) {
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
			_dataSource = new MultipleSelectionDataSource<ExtraOrderModelSimple, ExtraOrderEntryViewCell> (handler) {
				ReloadOnSelectionChanged = true
			};
			_dataSource.OnItemDeselected += OnExtraOrderDeselected;
			_dataSource.OnItemSelected += OnExtraOrderSelected;
			ExtraOrderTable.Source = _dataSource;
		}
		private void OnExtraOrderSelected (object sender, RowSelectionEventArgs<ExtraOrderModelSimple> e)
		{
		}
		private void OnExtraOrderDeselected (object sender, RowSelectionEventArgs<ExtraOrderModelSimple> e)
		{
		}
		private float GetHeightForRow (ExtraOrderModelSimple item)
		{
			return ExtraOrderEntryViewCell.CalculateRowHeight (item);
		}
		private void OnRequestExtraOrderSuccessful (ExtraOrderModel result)
		{
			OnRequestCompleted (true);
			SetDefaults (result);
			_model = result;

			var authorizedExtraOrder = AppContext.CurrentUser.Authorized ("MANAGE_EXTRA_ORDER_TYPE");
			_extraOrderEntryDVC.LoadModel (result);
			_extraOrders = result.extraOrders;
			_dataSource.Items = authorizedExtraOrder ? _extraOrders.ToList () :
				_extraOrders.Where(r => r.quantity > 0).ToList ();
			var mealPeriods = new List<KeyValuePair<string, string>> {
					//new KeyValuePair<string,string>(MealPeriodAll.ToString(),"All")
				};
			_model.allMealPeriods
				.ForEach (mp => {
					mealPeriods.Add(new KeyValuePair<string,string>(mp.id.ToString(),mp.label));
			});

			_mealPeriodView.TableView.SetItems (mealPeriods);
			_mealPeriodView.TableView.SetFrame (height: mealPeriods.Count * _mealPeriodView.TableView.RowHeight);
			_mealPeriodView.TableView.ReloadData ();
			RefreshExtraOrderTable ();
			LoadComments ();
			ShowSavedMealPeriod (mealPeriods, result.mealPeriodId ?? (long)_model.allMealPeriods.FirstOrDefault().id);

			if (Settings.DisableExtraOrderEdit && _model.profileDietOrderId != null && _model.profileDietOrderId != 0) {
				_mealPeriodView.UserInteractionEnabled = false;
				_extraOrderEntryDVC.View.UserInteractionEnabled = false;
			}
			SetRightButtons ();
		}
		private void LoadComments ()
		{
			if (_model.comments == null) {
				_model.comments = new List<string> ();
				return;
			}
			for (int i = 0; i < _model.comments.Count; i++) {
				_commentSource.Items.Add (new KeyValuePair<int, string> (i, _model.comments [i]));
			}
			CommentTable.ReloadData ();
		}
		private void SetDefaults (ExtraOrderModel model)
		{
			foreach (var extraOrder in model.extraOrders) {
				extraOrder.IsSuitableOnly = true;
				var totalQuantity = extraOrder.details.Sum (e => e.quantity);
				if (totalQuantity == 0 && _interfaceStatus.DIET_ORDERS) {
					extraOrder.quantity = Settings.DefaultNewExtraOrderQuantity;
				}
			}
		}
		private void RefreshExtraOrderTable ()
		{
			ExtraOrderTable.ReloadData ();
//			ShowExtraOrderSelections ();
		}
		private void ShowSavedMealPeriod (List<KeyValuePair<string, string>> mealPeriods, long mealPeriodId)
		{
			var id = mealPeriodId.ToString ();
			for (int i = 0; i < mealPeriods.Count; i++) {
				if (mealPeriods [i].Key == id) {
					_mealPeriodView.TableView.SelectRow (i);
				}
			}
		}
		private void ShowExtraOrderSelections()
		{
			var selectedRows = new List<int> ();
			for (int i = 0; i < _extraOrders.Count; i++) {
				if (_extraOrders [i].selected) {
					selectedRows.Add (i);
				}
			}
			_dataSource.SelectRows (selectedRows, ExtraOrderTable);
		}
		#region ICallback implementation
		public void ItemSelected (ExtraOrderModelSimple selected, int selectedIndex)
		{
		}
		#endregion

		#region IExtraOrderEntryCallback implementation
		public void DetailSelected (SpecialInstructionDetail selected)
		{
			var detail = GetDetail(selected);
			detail.selected = true;
			detail.quantity = selected.quantity;
		}
		public void DetailDeselected (SpecialInstructionDetail deselected)
		{
			var detail = GetDetail(deselected);
			detail.selected = false;
			detail.quantity = 0;
		}
		public void ValueChanged (ExtraOrderModelSimple model)
		{
			var extraOrder = GetModel (model);
			extraOrder.quantity = model.quantity;
			extraOrder.selected = model.selected;
			foreach (var newDetail in model.details) {
				var detail = GetDetail (newDetail);
				detail.quantity = newDetail.quantity;
				detail.selected = newDetail.selected;
			}
		}
		public void SelectionChanged (ExtraOrderModelSimple extraOrderModel, bool Reload = false)
		{
			for (int i = 0; i < _extraOrders.Count; i++) {
				if (_extraOrders [i].id != extraOrderModel.id)
					continue;
				_extraOrders [i] = extraOrderModel;
			}
			if (Reload)
				ExtraOrderTable.ReloadData ();
		}
		public void SuitableChanged (ExtraOrderModelSimple extraOrderModel, bool IsSuitableOnly)
		{
			var extraOrder = GetModel (extraOrderModel);
			extraOrder.IsSuitableOnly = IsSuitableOnly;
			RefreshExtraOrderTable ();
		}
		public bool GetAllowMultipleSelection ()
		{
			return _model.multipleSelection;
		}
		#endregion
		public void ItemSelected (KeyValuePair<int, string> selected, int selectedIndex)
		{
		}
		public void ActionButtonClicked (KeyValuePair<int, string> selected)
		{
			_model.comments.Remove(selected.Value);
			_commentSource.Items.Remove (selected);
			CommentTable.ReloadData ();
		}
		public bool EnableAction ()
		{
			return _interfaceStatus.DIET_ORDERS;
		}
		private SpecialInstructionDetail GetDetail(SpecialInstructionDetail detail)
		{
			return _extraOrders.SelectMany (e => e.details).Single (d => d.id == detail.id);
		}
		private ExtraOrderModelSimple GetModel(ExtraOrderModelSimple model)
		{
			return _extraOrders.Single (e => e.id == model.id);
		}
		private async void RequestGetModel()
		{
			var qs = Settings.DisableExtraOrderEdit ? 
				KnownUrls.GetExtraOrderEditModelBasedOnDateQueryString (_profileDietOrderId, _registrationId, AppDelegate.Instance.OperationDate) :
				KnownUrls.GetExtraOrderEditModelQueryString (_profileDietOrderId, _registrationId) ;

			await AppDelegate.Instance.HttpSender.Request<ExtraOrderModel>()
				.From (KnownUrls.GetExtraOrderEditModel)
				.WithQueryString(qs)
				.WhenSuccess (result => OnRequestExtraOrderSuccessful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private async void RequestSave(SaveExtraOrderModelIpad saveModel)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.SaveExtraOrder)
				.WithContent(saveModel)
				.WhenSuccess (result => OnRequestSaveSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnRequestSaveSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
		private void OnSaveButtonClicked()
		{
			var saveModel = GenerateSaveModel ();
			if (!ValidateInput (saveModel))
				return;

			_requestHandler.SendRequest (View, () => RequestSave (saveModel));
		}
		private bool ValidateInput (SaveExtraOrderModelIpad model)
		{
			if (_model.extraOrders.Where (e => e.quantity > 0).Count () == 0) {
				this.ShowAlert (AppContext, LanguageKeys.NoExtraOrderSelected);
				return false;
			}

			foreach (var extraOrder in model.extraOrder) {
				if (extraOrder.details.Any (d => d.selected && d.quantity == 0)) {
					this.ShowAlert (AppContext, "Total item's quantity must be equal to main quantity");
					return false;
				}

				var totalQuantity = extraOrder.details.Sum (d => d.quantity);
				var canOrderUnsuitable = AppContext.CurrentUser.Authorized ("MANAGE_EXTRA_ORDER_TYPE");
				if (canOrderUnsuitable && totalQuantity == 0)
					continue;

				var unsuitableQuantity = extraOrder.details.Where (d => !d.suitable).Sum (d => d.quantity);
				if (!canOrderUnsuitable && extraOrder.quantity == totalQuantity - unsuitableQuantity)
					continue;

				if (totalQuantity != extraOrder.quantity) {
					this.ShowAlert (AppContext, "Total item's quantity must be equal to main quantity");
					return false;
				}
			}
			return true;
		}
		private SaveExtraOrderModelIpad GenerateSaveModel()
		{
			var saveModel = new SaveExtraOrderModelIpad () {
				registrationId = _registrationId,
				profileId = (long)_locationWithRegistrationSimple.registrations.First ().profile_id,
				profileDietOrderId = _model.profileDietOrderId,
				mealOrderPeriodId = long.Parse (_mealPeriodView.Selected.Key),
				startTime = _extraOrderEntryDVC.GetStartDate (),
				endTime = _extraOrderEntryDVC.GetEndDate (),
				extraOrder = GetInput (),
				comments = _model.comments
			};
			SetDetailQuantityOnSingleSelection (saveModel);
			ReplaceUnsuitables (saveModel);
			return saveModel;
		}
		private void SetDetailQuantityOnSingleSelection (SaveExtraOrderModelIpad saveModel)
		{
			if (GetAllowMultipleSelection ())
				return;
			foreach (var eo in saveModel.extraOrder) {
				foreach (var detail in eo.details.Where(d => d.selected)) {
					detail.quantity = eo.quantity;
				}
			}
		}
		private void ReplaceUnsuitables (SaveExtraOrderModelIpad saveModel)
		{
			if (AppContext.CurrentUser.Authorized ("MANAGE_EXTRA_ORDER_TYPE"))
				return;
			foreach (var extraOrder in saveModel.extraOrder) {
				var totalQuantity = extraOrder.details.Sum (d => d.quantity);
				var unsuitableQuantity = extraOrder.details.Where (d => !d.suitable).Sum (d => d.quantity);
				if (extraOrder.quantity == totalQuantity - unsuitableQuantity) {
					foreach (var detail in extraOrder.details.Where(d => !d.suitable)) {
						detail.quantity = 0;
						detail.selected = false;
					}
				}
			}
		}
		private List<ExtraOrderModelSimple> GetInput ()
		{
			return _extraOrders
				.Where (e => e.quantity > 0)
				.ToList ();
		}
		private List<ExtraOrderModelSimple> ClearExtraModel ()
		{
			var result = _extraOrders;
			result.ForEach (e =>  {
				e.selected = false;
				e.details.ForEach (d =>  {
					d.selected = false;
					d.quantity = 0;
				});
			});
			return result;
		}
		partial void AddCommentButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			if (string.IsNullOrEmpty(CommentText.Text))
				return;
			_commentSource.Items.Add(new KeyValuePair<int, string>(_model.comments.Count, CommentText.Text));
			_model.comments.Add(CommentText.Text);
			CommentText.Text = "";
			CommentTable.ReloadData();
		}
	}
}

