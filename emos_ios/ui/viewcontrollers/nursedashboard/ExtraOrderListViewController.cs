﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public partial class ExtraOrderListViewController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> ExtraOrderListDataSource;

		private InfoDataListHandler _extraOrderListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private LocationWithRegistrationSimple _locationWithRegistrations;
		private RegistrationModelSimple _registration;
		private InterfaceStatusModel _interfaceStatus;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }
		private long _selectedId;

		public ExtraOrderListViewController (IApplicationContext appContent, LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance, "ExtraOrderListViewController", null)
		{
			_locationWithRegistrations = locationWithRegistration;
			_registration = locationWithRegistration.registrations.First ();
			_interfaceStatus = interfaceStatus;
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.ExtraOrders);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			ExtraOrderLabel.SetFrame (y: Measurements.TopY + 20.0f);
			if (!Settings.ShowDVCTitle)
				ExtraOrderLabel.SetFrame (height: 0);

			ExtraOrderListTable.SetFrameBelowTo (ExtraOrderLabel);

			/*
			if (Settings.ShowAdditionalPatientMenuLinks) {
				var headerDVC = new TrialOrderHeaderDVC (AppContext, PatientMenuCallback, _locationWithRegistrations, Title);
				headerDVC.View.SetFrameBelowTo (ExtraOrderLabel, 0, 768, 350, 0);
				View.AddSubview (headerDVC.View);
				ExtraOrderListTable.SetFrameBelowTo (headerDVC.View, height: Measurements.TrialOrderTableHeight, distanceToAbove: 0);
			} else {
				ExtraOrderListTable.SetFrameBelowTo (ExtraOrderLabel, height: Measurements.TrialOrderTableHeight, distanceToAbove: 0);
			}
			*/
			ExtraOrderListTable.SetFrameBelowTo (ExtraOrderLabel);
			_requestHandler.SendRequest (View, RequestExtraOrderViewModel);
		}

		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			var addButton = new UIBarButtonItem(AppDelegate.Instance.LanguageHandler.GetLocalizedString("Add"), UIBarButtonItemStyle.Done, delegate {
				OnEdit(0);
			});
			addButton.TintColor = UIColor.White;

			if(!_interfaceStatus.DIET_ORDERS)
				NavigationItem.SetRightBarButtonItem (addButton, false);
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_extraOrderListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				DisableDeleteButton = !AppDelegate.Instance.CurrentUser.Authorized ("DELETE_EXTRA_ORDER")
			};
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			ExtraOrderListTable.Hidden = false;
			ExtraOrderListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_extraOrderListViewCellHandler);
			ExtraOrderListDataSource.Items = _items;
			ExtraOrderListTable.Source = ExtraOrderListDataSource;
			ExtraOrderListTable.ReloadData ();
		}

		public void SetLayout()
		{
			ExtraOrderListTable.ScrollEnabled = true;
			ExtraOrderListTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			var controller = new ExtraOrderViewController (this,_locationWithRegistrations, id, _interfaceStatus);
			NavigationController.PushViewController (controller, true);
		}

		public void OnDelete(long id)
		{
			var extraOrderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.ExtraOrder);
			var extraOrderConfirmText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.DeleteExtraOrderConfirmation);
			_selectedId = id;
			this.ShowConfirmAlert (AppContext, extraOrderText, extraOrderConfirmText, OnDeleteConfirmed);
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestExtraOrderViewModel()
		{
			var queryString = KnownUrls.RegistrationIdQueryString ((long)_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>>()
				.From (KnownUrls.GetExtraOrderViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestTherapeuticModelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestTherapeuticModelSuccesful(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			_items = result;
			GenerateRightButton ();
			if (_items.Count == 0) {
				ShowNotFound ();
			} else {
				ShowTable ();
				Load ();
			}
		}

		private async void DeleteProfileTherapeutic()
		{
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (_selectedId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var extraOrderAlert = String.Format(AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.DeletedExtraOrderInformation), _items.First ().First (e => e.Key == "Identifier").Value);
				this.ShowAlert (AppContext, extraOrderAlert, translateMessage: false);
				_requestHandler.SendRequest (View, RequestExtraOrderViewModel);
			} else
				OnRequestCompleted (false);
		}
		private void OnDeleteConfirmed()
		{
			_requestHandler.SendRequest (View, DeleteProfileTherapeutic);
		}
		private void ShowTable()
		{
			ExtraOrderListTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			ExtraOrderListTable.Hidden = true;
			NotFoundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			ExtraOrderListTable.Hidden = true;
			NotFoundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestExtraOrderViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			ExtraOrderLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.ExtraOrders);
			ExtraOrderErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.NoExtraOrderIsFound);
		}
	}
}

