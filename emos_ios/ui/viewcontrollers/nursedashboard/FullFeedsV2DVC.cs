﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;
using RaEngine.Service;

namespace emos_ios
{
	public class FullFeedsV2DVC : BaseDVC, ICallback<int>
	{
		private FormattedDateElement _startDate, _endDate;
		private ICommonDataListCallback _callback;
		private CommonDietOrderV2Model _model;
		private long? _registrationId;
		private long? _profileDietOrderId;
		private InterfaceStatusModel _interfaceStatus;

		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;
		private Section _dietCommentSection;
		private UIBarButtonItem _deleteButton = new UIBarButtonItem (UIBarButtonSystemItem.Trash);

		private List<CheckboxGroupDVC<MealOrderPeriodModelSimple>> _mealPeriodsGroup;
		private List<Section> _mealPeriodCheckboxes;

		public FullFeedsV2DVC (ICommonDataListCallback controller, long? registrationId, long? profileDietOrderId, InterfaceStatusModel interfaceStatus,IApplicationContext appContext) : base (AppDelegate.Instance)
		{
			this._interfaceStatus = interfaceStatus;
			this._registrationId = registrationId;
			this._profileDietOrderId = profileDietOrderId;
			_dietComments = new List<MultilineEntryElement> ();
			_callback = controller;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetFullFeedsModel);
		}

		private void CreateSaveButton()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			if(_interfaceStatus.DIET_ORDERS!=true)
				NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var fullFeedInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fullfeedsinfo");
			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");

			_startDate = new FormattedDateElement (startDateText, _model.start_date ?? DateTime.UtcNow.ToDisplayDatetime ().Date);
			_endDate = new FormattedDateElement (endDateText, _model.end_date ?? DateTime.UtcNow.AddDays(Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ());

			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));

			Root.Add (new Section (fullFeedInfoText) {
				_startDate,
				_endDate,
			});

			LoadMealPeriods ();

			_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
				AddNewDietComment();
			}){
				BackgroundColor = UIColor.LightGray,
				TextColor = UIColor.DarkGray
			};

			this.LoadCommentSection ();
		}
		private void LoadMealPeriods(){
			_mealPeriodsGroup = new List<CheckboxGroupDVC<MealOrderPeriodModelSimple>> ();

			_mealPeriodCheckboxes = new List<Section> ();

			var checkboxGroup = new CheckboxGroupDVC<MealOrderPeriodModelSimple> ("Meal Periods", _model.orderedMealPeriods, _model.allMealPeriods);
			_mealPeriodsGroup.Add (checkboxGroup);
			_mealPeriodCheckboxes.Add (checkboxGroup.Create ());

			Root.Add (_mealPeriodCheckboxes);
		}
		private void AddNewDietComment()
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (commentText + ": " , "", "",_dietComments.Count,this));
			Section _tempSection = _dietCommentSection;
			this.LoadCommentSection ();
			Root.Remove (_tempSection);
		}

		private void LoadCommentSection()
		{
			var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FullFeedComments");
			_dietCommentSection = new Section (dietCommentsText) {
				_addCommentButton
			};
			_dietComments.ForEach (r => _dietCommentSection.Add (r));
			Root.Add (_dietCommentSection);
		}
		public void ItemSelected(int idx, int index)
		{
		}
		private void OnSave()
		{
			var finalResult = new List<MealOrderPeriodModelSimple> ();
			_mealPeriodsGroup.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});

			if (finalResult.Count <= 0) {
				this.ShowAlert (AppDelegate.Instance, "Pleaseselectatleastonemealperiod");
				return;
			}

			_model.comments.Clear ();
			foreach (MultilineEntryElement e in _dietComments) {
				if (e.Value == "")
					continue;

				_model.comments.Add(e.Value);
			}
				
			_model.orderedMealPeriods = finalResult.Distinct()
				.ToList();
			
			_model.start_date = _startDate.DateValue;
			_model.end_date = _endDate.DateValue;
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetFullFeedsModel()
		{
			var content = KnownUrls.GetCommonDietOrderQueryString ("DIET135", AppDelegate.Instance.InstitutionId, this._registrationId);

			if(_profileDietOrderId != null)
				content = KnownUrls.GetCommonDietOrderQueryV2String ("DIET135", AppDelegate.Instance.InstitutionId, this._registrationId, _profileDietOrderId);
			
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderV2Model> ()
				.From (KnownUrls.GetCommonDietOrderV2Model)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(CommonDietOrderV2Model result)
		{
			OnRequestCompleted (true);
			this._model = result;
			if (_model.comments != null) {
				int idx = 0;
				var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
				_dietComments.Clear ();
				_model.comments.ForEach (e => {
					_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : ", "", e, idx,this));
					idx++;
				});
			}
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.FullFeedsTitle));
			CreateSaveButton ();
			AddDeleteButton (result);
		}
		private void AddDeleteButton (CommonDietOrderV2Model result)
		{
			if (_interfaceStatus.DIET_ORDERS || result.profile_diet_order_id == 0)
				return;
			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);
			if (!rightButtons.Contains (_deleteButton)) {
				_deleteButton.Clicked+= (sender, e) => {
					View.ShowConfirmAlert(AppContext, "FullFeeds", "Areyousurewanttodeactivatethis?", OnDeleteConfirmed);
				};
				rightButtons.Add (_deleteButton);
			}
			NavigationItem.SetRightBarButtonItems (rightButtons.ToArray (), false);
		}
		private void OnDeleteConfirmed ()
		{
			_requestHandler.SendRequest (View, () => DeleteModel ());
		}
		private async void Save(CommonDietOrderV2Model model)
		{
			string saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SaveFullFeeds");
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveCommonDietOrderV2)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false,saveText,true))
				.Go ();
		}

		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success)
				this.ShowAlert (AppContext, "Savedsuccessful");
			else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
		private async void DeleteModel()
		{
			long profileDietOrderId = Convert.ToInt32 (_model.profile_diet_order_id);
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (result.success);
			if (!result.success)
				return;
			var message = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fullfeedshasbeendeactivated"));
			this.ShowAlert (AppContext, message);
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
	}
}

