﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;

namespace emos_ios
{
	public class FullFeedsDVC : BaseDVC, ICallback<int>
	{
		private FormattedDateTimeElement _startDate, _endDate;
		private IPatientMenu _callback;
		private CommonDietOrderModel _model;
		private long? _registrationId;
		private InterfaceStatusModel _interfaceStatus;

		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;
		private Section _dietCommentSection;
		private UIBarButtonItem _deleteButton = new UIBarButtonItem (UIBarButtonSystemItem.Trash);

		public FullFeedsDVC (IPatientMenu controller, long? registrationId, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance)
		{
			this._interfaceStatus = interfaceStatus;
			this._registrationId = registrationId;
			_dietComments = new List<MultilineEntryElement> ();
			_callback = controller;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetFullFeedsModel);
		}

		private void CreateSaveButton()
		{
			if (!_callback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			if(_interfaceStatus.DIET_ORDERS!=true)
				NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var fullFeedInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fullfeedsinfo");
			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");

			_startDate = new FormattedDateTimeElement (startDateText, _model.start_date ?? DateTime.UtcNow.ToDisplayDatetime ());
			_endDate = new FormattedDateTimeElement (endDateText, _model.end_date ?? DateTime.UtcNow.AddDays(Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ());

			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			if (_interfaceStatus.DIET_ORDERS) {
				var startDate = _model.start_date ?? DateTime.UtcNow;
				if (_model.end_date == null)
					_model.end_date = DateTime.UtcNow.AddDays (7);

				Root.Add (new Section (fullFeedInfoText) {
					new EntryElementDVC (startDateText, "", startDate.ToLocalTime ().ToString (), true),
					new EntryElementDVC (endDateText, "", _model.end_date.Value.ToLocalTime ().ToString (), true),
				});
				if (_model.comments.Count > 0) {
					var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FullFeedComments");
					_dietCommentSection = new Section (dietCommentsText);
					_model.comments.ForEach (r => _dietCommentSection.Add (new EntryElementDVC (r, "", "", true)));
					Root.Add (_dietCommentSection);
				}
			} else {
				Root.Add (new Section (fullFeedInfoText) {
					_startDate,
					_endDate,
				});
				_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
					AddNewDietComment();
				}){
					BackgroundColor = UIColor.LightGray,
					TextColor = UIColor.DarkGray
				};

				this.LoadCommentSection ();
			}
		}
		private void AddNewDietComment()
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (commentText + ": " , "", "",_dietComments.Count,this));
			Section _tempSection = _dietCommentSection;
			this.LoadCommentSection ();
			Root.Remove (_tempSection);
		}

		private void LoadCommentSection()
		{
			var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FullFeedComments");
			_dietCommentSection = new Section (dietCommentsText) {
				_addCommentButton
			};
			_dietComments.ForEach (r => _dietCommentSection.Add (r));
			Root.Add (_dietCommentSection);
		}
		public void ItemSelected(int idx, int index)
		{
		}
		private void OnSave()
		{
			_model.comments.Clear ();
			foreach (MultilineEntryElement e in _dietComments) {
				if (e.Value == "")
					continue;

				_model.comments.Add(e.Value);
			}

			_model.start_date = _startDate.DateValue;
			_model.end_date = _endDate.DateValue;
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetFullFeedsModel()
		{
			var content = KnownUrls.GetCommonDietOrderQueryString ("DIET135", AppDelegate.Instance.InstitutionId, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.GetCommonDietOrderModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(CommonDietOrderModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			if (_model.comments != null) {
				int idx = 0;
				var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
				_dietComments.Clear ();
				_model.comments.ForEach (e => {
					_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : ", "", e, idx,this));
					idx++;
				});
			}
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.FullFeedsTitle));
			CreateSaveButton ();
			AddDeleteButton (result);
		}
		private void AddDeleteButton (CommonDietOrderModel result)
		{
			if (_interfaceStatus.DIET_ORDERS || result.profile_diet_order_id == null || !_callback.LockResult.free_to_access)
				return;
			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);
			if (!rightButtons.Contains (_deleteButton)) {
				_deleteButton.Clicked+= (sender, e) => {
					View.ShowConfirmAlert(AppContext, "FullFeeds", "Areyousurewanttodeactivatethis?", OnDeleteConfirmed);
				};
				rightButtons.Add (_deleteButton);
			}
			NavigationItem.SetRightBarButtonItems (rightButtons.ToArray (), false);
		}
		private void OnDeleteConfirmed ()
		{
			_requestHandler.SendRequest (View, () => DeleteModel ());
		}
		private async void Save(CommonDietOrderModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.SaveCommonDietOrder)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			GetData ();
		}
		private async void DeleteModel()
		{
			long profileDietOrderId = Convert.ToInt32 (_model.profile_diet_order_id);
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (result.success);
			if (!result.success)
				return;
			var message = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fullfeedshasbeendeactivated"));
			this.ShowAlert (AppContext, message);
			_callback.PopToViewControllerAndUnlock ();
		}
	}
}

