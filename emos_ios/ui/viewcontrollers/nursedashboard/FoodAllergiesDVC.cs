﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;

namespace emos_ios
{
	public class FoodAllergiesDVC : BaseDVC, ICallback<int>
	{
		public List<AlleryTypeWithAllergies> AllergiesTypes;

		private List<CheckboxGroupDVC<RestrictionAllergyModel>> _allergyTypesGroup;
		private List<Section> _allergyCheckboxes;
		private FoodAllergiesModel _model;
		private IPatientMenu _callback;
		private InterfaceStatusModel _interfaceModel;
		private List<KeyValuePair<string,string>> _patientAllergies;
		private List<KeyValuePair<string,string>> _patientRemarks;
		private LocationWithRegistrationSimple _location;
		private List<MultilineEntryElement> _allergiesRemarks;
		private Section _remarksSection;
		public CommonDietOrderModel FullFeed { get; set; }
		public CommonDietOrderModel ClearFeed { get; set; }

		public FoodAllergiesDVC (IPatientMenu controller, LocationWithRegistrationSimple location, InterfaceStatusModel interfaceModel) : base (AppDelegate.Instance)
		{
			this._interfaceModel = interfaceModel;
			_location = location;
			_callback = controller;
			_allergiesRemarks = new List<MultilineEntryElement> ();
			_patientAllergies = new List<KeyValuePair<string,string>> ();
			_patientRemarks = new List<KeyValuePair<string,string>> ();
		}
		public void Load()
		{
			GetData ();
		}
		public override void GetData()
		{
			if (_interfaceModel.FOOD_ALLERGIES)
				_requestHandler.SendRequest (View, GetPatientAllergies);
			else
				_requestHandler.SendRequest (View, GetTherapeuticModel);
		}

		private void CreateSaveButton()
		{
			if (!_callback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			if(_interfaceModel.FOOD_ALLERGIES!=true)
				NavigationItem.SetRightBarButtonItem (rightButton, false);
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			if (_interfaceModel.FOOD_ALLERGIES) {
//				var listElement = new List<EntryElementDVC> ();

				if (_patientAllergies != null && _patientAllergies.Count > 0) {
					var patientFoodAllergiesText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodAllergies");
					var allergyText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Allergy");
					var typeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Type");

					var allergySection = new Section (patientFoodAllergiesText);
					_patientAllergies.ForEach (r => allergySection.Add (new EntryElementDVC (allergyText + ": " + r.Key, "", typeText + ": " + r.Value, true)));
					Root.Add (allergySection);	

					if (_patientRemarks != null && _patientRemarks.Count > 0) {
						var remarksSection = new Section ();
						_patientRemarks.ForEach (r => remarksSection.Add (new EntryElementDVC (r.Key, "", "", true)));
						Root.Add (remarksSection);
					}
				} else {
					var patientFoodAllergiesText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Patientfoodallergies");
					var alertMsgText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Noallergieshasbeenadded");
					Root.Add (new Section (patientFoodAllergiesText) {
						new EntryElementDVC (alertMsgText, "", "", true)
					});
				}

			} else {
				_allergyTypesGroup = new List<CheckboxGroupDVC<RestrictionAllergyModel>> ();
				_allergyCheckboxes = new List<Section> ();

				_model.allAllergyTypes.Where (e => e.allergies.Count > 0)
				.ToList ()
				.ForEach (rt => {

					var checkboxGroup = new CheckboxGroupDVC<RestrictionAllergyModel> (rt.label, _model.currentAllergies, rt.allergies);
					_allergyTypesGroup.Add (checkboxGroup);
					_allergyCheckboxes.Add (checkboxGroup.Create ());
				});
				Root.Add (DvcGenerator.PatientInfoWithFeed (AppContext, _model.registration, Settings.QueryMealTypeWithFeeds, ClearFeed, FullFeed));
				CreateOthersSection ();
				Root.Add (_allergyCheckboxes);

				if (!String.IsNullOrEmpty (_model.allergiesRemark)) {
					int idx = 0;
					string[] remarksArray = _model.allergiesRemark.Split ('|');
					foreach (string str in remarksArray) {
						if (str == "")
							continue;

						_allergiesRemarks.Add (new MultilineEntryElement ("", "", str, idx, this));
						idx++;
					}
				}
				this.LoadRemarks ();
			}
		}
		private void CreateOthersSection ()
		{
			if (!Settings.ShowAdditionalPatientMenuLinks)
				return;
			var dietOrderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.Therapeutic);
			Root.Add (new Section (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Others")) {
				new StyledStringElement ("Meal Type", () =>  {
					_callback.MealType (_location, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
				new StyledStringElement (dietOrderText, () =>  {
					_callback.TherapeuticDietOrder(_location, ClearFeed, FullFeed, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
			});
		}
		private void AddNewDietComment()
		{
			_allergiesRemarks.Add (new MultilineEntryElement ("" , "", "",_allergiesRemarks.Count,this));

			Section _tempSection = _remarksSection;
			this.LoadRemarks ();
			Root.Remove (_tempSection);
		}

		public void ItemSelected(int index, int idx)
		{
		}

		private void RemoveComment(int index)
		{
			if (index < _allergiesRemarks.Count) {
				_allergiesRemarks.RemoveAt (index);

				for (int i = 0; i < _allergiesRemarks.Count; i++) {
					_allergiesRemarks [i].Index = i;
				}

				CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction"));
			}
		}

		private void LoadRemarks(){
		
			if (_allergiesRemarks.Count == 0 && !_interfaceModel.FOOD_ALLERGIES) {
				_allergiesRemarks.Add (new MultilineEntryElement ("", "", "", 0, this));
			}
			_remarksSection = new Section (AppContext.LanguageHandler.GetLocalizedString("Remarks"));
			_allergiesRemarks.ForEach (r => _remarksSection.Add (r));

			Root.Add (_remarksSection);
		}

		private void OnSave()
		{
			_model.allergiesRemark = "";

			int count = _allergiesRemarks.Count ();
			int i = 0;

			foreach (MultilineEntryElement e in _allergiesRemarks) {
				i ++;
				if(e.Value == ""){
					continue;
				}

				_model.allergiesRemark += e.Value;
				if(i != count)
					_model.allergiesRemark += '|';
			}
				
			var finalResult = new List<RestrictionAllergyModel> ();
			_allergyTypesGroup.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});
			_model.currentAllergies = finalResult.Distinct()
				.ToList();
			_model.allAllergyTypes = null;
			_model.location_id = _location.id;

			_requestHandler.SendRequest (View, ()=> Save (_model) );
		}

		private async void GetTherapeuticModel()
		{
			var content = KnownUrls.InstitutionAndRegistrationQueryString (AppDelegate.Instance.InstitutionId, _location.registrations.First ().registration_id);
			await AppDelegate.Instance.HttpSender.Request<FoodAllergiesModel> ()
				.From (KnownUrls.GetFoodAllergiesModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private async void GetPatientAllergies()
		{
			if (Settings.FoodAllergiesRemarkSpecialQuery) {
				var queryString = KnownUrls.GetPatientAllergiesQueryString ((long)_location.registrations.First ().registration_id);
				await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>> ()
					.From (KnownUrls.GetPatientAllergies)
					.WithQueryString (queryString)
					.WhenSuccess (result => OnRequestPatientAllergiesAndRemarkCompleted (result))
					.WhenFail (result => OnRequestCompleted (false))
					.Go ();
			} else {
				var queryString = KnownUrls.GetPatientAllergiesQueryString ((long)_location.registrations.First ().registration_id);
				await AppDelegate.Instance.HttpSender.Request<List<KeyValuePair<string,string>>> ()
					.From (KnownUrls.GetPatientAllergies)
					.WithQueryString (queryString)
					.WhenSuccess (result => OnRequestPatientAllergiesCompleted(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			}
		}

		private void OnRequestModelCompleted(FoodAllergiesModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodAllergies"));
			CreateSaveButton ();
		}

		private void OnRequestPatientAllergiesCompleted(List<KeyValuePair<string,string>> result)
		{
			OnRequestCompleted (true);
			_patientAllergies = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodAllergies"));
			CreateSaveButton ();
		}

		private void OnRequestPatientAllergiesAndRemarkCompleted(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			if (result != null) {
				int count = result.Count;

				if (count >= 2) {
					_patientAllergies = result [0];
					_patientRemarks = result [1];
				} else {
					if(count > 0)
						_patientAllergies = result [0];
				}
			}

			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodAllergies"));
			CreateSaveButton ();
		}
		private async void Save(FoodAllergiesModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<FoodAllergiesModel> ()
				.From (KnownUrls.SaveFoodAllergies)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
		}
		protected override void OnRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			base.OnRequestCompleted (success, failedTitleCode, showFailedTitle);
			_callback.ShouldRunViewWillAppear = true;
		}
	}
}

