// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("PatientLanguageViewController")]
	partial class PatientLanguageViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton languageButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView patientLanguageContentView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView patientLanguageTitleContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel patientSelectedLanguageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel selectedLanguageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView tickImageView { get; set; }

		[Action ("exitButtonTapped:")]
		partial void exitButtonTapped (MonoTouch.Foundation.NSObject sender);

		[Action ("languageButtonTapped:")]
		partial void languageButtonTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (languageButton != null) {
				languageButton.Dispose ();
				languageButton = null;
			}

			if (patientLanguageContentView != null) {
				patientLanguageContentView.Dispose ();
				patientLanguageContentView = null;
			}

			if (patientLanguageTitleContainerView != null) {
				patientLanguageTitleContainerView.Dispose ();
				patientLanguageTitleContainerView = null;
			}

			if (patientSelectedLanguageLabel != null) {
				patientSelectedLanguageLabel.Dispose ();
				patientSelectedLanguageLabel = null;
			}

			if (selectedLanguageLabel != null) {
				selectedLanguageLabel.Dispose ();
				selectedLanguageLabel = null;
			}

			if (tickImageView != null) {
				tickImageView.Dispose ();
				tickImageView = null;
			}
		}
	}
}
