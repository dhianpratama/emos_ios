﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class FluidRestrictionListViewController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> FluidRestrictionListDataSource;

		private InfoDataListHandler _fluidRestrictionListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private RegistrationModelSimple _registration;
		private InterfaceStatusModel _interfaceStatus;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }

		public FluidRestrictionListViewController (LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance, "FluidRestrictionListViewController", null)
		{
			_registration = locationWithRegistration.registrations.First ();
			_interfaceStatus = interfaceStatus;
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction");
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			FluidRestrictionTitleLabel.SetFrame (y: Measurements.TopY + 20);
			if (!Settings.ShowDVCTitle)
				FluidRestrictionTitleLabel.SetFrame (height: 0);
			FluidTable.SetFrameBelowTo (FluidRestrictionTitleLabel);
			_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
		}

		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			string addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				OnEdit(0);
			});
			addButton.TintColor = UIColor.White;

			if (_items == null) {
				if (!_interfaceStatus.DIET_ORDERS)
					NavigationItem.SetRightBarButtonItem (addButton, false);
			} else {
				if (_items.Count == 0) {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (addButton, false);
				} else {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (null, false);
				}
			}
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_fluidRestrictionListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			FluidTable.Hidden = false;
			FluidRestrictionListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_fluidRestrictionListViewCellHandler);
			FluidRestrictionListDataSource.Items = _items;
			FluidTable.Source = FluidRestrictionListDataSource;
			FluidTable.ReloadData ();
		}

		public void SetLayout()
		{
			FluidTable.ScrollEnabled = true;
			FluidTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			var dvc = new FluidRestrictionDVC (this, _registration.registration_id);
			AppContext.PushDVC (dvc);
		}

		public void OnDelete(long id)
		{
			var fluidRestrictionText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction");
			var fluidRestrictionDetailsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeletethisfluidrestriction?");
			this.ShowConfirmAlert (AppContext, fluidRestrictionText, fluidRestrictionDetailsText, OnDeleteConfirmed);
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestTherapeuticViewModel()
		{
			var queryString = KnownUrls.RegistrationIdQueryString ((long)_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>>()
				.From (KnownUrls.GetFluidRestrictionViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestTherapeuticModelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestTherapeuticModelSuccesful(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			_items = result;
			GenerateRightButton ();
			if (_items.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private async void DeleteProfileTherapeutic()
		{
			long profileDietOrderId = Convert.ToInt32( _items.First ().First (e => e.Key == "Id").Value );
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var alertMsg = String.Format(AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fluidrestriction'{0}'hasbeendeactivated"),
					_items.First ().First (e => e.Key == "Identifier").Value);
				this.ShowAlert (AppContext, alertMsg, translateMessage: false);
				_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
			} else
				OnRequestCompleted (false);
		}

		private void OnDeleteConfirmed()
		{
			_requestHandler.SendRequest (View, DeleteProfileTherapeutic);
		}

		private void ShowTable()
		{
			FluidTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			FluidTable.Hidden = true;
			NotFoundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			FluidTable.Hidden = true;
			NotFoundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			FluidRestrictionTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidRestriction");
			FluidRestrictionErrorMessageLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nofluidrestrictionisfound");
		}
	}
}

