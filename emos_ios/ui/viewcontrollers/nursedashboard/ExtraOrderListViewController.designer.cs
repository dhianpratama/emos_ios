// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ExtraOrderListViewController")]
	partial class ExtraOrderListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ExtraOrderErrorMsgLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ExtraOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView ExtraOrderListTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ExtraOrderErrorMsgLabel != null) {
				ExtraOrderErrorMsgLabel.Dispose ();
				ExtraOrderErrorMsgLabel = null;
			}

			if (ExtraOrderLabel != null) {
				ExtraOrderLabel.Dispose ();
				ExtraOrderLabel = null;
			}

			if (ExtraOrderListTable != null) {
				ExtraOrderListTable.Dispose ();
				ExtraOrderListTable = null;
			}

			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}
		}
	}
}
