﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.models;
using emos_ios.tools;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class ViewAdmissionDVC : BaseDVC
	{
		private ViewAdmissionModel _admissionModel;
		private long? registration_id;

		public ViewAdmissionDVC (LocationWithRegistrationSimple location) : base (AppDelegate.Instance)
		{
			Initialize (location);
		}
		private void Initialize (LocationWithRegistrationSimple location)
		{
			if (location.registrations.Count == 0) {
				this.ShowAlert (AppContext, "Nopatientfoundinthisbed");
				NavigationController.PopViewControllerAnimated (true);
				return;
			}

			this.registration_id = location.registrations.First().registration_id;
			GetData ();
		}

		public override void GetData() 
		{
			_requestHandler.SendRequest (View, RequestAdmissionModel);
		}

		private async void RequestAdmissionModel()
		{
			var queryString = KnownUrls.GetViewAdmissionQueryString ((long)this.registration_id);
			await AppDelegate.Instance.HttpSender.Request<ViewAdmissionModel> ()
				.From (KnownUrls.GetViewAdmission)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		public void OnRequestModelCompleted(ViewAdmissionModel result)
		{
			OnRequestCompleted (true);
			_admissionModel = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ViewAdmission"));
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var genderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Gender");
			var languageText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Language");
			var treatmentCategoryText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TreatmentCategory");
			var mealClassText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealClass");
			var raceText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Race");
			var patientNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientName");
			var identifierNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("IdentifierNumber");
			var birthdayText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Birthday");
			var admissionTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionTime");
			var caseNumberText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CaseNumber");
			var patientDataText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientData");
			var admissionInformationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("AdmissionInformation");
			var documentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Document");
			var salutationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Salutation");
			var bedText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Bed");

			Root.Add (new Section (patientDataText) {
				new EntryElementDVC (documentText, "", _admissionModel.documentType, true),
				new EntryElementDVC (identifierNumberText, "", _admissionModel.identifierNumber, true),
				new EntryElementDVC (salutationText, "", _admissionModel.salutation, true),
				new EntryElementDVC (patientNameText, "", _admissionModel.patientName, true),
				new EntryElementDVC (raceText, "", _admissionModel.race, true),
				new EntryElementDVC (genderText, "", _admissionModel.gender, true),
				new EntryElementDVC (birthdayText, "", _admissionModel.birthday, true),
				new EntryElementDVC (languageText, "", _admissionModel.language, true),
			});

			Root.Add (new Section (admissionInformationText) {
				new EntryElementDVC (admissionTimeText, "", _admissionModel.admissionTime, true),
				new EntryElementDVC (caseNumberText, "", _admissionModel.caseNumber, true),
				new EntryElementDVC (bedText, "", _admissionModel.bed, true),
				new EntryElementDVC (treatmentCategoryText, "", _admissionModel.treatmentCategory, true),
				new EntryElementDVC (mealClassText, "", _admissionModel.mealClass, true),
			});
		}

	}
}

