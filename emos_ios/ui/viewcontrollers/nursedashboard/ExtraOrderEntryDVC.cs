﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Globalization;

namespace emos_ios
{
	public partial class ExtraOrderEntryDVC : BaseDVC, IDatePickerCallback
	{
		public static string DateFormat = "dd/MM/yyyy hh:mm tt";
		public EntryElement _startDateElement { get; set; }
		public EntryElement _endDateElement { get; set; }
		private LocationWithRegistrationSimple _locationWithRegistrationSimple;
//		private IDatePickerCallback _callback;
		private UIViewController _view;
		public ExtraOrderModel Model { get; set; }

		public ExtraOrderEntryDVC (UIViewController view, LocationWithRegistrationSimple locationWithRegistrationSimple, ExtraOrderModel model) : base (AppDelegate.Instance, true)
		{
			_view = view;
			_locationWithRegistrationSimple = locationWithRegistrationSimple;
			var startDate = GetDefaultStartDate ();
			var endDate = GetDefaultEndDate (startDate);
			Model = model ?? new ExtraOrderModel {
				startTime = startDate,
				endTime = endDate
			};
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");

			_startDateElement = CreateStartDateElement (startDateText, Model.startTime, DateType.StartDate);
			_endDateElement = CreateEndDateElement (endDateText, Model.endTime, DateType.EndDate);
			CreateRoot ();
		}
		private static DateTime? GetDefaultStartDate ()
		{
			return DateTime.Now;
		}
		private static DateTime? GetDefaultEndDate (DateTime? startDate)
		{
			var aStartDate = startDate ?? GetDefaultStartDate ().Value;

			return aStartDate.AddDays (Settings.StartDateToEndDateDefaultDays);
		}
		private EntryElement CreateStartDateElement(string caption, DateTime? dateTime, DateType dateType)
		{
			var element = new EntryElementDVC (caption, "", 
				Model.startTime.Value.ToString());
			element.EntryStarted += delegate {
				OpenDatePicker (Model.startTime.Value.ToLocalTime(), dateType);
			};
			return element;
		}
		private EntryElement CreateEndDateElement(string caption, DateTime? dateTime, DateType dateType)
		{
			var element = new EntryElementDVC (caption, "", dateTime.Value.ToString ());
			element.EntryStarted += delegate {
				OpenDatePicker (Model.endTime, dateType);
			};
			return element;
		}
		public void LoadModel (ExtraOrderModel model)
		{
			Model = model;
			Model.startTime = model.startTime ?? GetDefaultStartDate ();
			Model.endTime = model.endTime ?? GetDefaultEndDate (Model.startTime);

			SetStartDate (Model.startTime);
			SetEndDate (Model.endTime);
		}
		private void SetStartDate(DateTime? date)
		{
			_startDateElement.Value = (Settings.UseDateForDiets)? date.Value.ToDisplayDateFormatInString() :
				date.Value.ToDisplayDateString();
			Model.startTime = date;
		}
		private void SetEndDate(DateTime? date)
		{
			_endDateElement.Value = (Settings.UseDateForDiets)? date.Value.ToDisplayDateFormatInString() :
				date.Value.ToDisplayDateString();
			Model.endTime = date;
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var registration = _locationWithRegistrationSimple.registrations.FirstOrDefault ();
			Root.Add (new Section (registration.profile_name) {
				_startDateElement,
				_endDateElement,
			});
		}
		public void OpenDatePicker(DateTime? date, DateType dateType)
		{
			var _patientMenuViewController = new DateTimePicker (AppContext, date, this, dateType, (Settings.UseDateForDiets)? UIDatePickerMode.Date : UIDatePickerMode.DateAndTime);
			_patientMenuViewController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			_patientMenuViewController.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
			_view.NavigationController.PresentViewController (_patientMenuViewController, true, null);
		}
		public void DoneDatePicker(DateTime? date, DateType dateType)
		{
			if (dateType == DateType.StartDate)
				SetStartDate (date);
			if (dateType == DateType.EndDate)
				SetEndDate (date);
		}
		public DateTime GetStartDate ()
		{
			try {
				return Model.startTime.Value.ToLocalTime();
			} catch {
				return DateTime.Now;
			}
		}
		public DateTime? GetEndDate ()
		{
			try {
				return Model.endTime.Value.ToLocalTime();
			} catch {
				return null;
			}
		}

		private void OpenActionSheetDatePicker()
		{
		}

		public void SetUserInteraction(bool _bool)
		{
//			this.View.Use
		}
	}
}
