﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;

namespace emos_ios
{
	public class ClearFeedsDVC : BaseDVC, ICallback<int>
	{
		private FormattedDateTimeElement _startDate, _endDate;
//		private RootElement _mealPeriodOptions;
		private CommonDietOrderModel _model;
		private long? _registrationId;
		private IPatientMenu _callback;
		private InterfaceStatusModel _interfaceStatus;

		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;
		private Section _dietCommentSection;
		private UIBarButtonItem _deleteButton = new UIBarButtonItem (UIBarButtonSystemItem.Trash);

		public ClearFeedsDVC (IPatientMenu controller, long? registrationId, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance)
		{
			this._interfaceStatus = interfaceStatus;
			this._registrationId = registrationId;
			_dietComments = new List<MultilineEntryElement> ();
			_callback = controller;
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetClearFeedsModel);
		}

		private void CreateSaveButton()
		{
			if (!_callback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}
			var rightButtonText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(rightButtonText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			if(_interfaceStatus.DIET_ORDERS!=true)	
				NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var clearFeedsInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Clearfeedsinfo");
			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");

			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			if (_interfaceStatus.DIET_ORDERS) {
				var startDate = _model.start_date ?? DateTime.UtcNow;
				if (_model.end_date == null)
					_model.end_date = startDate.AddDays (7);

				Root.Add (new Section (clearFeedsInfoText) {
					new EntryElementDVC (startDateText, "", startDate.ToDisplayDateString (), true),
					new EntryElementDVC (endDateText, "", _model.end_date.Value.ToDisplayDateString (), true),
				});

				if (_model.comments.Count > 0) {
					var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ClearFeedComments");
					_dietCommentSection = new Section (dietCommentsText) {
					};
					_model.comments.ForEach (r => _dietCommentSection.Add(new EntryElementDVC (r, "", "", true)));
					Root.Add (_dietCommentSection);
				}
			} else {
				var startDate = (DateTime)(_model.start_date ?? DateTime.UtcNow);
				_startDate = new FormattedDateTimeElement (startDateText, startDate.ToDisplayDatetime ());
				if (_model.end_date == null)
					_model.end_date = startDate.AddDays (7);
				_endDate = new FormattedDateTimeElement (endDateText, ((DateTime)_model.end_date).ToDisplayDatetime ());

				Root.Add (new Section (clearFeedsInfoText) {
					_startDate,
					_endDate,
				});
				_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
					AddNewDietComment ();
				}) {
					BackgroundColor = UIColor.LightGray,
					TextColor = UIColor.DarkGray
				};
				this.LoadCommentSection ();
			}
		}
		private void AddNewDietComment()
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (commentText + ": " , "", "",_dietComments.Count,this));
			Section _tempSection = _dietCommentSection;
			this.LoadCommentSection ();
			Root.Remove (_tempSection);
		}

		private void LoadCommentSection()
		{
			var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ClearFeedComments");
			_dietCommentSection = new Section (dietCommentsText) {
				_addCommentButton,
			};
			_dietComments.ForEach(r => _dietCommentSection.Add(r));

			Root.Add (_dietCommentSection);
		}
		public void ItemSelected(int idx, int index)
		{
		}
		private void OnSave()
		{
			_model.start_date = _startDate.DateValue;
			_model.end_date = _endDate.DateValue;
			_model.comments.Clear ();
			foreach (MultilineEntryElement e in _dietComments) {
				if (e.Value == "")
					continue;

				_model.comments.Add(e.Value);
			}
			_requestHandler.SendRequest (View, ()=> Save (_model));
		}

		private async void GetClearFeedsModel()
		{
			var content = KnownUrls.GetCommonDietOrderQueryString ("DIET136", AppDelegate.Instance.InstitutionId, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.GetCommonDietOrderModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(CommonDietOrderModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			if (_model.comments != null) {
				int idx = 0;
				var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
				_dietComments.Clear ();
				_model.comments.ForEach (e => {
					_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : ", "", e, idx,this));
					idx++;
				});
			}

			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.ClearFeedsTitle));
			CreateSaveButton ();
			AddDeleteButton (result);
		}
		private void AddDeleteButton (CommonDietOrderModel result)
		{
			if (_interfaceStatus.DIET_ORDERS || result.profile_diet_order_id == null || !_callback.LockResult.free_to_access)
				return;

			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);
			if (!rightButtons.Contains (_deleteButton)) {
				_deleteButton.Clicked+= (sender, e) => {
					View.ShowConfirmAlert(AppContext, "ClearFeeds", "Areyousurewanttodeactivatethis?", OnDeleteConfirmed);
				};
				rightButtons.Add (_deleteButton);
			}
			NavigationItem.SetRightBarButtonItems (rightButtons.ToArray (), false);
		}
		private void OnDeleteConfirmed ()
		{
			_requestHandler.SendRequest (View, () => DeleteModel ());
		}
		private async void Save(CommonDietOrderModel model)
		{
			string saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SaveClearFeeds");
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderModel> ()
				.From (KnownUrls.SaveCommonDietOrder)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false,saveText,true))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			GetData ();
		}
		private async void DeleteModel()
		{
			long profileDietOrderId = Convert.ToInt32 (_model.profile_diet_order_id);
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			OnRequestCompleted (result.success);
			if (!result.success)
				return;
			var message = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Clearfeedshasbeendeactivated"));
			this.ShowAlert (AppContext, message);
			_callback.PopToViewControllerAndUnlock ();
		}
	}
}

