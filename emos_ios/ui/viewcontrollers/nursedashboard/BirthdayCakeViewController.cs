﻿
using System;
using System.Drawing;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Globalization;

namespace emos_ios
{
	[Register("BirthdayCakeViewController")]
	public partial class BirthdayCakeViewController : BaseViewController
	{
		private IPatientDayOrderCallback _callback;
		private LocationWithRegistrationSimple _locationWithRegistration;
		private BirthdayCakeModelIpad _saveModel;
		private RegistrationModelSimple _registration;

		public BirthdayCakeViewController(LocationWithRegistrationSimple locationWithRegistration, BirthdayCakeModelIpad model, IPatientDayOrderCallback callback): base (AppDelegate.Instance, "BirthdayCakeViewController", null)
		{
			_callback = callback;
			_locationWithRegistration = locationWithRegistration;
			_registration = locationWithRegistration.registrations.First ();
			HiddenLogo = true;
			if (model.profile_birthday_cake_id == 0) {
				_saveModel = new BirthdayCakeModelIpad () {
					profile_birthday_cake_id = 0,
					registration_id = _locationWithRegistration.registrations.First ().registration_id,
					institution_id = AppDelegate.Instance.InstitutionId,
					for_birthday_date = _locationWithRegistration.registrations.First ().birthday,
					order_time = DateTime.Now,
					profile_id = _locationWithRegistration.registrations.First ().profile_id,
					remark = "",
					mealOrderPeriods = model.mealOrderPeriods
				};
			} else {
				_saveModel = model;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			CancelOrderButton.Layer.CornerRadius = 10;
			CancelOrderButton.BackgroundColor = AppContext.ColorHandler.MainThemeColor;

			OrderButton.Layer.CornerRadius = 10;
			OrderButton.BackgroundColor = AppContext.ColorHandler.MainThemeColor;
			CakeImage.Image = UIImage.FromBundle ("Images/cake.jpg");

			if (_saveModel.profile_birthday_cake_id == 0) {
				OrderButton.Hidden = false;
				CancelOrderButton.Hidden = true;
			} else {
				OrderButton.Hidden = true;
				CancelOrderButton.Hidden = false;
			}
			CultureInfo languageCode = new CultureInfo(AppDelegate.Instance.LanguageHandler.LanguageCode);
			DateLabel.Text = _locationWithRegistration.registrations.First ().birthday.Value.ToString ("dd/MM/yyyy", languageCode);
			SetMealPeriodSegment ();
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 521, 336);
		}

		private void SetMealPeriodSegment()
		{
			MealPeriodSegment.RemoveAllSegments ();
			for (int i = 0; i < _saveModel.mealOrderPeriods.Count; i++) {
				if (Settings.UseMealPeriodAsBirthdayCakeSegment) {
					MealPeriodSegment.InsertSegment (_saveModel.mealOrderPeriods [i].label, i, true);
				} else {
					var key = String.Format ("BirthdayPeriod{0}", i);
					MealPeriodSegment.InsertSegment (AppContext.LanguageHandler.GetLocalizedString (key), 0, true);
				}
			}

			if (_saveModel.meal_order_period_id == null) {
				if (_saveModel.mealOrderPeriods.Count > 0) {
					MealPeriodSegment.SelectedSegment = 0;
					MealPeriodLabel.Text = _saveModel.mealOrderPeriods.First ().label;
					_saveModel.meal_order_period_id = _saveModel.mealOrderPeriods.First ().id;
				}
			} else {
				for (int i = 0; i < _saveModel.mealOrderPeriods.Count; i++) {
					if (_saveModel.mealOrderPeriods [i].id == _saveModel.meal_order_period_id) {
						MealPeriodSegment.SelectedSegment = i;
						MealPeriodLabel.Text = _saveModel.mealOrderPeriods [i].label;
					}
				}
			}
		}

		partial void OrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_requestHandler.SendRequest (View, SaveBirthdayCake);
		}

		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_callback.CloseBirthdayCakeForm();
		}

		private async void SaveBirthdayCake()
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveBirthdayCake)
				.WithContent(_saveModel)
				.WhenSuccess (result => OnSaveSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveSuccesful(OperationInfoIpad result)
		{
			if (!result.success) {
				OnRequestCompleted (false);
			} else {
				OnRequestCompleted (true);
			}
			_callback.CloseBirthdayCakeForm ();
			_callback.Refresh ();
		}

		partial void CancelOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Birthdaycake");
			var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Areyousurewanttocancelbirthdaycake?");
			this.ShowConfirmAlert(AppContext, alertMsg1, alertMsg2,ProceedCancel);
		}

		private void ProceedCancel()
		{
			_requestHandler.SendRequest (View, CancelBirthdayCake);
		}

		public async void CancelBirthdayCake()
		{
			var queryString = KnownUrls.CancelBirthdayCakeQueryString ((int)_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.CancelBirthdayCake)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnCancelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnCancelSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				_callback.CloseBirthdayCakeForm ();
				_callback.Refresh ();
			} else {
				OnRequestCompleted (false);
			}
		}

		partial void MealPeriodSegment_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			var selectedIndex = MealPeriodSegment.SelectedSegment;

			MealPeriodLabel.Text = _saveModel.mealOrderPeriods[selectedIndex].label;
			_saveModel.meal_order_period_id = _saveModel.mealOrderPeriods[selectedIndex].id;
		}
		public override void SetTextsByLanguage(){
			BirthdayCakeLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("BirthdayCake");
			BirthdayLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Birthday") + ":";
			OrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Order"), UIControlState.Normal);
			CancelOrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CancelOrder"), UIControlState.Normal);
		}
	}
}

