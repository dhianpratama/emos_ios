// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("TrialOrderListViewController")]
	partial class TrialOrderListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TrialOrderErrorMsgLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView TrialOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TrialOrderTitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}

			if (TrialOrderErrorMsgLabel != null) {
				TrialOrderErrorMsgLabel.Dispose ();
				TrialOrderErrorMsgLabel = null;
			}

			if (TrialOrderTable != null) {
				TrialOrderTable.Dispose ();
				TrialOrderTable = null;
			}

			if (TrialOrderTitleLabel != null) {
				TrialOrderTitleLabel.Dispose ();
				TrialOrderTitleLabel = null;
			}
		}
	}
}
