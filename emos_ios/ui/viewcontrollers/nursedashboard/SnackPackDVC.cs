﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using ios;

namespace emos_ios
{
	public class SnackPackDVC : BaseDVC
	{
		private FormattedDateTimeElement _startDate;
		private SnackPackModel _model;
		private long? _registrationId;
		private ICommonDataListCallback _callback;

		public SnackPackDVC (ICommonDataListCallback controller, long? registrationId) : base (AppDelegate.Instance)
		{
			this._registrationId = registrationId;
			_callback = controller;
			GetData ();
			CreateSaveButton ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetSnackPackModel);
		}

		private void CreateSaveButton()
		{
			var saveButton = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveButton, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot ();
			var leaveTimeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Leavetime");
			var snackPackInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Snackpackinfo");
			var identifierText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Identifier");

			_startDate = new FormattedDateTimeElement (leaveTimeText, (DateTime)( _model.start_date ?? DateTime.Now.ToDisplayDatetime ()));

			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			Root.Add (new Section (snackPackInfoText) {
				new EntryElementDVC (identifierText, "", _model.identifier_number, true),
				_startDate
			});
		}

		private void OnSave()
		{
			_model.start_date = DateTime.SpecifyKind( _startDate.DateValue, DateTimeKind.Utc);
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetSnackPackModel()
		{
			var content = KnownUrls.InstitutionAndRegistrationQueryString (AppDelegate.Instance.InstitutionId, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<SnackPackModel> ()
				.From (KnownUrls.GetSnackPackModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(SnackPackModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SnackPack[DNS150]"));
		}

		private async void Save(SnackPackModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<SnackPackModel> ()
				.From (KnownUrls.SaveSnackPack)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			this.ShowAlert (AppContext, "Savedsuccessful");
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
	}
}

