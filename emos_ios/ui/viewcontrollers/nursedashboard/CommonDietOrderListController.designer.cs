// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CommonDietOrderListController")]
	partial class CommonDietOrderListController
	{
		[Outlet]
		MonoTouch.UIKit.UIView CommonDietBackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CommonDietInfoLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView CommonDietsListTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CommonDietTitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CommonDietsListTable != null) {
				CommonDietsListTable.Dispose ();
				CommonDietsListTable = null;
			}

			if (CommonDietTitleLabel != null) {
				CommonDietTitleLabel.Dispose ();
				CommonDietTitleLabel = null;
			}

			if (CommonDietBackgroundView != null) {
				CommonDietBackgroundView.Dispose ();
				CommonDietBackgroundView = null;
			}

			if (CommonDietInfoLabel != null) {
				CommonDietInfoLabel.Dispose ();
				CommonDietInfoLabel = null;
			}
		}
	}
}
