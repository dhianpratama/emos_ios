﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public partial class CommonDietOrderListController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> TherapeuticListDataSource;

		private InfoDataListHandler _commonDietsListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private InterfaceStatusModel _interfaceStatus;
		private LocationWithRegistrationSimple _location;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }
		private string _dietCode;
		private CommonDietsType _diet;

		public CommonDietOrderListController (IPatientMenu patientMenuCallback, LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus, string title, string dietCode, CommonDietsType dietType) : base (AppDelegate.Instance, "CommonDietOrderListController", null)
		{
			_location = locationWithRegistration;
			_interfaceStatus = interfaceStatus;
			PatientMenuCallback = patientMenuCallback;
			Title = title;
			_dietCode = dietCode;
			_diet = dietType;
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			CommonDietTitleLabel.SetFrame (y: Measurements.TopY + 20);
			if (!Settings.ShowDVCTitle)
				CommonDietTitleLabel.SetFrame (height: 0);
			if (Settings.ShowAdditionalPatientMenuLinks) {
				var headerDVC = new CommonDietHeaderDVC (AppContext, _location, Title);
				headerDVC.View.SetFrameBelowTo (CommonDietTitleLabel, 0, 768, 260, 0);
				View.AddSubview (headerDVC.View);
				CommonDietsListTable.SetFrame (0, 380, 768, 1024-380);
			} else {
				CommonDietsListTable.SetFrameBelowTo (CommonDietTitleLabel, height: Measurements.TherapeuticListTableHeight, distanceToAbove: 0);
			}
			PatientMenuCallback.ShouldRunViewWillAppear = true;
			_requestHandler.SendRequest (View, RequestCommonDietViewModel);
		}
		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}
			var addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				AddNew();
			});
			addButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (addButton, false);
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_commonDietsListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			CommonDietsListTable .Hidden = false;
			TherapeuticListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_commonDietsListViewCellHandler);
			TherapeuticListDataSource.Items = _items;
			CommonDietsListTable.Source = TherapeuticListDataSource;
			CommonDietsListTable.ReloadData ();
		}

		public void SetLayout()
		{
			CommonDietsListTable.ScrollEnabled = true;
			CommonDietsListTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			switch (_diet) 
			{
			case CommonDietsType.NBM:
				{
					var dvc = new NilByMouthV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, id, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			case CommonDietsType.ClearFeeds:
				{
					var dvc = new ClearFeedsV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, id, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			case CommonDietsType.FullFeeds:
				{
					var dvc = new FullFeedsV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, id, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			}
		}
		public void AddNew ()
		{
			switch (_diet) 
			{
			case CommonDietsType.NBM:
				{
					var dvc = new NilByMouthV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, null, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			case CommonDietsType.ClearFeeds:
				{
					var dvc = new ClearFeedsV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, null, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			case CommonDietsType.FullFeeds:
				{
					var dvc = new FullFeedsV2DVC ((ICommonDataListCallback)this, _location.registrations.First ().registration_id, null, _interfaceStatus, AppDelegate.Instance);
					AppContext.PushDVC (dvc);
					break;
				}
			}
		}
		public void OnDelete(long id)
		{
			var alertMsg1 = Title;
			var text = "";
			switch (_diet) 
			{
			case CommonDietsType.NBM:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatedthisnbm?");
				break;
			case CommonDietsType.ClearFeeds:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatedthisclearfeed?");
				break;
			case CommonDietsType.FullFeeds:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatedthisfullfeed?");
				break;
			}
			this.ShowConfirmAlert (AppContext, alertMsg1, text, () => OnDeleteConfirmed (id));
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestCommonDietViewModel()
		{
			var queryString = KnownUrls.GetCommonDietOrderQueryString (_dietCode, AppContext.InstitutionId, (long)_location.registrations.First ().registration_id);
			await AppDelegate.Instance.HttpSender.Request<CommonDietOrderListModel>()
				.From (KnownUrls.GetCommonDietOrderListViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestCommonDietListModelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestCommonDietListModelSuccesful(CommonDietOrderListModel result)
		{
			OnRequestCompleted (true);
			_items = result.values;
			GenerateRightButton ();
			if (_items.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private async void DeleteProfileTherapeutic(long profileDietOrderId)
		{
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result, profileDietOrderId))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result, long profileDietOrderId)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var text = "";
				switch (_diet) 
				{
				case CommonDietsType.NBM:
					text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nbmhasbeendeactivated");
					break;
				case CommonDietsType.ClearFeeds:
					text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Clearfeedshasbeendeactivated");
					break;
				case CommonDietsType.FullFeeds:
					text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Fullfeedshasbeendeactivated");
					break;
				}

				this.ShowAlert (AppContext, text, translateMessage: false);
				_requestHandler.SendRequest (View, RequestCommonDietViewModel);
			} else
				OnRequestCompleted (false);
		}

		private void OnDeleteConfirmed(long profileDietOrderId)
		{
			_requestHandler.SendRequest (View, () => DeleteProfileTherapeutic (profileDietOrderId));
		}

		private void ShowTable()
		{
			CommonDietsListTable.Hidden = false;
			CommonDietBackgroundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			CommonDietsListTable.Hidden = true;
			CommonDietBackgroundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			CommonDietsListTable.Hidden = true;
			CommonDietBackgroundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestCommonDietViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			CommonDietTitleLabel.Text = Title;
			var text = "";
			switch (_diet) 
			{
			case CommonDietsType.NBM:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nonbmisfound");
				break;
			case CommonDietsType.ClearFeeds:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Noclearfeedsisfound");
				break;
			case CommonDietsType.FullFeeds:
				text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nofullfeedsisfound");
				break;
			}

			CommonDietInfoLabel.Text = text;
		}
	}
}

