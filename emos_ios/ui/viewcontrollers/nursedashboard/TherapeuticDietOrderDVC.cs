﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using ios;
using core_emos;
using RaEngine.Service;

namespace emos_ios
{
	public class TherapeuticDietOrderDVC : BaseDVC, ICallback<int>
	{
		public List<RestrictionTypeWithRestriction> RestrictionTypes;
		private List<CheckboxGroupDVC<RestrictionModelSimple>> _restrictionTypeGroups;
		private List<Section> _restrictionCheckboxes;
		private FormattedDateTimeElement _startDate, _endDate;
		private TherapeuticDietOrderModel _model;
		private long? _registrationId;
		private ICommonDataListCallback _callback;
		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;
		private bool isPrompting;
		private Section _dietCommentSection;
		private bool _isNew;
		long? _profileDietOrderId;

		public TherapeuticDietOrderDVC (ICommonDataListCallback controller, long? registrationId, bool isNew = false, long? profileDietOrderId = null) : base (AppDelegate.Instance)
		{
			this._registrationId = registrationId;
			_callback = controller;
			_isNew = isNew;
			_profileDietOrderId = profileDietOrderId;
			_dietComments = new List<MultilineEntryElement> ();
			GetData ();
		}

		public override void GetData()
		{
			if (_isNew)
				_requestHandler.SendRequest (View, AddTherapeutic);
			else
				_requestHandler.SendRequest (View, GetTherapeuticModelById);
		}

		private void CreateSaveButton()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");

			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");
			var dietCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("DietComments");
			var periodText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Period");

			_restrictionTypeGroups = new List<CheckboxGroupDVC<RestrictionModelSimple>> ();
			_restrictionCheckboxes = new List<Section> ();
			_model.allRestrictionTypes.Where (e => e.restrictions.Count > 0)
				.ToList ()
				.ForEach (rt => {

				var checkboxGroup = new CheckboxGroupDVC<RestrictionModelSimple> (rt.label, _model.currentRestrictions, rt.restrictions);
				_restrictionTypeGroups.Add (checkboxGroup);
				_restrictionCheckboxes.Add (checkboxGroup.Create ());
				checkboxGroup.OnCheckboxSelected += OnRestrictionSelected;
			});
			_startDate = new FormattedDateTimeElement (startDateText, (DateTime)(_model.start_date ?? DateTime.UtcNow.ToDisplayDatetime ()));
			_endDate = new FormattedDateTimeElement (endDateText, (DateTime)(_model.end_date ?? DateTime.UtcNow.AddDays (Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ()));

			_startDate.DateSelected+= delegate {
				DateSelected();
			};
			_endDate.DateSelected += delegate {
				DateSelected ();
			};
			_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
				AddNewDietComment();
			}){
				BackgroundColor = UIColor.LightGray,
				TextColor = UIColor.DarkGray
			};

			_dietCommentSection = new Section (dietCommentText);
			_dietCommentSection.Add (_addCommentButton);
			_dietComments.ForEach (r => _dietCommentSection.Add (r));

			Root.Add (DvcGenerator.PatientInfoShort (AppContext, _model.registration));
			Root.Add (new Section (periodText) {
				_startDate,
				_endDate
			});

			if(Settings.ShowTherapeuticComment)
				Root.Add (_dietCommentSection);
			
			Root.Add (_restrictionCheckboxes);
		}
		private void OnRestrictionSelected (object sender, ios.IdEventArgs e)
		{
			if (isPrompting)
				return;
			var booleanElement = (BooleanElement)sender;
			if (!booleanElement.Value)
				return;
			var checkbox = _restrictionTypeGroups.First (r => r.Items.Any (i => i.id == e.Id));
			CheckCollision (e, booleanElement, checkbox);
		}
		private void CheckCollision (ios.IdEventArgs e, BooleanElement booleanElement, CheckboxGroupDVC<RestrictionModelSimple> checkbox)
		{
			var restriction = _model.allRestrictionTypes.First ().restrictions.FirstOrDefault (r => r.id == e.Id);
			if (restriction == null) {
				restriction = _model.allRestrictionTypes [1].restrictions.FirstOrDefault (r => r.id == e.Id);
				if (restriction == null) {
					return;
				}
			}
			var collisionIndexes = GetCollisionIndexes (checkbox, restriction);
			if (!collisionIndexes.Any ())
				return;
			isPrompting = true;
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Confirmation");
			var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CollidedTherapeutic");
			collisionIndexes.ForEach (ci => {
				alertMsg2 = String.Format ("{0}\n{1}", alertMsg2, checkbox.GetLabel (ci));
			});
			var booleanElementIndex = GetCheckboxItemIndex (checkbox, e.Id);
			this.ShowConfirmAlert (AppContext, alertMsg1, alertMsg2, () => ClearCheckboxes (checkbox, collisionIndexes), actionCancel: () => CancelRestrictionValueChange (checkbox, booleanElementIndex));
		}
		private List<int> GetCollisionIndexes (CheckboxGroupDVC<RestrictionModelSimple> checkbox, RestrictionModelSimple restriction)
		{
			var collisionIndexes = new List<int> ();
			var collidedIds = restriction.collided_ids;
			foreach (var collidedId in collidedIds) {
				var collidedIndex = GetCheckboxItemIndex (checkbox, collidedId);
				if (checkbox.GetOnOff (collidedIndex)) {
					collisionIndexes.Add (collidedIndex);
				}
			}
			return collisionIndexes;
		}
		private void ClearCheckboxes(CheckboxGroupDVC<RestrictionModelSimple> checkbox, List<int> indexes)
		{
			indexes.ForEach (i => checkbox.ChangeState (i, false));
			isPrompting = false;
		}
		private void CancelRestrictionValueChange (CheckboxGroupDVC<RestrictionModelSimple> checkbox, int booleanElementIndex)
		{
			checkbox.ChangeState (booleanElementIndex, false);
			isPrompting = false;
		}
		private int GetCheckboxItemIndex(CheckboxGroupDVC<RestrictionModelSimple> checkbox, long? id)
		{
			for (int i = 0; i < checkbox.Items.Count; i++) {
				if (checkbox.Items [i].id == id) {
					return i;
				}
			}
			return -1;
		}
		private void AddNewDietComment()
		{
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : " , "", "",_dietComments.Count,this));
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therapeutic"));
		}

		private void OnSave()
		{
			var finalResult = new List<RestrictionModelSimple> ();
			_restrictionTypeGroups.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});
			if (Settings.FoodTextureIsCompulsory && FoodTextureNotSelected (finalResult)) {
				this.ShowAlert (AppContext, "Pleasesetfoodtexture");
				return;
			}
			_model.comments.Clear ();
			_dietComments.ForEach (e => {
				_model.comments.Add(e.Value);
			});

			_model.currentRestrictions = finalResult.Distinct()
				.ToList();
			_model.allRestrictionTypes = null;
			_model.start_date = _startDate.DateValue.ToDisplayDatetime ();
			_model.end_date = _endDate.DateValue.ToDisplayDatetime ();
			_model.registration.registration_id = _registrationId;

			_requestHandler.SendRequest (View, () => Save (_model));
		}
		private bool FoodTextureNotSelected (List<RestrictionModelSimple> selections)
		{
			var foodTextures = _model.allRestrictionTypes.Where(r => String.Equals (r.code, "Food Texture", StringComparison.InvariantCultureIgnoreCase)).SelectMany (r => r.restrictions);
			foreach (var selected in selections) {
				var foodTexture = foodTextures.FirstOrDefault (t => t.id == selected.id);
				if (foodTexture != null)
					return false;
			}
			return true;
		}
		private async void GetTherapeuticModelById()
		{
			var content = KnownUrls.GetTherapeuticModelByIdQueryString (AppContext.ServerConfig.InsitutionId, this._registrationId, _profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<TherapeuticDietOrderModel> ()
				.From (KnownUrls.GetTherapeuticModelById)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private async void AddTherapeutic()
		{
			var content = KnownUrls.InstitutionAndRegistrationQueryString (AppContext.ServerConfig.InsitutionId, this._registrationId);
			await AppDelegate.Instance.HttpSender.Request<TherapeuticDietOrderModel> ()
				.From (KnownUrls.AddTherapeutic)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnRequestModelCompleted(TherapeuticDietOrderModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			CreateComments ();
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therapeutic"));
			CreateSaveButton ();
		}
		private void CreateComments ()
		{
			if (_isNew)
				return;
			int idx = 0;
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_model.comments.ForEach (e =>  {
				_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : ", "", e, idx, this));
				idx++;
			});
		}
		private async void Save(TherapeuticDietOrderModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveTherapeuticDietOrder)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success)
				this.ShowAlert (AppContext, "Savedsuccessful");
			else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}

		public void ItemSelected(int idx, int index)
		{
			var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Doyouwanttodeletethiscomment");
			this.ShowConfirmAlert (AppContext, alertMsg1, alertMsg2, ()=>RemoveComment(idx));
		}

		private void RemoveComment(int index)
		{
			if (index < _dietComments.Count) {
				_dietComments.RemoveAt (index);

				for (int i = 0; i < _dietComments.Count; i++) {
					_dietComments [i].Index = i;
				}

				CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therapeutic"));
			}
		}
		private void DateSelected()
		{
			_model.start_date = _startDate.DateValue;
			_model.end_date = _endDate.DateValue;
		}
	}
}

