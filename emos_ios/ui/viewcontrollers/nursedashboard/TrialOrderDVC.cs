﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using ios;

namespace emos_ios
{
	public class TrialOrderDVC : BaseDVC, ICallback<int>
	{
		private FormattedDateTimeElement _startDateTime, _endDateTime;
		private FormattedDateElement _startDate, _endDate;
		private RootElement _mealPeriodOptions; 
		private RootElement _foodFeding, _fluidFeeding, _fluidConsistency;
		private ICommonDataListCallback _callback;
		private TrialOrderModel _model;
		private long? _registrationId;
		private List<KeyValuePairRadioGroupDVC<BaseIpadModel>> _dishTypeSectionList = new List<KeyValuePairRadioGroupDVC<BaseIpadModel>>();

		private List<MultilineEntryElement> _dietComments;
		private StyledStringElement _addCommentButton;
		private Section _dietCommentSection;

		public TrialOrderDVC (ICommonDataListCallback controller, long? registrationId) : base (AppDelegate.Instance)
		{
			this._registrationId = registrationId;
			_callback = controller;
			_dietComments = new List<MultilineEntryElement> ();
			GetData ();
		}

		public override void GetData()
		{
			_requestHandler.SendRequest (View, GetTrialModel);
		}

		private void CreateSaveButton()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var startDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Startdate");
			var endDateText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Enddate");
			var foodModeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodModeofFeeding");
			var fluidConsistencyText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidConsistency");
			var fluidModeofFeedingText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FluidModeofFeeding");
			var trialOrderOptionText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrderOptions");

			var newCommentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NewComment");

			if (Settings.UseDateForDiets) {
				_startDate = new FormattedDateElement (startDateText, (DateTime)(_model.startDate??DateTime.Now));
				if (_model.endDate == null) {
					_model.endDate = DateTime.Now.ToDisplayDatetime ();
				}
				_endDate = new FormattedDateElement (endDateText, (DateTime)_model.endDate);
			} else {
				_startDateTime = new FormattedDateTimeElement (startDateText, (DateTime)(_model.startDate??DateTime.Now));
				if (_model.endDate == null) {
					_model.endDate = DateTime.Now.AddDays (Settings.StartDateToEndDateDefaultDays).ToDisplayDatetime ();
				}
				_endDateTime = new FormattedDateTimeElement (endDateText, (DateTime)_model.endDate);
			}

			_mealPeriodOptions = new RadioGroupDVC<BaseIpadModel> ()
				.CreateRadioGroup("Meal order period", _model.mealPeriodId, _model.allMealPeriods);

			var foodFeedings = CopyAndAddDefault (_model.foodFeedingList);
			_foodFeding = new RadioGroupDVC<BaseIpadModel> ()
				.CreateRadioGroup(foodModeText, _model.foodModeOfFeeding.restrictionId, foodFeedings);

			var fluidConsistencies = CopyAndAddDefault (_model.fluidConsistencyList);
			_fluidConsistency = new RadioGroupDVC<BaseIpadModel> ()
				.CreateRadioGroup(fluidConsistencyText, _model.fluidConsistency.restrictionId, fluidConsistencies);

			var fluidFeedings = CopyAndAddDefault (_model.fluidFeedingList);
			_fluidFeeding = new RadioGroupDVC<BaseIpadModel> ()
				.CreateRadioGroup(fluidModeofFeedingText, _model.fluidModeOfFeeding.restrictionId, fluidFeedings);

			if (Settings.UseDateForDiets) {
				Root.Add (new Section (trialOrderOptionText) {
					_startDate,
					_endDate,
					_mealPeriodOptions
				});
			} else {
				Root.Add (new Section (trialOrderOptionText) {
					_startDateTime,
					_endDateTime,
					_mealPeriodOptions
				});
			}

			Root.Add (from a in _dishTypeSectionList
			          select (Section)a);
			if (!Settings.SimpleTrialOrder) {
				Root.Add (new Section (foodModeText) {
					_foodFeding
				});
				Root.Add (new Section (fluidConsistencyText) {
					_fluidConsistency
				});
				Root.Add (new Section (fluidModeofFeedingText) {
					_fluidFeeding
				});
			}

			if (Settings.ShowOtherDietsCommentField) {
				_addCommentButton = new StyledStringElement ("+ " + newCommentText, delegate {
					AddNewDietComment();
				}){
					BackgroundColor = UIColor.LightGray,
					TextColor = UIColor.DarkGray
				};

				this.LoadCommentSection ();
			}
		}
		private List<BaseIpadModel> CopyAndAddDefault (List<BaseIpadModel> values)
		{
			var results = new List<BaseIpadModel> {
				new BaseIpadModel {
					id = 0,
					code = "",
					label = "-- None --",
					active_data = true
				}
			};
			values.ForEach (v => {
				results.Add (new BaseIpadModel {
					id = v.id,
					code = v.code,
					label = v.label,
					active_data = v.active_data
				});
			});
			return results;
		}
		private void AddNewDietComment()
		{
			var commentText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
			_dietComments.Add (new MultilineEntryElement (commentText + ": " , "", "",_dietComments.Count,this));
			Section _tempSection = _dietCommentSection;
			this.LoadCommentSection ();
			Root.Remove (_tempSection);
		}

		private void LoadCommentSection()
		{
			var dietCommentsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrderComments");
			_dietCommentSection = new Section (dietCommentsText);
			_dietCommentSection.Add(_addCommentButton);
			_dietComments.ForEach(r => _dietCommentSection.Add(r));
			Root.Add (_dietCommentSection);
		}
		public void ItemSelected(int idx, int index)
		{
		}
		private void OnSave()
		{
			_model.comments.Clear ();
			foreach (MultilineEntryElement e in _dietComments) {
				if (e.Value == "")
					continue;

				_model.comments.Add(e.Value);
			}

			_model.startDate = (Settings.UseDateForDiets)? _startDate.DateValue.Date.ToDisplayDatetime() : _startDateTime.DateValue.ToDisplayDatetime ();
			_model.endDate = (Settings.UseDateForDiets)? _endDate.DateValue.Date.ToDisplayDatetime() : _endDateTime.DateValue.ToDisplayDatetime();
			_model.mealPeriodId = _model.allMealPeriods [_mealPeriodOptions.RadioSelected].id;
			if (!Settings.SimpleTrialOrder) {
				_model.foodModeOfFeeding.restrictionId = _foodFeding.RadioSelected == 0 ? null : _model.foodFeedingList [_foodFeding.RadioSelected - 1].id;
				_model.fluidConsistency.restrictionId = _fluidConsistency.RadioSelected == 0 ? null : _model.fluidConsistencyList [_fluidConsistency.RadioSelected - 1].id;
				_model.fluidModeOfFeeding.restrictionId = _fluidFeeding.RadioSelected == 0 ? null : _model.fluidFeedingList [_fluidFeeding.RadioSelected - 1].id;
			}
			for (int i = 0; i < _model.foodTextures.Count; i++) {
				_model.foodTextures [i].dishTypeId = _model.dishTypes[_dishTypeSectionList [i].ValueSelectedIndex()].id;
				_model.foodTextures [i].restrictionId = _model.foodTextureList[_dishTypeSectionList [i].KeySelectedIndex()].id;
			}
			_requestHandler.SendRequest (View, () => Save (_model));
		}

		private async void GetTrialModel()
		{
			var content = KnownUrls.GetTrialOrderEditModelQueryString ((long)_registrationId, (long)AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<TrialOrderModel> ()
				.From (KnownUrls.GetTrialOrderEditModel)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestModelCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(TrialOrderModel result)
		{
			OnRequestCompleted (true);
			this._model = result;
			if (_model.totalDishTypeReplacement > 0) {
				var dishTypeText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("DishType");
				var foodTextureText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FoodTexture");

				for (int i = 0; i < _model.totalDishTypeReplacement; i++) {
					try{
						_dishTypeSectionList.Add (
							new KeyValuePairRadioGroupDVC<BaseIpadModel> (foodTextureText).CreateElement (foodTextureText, _model.foodTextures[i].restrictionId, _model.foodTextureList,
								dishTypeText, _model.foodTextures[i].dishTypeId, _model.dishTypes));
					} catch(Exception) {
						_dishTypeSectionList.Add (
							new KeyValuePairRadioGroupDVC<BaseIpadModel> (foodTextureText).CreateElement (foodTextureText, null, _model.foodTextureList,
								dishTypeText, null, _model.dishTypes));
					}
				}
			}

			if (Settings.ShowOtherDietsCommentField && _model.comments != null) {
				int idx = 0;
				var alertMsg1 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Comment");
				_model.comments.ForEach (e => {
					_dietComments.Add (new MultilineEntryElement (alertMsg1 + " : ", "", e, idx,this));
					idx++;
				});
			}

			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrder"));
			CreateSaveButton ();
		}

		private async void Save(TrialOrderModel model)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveTrialOrder)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success) {
				this.ShowAlert (AppContext, "Savedsuccessful");
			} else {
				this.ShowAlert (AppContext, "Save failed");
			}
			_callback.Refresh ();
			NavigationController.PopViewControllerAnimated (true);
		}
	}
}

