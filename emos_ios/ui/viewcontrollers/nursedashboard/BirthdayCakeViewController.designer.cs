// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class BirthdayCakeViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BirthdayCakeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BirthdayLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView CakeImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CancelOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CloseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealPeriodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl MealPeriodSegment { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton OrderButton { get; set; }

		[Action ("CancelOrderButton_TouchUpInside:")]
		partial void CancelOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("CloseButton_TouchUpInside:")]
		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("MealPeriodSegment_ValueChanged:")]
		partial void MealPeriodSegment_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("OrderButton_TouchUpInside:")]
		partial void OrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BirthdayCakeLabel != null) {
				BirthdayCakeLabel.Dispose ();
				BirthdayCakeLabel = null;
			}

			if (BirthdayLabel != null) {
				BirthdayLabel.Dispose ();
				BirthdayLabel = null;
			}

			if (CakeImage != null) {
				CakeImage.Dispose ();
				CakeImage = null;
			}

			if (CancelOrderButton != null) {
				CancelOrderButton.Dispose ();
				CancelOrderButton = null;
			}

			if (CloseButton != null) {
				CloseButton.Dispose ();
				CloseButton = null;
			}

			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (MealPeriodLabel != null) {
				MealPeriodLabel.Dispose ();
				MealPeriodLabel = null;
			}

			if (MealPeriodSegment != null) {
				MealPeriodSegment.Dispose ();
				MealPeriodSegment = null;
			}

			if (OrderButton != null) {
				OrderButton.Dispose ();
				OrderButton = null;
			}
		}
	}
}
