// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("FluidRestrictionListViewController")]
	partial class FluidRestrictionListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel FluidRestrictionErrorMessageLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FluidRestrictionTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView FluidTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (FluidRestrictionErrorMessageLabel != null) {
				FluidRestrictionErrorMessageLabel.Dispose ();
				FluidRestrictionErrorMessageLabel = null;
			}

			if (FluidRestrictionTitleLabel != null) {
				FluidRestrictionTitleLabel.Dispose ();
				FluidRestrictionTitleLabel = null;
			}

			if (FluidTable != null) {
				FluidTable.Dispose ();
				FluidTable = null;
			}

			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}
		}
	}
}
