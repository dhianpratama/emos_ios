﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public partial class TrialOrderListViewController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> SnackPackListDataSource;

		private InfoDataListHandler _snackPackListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private RegistrationModelSimple _registration;
		private InterfaceStatusModel _interfaceStatus;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }
		private LocationWithRegistrationSimple _location;

		public TrialOrderListViewController (LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance, "TrialOrderListViewController", null)
		{
			_registration = locationWithRegistration.registrations.First ();
			_location = locationWithRegistration;
			_interfaceStatus = interfaceStatus;
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrder");
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			TrialOrderTitleLabel.SetFrame (y: (Settings.ShowAdditionalPatientMenuLinks) ? Measurements.TopY : Measurements.TopY + 20.0f);
			if (!Settings.ShowDVCTitle)
				TrialOrderTitleLabel.SetFrame (height: 0);

			if (Settings.ShowAdditionalPatientMenuLinks && AppContext.CurrentUser.Authorized ("ACCESS_EXTRA_ORDERS")){
				var headerDVC = new TrialOrderHeaderDVC (AppContext, PatientMenuCallback, _location, Title, true);
				headerDVC.View.SetFrameBelowTo (TrialOrderTitleLabel, 0, 768, 350, 0);
				View.AddSubview (headerDVC.View);
				TrialOrderTable.SetFrameBelowTo (headerDVC.View, height: Measurements.TrialOrderTableHeight, distanceToAbove: 0);
			} else {
				TrialOrderTable.SetFrameBelowTo (TrialOrderTitleLabel, height: Measurements.TrialOrderTableHeight, distanceToAbove: 0);
			}

			_requestHandler.SendRequest (View, RequestTrialOrderViewModel);
		}

		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}

			string addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				OnEdit(0);
			});
			addButton.TintColor = UIColor.White;

			if (_items == null) {
				if(!_interfaceStatus.DIET_ORDERS)
					NavigationItem.SetRightBarButtonItem (addButton, false);
			} else {
				if (_items.Count == 0) {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (addButton, false);
				} else {
					if (!_interfaceStatus.DIET_ORDERS)
						NavigationItem.SetRightBarButtonItem (null, false);
				}
			}

		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_snackPackListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				//EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
		}

		private bool InterfaceStatus()
		{
			return false;
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			TrialOrderTable.Hidden = false;
			SnackPackListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_snackPackListViewCellHandler);
			SnackPackListDataSource.Items = _items;
			TrialOrderTable.Source = SnackPackListDataSource;
			TrialOrderTable.ReloadData ();
		}

		public void SetLayout()
		{
			TrialOrderTable.ScrollEnabled = true;
			TrialOrderTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			var dvc = new TrialOrderDVC (this, _registration.registration_id);
			AppContext.PushDVC (dvc);
		}

		public void OnDelete(long id)
		{
			var snackPackText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrder");
			var alertMsg2 = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatethistrialorder?");
			this.ShowConfirmAlert (AppContext, snackPackText, alertMsg2, OnDeleteConfirmed);
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestTrialOrderViewModel()
		{
			var queryString = KnownUrls.RegistrationIdQueryString ((long)_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>>()
				.From (KnownUrls.GetTrialOrderViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestTrialOrderSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestTrialOrderSuccesful(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			_items = result;
			GenerateRightButton ();
			if (_items.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private async void DeleteSnackPack()
		{
			long profileDietOrderId = Convert.ToInt32( _items.First ().First (e => e.Key == "Id").Value );
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var alertMsg = String.Format( AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Trialorder'{0}'hasbeendeactivated"),
					_items.First ().First (e => e.Key == "Identifier").Value);
				this.ShowAlert (AppContext, alertMsg, translateMessage: false);
				_requestHandler.SendRequest (View, RequestTrialOrderViewModel);
			} else
				OnRequestCompleted (false);
		}

		private void OnDeleteConfirmed()
		{
			_requestHandler.SendRequest (View, DeleteSnackPack);
		}

		private void ShowTable()
		{
			TrialOrderTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			TrialOrderTable.Hidden = true;
			NotFoundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			TrialOrderTable.Hidden = true;
			NotFoundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestTrialOrderViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			TrialOrderTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TrialOrder");
			TrialOrderErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Notrialorderisfound");
		}
	}
}

