﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

using emos_ios.models;
using emos_ios.tools;

using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;


namespace emos_ios
{
	public class MealTypeDVC : BaseDVC
	{
		private MealTypeModel _model;
		private SaveMealTypeModel _saveModel;
		private IPatientMenu _callback;
		private List<CheckboxGroupDVC<RestrictionModelSimple>> _restrictionTypeGroups;
		private List<Section> _restrictionCheckboxes;
		private LocationWithRegistrationSimple _location;

		public MealTypeDVC (LocationWithRegistrationSimple location, IPatientMenu callback, InterfaceStatusModel interfaceStatus) : base (AppDelegate.Instance)
		{
			_location = location;
			this._callback = callback;
		}
		public void Load()
		{
			GetData ();
		}
		private void ShowSaveButton ()
		{
			if (!_callback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem (saveText, UIBarButtonItemStyle.Plain, delegate {
				onSaveClicked ();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}
		public override void GetData()
		{
			_requestHandler.SendRequest (View, RequestMealTypeModel);
		}

		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			_restrictionTypeGroups = new List<CheckboxGroupDVC<RestrictionModelSimple>> ();
			_restrictionCheckboxes = new List<Section> ();
			_model.mealTypes.Where(e=> e.restrictions.Count > 0)
				.ToList()
				.ForEach (rt => {

					var checkboxGroup = new CheckboxGroupDVC<RestrictionModelSimple>(rt.label, _model.currentTypes, rt.restrictions);
					_restrictionTypeGroups.Add(checkboxGroup);
					_restrictionCheckboxes.Add(checkboxGroup.Create());
				});

			Root.Add (DvcGenerator.PatientInfoWithFeed (AppContext, _model.registration, Settings.QueryMealTypeWithFeeds, _model.clearFeed, _model.fullFeed));
			CreateOthersSection ();
			Root.Add(_restrictionCheckboxes);
		}
		private void CreateOthersSection ()
		{
			if (!Settings.ShowAdditionalPatientMenuLinks)
				return;
			var dietOrderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.Therapeutic);
			Root.Add (new Section (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Others")) {
				new StyledStringElement ("Allergies", () =>  {
					_callback.FoodAllergies (_location, _model.clearFeed, _model.fullFeed, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
				new StyledStringElement (dietOrderText, () =>  {
					_callback.TherapeuticDietOrder(_location, _model.clearFeed, _model.fullFeed, true);
				}) {
					Image = UIImage.FromBundle ("Images/next_button_thumb.png")
				},
			});
		}
		private void RequestMealTypeModel()
		{
			if (Settings.QueryMealTypeWithFeeds)
				RequestMealTypeWithFeed ();
			else
				RequestMealType ();
		}
		private async void RequestMealTypeWithFeed ()
		{
			var param = KnownUrls.GetMealTypeWithFeedQueryString (_location.registrations.First ().registration_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<MealTypeModel> ()
				.From (KnownUrls.GetMealTypeWithFeed)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestModelCompleted (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private async void RequestMealType ()
		{
			var param = KnownUrls.GetMealTypeModelQueryString (_location.registrations.First ().registration_id, AppDelegate.Instance.InstitutionId);
			await AppDelegate.Instance.HttpSender.Request<MealTypeModel> ()
				.From (KnownUrls.GetMealTypeModel)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestModelCompleted (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void onSaveClicked()
		{
			var finalResult = new List<RestrictionModelSimple> ();
			_restrictionTypeGroups.ForEach (rt => {
				finalResult.AddRange(rt.Result);
			});
			_model.currentTypes = finalResult.Distinct()
				.ToList();

			_saveModel = new SaveMealTypeModel(){
				profile_id = _model.registration.profile_id,
				location_id = _location.id
			};

			_model.currentTypes
				.ForEach (t => {
					long res = t.id!=null ? (long)t.id : 0;
					long prof = t.profile_diet_order_restriction!=null ? (long)t.profile_diet_order_restriction : 0;
					_saveModel.IdList.Add(new KeyValuePair<long, long>(res,prof));
			});
			_requestHandler.SendRequest (View, () => Save (_saveModel));
		}

		private async void Save(SaveMealTypeModel content)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.SaveMealType)
				.WithContent(content)
				.WhenSuccess (result => onSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestModelCompleted(MealTypeModel result)
		{
			OnRequestCompleted (true);
			_model = result;
			CreateRoot (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealType"));
			ShowSaveButton ();
		}

		private void OnRequestFailed()
		{
			Console.WriteLine ("Request failed");
		}

		private void onSaveCompleted(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				this.ShowAlert (AppContext, "Mealtypesaved");
			} else
				OnRequestCompleted (false);
		}
		protected override void OnRequestCompleted (bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			base.OnRequestCompleted (success, failedTitleCode, showFailedTitle);
			_callback.ShouldRunViewWillAppear = true;
		}
	}
}

