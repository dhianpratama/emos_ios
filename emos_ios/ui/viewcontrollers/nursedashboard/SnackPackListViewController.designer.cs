// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("SnackPackListViewController")]
	partial class SnackPackListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SnackPackErrorMsgLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView SnackPackTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SnackPackTitleLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}

			if (SnackPackErrorMsgLabel != null) {
				SnackPackErrorMsgLabel.Dispose ();
				SnackPackErrorMsgLabel = null;
			}

			if (SnackPackTable != null) {
				SnackPackTable.Dispose ();
				SnackPackTable = null;
			}

			if (SnackPackTitleLabel != null) {
				SnackPackTitleLabel.Dispose ();
				SnackPackTitleLabel = null;
			}
		}
	}
}
