﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using emos_ios;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public partial class TherapeuticListViewController : BaseViewController, ICommonDataListCallback
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> TherapeuticListDataSource;

		private InfoDataListHandler _therapeuticListViewCellHandler;
		private List<List<KeyValuePair<string,string>>> _items;
		private InterfaceStatusModel _interfaceStatus;
		private LocationWithRegistrationSimple _location;
		public IPatientMenu PatientMenuCallback { get; set; }
		public bool EditEnabled { get { return PatientMenuCallback.LockResult.free_to_access; } }
		private CommonDietOrderModel _fullFeed;
		private CommonDietOrderModel _clearFeed;

		public TherapeuticListViewController (IPatientMenu patientMenuCallback, LocationWithRegistrationSimple locationWithRegistration, InterfaceStatusModel interfaceStatus, CommonDietOrderModel clearFeed, CommonDietOrderModel fullFeed) : base (AppDelegate.Instance, "TherapeuticListViewController", null)
		{
			_location = locationWithRegistration;
			_interfaceStatus = interfaceStatus;
			_fullFeed = fullFeed;
			_clearFeed = clearFeed;
			PatientMenuCallback = patientMenuCallback;
			Title = (Settings.UseDietOrderText) ? "Diet Orders" : 
			AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TherapeuticDiet");
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			HideTableAndNotFound ();
			TherapeuticDietTitleLabel.SetFrame (y: Measurements.TopY + 20);
			if (!Settings.ShowDVCTitle)
				TherapeuticDietTitleLabel.SetFrame (height: 0);
			if (Settings.ShowAdditionalPatientMenuLinks) {
				var dietOrderText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.Therapeutic);
				var headerDVC = new TherapeuticHeaderDVC (AppContext, PatientMenuCallback, _location, dietOrderText, Settings.QueryMealTypeWithFeeds, _clearFeed, _fullFeed);
				headerDVC.View.SetFrameBelowTo (TherapeuticDietTitleLabel, 0, 768, 480, 0);
				View.AddSubview (headerDVC.View);
				TherapeuticListTable.SetFrameBelowTo (headerDVC.View, height: Measurements.TherapeuticListTableHeight, distanceToAbove: 0);
			} else {
				TherapeuticListTable.SetFrameBelowTo (TherapeuticDietTitleLabel, height: Measurements.TherapeuticListTableHeight, distanceToAbove: 0);
			}
			PatientMenuCallback.ShouldRunViewWillAppear = true;
			_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
		}
		private void GenerateRightButton()
		{
			if (!PatientMenuCallback.LockResult.free_to_access && Settings.CheckLock) {
				View.UserInteractionEnabled = false;
				return;
			}
			var addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				AddNew();
			});
			addButton.TintColor = UIColor.White;
			if (!_interfaceStatus.DIET_ORDERS) {
				if (Settings.AllowToAddOnlyOneTherapeutic && _items.Count () == 1) {
					NavigationItem.SetRightBarButtonItem (null, false);
				} else {
					NavigationItem.SetRightBarButtonItem (addButton, false);
				}
			}
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_therapeuticListViewCellHandler = new InfoDataListHandler (InfoDataListViewCell.CellIdentifier, (ICommonDataListCallback)this, setting){
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = _interfaceStatus.DIET_ORDERS
			};
		}

		private float GetHeightForRow(List<KeyValuePair<string,string>> item)
		{
			return InfoDataListViewCell.GetRowHeight (item);
		}

		private float GetHeight(List<KeyValuePair<string,string>> item)
		{
			return 110;
		}

		public void ShowLayout()
		{
			TherapeuticListTable.Hidden = false;
			TherapeuticListDataSource = new DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> (_therapeuticListViewCellHandler);
			TherapeuticListDataSource.Items = _items;
			TherapeuticListTable.Source = TherapeuticListDataSource;
			TherapeuticListTable.ReloadData ();
		}

		public void SetLayout()
		{
			TherapeuticListTable.ScrollEnabled = true;
			TherapeuticListTable.RowHeight = 110;
		}
		public void OnEdit(long id)
		{
			var dvc = new TherapeuticDietOrderDVC (this, _location.registrations.First ().registration_id, false, id);
			AppContext.PushDVC (dvc);
		}
		public void AddNew ()
		{
			var dvc = new TherapeuticDietOrderDVC (this, _location.registrations.First ().registration_id, true);
			AppContext.PushDVC (dvc);
		}
		public void OnDelete(long id)
		{
			var alertMsg1 = (Settings.UseDietOrderText)? "Diet Order" : AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therapeutic");
			var alertMsg2 = (Settings.UseDietOrderText)? "Are you sure want to deactivated this diet order?" : AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Areyousurewanttodeactivatedthistherapeutic?");
			this.ShowConfirmAlert (AppContext, alertMsg1, alertMsg2, () => OnDeleteConfirmed (id));
		}

		public void ItemSelected (List<KeyValuePair<string, string>> selected, int selectedIndex)
		{

		}

		private async void RequestTherapeuticViewModel()
		{
			var queryString = KnownUrls.RegistrationIdQueryString ((long)_location.registrations.First ().registration_id);
			await AppDelegate.Instance.HttpSender.Request<List<List<KeyValuePair<string,string>>>>()
				.From (KnownUrls.GetTherapeuticViewModel)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestTherapeuticModelSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestTherapeuticModelSuccesful(List<List<KeyValuePair<string,string>>> result)
		{
			OnRequestCompleted (true);
			_items = result;
			GenerateRightButton ();
			if (_items.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private async void DeleteProfileTherapeutic(long profileDietOrderId)
		{
			var queryString = KnownUrls.ProfileDietOrderIdQueryString (profileDietOrderId);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteProfileDietOrderRestrictions)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnRequestDeleteSuccesful (result, profileDietOrderId))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}

		private void OnRequestDeleteSuccesful(OperationInfoIpad result, long profileDietOrderId)
		{
			if (result.success) {
				OnRequestCompleted (true);
				var alertMsg = (Settings.UseDietOrderText)? 
					String.Format("Diet order '{0}' has been deactivated.", profileDietOrderId) 
					: String.Format(AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therapeutic'{0}'hasbeendeactivated"), profileDietOrderId);

				this.ShowAlert (AppContext, alertMsg, translateMessage: false);
				_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
			} else
				OnRequestCompleted (false);
		}

		private void OnDeleteConfirmed(long profileDietOrderId)
		{
			_requestHandler.SendRequest (View, () => DeleteProfileTherapeutic (profileDietOrderId));
		}

		private void ShowTable()
		{
			TherapeuticListTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			TherapeuticListTable.Hidden = true;
			NotFoundView.Hidden = false;
		}
		private void HideTableAndNotFound()
		{
			TherapeuticListTable.Hidden = true;
			NotFoundView.Hidden = true;
		}

		public void Refresh()
		{
			HideTableAndNotFound ();
			_requestHandler.SendRequest (View, RequestTherapeuticViewModel);
		}

		public InterfaceStatusModel GetInterfaceStatus()
		{
			return _interfaceStatus;
		}
		public override void SetTextsByLanguage()
		{
			TherapeuticDietTitleLabel.Text = (Settings.UseDietOrderText)? "Diet Orders" : AppDelegate.Instance.LanguageHandler.GetLocalizedString ("TherapeuticDiet");
			TherapeuticDietErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.NoDietOnTherapeuticFound);
		}
	}
}

