﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public partial class PatientLanguageViewController : BaseViewController
	{
		private ProfileLanguageModelSimple _profileLanguageModel;
		private ProfileLanguageModelSimple _savedProfileLanguageModel;
		private const float gap = 5f;
		public event EventHandler<BaseEventArgs<UIViewController>> OnClose;
		public event EventHandler<ProfileLanguageEventArgs> OnSave;

		public PatientLanguageViewController (ProfileLanguageModelSimple profile) : base (AppDelegate.Instance, "PatientLanguageViewController", null)
		{
			_profileLanguageModel = profile;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			LoadUI ();
			// Perform any additional setup after loading the view, typically from a nib.
		}
		private void LoadUI(){
			UpdateTitleUI();

			if (AppDelegate.Instance.Languages != null) {
				int i = 0;
				foreach (LanguageModelSimple language in AppDelegate.Instance.Languages) {
					UIButton button = new UIButton(UIButtonType.Custom);
					//button.SetImage (UIImage.FromBundle ("Images/languages/" + language.code + ".png"), UIControlState.Normal);
					//button.ImageEdgeInsets = new UIEdgeInsets (6, 15, 6, 495);
					//button.TitleEdgeInsets = new UIEdgeInsets (0, -40, 0, 0);
					button.TitleEdgeInsets = new UIEdgeInsets (0, 20, 0, 0);
					button.SetFrame (width: 548f, height: 50f);
					button.Layer.CornerRadius= 8f;
					button.BackgroundColor = UIColor.Clear;
					button.Tag = (int)language.id;
					button.SetTitle (language.label, UIControlState.Normal);
					button.TitleLabel.Font = UIFont.FromName ("HelveticaNeue", 18);
					if(_profileLanguageModel.emlanguage_id == null && language.code == "en")
						button.TitleLabel.Font = UIFont.FromName ("HelveticaNeue-Bold", 18);
					else if (_profileLanguageModel.emlanguage_id != null && _profileLanguageModel.emlanguage_id == language.id)
						button.TitleLabel.Font = UIFont.FromName ("HelveticaNeue-Bold", 18);

					button.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
					button.SetFrame (10, patientLanguageTitleContainerView.Frame.Height + (i * (button.Frame.Height + gap)), button.Frame.Width, button.Frame.Height);
					UIView line = new UIView ();
					line.SetFrame (15, patientLanguageTitleContainerView.Frame.Height + (i * (button.Frame.Height + gap)) - 2, patientLanguageContentView.Frame.Width - 15, 1);
					line.BackgroundColor = UIColor.White;

					button.TouchUpInside += languageHandleTouchUpInside;
					patientLanguageContentView.AddSubview (button);
					if(i != 0)
						patientLanguageContentView.AddSubview (line);

					i++;
				}

				patientLanguageContentView.Frame = new RectangleF (0, 0, patientLanguageContentView.Frame.Width, ((50 + gap) * i) + patientLanguageTitleContainerView.Frame.Height);
				patientLanguageContentView.Center = new PointF (this.View.Frame.Width / 2, this.View.Frame.Height / 2);
			}
		}
		private void UpdateTitleUI(){
			patientLanguageTitleContainerView.BackgroundColor = Colors.MainThemeColor;
			var selectedLanguageTitle = "";
			LanguageModelSimple selectedLanguage = new LanguageModelSimple ();
			if (AppDelegate.Instance.Languages != null && _profileLanguageModel.emlanguage_id != null) {
				selectedLanguage = AppDelegate.Instance.Languages.Where (r => r.id == _profileLanguageModel.emlanguage_id).First ();
				selectedLanguageTitle = selectedLanguage.label ?? "";
			} else {
				selectedLanguage = AppDelegate.Instance.Languages.Where (r => r.code == "en").First ();
				selectedLanguageTitle = selectedLanguage.label ?? "";
			}
			patientSelectedLanguageLabel.Text = selectedLanguageTitle;
		}
		private void languageHandleTouchUpInside (object sender, EventArgs ea) {
			Console.Write("languageButtonTapped");
			UIButton buttonTapped = (UIButton)sender;
			Console.WriteLine("buttonTapped " + buttonTapped.Tag);

			_savedProfileLanguageModel = new ProfileLanguageModelSimple ();
			_savedProfileLanguageModel.profile_id = _profileLanguageModel.profile_id;
			_savedProfileLanguageModel.emlanguage_id = (long)buttonTapped.Tag;
			Save (_savedProfileLanguageModel);
		}

		partial void exitButtonTapped (MonoTouch.Foundation.NSObject sender){
			View.RemoveFromSuperview();
			OnClose.SafeInvoke(this, new BaseEventArgs<UIViewController>().Initiate(this));
		}
		private async void Save(ProfileLanguageModelSimple model)
		{
			await AppDelegate.Instance.HttpSender.Request<ProfileLanguageModelSimple> ()
				.From (KnownUrls.SavePatientLanguage)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			_profileLanguageModel = _savedProfileLanguageModel;
			this.ShowAlert (AppContext, "Savedsuccessful");
			View.RemoveFromSuperview();
			OnSave.SafeInvoke (this, new ProfileLanguageEventArgs (_profileLanguageModel));
			OnClose.SafeInvoke (this, new BaseEventArgs<UIViewController> ().Initiate (this));
		}
		public override void ShowBottomLogo(float x=0, float y=-1, float width=768, float height=-1)
		{

		}
	}
}

