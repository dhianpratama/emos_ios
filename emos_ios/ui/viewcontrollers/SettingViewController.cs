﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class SettingViewController : BaseViewController, ICallback<string>
	{
		private const float WardViewHeight = 490;
		private const float HospitalViewHeight = 490;
		private const float OperationDateViewHeight = 290;
		public event EventHandler WardSelectionCancelled;
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> WardSelectionDone;
		public event EventHandler HospitalSelectionCancelled;
		public event EventHandler OperationDateSelectionCancelled;
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OperationDateSelectionDone;
		public event EventHandler OperationTimeSelectionCancelled;
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OperationTimeSelectionDone;
		public event EventHandler CloseEvent;
		private KeyValuePair<string, string> _previousWard;
		private KeyValuePair<string, string> _previousHospital;
		private string _previousOperationDate;
		private KeyValuePair<string, string> _previousOperationTime;
		private BaseViewCellSetting _baseViewCellSetting;
		private KeyValuePairViewCellHandler _wardViewCellHandler;
		private KeyValuePairViewCellHandler _hospitalViewCellHandler;
		private TableAndHeaderView<KeyValuePair<string, string>, KeyValuePairViewCell> _hospitalView;
		private TableAndHeaderView<KeyValuePair<string,string>, KeyValuePairViewCell> _wardView;
		private TableAndHeaderView<string, StringViewCell> _operationDateView;
		private TableAndHeaderView<KeyValuePair<string,string>, KeyValuePairViewCell> _operationTimeView;
		private List<UITextField> _textFields = new List<UITextField>();
		private bool _hideDateSettingOnPatientMealOrder;

		public SettingViewController (bool hideDateSetting = false) : base (AppDelegate.Instance, "SettingViewController", null)	
		{
			HiddenLogo = true;
			_hideDateSettingOnPatientMealOrder = hideDateSetting;
			_baseViewCellSetting = new BaseViewCellSetting { BackgroundColor = UIColor.White };
			_wardViewCellHandler = new KeyValuePairViewCellHandler (this, _baseViewCellSetting);
			_hospitalViewCellHandler = new KeyValuePairViewCellHandler (this, _baseViewCellSetting);
			InitiateHospitalView ();
			InitiateWardView ();
			InitiateOperationDateView ();
			InitiateOperationTimeView ();
			_previousWard = AppDelegate.Instance.Ward;
			_previousHospital = AppDelegate.Instance.Hospital;
			_previousOperationDate = AppDelegate.Instance.OperationDate.ToString (Formats.OperationDateFormat);
			_previousOperationTime = AppDelegate.Instance.MealOrderPeriodGroup;
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			_requestHandler.SendRequest (View, () => RequestMealPeriodGroups ());
			BackgroundView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
			BackgroundView.Layer.BorderWidth = 1.0f;
			BackgroundView.BackgroundColor = UIColor.White;
			if (!Settings.HospitalBasedWards)
				SetNonHospitalBasedWardsLayout ();
			ShowCurrentSetting ();
			AddHospitalView ();
			AddWardView ();
			AddOperationDateView ();
			AddOperationTimeView ();
			SetTextsByLanguage ();
			SetSettingsEnabled (true);

			if (Settings.HideMealPeriodGroupInSetting) {
				MealGroupLabel.Hidden = true;
				_operationTimeView.Hidden = true;
				OperationText.Hidden = true;
			}
			FilterHospitalText.Hidden = !Settings.HospitalBasedWards;
			FilterHospitalLabel.Hidden = !Settings.HospitalBasedWards;


			SettingViewControllerHandler.AnimateMe (this, BackgroundView);
		}
		private void SetNonHospitalBasedWardsLayout ()
		{
			var top = 150f;
			if (_hideDateSettingOnPatientMealOrder) {
				top = 70f;
				switchDayContainerView.Hidden = true;
			}
			filterWardContainerView.Frame = new RectangleF (filterWardContainerView.Frame.X, top, filterWardContainerView.Frame.Width, filterWardContainerView.Frame.Height);
			mealGroupContainerView.Frame = new RectangleF (mealGroupContainerView.Frame.X, top + filterWardContainerView.Frame.Height, mealGroupContainerView.Frame.Width, mealGroupContainerView.Frame.Height);
		}
		private void ShowCurrentSetting ()
		{
			DayText.Text = AppDelegate.Instance.OperationDate.ToString (Formats.OperationDateFormat);
			OperationText.Text = AppDelegate.Instance.MealOrderPeriodGroup.Value;
			FilterHospitalText.Text = "";
			FilterWardText.Text = "";
			if (AppDelegate.Instance.HospitalId != null && AppDelegate.Instance.HospitalId != 0 && AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Any ()) {
				var hospital = AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.FirstOrDefault (e => e.id == AppDelegate.Instance.HospitalId);
				if (hospital == null)
					return;
				FilterHospitalText.Text = hospital.label;
				if (AppDelegate.Instance.WardId != null && AppDelegate.Instance.WardId != 0) {
					var ward = hospital.wards.FirstOrDefault (w => w.id == AppDelegate.Instance.WardId);
					if (ward != null)
						FilterWardText.Text = ward.label;
				}
			}
		}
		private void InitiateHospitalView ()
		{
			var hospitalsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Hospitals");
			_hospitalView = new TableAndHeaderView<KeyValuePair<string,string>, KeyValuePairViewCell> (AppContext, _hospitalViewCellHandler, hospitalsText);
			_hospitalView.OnDone += OnHospitalSelectionDone;
			_hospitalView.OnCancelled += OnHospitalSelectionCancelled;
			AppDelegate.Instance.HospitalViewHandler.OnRequestFailed += OnRequestHospitalFailed;
			AppDelegate.Instance.HospitalViewHandler.OnRequestSuccessful += OnRequestHospitalsSuccessful;
		}
		private void AddHospitalView ()
		{
			var x = BackgroundView.Frame.Left + FilterHospitalText.Frame.Left;
			var y = Measurements.FrameTop + filterHospitalContainerView.Frame.Y + FilterHospitalText.Frame.Y + FilterHospitalText.Frame.Height;
			_hospitalView.SetLayout (new RectangleF (x, y, FilterHospitalText.Frame.Width, HospitalViewHeight));
			_hospitalView.TableView.SetItems (AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Select (e => new KeyValuePair<string,string> (e.id.ToString (), e.label)));
			View.AddSubview (_hospitalView);
			_hospitalView.Hidden = true;
			_textFields.Add (FilterHospitalText);
		}
		private void InitiateWardView ()
		{
			var wardsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Wards");
			_wardView = new TableAndHeaderView<KeyValuePair<string,string>, KeyValuePairViewCell> (AppContext, _wardViewCellHandler, wardsText);
			_wardView.OnDone += OnWardSelectionDone;
			_wardView.OnCancelled += OnWardSelectionCancelled;
			AppDelegate.Instance.WardViewHandler.OnRequestFailed += OnRequestWardFailed;
			AppDelegate.Instance.WardViewHandler.OnRequestSuccessful += OnRequestWardsSuccessful;
		}
		private void AddWardView ()
		{
			var x = BackgroundView.Frame.Left + FilterWardText.Frame.Left;
			var y = Measurements.FrameTop + filterWardContainerView.Frame.Y + FilterWardText.Frame.Y + FilterWardText.Frame.Height;
			_wardView.SetLayout (new RectangleF (x, y, FilterWardText.Frame.Width, WardViewHeight));
			View.AddSubview (_wardView);
			_wardView.Hidden = true;
			_textFields.Add (FilterWardText);
		}
		private void OnWardSelectionCancelled (object sender, EventArgs e)
		{
			WardSelectionCancelled.SafeInvoke (this);
			Close ();
		}
		private void OnWardSelectionDone (object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (_wardView.Selected.Key))
				return;
			if (_previousWard.Key == _wardView.Selected.Key) {
				OnWardSelectionCancelled (sender, e);
				return;
			}
			AppContext.SetGlobalWard (_wardView.Selected);
			WardSelectionDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ().Initiate (_wardView.Selected));
			Close ();
		}
		private void Close ()
		{
			View.RemoveFromSuperview ();
			DismissViewController (false, null);
		}
		private void OnRequestWardFailed (object sender, EventArgs e)
		{
			string wardsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Wards");
			OnRequestCompleted (false, wardsText);
			OnWardSelectionCancelled (sender, e);
		}
		private void OnRequestWardsSuccessful (object sender, EventArgs e)
		{
			OnRequestCompleted (true);
			_wardView.TableView.SetItems (AppDelegate.Instance.WardViewHandler.Wards.Select (w => new  KeyValuePair<string, string> (w.id.ToString (), w.label)));
			_wardView.Hidden = false;
			_wardView.Reload ();
		}
		partial void FilterWardText_EditingDidBegin (MonoTouch.Foundation.NSObject sender)
		{
			SetSettingsEnabled(false);
			_wardView.Selected = new KeyValuePair<string, string> ("", "");
			if (!AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Any())
				return;
			var wards = new List<KeyValuePair<string,string>>();
			if (Settings.HospitalBasedWards) {
				var hospital = AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.FirstOrDefault(w=> w.id==AppDelegate.Instance.HospitalId);
				wards = hospital.wards.Select (w => new KeyValuePair<string,string> (w.id.ToString (), w.label)).ToList();
			} else {
				wards = AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.SelectMany(h => h.wards).Select(w => new KeyValuePair<string,string> (w.id.ToString (), w.label)).ToList();
			}
			_wardView.TableView.SetItems (wards);
			_wardView.Hidden = false;
			_wardView.Reload ();
		}

		private void SetSettingsEnabled(bool enabled)
		{
			UIColor backgroundColor;
			if (enabled)
				backgroundColor = UIColor.White;
			else
				backgroundColor = UIColor.DarkGray;
			foreach (var textField in _textFields) {
				textField.BackgroundColor = backgroundColor;
				textField.Enabled = enabled;
			}
		}
		partial void DayText_EditingDidBegin (MonoTouch.Foundation.NSObject sender)
		{
			_operationDateView.Hidden = false;
			SetSettingsEnabled(false);
			_operationDateView.Selected = "";
			ShowOperationDateFilter();
		}
		private void InitiateOperationDateView ()
		{
			var stringViewCellHandler = new StringViewCellHandler (this, _baseViewCellSetting);
			var datesText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Dates");
			_operationDateView = new TableAndHeaderView<string, StringViewCell> (AppContext, stringViewCellHandler, datesText);
			_operationDateView.OnDone += OnOperationDateSelectionDone;
			_operationDateView.OnCancelled += OnOperationDateSelectionCancelled;
			var operationDates = new List<string> ();
			var operationDate = DateTime.Today.AddDays(-1);
			for (int i = 1; i < 8; i++) {
				operationDate = operationDate.AddDays (1);
				if (operationDate != AppDelegate.Instance.OperationDate)
					operationDates.Add (operationDate.ToString (Formats.OperationDateFormat));
			}
			_operationDateView.TableView.SetItems (operationDates);
		}
		partial void OperationText_EditingDidBegin (MonoTouch.Foundation.NSObject sender)
		{
			_operationTimeView.Hidden = false;
			SetSettingsEnabled(false);
			_operationTimeView.Selected = new KeyValuePair<string, string> ("", "");
			ShowOperationTimeFilter();
		}
		private void InitiateOperationTimeView ()
		{
			var keyValuePairViewCellHandler = new KeyValuePairViewCellHandler (this, _baseViewCellSetting);
			var operationText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Operation");
			_operationTimeView = new TableAndHeaderView<KeyValuePair<string,string>, KeyValuePairViewCell> (AppContext, keyValuePairViewCellHandler, operationText);
			_operationTimeView.OnDone += OnOperationTimeSelectionDone;
			_operationTimeView.OnCancelled += OnOperationTimeSelectionCancelled;
		}
		private void AddOperationDateView ()
		{
			var x = BackgroundView.Frame.Left + DayText.Frame.Left;
			var y = Measurements.FrameTop + switchDayContainerView.Frame.Y  + DayText.Frame.Top + DayText.Frame.Height;
			_operationDateView.SetLayout (new RectangleF (x, y, DayText.Frame.Width, OperationDateViewHeight));
			View.AddSubview (_operationDateView);
			_operationDateView.Hidden = true;
			_textFields.Add (DayText);
		}
		private void OnOperationDateSelectionCancelled (object sender, EventArgs e)
		{
			OperationDateSelectionCancelled.SafeInvoke (this);
			Close ();
		}
		private void OnOperationDateSelectionDone (object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (_operationDateView.Selected))
				return;
			if (_previousOperationDate == _operationDateView.Selected) {
				OnOperationDateSelectionCancelled (sender, e);
				return;
			}
			AppDelegate.Instance.OperationDate = DateTime.ParseExact(_operationDateView.Selected, Formats.OperationDateFormat, CultureInfo.CurrentCulture);
			OperationDateSelectionDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ());
			Close ();
		}
		private void AddOperationTimeView ()
		{
			var x = BackgroundView.Frame.Left + DayText.Frame.Left;
			var y = Measurements.FrameTop + mealGroupContainerView.Frame.Y+ OperationText.Frame.Top + OperationText.Frame.Height;
			_operationTimeView.SetLayout (new RectangleF (x, y, DayText.Frame.Width, 130));
			View.AddSubview (_operationTimeView);
			_operationTimeView.Hidden = true;
			_textFields.Add (OperationText);
		}
		private void OnOperationTimeSelectionCancelled (object sender, EventArgs e)
		{
			OperationTimeSelectionCancelled.SafeInvoke (this);
			Close();
		}
		private void OnOperationTimeSelectionDone (object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (_operationTimeView.Selected.Key))
				return;
			AppDelegate.Instance.MealPeriodGroupCode = _operationTimeView.Selected.Key;
			if (_previousOperationTime.Key == _operationTimeView.Selected.Key) {
				OnOperationDateSelectionCancelled (sender, e);
				return;
			}
			AppDelegate.Instance.MealOrderPeriodGroup = _operationTimeView.Selected;
			OperationTimeSelectionDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ());
			Close ();
		}
		private void ShowOperationDateFilter()
		{
			_operationDateView.Hidden = false;
			_operationDateView.Reload ();
		}
		private void ShowOperationTimeFilter()
		{
			_operationTimeView.Hidden = false;
			_operationTimeView.Reload ();
		}
		#region ICallback implementation
		public void ItemSelected (string selected, int selectedIndex)
		{
		}
		#endregion
		private void OperationDateTableViewController_DoneClicked (object sender, EventArgs e)
		{
			base.TableViewController_DoneClicked ();
			if (SelectedKeyValue.Value == null)
				return;
			var selected = SelectedKeyValue;
			AppDelegate.Instance.OperationDate = DateTime.ParseExact (selected.Value, Formats.OperationDateFormat, null);
			Title = selected.Value;
		}
		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			//CloseEvent.SafeInvoke(this);
			Close();
		}
		private async void RequestMealPeriodGroups()
		{
			if (AppDelegate.Instance.MealOrderPeriodGroups.Count () > 0) {
				OnRequestMealPeriodGroupsSuccessful (AppDelegate.Instance.MealOrderPeriodGroups);
			}
			var mealPeriodText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealPeriod");
			await AppDelegate.Instance.HttpSender.Request<List<MealOrderPeriodGroupModel>> ()
				.From (KnownUrls.GetAllMealPeriodGroups)
				.WhenSuccess (result => OnRequestMealPeriodGroupsSuccessful(result))
				.WhenFail (result=> OnRequestCompleted(false, mealPeriodText))
				.Go ();
		}
		private void OnRequestMealPeriodGroupsSuccessful(IEnumerable<MealOrderPeriodGroupModel> result)
		{
			OnRequestCompleted (true);
			AppDelegate.Instance.MealOrderPeriodGroups = result;
			var mealOrderPeriodGroups = result.Select (r => new KeyValuePair<string, string> (r.code, r.label));
			_operationTimeView.TableView.SetItems (mealOrderPeriodGroups);
		}
		private void OnHospitalSelectionCancelled (object sender, EventArgs e)
		{
			HospitalSelectionCancelled.SafeInvoke (this);
		}
		private void OnHospitalSelectionDone (object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (_hospitalView.Selected.Key))
				return;
			if (_previousHospital.Key == _hospitalView.Selected.Key) {
				OnHospitalSelectionCancelled (sender, e);
				return;
			}
			AppDelegate.Instance.HospitalId = Convert.ToInt32(_hospitalView.Selected.Key);
			AppDelegate.Instance.Hospital = _hospitalView.Selected;
			AppDelegate.Instance.Ward = new KeyValuePair<string,string> ();
			AppDelegate.Instance.WardId = null;

			FilterHospitalText.Text = _hospitalView.Selected.Value;
			FilterWardText.Text = "";
			_hospitalView.Hidden = true;
			SetSettingsEnabled (true);
		}
		private void OnRequestHospitalFailed (object sender, EventArgs e)
		{
			var hospitalsText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Hospitals");
			OnRequestCompleted (false, hospitalsText);
			OnHospitalSelectionCancelled (sender, e);
		}
		private void OnRequestHospitalsSuccessful (object sender, EventArgs e)
		{
			OnRequestCompleted (true);
			_hospitalView.TableView.SetItems (AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Select (w => new  KeyValuePair<string, string> (w.id.ToString (), w.label)));
			_hospitalView.Hidden = false;
			_hospitalView.Reload ();
		}
		partial void FilterHospitalText_EditingDidBegin (MonoTouch.Foundation.NSObject sender)
		{
			SetSettingsEnabled(false);
			_hospitalView.Selected = new KeyValuePair<string, string> ("", "");
			//SendRequest (View, AppDelegate.Instance.HospitalViewHandler.Request);
			_hospitalView.TableView.SetItems (AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Select (w => new  KeyValuePair<string, string> (w.id.ToString (), w.label)));
			_hospitalView.Hidden = false;
			_hospitalView.Reload ();
		}
		public override void SetTextsByLanguage()
		{
			var switchDayText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SwitchDay");
			var filterHospitalText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FilterHospital");
			var filterWardText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("FilterWard");
			var mealGroupText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealGroup");
			SwitchDayLabel.Text = switchDayText;
			FilterHospitalLabel.Text = filterHospitalText;
			FilterWardLabel.Text = filterWardText;
			MealGroupLabel.Text = mealGroupText;
		}
	}
}

