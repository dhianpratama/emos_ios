
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios.tools;
using emos_ios.models;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using ios;
using System.IO;
using core_emos;

namespace emos_ios
{
	public partial class LoginViewController : BaseViewController
	{
		private const float BorderSize = 1f;
		private RoundedEntryElement _userNameElement;
		private RoundedEntryElement _passwordElement;
		private const float EntryElementLeft = 134;
		private const float EntryElementHeight = 50;
		private const float EntryElementWidth = 500;
		private const float CaptionWidth = 150;
		private UIView _controlBackgroundView;
		private UpdateViewController _updateViewController;
		private UIButton _cancelButton;
		private UIView _authenticationBorderView;
		public event EventHandler<BaseEventArgs<UIViewController>> OnClose;
		public event EventHandler OnSuccess;
		private bool _isAuthenticationMode;

		public LoginViewController (bool isAuthenticationMode) : base (AppDelegate.Instance, "LoginViewController", null)
		{
			_isAuthenticationMode = isAuthenticationMode;
			Title = "Login";
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			var topY = Settings.HideLoginNavigationBar ? 0 : Measurements.TopY;
			LoginImage.SetFrame (0, topY, height: Measurements.LoginImageHeight);
			NurseLoginLabel.SetFrameBelowTo(LoginImage, EntryElementLeft + 10, EntryElementWidth, Measurements.NurseLoginLabelHeight, distanceToAbove: Measurements.LoginImageDistanceToNurseLogin);
			GenerateControls ();
			SetTextsByLanguage ();
			SetLoginImage ();
			LoginButton.Layer.CornerRadius = 10;
			LoginButton.BackgroundColor = AppContext.ColorHandler.ButtonBackground;
			LoginButton.SetFrameBelowTo (_controlBackgroundView, EntryElementLeft, EntryElementWidth, EntryElementHeight);
			UnauthenticatedLabel.SetFrameBelowTo (LoginButton, EntryElementLeft, EntryElementWidth, EntryElementHeight, 5);
			UnauthenticatedLabel.Hidden = true;
		}
		private void HandleOnRequestWardsSuccessful (object sender, EventArgs e)
		{
			base.OnRequestCompleted (true);
			var items = AppDelegate.Instance.WardViewHandler.Wards.Select (w => new KeyValuePair<string, string> (w.id.ToString (), w.label)).ToList ();
			_wardDropDown.Load (items);
			_wardDropDown.ShowDropDown ();
		}
		private void HandleOnWardNotInitiated (object sender, EventArgs e)
		{
			AppContext.WardViewHandler.OnRequestSuccessful += HandleOnRequestWardsSuccessful;
			AppContext.WardViewHandler.OnRequestFailed += (ob, ev) => base.OnRequestCompleted (false);
			_requestHandler.SendRequest (View, () => AppContext.WardViewHandler.RequestInstitutionWards (AppContext.ServerConfig.InsitutionId.Value));
		}
		public void ShowAuthenticationMode ()
		{
			View.BackgroundColor = UIColor.FromRGBA (230, 230, 230, 150);
			NurseLoginLabel.Text = AppContext.LanguageHandler.GetLocalizedString ("NurseReauthenticate");
			LoginButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("Authenticate"), UIControlState.Normal);
			LoginImage.Hidden = true;
			ShowLoginInfoBorder ();
			ShowCancelButton ();

		}
		private void ShowLoginInfoBorder ()
		{
			var margin = 70;
			var width = _controlBackgroundView.Frame.Width + (margin * 2);
			var height = 360;
			_authenticationBorderView = new UIView ();
			_authenticationBorderView.SetFrame (_controlBackgroundView.Frame.Left - margin, NurseLoginLabel.Frame.Top - 20, width, height);
			_authenticationBorderView.Opaque = true;
			_authenticationBorderView.Layer.CornerRadius = 10;
			_authenticationBorderView.Layer.BorderWidth = 2.0f;
			_authenticationBorderView.BackgroundColor = UIColor.White;
			View.AddSubview (_authenticationBorderView);
			View.SendSubviewToBack (_authenticationBorderView);
		}
		private void ShowCancelButton ()
		{
			_cancelButton = new UIButton ();
			_cancelButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("Cancel"), UIControlState.Normal);
			_cancelButton.Layer.CornerRadius = 10;
			_cancelButton.BackgroundColor = UIColor.Red;
			_cancelButton.SetFrameBelowTo (LoginButton, EntryElementLeft, EntryElementWidth, EntryElementHeight);
			UnauthenticatedLabel.SetFrameBelowTo (_cancelButton, EntryElementLeft, EntryElementWidth, EntryElementHeight, 5);
			View.AddSubview (_cancelButton);
			_cancelButton.TouchUpInside += delegate {
				OnClose.SafeInvoke (this, new BaseEventArgs<UIViewController> ().Initiate (this));
			};
		}
		public override void ViewDidAppear (bool animated) {
			base.ViewDidAppear(animated);
			AppDelegate.Instance.KeyboardHandler.AddKeyboardObservers (View);
			LoadUpdateViewController ();
			if (NavigationController != null)
				NavigationController.NavigationBarHidden = Settings.HideLoginNavigationBar;
		}
		private void GenerateControls ()
		{
			_controlBackgroundView = new UIView {
				BackgroundColor = Colors.DarkViewCellBackground,
			};
			var controlCount = 2;
			if (Settings.ShowWardSelectionInLogin && !_isAuthenticationMode)
				controlCount = 3;
			_controlBackgroundView.SetFrameBelowTo (NurseLoginLabel, EntryElementLeft - BorderSize, EntryElementWidth + BorderSize, (EntryElementHeight * controlCount) + (BorderSize * (controlCount + 1)));
			_controlBackgroundView.Layer.BorderColor = Colors.DarkViewCellBackground.CGColor;
			_controlBackgroundView.Layer.BorderWidth = 1.3f;
			_userNameElement = new RoundedEntryElement (new RectangleF (BorderSize, BorderSize, EntryElementWidth - BorderSize, EntryElementHeight), CaptionWidth, "Username*", true, UIRectCorner.TopLeft, UIRectCorner.TopRight);
			if (Settings.ShowWardSelectionInLogin && !_isAuthenticationMode) {
				_passwordElement = new RoundedEntryElement (new RectangleF (BorderSize, _userNameElement.Frame.Top + _userNameElement.Frame.Height + BorderSize, EntryElementWidth - (BorderSize), EntryElementHeight), CaptionWidth, "Password*", true);
				ShowWardDropDown ();
			} else {
				_passwordElement = new RoundedEntryElement (new RectangleF (BorderSize, _userNameElement.Frame.Top + _userNameElement.Frame.Height + BorderSize, EntryElementWidth - (BorderSize), EntryElementHeight), CaptionWidth, "Password*", true, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
			}
			_controlBackgroundView.AddSubview (_userNameElement);
			_controlBackgroundView.AddSubview (_passwordElement);
			_userNameElement.TextField.ReturnKeyType = UIReturnKeyType.Next;
			_userNameElement.TextField.ShouldReturn += MoveFocusToPassword;
			_passwordElement.TextField.ReturnKeyType = UIReturnKeyType.Go;
			_passwordElement.TextField.ShouldReturn += LoginOnReturnKey;
			_userNameElement.TextField.AutocapitalizationType = UITextAutocapitalizationType.None;

			_controlBackgroundView.Layer.CornerRadius = 18f;
			View.AddSubview (_controlBackgroundView);

			_userNameElement.TextField.Text = AppDelegate.Instance.ServerConfig.User ?? "";
			_passwordElement.TextField.Text = AppDelegate.Instance.ServerConfig.Password ?? "";
			_passwordElement.TextField.SecureTextEntry = true;

			_userNameElement.Caption.AdjustsFontSizeToFitWidth = true;
			_passwordElement.Caption.AdjustsFontSizeToFitWidth = true;
		}
		private RoundedDropDown _wardDropDown;
		private void ShowWardDropDown ()
		{
			var frame = new RectangleF (BorderSize, _userNameElement.Frame.Top + _userNameElement.Frame.Height + BorderSize, EntryElementWidth - (BorderSize), EntryElementHeight);
			var items = AppDelegate.Instance.WardViewHandler.Wards.Select (w => new KeyValuePair<string, string> (w.id.ToString (), w.label)).ToList ();
			_wardDropDown = new RoundedDropDown (AppContext, _controlBackgroundView, frame, CaptionWidth, "Login To", items, true, UIRectCorner.BottomLeft, UIRectCorner.BottomRight);
			_wardDropDown.OnListNotInitiated += HandleOnWardNotInitiated;
			_wardDropDown.OnDone += HandleOnWardDone;
			_wardDropDown.SetElementFrameBelowTo (_passwordElement, distanceToAbove: BorderSize);
			_wardDropDown.Load (items);
			_controlBackgroundView.AddSubview (_wardDropDown);
		}
		private void HandleOnWardDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
			AppContext.SetGlobalWard (e.Value);
		}
		private bool MoveFocusToPassword(UITextField textField)
		{
			_userNameElement.TextField.ResignFirstResponder ();
			_passwordElement.TextField.BecomeFirstResponder ();
			return true;
		}
		private bool LoginOnReturnKey(UITextField textField)
		{
			Login (this);
			return true;
		}
		partial void Login (MonoTouch.Foundation.NSObject sender)
		{
			if (string.IsNullOrEmpty(_userNameElement.TextField.Text) || string.IsNullOrEmpty(_passwordElement.TextField.Text))
				return;
			View.EndEditing(true);
			UnauthenticatedLabel.Hidden = true;
			if (AppDelegate.Instance.ServerConfig.IsDemo) {
				if (_userNameElement.TextField.Text.ToLower() == "demo" && _passwordElement.TextField.Text.ToLower() == "demo") {
					LoginRequestSuccessful(new Login{success=true, logged_in=true}, false, true);
				} else {
					LoginRequestSuccessful(new Login{success=false, logged_in=false});
				}
			} else if (_userNameElement.TextField.Text.ToLower() == "admin") {
				if (_passwordElement.TextField.Text.ToLower() == "admin") {
					LoginRequestSuccessful(new Login{success=true, logged_in=true}, true);
				}
			} else {
				AppDelegate.Instance.CurrentUser = new User();
				_requestHandler.SendRequest (View, RequestLogin);
			}
		}
		private async void RequestLogin()
		{
			var param = KnownUrls.LoginQueryString (_userNameElement.TextField.Text, _passwordElement.TextField.Text);
			await AppDelegate.Instance.HttpSender.Request<Login> ()
				.From (KnownUrls.Login)
				.WithQueryString (param)
				.WhenSuccess (result => LoginRequestSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.GetAuthenticationCookie ()
				.Go ();
		}
		private void LoginRequestSuccessful(Login result, bool isAdmin = false, bool isDemoUser = false)
		{
			if (result == null) {
				OnRequestCompleted (false);
				return;
			}

			if (result!=null && result.success && result.logged_in) {
				AppDelegate.Instance.KeyboardHandler.RemoveKeyboardObservers ();
				AppDelegate.Instance.CurrentUser.Id = _userNameElement.TextField.Text;
				AppDelegate.Instance.CurrentUser.IsAdmin = isAdmin;
				AppDelegate.Instance.CurrentUser.IsDemoUser = isDemoUser;
				AppDelegate.Instance.OperationDate = AppDelegate.Instance.ServerConfig.OperationDate < DateTime.Today ? DateTime.Today : AppDelegate.Instance.ServerConfig.OperationDate;

				if (!isAdmin && !isDemoUser) {
					_requestHandler.SendRequest (View, () => RequestUserLocations (AppDelegate.Instance.CurrentUser.Id));
					return;
				}
				AppDelegate.Instance.LoadMenu (core_emos.Menu.EmosMenu.NurseDashboard);
				if (!isAdmin && !isDemoUser) {
					OnRequestCompleted (true);
				}
			} else {
				UnauthenticatedLabel.Hidden = false;
				OnRequestCompleted (false, showFailedTitle:false);
			}
		}
		private async void RequestAccessControl()
		{
			var param = KnownUrls.GetUserAccessControlQueryString (_userNameElement.TextField.Text);
			await AppDelegate.Instance.HttpSender.Request<List<string>> ()
				.From (KnownUrls.GetUserAccessControl)
				.WithQueryString (param)
				.WhenSuccess (result => AccessControlRequestSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void AccessControlRequestSuccesful(List<string> result)
		{
			OnRequestCompleted (true);
			AppDelegate.Instance.CurrentUser.UserAccessControl = result;

			if (Settings.CheckLoginAcl && !AppDelegate.Instance.CurrentUser.Authorized ("LOGIN_IPAD"))
				LoginRequestSuccessful (new Login { success = false });
			else if (!AppDelegate.Instance.CurrentUser.Authorized ("ACCESS_PATIENT_LANGUAGE")) {
				ExitAuthentication ();
			} else {
				RequestLanguages ();
			}
		}
		private void ExitAuthentication ()
		{
			if (_isAuthenticationMode) {
				OnSuccess.SafeInvoke (this);
				OnClose.SafeInvoke (this, new BaseEventArgs<UIViewController> ().Initiate (this));
			} else {
				if (AppContext.CurrentUser.Authorized ("ACCESS_FEEDBACK_DASHBOARD")) {
					AppContext.ControllerLoader.FeedbackLoader.LoadFeedback (AppDelegate.Instance);
				} else if (_wardDropDown != null && AppContext.WardViewHandler.CheckBulkOrder (_wardDropDown.SelectedKey)) {
					AppContext.ControllerLoader.BulkOrderSummaryLoader.Load (AppContext, _wardDropDown.Selected);
				} else {
					AppDelegate.Instance.LoadMenu (core_emos.Menu.EmosMenu.NurseDashboard);
				}
			}
		}
		private async void RequestUserLocations(string userName)
		{
			var param = KnownUrls.GetUserAssignedLocationsQueryString (userName);
			await AppDelegate.Instance.HttpSender.Request<UserAssignedLocationModel> ()
				.From (KnownUrls.GetUserAssignedLocations)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestUserLocationsSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestUserLocationsSuccesful(UserAssignedLocationModel result)
		{
			OnRequestCompleted (true);
			AppDelegate.Instance.CurrentUser.AssignedLocation = result;
			if (!_isAuthenticationMode)
				SetLocations (result);
			RequestAccessControl ();
		}
		private void SetLocations (UserAssignedLocationModel result)
		{
			AppDelegate.Instance.HospitalId = 0;
			if (result.default_hospital_id != null) {
				AppDelegate.Instance.HospitalId = result.default_hospital_id;
			}
			else
				if (result.hospitals.Count > 0) {
					AppDelegate.Instance.HospitalId = result.hospitals.First ().id;
				}
			if (_wardDropDown == null || String.IsNullOrEmpty (_wardDropDown.SelectedKey)) {
				AppDelegate.Instance.WardId = result.default_ward_id ?? 0;
				SetWard ();
			} else if (!Settings.HospitalBasedWards)
				AppContext.SetHospitalByWard ();
		}
		private void SetWard ()
		{
			if (AppDelegate.Instance.HospitalId != null && AppDelegate.Instance.HospitalId != 0 && AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.Any ()) {
				var hospital = AppDelegate.Instance.CurrentUser.AssignedLocation.hospitals.FirstOrDefault (e => e.id == AppDelegate.Instance.HospitalId);
				if (hospital != null && AppDelegate.Instance.WardId != null && AppDelegate.Instance.WardId != 0) {
					var ward = hospital.wards.FirstOrDefault (w => w.id == AppDelegate.Instance.WardId);
					if (ward != null)
						AppDelegate.Instance.Ward = new KeyValuePair<string, string> (ward.id.ToString (), ward.label);
				}
			}
		}
		public override void SetTextsByLanguage(){
			var nurseLoginLabelText = AppDelegate.Instance.LanguageHandler.GetLocalizedString("NurseLogin");
			var userNameText = AppDelegate.Instance.LanguageHandler.GetLocalizedString (LanguageKeys.UserName);
			var passwordText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Password");
			var loginText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Login");
			var errorMSg = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("UsernamePasswordIncorrect");

			NurseLoginLabel.Text = nurseLoginLabelText;
			if(_userNameElement != null)
				_userNameElement.Caption.Text = userNameText;
			if(_passwordElement != null)
				_passwordElement.Caption.Text = passwordText;
			LoginButton.SetTitle (loginText, UIControlState.Normal);
			UnauthenticatedLabel.Text = errorMSg;
		}
		private async void RequestLanguages()
		{
			var param = KnownUrls.UnlockRegistrationsByUserQueryString (AppContext.CurrentUser.Id);
			await AppDelegate.Instance.HttpSender.Request<MultiLingualModel> ()
				.From (KnownUrls.GetLanguages)
				.WithQueryString(param)
				.WhenSuccess (result => OnRequestLanguageCompleted (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}
		private void OnRequestLanguageCompleted(MultiLingualModel result)
		{
			OnRequestCompleted (true);
			AppDelegate.Instance.Languages = result.languages;
			ExitAuthentication ();
		}
		private void LoadUpdateViewController ()
		{
			if (!Settings.EnableTheme || String.IsNullOrEmpty (AppContext.ServerConfig.BaseUrl))
				return;
			_updateViewController = new UpdateViewController (AppContext);
			_updateViewController.OnUpdateComplete += HandleOnUpdateComplete;
			_updateViewController.OnColorUpdated += HandleOnColorUpdated;
			_updateViewController.OnNoUpdate += HandleNoUpdate;
			_updateViewController.NoThemeFromServer += HandleNoThemeFromServer;
			_updateViewController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			_updateViewController.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
			NavigationController.PresentViewController (_updateViewController, true, () => _updateViewController.Start ());
		}
		private void HandleNoThemeFromServer (object sender, EventArgs e)
		{
			_updateViewController.DismissViewController (false, null);
			var documentsFolderPath = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			var loginImageFile = Path.Combine (documentsFolderPath, UpdateViewController.LoginImageUpdateName);
			if (File.Exists (loginImageFile)) {
				System.IO.File.Delete (loginImageFile);
			}

			var logoFile = Path.Combine (documentsFolderPath, "logo.png");
			if (File.Exists (logoFile)){
				System.IO.File.Delete (logoFile);
			}

			LoginImage.Image = UIImage.FromBundle ("Images/login_image.png");
			if (LogoView != null)
				LogoView.SetDefaultLogoImage ();
			AppDelegate.Instance.PaintNavigationBar ();
			LoginButton.BackgroundColor = AppContext.ColorHandler.ButtonBackground;
		}
		private void HandleOnUpdateComplete (object sender, EventArgs e)
		{
			BeginInvokeOnMainThread (() => {
				_updateViewController.DismissViewController (false, null);
				SetLoginImage ();
				if (LogoView != null)
					LogoView.SetImage ();
				AppContext.PaintNavigationBar ();
			});
		}
		private void HandleOnColorUpdated (object sender, EventArgs e)
		{
			AppContext.PaintNavigationBar ();
			LoginButton.BackgroundColor = AppContext.ColorHandler.ButtonBackground;
		}
		private void SetLoginImage ()
		{
			var documentsFolderPath = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			var loginImageFile = Path.Combine (documentsFolderPath, UpdateViewController.LoginImageUpdateName);
			if (File.Exists(loginImageFile))
				LoginImage.Image = UIImage.FromFile (loginImageFile);
			else
				LoginImage.Image = UIImage.FromBundle ("Images/login_image.png");
		}
		private void HandleNoUpdate (object sender, EventArgs e)
		{
			_updateViewController.DismissViewController (false, null);
		}
	}
}

