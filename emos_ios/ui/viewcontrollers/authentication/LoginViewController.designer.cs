// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("LoginViewController")]
	partial class LoginViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton LoginButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView LoginImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NurseLoginLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel UnauthenticatedLabel { get; set; }

		[Action ("Login:")]
		partial void Login (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (LoginButton != null) {
				LoginButton.Dispose ();
				LoginButton = null;
			}

			if (LoginImage != null) {
				LoginImage.Dispose ();
				LoginImage = null;
			}

			if (NurseLoginLabel != null) {
				NurseLoginLabel.Dispose ();
				NurseLoginLabel = null;
			}

			if (UnauthenticatedLabel != null) {
				UnauthenticatedLabel.Dispose ();
				UnauthenticatedLabel = null;
			}
		}
	}
}
