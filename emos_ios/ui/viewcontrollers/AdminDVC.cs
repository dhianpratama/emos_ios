﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Web;
using emos_ios.tools;
using System.Reflection;

namespace emos_ios
{
	public partial class AdminDVC : BaseDVC
	{
		private EntryElement _baseUrlElement;

		public AdminDVC (IApplicationContext appContext) : base (appContext)
		{
			LogoY = Measurements.FrameTop;
			CreateRoot (AppContext.LanguageHandler.GetLocalizedString ("Admin"));
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var settingText = AppContext.LanguageHandler.GetLocalizedString ("Setting");
			var serverIPText = AppContext.LanguageHandler.GetLocalizedString ("ServerIP");
			var newIPText = AppContext.LanguageHandler.GetLocalizedString ("NewIP");
			var submitText = AppContext.LanguageHandler.GetLocalizedString ("Submit");
			var okText = AppContext.LanguageHandler.GetLocalizedString ("OK");
			var currentConfigurationText = AppContext.LanguageHandler.GetLocalizedString ("CurrentConfiguration");
			Root.Add (new Section ());
			Root.Add (new Section ());
			Root.Add (new Section (settingText) {
				(_baseUrlElement = new EntryElement (serverIPText, "http://ip:9999", AppContext.ServerConfig.BaseUrl)),
				new StringElement (submitText, () => {
					var alert = new UIAlertView (newIPText, _baseUrlElement.Value, null, okText, new string[0] { });
					alert.Show ();
					NSUserDefaults.StandardUserDefaults.SetString (_baseUrlElement.Value, "BaseUrlKey"); 
					AppContext.ServerConfig.BaseUrl = _baseUrlElement.Value;
					AppContext.SignOut (true);
				})
			});
			Root.Add (new Section (currentConfigurationText) {
				new StringElement (AppContext.ServerConfig.BaseUrl)
			});
		}
	}
}
