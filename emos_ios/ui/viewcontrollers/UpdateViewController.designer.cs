// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("UpdateViewController")]
	partial class UpdateViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ProgressLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView ProgressView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TitleText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ProgressLabel != null) {
				ProgressLabel.Dispose ();
				ProgressLabel = null;
			}

			if (ProgressView != null) {
				ProgressView.Dispose ();
				ProgressView = null;
			}

			if (TitleText != null) {
				TitleText.Dispose ();
				TitleText = null;
			}
		}
	}
}
