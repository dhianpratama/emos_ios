// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("IoChartingViewController")]
	partial class IoChartingViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView BodyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView CalendarTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NextButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PrevButton { get; set; }

		[Action ("NextButton_TouchUpInside:")]
		partial void NextButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("PrevButton_TouchUpInside:")]
		partial void PrevButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (BodyView != null) {
				BodyView.Dispose ();
				BodyView = null;
			}

			if (CalendarTable != null) {
				CalendarTable.Dispose ();
				CalendarTable = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (PrevButton != null) {
				PrevButton.Dispose ();
				PrevButton = null;
			}
		}
	}
}
