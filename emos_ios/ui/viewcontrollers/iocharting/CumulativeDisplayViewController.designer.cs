// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CumulativeDisplayViewController")]
	partial class CumulativeDisplayViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView DataView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MainView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SummaryLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView SummaryTableView { get; set; }

		[Action ("CloseButton_TouchUpInside:")]
		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DataView != null) {
				DataView.Dispose ();
				DataView = null;
			}

			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (MainView != null) {
				MainView.Dispose ();
				MainView = null;
			}

			if (SummaryLabel != null) {
				SummaryLabel.Dispose ();
				SummaryLabel = null;
			}

			if (SummaryTableView != null) {
				SummaryTableView.Dispose ();
				SummaryTableView = null;
			}
		}
	}
}
