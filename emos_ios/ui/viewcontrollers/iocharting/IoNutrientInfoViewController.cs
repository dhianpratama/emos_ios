﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public partial class IoNutrientInfoViewController : BaseViewController
	{
		private const float ControlsX = 20;
		private const int TodayNutrientCellHeight = 40;
		public LocationModelSimple Location { get; set; }
		public RegistrationModelSimple Registration { get; set; }
		public PatientConsumptionInfo PatientConsumptionInfo { get; set; }
		public ConsumptionChecklistCalender ConsumptionChecklistCalender { get; set; }
		private DynamicTableSource<NutrientForConsumptionChecklist, NutrientSummaryViewCell> _todayNutrientSource;
		private GroupedTableSource<DishModelSimple, DishConsumption, NutrientBreakdownViewCell> _breakdownNutrientSource;
		private PageTitleAndPatientSummaryView _patientSummaryView;
		private ConsumptionEpicHandler _consumptionEpicHandler;

		public IoNutrientInfoViewController () : base (AppDelegate.Instance, "IoNutrientInfoViewController", null)
		{

		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = Colors.ViewCellBackground;
			ShowHeader ();
			ShowTodayNutrient ();
			ShowNutrientBreakdown ();
			BindToScrollview ();
			SetFonts ();
			InitiateConsumptionEpicHandler ();
			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			SetTextsByLanguage ();
		}
		public IEnumerable<UIBarButtonItem> RightButtons (bool ShowNutrientButtons = false)
		{
			var lastUpdate = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Lastupdate");
			var text = String.Format ("{0}: {1}", lastUpdate, PatientConsumptionInfo.last_update_time.Value.ToDisplayDatetime().ToString ("dd/MM/yyyy hh:mm tt"));
			var lastUpdateText = PatientConsumptionInfo.last_update_time!=null ? text : "";

			var lastUpdateButton = new UIBarButtonItem (lastUpdateText, UIBarButtonItemStyle.Plain, (sender, args) => {});

			var buttons = new List<UIBarButtonItem> {
				//sendToEpicButton, 
				lastUpdateButton
			};
			return buttons;
		}
		private void InitiateConsumptionEpicHandler ()
		{
			_consumptionEpicHandler = new ConsumptionEpicHandler (AppDelegate.Instance) {
				PatientConsumptionInfo = PatientConsumptionInfo,
				Location = Location,
			};
			_consumptionEpicHandler.HandleRequestCompleted += HandleSendToEpicRequestResult;
		}
		private void HandleSendToEpicRequestResult (object sender, RequestEventArgs e)
		{
			OnRequestCompleted (e.Success, "SendDatatoEPIC");
			if (e.Success) {
				this.ShowAlert (AppContext, "InformationsuccessfullysenttoEPIC");
				return;
			}
		}
		private void ShowHeader ()
		{
			_patientSummaryView = new PageTitleAndPatientSummaryView (AppContext, Location, Registration);
			_patientSummaryView.Frame = new RectangleF (0, Measurements.TopY, 768, Measurements.HeaderHeight);
			_patientSummaryView.BackgroundColor = Colors.HeaderBackground;
			View.AddSubview (_patientSummaryView);
			uiScrollView.SetFrameBelowTo (_patientSummaryView, 0, 768, Measurements.BodyHeight, distanceToAbove: 0);
			uiScrollView.ClipsToBounds = true;
			uiScrollView.BackgroundColor = Colors.ViewCellBackground;
			uiScrollView.Opaque = true;
		}
		private void ShowTodayNutrient ()
		{
			var setting = new BaseViewCellSetting ();
			var handler = new NoCallbackViewCellHandler<NutrientForConsumptionChecklist> (AppContext, NutrientSummaryViewCell.CellIdentifier, setting);
			_todayNutrientSource = new DynamicTableSource<NutrientForConsumptionChecklist, NutrientSummaryViewCell> (handler);
			TodayTable.Source = _todayNutrientSource;
			_todayNutrientSource.Items = PatientConsumptionInfo.totalNutrientsToday;
		}
		private void ShowNutrientBreakdown ()
		{
			var setting = new BaseViewCellSetting ();
			var handler = new NoCallbackViewCellHandler<DishConsumption> (AppContext, NutrientBreakdownViewCell.CellIdentifier, setting) {
				GetHeightForRow = GetHeightForNutrientRow
			};
			InitializeNutrientSource (handler);
			BreakdownTable.RowHeight = 55;
			BreakdownTable.ClipsToBounds = true;
			BreakdownTable.Source = _breakdownNutrientSource;
		}
		private void InitializeNutrientSource (NoCallbackViewCellHandler<DishConsumption> handler)
		{
			_breakdownNutrientSource = new GroupedTableSource<DishModelSimple, DishConsumption, NutrientBreakdownViewCell> (handler);
			var dict = new Dictionary<DishModelSimple, List<DishConsumption>> ();
			var sectionTitles = new List<string> ();
			PatientConsumptionInfo.dishConsumptions.ToList ().ForEach (d =>  {
				sectionTitles.Add(d.Dish.dish_type_label);
				List<DishConsumption> d3 = new List <DishConsumption> ();
				d3.Add(d);
				dict.Add (d.Dish, d3);
			});
			_breakdownNutrientSource.SetGroupedItems (dict, sectionTitles);
			//_breakdownNutrientSource.Items = PatientConsumptionInfo.dishConsumptions;
		}
		private void BindToScrollview ()
		{
			TodayLabel.Frame = new RectangleF (ControlsX, 0, 500, 40);
			ValueLabel.SetFrame (325, TodayLabel.Frame.Top);
			CumulativeLabel.SetFrame (430, TodayLabel.Frame.Top);
			TodayTable.SetFrameBelowTo (TodayLabel, 0, 768, TodayNutrientCellHeight * PatientConsumptionInfo.totalNutrientsToday.Count);
			BreakdownLabel.SetFrameBelowTo (TodayTable, ControlsX, distanceToAbove: 50);
			BreakdownTable.SetFrameBelowTo (BreakdownLabel, 0, 768, CalculateBreakdownTableHeight ());
			uiScrollView.ContentSize = new SizeF (768, 100 + TodayLabel.Frame.Height + TodayTable.Frame.Height + BreakdownLabel.Frame.Height + BreakdownTable.Frame.Height);
		}
		private void SetFonts ()
		{
			TodayLabel.Font = Fonts.Header;
			BreakdownLabel.Font = Fonts.Header;
		}
		private float GetHeightForNutrientRow (DishConsumption item)
		{
			var height = NutrientBreakdownViewCell.CalculateRowHeight (item);
			return height;
		}
		private float CalculateBreakdownTableHeight()
		{
			float height = 0;
			foreach (var key in _breakdownNutrientSource.GroupedItems) {
				height += 47;
				foreach (DishConsumption dishConsumption in key.Value) {
					height += NutrientBreakdownViewCell.CalculateRowHeight (dishConsumption);
				}
				height += 1;
			}
			return height;
		}
		public override void SetTextsByLanguage()
		{
			TodayLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString("Today");
			CumulativeLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Cumulative");
			ValueLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Value");
			BreakdownLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Breakdown");
			Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NutrientInfo");
		}
	}
}