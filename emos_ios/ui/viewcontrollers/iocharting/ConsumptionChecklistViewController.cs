﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using emos_ios.tools;
using System.Linq;
using Newtonsoft.Json;

namespace emos_ios
{
	public partial class ConsumptionChecklistViewController : BaseViewController, IConsumptionChecklistMealOrderCallback, ITimePickerCallback, IBeveragesTimePickerCallback
	{
		private const float CellHeight = 150;
		private const float ControlsX = 20;
		private LocationModelSimple _location;
		private RegistrationModelSimple _registration;
		private DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderViewCell> _mealOrderSource;
		private DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderViewCell> _extraOrderSource;
		private DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderDisplayViewCell> _mealOrderDisplaySource;
		private DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderDisplayViewCell> _extraOrderDisplaySource;
		private ConsumptionChecklistMealOrderHandler _mealOrderViewCellHandler;
		private ConsumptionChecklistMealOrderHandler _extraOrderViewCellHandler;
		public ConsumptionChecklistCalender ConsumptionChecklistCalender { get; set; }
		private PatientConsumptionInfo _patientConsumptionInfo;
		private BeverageConsumptionView _beverageConsumptionView;
		private BeverageConsumptionDisplayView _beverageConsumptionDisplayView;
		private ConsumptionChecklistCalender _infoModel;
		private List<DishModelSimple> _dishes, _extraOrders;
		private IConsumptionChecklistCalenderCallback _callback;
		private OperationInfoConsumptionChecklistIpad _savedResult;
		private ConsumptionEpicHandler _consumptionEpicHandler;
		private TimePicker _timePicker;

		public ConsumptionChecklistViewController (LocationModelSimple location, RegistrationModelSimple registration, ConsumptionChecklistCalender model, IConsumptionChecklistCalenderCallback callback) : base (AppDelegate.Instance, "ConsumptionChecklistViewController", null)
		{
			var titleText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ConsumptionCharting");
			Title = titleText;
			_location = location;
			_registration = registration;
			_infoModel = model;
			_callback = callback;
			NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = Colors.ViewCellBackground;
			RemarkTextView.Hidden = true;
			OutsideFoodView.UpdateMask (RoundedRectangle.RoundedTopCorners);
			ShowHeader ();
			_requestHandler.SendRequest (View, RequestPatientConsumptionInfo);
			AppDelegate.Instance.KeyboardHandler.AddKeyboardObservers (View);
		}
		public override void ViewWillDisappear(bool animated)
		{
			AppDelegate.Instance.KeyboardHandler.RemoveKeyboardObservers ();
		}
		private void ShowHeader ()
		{
			HeaderView.Frame = new RectangleF (0, Measurements.TopY, 768, Measurements.HeaderHeight);
			HeaderView.BackgroundColor = Colors.HeaderBackground;
			HeaderView.ClipsToBounds = true;
			var patientSummaryView = new PageTitleAndPatientSummaryView (AppContext, _location, _registration);
			patientSummaryView.Frame = new RectangleF (0, 0, 768, Measurements.HeaderHeight);
			HeaderView.AddSubview (patientSummaryView);
			uiScrollView.SetFrameBelowTo (HeaderView, 0, 768, Measurements.PageHeight - Measurements.HeaderHeight);
			uiScrollView.ClipsToBounds = true;
			uiScrollView.BackgroundColor = Colors.ViewCellBackground;
			uiScrollView.Opaque = true;
			uiScrollView.Hidden = true;
 		}
		public IEnumerable<UIBarButtonItem> RightButtons (bool ShowNutrientButtons = false)
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var saveButton = new UIBarButtonItem (saveText, UIBarButtonItemStyle.Done, (sender, args) => {
				Save (this);
			});
			var buttons = new List<UIBarButtonItem> ();
			if(_infoModel.today_charting)
				buttons.Add (saveButton);

			if (ShowNutrientButtons) {
				var infoImageButton = new UIBarButtonItem (UIImage.FromFile("Images/info-icon-white-22x22.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
					ShowNutrient();
				});
				var nutrientInfoText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NutrientInfo");
				buttons.Add (infoImageButton);
				var infoButton = new UIBarButtonItem (nutrientInfoText, UIBarButtonItemStyle.Plain, (sender, args) => {
					ShowNutrient();
				});
				buttons.Add (infoButton);
			}
			return buttons;
		}
		private void ShowNutrient ()
		{
			var controller = new IoNutrientInfoViewController {
				Location = _location,
				Registration = _registration,
				PatientConsumptionInfo = _patientConsumptionInfo,
				ConsumptionChecklistCalender = _infoModel
			};
			NavigationController.PushViewController (controller, true);
		}
		private void ShowDetail ()
		{
			if (_patientConsumptionInfo.consumption_checklist_id > 0) {
				NavigationItem.SetRightBarButtonItems (RightButtons (true).ToArray (), true);
			}
			if (_patientConsumptionInfo.dishConsumptions.Count > 0) {
				OutsideFoodTakenSwitch.On = _patientConsumptionInfo.outside_food_taken;
				RemarkTextView.Text = _patientConsumptionInfo.remark ?? "";
				RemarkTextView.Hidden = !_patientConsumptionInfo.outside_food_taken;
				ShowMealOrder ();
				ShowExtraOrder ();
				ShowBeverages ();
				BindToScrollview ();
				SetFonts ();
				var lastEditByText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Lasteditedby");
				LastEditedByLabel.Text = String.Format ("{0}: {1}",lastEditByText, _patientConsumptionInfo.last_update_by);
				OutsideFoodTakenSwitch.OnTintColor = AppContext.ColorHandler.MainThemeColor;
				LoadBeverages ();
			} else {
				var text = new UILabel();
				text.Font = UIFont.FromName(Fonts.ValueFontName, 25f);
				text.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nomealorder");
				text.TextColor = UIColor.Red;
				text.Frame = new RectangleF (300, 250, 500, 30);
				HeaderView.AddSubview(text);
			}
			if(_patientConsumptionInfo.consumption_checklist_id==null || _patientConsumptionInfo.consumption_checklist_id==0)
				LastEditedByLabel.Hidden = true;
		}
		private void LoadBeverages ()
		{
			if (_infoModel.operation_code == 3) {
				_beverageConsumptionDisplayView.ExtraBeveragesModel = _patientConsumptionInfo.extraBeveragesWithTime;
				_beverageConsumptionDisplayView.Reload ();
			} else {
				_beverageConsumptionView.ExtraBeveragesModel = _patientConsumptionInfo.extraBeveragesWithTime;
				_beverageConsumptionView.Reload ();
			}
		}
		private void ShowMealOrder ()
		{
			if (_infoModel.operation_code == 3) {
				_mealOrderViewCellHandler = new ConsumptionChecklistMealOrderHandler (ConsumptionChecklistMealOrderDisplayViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_mealOrderDisplaySource = new DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderDisplayViewCell> (_mealOrderViewCellHandler);
				MealOrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				MealOrderTable.Source = _mealOrderDisplaySource;
				MealOrderTable.RowHeight = CellHeight;
				_mealOrderDisplaySource.Items = _dishes;
			} else {
				_mealOrderViewCellHandler = new ConsumptionChecklistMealOrderHandler (ConsumptionChecklistMealOrderViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_mealOrderSource = new DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderViewCell> (_mealOrderViewCellHandler);
				MealOrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				MealOrderTable.Source = _mealOrderSource;
				MealOrderTable.RowHeight = CellHeight;
				_mealOrderSource.Items = _dishes;
			}
		}
		private void ShowExtraOrder ()
		{
			if (_infoModel.operation_code == 3) {
				_extraOrderViewCellHandler = new ConsumptionChecklistMealOrderHandler (ConsumptionChecklistMealOrderDisplayViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_extraOrderDisplaySource = new DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderDisplayViewCell> (_extraOrderViewCellHandler);
				ExtraOrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				ExtraOrderTable.Source = _extraOrderDisplaySource;
				ExtraOrderTable.RowHeight = CellHeight;
				_extraOrderDisplaySource.Items = _extraOrders;
			} else {
				_extraOrderViewCellHandler = new ConsumptionChecklistMealOrderHandler (ConsumptionChecklistMealOrderViewCell.CellIdentifier, this, new BaseViewCellSetting ());
				_extraOrderSource = new DynamicTableSource<DishModelSimple, ConsumptionChecklistMealOrderViewCell> (_extraOrderViewCellHandler);
				ExtraOrderTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				ExtraOrderTable.Source = _extraOrderSource;
				ExtraOrderTable.RowHeight = CellHeight;
				_extraOrderSource.Items = _extraOrders;
			}
		}
		private void ShowBeverages ()
		{
			if (_infoModel.operation_code == 3) {
				_beverageConsumptionDisplayView = new BeverageConsumptionDisplayView ();
				_beverageConsumptionDisplayView.Callback = (IBeveragesTimePickerCallback)this;
				_beverageConsumptionDisplayView.OnTableLayoutChanged += HandleBeverageTableLayoutChanged;
			} else {
				_beverageConsumptionView = new BeverageConsumptionView ();
				_beverageConsumptionView.Callback = (IBeveragesTimePickerCallback)this;
				_beverageConsumptionView.OnTableLayoutChanged += HandleBeverageTableLayoutChanged;
			}
		}
		private void HandleBeverageTableLayoutChanged (object sender, EventArgs e)
		{
			if (_infoModel.operation_code == 3) {
				OutsideFoodView.SetFrameBelowTo (_beverageConsumptionDisplayView, ControlsX, 534, 41);
				RemarkTextView.SetFrameBelowTo (OutsideFoodView, ControlsX, 534, 300, 0);
			} else {
				OutsideFoodView.SetFrameBelowTo (_beverageConsumptionView, ControlsX, 534, 41);
				RemarkTextView.SetFrameBelowTo (OutsideFoodView, ControlsX, 534, 300, 0);
			}

			LastEditedByLabel.SetFrameBelowTo (RemarkTextView, ControlsX, 534, 40, 20);
			float height = _infoModel.operation_code == 3 ? _beverageConsumptionDisplayView.Frame.Height : _beverageConsumptionView.Frame.Height;
			uiScrollView.ContentSize = new SizeF (768, 100 + MealOrderLabel.Frame.Height + MealOrderTable.Frame.Height + ExtraOrderLabel.Frame.Height + ExtraOrderTable.Frame.Height + height + OutsideFoodView.Frame.Height + RemarkTextView.Frame.Height + LastEditedByLabel.Frame.Height);
		}
		private void BindToScrollview ()
		{
			uiScrollView.Hidden = false;
			if (_infoModel.operation_code == 3) {
				uiScrollView.AddSubview (_beverageConsumptionDisplayView);
			} else {
				uiScrollView.AddSubview (_beverageConsumptionView);
			}
			if (_patientConsumptionInfo.dishConsumptions.Count () == 0) {
				ShowNoOrderMode ();
			}

			var mealOrderHeight = _dishes.Count () * CellHeight;
			var extraOrderHeight = _extraOrders.Count () * CellHeight;

			MealOrderLabel.Frame = new RectangleF (ControlsX, 0, 200, 50);
			ConsumptionLabel.Frame = new RectangleF (525, MealOrderLabel.Frame.Top, 200, 50);
			MealOrderTable.SetFrameBelowTo(MealOrderLabel, 0, 768, mealOrderHeight, 0);
			if (_extraOrders.Count == 0) {
				ExtraOrderLabel.Frame = new RectangleF (0, 0, 0, 0);
				ExtraOrderTable.Frame = new RectangleF (0, 0, 0, 0);
				if(_infoModel.operation_code == 3)
					_beverageConsumptionDisplayView.SetFrameBelowTo (MealOrderTable, 0, 650, _beverageConsumptionDisplayView.GetMinimalHeight ());
				else
					_beverageConsumptionView.SetFrameBelowTo (MealOrderTable, 0, 650, _beverageConsumptionView.GetMinimalHeight ());
			} else {
				ExtraOrderLabel.SetFrameBelowTo (MealOrderTable, ControlsX, 768 - ControlsX, 50);
				ExtraOrderTable.SetFrameBelowTo (ExtraOrderLabel, 0, 768, extraOrderHeight);
				if(_infoModel.operation_code == 3)
					_beverageConsumptionDisplayView.SetFrameBelowTo (ExtraOrderTable, 0, 650, _beverageConsumptionDisplayView.GetMinimalHeight ());
				else
					_beverageConsumptionView.SetFrameBelowTo (ExtraOrderTable, 0, 650, _beverageConsumptionView.GetMinimalHeight ());
			}
			HandleBeverageTableLayoutChanged (this, new EventArgs ());
		}
		private void ShowNoOrderMode ()
		{
			var noOrderLabel = new UILabel {
				Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Noorder.")
			};
			noOrderLabel.Frame = new RectangleF (ControlsX, HeaderView.Frame.Height - 50, 768 - ControlsX, 50);
			HeaderView.AddSubview (noOrderLabel);
			return;
		}
		private void SetFonts ()
		{
			MealOrderLabel.Font = Fonts.Header;
			ConsumptionLabel.Font = Fonts.Header;
			ExtraOrderLabel.Font = Fonts.Header;
			OutsideFoodTakenLabel.Font = Fonts.Header;
		}
		private async void RequestPatientConsumptionInfo ()
		{
			var param = KnownUrls.PatientConsumptionInfoQueryString (_registration.registration_id);
			await AppDelegate.Instance.HttpSender.Request<PatientConsumptionInfo> ()
				.From (KnownUrls.GetPatientConsumptionInfo)
				.WithContent (ConsumptionChecklistCalender)
				.WithQueryString (param)
				.WhenSuccess (result => OnPatientConsumptionInfoSuccessful(result))
				.WhenFail (result=> OnRequestCompleted(false, AppDelegate.Instance.LanguageHandler.GetLocalizedString("SendingDatatoEPIC")))
				.Go ();
		}
		private void OnPatientConsumptionInfoSuccessful (PatientConsumptionInfo result)
		{
			OnRequestCompleted (true);
			_patientConsumptionInfo = result;
			InitiateConsumptionEpicHandler (result);

			_dishes = new List<DishModelSimple> ();
			_patientConsumptionInfo.dishConsumptions.Where (e => e.Dish.dish_type_label != "Extra Order").ToList ()
				.ForEach (dish => {
					_dishes.Add(dish.Dish);
				});

			var extraOrdersDishConsumptions = new List<DishConsumption>();
			extraOrdersDishConsumptions = _patientConsumptionInfo.dishConsumptions.Where (e => e.Dish.dish_type_label == "Extra Order")
				.Select (e => e)
				.ToList ();

			if (extraOrdersDishConsumptions != null) {
				extraOrdersDishConsumptions.ForEach (r => {
					var consumption = r.consumption_level;
					r.Dish.label = ("[" + r.dish_quantity.ToString() + "] serves of " + r.Dish.label);
					r.consumption_level = consumption / r.dish_quantity;
					r.Dish.consumptionQuantity = consumption / r.dish_quantity;
				});
				_extraOrders = extraOrdersDishConsumptions.Select (e => e.Dish).ToList ();
			}

			ShowDetail ();
		}
		private void InitiateConsumptionEpicHandler (PatientConsumptionInfo patientConsumptionInfo)
		{
			_consumptionEpicHandler = new ConsumptionEpicHandler (AppDelegate.Instance) {
				PatientConsumptionInfo = patientConsumptionInfo,
				Location = _location,
				Registration = _registration
			};
			_consumptionEpicHandler.HandleRequestCompleted += HandleSendToEpicRequestResult;
		}
		private void HandleSendToEpicRequestResult (object sender, RequestEventArgs e)
		{
			OnRequestCompleted (e.Success, "SendingDatatoEPIC");
			if (e.Success) {
				this.ShowAlert (AppContext, "InformationsuccessfullysenttoEPIC");
				_callback.Refresh ();
				NavigationController.PopViewControllerAnimated (true);
			}
		}
		public void ItemSelected(DishModelSimple selected, int selectedIndex)
		{
		}
		public void OnDecreaseQuantity(double quantity, long dishId)
		{
			if (_patientConsumptionInfo.dishConsumptions != null) {
				for (int i = 0; i < _patientConsumptionInfo.dishConsumptions.Count; i++) {
					if (_patientConsumptionInfo.dishConsumptions [i].dish_id == dishId) {
						if(_patientConsumptionInfo.dishConsumptions[i].consumption_level>0)
							_patientConsumptionInfo.dishConsumptions [i].consumption_level = (decimal)quantity;
						return;
					}
				}
			}
		}
		public void OnIncreaseQuantity(double quantity, long dishId)
		{
			if (_patientConsumptionInfo.dishConsumptions != null) {
				for (int i = 0; i < _patientConsumptionInfo.dishConsumptions.Count; i++) {
					if (_patientConsumptionInfo.dishConsumptions [i].dish_id == dishId) {
						if (_patientConsumptionInfo.dishConsumptions [i].consumption_level < 1)
							_patientConsumptionInfo.dishConsumptions [i].consumption_level = (decimal)quantity;
						return;
					}
				}
			}
		}
		private void Save (MonoTouch.Foundation.NSObject sender)
		{
			var saveModel = new ChecklistSaveModelIpad(){
				consumption_checklist_id = _patientConsumptionInfo.consumption_checklist_id != null ? (long)_patientConsumptionInfo.consumption_checklist_id : 0,
				registration_id = _registration.registration_id,
				institution_id = AppDelegate.Instance.InstitutionId,
				meal_order_date = _infoModel.Date,
				meal_order_period_id = _infoModel.meal_period_id
			};
			saveModel.composite_dish_list = new List<bool>();
			saveModel.dish_id_list = new List<long>();
			saveModel.consumption_level_list = new List<decimal>();
			saveModel.consumption_checklist_dish_id_list = new List<long>();
			saveModel.consumption_time_list = new List<string> ();
			saveModel.free_text_consumption_time_list = new List<string> ();
			saveModel.extraBeverages = _beverageConsumptionView.ExtraBeveragesModel
				.Select(e=> new KeyValuePair<string,string>(e.item,e.value))
				.ToList();
			saveModel.free_text_consumption_time_list = _beverageConsumptionView.ExtraBeveragesModel
				.Select (e => e.consumptionTime)
				.ToList ();
			saveModel.outside_food_taken = OutsideFoodTakenSwitch.On;
			if (OutsideFoodTakenSwitch.On) {
				saveModel.remark = RemarkTextView.Text;
			} else {
				saveModel.remark = "";
			}

			if(_patientConsumptionInfo.dishConsumptions!=null){
				_patientConsumptionInfo.dishConsumptions
					.ForEach(d=> {
						if(d.Dish.dish_type_label == "Extra Order"){
							d.consumption_level = d.dish_quantity * d.consumption_level;
						}
						saveModel.dish_id_list.Add((long)d.dish_id);
						saveModel.consumption_level_list.Add(d.consumption_level);
						saveModel.consumption_checklist_dish_id_list.Add((long)d.consumption_checklist_dish_id);
						saveModel.composite_dish_list.Add(d.composite_dish);
						saveModel.consumption_time_list.Add(d.Dish.consumptionUpdateTime);
					});

				_requestHandler.SendRequest (View, () => SaveRequest (saveModel), AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Saving"));
			}
		}
		private async void SaveRequest(ChecklistSaveModelIpad content)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoConsumptionChecklistIpad>()
				.From (KnownUrls.SaveConsumptionChecklist)
				.WithContent(content)
				.WhenSuccess (result => onSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void onSaveCompleted(OperationInfoConsumptionChecklistIpad result)
		{
			_savedResult = result;
			if (_savedResult.success) {
				if (_savedResult.sendToEpic)
					_requestHandler.SendRequest (View, () => RequestGetNutrient (_savedResult.consumptionChecklistId), "");
				else {
					this.ShowAlert (AppContext, "Informationsaved");
					_callback.Refresh ();
					NavigationController.PopViewControllerAnimated (true);
				}
			} else
				OnRequestCompleted (false);
		}
		public async void RequestGetNutrient(long? consumptionChecklistId)
		{
			var queryString = KnownUrls.GetNutrientsQueryString (consumptionChecklistId);
			await AppDelegate.Instance.HttpSender.Request<NutrientForConsumptionChecklistWithCreateTime>()
				.From (KnownUrls.GetNutrients)
				.WithQueryString(queryString)
				.WhenSuccess (result => GetNutrientSuccesful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		public void GetNutrientSuccesful(NutrientForConsumptionChecklistWithCreateTime result)
		{
			if (result != null && result.nutrients != null) {
				string errorText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SendingDatatoEPIC");
				_requestHandler.SendRequest (View, () => _consumptionEpicHandler.RequestSendToEpic (result), errorText);
			} else {
				OnRequestCompleted (true);
				this.ShowAlert (AppContext, "Nonutrientsfound");
			}
		}
		partial void OutsideFoodTakenSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			RemarkTextView.Hidden = !OutsideFoodTakenSwitch.On;
		}

		public void OnMealsConsumptionTimeClicked(long dishId)
		{
			var timeString = _patientConsumptionInfo.dishConsumptions.FirstOrDefault (e => e.dish_id == dishId).update_time;
			var date = DateTime.Now;
			var times = timeString!=null ? timeString.Split (':') : DateTime.Now.ToString("hh:mm").Split(':') ;
			if (times.Count() > 1) {
				date = new DateTime (date.Year, date.Month, date.Day, Convert.ToInt16(times [0]), Convert.ToInt16(times [1]), 0, DateTimeKind.Utc);
			}
			int? index = null;
			for (int i = 0; i < _patientConsumptionInfo.dishConsumptions.Count; i++) {
				if (dishId == _patientConsumptionInfo.dishConsumptions [i].dish_id)
					index = i;
			}

			_timePicker = new TimePicker ((ITimePickerCallback)this,date,index);
			_timePicker.Selection = TimePickerSelection.Meals;
			NavigationController.PresentViewController (_timePicker, true, null);
		}

		public void DoneTimePicker(TimePickerSelection selection, DateTime? date, int? index)
		{
			if (selection == TimePickerSelection.Meals) {
				if (index != null) {
					_patientConsumptionInfo.dishConsumptions [(int)index].update_time = date.Value.ToString ("H:mm");
					_patientConsumptionInfo.dishConsumptions [(int)index].Dish.consumptionUpdateTime = date.Value.ToString ("H:mm");
				}
				_mealOrderSource.Items = _patientConsumptionInfo.dishConsumptions
					.Where (e => e.Dish.dish_type_label != "Extra Order")
					.Select (e => e.Dish)
					.ToList ();

				_extraOrderSource.Items = _patientConsumptionInfo.dishConsumptions
					.Where (e => e.Dish.dish_type_label == "Extra Order")
					.Select (e => e.Dish)
					.ToList ();
				ExtraOrderTable.ReloadData ();
				MealOrderTable.ReloadData ();

			} else if (selection == TimePickerSelection.Beverages) {
				if (index != null) {
					_patientConsumptionInfo.extraBeveragesWithTime [(int)index].consumptionTime = date.Value.ToString ("H:mm");
				}
				_beverageConsumptionView.ExtraBeveragesModel = _patientConsumptionInfo.extraBeveragesWithTime;
				_beverageConsumptionView.ReloadTable ();
			}
		}

		public void OpenBeveragesTimePicker(ExtraBeverage item, int? index)
		{
			var timeString = item.consumptionTime;
			var date = DateTime.Now;
			var times = timeString.Split (':');
			if (times.Count() > 1) {
				date = new DateTime (date.Year, date.Month, date.Day, Convert.ToInt16(times [0]), Convert.ToInt16(times [1]), 0, DateTimeKind.Utc);
			}

			_timePicker = new TimePicker ((ITimePickerCallback)this,date,index);
			_timePicker.Selection = TimePickerSelection.Beverages;
			NavigationController.PresentViewController (_timePicker, true, null);
		}
		public override void SetTextsByLanguage()
		{
			MealOrderLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("MealOrder");
			ConsumptionLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Consumption");
			OutsideFoodTakenLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("OutsideFoodTaken");
		}
	}
}

