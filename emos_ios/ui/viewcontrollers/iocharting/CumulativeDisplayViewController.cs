﻿
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class CumulativeDisplayViewController : BaseViewController
	{
		public DynamicTableSource<List<KeyValuePair<string,string>>, InfoDataListViewCell> SummaryDataSource;
//		private InfoDataListHandler _handler;
		private List<KeyValuePair<string,string>> _items;
		private float RowHeight = 25;
		private static float FontSize = 16f;
		private ConsumptionChecklistCalender _calendar;

		public CumulativeDisplayViewController (ConsumptionChecklistCalender calendar) : base (AppDelegate.Instance, "CumulativeDisplayViewController", null)
		{
			GenerateData (calendar);
			_calendar = calendar;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			DateLabel.Text = _calendar.Date.ToString ("dddd, dd MMMM yyyy");
			GenerateLabelList (_items);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 500, 400);
		}

		private void GenerateData(ConsumptionChecklistCalender calendar)
		{
			_items = new List<KeyValuePair<string, string>>();
			if (calendar.total_daily_nutrients != null) {
				calendar.total_daily_nutrients
					.ForEach (n => {
						var dec = Math.Round((decimal)n.value,1);
						_items.Add(new KeyValuePair<string, string>(n.label,(dec.ToString())));
				});
			}
		}

		public void GenerateLabelList(List<KeyValuePair<string,string>> items)
		{
			for(int i=0; i < items.Count; i++) {
				var keyLabel = new UILabel();
				keyLabel.Font = UIFont.FromName (Fonts.KeyFontName, 18);
				keyLabel.Text = items [i].Key + " : ";
				keyLabel.Frame = new RectangleF (10, (i * RowHeight) + 25, GetLabelWidth(items[i].Key), RowHeight);
				DataView.AddSubview(keyLabel);

				var size = keyLabel.StringSize(keyLabel.Text, keyLabel.Font, new SizeF(keyLabel.Frame.Width, 20));

				var valueLabel = new UILabel();
				valueLabel.Font = UIFont.FromName(Fonts.ValueFontName, FontSize);
				valueLabel.Text = items [i].Value;
				valueLabel.Frame = new RectangleF (15+size.Width, (i * RowHeight) + 25, GetLabelWidth(items[i].Value), RowHeight);
				valueLabel.LineBreakMode = UILineBreakMode.WordWrap;
				DataView.AddSubview(valueLabel);

			};
		}

		private int GetLabelWidth(string text)
		{
			return (5 + text.Length) * Convert.ToInt32 (FontSize);
		}

		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true,null);
		}
		public override void SetTextsByLanguage()
		{
			SummaryLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Summary");
		}
	}
}

