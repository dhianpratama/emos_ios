﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;

using emos_ios.tools;

namespace emos_ios
{
	public partial class IoChartingViewController : BaseViewController, IConsumptionChecklistCalenderRowCallback, IConsumptionChecklistCalenderCallback
	{
		private const float CellWidth = 90; 
		private const float CellHeight = 90;
		private LocationModelSimple _location;
		private RegistrationModelSimple _registration;
		private IoChartingDateRowView _ioChartingDateRowView;
		private ConsumptionChecklistCalenderRowViewCellSetting _consumptionChecklistViewCellSetting;
		private ConsumptionChecklistCalenderRowViewCellHandler _ConsumptionChecklistCalenderViewCellHandler;
		private DynamicTableSource<MealPeriodForCalender, ConsumptionChecklistCalenderRowViewCell> _ioChartingCalenderSource;
		private float _calendarLeft;
		private ConsumptionChecklistModel _modelPatient;
		private long _registrationId;
		private DateTime _selectedDate;

		public IoChartingViewController (LocationModelSimple location, RegistrationModelSimple registration) : base(AppDelegate.Instance, "IoChartingViewController", null)
		{
			var titleText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("ConsumptionCharting");
			Title = titleText;
			_registrationId = (long) registration.registration_id;
			_location = location;
			_registration = registration;
			_selectedDate = DateTime.SpecifyKind(DateTime.Today, DateTimeKind.Utc);
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ShowHeader ();
			_requestHandler.SendRequest (View, RequestPatientCalenderForConsumptionChecklist);
			BackgroundView.BackgroundColor = Colors.ViewCellBackground;
			HeaderView.BackgroundColor = Colors.ViewCellBackground;
			BodyView.BackgroundColor = Colors.ViewCellBackground;
			NextButton.SetImage (UIImage.FromBundle ("Images/next_button.png"), UIControlState.Normal);
			PrevButton.SetImage (UIImage.FromBundle ("Images/previous_button.png"), UIControlState.Normal);
		}
		private void ShowHeader ()
		{
			HeaderView.Frame = new RectangleF (0, 0, 768, Measurements.HeaderHeight);
			BackgroundView.Frame = new RectangleF (0, Measurements.TopY, 768, Measurements.PageHeight);
			var patientSummaryView = new PageTitleAndPatientSummaryView (AppContext, _location, _registration);
			HeaderView.AddSubview (patientSummaryView);
		}
		public void OnRequestPatientCalenderSuccessful (ConsumptionChecklistModel result)
		{
			OnRequestCompleted (true);
			_modelPatient = result;
			_calendarLeft = 768 - (7 * CellWidth);
			HeaderView.BackgroundColor = Colors.HeaderBackground;
			var BodyViewTop = HeaderView.Frame.Top + HeaderView.Frame.Height;

			var viewHeight = this.View.Frame.Height - this.NavigationController.NavigationBar.Frame.Size.Height;
			BodyView.SetFrameBelowTo (HeaderView, 0, 768, viewHeight - BodyViewTop);

			ShowFirstStep ();
		}
		private void ShowFirstStep ()
		{
			ShowDates ();
			ShowCalendar ();
		}
		private void ShowCalendar ()
		{
			_consumptionChecklistViewCellSetting = new ConsumptionChecklistCalenderRowViewCellSetting {
				CellWidth = CellWidth * _modelPatient.dates.Count (),
				CellHeight = CellHeight,
				ColumnCount = _modelPatient.dates.Count (),
				CalendarLeft = _calendarLeft
			};
			_ConsumptionChecklistCalenderViewCellHandler = new ConsumptionChecklistCalenderRowViewCellHandler (ConsumptionChecklistCalenderRowViewCell.CellIdentifier, this, _consumptionChecklistViewCellSetting);
			_ioChartingCalenderSource = new DynamicTableSource<MealPeriodForCalender, ConsumptionChecklistCalenderRowViewCell> (_ConsumptionChecklistCalenderViewCellHandler);

			var calenderTable = new UITableView {
				Frame = new RectangleF (0, _ioChartingDateRowView.Frame.Height, 768, BodyView.Frame.Height - _ioChartingDateRowView.Frame.Height - Measurements.LogoHeight),
				SeparatorStyle = UITableViewCellSeparatorStyle.None,
				Source = _ioChartingCalenderSource,
				RowHeight = CellHeight,
				UserInteractionEnabled = true
			};

			BodyView.AddSubview (calenderTable);
			_ioChartingCalenderSource.Items = _modelPatient.mealPeriods;
		}
		private void ShowDates ()
		{
			_ioChartingDateRowView = new IoChartingDateRowView (this) {
				Frame = new RectangleF (0, 0, 768, CellHeight),
			};
			_ioChartingDateRowView.Load (_modelPatient, _calendarLeft, CellWidth, CellHeight);
			ViewHandler.ClearSubviews (BodyView);
			BodyView.AddSubview (_ioChartingDateRowView);
		}
		private RectangleF BodyChildRectangleF()
		{
			return new RectangleF (0, 0, 768, BodyView.Frame.Height);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public void ItemSelected(MealPeriodForCalender selected, int selectedIndex)
		{
		}
		public void ItemSelected(ConsumptionChecklistCalender selected, int selectedIndex)
		{
		}
		private async void RequestPatientCalenderForConsumptionChecklist()
		{
			var param = KnownUrls.PatientCalenderConsumptionQueryString (_selectedDate, (long)AppDelegate.Instance.InstitutionId, (long)_registrationId, (long)AppDelegate.Instance.HospitalId);
			await AppDelegate.Instance.HttpSender.Request<ConsumptionChecklistModel> ()
				.From (KnownUrls.GetPatientCalenderForConsumptionChecklist)
				.WithQueryString (param)
				.WhenSuccess (result => OnRequestPatientCalenderSuccessful(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		public void LoadConsumptionList(ConsumptionChecklistCalender model)
		{
			if (model.operation_code == 2 || model.operation_code == 3) {
				var controller = new ConsumptionChecklistViewController (_location, _registration, model, (IConsumptionChecklistCalenderCallback)this);
				controller.ConsumptionChecklistCalender = model;
				AppDelegate.Instance.Nav.PushViewController (controller, true);
			} else if (model.operation_code == 100) {
				var controller = new CumulativeDisplayViewController (model);
				controller.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				controller.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
				NavigationController.PresentViewController (controller, true, null);
			}
		}
		public void Refresh()
		{
			Console.WriteLine ("REFRESH AFTER CONSUMPTION SAVED");
			_requestHandler.SendRequest (View, RequestPatientCalenderForConsumptionChecklist);
		}

		private void GoToPreviousWeek()
		{
			_selectedDate = _selectedDate.AddDays (-7);
			_requestHandler.SendRequest (View, RequestPatientCalenderForConsumptionChecklist);
		}

		private void GoToNextWeek()
		{
			_selectedDate = _selectedDate.AddDays (7);
			_requestHandler.SendRequest (View, RequestPatientCalenderForConsumptionChecklist);
		}

		partial void NextButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			GoToNextWeek();
		}

		partial void PrevButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			GoToPreviousWeek();
		}
	}
}

