// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("ConsumptionChecklistViewController")]
	partial class ConsumptionChecklistViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel ConsumptionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ExtraOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView ExtraOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LastEditedByLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealOrderLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView MealOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OutsideFoodTakenLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch OutsideFoodTakenSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView OutsideFoodView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView RemarkTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SendToEpicButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIScrollView uiScrollView { get; set; }

		[Action ("OutsideFoodTakenSwitch_ValueChanged:")]
		partial void OutsideFoodTakenSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("SendToEpicClicked:")]
		partial void SendToEpicClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ConsumptionLabel != null) {
				ConsumptionLabel.Dispose ();
				ConsumptionLabel = null;
			}

			if (ExtraOrderLabel != null) {
				ExtraOrderLabel.Dispose ();
				ExtraOrderLabel = null;
			}

			if (ExtraOrderTable != null) {
				ExtraOrderTable.Dispose ();
				ExtraOrderTable = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (LastEditedByLabel != null) {
				LastEditedByLabel.Dispose ();
				LastEditedByLabel = null;
			}

			if (MealOrderLabel != null) {
				MealOrderLabel.Dispose ();
				MealOrderLabel = null;
			}

			if (MealOrderTable != null) {
				MealOrderTable.Dispose ();
				MealOrderTable = null;
			}

			if (OutsideFoodTakenLabel != null) {
				OutsideFoodTakenLabel.Dispose ();
				OutsideFoodTakenLabel = null;
			}

			if (OutsideFoodTakenSwitch != null) {
				OutsideFoodTakenSwitch.Dispose ();
				OutsideFoodTakenSwitch = null;
			}

			if (OutsideFoodView != null) {
				OutsideFoodView.Dispose ();
				OutsideFoodView = null;
			}

			if (RemarkTextView != null) {
				RemarkTextView.Dispose ();
				RemarkTextView = null;
			}

			if (SendToEpicButton != null) {
				SendToEpicButton.Dispose ();
				SendToEpicButton = null;
			}

			if (uiScrollView != null) {
				uiScrollView.Dispose ();
				uiScrollView = null;
			}
		}
	}
}
