// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("IoNutrientInfoViewController")]
	partial class IoNutrientInfoViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BreakdownLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView BreakdownTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel CumulativeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TodayLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView TodayTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIScrollView uiScrollView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel ValueLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BreakdownLabel != null) {
				BreakdownLabel.Dispose ();
				BreakdownLabel = null;
			}

			if (BreakdownTable != null) {
				BreakdownTable.Dispose ();
				BreakdownTable = null;
			}

			if (CumulativeLabel != null) {
				CumulativeLabel.Dispose ();
				CumulativeLabel = null;
			}

			if (TodayLabel != null) {
				TodayLabel.Dispose ();
				TodayLabel = null;
			}

			if (TodayTable != null) {
				TodayTable.Dispose ();
				TodayTable = null;
			}

			if (uiScrollView != null) {
				uiScrollView.Dispose ();
				uiScrollView = null;
			}

			if (ValueLabel != null) {
				ValueLabel.Dispose ();
				ValueLabel = null;
			}
		}
	}
}
