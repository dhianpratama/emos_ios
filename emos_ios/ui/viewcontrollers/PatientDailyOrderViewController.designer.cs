// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("PatientDailyOrderViewController")]
	partial class PatientDailyOrderViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MealOperationContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView OrderTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (OrderTable != null) {
				OrderTable.Dispose ();
				OrderTable = null;
			}

			if (MealOperationContainerView != null) {
				MealOperationContainerView.Dispose ();
				MealOperationContainerView = null;
			}
		}
	}
}
