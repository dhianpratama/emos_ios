// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("SettingViewController")]
	partial class SettingViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField DayText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView filterHospitalContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FilterHospitalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField FilterHospitalText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView filterWardContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel FilterWardLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField FilterWardText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView mealGroupContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealGroupLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField OperationText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView switchDayContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SwitchDayLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopView { get; set; }

		[Action ("CloseButton_TouchUpInside:")]
		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("DayText_EditingDidBegin:")]
		partial void DayText_EditingDidBegin (MonoTouch.Foundation.NSObject sender);

		[Action ("FilterHospitalText_EditingDidBegin:")]
		partial void FilterHospitalText_EditingDidBegin (MonoTouch.Foundation.NSObject sender);

		[Action ("FilterWardText_EditingDidBegin:")]
		partial void FilterWardText_EditingDidBegin (MonoTouch.Foundation.NSObject sender);

		[Action ("OperationText_EditingDidBegin:")]
		partial void OperationText_EditingDidBegin (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (DayText != null) {
				DayText.Dispose ();
				DayText = null;
			}

			if (FilterHospitalLabel != null) {
				FilterHospitalLabel.Dispose ();
				FilterHospitalLabel = null;
			}

			if (FilterHospitalText != null) {
				FilterHospitalText.Dispose ();
				FilterHospitalText = null;
			}

			if (FilterWardLabel != null) {
				FilterWardLabel.Dispose ();
				FilterWardLabel = null;
			}

			if (FilterWardText != null) {
				FilterWardText.Dispose ();
				FilterWardText = null;
			}

			if (MealGroupLabel != null) {
				MealGroupLabel.Dispose ();
				MealGroupLabel = null;
			}

			if (OperationText != null) {
				OperationText.Dispose ();
				OperationText = null;
			}

			if (SwitchDayLabel != null) {
				SwitchDayLabel.Dispose ();
				SwitchDayLabel = null;
			}

			if (TopView != null) {
				TopView.Dispose ();
				TopView = null;
			}

			if (switchDayContainerView != null) {
				switchDayContainerView.Dispose ();
				switchDayContainerView = null;
			}

			if (filterHospitalContainerView != null) {
				filterHospitalContainerView.Dispose ();
				filterHospitalContainerView = null;
			}

			if (filterWardContainerView != null) {
				filterWardContainerView.Dispose ();
				filterWardContainerView = null;
			}

			if (mealGroupContainerView != null) {
				mealGroupContainerView.Dispose ();
				mealGroupContainerView = null;
			}
		}
	}
}
