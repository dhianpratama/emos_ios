﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using core_emos;
using System.Linq;

namespace emos_ios
{
	public partial class CompanionOrderListViewController : BaseViewController, ICompanionOrderListCallback
	{
		public DynamicTableSource<CompanionOrder, CompanionOrderListViewCell> CompanionOrderListDataSource;
		private CompanionOrderViewCellHandler _companionOrderViewCellHandler;
		private List<CompanionOrder> _orders;
		public bool IsPatient { get; set; }
		public bool ViewOnly {get; set;}
		private PatientOrderCommand _patientOrderCommand;
		private long? _mealOrderId;

		public CompanionOrderListViewController (PatientOrderCommand patientOrderCommand) : base (AppDelegate.Instance, "CompanionOrderListViewController", null)
		{
			_patientOrderCommand = patientOrderCommand;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			ShowHeader ();
			_requestHandler.SendRequest (View, GetOrderList);
		}

		void ShowHeader ()
		{
			if (!Settings.ShowImageAsHeader)
				return;
			CompanionOrderTitleLabel.Hidden = true;
			var headerImageView = new PageTitleAndPatientSummaryView (AppContext, _patientOrderCommand.Location, _patientOrderCommand.Registration);
			headerImageView.SetHeaderImage (UIImage.FromFile ("Images/companion_order_list_header.png"));
			headerImageView.SetFrame (0, Measurements.TopY, View.Frame.Width, Measurements.HeaderHeight);
			View.AddSubview (headerImageView);
			NotFoundView.SetFrameBelowTo (headerImageView);
			CompanionOrderTable.SetFrameBelowTo (headerImageView);
		}

		private void GenerateRightButton()
		{
			if (ViewOnly)
				return;

			var addText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add");
			var addButton = new UIBarButtonItem(addText, UIBarButtonItemStyle.Done, delegate {
				AppDelegate.Instance.SelectedOperationCode = 1;
				OnEdit(0);
			});
			addButton.TintColor = UIColor.White;

			if (Settings.MultipleCompanionOrder)
				NavigationItem.SetRightBarButtonItem (addButton, false);
			else {
				if (_orders.Count == 0) {
					NavigationItem.SetRightBarButtonItem (addButton, false);
				} else {
					NavigationItem.RightBarButtonItem = null;
				}
			}
		}

		public void Load()
		{
			InitializeHandlers ();
			ShowLayout ();
		}

		private void InitializeHandlers ()
		{
			var setting = new BaseViewCellSetting()
			{

			};
			_companionOrderViewCellHandler = new CompanionOrderViewCellHandler (CompanionOrderListViewCell.CellIdentifier, (ICompanionOrderListCallback)this, setting){
				GetHeightForRow = GetHeightForRow
			};
		}

		private float GetHeightForRow(CompanionOrder item)
		{
			return CompanionOrderListViewCell.GetRowHeight (item);
		}

		private void ShowLayout()
		{
			CompanionOrderListDataSource = new DynamicTableSource<CompanionOrder, CompanionOrderListViewCell> (_companionOrderViewCellHandler);
			CompanionOrderListDataSource.Items = _orders;
			CompanionOrderTable.Source = CompanionOrderListDataSource;
			CompanionOrderTable.ReloadData ();
		}

		private async void GetOrderList()
		{
			var queryString = KnownUrls.GetCompanionOrdersQueryString ((long) _patientOrderCommand.Registration.registration_id, (long)AppDelegate.Instance.InstitutionId, 
				(long)AppDelegate.Instance.SelectedMealOrderPeriod.id, (long)AppDelegate.Instance.SelectedOperationCode, _patientOrderCommand.OperationDate);

			await AppDelegate.Instance.HttpSender.Request<List<CompanionOrder>> ()
				.From (KnownUrls.GetCompanionOrders)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnGetOrderSuccesful (result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}

		private void OnGetOrderSuccesful(List<CompanionOrder> result)
		{
			OnRequestCompleted (true);
			_orders = result;
			GenerateRightButton ();

			if (_orders.Count == 0)
				ShowNotFound ();
			else
				ShowTable ();
			Load ();
		}

		private void ShowTable()
		{
			CompanionOrderTable.Hidden = false;
			NotFoundView.Hidden = true;
		}
		private void ShowNotFound()
		{
			CompanionOrderTable.Hidden = true;
			NotFoundView.Hidden = false;
		}

		public void OnEdit(long id, bool chargeable = true)
		{
			if (Settings.CompanionMealChargeableOption && id == 0) {
				this.ShowSelectionConfirmAlert (AppContext, AppContext.LanguageHandler.GetLocalizedString("Confirmation"),
					AppContext.LanguageHandler.GetLocalizedString("ChargaeableCompanionMealConfirmationMsg"),
					() => ProceedMealOrder (id),() => ProceedMealOrder (id, false) );
				return;
			} 
			else 
			{
				ProceedMealOrder (id, chargeable);
			}
		}

		private void ProceedMealOrder (long id, bool chargeable = true)
		{
			_patientOrderCommand.chargeable = chargeable;
			var call = (ICompanionOrderListCallback)this;
			var vc = new MealOrderViewController (_patientOrderCommand, id, Dashboard.DashboardMenu.CompanionMealOrder, call);
			vc.OnSave += CloseChildAndReload;
			vc.IsPatient = IsPatient;
			NavigationController.PushViewController (vc, true);
		}

		private void CloseChildAndReload (object sender, PatientOrderCommandEventArgs e)
		{
			NavigationController.PopViewControllerAnimated (true);
			Refresh ();
		}
		public void OnDelete(long id)
		{
			this.ShowConfirmAlert (AppContext, "CompanionOrder", "Areyousurewanttodeletethismealorder?", ()=>OnDeleteConfirmed(id));
		}
		private void OnDeleteConfirmed(long id)
		{
			_requestHandler.SendRequest (View, () => DeleteOrder (id));
		}
		public void Refresh()
		{
			_requestHandler.SendRequest (View, GetOrderList);
		}
		public InterfaceStatusModel GetInterfaceStatus()
		{
			return null;
		}
		public void ItemSelected(CompanionOrder item, int idx)
		{

		}

		private async void DeleteOrder(long id)
		{
			var queryString = KnownUrls.DeleteCompanionOrderQueryString (id);

			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.DeleteCompanionOrder)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnDeleteSuccesful(result))
				.WhenFail (result => OnRequestCompleted (false))
				.Go ();
		}

		private void OnDeleteSuccesful(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);
				Refresh ();
			} else {
				OnRequestCompleted (false);
			}
		}
		public override void SetTextsByLanguage()
		{
			if (Settings.ChangeCompanionLabelToLodger) {
				CompanionOrderTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("LodgerOrder");
				CompanionOrderErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nolodgermealorderisfound");
			} else {
				CompanionOrderTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CompanionOrder");
				CompanionOrderErrorMsgLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Nocompanionmealorderisfound");
			}
		}
		public void OnChargeable(long id, bool chargeable = true)
		{
			var text = "Are you sure want to change this companion meal order to chargeable order?";
			if (chargeable)
				text = "Are you sure want to change this companion meal order to non-chargeable order?";
			
			this.ShowSelectionConfirmAlert (AppContext, "Confirmation", 
				text, 
				()=>OnChargeableConfirmation(id), null);
		}
		private void OnChargeableConfirmation(long id)
		{
			_mealOrderId = id;
			_requestHandler.SendRequest (View, () => UpdateChargeable(id));
		}
		private async void UpdateChargeable(long id)
		{
			var queryString = KnownUrls.GetCompanionChargeableQuery (id);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad>()
				.From (KnownUrls.GetUpdateChargeableStatusURL)
				.WithQueryString(queryString)
				.WhenSuccess (result => OnRequestUpdateSuccessfully(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnRequestUpdateSuccessfully(OperationInfoIpad result)
		{
			if (result.success) {
				OnRequestCompleted (true);

				if (_mealOrderId != 0) {
					bool charge = _orders.FirstOrDefault (e => e.companionMealOrderId == _mealOrderId).chargable;
					charge = !charge;

					_orders.FirstOrDefault (e => e.companionMealOrderId == _mealOrderId).chargable = charge;

					CompanionOrderListDataSource.Items = _orders;
					CompanionOrderTable.Source = CompanionOrderListDataSource;
					CompanionOrderTable.ReloadData ();
				}

				this.ShowAlert (AppContext, "Savedsuccessful");
			} else {
				OnRequestCompleted (false);
			}
		}

	}
}
