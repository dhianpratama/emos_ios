﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public partial class PatientDailyOrderViewController : BaseViewController, IPatientDayOrderCallback
	{
		private LocationWithRegistrationSimple _bed;
		private DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> OrderSource;
		private List<PatientOrderCommand> _sequentialOrderCommands;
		private List<NurseDashboardHeaderModel> _headers;
		private PatientDayOrderViewCellHandler _orderHandler;
		private readonly IPatientDayOrderCallback _callback;
		private readonly IPatientMenu _patientMenuCallback;
		public event EventHandler OnFinish;

		public PatientDailyOrderViewController (IPatientMenu patientMenuCallback, IPatientDayOrderCallback callback, NurseDashboardHeaderView headerView, LocationWithRegistrationSimple bed, List<PatientOrderCommand> sequentialOrderCommands) : base (AppDelegate.Instance, "PatientDailyOrderViewController", null)
		{
			_callback = callback;
			_patientMenuCallback = patientMenuCallback;
			_sequentialOrderCommands = sequentialOrderCommands;
			_bed = bed;
			RemoveSequentiallyOrdered (headerView);
			NavigationItem.Title = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NextOrder");
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if (!_headers.Any ()) {
				NavigationController.PopViewControllerAnimated (true);
				OnFinish.SafeInvoke (this);
				return;
			}
			InitializeHandlers ();
			ShowHeader ();
			ShowTable ();
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			_patientMenuCallback.ShouldRunViewWillAppear = true;
			AppContext.LockHandler.UnlockRegistration ();
		}
		private void RemoveSequentiallyOrdered (NurseDashboardHeaderView headerView)
		{
			_headers = headerView.GetNurseDashboardModel ().header;
			_sequentialOrderCommands.ForEach (c => {
				_headers.RemoveAll(h => h.meal_order_period_id == c.MealOrderPeriod.id);
				_bed.registrations [0].mealOrderPeriods.RemoveAll (p => p.id == c.MealOrderPeriod.id);
			});
		}
		private void InitializeHandlers ()
		{
			var cellWidth = (float)Math.Ceiling ((UIScreen.MainScreen.Bounds.Right - Measurements.NurseDashboardOperationContainerLeft) / _headers.Count ());
			var patientDayOrderSetting = new PatientDayOrderViewCellSetting
			{
				CellWidth = cellWidth,
				CellHeight = Measurements.NurseDashboardRowHeight,
				BackgroundColor = UIColor.Clear,
				OrderContainerFrame = new RectangleF (Measurements.NurseDashboardOperationContainerLeft, 0, _headers.Count () * cellWidth, 55)
			};
			_orderHandler = new PatientDayOrderViewCellHandler (PatientDayOrderViewCell.CellIdentifier, this, patientDayOrderSetting);
		}
		private void ShowHeader ()
		{
			HeaderView.BackgroundColor = Colors.HeaderBackground;
			HeaderView.SetFrame (y: Measurements.TopY, height: NurseDashboardHeaderView.HeaderTitleHeight);
			MealOperationContainerView.AddSubviews (CreateMealOperationViews (_headers));
			SetMealOperationContainerViewLayout ();
		}
		public MealOperationView[] CreateMealOperationViews(List<NurseDashboardHeaderModel> headers)
		{
			var result = new List<MealOperationView> ();
			foreach (var mealOperation in headers) {
				var mealOperationView = new MealOperationView (_orderHandler.Setting.CellWidth);
				mealOperationView.SetCellContent (AppContext, mealOperation);
				result.Add (mealOperationView);
			}
			return result.ToArray ();
		}
		private void SetMealOperationContainerViewLayout ()
		{
			for (int i = 0; i < _headers.Count (); i++) {
				var mealOperationView = (MealOperationView)MealOperationContainerView.Subviews [i];
				mealOperationView.Frame = new RectangleF ((_orderHandler.Setting.CellWidth * i), 0, _orderHandler.Setting.CellWidth, 47f);
				mealOperationView.SetLayout ();
			}
		}
		private void ShowTable ()
		{
			OrderTable.SetFrame (0, Measurements.TopY + HeaderView.Frame.Height, 768, Measurements.NurseDashboardRowHeight);
			OrderTable.RowHeight = Measurements.NurseDashboardRowHeight;
			OrderSource = new DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> (_orderHandler);
			OrderSource.Items = new List<LocationWithRegistrationSimple> { _bed };
			OrderTable.Source = OrderSource;
		}
		public void PatientNameClicked (LocationWithRegistrationSimple location)
		{
		}
		public void IdClicked (LocationWithRegistrationSimple location)
		{
		}
		public void OrderButtonClicked (PatientOrderCommand item)
		{
			_patientMenuCallback.ShouldRunViewWillAppear = false;
			_callback.OrderButtonClicked (item);
		}
		public void Refresh ()
		{
		}
		public void OnDashboardChanged (Dashboard.OrderTabs tab, bool refresh = true)
		{
		}
		public void PopToViewControllerAndUnlock (bool unlock = true, bool animated = true)
		{
		}
		public void ShowWardSelectionAlert ()
		{
		}
		public void OpenBirthdayCakeForm (LocationWithRegistrationSimple locationWithRegistration)
		{
		}
		public void CloseBirthdayCakeForm ()
		{
		}
		public void ItemSelected(LocationWithRegistrationSimple selected, int selectedIndex)
		{
		}
		public void SetOrderBy (string orderBy)
		{
		}
	}
}

