// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CaregiverCell")]
	partial class CaregiverCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView patientNameIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel patientNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView phoneNumberIcon { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel phoneNUmberLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel relationLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView relationshipIcon { get; set; }

		[Action ("deleteButtonOnTapped:")]
		partial void deleteButtonOnTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (patientNameLabel != null) {
				patientNameLabel.Dispose ();
				patientNameLabel = null;
			}

			if (phoneNUmberLabel != null) {
				phoneNUmberLabel.Dispose ();
				phoneNUmberLabel = null;
			}

			if (relationLabel != null) {
				relationLabel.Dispose ();
				relationLabel = null;
			}

			if (patientNameIcon != null) {
				patientNameIcon.Dispose ();
				patientNameIcon = null;
			}

			if (phoneNumberIcon != null) {
				phoneNumberIcon.Dispose ();
				phoneNumberIcon = null;
			}

			if (relationshipIcon != null) {
				relationshipIcon.Dispose ();
				relationshipIcon = null;
			}
		}
	}
}
