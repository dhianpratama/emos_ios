﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using ios;
using core_emos;
using System.Linq;
using RaEngine.Service;

namespace emos_ios
{
	public partial class WebPortalRegistrationViewController : BaseViewController, IWebPortalRegistrationModelCallback
	{
		private LocationModelSimple _location;
		private RegistrationModelSimple _registration;
		private WebPortalRegistraionModalSimple _result;
		private WebPortalCaregiverRegistrationViewController _childCareRegistrationVC;

		public WebPortalRegistrationViewController (LocationModelSimple location, RegistrationModelSimple registration) :
		base (AppDelegate.Instance, "WebPortalRegistrationViewController", null)
		{
			_location = location;
			_registration = registration;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			//webPortalTableView.Source = 
			//	new WebPortalRegistrationSource (_result, _registration, _location);

			RequestCareGiverModel ();
			// Perform any additional setup after loading the view, typically from a nib.
		}
		private void RequestCareGiverModel()
		{
			if (_registration.registration_id == 0) {
				_requestHandler.StopRequest ();
				return;
			}

			var param = KnownUrls.GetFeedbackDraftByRegistration (_registration.registration_id);
			_requestHandler.SendRequest (View, () => RequestCareGiver (param));
		}

		private async void RequestCareGiver(string paran){

			var requestFailedText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("RequestFailed");

			await AppDelegate.Instance.HttpSender.Request<WebPortalRegistraionModalSimple> ()
				.From (KnownUrls.GetCareGiverURL)
				.WithQueryString (paran)
				.WhenSuccess (result => OnRequestCareGiverSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false, requestFailedText))
				.Go ();
		}
		private void OnRequestCareGiverSuccessful (WebPortalRegistraionModalSimple result)
		{
			OnRequestCompleted (true);
			_result = result;
			if(result != null)
				webPortalTableView.Source = new WebPortalRegistrationSource (_result, _registration, _location, this);

			webPortalTableView.ReloadData ();
		}
		public void UpdateModel(WebPortalRegistraionModalSimple model)
		{
			_result = model;

			webPortalTableView.Source = new WebPortalRegistrationSource (_result, _registration, _location, this);
			webPortalTableView.ReloadData ();
		}
		public void AddCaregiver()
		{
			_childCareRegistrationVC = new WebPortalCaregiverRegistrationViewController (this);
			UIApplication.SharedApplication.KeyWindow.Add(_childCareRegistrationVC.View);
		}
		public void AddCaregiverModel(CareGiverSimple cg)
		{
			if(!_result.cg.Any(e => e.cg_name == cg.cg_name 
				&& e.cg_relationship == cg.cg_relationship
				&& e.cg_number == cg.cg_number)){

				ExitButtonTapped ();
				_result.cg.Add(cg);
				_requestHandler.SendRequest (View, () => SaveCareGiver (_result));
			}
		}
		public void DeleteCaregiver(CareGiverSimple cg)
		{
			var _toRemove = _result.cg.FirstOrDefault (e => e.cg_name == cg.cg_name
				&& e.cg_relationship == cg.cg_relationship
				&& e.cg_number == cg.cg_number);
			
			if(_toRemove != null){
				_result.cg.Remove (_toRemove);
				_requestHandler.SendRequest (View, () => SaveCareGiver (_result));
			}
		}
		private async void SaveCareGiver(WebPortalRegistraionModalSimple model)
		{
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveCaregiver)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCaregiverCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnSaveCaregiverCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success){	
				webPortalTableView.Source = new WebPortalRegistrationSource (_result, _registration, _location, this);
				webPortalTableView.ReloadData ();
			}else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
		}
		public void ExitButtonTapped()
		{
			if (_childCareRegistrationVC == null)
				return;
			
			_childCareRegistrationVC.View.RemoveFromSuperview ();
			_childCareRegistrationVC.View = null;
			_childCareRegistrationVC = null;
		}
	}
}

