﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class CaregiverCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("CaregiverCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("CaregiverCell");
		public CareGiverSimple models;
		public IWebPortalRegistrationModelCallback _callback;

		public CaregiverCell (IntPtr handle) : base (handle)
		{
		}

		public static CaregiverCell Create ()
		{
			return (CaregiverCell)Nib.Instantiate (null, null) [0];
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			patientNameIcon.Image = UIImage.FromBundle ("Images/user.png");
			phoneNumberIcon.Image = UIImage.FromBundle ("Images/phone2.png");
			relationshipIcon.Image = UIImage.FromBundle ("Images/team1.png");

			if (models != null) {
				patientNameLabel.Text = models.cg_name;
				relationLabel.Text = models.cg_relationship;
				phoneNUmberLabel.Text = models.cg_number;
			}
		}
		partial void deleteButtonOnTapped (MonoTouch.Foundation.NSObject sender){
			if(_callback != null)
				_callback.DeleteCaregiver(models);
		}
	}
}

