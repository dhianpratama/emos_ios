﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;


namespace emos_ios
{
	public class WebPortalRegistrationSource : UITableViewSource, IWebPortalRegistrationOrderViaWeb
	{
		private WebPortalRegistraionModalSimple _model;
		private List<HeaderSectionModel> _headerSectionModel = new List<HeaderSectionModel>();
		private IWebPortalRegistrationModelCallback _callback;

		public WebPortalRegistrationSource (WebPortalRegistraionModalSimple model, RegistrationModelSimple registration, 
			LocationModelSimple _location, IWebPortalRegistrationModelCallback callback)
		{
			_model = model;
			_callback = callback;

			if (registration != null && _location != null && model != null) {
				_headerSectionModel.Add (new HeaderSectionModel {
					title = "Patient Name",
					description = registration.profile_name,
				});

				_headerSectionModel.Add (new HeaderSectionModel {
					title = "ID Number",
					description = registration.registration_number,
				});

				_headerSectionModel.Add (new HeaderSectionModel {
					title = "Bed",
					description = _location.code,
				});

				_headerSectionModel.Add (new HeaderSectionModel {
					title = "Order via Web Portal",
					description = "",
					forOrderViaWebPortal = true,
					orderViaWebPortal = _model.enable_web_portal
				});
			}
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			if (_model.enable_web_portal)
				return 2;
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// TODO: return the actual number of items in the section
			if (section == 0) {
				return _headerSectionModel.Count ();
			} else {
				if (!_model.enable_web_portal)
					return 0;
				else
					return _model.cg.Count () + 1;
			}
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if (section == 0) {
				return "Patient Information";
			} else {
				return "Caregiver Information Section";
			}
		}
		public override float GetHeightForFooter (UITableView tableView, int section)
		{
			return 30f;
		}
		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 60f;
		}
		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if(_model.enable_web_portal && indexPath.Section == 1 && indexPath.Row == 0)
				return 70f;
			return 50f;
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			// TODO: populate the cell with the appropriate data based on the indexPath

			var cell = 	(WebPortalRegistrationCell)
				tableView.DequeueReusableCell (WebPortalRegistrationCell.Key);
			if(cell == null)
				cell = WebPortalRegistrationCell.Create();

			// first section model
			if (indexPath.Section == 0) {
				if (_headerSectionModel.Count () > indexPath.Row) {
					var _temp = _headerSectionModel.ElementAt (indexPath.Row);
					cell.model = _temp;
					if (_temp.forOrderViaWebPortal && _temp.orderViaWebPortal) {
						cell.Accessory = UITableViewCellAccessory.Checkmark;
					} else {
						cell.Accessory = UITableViewCellAccessory.None;
					}
					cell._callback = this;
				}
			} else if (indexPath.Row == 0 && indexPath.Section == 1) {
				var addButtonCell = (WebPortalRegistrationAddCaregiverCell)
					tableView.DequeueReusableCell (WebPortalRegistrationAddCaregiverCell.Key);
				if (addButtonCell == null) {
					addButtonCell = WebPortalRegistrationAddCaregiverCell.Create ();
				}
				addButtonCell._callback = _callback;
				return addButtonCell;
			} else {
				if(_model.cg.Count() > indexPath.Row - 1){
				var cregiverCell = (CaregiverCell)
						tableView.DequeueReusableCell (CaregiverCell.Key);
					if (cregiverCell == null) {
						cregiverCell = CaregiverCell.Create ();
					}
					var _cg = _model.cg [indexPath.Row - 1];

					var _temp = new CareGiverSimple ();
					_temp.cg_name = _cg.cg_name;
					_temp.cg_relationship = _cg.cg_relationship;
					_temp.cg_number = _cg.cg_number;

					cregiverCell._callback = _callback;
					cregiverCell.models = _temp;
					return cregiverCell;
				}
			}
			return cell;
		}
		public void AddCaregiver()
		{
		}
		public void OrderViaWeb(bool order)
		{
			_model.enable_web_portal = order;
			_callback.UpdateModel (_model);
		}

	}
		
	public class HeaderSectionModel
	{
		public String title { get; set;}
		public String description { get; set;}
		public bool forOrderViaWebPortal { get; set;}
		public bool orderViaWebPortal { get; set;}
	}
}

