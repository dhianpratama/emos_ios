﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("WebPortalRegistrationAddCaregiverCell")]
	partial class WebPortalRegistrationAddCaregiverCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addCaregiverButton { get; set; }

		[Action ("addCaregiverButtonTapped:")]
		partial void addCaregiverButtonTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (addCaregiverButton != null) {
				addCaregiverButton.Dispose ();
				addCaregiverButton = null;
			}
		}
	}
}
