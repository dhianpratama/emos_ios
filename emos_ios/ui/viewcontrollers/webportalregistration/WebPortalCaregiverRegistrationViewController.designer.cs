// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("WebPortalCaregiverRegistrationViewController")]
	partial class WebPortalCaregiverRegistrationViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton applyButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton closeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField mobileNumberTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField nameTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField relationTextField { get; set; }

		[Action ("applyButtonTapped:")]
		partial void applyButtonTapped (MonoTouch.Foundation.NSObject sender);

		[Action ("closeButtonTapped:")]
		partial void closeButtonTapped (MonoTouch.Foundation.NSObject sender);

		[Action ("exitButtonTapped:")]
		partial void exitButtonTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (applyButton != null) {
				applyButton.Dispose ();
				applyButton = null;
			}

			if (closeButton != null) {
				closeButton.Dispose ();
				closeButton = null;
			}

			if (nameTextField != null) {
				nameTextField.Dispose ();
				nameTextField = null;
			}

			if (mobileNumberTextField != null) {
				mobileNumberTextField.Dispose ();
				mobileNumberTextField = null;
			}

			if (relationTextField != null) {
				relationTextField.Dispose ();
				relationTextField = null;
			}
		}
	}
}
