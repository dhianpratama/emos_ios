// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("WebPortalRegistrationCell")]
	partial class WebPortalRegistrationCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton descriptionButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel descriptionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel titleLabel { get; set; }

		[Action ("descriptionButtonTapped:")]
		partial void descriptionButtonTapped (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (descriptionLabel != null) {
				descriptionLabel.Dispose ();
				descriptionLabel = null;
			}

			if (titleLabel != null) {
				titleLabel.Dispose ();
				titleLabel = null;
			}

			if (descriptionButton != null) {
				descriptionButton.Dispose ();
				descriptionButton = null;
			}
		}
	}
}
