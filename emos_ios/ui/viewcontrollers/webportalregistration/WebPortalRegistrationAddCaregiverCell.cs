﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public partial class WebPortalRegistrationAddCaregiverCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("WebPortalRegistrationAddCaregiverCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("WebPortalRegistrationAddCaregiverCell");
		public IWebPortalRegistrationModelCallback _callback;

		public WebPortalRegistrationAddCaregiverCell (IntPtr handle) : base (handle)
		{
		}

		public static WebPortalRegistrationAddCaregiverCell Create ()
		{
			return (WebPortalRegistrationAddCaregiverCell)Nib.Instantiate (null, null) [0];
		}
		partial void addCaregiverButtonTapped (MonoTouch.Foundation.NSObject sender)
		{
			_callback.AddCaregiver();
		}
	}
}

