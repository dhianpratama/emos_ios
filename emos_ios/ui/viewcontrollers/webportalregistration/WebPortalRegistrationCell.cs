﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public partial class WebPortalRegistrationCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("WebPortalRegistrationCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("WebPortalRegistrationCell");
		public IWebPortalRegistrationOrderViaWeb _callback;

		public HeaderSectionModel model;


		public WebPortalRegistrationCell (IntPtr handle) : base (handle)
		{
		}

		public static WebPortalRegistrationCell Create ()
		{
			return (WebPortalRegistrationCell)Nib.Instantiate (null, null) [0];
		}
		partial void descriptionButtonTapped (MonoTouch.Foundation.NSObject sender)
		{
			Console.WriteLine("descriptionButtonTapped");
			_callback.OrderViaWeb(!model.orderViaWebPortal);
		}
		public override void LayoutSubviews ()  
		{
			base.LayoutSubviews ();
			if (model != null) {
				titleLabel.Text = model.title;
				descriptionLabel.Text = model.description;

				descriptionButton.UserInteractionEnabled = model.forOrderViaWebPortal;
			}
		}
	}
}

