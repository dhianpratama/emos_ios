﻿using System;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public interface IWebPortalRegistrationModelCallback
	{
		void UpdateModel(WebPortalRegistraionModalSimple model);
		void AddCaregiver();

		void AddCaregiverModel(CareGiverSimple cg);
		void DeleteCaregiver(CareGiverSimple cg);
		void ExitButtonTapped();
	}
	public interface IWebPortalRegistrationOrderViaWeb
	{
		void OrderViaWeb(bool order);
	}
}

