﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class WebPortalCaregiverRegistrationViewController : BaseViewController
	{
		private IWebPortalRegistrationModelCallback _callback;
		private CareGiverSimple _newCareGiver;

		public WebPortalCaregiverRegistrationViewController (IWebPortalRegistrationModelCallback callback) : 
		base (AppDelegate.Instance, "WebPortalCaregiverRegistrationViewController", null)
		{
			_callback = callback;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
		partial void applyButtonTapped (MonoTouch.Foundation.NSObject sender)
		{
			if(!ValidAnswer())
				return;

			_newCareGiver = new CareGiverSimple();
			_newCareGiver.cg_name = nameTextField.Text.Trim();
			_newCareGiver.cg_relationship = relationTextField.Text.Trim();
			_newCareGiver.cg_number = mobileNumberTextField.Text.Trim();

			_callback.AddCaregiverModel(_newCareGiver);
		}
		private bool ValidAnswer(){
			var nameText = nameTextField.Text.Trim();;
			var relationText = relationTextField.Text.Trim();
			var phoneText = mobileNumberTextField.Text.Trim();

			var alertText = "";
			List<String> alertMsg = new List<string> (); 

			if(nameText == "")
				alertMsg.Add("Caregiver's Name\n");
			if(phoneText == "")
				alertMsg.Add("Caregiver's Mobile Number\n");
			if(relationText == "")
				alertMsg.Add("Caregiver's Relationship\n");

			int i = 1;
			alertMsg.ForEach (e => {
				alertText = String.Format("{0}{1}) {2}", alertText, i++.ToString(), e);
			});

			if (alertText == "")
				return true;
			else {
				this.ShowAlert (AppDelegate.Instance, 
					String.Format("Please fill up the following: \n {0}", alertText));
				return false;
			}
		}

		partial void closeButtonTapped (MonoTouch.Foundation.NSObject sender){
			_callback.ExitButtonTapped();
		}

		partial void exitButtonTapped (MonoTouch.Foundation.NSObject sender){
			_callback.ExitButtonTapped();
		}
	}
}

