// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("MealOrderDetailViewController")]
	partial class MealOrderDetailViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BackgroundView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ChooseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CloseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView DescriptionTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView Image { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealServeInfoLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView NameTextView { get; set; }

		[Action ("ChooseButton_TouchDown:")]
		partial void ChooseButton_TouchDown (MonoTouch.Foundation.NSObject sender);

		[Action ("CloseButton_TouchDown:")]
		partial void CloseButton_TouchDown (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BackgroundView != null) {
				BackgroundView.Dispose ();
				BackgroundView = null;
			}

			if (ChooseButton != null) {
				ChooseButton.Dispose ();
				ChooseButton = null;
			}

			if (DescriptionTextView != null) {
				DescriptionTextView.Dispose ();
				DescriptionTextView = null;
			}

			if (Image != null) {
				Image.Dispose ();
				Image = null;
			}

			if (MealServeInfoLabel != null) {
				MealServeInfoLabel.Dispose ();
				MealServeInfoLabel = null;
			}

			if (NameTextView != null) {
				NameTextView.Dispose ();
				NameTextView = null;
			}

			if (CloseButton != null) {
				CloseButton.Dispose ();
				CloseButton = null;
			}
		}
	}
}
