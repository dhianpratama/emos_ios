﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class MealOrderDetailViewController : BaseViewController, IImageUpdated
	{
		private DishModelWithTags _dish;
		private ImageDownloader _imageDownloader = new ImageDownloader ();
		public event EventHandler<BaseEventArgs<DishModelWithTags>> OnSelected;

		public MealOrderDetailViewController (DishModelWithTags dish) : base (AppDelegate.Instance, "MealOrderDetailViewController", null)
		{
			try {
				_dish = dish;
				HiddenLogo = true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				BackgroundView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
				BackgroundView.Layer.BorderWidth = 1.0f;
				DownloadImage (_dish.picture_url, 462, 462);
				DescriptionTextView.Text = _dish.description;
				NameTextView.Text = _dish.label;
				if (Settings.ShowYesNoImageButtonOnMealOrderDetail) {
					CloseButton.SetImage(UIImage.FromFile ("Images/cancel.png"), UIControlState.Normal);
					ChooseButton.SetImage (UIImage.FromFile ("Images/ok.png"), UIControlState.Normal);
				} else {
					CloseButton.SetTitle ("X", UIControlState.Normal);
					ChooseButton.Hidden = true;
				}
				if (Settings.ShowDetailOnImageView) {
					if (Settings.UseSmallCap) {
						MealServeInfoLabel.Text = "(" + AppDelegate.Instance.LanguageHandler.GetLocalizedString ("mealservedmayvaryfromphoto") + ")";
					} else {
						MealServeInfoLabel.Text = "(" + AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Mealservedmayvaryfromphoto") + ")";
					}
					MealServeInfoLabel.Hidden = false;
				} else {
					MealServeInfoLabel.Text = "";
					MealServeInfoLabel.Hidden = true;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewWillLayoutSubviews ()
		{
			try {
				base.ViewWillLayoutSubviews ();
				this.View.Superview.Bounds = new RectangleF (0, 0, 728, 462);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		partial void CloseButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			try {
				DismissViewController(true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			try {
				_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
				_imageDownloader.OnImageFound += HandleOnImageFound;
				_imageDownloader.Download (AppContext, this, imagePath, width, height);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			try {
				InvokeOnMainThread (() => {
					Image.Image = ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this);
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void UpdatedImage (Uri uri)
		{
			try {
				InvokeOnMainThread (() => {
					if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
						Image.Image = ImageDownloader.LazyLoad (uri.ToString (), this);
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			try {
				InvokeOnMainThread (() => {
					Image.Image = ImageDownloader.GetNotAvailableImage ();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		partial void ChooseButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			try {
				OnSelected.SafeInvoke(this, new BaseEventArgs<DishModelWithTags>().Initiate(_dish));
				DismissViewController(true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
	}
}

