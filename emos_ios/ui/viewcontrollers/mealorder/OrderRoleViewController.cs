﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using ios;
using core_emos;

namespace emos_ios
{
	public partial class OrderRoleViewController : BaseViewController//, IMealOrderCallback
	{
		private IApplicationContext _appContext;
		private MealOrderHeaderView _mealOrderHeaderView;
		public bool IsPatient { get; set; }
		public bool IsCompanion { get; set; }
		public event EventHandler<StringEventArgs> OnRoleSelected;
		public OrderRoleViewController (IApplicationContext appContext) : base (appContext, "OrderRoleViewController", null)
		{
			_appContext = appContext;
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ShowBody ();

		}
		private void ShowBody ()
		{
			BodyView.SetFrame (y: Measurements.TopY);
			_mealOrderHeaderView = new MealOrderHeaderView (_appContext);
			_mealOrderHeaderView.IsPatient = IsPatient;
			_mealOrderHeaderView.IsCompanion = IsCompanion;
			BodyView.AddSubview (_mealOrderHeaderView);
			_mealOrderHeaderView.Load (new MealOrderModel (), true, 0);
			NurseButton.SetImage (UIImage.FromBundle ("Images/nurse.png"), UIControlState.Normal);
			PatientButton.SetImage (UIImage.FromBundle ("Images/patient.png"), UIControlState.Normal);
			CaregiverButton.SetImage (UIImage.FromBundle ("Images/caregiver.png"), UIControlState.Normal);
		}
		partial void CaregiverButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			OnRoleSelected.SafeInvoke(this, new StringEventArgs(OrderRole.Caregiver));
		}
		partial void NurseButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			OnRoleSelected.SafeInvoke(this, new StringEventArgs(OrderRole.Nurse));
		}
		partial void PatientButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			OnRoleSelected.SafeInvoke(this, new StringEventArgs(OrderRole.Patient));
		}
		#region Unused callbacks
		public void FilterDish (int dishTypeIndex, int tagIndex)
		{
		}
		public void ShowAllAvailableDish ()
		{
		}
		public void ShowSuitableDishesOnly ()
		{
		}
		public void NextStep ()
		{
		}
		public bool CanOrder ()
		{
			return true;
		}
		public RegistrationModelSimple GetRegistration ()
		{
			return null;
		}
		public MealOrderPeriodModelSimple GetMealOrderPeriod ()
		{
			return null;
		}
		public void ItemSelected(DishModelWithTags selected, int index)
		{
		}
		#endregion
	}
}

