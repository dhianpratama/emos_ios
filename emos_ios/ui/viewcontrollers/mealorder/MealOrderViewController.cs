
using System;
using System.Drawing;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;

using emos_ios.tools;
using emos_ios;
using core_emos;

namespace emos_ios
{
	public partial class MealOrderViewController : BaseViewController, IMealOrdersCallback, IMealOrderCallback
	{
		private const int DishRowHeight = 200;
		private MealOrderHeaderView _mealOrderHeaderView;
		private MealOrderModel _mealOrderModel;
		private int _step = 0;
		private int _tagSelected = 0;
		private long? _mainDishTagId;
		private List<DishModelWithTags> _suitableDishes, _moreDishes, _availableDishes;
		private MealOrderTableView _mealOrderTableView;
		private List<DishModelSimple> _selectedDishes = new List<DishModelSimple>();
		private MealOrderSummaryView _mealOrderSummaryView;
		private MealOrderSaveModelIpad _saveModel;
		private ICompanionOrderListCallback _companionCallback;
		private IPatientDayOrderCallback _callback;
		private Dashboard.DashboardMenu _tabStatus;
		public event EventHandler<PatientOrderCommandEventArgs> OnClose;
		public event EventHandler<PatientOrderCommandEventArgs> OnSave;
		public event EventHandler<PatientOrderCommandEventArgs> OnDecline;
		public bool IsPatient { get; set; }
		public bool IsPatientCalendar { get; set; }
		private PatientOrderCommand _patientOrderCommand;
		private long? _companionOrderId;
		private bool _onlySuitable;
		public bool OnlySuitable { get { return _onlySuitable; } }
		public MealOrderViewController (PatientOrderCommand patientOrderCommand, Dashboard.DashboardMenu tab, IPatientDayOrderCallback callback) : base (AppDelegate.Instance, "MealOrderViewController", null)
		{
			try {
				_tabStatus = tab;
				_patientOrderCommand = patientOrderCommand;
				_callback = callback;
				if (Settings.ShowMealOrderTitle)
					Title = AppContext.LanguageHandler.GetLocalizedString ("MealOrder");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public MealOrderViewController (PatientOrderCommand patientOrderCommand,long? companionOrderId, Dashboard.DashboardMenu tab, ICompanionOrderListCallback callback) : base (AppDelegate.Instance, "MealOrderViewController", null)
		{
			try {
				_tabStatus = tab;
				_companionCallback = callback;
				_companionOrderId = companionOrderId;
				_patientOrderCommand = patientOrderCommand;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			try {
				// Releases the view if it doesn't have a superview.
				base.DidReceiveMemoryWarning ();

				// Release any cached data, images, etc that aren't in use.
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewDidLoad ()
		{
			try {
				base.ViewDidLoad ();
				HeaderView.Frame = new RectangleF (0, Measurements.TopY, 768, Measurements.MealOrderHeaderHeight);
				View.BackgroundColor = Colors.MealOrderBackground;
				HeaderView.BackgroundColor = UIColor.White;
				AppDelegate.Instance.KeyboardHandler.AddKeyboardObservers (View);
				RequestModel ();

				infoLabel.Hidden = !Settings.ShowInfoMsgOnMealOrdering;
				infoLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Picturesareforillustrationspurposesonly");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public override void ViewWillDisappear(bool animated)
		{
			AppDelegate.Instance.KeyboardHandler.RemoveKeyboardObservers ();
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void RequestModel ()
		{
			try {
				switch (_tabStatus) {
				case Dashboard.DashboardMenu.NurseDashboard:
				case Dashboard.DashboardMenu.PatientMealOrder:
					if (AppDelegate.Instance.SelectedOperationCode == (long?)OperationCodeStatus.OperationCodes.ViewDraft)
						_requestHandler.SendRequest (View, RequestMealOrderDraftModel);
					else
						_requestHandler.SendRequest (View, RequestMealOrderModel);
					break;
				case Dashboard.DashboardMenu.CompanionMealOrder:
					_requestHandler.SendRequest (View, RequestCompanionMealOrderModel);
					break;
				};
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			try {
				if (AppDelegate.Instance.SelectedOperationCode == (long?)OperationCodeStatus.OperationCodes.Ordered) {
					return new UIBarButtonItem[] {
						CreateInfoButton (), 
					};
				}

				var rightButtons = new List<UIBarButtonItem> ();
				if (OperationCodeHandler.IsViewExistingOrder (AppDelegate.Instance.SelectedOperationCode)) {
					rightButtons.Add (CreateChangeOrderButton ());
					if (_mealOrderModel!=null && !_mealOrderModel.put_on_hold && !Settings.HideMealOnHold) {
						rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());
						rightButtons.Add (CreateShortCutOnHoldButton ());
					}
				}else
					rightButtons.Add (CreateSaveButton ());

				rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());

				if (!Settings.HideSaveDraft && (AppDelegate.Instance.SelectedOperationCode == 1 
					|| AppDelegate.Instance.SelectedOperationCode == 11) && _tabStatus != Dashboard.DashboardMenu.CompanionMealOrder) {
					rightButtons.Add (CreateSaveDraftButton ());
					rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());
				}
				if (Settings.EnableDeclineMealOrder && 
					AppDelegate.Instance.SelectedOperationCode != (long?)OperationCodeStatus.OperationCodes.Decline &&
					!IsPatient) {
					rightButtons.Add (CreateDeclineButton ());
					rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());
				}
				if (Settings.ShowGotoPatientModeButtonOnMealOrder && _companionCallback == null) {
					rightButtons.Add (CreateSwitchOrderRoleButton ());
					rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());
				}
				rightButtons.Add (CreateInfoButton ());
				return rightButtons.ToArray ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		public IEnumerable<UIBarButtonItem> DummyLeftButtons ()
		{
			try {
				return new UIBarButtonItem[1] { CreateDummyButton () };
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		UIBarButtonItem CreateDummyButton ()
		{
			try {
				return new UIBarButtonItem (" ", UIBarButtonItemStyle.Done, (sender, args) => {
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			try {
				return new UIBarButtonItem[1] { CreateBackButton () };
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateBackButton()
		{
			try {
				return new UIBarButtonItem ("Back", UIBarButtonItemStyle.Done, (sender, args) => {
					OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand));
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateInfoButton()
		{
			try {
				return new UIBarButtonItem (UIImage.FromFile("Images/info-icon-white-22x22.png"), UIBarButtonItemStyle.Plain, (sender, args) => {
					ShowPatientInfo();
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		#region Create Button
		private UIBarButtonItem CreateSaveButton()
		{
			try {
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString("Order"), 
					UIBarButtonItemStyle.Done, (sender, args) =>  {
						PreSave();
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateChangeOrderButton()
		{
			try {
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Change") 	,			
					UIBarButtonItemStyle.Plain, (sender, args) => {
						AppDelegate.Instance.SelectedOperationCode = (long?)OperationCodeStatus.OperationCodes.ChangeOrder;
						if (_tabStatus == Dashboard.DashboardMenu.CompanionMealOrder)
							_requestHandler.SendRequest (View, RequestCompanionMealOrderModel);
						else
							_requestHandler.SendRequest (View, RequestMealOrderModel);
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateShortCutOnHoldButton()
		{
			try {
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Hold"), 
					UIBarButtonItemStyle.Plain, (sender, args) => {
						AppDelegate.Instance.SelectedOperationCode = (long?)OperationCodeStatus.OperationCodes.ChangeOrder;
						if (_tabStatus == Dashboard.DashboardMenu.CompanionMealOrder)
							_requestHandler.SendRequest (View, RequestCompanionMealOrderHoldModel);
						else
							_requestHandler.SendRequest (View, RequestMealOrderHoldModel);
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateDeclineButton()
		{
			try {
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Decline"), 
					UIBarButtonItemStyle.Plain, (sender, args) => {
						var title = AppContext.LanguageHandler.GetLocalizedString ("Decline");
						var message = String.Format ("{0}?", AppContext.LanguageHandler.GetLocalizedString ("Areyousurewanttodeclinethemeal"));
						this.ShowConfirmAlert (AppContext, title, message, () => {
							_requestHandler.SendRequest (View, DeclineMealOrder);
						}, false);
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateSaveDraftButton()
		{
			try {
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString("SaveDraft"),
					UIBarButtonItemStyle.Plain, (sender, args) =>  {
						SaveDraftOrder();
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private UIBarButtonItem CreateSwitchOrderRoleButton()
		{
			try {
				var key = _patientOrderCommand.OrderBy == OrderRole.Nurse ? "GoToPatientMode" : "GoToNurseMode";
				return new UIBarButtonItem (AppDelegate.Instance.LanguageHandler.GetLocalizedString (key),
					UIBarButtonItemStyle.Plain, (sender, args) => {
						SwitchOrderRole ();
					});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		#endregion

		private void SwitchOrderRole ()
		{
			try {
				if (_patientOrderCommand.OrderBy == OrderRole.Nurse)
					ApplyRoleSettingAndRefresh (OrderRole.Patient);
				else
					AppContext.LoadAuthentication (HandleOnAuthenticationSuccess);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleOnAuthenticationSuccess (object sender, EventArgs e)
		{
			try {
				ApplyRoleSettingAndRefresh (OrderRole.Nurse);
				AppContext.LoadMenu(Menu.EmosMenu.NurseDashboard);
				//OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ApplyRoleSettingAndRefresh (string orderRole)
		{
			try {
				ApplyRoleSetting (orderRole);
				OnMealOrderLoadRequestSuccessful (_mealOrderModel);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ApplyRoleSetting (string orderRole)
		{
			try {
				_patientOrderCommand.OrderBy = orderRole;
				IsPatient = orderRole == OrderRole.Patient;
				_callback.SetOrderBy (orderRole);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowPatientInfo ()
		{
			try {
				AppContext.ControllerLoader.PatientInfoLoader.Load (AppContext);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private class IndexList
		{
			public int tagIdx { get; set; }
			public DishTypeModelSimple type { get; set; }
		}
		private void RemoveEmptyDishType()
		{
			try {
				if (!AppDelegate.Instance.CurrentUser.Authorized ("ACCESS_MORE_DISHES")) {
					List<IndexList> idx = new List<IndexList> ();
					var temp = _mealOrderModel;
					for (int i = 0; i < temp.tagsMealOrder.Count; i++) {
						for (int j = 0; j < temp.tagsMealOrder [i].types.Count; j++) {
							if (!temp.tagsMealOrder [i].dishes.Any (e => e.dish_type_id == temp.tagsMealOrder [i].types [j].id && !e.isAdditionalDish)) {
								idx.Add (new IndexList () {
									tagIdx = i,
									type = temp.tagsMealOrder[i].types[j]
								});
							}
						}
					}

					idx.ForEach (e => {
						_mealOrderModel.tagsMealOrder[e.tagIdx].types.Remove(e.type);
					});
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private bool InvalidOrderModel (MealOrderModel model)
		{
			try {
				if (!OperationCodeHandler.IsViewExistingOrder (AppDelegate.Instance.SelectedOperationCode)) {
					if (model.expired_diet_order) {
						this.ShowAlert (AppContext, "ExpiredDietsAlertMsg", action: () => OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand)));
						return true;
					}
					else if (model.no_food_texture) {
						this.ShowAlert (AppContext, "FoodTextureAlertMsg", action: () => OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand)));
						return true;
					}
					else if (model.tagsMealOrder.Count == 0) {
						this.ShowAlert (AppContext, "Therearenosuitabledishesavailableduetopatient'srestrictionortreatmentcategory", action: () => OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand)));
						return true;
					}
				}
				return false;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		private void InitiateMealOrder (MealOrderModel mealOrder)
		{
			try {
				_mainDishTagId = mealOrder.main_dish_tag_id;
				for (int i = 0; i < mealOrder.tagsMealOrder.Count; i++) {
					if (_mainDishTagId == mealOrder.tagsMealOrder [i].tag.id) {
						_tagSelected = i;
						break;
					}
				}
				if (OperationCodeHandler.IsViewExistingOrder (AppDelegate.Instance.SelectedOperationCode))
					GenerateSummaryDataForViewOnly ();
				else
					InitiateSelectedDish (_mealOrderModel, _tagSelected);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void InitiateSelectedDish (MealOrderModel mealOrder, int tagIndex)
		{
			try {
				ResetOrder (tagIndex);
				if (!Settings.LoadExistingOrderOnEdit && AppDelegate.Instance.SelectedOperationCode != 
					(long?)OperationCodeStatus.OperationCodes.ViewDraft)
					return;
				if (_mealOrderModel.main_dish_tag_id == null)
					return;
				if (_mealOrderModel.main_dish_tag_id != _mealOrderModel.tagsMealOrder [tagIndex].tag.id)
					return;
				if (AppDelegate.Instance.SelectedOperationCode != (long?)OperationCodeStatus.OperationCodes.ChangeOrder &&
					AppDelegate.Instance.SelectedOperationCode != (long?)OperationCodeStatus.OperationCodes.ViewDraft)
					return;
				if (mealOrder.orderedDishes == null)
					return;
				_selectedDishes.Clear ();
				var tags = Settings.HideSkippedStep ? mealOrder.tagsMealOrder [tagIndex].types.Where (r => !r.skip).ToList () : mealOrder.tagsMealOrder [tagIndex].types.ToList ();
				tags.ForEach (dishType => {
					_selectedDishes.Add (mealOrder.orderedDishes.FirstOrDefault (o => o.dish_type_id == dishType.id));
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowFirstStep ()
		{
			try {
				ViewHandler.ClearSubviews (BodyView);
				_mealOrderHeaderView.SelectCategory (_tagSelected);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowSummary(IEnumerable<DishModelSimple> items, bool isViewExistingOrder = false)
		{
			try {
				ViewHandler.ClearSubviews (BodyView);
				if (_mealOrderSummaryView != null)
					_mealOrderSummaryView.Dispose ();

				bool companionOrder = false;
				if (_tabStatus == Dashboard.DashboardMenu.CompanionMealOrder)
					companionOrder = true;

				var setting = new MealOrderSummaryViewSetting {
					Controller = this,
					MealOrderModel = _mealOrderModel,
					SelectedDishes = items,
					IsViewOnly = isViewExistingOrder,
					IsPatient = IsPatient,
					IsCompanionMeal = companionOrder,
					CurrentAndNextMealPeriodExtraOrder = Settings.CurrentAndNextMealPeriodExtraOrder,
					ExtraOrderQuantityInMealOrderHidden = Settings.ExtraOrderQuantityInMealOrderHidden,
					MaximumRemarkLength = Settings.MaximumRemarkLengthInMealOrder,
					SelectedTagId = _mainDishTagId ?? 0
				};
				_mealOrderSummaryView = new MealOrderSummaryView (setting);
				var scrollView = new UIScrollView {
					Frame = new RectangleF (0, 2, 768, BodyView.Frame.Height - 2),
					ContentSize = new SizeF (768, _mealOrderSummaryView.Frame.Height),
					AutoresizingMask = UIViewAutoresizing.FlexibleHeight,
				};
				_mealOrderSummaryView.Remark = _mealOrderModel.freetext1;
				_mealOrderSummaryView.UserInteractionEnabled = !isViewExistingOrder;
				scrollView.AddSubview (_mealOrderSummaryView);

				infoLabel.Hidden = true;
				BodyView.AddSubview (scrollView);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowHeader ()
		{
			try {
				ViewHandler.ClearSubviews (HeaderView);
				_mealOrderHeaderView = new MealOrderHeaderView (AppContext) {
					Frame = new RectangleF(0, 0, 768, Measurements.MealOrderHeaderHeight),
				};
				CheckForOneDishMeal ();

				_mealOrderHeaderView.Callback = this;
				_mealOrderHeaderView.IsPatient = IsPatient;
				_mealOrderHeaderView.IsCompanion = _tabStatus == Dashboard.DashboardMenu.CompanionMealOrder;
				_mealOrderHeaderView.OnStepSelected += OnStepSelected;
				HeaderView.AddSubview (_mealOrderHeaderView);
				_mealOrderHeaderView.Load (_mealOrderModel, OperationCodeHandler.IsViewExistingOrder(AppDelegate.Instance.SelectedOperationCode), _tagSelected);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnStepSelected (object sender, MealOrderHeaderEventArgs e)
		{
			try {
				if (_mealOrderHeaderView.CurrentStep > e.SelectedStep ||
					AllStepWithSuitableDishOrdered (e.SelectedStep)) {
					_mealOrderHeaderView.SelectStep (e.SelectedStep);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private bool MainDishOrdered()
		{
			try {
				if (_selectedDishes == null || _selectedDishes.Count == 0)
					return false;

				return _selectedDishes [0] != null;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		private bool AllStepWithSuitableDishOrdered(int step)
		{
			try {
				if (Settings.MandatoryMainDish) {
					if (!MainDishOrdered ())
						return false;
				}
				if (Settings.IgnoreIncompleteOrder)
					return true;

				for (int i = _mealOrderHeaderView.CurrentStep; i < step; i++) {
					if (_mealOrderModel.tagsMealOrder [_tagSelected].types [i].skip)
						continue;
					var availableDishes = _mealOrderModel.dishes.Where (d => d.dish_type_id == _mealOrderModel.tagsMealOrder [_tagSelected].types [i].id &&
						d.dishTags.Any (dt => dt.id == _mealOrderHeaderView.CurrentMealTag.id));
					var suitableDishes = availableDishes.Where (e => !e.isAdditionalDish).ToList ();
					if (suitableDishes.Count > 0 && (i == _selectedDishes.Count || _selectedDishes [i] == null))
						return false;
				}
				return true;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		public void ItemSelected(List<DishModelWithTags> selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ItemSelected(DishModelWithTags selected, int selectedIndex)
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void FilterDish(int dishTypeIndex, int tagIndex)
		{
			try {
				ViewHandler.ClearSubviews (BodyView);
				var dishTypes = Settings.HideSkippedStep ?
					_mealOrderModel.tagsMealOrder [_tagSelected].types.Where(r => r.skip == false).ToList()
					:
					_mealOrderModel.tagsMealOrder [_tagSelected].types.ToList();
				if (dishTypeIndex == dishTypes.Count) {
					if (dishTypes.Count > 0) {
						_mealOrderTableView.SetItems (new List<DishModelWithTags> (), false, false);
						_mealOrderTableView.Refresh ();
						ShowSummary (_selectedDishes.Where (e => e != null));
					} else {
						this.ShowAlert (AppContext, "Nodishavailableforthismealperiod", action: () => OnClose.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand)));
					}
				} else {
					if (dishTypeIndex == 0 && tagIndex != _tagSelected) {
						ResetOrder (tagIndex);
					}
					_step = dishTypeIndex;
					_tagSelected = tagIndex;
					_mainDishTagId = _mealOrderModel.tagsMealOrder [_tagSelected].tag.id;
					_availableDishes = _mealOrderModel.dishes
						.Where (d => d.dish_type_id == _mealOrderHeaderView.CurrentDishType.id &&
							d.dishTags.Any (dt => dt.id == _mealOrderHeaderView.CurrentMealTag.id))
						.ToList ();
					_suitableDishes = _availableDishes.Where (e => !e.isAdditionalDish).ToList();
					_moreDishes = _availableDishes.Where (e => e.isAdditionalDish).ToList ();
					SelectDefaultDish ();
					MarkSelectedDish ();

					var availableTitleText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("available");
					var suitableTitleText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("suitable");

					var availabilityText = String.Format ("({0} available, {1} suitable)", _availableDishes.Count (), _suitableDishes.Count ());
					var translatedAvailabilityText = String.Format ("({0} {1}, {2} {3})", _availableDishes.Count (), availableTitleText, _suitableDishes.Count (), suitableTitleText);

					if (ShowAvailability ()) {
						var addThereIsNoDishText = false;
						if (_availableDishes.Count == 0) {
							addThereIsNoDishText = true;
						} else {
							if (IsPatient) {
								if (_suitableDishes.Count == 0) {
									addThereIsNoDishText = true;
								}
							}
						}
						if (addThereIsNoDishText) {
							availabilityText = String.Format ("There is no suitable dish under '{0}' ", dishTypes [dishTypeIndex].label);
							var strMsg = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therearenosuitabledishunder'{0}'"),
								dishTypes[dishTypeIndex].label);
							translatedAvailabilityText = strMsg;
						}
					} else {
						availabilityText = "";
						translatedAvailabilityText = "";
						if (_suitableDishes.Count == 0) {
							availabilityText = String.Format( "There is no suitable dish under '{0}' ", dishTypes [dishTypeIndex].label);
							var strMsg = String.Format (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Therearenosuitabledishunder'{0}'"),
								dishTypes [dishTypeIndex].label); 
							translatedAvailabilityText = strMsg;
						}
					}

					if (_mealOrderTableView == null) {
						IMealOrderCallback callback = (IMealOrderCallback)this;
						_mealOrderTableView = new MealOrderTableView (AppContext, this, callback, 
							availabilityText, BodyView.Frame.Height, _mealOrderModel.view_more_dishes_up);
						_mealOrderTableView.Frame = new RectangleF (0, 0, 768, BodyView.Frame.Height);
						_mealOrderTableView.SetSeparatorStyle (MealOrderViewCell.SeparatorStyle);
					}
					SetAvailabilityText (availabilityText, translatedAvailabilityText);
					CheckMoreDishButtonVisibility (_moreDishes);
					_mealOrderTableView.FilterBySuitableSegment ();
					BodyView.AddSubview (_mealOrderTableView);

					infoLabel.Hidden = !Settings.ShowInfoMsgOnMealOrdering;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public bool ShowAvailability ()
		{
			try {
				return !Settings.HideAvailability && AppContext.CurrentUser.Authorized ("ACCESS_MORE_DISHES");
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		public void OrderDish(List<DishModelWithTags> dish)
		{
			try {
				CheckOneDishMealOrder (dish);
				if(_step < _selectedDishes.Count && _step == 0)
					ResetDishType ();

				if(_step < _selectedDishes.Count)
					_selectedDishes [_step] = dish.First ();
				_mealOrderHeaderView.Order (_step);
				_step++;

				OrderMeal (dish.First ());
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ShowAllDishes ()
		{
			try {
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void CheckOneDishMealOrder (List<DishModelWithTags> dish)
		{
			try {
				if (!Settings.OneDishMeal)
					return;
				if (_step < _selectedDishes.Count && _selectedDishes [_step] != null && _selectedDishes [_step] != dish.First ()) {
					if (_selectedDishes [_step].mutualDishTypeList != dish.First ().mutualDishTypeList) {
						for (int i = _step; i < _selectedDishes.Count; i++)
							_selectedDishes [i] = null;
					}
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void CancelDishOrder(List<DishModelWithTags> dish)
		{
			try {
				_selectedDishes [_step] = null;
				_mealOrderHeaderView.CancelOrder (_step);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OrderMeal(DishModelWithTags dish)
		{
			try {
				if (Settings.OneDishMeal) {
					if (dish.mutualDishTypeList != null && dish.mutualDishTypeList.Count > 0) {
						SkipOneDishMealType (dish);
						return;
					} else if (_step == 1) {
						ResetOneDishMealType ();
						return;
					}
				}
				_mealOrderHeaderView.SelectStep (_step);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SelectDefaultDish ()
		{
			try {
				if (AppContext.SelectedOperationCode != 1)
					return;
				if (_selectedDishes [_step] != null)
					return;
				if (_mealOrderModel.dishTypes [_step].skip)
					return;
				if (_mealOrderModel.default_dishes == null)
					return;

				var defaultDish = _mealOrderModel.default_dishes.FirstOrDefault (d => d.dish_type_id == _mealOrderHeaderView.CurrentDishType.id);
				if (defaultDish == null || defaultDish.dish_type_id == null || !defaultDish.dish_ids.Any ())
					return;
				_selectedDishes [_step] = _mealOrderModel.dishes.First (d => d.id == defaultDish.dish_ids.First ());
				_mealOrderHeaderView.Order (_step);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ResetDishType(){
			try {
				_mealOrderModel.tagsMealOrder [_tagSelected].types.ForEach (r => r.skip = false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ResetOneDishMealType()
		{
			try {
				_mealOrderHeaderView.SkipStepButtons (_mealOrderModel, _step, false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SkipOneDishMealType(DishModelWithTags dish)
		{
			try {
				int stepShouldGo = _step;
				List<int> typeids = new List<int> ();

				var temp = _mealOrderModel;
				for (int a = _step; a < temp.tagsMealOrder [_tagSelected].types.Count; a++) {
					if (dish.mutualDishTypeList.Any (r => r == temp.tagsMealOrder [_tagSelected].types [a].id)) {
						typeids.Add(a);
					} else {
						if (stepShouldGo == _step) {
							stepShouldGo = a;
						}
					}
				}
				typeids.ForEach (e => {
					_mealOrderModel.tagsMealOrder [_tagSelected].types[e].skip = true;
				});

				_mealOrderHeaderView.SkipStepButtons (_mealOrderModel, _step, true);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ShowDetail(List<DishModelWithTags> dishes)
		{
			try {
				var controller = new MealOrderDetailViewController (dishes.First ());
				controller.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				controller.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;
				controller.OnSelected += HandleDishSelection;
				NavigationController.PresentViewController (controller, true, null);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void HandleDishSelection (object sender, BaseEventArgs<DishModelWithTags> e)
		{
			try {
				OrderDish (new List<DishModelWithTags> { e.Value });
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private bool IsOrderValid()
		{
			try {
				bool valid = true;
				var orderedIds = GetOrderedIds ();
				if (orderedIds.Count() == 0) {
					this.ShowAlert (AppContext, "Nodishselected");
					return false;
				}
				var stepsCount = Settings.HideSkippedStep ?
					_mealOrderModel.tagsMealOrder [_tagSelected].types.Where (r => r.skip == false).Count ()
					:
					_mealOrderModel.tagsMealOrder [_tagSelected].types.Count ();
				if (!AllStepWithSuitableDishOrdered (stepsCount)) {
					this.ShowAlert (AppContext, "Pleasecompleteyourmealorder");
					return false;
				}

				var mealOnHold = _mealOrderSummaryView != null ? _mealOrderSummaryView.MealOnHold : _mealOrderModel.put_on_hold;
				var earlyDelivery = _mealOrderSummaryView != null ? _mealOrderSummaryView.EarlyDelivery : _mealOrderModel.early_delivery;
				if (mealOnHold && earlyDelivery) {
					this.ShowAlert (AppContext, "CantSelectBothEarlyDeliveryAndMealOnHold");
					return false;
				}
				return valid;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		private void ConfirmAndSave()
		{
			try {
				var orderedIds = GetOrderedIds ();
				if (!Settings.IgnoreIncompleteOrder || orderedIds.Count == _mealOrderHeaderView.StepButtons.Count - 1) {
					SaveOrder ();
					return;
				}
				var title = AppContext.LanguageHandler.GetLocalizedString ("IncompleteOrder");
				var message = String.Format ("{0}?", AppContext.LanguageHandler.GetLocalizedString ("Continue"));
				this.ShowConfirmAlert (AppContext, title, message, () => {
					SaveOrder ();
				}, false);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void ConfirmHoldAndSave()
		{
			try {
				SaveHold ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private List<long> GetOrderedIds()
		{
			try {
				return _selectedDishes.Where (d => d != null).Where (d => d.id != null).Select (d => (long)d.id).ToList ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		private void PreSave()
		{
			try {
				if (!IsOrderValid ())
					return;
				ConfirmAndSave ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SaveOrder()
		{
			try {
				GenerateSaveModel ();

				switch (_tabStatus) {
				case Dashboard.DashboardMenu.NurseDashboard:
				case Dashboard.DashboardMenu.PatientMealOrder:
					_requestHandler.SendRequest (View, SaveMealOrderReqeust);
					break;
				case Dashboard.DashboardMenu.CompanionMealOrder:
					_requestHandler.SendRequest (View, SaveCompanionOrderRequest);
					break;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SaveHold()
		{
			try {
				GenerateHoldModel ();

				switch (_tabStatus) {
				case Dashboard.DashboardMenu.NurseDashboard:
				case Dashboard.DashboardMenu.PatientMealOrder:
					_requestHandler.SendRequest (View, SaveMealOrderReqeust);
					break;
				case Dashboard.DashboardMenu.CompanionMealOrder:
					_requestHandler.SendRequest (View, SaveCompanionOrderRequest);
					break;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SaveDraftOrder()
		{
			try {
				bool valid = true;
				var orderedIds = GetOrderedIds ();
				if (orderedIds.Count() == 0) {
					this.ShowAlert (AppContext, "Nodishselected");
					valid = false;
				}

				if (valid) {
					GenerateSaveModel ();
					_requestHandler.SendRequest (View, SaveMealOrderDraftRequest);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}


		private void ResetOrder(int tagIndex)
		{
			try {
				_selectedDishes.Clear ();
				var tags = Settings.HideSkippedStep ? _mealOrderModel.tagsMealOrder [tagIndex].types.Where (r => !r.skip).ToList () : _mealOrderModel.tagsMealOrder [tagIndex].types.ToList ();
				tags.ForEach (dishType =>  {
					_selectedDishes.Add (null);
				});
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void GenerateSummaryDataForViewOnly()
		{
			try {
				if(_mealOrderModel.tagsMealOrder.Count>0)
					_mealOrderModel.tagsMealOrder [_tagSelected].types.Clear();

				if (_mealOrderModel.orderedDishes != null) {
					_selectedDishes.Clear ();
					_mealOrderModel.orderedDishes
						.ForEach (od => {
							_selectedDishes.Add(od);
						});
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}

		private void MarkSelectedDish()
		{
			try {
				for (int i = 0; i < _availableDishes.Count (); i++) {
					_availableDishes [i].active_data = false;
				};
				for (int i = 0; i < _availableDishes.Count (); i++) {
					for (int j = 0; j < _selectedDishes.Count (); j++) {
						if (_selectedDishes [j] != null) {
							if (_availableDishes [i].id == _selectedDishes [j].id) {
								_availableDishes [i].active_data = true;
								return;
							}
						}
					}
				};
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void CheckForOneDishMeal(){
			try {
				if (!Settings.OneDishMeal)
					return;

				//			if (AppDelegate.Instance.SelectedOperationCode != 11)
				//				return;

				if (!OperationCodeHandler.EditModeAndValidForOneDishMeal (AppDelegate.Instance.SelectedOperationCode))
					return;

				foreach (DishModelSimple dish in _mealOrderModel.orderedDishes.ToList()) {
					var mutual = dish.mutualDishTypeList.Where(r => r != dish.dish_type_id).ToList();
					if (dish.mutualDishTypeList.Count > 0 && _mealOrderModel.tagsMealOrder.Count > _tagSelected) {
						foreach (var a in _mealOrderModel.tagsMealOrder [_tagSelected].types.ToList()) {
							if(mutual.Any(r => r == a.id)){
								a.skip = true;
							}
						}
					}
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ShowAllAvailableDish()
		{
			try {
				_onlySuitable = false;
				_mealOrderTableView.SelectSuitability (Suitability.Suitabilities.All);
				_mealOrderTableView.SetItems (_availableDishes, _availableDishes.Count != _suitableDishes.Count, false);
				_mealOrderTableView.Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void ShowSuitableDishesOnly()
		{
			try {
				_onlySuitable = true;
				_mealOrderTableView.SelectSuitability (Suitability.Suitabilities.Suitable);
				_mealOrderTableView.SetItems (_suitableDishes, _availableDishes.Count != _suitableDishes.Count, true);
				_mealOrderTableView.Refresh ();	
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public bool NonSuitableDishSelected ()
		{
			try {
				return _availableDishes.Any (v => v.active_data && v.isAdditionalDish);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		public List<DishModelWithTags> MarkOrderedDishesFromDb(List<DishModelWithTags> dishes)
		{
			try {
				if (_mealOrderModel.orderedDishes != null) {
					if (!dishes.Any (d => d.active_data)) {
						dishes.ForEach (d => {
							if (_mealOrderModel.orderedDishes.Any (od => od.id == d.id)) {
								d.active_data = true;
							}
						});
					}
				}
				return dishes;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		public void NextStep()
		{
			try {
				_step++;
				_mealOrderHeaderView.SelectStep (_step);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public bool CanOrder ()
		{
			try {
				return !OperationCodeHandler.IsViewExistingOrder (AppDelegate.Instance.SelectedOperationCode);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return false;
			}
		}
		public RegistrationModelSimple GetRegistration ()
		{
			try {
				return _patientOrderCommand.Registration;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		public MealOrderPeriodModelSimple GetMealOrderPeriod ()
		{
			try {
				return _patientOrderCommand.MealOrderPeriod;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return null;
			}
		}
		public void CheckMoreDishButtonVisibility(List<DishModelWithTags> moreDishes)
		{
			try {
				if (IsPatient || !AppContext.CurrentUser.Authorized ("ACCESS_MORE_DISHES")) {
					_mealOrderTableView.SetSuitableSegmentHidden(true);
					return;
				}
				_mealOrderTableView.SetSuitableSegmentHidden (moreDishes.Count () == 0);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void SetAvailabilityText(string str, string translatedStr)
		{
			try {
				_mealOrderTableView.SetAvailabilityText (translatedStr);
				if (str != "") {
					var firstWord = str.Substring (0, str.IndexOf (' '));
					if (firstWord == "There")
						_mealOrderTableView.SetAvailabilityText (translatedStr);
					else {
						if (IsPatient)
							_mealOrderTableView.SetAvailabilityText ("");
					}
				}
				if (Settings.ShowAvailabilityInMealOrderTabs)
					_mealOrderHeaderView.SetTitle (_availableDishes, _suitableDishes);
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		#region Request
		private async void SaveMealOrderDraftRequest()
		{
			try {
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.SaveMealOrderDraft)
					.WithContent (_saveModel)
					.WhenSuccess (result => OnSaveDraftSuccessful())
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestMealOrderModel()
		{
			try {
				bool patientMode = false;
				if (_tabStatus == Dashboard.DashboardMenu.PatientMealOrder)
					patientMode = true;

				var param = KnownUrls.GetMealOrderModelQueryString (AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, AppDelegate.Instance.OperationDate, patientMode);
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.GetMealOrderModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnMealOrderLoadRequestSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestMealOrderHoldModel()
		{
			try {
				bool patientMode = false;
				if (_tabStatus == Dashboard.DashboardMenu.PatientMealOrder)
					patientMode = true;

				var param = KnownUrls.GetMealOrderModelQueryString (AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, AppDelegate.Instance.OperationDate, patientMode);
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.GetMealOrderModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnMealOrderHoldLoadRequestSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestMealOrderDraftModel()
		{
			try {
				bool patientMode = false;
				if (_tabStatus == Dashboard.DashboardMenu.PatientMealOrder)
					patientMode = true;

				var param = KnownUrls.GetMealOrderModelQueryString (AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, AppDelegate.Instance.OperationDate, patientMode);
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.GetMealOrderDraftModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnMealOrderLoadRequestSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void DeclineMealOrder()
		{
			try {
				var param = KnownUrls.DeclineMealOrderQueryString (AppDelegate.Instance.InstitutionId, 
					AppDelegate.Instance.SelectedLocation.id, 
					_mealOrderModel.meal_order_id,
					_mealOrderModel.menu_cycle_menu_id,
					AppDelegate.Instance.SelectedMealOrderPeriod.id,
					AppDelegate.Instance.SelectedRegistration.registration_id,
					AppDelegate.Instance.OperationDate.Date);
				await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
					.From (KnownUrls.DeclineMealOrder)
					.WithQueryString (param)
					.WhenSuccess (result => OnDeclineSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void SaveMealOrderReqeust()
		{
			try {
				await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
					.From (KnownUrls.SaveMealOrder)
					.WithContent (_saveModel)
					.WhenSuccess (result => OnSaveSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestCompanionMealOrderModel()
		{
			try {
				var param = KnownUrls.GetCompanionMealOrderModelQueryString (_companionOrderId, AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, _patientOrderCommand.OperationDate, IsPatient);
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.GetCompanionMealOrderModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnMealOrderLoadRequestSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void RequestCompanionMealOrderHoldModel()
		{
			try {
				var param = KnownUrls.GetCompanionMealOrderModelQueryString (_companionOrderId, AppDelegate.Instance.SelectedRegistration.registration_id, 
					AppDelegate.Instance.SelectedMealOrderPeriod.id, AppDelegate.Instance.SelectedOperationCode, AppDelegate.Instance.OperationDate, IsPatient);
				await AppDelegate.Instance.HttpSender.Request<MealOrderModel> ()
					.From (KnownUrls.GetCompanionMealOrderModel)
					.WithQueryString (param)
					.WhenSuccess (result => OnMealOrderHoldLoadRequestSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private async void SaveCompanionOrderRequest()
		{
			try {
				await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
					.From (KnownUrls.SaveCompanionOrder)
					.WithContent (_saveModel)
					.WhenSuccess (result => OnSaveSuccessful(result))
					.WhenFail (result=> OnRequestCompleted(false))
					.Go ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		#endregion

		#region result from services
		public void OnMealOrderHoldLoadRequestSuccessful (MealOrderModel result)
		{
			try {
				OnRequestCompleted (true);
				_mealOrderModel = result;
				SaveHold ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		public void OnMealOrderLoadRequestSuccessful (MealOrderModel result)
		{
			try {
				OnRequestCompleted (true);
				_onlySuitable = true;
				if (AppDelegate.Instance.SelectedOperationCode != (long?)OperationCodeStatus.OperationCodes.Ordered) {
					if (InvalidOrderModel (result)) {
						BodyView.Hidden = true;
						return;
					}
				}
				_mealOrderModel = result;
				NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
				if (!IsPatientCalendar && Settings.ReplaceMealOrderBackButton && _patientOrderCommand.OrderBy == OrderRole.Patient)
					NavigationItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
				//else
				//NavigationItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);

				RemoveEmptyDishType ();
				ShowHeader ();
				InitiateMealOrder (_mealOrderModel);
				BodyView.SetFrameBelowTo (HeaderView, 0, 768, Measurements.BottomY - (HeaderView.Frame.Top + HeaderView.Frame.Height), 0);

				if (OperationCodeHandler.IsViewExistingOrder (AppDelegate.Instance.SelectedOperationCode)) {
					ShowSummary (_mealOrderModel.orderedDishes, true);
				} else {
					ShowFirstStep ();
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnSaveSuccessful(OperationInfoIpad result)
		{
			try {
				if (result.success) {
					OnRequestCompleted (true);
					this.ShowAlert (AppContext, "Mealsuccessfullyordered");
				} else {
					OnRequestCompleted (false);
				}
				OnSave.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand));
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnSaveDraftSuccessful()
		{
			try {
				OnRequestCompleted (true);
				this.ShowAlert (AppContext, "Mealssuccessfullysavedintodraft");
				NavigationController.PopViewControllerAnimated (true);
				if (_tabStatus == Dashboard.DashboardMenu.CompanionMealOrder)
					_companionCallback.Refresh ();
				//			else
				//				_callback.Refresh ();
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private void OnDeclineSuccessful(OperationInfoIpad result)
		{
			try {
				if (result.success) {
					OnRequestCompleted (true);
					this.ShowAlert (AppContext, "Mealdeclined");
					OnSave.SafeInvoke (this, new PatientOrderCommandEventArgs (_patientOrderCommand));
				} else {
					OnRequestCompleted (false);
					NavigationController.PopViewControllerAnimated (true);
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		#endregion

		#region Generate Models
		private void GenerateHoldModel()
		{
			try {
				var orderedIds = _selectedDishes.Where (d => d != null).Where (d => d.id != null).Select (d => (long)d.id).ToList ();
				var menuIds = _selectedDishes.Where (d => d != null).Where (d => d.id != null).Select (d => (long?)d.menu_id).ToList ();

				_saveModel = new MealOrderSaveModelIpad () {
					meal_order_id = _mealOrderModel.meal_order_id,
					meal_order_draft_id = _mealOrderModel.meal_order_draft_id,
					institution_id = AppDelegate.Instance.InstitutionId,
					registration_id = AppDelegate.Instance.SelectedRegistration.registration_id,
					location_id = AppDelegate.Instance.SelectedLocation.id,
					meal_order_date = DateTime.SpecifyKind (AppDelegate.Instance.OperationDate, DateTimeKind.Utc),
					meal_order_period_id = AppDelegate.Instance.SelectedMealOrderPeriod.id,
					menu_cycle_menu_id = _mealOrderModel.menu_cycle_menu_id,
					ward_group_id = AppDelegate.Instance.WardGroupId,
					main_dish_tag_id = _mainDishTagId,
					orderedDishIdList = orderedIds,
					order_performed_by = AppDelegate.Instance.CurrentUser.Id,
					hospital_id = AppDelegate.Instance.HospitalId,
					class_id = _mealOrderModel.class_id,
					treatment_category_id = _mealOrderModel.treatment_category_id,

					therapeutic_id_list = _mealOrderModel.therapeutic_id_list,
					trial_id_list = _mealOrderModel.trial_id_list,
					clear_feed_id_list = _mealOrderModel.clear_feed_id_list,
					full_feed_id_list = _mealOrderModel.full_feed_id_list,
					allergy_id_list = _mealOrderModel.allergy_id_list,

					halal_meal_order = _mealOrderModel.halal_meal_order,
					stay_for_current_meal = _mealOrderSummaryView != null ? _mealOrderSummaryView.StayForMeal : _mealOrderModel.stay_for_current_meal,
					put_on_hold = true,
					use_as_going_remark = _mealOrderSummaryView != null ? _mealOrderSummaryView.UseAsOngoingRemark : _mealOrderModel.use_as_going_remark,
					bigger_portion = _mealOrderSummaryView != null ? _mealOrderSummaryView.BiggerPortion : _mealOrderModel.bigger_portion,
					less_salt = _mealOrderSummaryView != null ? _mealOrderSummaryView.LessSalt : _mealOrderModel.less_salt,
					less_oily = _mealOrderSummaryView != null ? _mealOrderSummaryView.LessOily : _mealOrderModel.less_oily,
					more_gravy = _mealOrderSummaryView != null ? _mealOrderSummaryView.MoreGravy : _mealOrderModel.more_gravy,
					no_garnishes = _mealOrderSummaryView != null ? _mealOrderSummaryView.NoGarnishes : _mealOrderModel.no_garnishes,
					early_delivery = false,
					garlic_onion = _mealOrderSummaryView != null ? _mealOrderSummaryView.SelectedGarlicOnion : _mealOrderModel.garlic_onion,
					spiciness_level = _mealOrderSummaryView != null ? _mealOrderSummaryView.SelectedSpicinessLevel : _mealOrderModel.spiciness_level,
					profile_id = AppDelegate.Instance.SelectedRegistration.profile_id.Value,
					SnackAndBeverageDietOrderEnteredBy = _mealOrderModel.SnackAndBeverageDietOrderEnteredBy,
					order_by_patient = IsPatient,
					menu_ids = menuIds,
				};

				_mealOrderModel.extraOrders
					.ForEach (e => {
						e.details.ForEach (d => {
							_saveModel.specialInstructionIdList.Add ((long)d.id);
							_saveModel.instructionQuantityList.Add (Convert.ToInt32(e.quantity));
						});
					});

				if (_mealOrderSummaryView != null) {
					_saveModel.freetext1 = _mealOrderSummaryView.Remark;
				}
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		private List<DishModelSimple> GetMenuOfTheDayDishes ()
		{
			var result = new List<DishModelSimple> ();
			if (_mealOrderModel.MenuOfTheDayTags == null)
				return result;
			if (!_mealOrderModel.MenuOfTheDayTags.Any ())
				return result;
			var dishTypes = _mealOrderModel.MenuOfTheDayTags.Where (m => m.tag_id == _mainDishTagId && m.dishTypes != null && m.dishTypes.Any ()).SelectMany (m => m.dishTypes).ToList ();
			if (_selectedDishes [0].vegetarian_meal)
				result = dishTypes.Where (t => t.VegetableDish != null).Select (d => d.VegetableDish).ToList ();
			else
				result = dishTypes.Where (t => t.Dish != null).Select (d => d.Dish).ToList ();
			return result;
		}
		private void GenerateSaveModel()
		{
			try {
				var orderedIds = _selectedDishes.Where (d => d != null).Where (d => d.id != null).Select (d => (long)d.id).ToList ();
				var menuIds = _selectedDishes.Where (d => d != null).Where (d => d.id != null).Select (d => (long?)d.menu_id).ToList ();

			var menuOfTheDayDishes = GetMenuOfTheDayDishes ();
			if (menuOfTheDayDishes != null && menuOfTheDayDishes.Any ()) {
				menuOfTheDayDishes.ForEach (m => {
					orderedIds.Add((long)m.id);
					menuIds.Add((long)m.menu_id);
				});
			}

				_saveModel = new MealOrderSaveModelIpad () {
					meal_order_id = _mealOrderModel.meal_order_id,
					meal_order_draft_id = _mealOrderModel.meal_order_draft_id,
					institution_id = AppDelegate.Instance.InstitutionId,
					registration_id = AppDelegate.Instance.SelectedRegistration.registration_id,
					location_id = AppDelegate.Instance.SelectedLocation.id,
					meal_order_date = _patientOrderCommand.OperationDate,
					meal_order_period_id = AppDelegate.Instance.SelectedMealOrderPeriod.id,
					menu_cycle_menu_id = _mealOrderModel.menu_cycle_menu_id,
					ward_group_id = AppDelegate.Instance.WardGroupId,
					main_dish_tag_id = _mainDishTagId,
					orderedDishIdList = orderedIds,
					order_performed_by = String.IsNullOrEmpty(_patientOrderCommand.OrderBy) ? AppDelegate.Instance.CurrentUser.Id : _patientOrderCommand.OrderBy,
					hospital_id = AppDelegate.Instance.HospitalId,
					class_id = _mealOrderModel.class_id,
					treatment_category_id = _mealOrderModel.treatment_category_id,

					therapeutic_id_list = _mealOrderModel.therapeutic_id_list,
					trial_id_list = _mealOrderModel.trial_id_list,
					clear_feed_id_list = _mealOrderModel.clear_feed_id_list,
					full_feed_id_list = _mealOrderModel.full_feed_id_list,
					allergy_id_list = _mealOrderModel.allergy_id_list,

					halal_meal_order = _mealOrderModel.halal_meal_order,
					stay_for_current_meal = _mealOrderSummaryView != null ? _mealOrderSummaryView.StayForMeal : _mealOrderModel.stay_for_current_meal,
					put_on_hold = _mealOrderSummaryView != null ? _mealOrderSummaryView.MealOnHold : _mealOrderModel.put_on_hold,
					use_as_going_remark = _mealOrderSummaryView != null ? _mealOrderSummaryView.UseAsOngoingRemark : _mealOrderModel.use_as_going_remark,
					bigger_portion = _mealOrderSummaryView != null ? _mealOrderSummaryView.BiggerPortion : _mealOrderModel.bigger_portion,
					less_salt = _mealOrderSummaryView != null ? _mealOrderSummaryView.LessSalt : _mealOrderModel.less_salt,
					less_oily = _mealOrderSummaryView != null ? _mealOrderSummaryView.LessOily : _mealOrderModel.less_oily,
					more_gravy = _mealOrderSummaryView != null ? _mealOrderSummaryView.MoreGravy : _mealOrderModel.more_gravy,
					no_garnishes = _mealOrderSummaryView != null ? _mealOrderSummaryView.NoGarnishes : _mealOrderModel.no_garnishes,
					early_delivery = _mealOrderSummaryView != null ? _mealOrderSummaryView.EarlyDelivery : _mealOrderModel.early_delivery,
					garlic_onion = _mealOrderSummaryView != null ? _mealOrderSummaryView.SelectedGarlicOnion : _mealOrderModel.garlic_onion,
					spiciness_level = _mealOrderSummaryView != null ? _mealOrderSummaryView.SelectedSpicinessLevel : _mealOrderModel.spiciness_level,
					profile_id = AppDelegate.Instance.SelectedRegistration.profile_id.Value,
					SnackAndBeverageDietOrderEnteredBy = _mealOrderModel.SnackAndBeverageDietOrderEnteredBy,
					order_by_patient = IsPatient,
					menu_ids = menuIds,
				};

				if (Settings.CurrentAndNextMealPeriodExtraOrder) {
					if (_mealOrderModel.extraOrders.Count > 0) {
						_mealOrderModel.extraOrders [0].details.ForEach (e => {
							_saveModel.specialInstructionIdList.Add (e.id.Value);
							_saveModel.instructionQuantityList.Add (Convert.ToInt32 (e.quantity));
						});
					}
					if (_mealOrderModel.extraOrders.Count > 1) {
						_mealOrderModel.extraOrders [1].details.ForEach (e => {
							_saveModel.followingSpecialInstructionIds.Add (Convert.ToInt32 (e.id.Value));
							_saveModel.followingSpecialInstructionQuantities.Add ((int)(e.quantity ?? 0));
						});
					}
				} else {
					_mealOrderModel.extraOrders
						.ForEach (e => {
							e.details.ForEach (d => {
								_saveModel.specialInstructionIdList.Add ((long)d.id);
								_saveModel.instructionQuantityList.Add (Convert.ToInt32 (e.quantity));
							});
						});
				}

				if (_mealOrderSummaryView != null) {
					_saveModel.freetext1 = _mealOrderSummaryView.Remark;
				}

				if (Settings.CompanionMealChargeableOption)
					_saveModel.chargeable = _patientOrderCommand.chargeable;
			} catch (Exception) {
				this.ShowAlert (AppContext, "ExceptionOccured");
				return;
			}
		}
		#endregion
	}
}
