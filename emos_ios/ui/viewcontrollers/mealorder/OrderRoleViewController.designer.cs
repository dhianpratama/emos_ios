// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("OrderRoleViewController")]
	partial class OrderRoleViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIView BodyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CaregiverButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NurseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PatientButton { get; set; }

		[Action ("CaregiverButton_TouchDown:")]
		partial void CaregiverButton_TouchDown (MonoTouch.Foundation.NSObject sender);

		[Action ("NurseButton_TouchDown:")]
		partial void NurseButton_TouchDown (MonoTouch.Foundation.NSObject sender);

		[Action ("PatientButton_TouchDown:")]
		partial void PatientButton_TouchDown (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (NurseButton != null) {
				NurseButton.Dispose ();
				NurseButton = null;
			}

			if (PatientButton != null) {
				PatientButton.Dispose ();
				PatientButton = null;
			}

			if (CaregiverButton != null) {
				CaregiverButton.Dispose ();
				CaregiverButton = null;
			}

			if (BodyView != null) {
				BodyView.Dispose ();
				BodyView = null;
			}
		}
	}
}
