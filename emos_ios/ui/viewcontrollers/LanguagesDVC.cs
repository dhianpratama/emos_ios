﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Web;
using emos_ios.tools;
using System.Reflection;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public partial class LanguageDVC : BaseDVC
	{
		RootElement _languagesOption;
		UserLanguageModelSimple _userLanguage;

		public LanguageDVC (IApplicationContext appContext) : base (appContext)
		{
			//LogoY = Measurements.FrameTop;
			_userLanguage = AppContext.CurrentUser.SelectedLanguage;
			CreateRoot (AppContext.LanguageHandler.GetLocalizedString ("DishLanguage"));
			CreateSaveButton ();
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var dishLanguageText = AppContext.LanguageHandler.GetLocalizedString ("DishLanguage");
			var languageText = AppContext.LanguageHandler.GetLocalizedString ("Language");

			_languagesOption = new RadioGroupDVC<LanguageModelSimple> ()
				.CreateRadioGroup (languageText, _userLanguage.language_id, AppContext.Languages);

			Root.Add (new Section (dishLanguageText) {
				_languagesOption
			});
		}
		private void CreateSaveButton()
		{
			var saveText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Save");
			var rightButton = new UIBarButtonItem(saveText, UIBarButtonItemStyle.Plain, delegate {
				OnSave();
			});
			rightButton.TintColor = UIColor.White;
			NavigationItem.SetRightBarButtonItem (rightButton, false);
		}

		private void OnSave()
		{
			long? id = 0;
			if(AppContext.Languages.Count > _languagesOption.RadioSelected )
				 id = AppContext.Languages [_languagesOption.RadioSelected].id;

			_userLanguage.language_id = id;
			_requestHandler.SendRequest (View, ()=> Save (_userLanguage));
		}
		private async void Save(UserLanguageModelSimple model)
		{
			await AppDelegate.Instance.HttpSender.Request<UserLanguageModelSimple> ()
				.From (KnownUrls.SavePatientLanguage)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted())
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}

		private void OnSaveCompleted()
		{
			OnRequestCompleted (true);
			AppContext.CurrentUser.SelectedLanguage = _userLanguage;
			this.ShowAlert (AppContext, "Savedsuccessful");
		}
	}
}
