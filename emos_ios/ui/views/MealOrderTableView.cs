﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	[Register("MealOrderTableView")]
	public partial class MealOrderTableView : UIView
	{
		private IApplicationContext _appContext;
		private List<List<DishModelWithTags>> _items;
		private DynamicTableSource<List<DishModelWithTags>, MealOrdersViewCell> _mealOrderSource;
		private IViewCellHandler<List<DishModelWithTags>> _cellHandler;
		private IMealOrderCallback _callback;
		private bool _showViewAllButton;

		public MealOrderTableView(IntPtr h): base(h)
		{
		}
		public MealOrderTableView (IApplicationContext appContext, UIViewController controller, IMealOrderCallback callback, string availability, float height, bool showViewAllButton = false)
		{
			_appContext = appContext;
			_callback = callback;
			_items = new List<List<DishModelWithTags>> ();
			_showViewAllButton = showViewAllButton;
			var arr = NSBundle.MainBundle.LoadNib("MealOrderTableView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);

			AvailabilityLabel.Text = availability;
			if (!callback.ShowAvailability ())
				AvailabilityLabel.Text = "";

			SelectSuitability (Suitability.Suitabilities.Suitable);
			SuitableSegment.Hidden = false;
			AvailabilityLabel.Frame = new RectangleF (0, 0, 768, 50);
			AvailabilityLabel.BackgroundColor = _appContext.ColorHandler.MealOrderBackground;
			MealOrderTable.BackgroundColor = _appContext.ColorHandler.MealOrderBackground;
			var viewCellSetting = new BaseViewCellSetting { CellHeight = 200, CellWidth = 375 };
			viewCellSetting.BackgroundColor = _appContext.ColorHandler.MealOrderBackground;
			MealOrderTable.Frame = new RectangleF (0, 50, 768, 485);
			MealOrderTable.RowHeight = viewCellSetting.CellHeight;
			_cellHandler = new MealOrdersViewCellHandler (MealOrdersViewCell.CellIdentifier, (IMealOrdersCallback) controller, viewCellSetting);
			_mealOrderSource = new DynamicTableSource<List<DishModelWithTags>, MealOrdersViewCell> (_cellHandler);
			MealOrderTable.Source = _mealOrderSource;

			NextButton.Layer.CornerRadius = 10;
			NextButton.BackgroundColor = _callback.GetBaseContext ().ColorHandler.MainThemeColor;
			NextButton.SetTitle (_appContext.LanguageHandler.GetLocalizedString ("Next"), UIControlState.Normal);
			NextButton.Hidden = true;
		}
		public void SetItems(List<DishModelWithTags> values, bool nonSuitableExists, bool onlySuitable)
		{
			var items = values;
			if (_showViewAllButton && nonSuitableExists && _appContext.CurrentUser.Authorized ("ACCESS_MORE_DISHES")) {
				if (onlySuitable) {
					if (_callback.NonSuitableDishSelected ()) {
						ShowDishes (true);
						return;
					}
				} else {
					items = items.OrderBy (i => i.isAdditionalDish).ThenBy (i => i.label).ToList ();
				}
				if (!items.Any (i => i.id == null))
					items.Add (new DishModelWithTags { id = null });
			}
			_items = CreateTwoColumnsItems (items);
			_mealOrderSource.Items = _items;

			CheckNextButton (items);
		}
		public void SetAvailabilityText(string value)
		{
			AvailabilityLabel.Text = value;
		}
		public void SetSuitableSegmentHidden(bool hidden)
		{
			SuitableSegment.Hidden = hidden;
		}
		private List<List<DishModelWithTags>> CreateTwoColumnsItems (List<DishModelWithTags> values)
		{
			var result = new List<List<DishModelWithTags>> ();
			if (values == null)
				values = new List<DishModelWithTags> ();
			for (int i = 0; i < values.Count (); i+=2) {
				var mealOrders = new List<DishModelWithTags> {
					values [i]
				};
				if (i < values.Count() - 1)
					mealOrders.Add (values [i + 1]);
				result.Add (mealOrders);
			}
			return result;
		}
		public void Refresh()
		{
			MealOrderTable.ReloadData ();
		}
		partial void SuitableSegment_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			FilterBySuitableSegment ();
		}
		public void SelectSuitability(Suitability.Suitabilities suitability)
		{
			SuitableSegment.SelectedSegment = (int) suitability;
		}
		public void FilterBySuitableSegment ()
		{
			if(_appContext.CurrentUser.Authorized("ACCESS_MORE_DISHES") && SuitableSegment.SelectedSegment == (int) Suitability.Suitabilities.All)
				ShowDishes (true);
			else
				ShowDishes (false);
		}
		private void ShowDishes (bool showAll)
		{
			if (showAll)
				_callback.ShowAllAvailableDish ();
			else
				_callback.ShowSuitableDishesOnly ();
		}
		partial void NextButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			_callback.NextStep();
		}
		private void CheckNextButton(List<DishModelWithTags> items)
		{
			if (items.Count == 0) {
				if(!_appContext.CurrentUser.Authorized("ACCESS_MORE_DISHES"))
					NextButton.Hidden = false;
			}
			else
				NextButton.Hidden = true;
		}
		public void SetSeparatorStyle(UITableViewCellSeparatorStyle style)
		{
			MealOrderTable.SeparatorStyle = style;
		}
	}
}