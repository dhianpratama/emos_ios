﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using ios;

namespace emos_ios
{
	[Register("MealOperationView")]
	public partial class MealOperationView : UIView
	{
		private DynamicTableSource<CutOffModel, CutOffViewCell> mealCutOffSource;
		private float _cellWidth;

		public MealOperationView (IntPtr h) : base(h)
		{
		}
		public MealOperationView (float cellWidth)
		{
			_cellWidth = cellWidth;
			var arr = NSBundle.MainBundle.LoadNib ("MealOperationView", this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			AddSubview (v);
		}
		public void SetCellContent (IBaseContext baseContext, NurseDashboardHeaderModel item)
		{
			var setting = new BaseViewCellSetting { CellWidth = _cellWidth, BackgroundColor = BackgroundColor };
			var handler = new NoCallbackViewCellHandler<CutOffModel> (baseContext, CutOffViewCell.CellIdentifier, setting);
			mealCutOffSource = new DynamicTableSource<CutOffModel, CutOffViewCell> (handler);
			MealPeriodText.Text = item.meal_order_period_label;
			MealPeriodText.Font = Fonts.CutoffHeader;
			MealPeriodText.TextColor = Colors.NurseDashboardHeaderFontColor;
			mealCutOffSource.Items = item.cut_off_collections;
			CutoffTableView.Source = mealCutOffSource;
			CutoffTableView.RowHeight = CutOffViewCell.RowHeight;
		}
		public void SetLayout ()
		{
			MealPeriodText.Frame = new RectangleF (-5, 10, _cellWidth, Measurements.NurseDashboardCutoffTitleHeight);
			var sectionNumber = CutoffTableView.Source.RowsInSection (CutoffTableView, 0);
			CutoffTableView.SetFrameBelowTo (MealPeriodText, 0, Frame.Width, sectionNumber * CutOffViewCell.RowHeight, 0);
		}			
	}
}

