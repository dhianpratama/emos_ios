// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealOrderTableView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AvailabilityLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView MealOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NextButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl SuitableSegment { get; set; }

		[Action ("NextButtonClicked:")]
		partial void NextButtonClicked (MonoTouch.Foundation.NSObject sender);

		[Action ("SuitableSegment_ValueChanged:")]
		partial void SuitableSegment_ValueChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (AvailabilityLabel != null) {
				AvailabilityLabel.Dispose ();
				AvailabilityLabel = null;
			}

			if (MealOrderTable != null) {
				MealOrderTable.Dispose ();
				MealOrderTable = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (SuitableSegment != null) {
				SuitableSegment.Dispose ();
				SuitableSegment = null;
			}
		}
	}
}
