// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class BeverageConsumptionView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton AddBeverageButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField BeverageName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView BeveragesTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BeveragesTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField BeverageWeight { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MLLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopView { get; set; }

		[Action ("AddBeverageButton_TouchDown:")]
		partial void AddBeverageButton_TouchDown (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (AddBeverageButton != null) {
				AddBeverageButton.Dispose ();
				AddBeverageButton = null;
			}

			if (BeverageName != null) {
				BeverageName.Dispose ();
				BeverageName = null;
			}

			if (BeveragesTable != null) {
				BeveragesTable.Dispose ();
				BeveragesTable = null;
			}

			if (BeveragesTitleLabel != null) {
				BeveragesTitleLabel.Dispose ();
				BeveragesTitleLabel = null;
			}

			if (BeverageWeight != null) {
				BeverageWeight.Dispose ();
				BeverageWeight = null;
			}

			if (MLLabel != null) {
				MLLabel.Dispose ();
				MLLabel = null;
			}

			if (TopView != null) {
				TopView.Dispose ();
				TopView = null;
			}
		}
	}
}
