﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;

namespace emos_ios
{
	[Register("IoChartingDateRowView")]
	public partial class IoChartingDateRowView : UIView
	{
		private const int VerticalSeparatorWidth = 1;
//		private ConsumptionChecklistModel _consumptionCheckList;

		public IoChartingDateRowView(IntPtr h): base(h)
		{
		}			
		public IoChartingDateRowView (UIViewController controller)
		{
			var arr = NSBundle.MainBundle.LoadNib("IoChartingDateRowView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.BackgroundColor = Colors.ViewCellBackground;
			AddSubview (v);
		}
		public void Load (ConsumptionChecklistModel model, float calendarLeft, float cellWidth, float cellHeight)
		{
//			_consumptionCheckList = model;
			for (int i = 0; i < model.dates.Count (); i++) {
				if (i > 0)
					IoChartingDatesView.AddSubview(ViewHandler.DrawVerticalLine(new RectangleF (i * cellWidth, 0, VerticalSeparatorWidth, cellHeight), UIColor.Black));
				var dateView = new IoChartingDateView (model.dates [i]) {
					Frame = new RectangleF ((i * cellWidth) + VerticalSeparatorWidth, 0, cellWidth, cellHeight),
					BackgroundColor = Colors.ViewCellBackground
				};
				IoChartingDatesView.AddSubview (dateView);
			}
			IoChartingDatesView.Frame = new RectangleF (calendarLeft, 0, 768, cellHeight);
		}
	}
}

