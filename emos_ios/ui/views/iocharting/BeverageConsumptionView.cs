﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using System.Linq;

using VMS_IRIS.Areas.EmosIpad.Models;
using ios;

namespace emos_ios
{
	[Register("BeverageConsumptionView")]
	public partial class BeverageConsumptionView : BaseView, IBeverageConsumptionCallback
	{
		private const float BeverageTableWidth = 768f;
		//public List<KeyValuePair<string, string>> Models { get; set; }
		public List<ExtraBeverage> ExtraBeveragesModel { get; set; }
		private DynamicTableSource<ExtraBeverage, BeverageConsumptionViewCell> _source;
		private BaseViewCellSetting _baseViewCellSetting;
		public event EventHandler OnTableLayoutChanged;
		public IBeveragesTimePickerCallback Callback;

		public BeverageConsumptionView(IntPtr h): base(h)
		{
		}			
		public BeverageConsumptionView () : base (AppDelegate.Instance)
		{
			var arr = NSBundle.MainBundle.LoadNib("BeverageConsumptionView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.BackgroundColor = Colors.ViewCellBackground;
			ExtraBeveragesModel = new List<ExtraBeverage> ();
			AddSubview (v);
			Load ();
		}
		private void Load()
		{
			_baseViewCellSetting = new BaseViewCellSetting { CellHeight = 44 };
			var cellHandler = new BeverageConsumptionViewCellHandler (BeverageConsumptionViewCell.CellIdentifier, this, _baseViewCellSetting);
			_source = new DynamicTableSource<ExtraBeverage, BeverageConsumptionViewCell> (cellHandler);
			_source.Items = ExtraBeveragesModel;
			BeveragesTable.Source = _source;
			BeveragesTable.SetFrame (width: BeverageTableWidth, height: 0);
			this.Frame = new RectangleF (0, 0, 768, GetMinimalHeight ());
			BeveragesTable.SeparatorColor = UIColor.Clear;
			AddBeverageButton.Layer.CornerRadius = 10;
			this.ClipsToBounds = true;
			MLLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("mL");
			BeveragesTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Beverages");
			AddBeverageButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Add"), UIControlState.Normal);
			BeverageWeight.KeyboardType = UIKeyboardType.NumberPad;
			Refresh ();
		}
		partial void AddBeverageButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			double n;
			if (String.IsNullOrEmpty(BeverageName.Text) || !Double.TryParse(BeverageWeight.Text, out n) || 
				ExtraBeveragesModel.Any(m => m.item.Trim().Equals( BeverageName.Text, StringComparison.OrdinalIgnoreCase)))
				return;
			EndEditing(true);
			ExtraBeveragesModel.Add(new ExtraBeverage()
				{
					item = BeverageName.Text,
					value = BeverageWeight.Text,
					consumptionTime = DateTime.Now.ToString("H:mm")
				});
			_source.Items = ExtraBeveragesModel;
			BeverageName.Text = "";
			BeverageWeight.Text = "";
			Refresh();
		}
		private void Refresh()
		{
			var tableHeight = ExtraBeveragesModel.Count * _baseViewCellSetting.CellHeight;
			BeveragesTable.Frame = new RectangleF (0, TopView.Frame.Height, BeverageTableWidth, tableHeight);

			this.SetFrame (width:BeverageTableWidth, height: TopView.Frame.Height + BeveragesTable.Frame.Height);
			BeveragesTable.ReloadData ();
			OnTableLayoutChanged.SafeInvoke (this);
		}
		public void RemoveBeverage(ExtraBeverage item)
		{
			ExtraBeveragesModel.RemoveAll (e => e.item == item.item);
			Refresh ();
		}
		public void ItemSelected(ExtraBeverage item, int index)
		{
		}
		public float GetMinimalHeight()
		{
			return TopView.Frame.Height;
		}
		public void Reload()
		{
			_source.Items = ExtraBeveragesModel;
			Refresh ();
		}

		public void OpenBeveragesTimePicker(ExtraBeverage item)
		{
			int? index = null;
			for (int i = 0; i < ExtraBeveragesModel.Count; i++) {
				if (item.item == ExtraBeveragesModel [i].item)
					index = i;
			}
			Callback.OpenBeveragesTimePicker (item,index);
		}

		public void ReloadTable()
		{
			_source.Items = ExtraBeveragesModel;
			BeveragesTable.ReloadData ();
		}
	}
}

