﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using System.Linq;

using VMS_IRIS.Areas.EmosIpad.Models;
using ios;

namespace emos_ios
{
	[Register("BeverageConsumptionDisplayView")]
	public partial class BeverageConsumptionDisplayView : BaseView, IBeverageConsumptionCallback
	{
		private const float BeverageTableWidth = 768f;
		//public List<KeyValuePair<string, string>> Models { get; set; }
		public List<ExtraBeverage> ExtraBeveragesModel { get; set; }
		private DynamicTableSource<ExtraBeverage, BeverageConsumptionDisplayViewCell> _source;
		private BaseViewCellSetting _baseViewCellSetting;
		public event EventHandler OnTableLayoutChanged;
		public IBeveragesTimePickerCallback Callback;

		public BeverageConsumptionDisplayView(IntPtr h): base(h)
		{
		}			
		public BeverageConsumptionDisplayView () : base (AppDelegate.Instance)
		{
			var arr = NSBundle.MainBundle.LoadNib("BeverageConsumptionDisplayView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.BackgroundColor = Colors.ViewCellBackground;
			ExtraBeveragesModel = new List<ExtraBeverage> ();
			AddSubview (v);
			Load ();
		}
		public void OpenBeveragesTimePicker(ExtraBeverage item)
		{
		}
		private void Load()
		{
			_baseViewCellSetting = new BaseViewCellSetting { CellHeight = 44 };
			var cellHandler = new BeverageConsumptionViewCellHandler (BeverageConsumptionDisplayViewCell.CellIdentifier, this, _baseViewCellSetting);
			_source = new DynamicTableSource<ExtraBeverage, BeverageConsumptionDisplayViewCell> (cellHandler);
			_source.Items = ExtraBeveragesModel;
			BeveragesTableView.Source = _source;
			BeveragesTableView.SetFrame (width: BeverageTableWidth, height: 0);
			this.Frame = new RectangleF (0, 0, 768, GetMinimalHeight ());
			BeveragesTableView.SeparatorColor = UIColor.Clear;
			this.ClipsToBounds = true;
			Refresh ();
		}
		private void Refresh()
		{
			var tableHeight = ExtraBeveragesModel.Count * _baseViewCellSetting.CellHeight;
			BeveragesTableView.Frame = new RectangleF (0, 0, BeverageTableWidth, tableHeight);

			this.SetFrame (width:BeverageTableWidth, height: BeveragesTableView.Frame.Height);
			BeveragesTableView.ReloadData ();
			OnTableLayoutChanged.SafeInvoke (this);
		}
		public void RemoveBeverage(ExtraBeverage item)
		{
			ExtraBeveragesModel.RemoveAll (e => e.item == item.item);
			Refresh ();
		}
		public void ItemSelected(ExtraBeverage item, int index)
		{
		}
		public float GetMinimalHeight()
		{
			return 0f;
		}
		public void Reload()
		{
			_source.Items = ExtraBeveragesModel;
			Refresh ();
		}
		public void ReloadTable()
		{
			_source.Items = ExtraBeveragesModel;
			BeveragesTableView.ReloadData ();
		}
	}
}

