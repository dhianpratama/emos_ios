﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Globalization;

namespace emos_ios
{
	[Register("IoChartingDateView")]
	public partial class IoChartingDateView : UIView
	{
		public IoChartingDateView (IntPtr h) : base(h)
		{
		}
		public IoChartingDateView (DateTime dateTime)
		{
			var arr = NSBundle.MainBundle.LoadNib("IoChartingDateView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.BackgroundColor = Colors.ViewCellBackground;
			AddSubview (v);

			CultureInfo languageCode = new CultureInfo(AppDelegate.Instance.LanguageHandler.LanguageCode);
			DateLabel.Text = dateTime.Day.ToString ("00");
			MonthLabel.Text = dateTime.ToString ("MMMM", languageCode);
			DayLabel.Text = languageCode.DateTimeFormat.GetDayName (dateTime.DayOfWeek);
		}
	}
}

