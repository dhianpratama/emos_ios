// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class PatientMealOrderView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BedLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CompanionMealOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DateAndMealTypeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel InstructionTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MealOperationContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NurseDashboardButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PatientMealOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView PatientsOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PatientTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopBarView { get; set; }

		[Action ("CompanionMealOrderButtonClicked:")]
		partial void CompanionMealOrderButtonClicked (MonoTouch.Foundation.NSObject sender);

		[Action ("NurseDashboardButtonClicked:")]
		partial void NurseDashboardButtonClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BedLabel != null) {
				BedLabel.Dispose ();
				BedLabel = null;
			}

			if (CompanionMealOrderButton != null) {
				CompanionMealOrderButton.Dispose ();
				CompanionMealOrderButton = null;
			}

			if (DateAndMealTypeLabel != null) {
				DateAndMealTypeLabel.Dispose ();
				DateAndMealTypeLabel = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (InstructionTitleLabel != null) {
				InstructionTitleLabel.Dispose ();
				InstructionTitleLabel = null;
			}

			if (MealOperationContainerView != null) {
				MealOperationContainerView.Dispose ();
				MealOperationContainerView = null;
			}

			if (NurseDashboardButton != null) {
				NurseDashboardButton.Dispose ();
				NurseDashboardButton = null;
			}

			if (PatientMealOrderButton != null) {
				PatientMealOrderButton.Dispose ();
				PatientMealOrderButton = null;
			}

			if (PatientsOrderTable != null) {
				PatientsOrderTable.Dispose ();
				PatientsOrderTable = null;
			}

			if (PatientTitleLabel != null) {
				PatientTitleLabel.Dispose ();
				PatientTitleLabel = null;
			}

			if (TopBarView != null) {
				TopBarView.Dispose ();
				TopBarView = null;
			}
		}
	}
}
