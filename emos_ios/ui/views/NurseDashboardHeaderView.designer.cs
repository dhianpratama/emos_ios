// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class NurseDashboardHeaderView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel BedLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel BedTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CompanionMealOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView HeaderView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MealOperationContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NurseDashboardButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton PatientMealOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView PatientsOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView TopBarView { get; set; }

		[Action ("CompanionMealOrderButton_TouchUpInside:")]
		partial void CompanionMealOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("NurseDashboardButton_TouchUpInside:")]
		partial void NurseDashboardButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("PatientMealOrderButtonClicked:")]
		partial void PatientMealOrderButtonClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BedLabel != null) {
				BedLabel.Dispose ();
				BedLabel = null;
			}

			if (BedTitleLabel != null) {
				BedTitleLabel.Dispose ();
				BedTitleLabel = null;
			}

			if (CompanionMealOrderButton != null) {
				CompanionMealOrderButton.Dispose ();
				CompanionMealOrderButton = null;
			}

			if (HeaderView != null) {
				HeaderView.Dispose ();
				HeaderView = null;
			}

			if (MealOperationContainerView != null) {
				MealOperationContainerView.Dispose ();
				MealOperationContainerView = null;
			}

			if (NurseDashboardButton != null) {
				NurseDashboardButton.Dispose ();
				NurseDashboardButton = null;
			}

			if (PatientMealOrderButton != null) {
				PatientMealOrderButton.Dispose ();
				PatientMealOrderButton = null;
			}

			if (PatientsOrderTable != null) {
				PatientsOrderTable.Dispose ();
				PatientsOrderTable = null;
			}

			if (TopBarView != null) {
				TopBarView.Dispose ();
				TopBarView = null;
			}
		}
	}
}
