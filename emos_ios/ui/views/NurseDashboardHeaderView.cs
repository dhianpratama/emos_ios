﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Dialog;
using MonoTouch.CoreGraphics;
using VMS_IRIS.Areas.EmosIpad.Models;

using emos_ios.tools;

namespace emos_ios
{
	[Register("NurseDashboardHeaderView")]
	public partial class NurseDashboardHeaderView : UIView
	{
		public const float HeaderTitleHeight = 47f;
		IPatientDayOrderCallback _controller;
		public DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> PatientsOrderSource { get; set;}
		public PatientDayOrderViewCellHandler PatientDayOrderViewCellHandler { get; set; }
		private NurseDashboardModel _nurseDashboardModel;
		private bool _cutoffVisible;
		public UIView Header { get { return HeaderView; } }
		public int OriginalHeaderCount { get; set; }

		public NurseDashboardHeaderView(IntPtr h): base(h)
		{
		}
		public NurseDashboardHeaderView (IPatientDayOrderCallback controller, Dashboard.DashboardMenu tabStatus)
		{
			_controller = controller;
			var arr = NSBundle.MainBundle.LoadNib("NurseDashboardHeaderView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new RectangleF(0, 0, Frame.Width, Frame.Height);
			AddSubview (v);
			this.Hidden = true;
			PatientsOrderTable.RowHeight = Measurements.NurseDashboardRowHeight;
			NurseDashboardButton.Layer.CornerRadius = 10;
			CompanionMealOrderButton.Layer.CornerRadius = 10;
			BedLabel.Font = Fonts.CutoffHeader;
			BedTitleLabel.Font = Fonts.CutoffHeader;
			BedLabel.TextColor = Colors.NurseDashboardHeaderFontColor;
			BedTitleLabel.TextColor = Colors.NurseDashboardHeaderFontColor;

			switch (tabStatus) {
			case Dashboard.DashboardMenu.NurseDashboard:
				NurseDashboardButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;
				CompanionMealOrderButton.BackgroundColor = UIColor.Clear;
				break;
			case Dashboard.DashboardMenu.PatientMealOrder:
				NurseDashboardButton.BackgroundColor = UIColor.Clear;
				CompanionMealOrderButton.BackgroundColor = UIColor.Clear;
				break;
			case Dashboard.DashboardMenu.CompanionMealOrder:
				NurseDashboardButton.BackgroundColor = UIColor.Clear;
				CompanionMealOrderButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;
				break;
			}
			HidePatientDayOrderTableVisibility ();
		}
		public void Load (NurseDashboardModel model)
		{
			this.Hidden = false;
			_nurseDashboardModel = model;
			OriginalHeaderCount = model.header.Count;
			InitializeHandlers ();
			ShowTopBar ();
			ShowHeader (model.header);
			ShowBody ();
			SetLayout ();
			SetTextsByLanguage ();
			_cutoffVisible = Settings.AllowDashboardCutoffToggle;
			ToggleCutoffVisibility ();
		}
		private void ShowTopBar ()
		{
			TopBarView.Hidden = Measurements.NurseDashboardTopBarHeight == 0;
		}
		private void ShowHeader (List<NurseDashboardHeaderModel> headers)
		{
			if (!Settings.ShowCompanionLabelOnDashboard) {
				CompanionMealOrderButton.Hidden = true;
				NurseDashboardButton.SetFrame (x: 123f);
				PatientMealOrderButton.SetFrame(x: 446f);
			}
			TopBarView.BackgroundColor = Colors.NurseDashboardTopBarColor;
			//NurseDashboardButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;
			//CompanionMealOrderButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;
			//PatientMealOrderButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;

			ViewHandler.ClearSubviews (MealOperationContainerView);
			MealOperationContainerView.AddSubviews (CreateMealOperationViews (headers));
		}
		public MealOperationView[] CreateMealOperationViews(List<NurseDashboardHeaderModel> headers)
		{
			var result = new List<MealOperationView> ();
			foreach (var mealOperation in headers) {
				var mealOperationView = new MealOperationView (PatientDayOrderViewCellHandler.Setting.CellWidth);
				mealOperationView.BackgroundColor = Colors.NurseDashboardHeaderBackgroundColor;
				mealOperationView.SetCellContent (_controller.GetBaseContext (), mealOperation);
				result.Add (mealOperationView);
			}
			return result.ToArray ();
		}
		private void ShowBody ()
		{
			if (_nurseDashboardModel.beds == null || _nurseDashboardModel.beds.Count == 0) {
				PatientsOrderTable.Hidden = true;
				return;
			}
			PatientsOrderTable.SeparatorColor = Colors.NurseDashboardPatientTableSeparatorColor;
			PatientsOrderTable.Hidden = false;
			PatientsOrderSource = new DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> (PatientDayOrderViewCellHandler);
			PatientsOrderSource.Items = _nurseDashboardModel.beds;
			PatientsOrderTable.Source = PatientsOrderSource;
		}
		public LocationWithRegistrationSimple GetBed(PatientOrderCommand patientOrderCommand)
		{
			return _nurseDashboardModel.beds.First (b => b.registrations.Any (r => r.profile_id == patientOrderCommand.Registration.profile_id));
		}
		public void ToggleCutoffVisibility()
		{
			_cutoffVisible = !_cutoffVisible;
			if (_cutoffVisible)
				ShowCutoff ();
			else
				HideCutoff ();
		}
		public void SetLayout()
		{
			SetHeaderLayout ();
			var y = HeaderView.Frame.Height + TopBarView.Frame.Height;
			PatientsOrderTable.Frame = new RectangleF (0, y, Frame.Width, Measurements.BottomY - HeaderView.Frame.Height - TopBarView.Frame.Height - Measurements.TopY);
		}
		private void SetHeaderLayout ()
		{
			TopBarView.SetFrame (0, 0, height: Measurements.NurseDashboardTopBarHeight);
			HeaderView.SetFrameBelowTo (TopBarView, 0, 768, HeaderTitleHeight, distanceToAbove: 0);
			var cutOffCount = _nurseDashboardModel.header [0].cut_off_collections.Where (c => c.operation_label != "").Count ();
			var totalHeight = (cutOffCount * (((int) CutOffViewCell.RowHeight) + 10)) + 30;
			MealOperationContainerView.Frame = new RectangleF (Measurements.NurseDashboardOperationContainerLeft, 0, UIScreen.MainScreen.Bounds.Right - Measurements.NurseDashboardOperationContainerLeft, totalHeight);
			SetMealOperationContainerViewLayout (totalHeight);
		}
		private void SetMealOperationContainerViewLayout (float totalHeight)
		{
			for (int i = 0; i < _nurseDashboardModel.header.Count (); i++) {
				var mealOperationView = (MealOperationView)MealOperationContainerView.Subviews [i];
				mealOperationView.Frame = new RectangleF ((PatientDayOrderViewCellHandler.Setting.CellWidth * i), 0, PatientDayOrderViewCellHandler.Setting.CellWidth, totalHeight);
				mealOperationView.SetLayout ();
			}
		}
		public void HideCutoff()
		{
			HeaderView.SetFrame (height: HeaderTitleHeight);
			PatientsOrderTable.SetFrameBelowTo (HeaderView, height: Measurements.PageHeight - TopBarView.Frame.Height - HeaderView.Frame.Height, distanceToAbove: 0);
		}
		public void ShowCutoff()
		{
			var headerHeight = Settings.AllowDashboardCutoffToggle ? HeaderView.Frame.Height + MealOperationContainerView.Frame.Height : Measurements.HeaderHeight - TopBarView.Frame.Height;
			HeaderView.SetFrame (height: headerHeight);
			HeaderView.BackgroundColor = Colors.NurseDashboardHeaderBackgroundColor;
			PatientsOrderTable.SetFrameBelowTo (HeaderView, height: Measurements.PageHeight - TopBarView.Frame.Height - HeaderView.Frame.Height, distanceToAbove: 0);
		}
		private void InitializeHandlers ()
		{
			var cellWidth = (float)Math.Ceiling ((UIScreen.MainScreen.Bounds.Right - Measurements.NurseDashboardOperationContainerLeft) / _nurseDashboardModel.header.Count ());
			var patientDayOrderSetting = new PatientDayOrderViewCellSetting
			{
				CellWidth = cellWidth,
				CellHeight = Measurements.NurseDashboardRowHeight,
				BackgroundColor = UIColor.Clear,
				OrderContainerFrame = new RectangleF (MealOperationContainerView.Frame.Location.X, 0, _nurseDashboardModel.header.Count () * cellWidth, 55)
			};
			PatientDayOrderViewCellHandler = new PatientDayOrderViewCellHandler (PatientDayOrderViewCell.CellIdentifier, _controller, patientDayOrderSetting);
		}
		private void onRequestFailed()
		{
			Console.WriteLine ("REQUEST FAILED");
		}
		public void HidePatientDayOrderTableVisibility ()
		{
			PatientsOrderTable.Hidden = true;
		}
		partial void PatientMealOrderButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			_controller.OnDashboardChanged(Dashboard.OrderTabs.PatientMealOrder);
		}
		partial void CompanionMealOrderButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_controller.OnDashboardChanged(Dashboard.OrderTabs.CompanionMealOrder);
		}
		partial void NurseDashboardButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_controller.OnDashboardChanged(Dashboard.OrderTabs.NurseDashboard);
		}
		private void SetTextsByLanguage()
		{
			NurseDashboardButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NurseDashboard"), UIControlState.Normal);
			PatientMealOrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientMealOrder"), UIControlState.Normal);
			CompanionMealOrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CompanionMealOrder"), UIControlState.Normal);
			BedTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Beds");
			BedLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Patient");
		}
		public NurseDashboardModel GetNurseDashboardModel()
		{
			return _nurseDashboardModel;
		}
	}
}