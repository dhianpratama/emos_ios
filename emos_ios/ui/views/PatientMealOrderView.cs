﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using System.Globalization;

namespace emos_ios
{
	[Register("PatientMealOrderView")]
	public partial class PatientMealOrderView : UIView
	{
		IPatientDayOrderCallback _controller;
		public DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> PatientsOrderSource { get; set;}
		private PatientDayOrderViewCellHandler _patientDayOrderViewCellHandler;
		public PatientMealOrderModel _model;

		public PatientMealOrderView(IntPtr h): base(h)
		{
		}
		public PatientMealOrderView (IPatientDayOrderCallback controller)
		{
			_controller = controller;
			var arr = NSBundle.MainBundle.LoadNib("PatientMealOrderView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			v.Frame = new RectangleF(0, 0, Frame.Width, Frame.Height);
			AddSubview (v);
			this.Hidden = true;
			PatientMealOrderButton.Layer.CornerRadius = 10;
			HidePatientDayOrderTableVisibility ();
		}
		public void Load (PatientMealOrderModel model)
		{
			_model = model;
			this.Hidden = false;
			InitializeHandlers ();
			ShowHeader ();
			ShowBody ();
			SetLayout ();
			SetTextsByLanguage ();

			BedLabel.TextColor = Colors.NurseDashboardHeaderFontColor;
			PatientTitleLabel.TextColor = Colors.NurseDashboardHeaderFontColor;
			InstructionTitleLabel.TextColor = Colors.NurseDashboardHeaderFontColor;
			DateAndMealTypeLabel.TextColor = Colors.NurseDashboardHeaderFontColor;
		}
		private void ShowHeader ()
		{
			CultureInfo languageCode = new CultureInfo(AppDelegate.Instance.LanguageHandler.LanguageCode);
			TopBarView.SetFrame (0, 0, height: Measurements.NurseDashboardTopBarHeight);
			MealOperationContainerView.Frame = new RectangleF (MealOperationContainerView.Frame.Location.X, 0, MealOperationContainerView.Frame.Size.Width, 200);
			HeaderView.Frame = new RectangleF (0, TopBarView.Frame.Height + TopBarView.Frame.Top, 768, Measurements.PatientMealOrderHeaderHeight - TopBarView.Frame.Height);
			HeaderView.BackgroundColor = Colors.NurseDashboardHeaderBackgroundColor;
			if (_model.mealPeriod != null)
				DateAndMealTypeLabel.Text = AppDelegate.Instance.OperationDate.ToString ("dddd, dd MMM yyyy", languageCode) + " - " + _model.mealPeriod.label;
			else {
				DateAndMealTypeLabel.TextColor = UIColor.Red;
				var lastCutOffMsg = String.Format(AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Thelastcutofftimefor{0}hasalreadypassed"),
					AppDelegate.Instance.OperationDate.ToString ("dddd, dd MMM yyyy", languageCode)) ;
				DateAndMealTypeLabel.Text = lastCutOffMsg;
			}
			if (!Settings.ShowCompanionLabelOnDashboard) {
				CompanionMealOrderButton.Hidden = true;
				NurseDashboardButton.SetFrame (x: 123f);
				PatientMealOrderButton.SetFrame(x: 446f);
			}
			TopBarView.BackgroundColor = Colors.NurseDashboardTopBarColor;
			PatientMealOrderButton.BackgroundColor = Colors.NurseDashboardHeaderButtonColor;
		}
		private void ShowBody ()
		{
			if (_model.beds == null || _model.beds.Count == 0) {
				PatientsOrderTable.Hidden = true;
				return;
			}
			PatientsOrderTable.SeparatorColor = Colors.NurseDashboardPatientTableSeparatorColor;
			PatientsOrderTable.Hidden = false;
			PatientsOrderSource = new DynamicTableSource<LocationWithRegistrationSimple, PatientDayOrderViewCell> (_patientDayOrderViewCellHandler);
			PatientsOrderSource.Items = _model.beds;
			PatientsOrderTable.Source = PatientsOrderSource;
			PatientsOrderTable.ReloadData ();
		}
		public void SetLayout()
		{
			var y = HeaderView.Frame.Height + TopBarView.Frame.Height;
			PatientsOrderTable.Frame = new RectangleF (0, y, Frame.Width, Measurements.BottomY - HeaderView.Frame.Height - TopBarView.Frame.Height - Measurements.TopY);
		}
		private void InitializeHandlers ()
		{
			var cellWidth = PatientsOrderTable.Frame.Width;
			var patientDayOrderSetting = new PatientDayOrderViewCellSetting
			{
				CellWidth = cellWidth,
				CellHeight = Measurements.NurseDashboardRowHeight,
				BackgroundColor = UIColor.Clear,
				OrderContainerFrame = new RectangleF (MealOperationContainerView.Frame.Location.X, 0, cellWidth, Measurements.NurseDashboardRowHeight),
			};
			_patientDayOrderViewCellHandler = new PatientDayOrderViewCellHandler (PatientDayOrderViewCell.CellIdentifier, _controller, patientDayOrderSetting) {
				GetHeightForRow = GetHeightForRow
			};
		}
		private float GetHeightForRow(LocationWithRegistrationSimple location)
		{
			return PatientDayOrderViewCell.GetRowHeight (location);
		}
		public void HidePatientDayOrderTableVisibility ()
		{
			PatientsOrderTable.Hidden = true;
		}

		partial void NurseDashboardButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			AppDelegate.Instance.CurrentTab = Dashboard.OrderTabs.NurseDashboard;
			_controller.OnDashboardChanged(Dashboard.OrderTabs.NurseDashboard);
		}
		partial void CompanionMealOrderButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			AppDelegate.Instance.CurrentTab = Dashboard.OrderTabs.CompanionMealOrder;
			_controller.OnDashboardChanged(Dashboard.OrderTabs.CompanionMealOrder);
		}
		private void SetTextsByLanguage()
		{
			NurseDashboardButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NurseDashboard"), UIControlState.Normal);
			PatientMealOrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("PatientMealOrder"), UIControlState.Normal);
			CompanionMealOrderButton.SetTitle (AppDelegate.Instance.LanguageHandler.GetLocalizedString ("CompanionMealOrder"), UIControlState.Normal);
			BedLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Beds");
			PatientTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Patient");
		}
	}
}