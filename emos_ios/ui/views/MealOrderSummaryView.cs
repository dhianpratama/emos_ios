﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ios;
using MonoTouch.CoreAnimation;
using VMS_IRIS.Areas;

namespace emos_ios
{
	[Register("MealOrderSummaryView")]
	public partial class MealOrderSummaryView : UIView, INameQuantityCallback<SpecialInstructionDetail>
	{
		private const int DishRowHeight = 160;
		private const int SpecialInstructionRowHeight = 40;
		private const int LeftPadding = 20;
		private const int OuterWidth = 768 - LeftPadding;
		private const int InsideWidth = 534 - LeftPadding;

		private DynamicTableSource<DishModelSimple, DishSummaryViewCell> _dishSource;
		private DynamicTableSource<DishModelSimple, DishSummaryViewCell> _menuOfTheDaySource;
		private DynamicTableSource<SpecialInstructionDetail, NameQuantityViewCell> _specialInstructionSource;
		private NameQuantityViewCellHandler<SpecialInstructionDetail> _specialInstructionViewCellHandler;
		public List<DishModelSimple> Dishes { get; set; }
		public List<ExtraOrderModelSimple> ExtraOrders { get; set; }
		public bool IsPatient { get; set; }
		private bool _isViewOnly;
		private MealOrderModel _mealOrderModel { get; set; }
		private List<DishModelSimple> _selectedDishes { get; set; }
		private UIStringAttributes _specialInstructionCaptionAttributes { get; set; }
		private UIView _specialInstructionView;
		private List<SpecialInstructionDetail> _specialInstructionDetails;
		private PaddingLabel _subjectToAvailabilityLabel;
		private IMealOrderCallback _controller;
		private MealOrderSummaryViewSetting _setting;

		public string Remark
		{
			get { return RemarkTextView.Text; }
			set { RemarkTextView.Text = value; }
		}
		public bool StayForMeal { get { return StayForMealSwitch.On; } set { StayForMealSwitch.On = value; } }
		public bool MealOnHold { get { return MealOnHoldSwitch.On; } set { MealOnHoldSwitch.On = value; } }
		public bool UseAsOngoingRemark { get { return UseAsOngoingRemarkSwitch.On; } set { UseAsOngoingRemarkSwitch.On = value; } }
		public bool BiggerPortion { get { return BiggerPortionSwitch.On; } set { BiggerPortionSwitch.On = value; } }
		public bool LessSalt { get { return LessSaltSwitch.On; } set { LessSaltSwitch.On = value; } }
		public bool LessOily { get { return LessOilySwitch.On; } set { LessOilySwitch.On = value; } }
		public bool MoreGravy { get { return MoreGravySwitch.On; } set { MoreGravySwitch.On = value; } }
		public bool NoGarnishes { get { return NoGarnishesSwitch.On; } set { NoGarnishesSwitch.On = value; } }
		public GarlicOnionOption SelectedGarlicOnion { get { return _mealOrderModel.garlic_onion; } }
		public SpicinessLevel SelectedSpicinessLevel { get { return _mealOrderModel.spiciness_level; } }
		public bool EarlyDelivery { get { return EarlyDeliverySwitch.On; } set { EarlyDeliverySwitch.On = value; } }
		public bool IsCompanion { get; set; }

		public MealOrderSummaryView(IntPtr h): base(h)
		{
		}
		public MealOrderSummaryView (MealOrderSummaryViewSetting setting)
		{
			_setting = setting;
			_controller = setting.Controller;
			IsPatient = setting.IsPatient;
			IsCompanion = setting.IsCompanionMeal;
			_isViewOnly = setting.IsViewOnly;
			_mealOrderModel = setting.MealOrderModel;
			if (setting.SelectedDishes != null) {
				if (!Settings.HideMenuOfTheDay && _isViewOnly) {
					var dishTypeIds = _mealOrderModel.dishTypes.Select (e => e.id).ToList ();
					var motd = _mealOrderModel.orderedDishes.Where (e => dishTypeIds.Contains (e.dish_type_id)).ToList ();
					_selectedDishes = motd;
				}
				else {
					_selectedDishes = setting.SelectedDishes.ToList ();
				}
			}
			else
				_selectedDishes = new List<DishModelSimple> ();

			_specialInstructionCaptionAttributes = new UIStringAttributes {
				ForegroundColor = UIColor.White,
				BackgroundColor = UIColor.Black
			};
			var specialInstructionBaseSetting = new NameQuantityViewCellSetting { CellHeight = SpecialInstructionRowHeight, BackgroundColor = UIColor.White };
			specialInstructionBaseSetting.BackgroundColor = UIColor.White;
			specialInstructionBaseSetting.QuantityTextHidden = _setting.ExtraOrderQuantityInMealOrderHidden;
			specialInstructionBaseSetting.QuantityLabelHidden = true;
			specialInstructionBaseSetting.ShowQuantityOnly = Settings.ExtraOrderQuantityInMealOrderHidden;
			specialInstructionBaseSetting.MinimumQuantity = Settings.ExtraOrderMinimumQuantity;
			specialInstructionBaseSetting.MaximumQuantity = Settings.ExtraOrderMaximumQuantity;
			if (!_setting.ExtraOrderQuantityInMealOrderHidden)
				specialInstructionBaseSetting.QuantityLabelX = 280;
			_specialInstructionViewCellHandler = new NameQuantityViewCellHandler<SpecialInstructionDetail> (NameQuantityViewCell.CellIdentifier, this, specialInstructionBaseSetting);

			var arr = NSBundle.MainBundle.LoadNib("MealOrderSummaryView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);

			RemarkTextView.Text = "";
			RemarksView.BackgroundColor = Colors.MealOrderBackground;
			ShowPatientOptions ();
			InitiateSummaryTable ();
			InitiateMenuOfTheDayTable ();
			CreateSpecialInstructionView ();
			CreateSubjectToAvailabilityLabel ();
			SetUserOptions (setting.MealOrderModel);
			if (Settings.ShowOnGoingRemark && !IsCompanion)
				UseAsOngoingRemarkSwitch.On = setting.MealOrderModel.use_as_going_remark;
			SetLayout ();
			LimitChar ();
			SetTextsByLanguage ();
		}
		private void SetUserOptions (MealOrderModel mealOrderModel)
		{
			MealOnHoldSwitch.On = mealOrderModel.put_on_hold;
			StayForMealSwitch.On = mealOrderModel.stay_for_current_meal;
			BiggerPortionSwitch.On = mealOrderModel.bigger_portion;
			EarlyDeliverySwitch.On = mealOrderModel.early_delivery;
			LessSaltSwitch.On = mealOrderModel.less_salt;
			LessOilySwitch.On = mealOrderModel.less_oily;
			MoreGravySwitch.On = mealOrderModel.more_gravy;
			NoGarnishesSwitch.On = mealOrderModel.no_garnishes;
			GarlicOnionPicker.Select ((int)mealOrderModel.garlic_onion, 0, true);
			SpicinessPicker.Select ((int)mealOrderModel.spiciness_level, 0, true);
		}
		private void ShowPatientOptions ()
		{
			if (!Settings.ShowPatientPreference) {
				GarlicOnionPicker.Hidden = true;
				SpicinessPicker.Hidden = true;
				BiggerPortionView.Hidden = true;
				LessOilyView.Hidden = true;
				LessSaltView.Hidden = true;
				MoreGravyView.Hidden = true;
				NoGarnishesView.Hidden = true;
				return;
			}
			InitiateSpicinessPicker ();
			InitiateGarlicOnionPicker ();
		}
		private void CreateSubjectToAvailabilityLabel ()
		{
			_subjectToAvailabilityLabel = new PaddingLabel {
				BackgroundColor = UIColor.Black,
				Font = Fonts.Header,
				TextColor = UIColor.White,
				EdgeInsets = new UIEdgeInsets (0, 20, 0, 20),
			};
			RemarksView.AddSubview (_subjectToAvailabilityLabel);
		}
		private void InitiateSpicinessPicker ()
		{
			var spicinessLevels = new List<KeyValuePair<int,string>> {
				new KeyValuePair<int, string> ((int) SpicinessLevel.NoPreference, "No preference"),
				new KeyValuePair<int, string> ((int) SpicinessLevel.NotSpicy, "Not Spicy"),
				new KeyValuePair<int, string> ((int) SpicinessLevel.MoreSpicy, "More Spicy")
			};
			var pickerModel = new PickerModel (spicinessLevels);
			pickerModel.OnSelectionChanged += HandleSpicinessSelectionChanged;
			SpicinessPicker.Model = pickerModel;
			if (SpicinessPicker.UserInteractionEnabled && _controller.CanOrder ())
				SpicinessPicker.BackgroundColor = _controller.GetBaseContext ().ColorHandler.ButtonBackground;
			else
				SpicinessPicker.BackgroundColor = Colors.DarkViewCellBackground;
			SpicinessPicker.Layer.CornerRadius = 20f;
		}
		private void HandleSpicinessSelectionChanged (object sender, PickerChangedEventArgs e)
		{
			_mealOrderModel.spiciness_level = (SpicinessLevel)Enum.Parse (typeof(SpicinessLevel), e.SelectedValue.Key.ToString ());
		}
		private void InitiateGarlicOnionPicker ()
		{
			var specialIngredients = new List<KeyValuePair<int,string>> {
				new KeyValuePair<int, string> ((int) GarlicOnionOption.NoPreference, "No preference"),
				new KeyValuePair<int, string> ((int) GarlicOnionOption.NoGarlic, "No garlic"),
				new KeyValuePair<int, string> ((int) GarlicOnionOption.NoOnion, "No onion"),
				new KeyValuePair<int, string> ((int) GarlicOnionOption.NoGarlicNoOnion, "No onion and garlic")
			};
			var pickerModel = new PickerModel (specialIngredients);
			pickerModel.OnSelectionChanged += HandleGarlicOnionSelectionChanged;
			GarlicOnionPicker.Model = pickerModel;
			if (GarlicOnionPicker.UserInteractionEnabled && _controller.CanOrder ())
				GarlicOnionPicker.BackgroundColor = _controller.GetBaseContext().ColorHandler.ButtonBackground;
			else
				GarlicOnionPicker.BackgroundColor = Colors.DarkViewCellBackground;
			GarlicOnionPicker.Layer.CornerRadius = 20f;
		}
		private void HandleGarlicOnionSelectionChanged (object sender, PickerChangedEventArgs e)
		{
			_mealOrderModel.garlic_onion = (GarlicOnionOption)Enum.Parse (typeof(GarlicOnionOption), e.SelectedValue.Key.ToString ());
		}
		private void InitiateSummaryTable ()
		{
			SummaryTable.RowHeight = DishRowHeight;
			var baseSetting = new BaseViewCellSetting {
				CellHeight = DishRowHeight
			};
			baseSetting.BackgroundColor = Colors.MealOrderBackground;
			var cellHandler = new NoCallbackViewCellHandler<DishModelSimple> (_controller.GetBaseContext (), DishSummaryViewCell.CellIdentifier, baseSetting);
			_dishSource = new DynamicTableSource<DishModelSimple, DishSummaryViewCell> (cellHandler);
			_dishSource.Items = _selectedDishes.Where (d => d.id != null).ToList ();
			SummaryTable.Source = _dishSource;
		}
		private void InitiateMenuOfTheDayTable ()
		{
			if (Settings.HideMenuOfTheDay) {
				HideMenuOfTheDay ();
				return;
			}
			var menuOfTheDayDishes = GetMenuOfTheDayDishes ();
			if (!menuOfTheDayDishes.Any ()) {
				HideMenuOfTheDay ();
				return;
			}
			MenuOfTheDayTable.Hidden = false;
			MenuOfTheDayLabel.Hidden = false;
			MenuOfTheDayTable.RowHeight = DishRowHeight;
			var baseSetting = new BaseViewCellSetting {
				CellHeight = DishRowHeight
			};
			baseSetting.BackgroundColor = Colors.MealOrderBackground;
			var cellHandler = new NoCallbackViewCellHandler<DishModelSimple> (_controller.GetBaseContext (), DishSummaryViewCell.CellIdentifier, baseSetting);
			_menuOfTheDaySource = new DynamicTableSource<DishModelSimple, DishSummaryViewCell> (cellHandler);
			_menuOfTheDaySource.Items = menuOfTheDayDishes;
			MenuOfTheDayTable.Source = _menuOfTheDaySource;
			MenuOfTheDayTable.SetFrame (height: _menuOfTheDaySource.Items.Count * DishRowHeight);
		}
		private void HideMenuOfTheDay ()
		{
			MenuOfTheDayTable.SetFrame (height: 0);
			MenuOfTheDayTable.Hidden = true;
			MenuOfTheDayLabel.SetFrame (height: 0);
			MenuOfTheDayLabel.Hidden = true;
		}

		private List<DishModelSimple> GetMenuOfTheDayDishes ()
		{
			if (_setting.IsViewOnly) {
				var dishTypeIds = _mealOrderModel.dishTypes.Select (e => e.id).ToList ();
				var motd = _mealOrderModel.orderedDishes.Where (e => !dishTypeIds.Contains (e.dish_type_id)).ToList ();
				return motd;
			}

			var result = new List<DishModelSimple> ();
			if (_mealOrderModel.MenuOfTheDayTags == null)
				return result;
			if (!_mealOrderModel.MenuOfTheDayTags.Any ())
				return result;
			var dishTypes = _mealOrderModel.MenuOfTheDayTags.Where (m => m.tag_id == _setting.SelectedTagId && m.dishTypes != null && m.dishTypes.Any ()).SelectMany (m => m.dishTypes).ToList ();
			if (_selectedDishes [0].vegetarian_meal)
				result = dishTypes.Where (t => t.VegetableDish != null).Select (d => d.VegetableDish).ToList ();
			else
				result = dishTypes.Where (t => t.Dish != null).Select (d => d.Dish).ToList ();
			return result;
		}
		private void CreateSpecialInstructionView()
		{
			_specialInstructionView = new UIView {
				AutosizesSubviews = false
			};
			var left = 20;
			float totalHeight = 0;
			for (int i = 0; i < _mealOrderModel.extraOrders.Count (); i++) {
				var captionTitle = _setting.CurrentAndNextMealPeriodExtraOrder ? _mealOrderModel.extraOrders [i].meal_period_label : _mealOrderModel.extraOrders [i].label;
				var caption = CreateSpecialInstructionCaption (new RectangleF (left, totalHeight, InsideWidth, 40), captionTitle);
				var table = CreateSpecialInstructionDetails (i, left, totalHeight + caption.Frame.Height, InsideWidth);
				var currentHeight = caption.Frame.Height + table.Frame.Height + 10;
				_specialInstructionView.AddSubview (caption);
				_specialInstructionView.AddSubview (table);
				totalHeight += currentHeight;
			}
			_specialInstructionView.Frame = new RectangleF (0, 0, InsideWidth, totalHeight);
			if (_mealOrderModel.extraOrders.Count == 0)
				SpecialInstructionsLabel.Hidden = true;
			AddSubview (_specialInstructionView);
		}
		private UITableView CreateSpecialInstructionDetails (int i, float left, float top, float width)
		{
			_specialInstructionDetails = _mealOrderModel.extraOrders [i].details.ToList ();
			if (!_specialInstructionDetails.Any ())
				_specialInstructionDetails.Add (new SpecialInstructionDetail { label = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("NoSpecialInstructionassigned.") });
				
			_specialInstructionSource = new DynamicTableSource<SpecialInstructionDetail, NameQuantityViewCell> (_specialInstructionViewCellHandler);
			var table = new UITableView () {
				BackgroundColor = UIColor.White,
				RowHeight = SpecialInstructionRowHeight,
				Frame = new RectangleF (left, top, width, SpecialInstructionRowHeight * _specialInstructionDetails.Count ()),
				Source = _specialInstructionSource,
				AutosizesSubviews = false
			};
			_specialInstructionSource.Items = _specialInstructionDetails;
			return table;
		}
		private UILabel CreateSpecialInstructionCaption (RectangleF frame, string title)
		{
			var defaultFont = UIFont.FromName("Helvetica", 18f);
			var caption = new PaddingLabel () {
				BackgroundColor = UIColor.Black,
				Frame = frame,
				Text = title,
				Font = defaultFont,
				TextColor = UIColor.White,
				EdgeInsets = new UIEdgeInsets (0, 20, 0, 20),
			};
			caption.UpdateMask (RoundedRectangle.RoundedTopCorners);
			return caption;
		}
		private void SetLayout ()
		{
			OrderSummaryLabel.Frame = new RectangleF (LeftPadding, 0, OuterWidth, 40);
			SummaryTable.SetFrameBelowTo(OrderSummaryLabel, LeftPadding, OuterWidth, _dishSource.Items.Count * DishRowHeight);
			MenuOfTheDayLabel.SetFrameBelowTo (SummaryTable, LeftPadding, OuterWidth, distanceToAbove: MenuOfTheDayLabel.Hidden ? 0 : 20);
			MenuOfTheDayTable.SetFrameBelowTo (MenuOfTheDayLabel, LeftPadding, OuterWidth, distanceToAbove: MenuOfTheDayLabel.Hidden ? 0 : 20);
			MealServedMayVaryLabel.SetFrameBelowTo (MenuOfTheDayTable, LeftPadding, OuterWidth, 40, 5);
			SpecialInstructionsLabel.SetFrameBelowTo (MealServedMayVaryLabel, LeftPadding, OuterWidth, 40);
			_specialInstructionView.SetFrameBelowTo (SpecialInstructionsLabel, LeftPadding, InsideWidth);

			var totalHeight = 100 + OrderSummaryLabel.Frame.Height + SummaryTable.Frame.Height + MenuOfTheDayTable.Frame.Height + MealServedMayVaryLabel.Frame.Height + SpecialInstructionsLabel.Frame.Height + _specialInstructionView.Frame.Height;
			if (!MenuOfTheDayTable.Hidden)
				totalHeight += 40;
			RemarksView.SetFrameBelowTo(_specialInstructionView.Frame.Height > 0 ? _specialInstructionView : MealServedMayVaryLabel, LeftPadding, 700);
			if (IsPatient) {
				RemarksView.Hidden = true;
				RemarksView.SetFrame (height: 0);
			} else {
				RemarksView.SetFrame (height: 350);
				RemarksLabel.SetFrame (0, 0, OuterWidth, 40);
				UseAsOngoingRemarkView.SetFrameBelowTo(RemarksLabel, 0, InsideWidth, distanceToAbove: 5);
				if (Settings.ShowOnGoingRemark && !_isViewOnly && !IsCompanion) {
					UseAsOngoingRemarkView.Hidden = false;
					UseAsOngoingRemarkSwitch.OnTintColor = Colors.MainThemeColor;
				} else {
					UseAsOngoingRemarkView.Hidden = true;
					UseAsOngoingRemarkView.SetFrame (height: 0);
				}
				DisclaimerRemarkLabel.SetFrameBelowTo (UseAsOngoingRemarkView, 0, 350, 25, 5);
				LimitLabel.Text = String.Format("({0}/{1})", 0, _setting.MaximumRemarkLength);
				LimitLabel.Frame = new RectangleF (InsideWidth - 100, DisclaimerRemarkLabel.Frame.Top, LimitLabel.Frame.Width, LimitLabel.Frame.Height);
				_subjectToAvailabilityLabel.SetFrameBelowTo (DisclaimerRemarkLabel, 0, InsideWidth, 40);
				_subjectToAvailabilityLabel.UpdateMask (RoundedRectangle.RoundedTopCorners);
				RemarkTextView.SetFrameBelowTo (_subjectToAvailabilityLabel, 0, _subjectToAvailabilityLabel.Frame.Width, 150, distanceToAbove: 0);
				totalHeight += 20 + RemarksView.Frame.Height;
			}

			MealOnHoldView.Hidden = Settings.HideMealOnHold || IsPatient;
			MealOnHoldView.SetFrameBelowTo (RemarksView, LeftPadding, InsideWidth, distanceToAbove: 5);
			totalHeight += 5 + MealOnHoldView.Frame.Height;

			EarlyDeliveryView.SetFrameBelowTo (MealOnHoldView, LeftPadding, 220, distanceToAbove: 5);
			if (Settings.HideEarlyDelivery || IsPatient || !_mealOrderModel.is_early_delivery_enabled) {
				EarlyDeliveryView.SetFrame (height: 0);
				EarlyDeliveryView.Hidden = true;
			} else
				totalHeight += 5 + EarlyDeliveryView.Frame.Height;

			StayForMealView.SetFrameBelowTo (EarlyDeliveryView, LeftPadding, InsideWidth, distanceToAbove: 5);
			if (ShouldEnableStayOption ()) {
				totalHeight += 5 + StayForMealView.Frame.Height;
			} else {
				StayForMealView.SetFrame (height: 0);
				StayForMealView.Hidden = true;
			}
			const float wheelWidth = 220;
			BiggerPortionView.SetFrameBelowTo (StayForMealView, LeftPadding, 220);
			SpicinessPicker.SetFrame (BiggerPortionView.Frame.Left + BiggerPortionView.Frame.Width + 10, BiggerPortionView.Frame.Top, wheelWidth);
			GarlicOnionPicker.SetFrame (SpicinessPicker.Frame.Left + SpicinessPicker.Frame.Width + 10, SpicinessPicker.Frame.Top, wheelWidth);
			LessSaltView.SetFrameBelowTo (BiggerPortionView, LeftPadding, 220, distanceToAbove: 0);
			LessOilyView.SetFrameBelowTo (LessSaltView, LeftPadding, 220, distanceToAbove: 0);
			MoreGravyView.SetFrameBelowTo (LessOilyView, LeftPadding, 220, distanceToAbove: 0);
			NoGarnishesView.SetFrameBelowTo (MoreGravyView, LeftPadding, 220, distanceToAbove: 0);

			if (Settings.ShowPatientPreference)
				totalHeight += 220;
			Frame = new RectangleF (0, 0, 768, totalHeight);
		}
		private bool ShouldEnableStayOption ()
		{
			if (Settings.HideStayForMeal || IsPatient || !_controller.GetRegistration ().planned_discharged_patient)
				return false;
			return true;
		}
		private void LimitChar()
		{
			RemarkTextView.ShouldChangeText = ((textView, range, text) => 
				{
					LimitLabel.Text = String.Format("({0}/{1})", textView.Text.Length, _setting.MaximumRemarkLength);
					var newContent = new NSString(textView.Text).Replace(range, new NSString(text)).ToString();
					return newContent.Length <= _setting.MaximumRemarkLength;
				});
		}
		private void SetTextsByLanguage()
		{
			OrderSummaryTitleLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("OrderSummary");
			SpecialInstructionsLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("SpecialInstructions");
			MealServedMayVaryLabel.Text = "(" + AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Mealservedmayvaryfromphoto") + ")";
			if(Settings.ShowInfoMsgOnMealOrdering)
				MealServedMayVaryLabel.Text = "(" + AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Picturesareforillustrationspurposesonly") + ")";
			RemarksLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Remarks");
			DisclaimerRemarkLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Requestwillbesubjectedtokitchenavailability");
			_subjectToAvailabilityLabel.Text = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Subjecttoavailability");
		}
		partial void BiggerPortionSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.bigger_portion = ((UISwitch) sender).On;
		}
		partial void MealOnHoldSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.put_on_hold = ((UISwitch) sender).On;
		}
		partial void StayForMealSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.stay_for_current_meal = ((UISwitch) sender).On;
		}
		partial void EarlyDelivery_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.early_delivery = ((UISwitch) sender).On;
		}
		partial void LessOilySwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.less_oily = ((UISwitch) sender).On;
		}
		partial void LessSaltSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.less_salt = ((UISwitch) sender).On;
		}
		partial void MoreGravySwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.more_gravy = ((UISwitch) sender).On;
		}
		partial void NoGarnishesSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender)
		{
			_mealOrderModel.no_garnishes = ((UISwitch) sender).On;
		}
		#region ISpecialInstructionDetailCallback implementation
		public void DetailQuantityChanged (SpecialInstructionDetail item)
		{
			if (item.id != null) {
				var detail = _mealOrderModel.extraOrders.First (e => e.meal_period_id == item.detailId).details.First (d => d.id == item.id);
				detail.selected = item.selected;
				detail.quantity = item.quantity;
			}
		}
		public void DetailSelectionChanged (SpecialInstructionDetail item)
		{
		}
		public bool GetAllowChangeQuantity ()
		{
			return _setting.ExtraOrderQuantityInMealOrderHidden;
		}
		#endregion
		#region ICallback implementation
		public IBaseContext GetBaseContext ()
		{
			return _setting.Controller.GetBaseContext ();
		}
		public void ItemSelected (SpecialInstructionDetail selected, int selectedIndex)
		{
		}
		#endregion
	}
}

