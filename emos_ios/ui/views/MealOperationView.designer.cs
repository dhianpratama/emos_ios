// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealOperationView
	{
		[Outlet]
		MonoTouch.UIKit.UITableView CutoffTableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView MealPeriodText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CutoffTableView != null) {
				CutoffTableView.Dispose ();
				CutoffTableView = null;
			}

			if (MealPeriodText != null) {
				MealPeriodText.Dispose ();
				MealPeriodText = null;
			}
		}
	}
}
