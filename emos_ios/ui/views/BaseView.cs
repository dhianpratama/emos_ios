﻿using System;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;

namespace emos_ios
{
	public abstract class BaseView<T> : UIView
	{
		public BaseView (IntPtr h) : base(h)
		{
		}
		public BaseView (string nibName)
		{
			var arr = NSBundle.MainBundle.LoadNib (nibName, this, null);
			var v = Runtime.GetNSObject (arr.ValueAt (0)) as UIView;
			AddSubview (v);
		}
		public virtual void SetCellContent(T item)
		{
		}
	}
}

