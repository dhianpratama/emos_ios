// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealOrderSummaryView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel AvailabilityStaticLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch BiggerPortionSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView BiggerPortionView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DisclaimerRemarkLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch EarlyDeliverySwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView EarlyDeliveryView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIPickerView GarlicOnionPicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch LessOilySwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LessOilyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch LessSaltSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView LessSaltView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LimitLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch MealOnHoldSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MealOnHoldView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MealServedMayVaryLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel MenuOfTheDayLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView MenuOfTheDayTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch MoreGravySwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MoreGravyView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch NoGarnishesSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NoGarnishesView { get; set; }

		[Outlet]
		emos_ios.MealOrderSummaryView OrderSummaryLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OrderSummaryTitleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel RemarksLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView RemarksView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView RemarkTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel SpecialInstructionsLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIPickerView SpicinessPicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch StayForMealSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView StayForMealView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView SummaryTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISwitch UseAsOngoingRemarkSwitch { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView UseAsOngoingRemarkView { get; set; }

		[Action ("BiggerPortionSwitch_ValueChanged:")]
		partial void BiggerPortionSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("EarlyDelivery_ValueChanged:")]
		partial void EarlyDelivery_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("LessOilySwitch_ValueChanged:")]
		partial void LessOilySwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("LessSaltSwitch_ValueChanged:")]
		partial void LessSaltSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("MealOnHoldSwitch_ValueChanged:")]
		partial void MealOnHoldSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("MoreGravySwitch_ValueChanged:")]
		partial void MoreGravySwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("NoGarnishesSwitch_ValueChanged:")]
		partial void NoGarnishesSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("StayForMealSwitch_ValueChanged:")]
		partial void StayForMealSwitch_ValueChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (MenuOfTheDayLabel != null) {
				MenuOfTheDayLabel.Dispose ();
				MenuOfTheDayLabel = null;
			}

			if (AvailabilityStaticLabel != null) {
				AvailabilityStaticLabel.Dispose ();
				AvailabilityStaticLabel = null;
			}

			if (BiggerPortionSwitch != null) {
				BiggerPortionSwitch.Dispose ();
				BiggerPortionSwitch = null;
			}

			if (BiggerPortionView != null) {
				BiggerPortionView.Dispose ();
				BiggerPortionView = null;
			}

			if (DisclaimerRemarkLabel != null) {
				DisclaimerRemarkLabel.Dispose ();
				DisclaimerRemarkLabel = null;
			}

			if (EarlyDeliverySwitch != null) {
				EarlyDeliverySwitch.Dispose ();
				EarlyDeliverySwitch = null;
			}

			if (EarlyDeliveryView != null) {
				EarlyDeliveryView.Dispose ();
				EarlyDeliveryView = null;
			}

			if (GarlicOnionPicker != null) {
				GarlicOnionPicker.Dispose ();
				GarlicOnionPicker = null;
			}

			if (LessOilySwitch != null) {
				LessOilySwitch.Dispose ();
				LessOilySwitch = null;
			}

			if (LessOilyView != null) {
				LessOilyView.Dispose ();
				LessOilyView = null;
			}

			if (LessSaltSwitch != null) {
				LessSaltSwitch.Dispose ();
				LessSaltSwitch = null;
			}

			if (LessSaltView != null) {
				LessSaltView.Dispose ();
				LessSaltView = null;
			}

			if (LimitLabel != null) {
				LimitLabel.Dispose ();
				LimitLabel = null;
			}

			if (MealOnHoldSwitch != null) {
				MealOnHoldSwitch.Dispose ();
				MealOnHoldSwitch = null;
			}

			if (MealOnHoldView != null) {
				MealOnHoldView.Dispose ();
				MealOnHoldView = null;
			}

			if (MealServedMayVaryLabel != null) {
				MealServedMayVaryLabel.Dispose ();
				MealServedMayVaryLabel = null;
			}

			if (MenuOfTheDayTable != null) {
				MenuOfTheDayTable.Dispose ();
				MenuOfTheDayTable = null;
			}

			if (MoreGravySwitch != null) {
				MoreGravySwitch.Dispose ();
				MoreGravySwitch = null;
			}

			if (MoreGravyView != null) {
				MoreGravyView.Dispose ();
				MoreGravyView = null;
			}

			if (NoGarnishesSwitch != null) {
				NoGarnishesSwitch.Dispose ();
				NoGarnishesSwitch = null;
			}

			if (NoGarnishesView != null) {
				NoGarnishesView.Dispose ();
				NoGarnishesView = null;
			}

			if (OrderSummaryLabel != null) {
				OrderSummaryLabel.Dispose ();
				OrderSummaryLabel = null;
			}

			if (OrderSummaryTitleLabel != null) {
				OrderSummaryTitleLabel.Dispose ();
				OrderSummaryTitleLabel = null;
			}

			if (RemarksLabel != null) {
				RemarksLabel.Dispose ();
				RemarksLabel = null;
			}

			if (RemarksView != null) {
				RemarksView.Dispose ();
				RemarksView = null;
			}

			if (RemarkTextView != null) {
				RemarkTextView.Dispose ();
				RemarkTextView = null;
			}

			if (SpecialInstructionsLabel != null) {
				SpecialInstructionsLabel.Dispose ();
				SpecialInstructionsLabel = null;
			}

			if (SpicinessPicker != null) {
				SpicinessPicker.Dispose ();
				SpicinessPicker = null;
			}

			if (StayForMealSwitch != null) {
				StayForMealSwitch.Dispose ();
				StayForMealSwitch = null;
			}

			if (StayForMealView != null) {
				StayForMealView.Dispose ();
				StayForMealView = null;
			}

			if (SummaryTable != null) {
				SummaryTable.Dispose ();
				SummaryTable = null;
			}

			if (UseAsOngoingRemarkSwitch != null) {
				UseAsOngoingRemarkSwitch.Dispose ();
				UseAsOngoingRemarkSwitch = null;
			}

			if (UseAsOngoingRemarkView != null) {
				UseAsOngoingRemarkView.Dispose ();
				UseAsOngoingRemarkView = null;
			}
		}
	}
}
