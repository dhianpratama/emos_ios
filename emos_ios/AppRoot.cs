﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using JASidePanels;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;

namespace emos_ios
{
	public class AppRoot
	{
		public AppRoot ()
		{
		}
		public UIViewController CreateContentController(core_emos.Menu.EmosMenu emosMenu, bool isRoot = true)
		{
			var result = new UIViewController ();
			if (!AppDelegate.Instance.IsAuthorized ()) {
				result = new LoginViewController (false);
				} else {
				switch (emosMenu) {
				case core_emos.Menu.EmosMenu.NurseDashboard:
					result = new MainViewController (emos_ios.Dashboard.OrderTabs.NurseDashboard);
					break;
				case core_emos.Menu.EmosMenu.PatientMealOrder:
					result = new MainViewController (emos_ios.Dashboard.OrderTabs.PatientMealOrder);
					break;
				case core_emos.Menu.EmosMenu.RestrictedPatientMealOrder:
					result = new PatientMealCalendarViewController (AppDelegate.Instance, AppDelegate.Instance.SelectedLocation, AppDelegate.Instance.SelectedRegistration, false);
					break;
				case core_emos.Menu.EmosMenu.RestrictedCompanionMealOrder:
					result = new PatientMealCalendarViewController (AppDelegate.Instance, AppDelegate.Instance.SelectedLocation, AppDelegate.Instance.SelectedRegistration, true);
					break;
				case core_emos.Menu.EmosMenu.CompanionMealOrder:
					result = new MainViewController (emos_ios.Dashboard.OrderTabs.CompanionMealOrder);
					break;
				case core_emos.Menu.EmosMenu.Setting:
					result = new AdminDVC (AppDelegate.Instance);
					break;
				case core_emos.Menu.EmosMenu.About:
					result = new AboutDVC (AppDelegate.Instance);
					break;
				case core_emos.Menu.EmosMenu.DishLanguage:
					result = new LanguageDVC (AppDelegate.Instance);
					break;

				}
			}
			result.View.AutosizesSubviews = false;
			return result;
		}
		private RectangleF InitControllerFrame ()
		{
			return new RectangleF (0, 90, 768, 964);
		}
		public UIViewController CreateMealOrderController (Dashboard.DashboardMenu dashboardMenu, bool IsPatient, PatientOrderCommand item, EventHandler<PatientOrderCommandEventArgs> onClose, EventHandler<PatientOrderCommandEventArgs> onSave, IPatientDayOrderCallback callback, string orderBy, bool isPatientCalendar)
		{
			item.OrderBy = orderBy;
			var controller = new MealOrderViewController (item, dashboardMenu, callback);
			controller.IsPatient = IsPatient;
			controller.IsPatientCalendar = isPatientCalendar;
			controller.OnClose += onClose;
			controller.OnSave += onSave;
			if (IsPatient)
				controller.ApplyRoleSetting (OrderRole.Patient);
			else
				controller.ApplyRoleSetting (OrderRole.Nurse);
			return controller;
		}
		public UIViewController CreateCompanionMealOrderController (bool IsPatient, PatientOrderCommand item)
		{
			var controller = new CompanionOrderListViewController (item);
			controller.IsPatient = IsPatient;
			controller.ViewOnly = OperationCodeHandler.IsOrdered (item.OperationCode);
			return controller;
		}
		public UIViewController CreateLanguageController (ProfileLanguageModelSimple profileLanguage, EventHandler<BaseEventArgs<UIViewController>> onClose, EventHandler<ProfileLanguageEventArgs> onSave)
		{
			var controller = new PatientLanguageViewController (profileLanguage);
			controller.OnClose += onClose;
			controller.OnSave += onSave;
			return controller;
		}
		public UIViewController CreateAuthenticationController (EventHandler<BaseEventArgs<UIViewController>> onClose, EventHandler onSuccess)
		{
			LoginViewController result;
			result = new LoginViewController (true);
			result.OnClose += onClose;
			result.OnSuccess += onSuccess;
			result.View.BackgroundColor = UIColor.Clear;
			result.View.Opaque = false;
			result.ShowAuthenticationMode ();
			return result;
		}
	}
}

