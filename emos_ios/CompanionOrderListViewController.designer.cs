// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CompanionOrderListViewController")]
	partial class CompanionOrderListViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView CompanionOrderTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView NotFoundView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CompanionOrderTable != null) {
				CompanionOrderTable.Dispose ();
				CompanionOrderTable = null;
			}

			if (NotFoundView != null) {
				NotFoundView.Dispose ();
				NotFoundView = null;
			}
		}
	}
}
