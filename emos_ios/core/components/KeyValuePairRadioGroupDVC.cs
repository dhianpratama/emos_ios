﻿using System;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class KeyValuePairRadioGroupDVC<T> : Section where T: BaseIpadModel
	{
		public RootElement _keyOptions, _valueOptions;

		public KeyValuePairRadioGroupDVC(string caption) : base (caption)
		{
		}

		public KeyValuePairRadioGroupDVC<T> CreateElement(string caption1, long? modelId1, List<T> list1,
			string caption2, long? modelId2, List<T> list2)
		{
			_keyOptions = new RadioGroupDVC<T> ()
				.CreateRadioGroup(caption1, modelId1, list1);

			_valueOptions = new RadioGroupDVC<T> ()
				.CreateRadioGroup(caption2, modelId2, list2);
			this.Add (_keyOptions);
			this.Add (_valueOptions);

			return this;
		}

		public int KeySelectedIndex ()
		{
			return _keyOptions.RadioSelected;
		}
		public int ValueSelectedIndex ()
		{
			return _valueOptions.RadioSelected;
		}

	}
}

