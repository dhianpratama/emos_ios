﻿using System;
using MonoTouch.Foundation;
using ios;
using MonoTouch.UIKit;

namespace emos_ios
{
		public class ColorHandler : IColorHandler
	{
		private const char ColorSeparator = '/';
		private const string MainThemeColorKey = "MainThemeColor";
		private const string ButtonBackgroundKey = "ButtonBackground";
		private const string MealOrderStepButtonBackgroundKey = "MealOrderStepButtonBackground";
		private UIColor _mainThemeColor;
		public UIColor MainThemeColor {
			get {
				return _mainThemeColor;
			}
			private set { }
		}
		private UIColor _buttonBackground;
		public UIColor ButtonBackground {
			get {
				return _buttonBackground;
			}
			private set { }
		}
		private UIColor _mealOrderStepButtonBackground;
		public UIColor MealOrderStepButtonBackground {
			get {
				return _mealOrderStepButtonBackground;
			}
			private set { }
		}
		public UIColor ViewCellBackground {
			get {
				return Colors.ViewCellBackground;
			}
		}
		public UIColor MealOrderBackground {
			get {
				return Colors.MealOrderBackground;
			}
		}
		public ColorHandler ()
		{
			InitiateColors ();
		}
		public void UpdateTheme (int mainThemeColor, int buttonBackground, int mealOrderStepButtonBackground)
		{
			NSUserDefaults.StandardUserDefaults.SetInt (mainThemeColor, MainThemeColorKey);
			NSUserDefaults.StandardUserDefaults.SetInt (buttonBackground, ButtonBackgroundKey);
			NSUserDefaults.StandardUserDefaults.SetInt (mealOrderStepButtonBackground, MealOrderStepButtonBackgroundKey);
			InitiateColors ();
		}
		public void PaintNavigationBar (UINavigationBar bar)
		{
			bar.BarTintColor = MainThemeColor;
			bar.BackgroundColor = MainThemeColor;
			bar.SetTitleTextAttributes(new UITextAttributes {
				TextColor = UIColor.White
			});
		}
		private void InitiateColors ()
		{
			_mainThemeColor = ReadColor (MainThemeColorKey);
			_buttonBackground = ReadColor (ButtonBackgroundKey);
			_mealOrderStepButtonBackground = ReadColor (MealOrderStepButtonBackgroundKey);
		}
		private UIColor ReadColor (string ColorKey)
		{
			var color = NSUserDefaults.StandardUserDefaults.IntForKey (ColorKey);
			return color != 0 ? UIColorExtension.FromHex (color) : GetDefaultColor (ColorKey);
		}
		private static UIColor GetDefaultColor (string ColorKey)
		{
			switch (ColorKey) {
			case MainThemeColorKey:
				return Colors.MainThemeColor;
			case ButtonBackgroundKey:
				return Colors.ButtonBackground;
			case MealOrderStepButtonBackgroundKey:
				return Colors.MealOrderStepButtonBackground;
			}
			return UIColor.White;
		}
		private string JoinAsString(UIColor color)
		{
			float red;
			float green;
			float blue;
			float alpha;
			color.GetRGBA (out red, out green, out blue, out alpha);
			return String.Concat (red, ColorSeparator, green, ColorSeparator, blue, ColorSeparator, alpha);
		}
	}
}

