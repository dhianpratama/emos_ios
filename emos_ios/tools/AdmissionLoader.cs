﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios;

namespace emos_ktph
{
	public class AdmissionLoader : IAdmissionLoader
	{
		public void LoadEditAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location, IPatientMenu callback)
		{
			var controller = new AdmissionViewController (appContext, location, callback);
			appContext.LoadController (false, true, controller);
		}
		public void LoadSpecialInstruction (IApplicationContext appContext, LocationWithRegistrationSimple location, IPatientMenu callback, InterfaceStatusModel _interfaceStatus)
		{
			var controller = new SpecialInstructionViewController (callback, location, _interfaceStatus, appContext) {
				
			};
			appContext.Nav.PushViewController(controller, true);
		}
	}
}

