﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public class BaseAdmissionLoader : IAdmissionLoader
	{
		public void LoadEditAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location, EventHandler onSave = null)
		{
			var registration = location.registrations.FirstOrDefault ();
			var registrationId = registration == null ? null : registration.registration_id;
			var dvc = new EditAdmissionDVC (registrationId, location.id);
			appContext.PushDVC (dvc);
		}
		public void LoadViewAdmission (IApplicationContext appContext, LocationWithRegistrationSimple location)
		{
			var dvc = new ViewAdmissionDVC (location);
			appContext.PushDVC (dvc);
		}
	}
}
