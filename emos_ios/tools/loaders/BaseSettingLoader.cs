﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class BaseSettingLoader : ISettingLoader
	{
		public void Load (IApplicationContext appContext, bool hideDateSetting = false, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDone = null, EventHandler OnCancel = null, EventHandler<BaseEventArgs<KeyValuePair<string,string>>> OnDateChanged = null)
		{
			var controller = new SettingViewController (hideDateSetting);
			controller.CloseEvent += OnCancel;
			controller.HospitalSelectionCancelled += OnCancel;
			controller.WardSelectionCancelled += OnCancel;
			controller.WardSelectionDone += OnDone;
			controller.OperationDateSelectionCancelled += OnCancel;
			controller.OperationDateSelectionDone += OnDateChanged;
			controller.OperationTimeSelectionCancelled += OnCancel;
			controller.OperationTimeSelectionDone += OnDone;
			appContext.LoadModalView (controller.View);
		}
	}
}

