﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public class BasePatientInfoLoader : ILoader
	{
		public void Load (IApplicationContext appContext)
		{
			var controller = new PatientInfoViewController ();
			appContext.LoadModalView (controller.View);
			controller.Load ();
		}
	}
}

