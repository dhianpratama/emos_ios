﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;

namespace emos_ios
{
	public class BaseSpecialInstructionLoader : ISpecialInstructionLoader
	{
		public void LoadSpecialInstruction (IApplicationContext appContext, LocationWithRegistrationSimple location, IPatientMenu callback, InterfaceStatusModel _interfaceStatus)
		{
			var controller = new ExtraOrderListViewController (appContext, location, _interfaceStatus) {
				PatientMenuCallback = callback
			};
			appContext.Nav.PushViewController(controller, true);
		}
	}
}
