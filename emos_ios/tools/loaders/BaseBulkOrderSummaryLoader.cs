﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class BaseBulkOrderSummaryLoader : IBulkOrderSummaryLoader
	{
		public void Load (IApplicationContext appContext, KeyValuePair<string,string> locationId)
		{
			appContext.ShowAlert (this, new ios.MessageEventArgs ("NotImplemented"));
		}
	}
}