﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios.tools;
using System.Threading;
using emos_ios.models;

using VMS_IRIS.Areas.EmosIpad.Models;
using ios;
using System.Net;
using JASidePanels;
using core_emos;

namespace emos_ios
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate, IHttpSenderContext, IApplicationContext
	{
		private const int TimeOutTime = 1800;
		UIWindow _window;
		public ServerConfig ServerConfig { get; set; }
		public event EventHandler OnConnectionProblem;
		public UINavigationController Nav { get; private set; }
		public HttpSender HttpSender { get; set; }
		public User CurrentUser { get; set; }
		public KeyboardHandler KeyboardHandler { get; set; }
		public IControllerLoader ControllerLoader { get; set; }
		public long? InstitutionId { get; set; }
		//TODO: remove this field later
		public RegistrationModelSimple SelectedRegistration { get; set; }
		public LocationModelSimple SelectedLocation { get; set; }
		public MealOrderPeriodModelSimple SelectedMealOrderPeriod { get; set; }
		public long? SelectedOperationCode { get; set; }
		public DateTime OperationDate { get; set; }
		public long? WardGroupId { get; set; }
		public long? WardId { get; set; }
		public long? HospitalId { get; set; }
		public KeyValuePair<string, string> Ward { get; set; }
		public KeyValuePair<string, string> Hospital { get; set; }
		public KeyValuePair<string, string> MealOrderPeriodGroup { get; set; }
		public IEnumerable<MealOrderPeriodGroupModel> MealOrderPeriodGroups { get; set; }
		private AppRoot _appRoot;
		public WardViewHandler WardViewHandler { get; set; }
		public HospitalViewHandler HospitalViewHandler { get; set; }
		public LanguageHandler LanguageHandler { get; set; }
		public OperationCodeHandler OperationCodeHandler { get; set; }
		public string MealPeriodGroupCode { get; set; } = "MEAL";
		public Dashboard.OrderTabs CurrentTab = Dashboard.OrderTabs.NurseDashboard;
		private JASidePanelController _sidePanelController;
		private LoadingOverlay _loadingOverlay;
		private TimerHandler _sessionTimerHandler;
		public LockHandler LockHandler { get; set; }
		public IColorHandler ColorHandler { get; set; }
		public List<LanguageModelSimple> Languages { get; set; }
		private UIViewController _languageViewController;
		private UIViewController _authenticationViewController;
		public string WardLabel { get; set; }

		public static AppDelegate Instance
		{
			get {
				return UIApplication.SharedApplication.Delegate as AppDelegate;
			}
		}

		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			var jsonSerializer = new JsonSerializer ();
			ServerConfig = new ServerConfig ();
			this.InstitutionId = ServerConfig.InsitutionId;
			OperationDate = ServerConfig.OperationDate == DateTime.MinValue? DateTime.Today.Date : ServerConfig.OperationDate;
			Ward = new KeyValuePair<string,string> ();
			MealOrderPeriodGroup = new KeyValuePair<string, string> ("MEAL", "Normal Meals");
			MealOrderPeriodGroups = new List<MealOrderPeriodGroupModel> ();
			HttpSender = new HttpSender (this, jsonSerializer, SynchronizationContext.Current);
			WardViewHandler = new WardViewHandler (this);
			HospitalViewHandler = new HospitalViewHandler ();
			KeyboardHandler = new KeyboardHandler ();
			InitiateControllerLoaders ();
			LanguageHandler = new LanguageHandler {
				LanguageCode = "en"
			};
			OperationCodeHandler = new OperationCodeHandler (this);
			LockHandler = new LockHandler (this, Settings.CheckLock);
			ColorHandler = new ColorHandler ();
			CurrentUser = new User ();
			_appRoot = new AppRoot ();
			_window = new UIWindow (UIScreen.MainScreen.Bounds);
			// If you have defined a root view controller, set it here:
			ShowLogin ();
			PaintNavigationBar ();
			// make the window visible
			_window.MakeKeyAndVisible ();
			
			return true;
		}
		private void InitiateControllerLoaders ()
		{
			ControllerLoader = new ControllerLoader ();
			if (ControllerLoader.AdmissionLoader == null)
				ControllerLoader.AdmissionLoader = new BaseAdmissionLoader ();
			if (ControllerLoader.SpecialInstructionLoader == null)
				ControllerLoader.SpecialInstructionLoader = new BaseSpecialInstructionLoader ();
			if (ControllerLoader.FeedbackLoader == null)
				ControllerLoader.FeedbackLoader = new BaseFeedbackLoader ();
			if (ControllerLoader.PatientInfoLoader == null)
				ControllerLoader.PatientInfoLoader = new BasePatientInfoLoader ();
			if (ControllerLoader.SettingLoader == null)
				ControllerLoader.SettingLoader = new BaseSettingLoader ();
			if (ControllerLoader.BulkOrderSummaryLoader == null)
				ControllerLoader.BulkOrderSummaryLoader = new BaseBulkOrderSummaryLoader ();
			if (ControllerLoader.BulkOrderDetailLoader == null)
				ControllerLoader.BulkOrderSummaryLoader = new BaseBulkOrderSummaryLoader ();
		}
		private void ShowLogin ()
		{
			LoadMenu (core_emos.Menu.EmosMenu.Login);
		}
		public void PaintNavigationBar ()
		{
			ColorHandler.PaintNavigationBar (Nav.NavigationBar);
			UINavigationBar.Appearance.TintColor = UIColor.White;
		}
		public void LoadMenu(core_emos.Menu.EmosMenu emosMenu, bool isRoot = true, bool showLeftMenu = true)
		{
			var controller = _appRoot.CreateContentController (emosMenu, isRoot);
			LoadController (isRoot, showLeftMenu, controller);
		}
		public void LoadController (bool isRoot, bool showLeftMenu, UIViewController controller)
		{
			if (isRoot) {
				if (Nav != null)
					Nav.Dispose ();
				Nav = new UINavigationController (controller) {
					ModalPresentationStyle = UIModalPresentationStyle.CurrentContext
				};
				PaintNavigationBar ();
				if (IsAuthorized () && showLeftMenu) {
					InitiateSideController (Nav);
					controller.NavigationItem.LeftBarButtonItem.Image = UIImage.FromFile ("Images/threelines.png");
				}
				else {
					_window.RootViewController = Nav;
				}
			}
			else {
				Nav.PushViewController (controller, true);
			}
		}
		public void LoadNurseMealOrder (bool isRoot, bool showLeftMenu, Dashboard.DashboardMenu dashboardMenu, bool IsPatient, PatientOrderCommand item, EventHandler<PatientOrderCommandEventArgs> onClose, EventHandler<PatientOrderCommandEventArgs> onSave, IPatientDayOrderCallback callback, bool isPatientCalendar)
		{
			var orderRole = OrderRole.Nurse;
			var controller = _appRoot.CreateMealOrderController (dashboardMenu, IsPatient, item, onClose, onSave, callback, orderRole, isPatientCalendar);
			LoadController (isRoot, showLeftMenu, controller);
		}
		public void LoadPatientMealOrder (bool isRoot, bool showLeftMenu, Dashboard.DashboardMenu dashboardMenu, bool IsPatient, PatientOrderCommand item, EventHandler<PatientOrderCommandEventArgs> onClose, EventHandler<PatientOrderCommandEventArgs> onSave, IPatientDayOrderCallback callback, bool isPatientCalendar)
		{
			var orderRole = OrderRole.Patient;
			var controller = _appRoot.CreateMealOrderController (dashboardMenu, IsPatient, item, onClose, onSave, callback, orderRole, isPatientCalendar);
			LoadController (isRoot, showLeftMenu, controller);
		}
		public void LoadCompanionMealOrder (bool isRoot, bool showLeftMenu, bool IsPatient, PatientOrderCommand item)
		{
			var controller = _appRoot.CreateCompanionMealOrderController (IsPatient, item);
			LoadController (isRoot, showLeftMenu, controller);
		}
		public void LoadPatientLanguage (bool isRoot, bool showLeftMenu, ProfileLanguageModelSimple profileLanguage, EventHandler<ProfileLanguageEventArgs> onSave)
		{
			_languageViewController = _appRoot.CreateLanguageController (profileLanguage, CloseChildView, onSave);
			UIApplication.SharedApplication.KeyWindow.AddSubview (_languageViewController.View);
		}
		public void LoadAuthentication (EventHandler onSuccess)
		{
			_authenticationViewController = _appRoot.CreateAuthenticationController (CloseChildView, onSuccess);
			UIApplication.SharedApplication.KeyWindow.AddSubview (_authenticationViewController.View);
		}
		private void CloseChildView (object sender, BaseEventArgs<UIViewController> e)
		{
			UIViewController controller = e.Value;
			controller.View.RemoveFromSuperview ();
			controller.Dispose ();
		}
		public void InitiateSideController (UIViewController controller)
		{
			_sidePanelController = new JASidePanelController ();
			_sidePanelController.ShouldDelegateAutorotateToVisiblePanel = false;
			_sidePanelController.LeftPanel = new SideMenuDVC (this);
			_sidePanelController.LeftFixedWidth = 220.0f;	
			_window.RootViewController = _sidePanelController;
			if (controller != null)
				_sidePanelController.CenterPanel = controller;
		}
		private CookieContainer _currentAspxAuth;
		CookieContainer IHttpSenderContext.CurrentAspxAuth {
			get {
				return _currentAspxAuth;
			}
			set {
				_currentAspxAuth = value;
			}
		}
		string IHttpSenderContext.BaseUrl
		{
			get 
			{
				return ServerConfig.BaseUrl;
			}
		}
		void IHttpSenderContext.OnServerUnavailable()
		{
		}
		void IHttpSenderContext.OnConnectionProblem ()
		{
			if (OnConnectionProblem!=null)
				OnConnectionProblem (this, null);
		}
		public bool IsAuthorized()
		{
			return !string.IsNullOrEmpty (AppDelegate.Instance.CurrentUser.Id);
		}
		public void SignOut (bool isAdmin = false)
		{
			if (String.IsNullOrEmpty (CurrentUser.Id)) {
				OnLogoutSuccessful ();
				return;
			}
			CurrentUser = new User ();
			Languages = new List<LanguageModelSimple> ();

			LockHandler.UnlockRegistrationsByUser ();
			_sessionTimerHandler.Stop ();
			RequestLogout (isAdmin);
		}
		public void ClearAllGlobalData()
		{
			AppDelegate.Instance.CurrentUser = new User ();
			AppDelegate.Instance.SelectedLocation = null;
			AppDelegate.Instance.SelectedMealOrderPeriod = null;
			AppDelegate.Instance.SelectedOperationCode = null;
			AppDelegate.Instance.SelectedRegistration = null;
			AppDelegate.Instance.WardGroupId = null;
			AppDelegate.Instance.WardId = null;
			AppDelegate.Instance.Ward = new KeyValuePair<string,string>("","");
			AppDelegate.Instance.HospitalId = null;
			AppDelegate.Instance.Hospital = new KeyValuePair<string,string>("","");
		}
		public void RequestLogout (bool isAdmin = false)
		{
			if (AppDelegate.Instance.ServerConfig.IsDemo || isAdmin) {
				OnLogoutSuccessful ();
			} else {
				SendLogoutRequest ();
			}
		}
		public async void SendLogoutRequest()
		{
			var queryString = KnownUrls.LogoutQueryString (AppDelegate.Instance.CurrentUser.Id);
			await AppDelegate.Instance.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.Logout)
				.WithQueryString (queryString)
				.WhenSuccess (result => OnLogoutSuccessful ())
				.WhenFail (result => OnLogoutFailed ())
				.Go ();
		}
		private void OnLogoutSuccessful()
		{
			if (_sessionTimerHandler != null)
				_sessionTimerHandler.Stop ();
			ClearAllGlobalData ();
			ShowLogin ();
		}
		private void OnLogoutFailed()
		{

		}
		private void ShowLoadingOverlay(RequestLoadingEventArgs e)
		{
			try {
				if (_loadingOverlay != null && !_loadingOverlay.Hidden) {
					return;
				}
			} catch (ObjectDisposedException) {
			}
			string loadingText;
			if (e.LoadingText == "Loading")
				loadingText = LanguageHandler.GetLocalizedString ("Loading");
			else
				loadingText = e.LoadingText;

			_loadingOverlay = new LoadingOverlay (UIScreen.MainScreen.Bounds, loadingText);
			e.View.AddSubview (_loadingOverlay);
		}
		public void HideLoadingOverlay (object sender, EventArgs e)
		{
			try {
				if (_loadingOverlay != null) {
					_loadingOverlay.Hide ();
					_loadingOverlay.Dispose ();
				}
			} catch (ObjectDisposedException) {
				return;
			}
		}
		public void ShowAlert (object sender, MessageEventArgs e)
		{
			_window.ShowAlert(this, e.TitleCode, e.MessageCode);
		}
		public void PushDVC (IBaseDVC dvc)
		{
			Nav.PushViewController ((BaseDVC) dvc, true);
		}
		public void PopViewController(bool animated=true)
		{
			AppDelegate.Instance.Nav.PopViewControllerAnimated (animated);
		}
		public BaseUser GetBaseUser()
		{
			return CurrentUser;
		}
		private void RenewSession ()
		{
			if (_sessionTimerHandler != null)
				_sessionTimerHandler.Stop ();

			_sessionTimerHandler = new TimerHandler();
			_sessionTimerHandler.OnTick += HandleOnSessionTimerTick;
			_sessionTimerHandler.Restart(DateTime.UtcNow, TimeOutTime);
		}
		private void HandleOnSessionTimerTick (object sender, EventArgs e)
		{
			InvokeOnMainThread (() => {
				SignOut ();
			});
		}
		public void StartRequest(object sender, RequestLoadingEventArgs e)
		{
			ShowLoadingOverlay (e);
			RenewSession ();
		}
		public void SetHospitalByWard ()
		{
			if (Ward.Key == null)
				return;
			foreach (var hospital in CurrentUser.AssignedLocation.hospitals) {
				if (hospital.wards.Any (w => w.id == long.Parse(Ward.Key))) {
					HospitalId = hospital.id;
					Hospital = new KeyValuePair<string, string> (hospital.id.ToString (), hospital.label);
					return;
				}
			}
		}
		public void SetGlobalWard(KeyValuePair<string,string> selected)
		{
			if (string.IsNullOrEmpty (selected.Key))
				return;
			WardId = long.Parse (selected.Key);
			Ward = selected;
			if (!Settings.HospitalBasedWards)
				SetHospitalByWard ();
		}
		public void LoadModalView (UIView view)
		{
			UIApplication.SharedApplication.KeyWindow.AddSubview (view);
		}
	}
}

