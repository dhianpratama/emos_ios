﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public static class Colors
	{
		public static UIColor DarkViewCellBackground = UIColor.LightGray;
		public static UIColor ViewCellBackground = UIColor.FromRGB (230, 230, 230);
		public static UIColor MealOrderBackground = UIColor.FromRGB (210, 210, 210);
		public static UIColor HeaderBackground = UIColor.LightGray;
		public static UIColor MainThemeColor = UIColor.FromRGB (178, 169, 128); //Parkway Blue
		public static UIColor ButtonBackground = UIColor.FromRGB (214, 212, 189); // Parkway Green
		public static UIColor MealOrderStepButtonBackground = UIColor.FromRGB (214, 212, 189); // Parkway Green
		public static UIColor PlannedDischargeBackground = UIColor.FromRGB (248, 173, 22);
		public static UIColor RegularAdmissionBackground = UIColor.FromRGBA (49, 136, 204, 48);
		public static UIColor ManualAdmissionBackground = UIColor.FromRGBA (147, 118, 32, 30);
		public static UIColor ManualAdmissionButtonColor = UIColor.Clear;
		public static UIColor ManualAdmissionButtonFontColor = UIColor.Clear;
		public static UIColor NurseDashboardPatientTableSeparatorColor = UIColor.Black;
		public static UIColor NurseDashboardHeaderBackgroundColor = UIColor.LightGray;
		public static UIColor NurseDashboardHeaderButtonColor = UIColor.Black;
		public static UIColor NurseDashboardHeaderFontColor = UIColor.Black;
		public static UIColor NurseDashboardTopBarColor = UIColor.DarkGray;
	}
}

