﻿using System;

namespace emos_ios
{
	public static class Measurements
	{
		public static float FrameTop = 64f;
		public static float LogoHeight = 104f;
		public static float KeyboardHeight = 270f;
		public static float TopLogoY = 0f;
		public static float TopLogoHeight = 0f;
		public static float HeaderHeight = 290f;
		public static float MealOrderHeaderHeight = HeaderHeight;
		public static float HeaderImageHeight = 0f;

		public static float BottomLogoY = 920f;
		public static float BottomLogoHeight = 104;

		public static float NurseDashboardRowHeight = 55f;
		public static float NurseDashboardTopBarHeight = 60f;
		public static float CompanionMealOrderHeaderHeight = HeaderHeight;
		public static float PatientMealOrderHeaderHeight = HeaderHeight;
		public static float NurseDashboardCutoffTitleHeight = 57f;
		public static float NurseDashboardCutOffTableDistance = 20f;
		public static float NurseDashboardOperationContainerLeft = 300f;

		public static float NurseLoginLabelHeight = 40f;

		public static float BottomY = 920f;
		public static float LogoY = 920f;

		public static float TopY = TopLogoHeight + FrameTop;
		public static float PageHeight = 1024 - FrameTop - LogoHeight;
		public static float BodyHeight = PageHeight - HeaderHeight;
		public static float TherapeuticListTableHeight = 800;
		public static float TrialOrderTableHeight = 800;
		public static float LoginImageHeight = 550;
		public static float LoginImageDistanceToNurseLogin = 20;
	}
}

