// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("CutOffViewCell")]
	partial class CutOffViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel CutoffLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel DayDescriptionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel TimeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CutoffLabel != null) {
				CutoffLabel.Dispose ();
				CutoffLabel = null;
			}

			if (DayDescriptionLabel != null) {
				DayDescriptionLabel.Dispose ();
				DayDescriptionLabel = null;
			}

			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
		}
	}
}
