// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace core_emos
{
	[Register ("VerticalMealOrderViewCell")]
	partial class VerticalMealOrderViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton OrderButton { get; set; }

		[Action ("OrderButton_TouchDown:")]
		partial void OrderButton_TouchDown (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (OrderButton != null) {
				OrderButton.Dispose ();
				OrderButton = null;
			}
		}
	}
}
