﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using System.Collections.Generic;

using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using System.Linq;
using core_emos;
using ios;

namespace emos_ios
{
	public partial class MealOrderViewCell : BaseViewCell<List<DishModelWithTags>>, IImageUpdated
	{
		public const string CellIdentifier = "MealOrderViewCell";
		public static UITableViewCellSeparatorStyle SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
		private MealOrdersViewCellHandler _handler;
		private IMealOrdersCallback _callback;
		private List<DishModelWithTags> _item;
		public bool IsSelected;
		private ImageDownloader _imageDownloader = new ImageDownloader ();

		public MealOrderViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (List<DishModelWithTags> item)
		{
			_item = item;
			DownloadImage (_item.First ().picture_url);
			NameTextView.Text = item [0].label;
			if (item [0].active_data) {
				IsSelected = true;
				OrderButon.BackgroundColor = Colors.ButtonBackground;
				OrderButon.SetTitle (_callback.GetBaseContext ().LanguageHandler.GetLocalizedString ("Ordered"), UIControlState.Normal);
			}
			OrderButon.Hidden = false;
			DietitianText.Hidden = true;
			if (item [0].isAdditionalDish) {
				DietitianText.Hidden = false;
				if (!_callback.GetBaseContext ().GetBaseUser ().Authorized ("ACCESS_MORE_DISHES")) {
					OrderButon.Hidden = true;
				}
			}
			OrderButon.Layer.CornerRadius = 10;
		}
		protected override void SetViewCellHandler(IViewCellHandler<List<DishModelWithTags>> handler)
		{
			_handler = (MealOrdersViewCellHandler) handler;
			_callback = (IMealOrdersCallback) _handler.Callback;
		}
		partial void OrderButtonClicked (MonoTouch.Foundation.NSObject sender)
		{
			IsSelected = !IsSelected;
			if(IsSelected)
				OrderButon.BackgroundColor = UIColor.Red;
			else
				OrderButon.BackgroundColor = UIColor.Gray;

			_callback.OrderDish(_item);
		}
		partial void MealImageButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			_callback.ShowDetail(_item);
		}
		private void DownloadImage (string imagePath, int width = 154, int height = 154)
		{
			_imageDownloader.OnImageNotFound += HandleOnImageNotFound;
			_imageDownloader.OnImageFound += HandleOnImageFound;
			_imageDownloader.Download (_handler.Callback.GetBaseContext (), this, imagePath, width, height);
		}
		private void HandleOnImageFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				MealImageButton.SetImage (ImageDownloader.LazyLoad (_imageDownloader.ImageUrl, this), UIControlState.Normal);
			});
		}
		public void UpdatedImage (Uri uri)
		{
			InvokeOnMainThread (() => {
				if (String.Equals(uri.ToString (), _imageDownloader.ImageUrl, StringComparison.OrdinalIgnoreCase))
					MealImageButton.SetImage (ImageDownloader.LazyLoad (uri.ToString (), this), UIControlState.Normal);
			});
		}
		private void HandleOnImageNotFound (object sender, StringEventArgs e)
		{
			InvokeOnMainThread (() => {
				MealImageButton.SetImage (ImageDownloader.GetNotAvailableImage (), UIControlState.Normal);
			});
		}
	}
}

