// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("PatientMenuViewController")]
	partial class PatientMenuViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton ClearFeedsButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton CloseButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ConsumptionChecklistButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton DischargePatientButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton EditAdmissionButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ExtraOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FeedbackButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FluidRestrictionButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FoodAllergiesButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton FullFeedsButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton MealTypeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView MenuView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton NilByMouthButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton SnackPackButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TherapeuticDietButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TransferPatientButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton TrialOrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ViewAdmissionButton { get; set; }

		[Action ("ClearFeeds_TouchUpInside:")]
		partial void ClearFeeds_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("ClosePatientMenu_TouchUpInside:")]
		partial void ClosePatientMenu_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("ConsumptionChecklist_TouchUpInside:")]
		partial void ConsumptionChecklist_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("DischargePatient_TouchUpInside:")]
		partial void DischargePatient_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("EditAdmission_TouchUpInside:")]
		partial void EditAdmission_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("Feedback_TouchUpInside:")]
		partial void Feedback_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("FluidRestriction_TouchUpInside:")]
		partial void FluidRestriction_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("FoodAllergies_TouchUpInside:")]
		partial void FoodAllergies_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("FullFeeds_TouchUpInside:")]
		partial void FullFeeds_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("MealType_TouchUpInside:")]
		partial void MealType_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("NilByMouth_TouchUpInside:")]
		partial void NilByMouth_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("SnackPack_TouchUpInside:")]
		partial void SnackPack_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("SpecialInstruction_TouchUpInside:")]
		partial void SpecialInstruction_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("TherapeuticDietOrder_TouchUpInside:")]
		partial void TherapeuticDietOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("TransferPatient_TouchUpInside:")]
		partial void TransferPatient_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("TrialOrder_TouchUpInside:")]
		partial void TrialOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender);

		[Action ("VIewAdmission_TouchUpInside:")]
		partial void VIewAdmission_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ClearFeedsButton != null) {
				ClearFeedsButton.Dispose ();
				ClearFeedsButton = null;
			}

			if (CloseButton != null) {
				CloseButton.Dispose ();
				CloseButton = null;
			}

			if (ConsumptionChecklistButton != null) {
				ConsumptionChecklistButton.Dispose ();
				ConsumptionChecklistButton = null;
			}

			if (DischargePatientButton != null) {
				DischargePatientButton.Dispose ();
				DischargePatientButton = null;
			}

			if (EditAdmissionButton != null) {
				EditAdmissionButton.Dispose ();
				EditAdmissionButton = null;
			}

			if (ExtraOrderButton != null) {
				ExtraOrderButton.Dispose ();
				ExtraOrderButton = null;
			}

			if (FeedbackButton != null) {
				FeedbackButton.Dispose ();
				FeedbackButton = null;
			}

			if (FluidRestrictionButton != null) {
				FluidRestrictionButton.Dispose ();
				FluidRestrictionButton = null;
			}

			if (FoodAllergiesButton != null) {
				FoodAllergiesButton.Dispose ();
				FoodAllergiesButton = null;
			}

			if (FullFeedsButton != null) {
				FullFeedsButton.Dispose ();
				FullFeedsButton = null;
			}

			if (MealTypeButton != null) {
				MealTypeButton.Dispose ();
				MealTypeButton = null;
			}

			if (NilByMouthButton != null) {
				NilByMouthButton.Dispose ();
				NilByMouthButton = null;
			}

			if (SnackPackButton != null) {
				SnackPackButton.Dispose ();
				SnackPackButton = null;
			}

			if (TherapeuticDietButton != null) {
				TherapeuticDietButton.Dispose ();
				TherapeuticDietButton = null;
			}

			if (TransferPatientButton != null) {
				TransferPatientButton.Dispose ();
				TransferPatientButton = null;
			}

			if (TrialOrderButton != null) {
				TrialOrderButton.Dispose ();
				TrialOrderButton = null;
			}

			if (ViewAdmissionButton != null) {
				ViewAdmissionButton.Dispose ();
				ViewAdmissionButton = null;
			}

			if (MenuView != null) {
				MenuView.Dispose ();
				MenuView = null;
			}
		}
	}
}
