﻿
using System;
using System.Drawing;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.IO;
using MonoTouch.Dialog.Utilities;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios.tools;

namespace emos_ios
{
	public partial class PatientMenuViewController : BaseViewController
	{
		private IPatientMenu _controller;
		private LocationWithRegistrationSimple _location;
		private InterfaceStatusModel _interfaceStatus;
		private List<KeyValuePair<string,NSAction>> _menus;
		private int _heightView;

		public PatientMenuViewController (IApplicationContext appContext, IPatientMenu controller, LocationWithRegistrationSimple location, InterfaceStatusModel interfaceStatus) : base (appContext, "PatientMenuViewController", null)
		{
			_controller = controller;
			_location = location;
			_interfaceStatus = interfaceStatus;
			HiddenLogo = true;
			RegisterMenu ();
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if (_interfaceStatus.ADT) {
				DisableButton (EditAdmissionButton, "SAP UP");
				DisableButton (DischargePatientButton, "SAP UP");
				DisableButton (TransferPatientButton, "SAP UP");
			}
			if (!AppContext.CurrentUser.Authorized ("ACCESS_MORE_DISHES")) {
				DisableButton (ExtraOrderButton);
				DisableButton (TrialOrderButton);
			}
			GenerateMenuToView ();
			CalculateHeightView ();

		}
		private void RegisterMenu()
		{
			_menus = new List<KeyValuePair<string,NSAction>> ();
			_menus.Add(new KeyValuePair<string,NSAction>("View Admission", ()=>_controller.ViewAdmission(_location)));
			if (!_interfaceStatus.ADT) {
				_menus.Add (new KeyValuePair<string,NSAction> ("Edit Admission", () => _controller.EditAdmission(_location)));
				_menus.Add (new KeyValuePair<string,NSAction> ("Discharge Patient", () => _controller.DischargePatient (_location)));
				_menus.Add (new KeyValuePair<string,NSAction> ("Transfer Patient", () => _controller.TransferPatient (_location)));
			}
			_menus.Add (new KeyValuePair<string,NSAction> ("Consumption Charting", () => _controller.ConsumptionChecklist (_location, _location.registrations.First ())));
			_menus.Add(new KeyValuePair<string,NSAction>("Meal Type", ()=>_controller.MealType(_location)));
			_menus.Add(new KeyValuePair<string,NSAction>("Food Allergies", ()=>_controller.FoodAllergies(_location, null, null)));
			_menus.Add(new KeyValuePair<string,NSAction>("Diet Orders", ()=>_controller.TherapeuticDietOrder(_location, null, null)));
			_menus.Add(new KeyValuePair<string,NSAction>("Fluid Restriction", ()=>_controller.FluidRestriction(_location)));
			_menus.Add(new KeyValuePair<string,NSAction>("Snack Pack", ()=>_controller.SnackPack(_location)));
			_menus.Add(new KeyValuePair<string,NSAction>("Nil by Mouth", ()=>_controller.NillByMouth(_location)));
			_menus.Add(new KeyValuePair<string,NSAction>("Full Feeds", ()=>_controller.FullFeeds(_location)));
			_menus.Add(new KeyValuePair<string,NSAction>("Clear Feeds", ()=>_controller.ClearFeeds(_location)));
			if (AppContext.CurrentUser.Authorized ("ACCESS_EXTRA_ORDERS")) {
				_menus.Add(new KeyValuePair<string,NSAction>("Extra Order", ()=>_controller.SpecialInstruction(_location)));
			}
			if (AppContext.CurrentUser.Authorized("ACCESS_TRIAL_DIET_ORDERS")) {

				_menus.Add(new KeyValuePair<string,NSAction>("Trial Order", ()=>_controller.TrialOrder(_location)));
			}
			_menus.Add(new KeyValuePair<string,NSAction>("Meal Feedback", ()=>_controller.Feedback(_location)));
		}

		private void GenerateMenuToView()
		{
			int i = 0;
			_menus.ForEach (m => {
				int gap = 1;
				var button = new UIButton();
				button.Frame = new RectangleF(0, (i*40)+(gap+i),540,40);
				button.SetTitle(m.Key,UIControlState.Normal);
				button.SetTitleColor(UIColor.Black,UIControlState.Normal);
				button.Font = UIFont.BoldSystemFontOfSize(21f);
				button.BackgroundColor = UIColor.White;
				button.TouchUpInside += delegate {
					DismissViewController(true, m.Value);
				};
				MenuView.AddSubview(button);
				i++;
			});
		}

		private void CalculateHeightView()
		{
			if (_menus != null) {
				_heightView = (_menus.Count * 40) + (1 + _menus.Count) + 60;
			}
		}

		private void DisableButton (UIButton button, string message="")
		{
			button.Enabled = false;
			button.SetTitleColor (UIColor.LightGray, UIControlState.Disabled);
			button.SetTitle (String.Format ("{0} {1}", button.Title (UIControlState.Normal), message), UIControlState.Disabled);
		}
		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 540, _heightView);
		}
		partial void ClearFeeds_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, ()=>_controller.ClearFeeds(_location));
		}
		partial void ClosePatientMenu_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			_controller.ClosePatientMenu();
		}
		partial void ConsumptionChecklist_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.ConsumptionChecklist(_location, _location.registrations.First()));
		}
		partial void DischargePatient_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(_interfaceStatus.ADT==false)
				DismissViewController(true, () => _controller.DischargePatient(_location));
			else
				this.ShowAlert(AppContext, "Youcan'tdischargepatientwhenSAPison.");
		}
		partial void EditAdmission_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(_interfaceStatus.ADT==false)
				DismissViewController(true, () => _controller.EditAdmission(_location));
			else
				this.ShowAlert(AppContext, "Youcan'teditadmissionwhenSAPison.");
		}
		partial void FullFeeds_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.FullFeeds(_location));
		}
		partial void MealType_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.MealType(_location));
		}
		partial void NilByMouth_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.NillByMouth(_location));
		}
		partial void SnackPack_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.SnackPack(_location));
		}			
		partial void TherapeuticDietOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.TherapeuticDietOrder(_location, null, null));
		}
		partial void TransferPatient_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			if(_interfaceStatus.ADT==false)
				DismissViewController(true, () => _controller.TransferPatient(_location));
			else
				this.ShowAlert(AppContext, "Youcan'teditadmissionwhenSAPison.");
		}
		partial void FoodAllergies_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.FoodAllergies(_location, null, null));
		}
		partial void SpecialInstruction_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.SpecialInstruction(_location));
		}
		partial void VIewAdmission_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, () => _controller.ViewAdmission(_location));
		}

		partial void TrialOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, ()=> _controller.TrialOrder(_location));
		}

		partial void FluidRestriction_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, ()=> _controller.FluidRestriction(_location));
		}

		partial void Feedback_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DismissViewController(true, ()=> _controller.Feedback(_location));
		}
		
		public override void SetTextsByLanguage()
		{
			ViewAdmissionButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("ViewAdmission"), UIControlState.Normal);
			EditAdmissionButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("EditAdmission"), UIControlState.Normal);
			DischargePatientButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("DischargePatient"), UIControlState.Normal);
			TransferPatientButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("TransferPatient"), UIControlState.Normal);
			ConsumptionChecklistButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("ConsumptionCharting"), UIControlState.Normal);
			MealTypeButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("MealType"), UIControlState.Normal);
			FoodAllergiesButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("FoodAllergies"), UIControlState.Normal);
			TherapeuticDietButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("TherapeuticDietOrder"), UIControlState.Normal);
			SnackPackButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("SnackPack"), UIControlState.Normal);
			NilByMouthButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("NilbyMouth"), UIControlState.Normal);
			FullFeedsButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("FullFeeds"), UIControlState.Normal);
			ClearFeedsButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("ClearFeeds"), UIControlState.Normal);
			ExtraOrderButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("ExtraOrder"), UIControlState.Normal);
			TrialOrderButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("TrialOrder"), UIControlState.Normal);
			FluidRestrictionButton.SetTitle (AppContext.LanguageHandler.GetLocalizedString ("FluidRestriction"), UIControlState.Normal);
		}

	}
}

