﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using core_emos;

namespace emos_ios
{
	public partial class SideMenuDVC : BaseDVC
	{
		public SideMenuDVC (IApplicationContext applicationContext) : base (applicationContext)
		{
			var emosMenuText = AppContext.LanguageHandler.GetLocalizedString ("EmosMenu");
			var mainMenuText = AppContext.LanguageHandler.GetLocalizedString ("MainMenu");
			var nurseDashboardText = AppContext.LanguageHandler.GetLocalizedString ("NurseDashboard");
			var othersText = AppContext.LanguageHandler.GetLocalizedString ("Others");
			var settingText = AppContext.LanguageHandler.GetLocalizedString ("Setting");
			var signoutText = AppContext.LanguageHandler.GetLocalizedString ("Signout");

			var rootElement = new RootElement (emosMenuText) {
				new Section() {
				},
				new Section() {
				},
				new Section (mainMenuText) {
					new StringElement (nurseDashboardText, () => {
						AppContext.LoadMenu (Menu.EmosMenu.NurseDashboard);
					}),
				}
			};
			if (AppContext.CurrentUser.IsAdmin) {
				rootElement.Add (new Section (othersText) {
					new StringElement (settingText, () => {
						AppContext.LoadMenu (Menu.EmosMenu.Setting);
					}),
				});
			}
			rootElement.Add (new Section () {
				new StringElement (signoutText, () => {
					AppContext.SignOut(AppContext.CurrentUser.IsAdmin);
				})
			});
			Root = rootElement;
		}

	}
}
