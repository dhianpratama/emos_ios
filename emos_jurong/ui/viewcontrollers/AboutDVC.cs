﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using emos_ios;

namespace emos_ios
{
	public partial class AboutDVC : BaseDVC
	{
		public AboutDVC (IApplicationContext appContext) : base (appContext)
		{
			LogoY = Measurements.FrameTop;
			CreateRoot (AppContext.LanguageHandler.GetLocalizedString ("Admin"));
		}
		public override void CreateRoot(string title = "")
		{
			base.CreateRoot (title);
			var versionText = AppContext.LanguageHandler.GetLocalizedString ("ServerIP");
			Root.Add (new Section ());
			Root.Add (new Section ());
			Root.Add (new Section () {
				(new EntryElement (versionText, "", AppContext.ServerConfig.Version)),
			});
		}
	}
}