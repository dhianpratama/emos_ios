﻿using System;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.ObjCRuntime;
using emos_ios.tools;
using ios;
using System.Drawing;

namespace emos_ios
{
	public abstract class BaseDVC : DialogViewController, IBaseDVC
	{
		protected IApplicationContext AppContext;
		protected RequestHandler _requestHandler;
		protected bool HiddenLogo;
		protected UIView LogoView;
		protected float LogoY;

		public BaseDVC (IApplicationContext appContext, bool hiddenLogo = true) : base (UITableViewStyle.Grouped, null, true)
		{
			AppContext = appContext;
			HiddenLogo = hiddenLogo;
			_requestHandler = new RequestHandler (appContext, this);
		}
		public virtual void CreateRoot(string title = "")
		{
			Root = new RootElement (title);
		}
		public virtual void GetData ()
		{
		}
		public UIView GetDvcView()
		{
			return View;
		}
		protected virtual void OnRequestCompleted(bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			_requestHandler.CompletedRequest (success, failedTitleCode, showFailedTitle);
		}
		public IBaseContext GetBaseContext()
		{
			return AppContext;
		}
	}
}

