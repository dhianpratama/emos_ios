﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace emos_ios
{
	[Register("LogoView")]
	public partial class LogoView : UIView
	{
		public LogoView (IntPtr h) : base(h)
		{
		}
		public LogoView (string version, bool isDemo)
		{
			var arr = NSBundle.MainBundle.LoadNib("LogoView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
			VersionLabel.Text = version;
			LogoImage.ContentMode = UIViewContentMode.ScaleAspectFit;
			LogoImage.SetFrame (width: 479, height: 100);
			SetImage ();
		}
		public void ChangeOperationDateLabel (string operationDate)
		{
		}
		public void SetImage ()
		{
			LogoImage.Image = UIImage.FromBundle ("Images/logo.png");
		}
		public void SetDefaultLogoImage()
		{
		}
	}
}

