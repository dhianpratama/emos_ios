// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class LogoView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView LogoImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel VersionLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (VersionLabel != null) {
				VersionLabel.Dispose ();
				VersionLabel = null;
			}

			if (LogoImage != null) {
				LogoImage.Dispose ();
				LogoImage = null;
			}
		}
	}
}
