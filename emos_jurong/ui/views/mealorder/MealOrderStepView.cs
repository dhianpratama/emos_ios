﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using VMS_IRIS.Areas.EmosIpad.Models;
using MonoTouch.ObjCRuntime;
using System.Drawing;
using ios;

namespace emos_ios
{
	[Register("MealOrderStepView")]
	public partial class MealOrderStepView : UIView
	{
		public int Step { get; private set; }
		private bool _pressed;
		private IBaseContext _appContext;
		public bool Pressed {
			get { return _pressed; }
			set { _pressed = value; Select (value); }
		}
		public EventHandler OnStepClicked;
		public MealOrderStepView(IntPtr h): base(h)
		{
		}
		public MealOrderStepView (IBaseContext appContext, string buttonTitle, int step)
		{
			_appContext = appContext;
			var arr = NSBundle.MainBundle.LoadNib("MealOrderStepView", this, null);
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as UIView;
			AddSubview (v);
			MealOrderButton.Layer.CornerRadius = 10;
			Step = step;
		}
		public void SetButtonTitle(string buttonTitle)
		{
			MealOrderButton.SetTitle (buttonTitle, UIControlState.Normal);
		}
		partial void Order (MonoTouch.Foundation.NSObject sender)
		{
			var handler = OnStepClicked;
			if (handler != null)
				handler(this, new EventArgs());
		}
		private	void Select (bool selected)
		{
			if (selected) {
				MealOrderButton.BackgroundColor = _appContext.ColorHandler.MainThemeColor;
			} else {
				MealOrderButton.BackgroundColor = _appContext.ColorHandler.MealOrderStepButtonBackground;
			}
		}
	}
}

