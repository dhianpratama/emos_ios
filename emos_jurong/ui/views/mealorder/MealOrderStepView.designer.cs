// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	partial class MealOrderStepView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton MealOrderButton { get; set; }

		[Action ("Order:")]
		partial void Order (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (MealOrderButton != null) {
				MealOrderButton.Dispose ();
				MealOrderButton = null;
			}
		}
	}
}
