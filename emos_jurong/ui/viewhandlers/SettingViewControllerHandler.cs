﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using emos_ios;

namespace emos_ios
{
	public static class SettingViewControllerHandler
	{
		public static void AnimateMe(UIViewController viewController, UIView backgroundView)
		{
			backgroundView.Frame = new RectangleF (368, UIScreen.MainScreen.Bounds.Bottom, 400, 400);
			UIView.BeginAnimations ("slideAnimation");
			UIView.SetAnimationDuration (0.5);
			UIView.SetAnimationDelegate (viewController);
			backgroundView.Center = new PointF (UIScreen.MainScreen.Bounds.Right - backgroundView.Frame.Width / 2, Measurements.FrameTop + backgroundView.Frame.Height / 2);
			UIView.CommitAnimations ();
		}
	}
}

