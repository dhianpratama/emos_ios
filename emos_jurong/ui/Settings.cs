﻿using System;

namespace emos_ios
{
	//Jurong
	public static class Settings
	{
		public static bool UseDateAsDashboardTitle = true;
		public static bool HideNurseDashboardTab = false;
		public static bool ShowCompanionLabelOnDashboard = true;
		public static bool ShowBedNameOnDashboard = false;
		public static bool HideMealOnHold = true;
		public static bool HideStayForMeal = true;
		public static bool HideEarlyDelivery = true;
		public static long DefaultNewExtraOrderQuantity = 1;
		public static bool IgnoreIncompleteOrder = false;
		public static bool UseMealPeriodAsBirthdayCakeSegment = true;
		public static bool ChangeCompanionLabelToLodger = false;
		public static bool SimpleTrialOrder = false;
		public static bool DoubleTapNurseDashboardTableView = true;
		public static bool UseDietOrderText = true;
		public static bool UseExtraOrdersText = true;
		public static bool MultipleCompanionOrder = false;
		public static bool MandatoryMainDish = true;
		public static bool HideMealPeriodGroupInSetting = false;
		public static bool EnableDeclineMealOrder = false;
		public static bool HideSaveDraft = false;
		public static bool FoodAllergiesRemarkSpecialQuery = true;
		public static bool ShowOtherDietsCommentField = true;
		public static bool ShowActiveDietInformation = true;
		public static bool ShowOnGoingRemark = true;
		public static bool ShowPatientPreference = false;
		public static bool CheckLock = false;
		public static bool ShowDVCTitle = true;
		public static bool ShowAdditionalPatientMenuLinks = false;
		public static bool UseRegistrationNumberInsteadOfId = false;
		public static int StartDateToEndDateDefaultDays = 7;
		public static bool DisableExtraOrderEdit = true;
		public static bool OneDishMeal = true;
		public static bool LoadExistingOrderOnEdit = false;
		public static bool AllowDashboardCutoffToggle = false;
		public static bool PatientDailyOrderMode = false;
		public static bool QueryMealTypeWithFeeds = false;
		public static bool EnableTheme = false;
		public static bool FoodTextureIsCompulsory = false;
		public static bool ShowDetailOnImageView = false;
		public static bool ShowYesNoImageButtonOnMealOrderDetail = false;
		public static bool UseSmallCap = false;
		public static bool ShowOrderRoleSelection = false;
		public static bool ShowMealOrderTitle = true;
		public static bool ShowGotoPatientModeButtonOnMealOrder = false;
		public static bool ShowImageAsHeader = false;
		public static bool DietsEndToday = false;
		public static bool CommonDietsVersion2 = false;
		public static bool HideLoginNavigationBar = false;
		public static bool ShowWardSelectionInLogin = false;
		public static bool ReplaceMealOrderBackButton = false;
		public static bool UseDateForDiets = false;
		public static bool HospitalBasedWards = true;
		public static bool GradientMenuView = false;
		public static bool ShowInfoMsgOnMealOrdering = true;
		public static bool MealOrderBasedPatientInfo = true;
		public static bool HideNurseDashboardIcons = false;
		public static bool HideAvailability = false;
		public static bool HideDateSettingOnPatientMeal = false;
		public static bool ShowTherapeuticComment = true;
		public static bool CompanionMealChargeableOption = false;
		public static bool CurrentAndNextMealPeriodExtraOrder = false;
		public static bool ExtraOrderQuantityInMealOrderHidden = true;
		public static int ExtraOrderMinimumQuantity = 0;
		public static int ExtraOrderMaximumQuantity = 3;
		public static int MaximumRemarkLengthInMealOrder = 150;
		public static bool CheckLoginAcl = false;
		public static bool HideSkippedStep = true;
		public static bool ShowAvailabilityInMealOrderTabs = false;
		public static bool AllowToAddOnlyOneTherapeutic = true;
		public static bool HideMenuOfTheDay = true;
		public static bool HideManualAdmissionOnSapUp = false;
	}
}

