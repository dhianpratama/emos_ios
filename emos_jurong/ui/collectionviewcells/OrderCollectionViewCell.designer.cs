// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("OrderCollectionViewCell")]
	partial class OrderCollectionViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel CompanionStatusLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton OrderButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OrderByLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel OrderLabel { get; set; }

		[Action ("ButtonOrder_TouchUpInside:")]
		partial void ButtonOrder_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (OrderButton != null) {
				OrderButton.Dispose ();
				OrderButton = null;
			}

			if (OrderByLabel != null) {
				OrderByLabel.Dispose ();
				OrderByLabel = null;
			}

			if (OrderLabel != null) {
				OrderLabel.Dispose ();
				OrderLabel = null;
			}

			if (CompanionStatusLabel != null) {
				CompanionStatusLabel.Dispose ();
				CompanionStatusLabel = null;
			}
		}
	}
}
