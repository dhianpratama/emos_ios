﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using System.Linq;
using System.Drawing;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class GroupedTableSource<TKey, T, TCell> : DynamicTableSource<T, TCell> where TCell : BaseViewCell<T>
	{
		public List<TKey> Keys { get; private set; }
		public IList<string> SectionTitles { get; set; }
		public IDictionary<TKey, List<T>> GroupedItems { get; set; }
		public GroupedTableSource (IViewCellHandler<T> cellHandler) : base (cellHandler)
		{
		}
		public override int RowsInSection (UITableView tableView, int section)
		{
			return GroupedItems[Keys[section]].Count;
		}
		public override int NumberOfSections (UITableView tableView)
		{
			return GroupedItems.Count;
		}
		public void SetGroupedItems (IDictionary<TKey, List<T>> items, IList<string> sectionTitles)
		{
			GroupedItems = items;
			Keys = items.Select (g => g.Key).ToList ();
			Items = items.SelectMany (g => g.Value).ToList ();
			SectionTitles = sectionTitles;
		}
		public override string TitleForHeader (UITableView tableView, int section)
		{
			return SectionTitles[section];
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = (TCell) tableView.DequeueReusableCell (CellHandler.CellIdentifier);
			if (cell == null) {
				cell = (TCell)Nib.Instantiate (null, null) [0];
			}
			cell.BackgroundColor = CellHandler.BaseViewCellSetting.BackgroundColor;
			cell.Initialize (CellHandler, GetItem(indexPath));

			return cell;
		}
		private T GetItem(NSIndexPath indexPath)
		{
			return GroupedItems [Keys [indexPath.Section]] [indexPath.Row];
		}
		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CellHandler.GetHeightForRow == null ? tableView.RowHeight : CellHandler.GetHeightForRow (GetItem (indexPath));
		}
//		public override UIView GetViewForHeader (UITableView tableView, int section)
//		{
//			var root = new UIView ();
//			root.Frame = new RectangleF (0, 0, 768, 40);
//			root.ClipsToBounds = true;
//			UIImage img = UIImage.FromFile ("Images/Breakfast.png");
//			var imgView = new UIImageView (img, img) { Frame = root.Frame };
//			root.AddSubview(imgView);
//			return root;
//		}
	}
}

