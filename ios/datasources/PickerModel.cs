﻿using System;
using MonoTouch.UIKit;
using System.Collections;
using emos_ios;
using System.Collections.Generic;

namespace ios
{
	public class PickerModel : UIPickerViewModel
	{
		private List<KeyValuePair<int, string>> _items;
		public List<KeyValuePair<int, string>> Items {
			get { return _items; }
			set {
				_items = value;
				Selected (null, 0, 0);
			}
		}
		public KeyValuePair<int, string> SelectedItem;
		public event EventHandler<PickerChangedEventArgs> OnSelectionChanged;

		public PickerModel(List<KeyValuePair<int, string>> values)
		{
			Items = values;
		}
		public override int GetComponentCount (UIPickerView picker)
		{
			return 1;
		}
		public override int GetRowsInComponent (UIPickerView picker, int component)
		{
			if (NoItem ())
				return 1;
			return Items.Count;
		}
		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			if (NoItem(row))
				return "";
			return Items [row].Value;
		}
		public override float GetRowHeight (UIPickerView picker, int component)
		{
			return 40f;
		}
		private bool NoItem(int row = 0)
		{
			return Items == null || row >= Items.Count;
		}
		public override void Selected (UIPickerView picker, int row, int component)
		{
			if (this.OnSelectionChanged != null)
			{
				this.OnSelectionChanged (this, new PickerChangedEventArgs{ SelectedValue = Items [row] });
			}
		}
	}

	public class PickerChangedEventArgs : EventArgs{
		public KeyValuePair<int, string> SelectedValue {get;set;}
	}
}

