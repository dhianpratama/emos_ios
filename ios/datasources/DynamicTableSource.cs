﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.Dialog;
using System.Drawing;

namespace emos_ios
{
	public class DynamicTableSource<T, TCell> : UITableViewSource where TCell : BaseViewCell<T>
	{
		public List<T> Items { get; set; }
		protected IViewCellHandler<T> CellHandler;
		protected readonly UINib Nib;
		public event EventHandler<RowSelectionEventArgs<T>> OnItemSelected;
		public event EventHandler<RowSelectionEventArgs<T>> OnItemDeselected;
		public bool ReloadOnSelectionChanged { get; set; }
		public bool CheckmarkVisible { get; set; }
		public DynamicTableSource (IViewCellHandler<T> cellHandler)
		{
			ReloadOnSelectionChanged = false;
			Items = new List<T> ();
			CellHandler = cellHandler;
			Nib = UINib.FromName (CellHandler.CellIdentifier, NSBundle.MainBundle);
		}

		public override int RowsInSection (UITableView tableView, int section)
		{
			return Items.Count ();
		}
		public override int NumberOfSections (UITableView tableView)
		{
			return 1;
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = (TCell) tableView.DequeueReusableCell (CellHandler.CellIdentifier);
			if (cell == null)
				cell = (TCell)Nib.Instantiate (null, null) [0];
			SetBackgroundColor (cell, indexPath);
			cell.Initialize (CellHandler, Items [indexPath.Row]);
			return cell;
		}
		private void SetBackgroundColor (TCell cell, NSIndexPath indexPath)
		{
			if (!CellHandler.BaseViewCellSetting.AlternateBackground) {
				cell.BackgroundColor = CellHandler.BaseViewCellSetting.BackgroundColor;
				return;
			}
			if (indexPath == null)
				return;
			var index = indexPath.Row;
			cell.BackgroundColor = (index % 2 == 0) ? CellHandler.BaseViewCellSetting.BackgroundColor : CellHandler.BaseViewCellSetting.AlternativeBackgroundColor;
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (!Items.Any ())
				return;
			if (CellHandler.Callback != null)
				CellHandler.Callback.ItemSelected (Items [indexPath.Row], indexPath.Row);
			this.OnItemSelected.SafeInvoke (this, new RowSelectionEventArgs<T> (tableView, Items [indexPath.Row], indexPath.Row));
		}
		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			this.OnItemDeselected.SafeInvoke (this, new RowSelectionEventArgs<T>(tableView, Items[indexPath.Row], indexPath.Row));
		}
		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CellHandler.GetHeightForRow == null ? tableView.RowHeight : CellHandler.GetHeightForRow (Items [indexPath.Row]);
		}
		public void SelectRow(int index, UITableView tableView)
		{
			var indexPath = NSIndexPath.FromRowSection (index, 0);
			tableView.SelectRow (indexPath, false, UITableViewScrollPosition.None);
			RowSelected (tableView, indexPath);
			ShowCheckmark (tableView, indexPath);
		}
		private void ShowCheckmark (UITableView tableView, NSIndexPath indexPath)
		{
			if (!CheckmarkVisible)
				return;
			var cell = (TCell)tableView.CellAt (indexPath);
			if (cell != null)
				cell.Accessory = UITableViewCellAccessory.Checkmark;
		}
		public void DeselectRow(int index, UITableView tableView)
		{
			var indexPath = NSIndexPath.FromRowSection (index, 0);
			HideCheckmark (tableView, indexPath);
			tableView.DeselectRow(indexPath, false);
			RowDeselected(tableView, indexPath);
		}
		private void HideCheckmark (UITableView tableView, NSIndexPath indexPath)
		{
			if (!CheckmarkVisible)
				return;
			var cell = (TCell)tableView.CellAt (indexPath);
			if (cell != null) {
				cell.Accessory = UITableViewCellAccessory.None;
			}
		}
	}
	public class RowSelectionEventArgs<T> : EventArgs
	{
		public const int NoIndex = 999999;
		public UITableView Table { get; set; }
		public T Item { get; set; }
		public int Index { get; set; }
		public RowSelectionEventArgs(UITableView table, T item, int index = NoIndex)
		{
			Table = table;
			Item = item;
			Index = index;
		}
		public bool NoIndexSet()
		{
			return Index == NoIndex;
		}
	}
}