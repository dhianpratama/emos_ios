﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public class BaseViewCellSetting
	{
		public bool CustomRowSize { get; set; }
		public float CellWidth { get; set;}
		public float CellHeight { get; set; }
		public UIColor BackgroundColor { get; set; }
		public UIColor AlternativeBackgroundColor  { get; set; }
		public bool AlternateBackground { get; set; }

		public BaseViewCellSetting(UIColor defaultCellBackground = null)
		{
			BackgroundColor = defaultCellBackground ?? UIColor.FromRGB (230, 230, 230);
			AlternativeBackgroundColor = UIColor.White;
		}
	}
}

