﻿using System;
using ios;

namespace emos_ios
{
	public class NoCallbackViewCellHandler<T> : BaseViewCellHandler<T>
	{
		public NoCallbackViewCellHandler (IBaseContext baseContext, string cellIdentifier, BaseViewCellSetting setting) : base(cellIdentifier, new NoCallback<T>(baseContext), setting)
		{
		}
	}
}

