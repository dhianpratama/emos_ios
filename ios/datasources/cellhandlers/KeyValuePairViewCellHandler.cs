﻿using System;
using System.Collections.Generic;

namespace emos_ios
{
	public class KeyValuePairViewCellHandler : BaseViewCellHandler<KeyValuePair<string, string>>
	{
		public KeyValuePairViewCellHandler (ICallback<KeyValuePair<string, string>> callback, BaseViewCellSetting setting) : base(KeyValuePairViewCell.CellIdentifier, callback, setting)
		{
		}
	}
}

