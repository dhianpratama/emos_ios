﻿using System;
using MonoTouch.UIKit;

namespace ios
{
	public class ImageEventArgs : EventArgs
	{
		public UIImage Image { get; set; }
		public ImageEventArgs (UIImage image)
		{
			Image = image;
		}
	}
}

