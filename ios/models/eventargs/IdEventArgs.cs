﻿using System;

namespace ios
{
	public class IdEventArgs : EventArgs
	{
		public long? Id { get; set; }
		public IdEventArgs (long? id)
		{
			Id = id;
		}
	}
}

