﻿using System;

namespace emos_ios
{
	public class BaseEventArgs <T> : EventArgs
	{
		public T Value {get; set;}
	}
	public static class BaseEventArgsExtensions
	{
		public static BaseEventArgs<T> Initiate <T>(this BaseEventArgs<T> baseEventArgs, T value)
		{
			baseEventArgs.Value = value;
			return baseEventArgs;
		}
	}
}

