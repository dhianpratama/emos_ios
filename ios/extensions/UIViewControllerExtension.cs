﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using ios;

namespace emos_ios
{
	public static class UIViewControllerExtension
	{
		public enum AlertType {
			Error, Information
		}
		public static void ShowAlert (this NSObject vc, IBaseContext appContext, string messageKey, Action action = null, bool translateMessage = true)
		{
			ShowAlert (vc, appContext, AlertType.Information, messageKey, action, translateMessage);
		}
		public static void ShowAlert (this NSObject vc, IBaseContext appContext, AlertType alertType, string messageCode, Action action, bool translateMessage = true)
		{
			string titleCode;
			switch (alertType) {
			case AlertType.Error:
				titleCode = "Error";
				break;
			default:
				titleCode = "Information";
				break;
			}
			ShowAlert (vc, appContext, titleCode, messageCode, action: action, translateMessage: translateMessage);
		}
		public static UIAlertView ShowAlert (this NSObject vc, IBaseContext appContext, string titleCode, string messageCode, string okCode = "OK", Action action = null, bool translateMessage = true)
		{
			var message = translateMessage ? appContext.LanguageHandler.GetLocalizedString (messageCode) : messageCode;
			var okText = appContext.LanguageHandler.GetLocalizedString (okCode);
			var title = appContext.LanguageHandler.GetLocalizedString(titleCode);
			var alert = new UIAlertView(title, message, null, okText, new string[0] {});
			if (action != null)
				alert.Clicked += (s,e) => action();
			if (!NSThread.IsMain)
				vc.BeginInvokeOnMainThread (() => alert.Show ());
			else
				alert.Show();
			return alert;
		}
		public static UIAlertView ShowConfirmAlert(this NSObject vc, IBaseContext appContext, string title, string messageCode, Action action, bool translateMessage = true, Action actionCancel = null)
		{
			var okText = appContext.LanguageHandler.GetLocalizedString ("OK");
			var cancelText = appContext.LanguageHandler.GetLocalizedString ("Cancel");
			var titleMessage = translateMessage ? appContext.LanguageHandler.GetLocalizedString (title) : title;
			var message = translateMessage ? appContext.LanguageHandler.GetLocalizedString (messageCode) : messageCode;

			var alert = new UIAlertView (titleMessage, message, null, okText, new string[] { cancelText });
			alert.Clicked += (s, b) => {
				if (b.ButtonIndex == 0 && action != null) {
					action ();
				} else if (b.ButtonIndex == 1 && actionCancel != null) {
					actionCancel ();
				}
			};
			alert.Show ();
			return alert;
		}
		public static UIAlertView ShowSelectionConfirmAlert(this NSObject vc, IBaseContext appContext, string title, string messageCode, Action action, Action cancelAction)
		{
			var okText = appContext.LanguageHandler.GetLocalizedString ("Yes");
			var cancelText = appContext.LanguageHandler.GetLocalizedString ("No");

			var alert = new UIAlertView (title, messageCode, null, okText, new string[] { cancelText });
			alert.Clicked += (s, b) => {
				if (b.ButtonIndex == 0 && action != null) {
					action ();
				} else if (b.ButtonIndex == 1 && cancelAction != null) {
					cancelAction ();
				}
			};
			alert.Show ();
			return alert;
		}
	}
}


