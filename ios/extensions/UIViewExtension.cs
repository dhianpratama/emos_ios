﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public static class UIViewExtension
	{
		public static void SetFrame (this UIView view, float? x = null, float? y = null, float? width = null, float? height = null)
		{
			view.Frame = new RectangleF (x ?? view.Frame.Left, y ?? view.Frame.Top, width ?? view.Frame.Width, height ?? view.Frame.Height);
		}
		public static void SetFrame (this UITableViewCell viewCell, float? x = null, float? y = null, float? width = null, float? height = null)
		{
			viewCell.Frame = new RectangleF (x ?? viewCell.Frame.Left, y ?? viewCell.Frame.Top, width ?? viewCell.Frame.Width, height ?? viewCell.Frame.Height);
		}
		public static void SetFrameBelowTo (this UIView view, UIView aboveView, float? x = null, float? width = null, float? height = null, float distanceToAbove = 20)
		{
			view.Frame = new RectangleF (x ?? view.Frame.Left, aboveView.Frame.Top + aboveView.Frame.Height + distanceToAbove, width ?? view.Frame.Width, height ?? view.Frame.Height);
		}
	}
}

