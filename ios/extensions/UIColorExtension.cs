﻿using System;
using MonoTouch.UIKit;

namespace ios
{
	public static class UIColorExtension
	{
		public static UIColor FromHex(int hexValue)
		{
			try {
				var color = UIColor.FromRGB (
					           (((float)((hexValue & 0xFF0000) >> 16)) / 255.0f),
					           (((float)((hexValue & 0xFF00) >> 8)) / 255.0f),
					           (((float)(hexValue & 0xFF)) / 255.0f)
				           );
				return color;
			} catch {
				return null;
			}
		}
	}
}

