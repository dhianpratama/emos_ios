﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace ios
{
	public class TransparentBackgroundHandler
	{
		public static UIView CreateBackground ()
		{
			var result = new UIView (new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
			result.Alpha = 0.5f;
			result.AutosizesSubviews = false;
			return result;
		}
		public static void SetViewTransparency(UIView View)
		{
			View.Frame = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);
			View.AutosizesSubviews = false;
			View.Opaque = false;
			View.BackgroundColor = UIColor.Clear;
		}
	}
}

