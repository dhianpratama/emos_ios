﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public static class StringHandler
	{
		public static SizeF MeasureTextHeight(string text, double width,
			double fontSize, string fontName = null)
		{
			var nsText = new NSString (text);
			var boundSize = new SizeF ((float)width, float.MaxValue);
			var options = NSStringDrawingOptions.UsesFontLeading |
				NSStringDrawingOptions.UsesLineFragmentOrigin;

			if (fontName == null) {
				fontName = "HelveticaNeue";
			}
			var attributes = new UIStringAttributes {
				Font = UIFont.FromName (fontName, (float)fontSize)
			};

			var sizeF = nsText.GetBoundingRect (boundSize, options, attributes, null).Size;

			return sizeF;
		}
	}
}

