﻿using System;
using emos_ios;
using MonoTouch.UIKit;
using emos_ios.tools;

namespace ios
{
	public interface IBaseContext
	{
		ServerConfig ServerConfig { get; set; }
		LanguageHandler LanguageHandler { get; set; }
		HttpSender HttpSender { get; set; }
		IColorHandler ColorHandler { get; set; }
		UINavigationController Nav { get; }
		DateTime OperationDate { get; set; }

		event EventHandler OnConnectionProblem;

		BaseUser GetBaseUser ();
		void StartRequest (object sender, RequestLoadingEventArgs e);
		void HideLoadingOverlay (object sender, EventArgs e);
		void ShowAlert (object sender, MessageEventArgs e);
		void PushDVC (IBaseDVC dvc);
		void PaintNavigationBar ();

		void SignOut(bool isAdmin = false);
	}

	public interface IBaseDVC
	{
		UIView GetDvcView();
	}
}

