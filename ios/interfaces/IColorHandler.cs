﻿using System;
using MonoTouch.UIKit;

namespace ios
{
	public interface IColorHandler
	{
		UIColor MainThemeColor { get; }
		UIColor ButtonBackground { get; }
		UIColor MealOrderStepButtonBackground { get; }
		UIColor ViewCellBackground { get; }
		UIColor MealOrderBackground { get; }
		void UpdateTheme (int mainThemeColor, int buttonBackground, int mealOrderStepButtonBackground);
		void PaintNavigationBar (UINavigationBar bar);
	}
}

