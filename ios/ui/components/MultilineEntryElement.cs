﻿using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using System.Drawing;


namespace emos_ios
{
	public class MultilineEntryElement : Element, IElementSizing  {
		public string Value { get; set; }
		public string _placeHolderText { get; set; }
		public int Index { get; set; }
		static NSString ekey = new NSString ("MultilineEntryElement");
		UITextView entry;
		static UIFont font = UIFont.BoldSystemFontOfSize (17f);
		private ICallback<int> _callback;
		private bool _useDeleteButton, _readOnly;
		public float Height { get; set; }

		/// <summary>
		/// Constructs an EntryElement with the given caption, placeholder and initial value.
		/// </summary>
		/// <param name="caption">
		/// The caption to use
		/// </param>
		/// <param name="placeholder">
		/// Placeholder to display.
		/// </param>
		/// <param name="value">
		/// Initial value.
		/// </param>
		public MultilineEntryElement (string caption, string placeholder, string value, 
			int index, ICallback<int> callback, bool useDeleteButton=false, bool readOnly=false, float height = 30f) : base (caption)
		{
			Value = value;
			Index = index;
			Height = height;
			_callback = callback;
			_placeHolderText = placeholder;
			_useDeleteButton = useDeleteButton;
			_readOnly = readOnly;
		}

		/// <summary>
		/// Constructs  an EntryElement for password entry with the given caption, placeholder and initial value.
		/// </summary>
		/// <param name="caption">
		/// The caption to use
		/// </param>
		/// <param name="placeholder">
		/// Placeholder to display.
		/// </param>
		/// <param name="value">
		/// Initial value.
		/// </param>
		/// <param name="isPassword">
		/// True if this should be used to enter a password.
		/// </param>

		public override string Summary ()
		{
			return Value;
		}

		// 
		// Computes the X position for the entry by aligning all the entries in the Section
		//
		SizeF ComputeEntryPosition (UITableView tv, UITableViewCell cell)
		{
			Section s = Parent as Section;
			if (s.EntryAlignment.Width != 0)
				return s.EntryAlignment;

			SizeF max = new SizeF (-1, -1);
			foreach (var e in s.Elements){
				var ee = e as MultilineEntryElement;
				if (ee == null)
					continue;

				var size = tv.StringSize (ee.Caption, font);
				if (size.Width > max.Width)
					max = size;				
			}
			s.EntryAlignment = new SizeF (25 + Math.Min (max.Width, 160), max.Height);
			return s.EntryAlignment;
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = tv.DequeueReusableCell (ekey);
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, ekey);
				cell.Frame = new RectangleF (cell.Frame.X, cell.Frame.Y, 768, cell.Frame.Height);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			} else {
				RemoveTag (cell, 1);
				foreach (var subview in cell.ContentView.Subviews) {
					subview.RemoveFromSuperview ();
				}
			}


//			var cell = new UITableViewCell (UITableViewCellStyle.Default, ekey);
//			cell.Frame = new RectangleF (cell.Frame.X, cell.Frame.Y, 768, cell.Frame.Height);
//			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			if (entry == null){
				SizeF size = ComputeEntryPosition (tv, cell);
				/*entry = new UITextField (new RectangleF (size.Width, (cell.ContentView.Bounds.Height-size.Height)/2-1, 320-size.Width, size.Height)){
					Tag = 1,
					Placeholder = placeholder,
					SecureTextEntry = isPassword
				};*/
				entry = new UITextView(new RectangleF(size.Width+20,(cell.ContentView.Bounds.Height-size.Height)/2-5, (768-size.Width)-100, Height));
				entry.Text = Value ?? "";
				entry.Font = UIFont.FromName("HelveticaNeue", 17f);
//				entry.AutoresizingMask = UIViewAutoresizing.FlexibleWidth |
//					UIViewAutoresizing.FlexibleLeftMargin;

				entry.Ended += delegate {
					Value = entry!=null ? entry.Text : Value;
				};
				entry.ReturnKeyType = UIReturnKeyType.Done;
				entry.Changed += delegate(object sender, EventArgs e) {
					if(entry!=null){
						if(entry.Text.Length!=0){
							int i = entry.Text.IndexOf("\n", entry.Text.Length -1);
							if (i > -1)
							{
								entry.Text = entry.Text.Substring(0,entry.Text.Length -1); 
								entry.ResignFirstResponder();	
							}
						}
						Value = entry.Text;
					}
				};

				if (_readOnly)
					entry.UserInteractionEnabled = false;
				else
					entry.UserInteractionEnabled = true;
			}

			cell.TextLabel.Text = Caption;

			//var gr = new UILongPressGestureRecognizer (tv, new MonoTouch.ObjCRuntime.Selector ("ButtonLongPressed"));
//			var gr = new UILongPressGestureRecognizer (ButtonLongPressed);
//			cell.AddGestureRecognizer (gr);

			var deleteButton = new UIButton (UIButtonType.RoundedRect);
			deleteButton.Frame = new RectangleF (768 - 50, 10, 50, 50);
			deleteButton.TintColor = UIColor.Red;
			deleteButton.SetTitle ("X", UIControlState.Normal);
			deleteButton.TouchUpInside += delegate {
				_callback.ItemSelected(Index,Index);
			};

			cell.ContentView.AddSubview (entry);

			if(_useDeleteButton)
				cell.ContentView.AddSubview (deleteButton);

			return cell;
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing){
				if(entry!=null)
					entry.Dispose ();

				entry = null;
			}
		}

		public float GetHeight (UITableView tableView, NSIndexPath indexPath)
		{
			return Height;
		}

		//[Export("ButtonLongPressed")]
		public void ButtonLongPressed()
		{
			_callback.ItemSelected (Index, Index);
		}

		public void ChangeCaption(string caption)
		{

		}
	}
}

