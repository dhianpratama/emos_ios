﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace emos_ios
{
	public class CheckboxDVC : CheckboxElement
	{
		private Action _action;
		public CheckboxDVC (string label) : base(label)
		{

		}

		public void OnTapped(Action action)
		{
			_action = action;
			_action();
		}
	}

}

