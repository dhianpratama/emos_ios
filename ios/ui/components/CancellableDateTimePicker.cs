﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using System.Collections.Generic;
using System.Linq;
using ios;

namespace emos_ios
{
	public partial class CancellableDateTimePicker : UIViewController
	{
		public event EventHandler OnCancel;
		public event EventHandler<BaseEventArgs<DateTime>> OnDone;
		private IBaseContext _appContext;
		private UIBarButtonItem _doneButton;
		private UIBarButtonItem _backButton;
		private UIDatePickerMode _mode;
		public UIDatePickerMode DatePickerMode {
			get { return _mode; }
			set {
				_mode = value;
				if (DatePicker != null)
					DatePicker.Mode = value;
			}
		}
		private RectangleF _defaultPickerFrame;
		public bool LimitToSevenDays { get; set; }

		public CancellableDateTimePicker (IBaseContext appContext, string title) : base ("CancellableDateTimePicker", null)
		{
			_appContext = appContext;
			Title = title;
			_defaultPickerFrame = new RectangleF (184, 650, 400, 264);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			SetNavigationBar ();
			DatePicker.Mode = _mode;

			if (LimitToSevenDays) {
				DatePicker.MinimumDate = DateTime.Today;
				DatePicker.MaximumDate = DateTime.Today.AddDays (7);
			}
		}
		private void SetNavigationBar()
		{
			NavigationBar.TopItem.Title = Title;
			NavigationBar.TopItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);
			NavigationBar.TopItem.SetLeftBarButtonItems (LeftButtons ().ToArray (), true);
			NavigationBar.SetFrame (width: BackgroundView.Frame.Width);
			_appContext.ColorHandler.PaintNavigationBar (NavigationBar);
		}
		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var doneText = _appContext.LanguageHandler.GetLocalizedString ("Done");
			_doneButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Done, (sender, args) => {
				var date = DateTimeHelper.FromNSDate(DatePicker.Date);
				OnDone.SafeInvoke (this, new BaseEventArgs<DateTime>().Initiate(date));
			});
			var buttons = new List<UIBarButtonItem> {
				_doneButton
			};
			return buttons;
		}
		public IEnumerable<UIBarButtonItem> LeftButtons ()
		{
			var backText = _appContext.LanguageHandler.GetLocalizedString ("Back");
			_backButton = new UIBarButtonItem (backText, UIBarButtonItemStyle.Done, (sender, args) => {
				OnCancel.SafeInvoke (this);
			});
			var buttons = new List<UIBarButtonItem> {
				_backButton
			};
			return buttons;
		}
		public RectangleF PickerFrame {
			get {
				return BackgroundView == null ? _defaultPickerFrame : BackgroundView.Frame;
			}
			set {
				if (BackgroundView == null)
					return;
				BackgroundView.SetFrame (value.X, value.Y, value.Width, value.Height);
				DatePicker.SetFrame (width: value.Width);
				NavigationBar.SetFrame (width: value.Width);
			}
		}
		public void setDate(DateTime date) {
			DatePicker.SetDate(date, false);
		}
	}
}

