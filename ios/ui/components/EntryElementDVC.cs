﻿using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace emos_ios
{
	public class EntryElementDVC : EntryElement
	{
		private bool isReadOnly;

		public EntryElementDVC (string caption, string placeholder, string value, bool isReadOnly=false) 
			: base(caption, placeholder, value)
		{
			this.isReadOnly = isReadOnly;
		}

		protected override UITextField CreateTextField(System.Drawing.RectangleF frame)
		{
			var field = base.CreateTextField(frame);
			field.Enabled = !isReadOnly;
			return field;
		}
	}
}

