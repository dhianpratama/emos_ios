// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("TimePicker")]
	partial class TimePicker
	{
		[Outlet]
		MonoTouch.UIKit.UIDatePicker TimePickerElement { get; set; }

		[Action ("CloseButton_TouchUpInside:")]
		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (TimePickerElement != null) {
				TimePickerElement.Dispose ();
				TimePickerElement = null;
			}
		}
	}
}
