﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public class RoundedEntryElement : UIView
	{
		private const float Padding = 20f;
		public PaddingLabel Caption { get; set; }
		public UITextField TextField { get; set; }
		private UIColor _captionBackgroundColor;
		private UIColor _contentBackgroundColor;
		private UIFont _textViewFont;
		public UIFont TextViewFont {
			get { return _textViewFont; }
			set {
				_textViewFont = value;
				TextField.Font = value;
			}
		}
		public float CaptionTotalWidth { get { return (2 * Padding) + Caption.Frame.Width; } private set { } }
		public float TotalWidth { get { return Frame.Width; } private set { } }
		private UIFont _captionFont;
		private UIFont _contentFont;

		public RoundedEntryElement (RectangleF frame, float captionWidth, string caption, bool editable, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone)
		{
			_captionFont = UIFont.FromName("Helvetica", 20f);
			_contentFont = UIFont.FromName("Helvetica", 20f);
			_captionBackgroundColor = SharedColors.ViewCellBackground;
			_contentBackgroundColor = UIColor.White;
			Frame = frame;
			CreateCaption (frame, captionWidth, caption, _captionBackgroundColor, _captionFont);
			CreateContent (frame, captionWidth, editable, _contentFont);
			UpdateMask (captionRectCorner, textFieldRectCorner);
			AddSubview (Caption);
			AddSubview (TextField);
			this.BackgroundColor = UIColor.Clear;
		}
		public void SetCaptionFont(float size, bool bold)
		{
			var fontName = bold ? "Helvetica-Bold" : "Helvetica";
			Caption.Font = UIFont.FromName (fontName, size);
		}
		private void UpdateMask(UIRectCorner captionRectCorner, UIRectCorner textFieldRectCorner)
		{
			if (captionRectCorner != RoundedRectangle.RoundedNone)
				Caption.UpdateMask (captionRectCorner);
			if (textFieldRectCorner != RoundedRectangle.RoundedNone)
				TextField.UpdateMask (textFieldRectCorner);
		}
		private void CreateCaption (RectangleF frame, float captionWidth, string caption, UIColor captionBackgroundColor, UIFont font)
		{
			BackgroundColor = captionBackgroundColor;
			Caption = new PaddingLabel () {
				EdgeInsets = new UIEdgeInsets(0, Padding, 0, Padding),
				BackgroundColor = captionBackgroundColor,
				Frame = new RectangleF (0, 0, captionWidth, frame.Height),
				Text = caption,
				Font = font
			};
		}
		private void CreateContent (RectangleF frame, float captionWidth, bool editable, UIFont font)
		{
			var textFieldLeft = Caption.Frame.Left + captionWidth;
			TextField = new PaddingTextField {
				EdgeInsets = new UIEdgeInsets (0, Padding, 0, Padding),
				BackgroundColor = _contentBackgroundColor,
				Frame = new RectangleF (textFieldLeft, 0, frame.Width - textFieldLeft, frame.Height),
				Font = font,
			};
			Enabled = editable;
		}
		private void SetClearButtonMode (bool editable)
		{
			if (editable)
				TextField.ClearButtonMode = UITextFieldViewMode.Always;
			else
				TextField.ClearButtonMode = UITextFieldViewMode.Never;
		}
		private bool _enabled;
		public bool Enabled {
			get { return _enabled; }
			set {
				_enabled = value;
				TextField.Enabled = value;
				SetClearButtonMode (value);
			}
		}
	}
}

