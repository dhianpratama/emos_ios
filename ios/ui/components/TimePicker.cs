﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public enum TimePickerSelection
	{
		Meals, Beverages
	}

	public partial class TimePicker : UIViewController
	{
		public DateTime DateValue { get; set; }
		public TimePickerSelection Selection { get; set; }
		private ITimePickerCallback _callback;
		private int? _index;
		private DateTime? _model;

		public TimePicker (ITimePickerCallback callback, DateTime? model, int? index) : base ("TimePicker", null)
		{
			this.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			this.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;

			_callback = callback;
			_model = model!=null ? model : DateTime.Now ;
			_model = DateTime.SpecifyKind ((DateTime)_model, DateTimeKind.Utc);
			_index = index;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			TimePickerElement.Mode = UIDatePickerMode.Time;
			TimePickerElement.TimeZone = new NSTimeZone ("UTC");
//			TimePicker.MinimumDate = DateTime.Today.AddDays (-3000);
//			TimePicker.MaximumDate = DateTime.Today.AddDays (3000);
			TimePickerElement.SetDate ((NSDate)_model, true);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 400, 300);
		}

		partial void CloseButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			DateValue = (DateTime)TimePickerElement.Date;
			_callback.DoneTimePicker(Selection, DateValue, _index);
			DismissViewController(true,null);
		}
	}
}

