﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using ios;

namespace emos_ios
{
	public class RoundedDatePickerButton : UIView
	{
		private const float Padding = 20f;
		public PaddingLabel Caption { get; set; }
		public UIDatePickerButton DatePickerButton { get; set; }
		private UIFont _textViewFont;
		private UIColor _captionBackgroundColor;
		private UIColor _contentBackgroundColor;
		private UIColor _contentColor;
		public UIFont TextViewFont {
			get { return _textViewFont; }
			set {
				_textViewFont = value;
				DatePickerButton.Font = value;
			}
		}
		public DateTime SelectedDate {
			get { return DatePickerButton.SelectedDate; }
			set { DatePickerButton.SelectedDate = value; }
		}
		public UIDatePickerMode DatePickerMode {
			get { return DatePickerButton.DatePicker.DatePickerMode; }
			set { DatePickerButton.DatePicker.DatePickerMode = value; }
		}
		public float CaptionTotalWidth { get { return (2 * Padding) + Caption.Frame.Width; } private set { } }
		public float TotalWidth { get { return Frame.Width; } private set { } }
		private UIFont _captionFont;
		private UIFont _textFieldFont;
		private IBaseContext _appContext;
		private UIView _parentView;
		public bool LimitToSevenDays { get; set; }

		public RoundedDatePickerButton (IBaseContext appContext, UIView parentView, RectangleF frame, float captionWidth, string caption, bool editable, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone)
		{
			_appContext = appContext;
			_parentView = parentView;
			_captionFont = UIFont.FromName("Helvetica", 20f);
			_textFieldFont = UIFont.FromName("Helvetica", 20f);
			_captionBackgroundColor = SharedColors.ViewCellBackground;
			_contentBackgroundColor = UIColor.White;
			_contentColor = UIColor.Black;
			Frame = frame;
			CreateCaption (frame, captionWidth, caption, _captionBackgroundColor, _captionFont);
			CreateContent (frame, captionWidth, editable, _textFieldFont);
			UpdateMask (captionRectCorner, textFieldRectCorner);
			AddSubview (Caption);
			AddSubview (DatePickerButton);
			this.BackgroundColor = UIColor.Clear;
		}
		public void SetCaptionFont(float size, bool bold)
		{
			var fontName = bold ? "Helvetica-Bold" : "Helvetica";
			Caption.Font = UIFont.FromName (fontName, size);
		}
		private void UpdateMask(UIRectCorner captionRectCorner, UIRectCorner textFieldRectCorner)
		{
			if (captionRectCorner != RoundedRectangle.RoundedNone)
				Caption.UpdateMask (captionRectCorner);
			if (textFieldRectCorner != RoundedRectangle.RoundedNone)
				DatePickerButton.UpdateMask (textFieldRectCorner);
		}
		private void CreateCaption (RectangleF frame, float captionWidth, string caption, UIColor captionBackgroundColor, UIFont font)
		{
			BackgroundColor = captionBackgroundColor;
			Caption = new PaddingLabel () {
				EdgeInsets = new UIEdgeInsets(0, Padding, 0, Padding),
				BackgroundColor = captionBackgroundColor,
				Frame = new RectangleF (0, 0, captionWidth, frame.Height),
				Text = caption,
				Font = font
			};
		}
		private void CreateContent (RectangleF frame, float captionWidth, bool editable, UIFont font)
		{
			var left = Caption.Frame.Left + captionWidth;
			var contentFrame = new RectangleF (left, 0, frame.Width - left, frame.Height);
			DatePickerButton = new UIDatePickerButton (_appContext, _parentView, Caption.Text, contentFrame, LimitToSevenDays) {
				TitleEdgeInsets = new UIEdgeInsets (0, Padding, 0, Padding),
				BackgroundColor = _contentBackgroundColor,
				Font = font,
				HorizontalAlignment = UIControlContentHorizontalAlignment.Left
			};
			DatePickerButton.SetTitleColor (_contentColor, UIControlState.Normal);
			Enabled = editable;
		}

		private bool _enabled;
		public bool Enabled {
			get { return _enabled; }
			set {
				_enabled = value;
				DatePickerButton.Enabled = value;
				if (value) {
					BackgroundColor = UIColor.White;
					DatePickerButton.BackgroundColor = _contentBackgroundColor;
					Caption.BackgroundColor = _captionBackgroundColor;
					this.BackgroundColor = _captionBackgroundColor;
				} else {
					DatePickerButton.BackgroundColor = SharedColors.DarkViewCellBackground;
					Caption.BackgroundColor = SharedColors.DarkViewCellBackground;
					this.BackgroundColor = SharedColors.DarkViewCellBackground;
				}
			}
		}
	}
}
