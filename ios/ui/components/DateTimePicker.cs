﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ios;

namespace emos_ios
{
	public partial class DateTimePicker : UIViewController
	{
		private IDatePickerCallback _callback;
		private DateTime? _date;
		private DateType _dateType;
		private IBaseContext _appContext;
		private UIDatePickerMode _datePickerMode;

		public DateTimePicker (IBaseContext appContext, DateTime? date, IDatePickerCallback callback, DateType dateType, UIDatePickerMode pickerMode = UIDatePickerMode.DateAndTime) : base ("DateTimePicker", null)
		{
			_appContext = appContext;
			_date = date;
			_callback = callback;
			_dateType = dateType;
			_datePickerMode = pickerMode;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			DatePicker.Mode = _datePickerMode;
			//NSLocale cultureInfo = new NSLocale(_appContext.LanguageHandler.LanguageCode);

			DatePicker.TimeZone = new NSTimeZone("Asia/Singapore");
			DatePicker.MinimumDate = DateTime.Today.AddDays (-3000);
			DatePicker.MaximumDate = DateTime.Today.AddDays (3000);
			//DatePicker.Locale = cultureInfo;
			if (_date == null)
				_date = DateTime.Now ;
			DatePicker.SetDate (_date.Value.ToLocalTime(), true);
			doneButton.SetTitle (_appContext.LanguageHandler.GetLocalizedString ("Done"), UIControlState.Normal);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			this.View.Superview.Bounds = new RectangleF (0, 0, 400, 300);
		}

		partial void DoneButton_TouchUpInside (MonoTouch.Foundation.NSObject sender)
		{
			var date = (DateTime?) DatePicker.Date;
			DismissViewController(true,null);
			_callback.DoneDatePicker(date, _dateType);
		}
	}
}

