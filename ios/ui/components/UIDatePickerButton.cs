﻿using System;
using MonoTouch.UIKit;
using ios;
using System.Drawing;

namespace emos_ios
{
	public class UIDatePickerButton : UIButton
	{
		public CancellableDateTimePicker DatePicker { get; set; }
		private UIView _parentView;
		private DateTime _selectedDate;
		public DateTime SelectedDate {
			get { return DatePickerMode == UIDatePickerMode.Date ? _selectedDate.Date : _selectedDate; }
			set {
				_selectedDate = value;
				ShowDate (value);
			}
		}
		public UIDatePickerMode DatePickerMode {
			get { return DatePicker.DatePickerMode; }
			set { DatePicker.DatePickerMode = value; }
		}

		public UIDatePickerButton (IBaseContext appContext, UIView parentView, string title, RectangleF frame, bool limitToSevenDays=false) : base(UIButtonType.Custom)
		{
			Frame = frame;
			DatePicker = new CancellableDateTimePicker (appContext, title);
			DatePicker.LimitToSevenDays = limitToSevenDays;
			DatePicker.OnDone += HandleOnPickerDone;
			DatePicker.OnCancel += HandleOnPickerCancel;
			_parentView = parentView;
			TouchUpInside += HandleTouchUpInside;
		}
		private void HandleTouchUpInside (object sender, EventArgs e)
		{
			UIApplication.SharedApplication.KeyWindow.AddSubview (DatePicker.View);
			UIApplication.SharedApplication.KeyWindow.BringSubviewToFront (DatePicker.View);
			SetDatePickerLayout ();
		}
		private void SetDatePickerLayout ()
		{
			var topLeft = ConvertPointToView(new PointF(0,0), UIApplication.SharedApplication.KeyWindow);
			var top = topLeft.Y + Frame.Height;
			var maxDropDownHeight = UIScreen.MainScreen.Bounds.Bottom - top - 3;
			var fullHeight = DatePicker.PickerFrame.Height;
			if (fullHeight > maxDropDownHeight)
				top = topLeft.Y - fullHeight;
			var dropDownHeight = fullHeight > maxDropDownHeight ? maxDropDownHeight : fullHeight;
			DatePicker.PickerFrame = new RectangleF (topLeft.X, top, Frame.Width, dropDownHeight);
			DatePicker.setDate (_selectedDate);
		}
		private void HandleOnPickerDone (object sender, BaseEventArgs<DateTime> e)
		{
			HandleOnPickerCancel (sender, e);
			SelectedDate = e.Value;
			ShowDate (e.Value);
		}
		private void ShowDate (DateTime date)
		{
			if (DatePickerMode == UIDatePickerMode.Date) {
				SetTitle (date.ToDisplayDateFormatInString (), UIControlState.Normal);
			} else
				SetTitle (date.ToDisplayDateString (), UIControlState.Normal);
		}
		private void HandleOnPickerCancel (object sender, EventArgs e)
		{
			DatePicker.View.RemoveFromSuperview ();
		}

	}
}
