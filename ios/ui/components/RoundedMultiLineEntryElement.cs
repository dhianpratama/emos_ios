﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public class RoundedMultiLineEntryElement : UIView
	{
		private const float Padding = 20f;
		private UITextView MultiLineCaption { get; set; }
		private PaddingLabel Caption { get; set; }
		public LimitedTextView TextView { get; set; }
		private UIColor _captionBackgroundColor;
		private UIColor _contentBackgroundColor;
		private UIFont _textViewFont;
		public UIFont TextViewFont {
			get { return _textViewFont; }
			set {
				_textViewFont = value;
				TextView.Font = value;
			}
		}
		public int Row { get; private set; }
//		public float CaptionTotalWidth { get { return (2 * Padding) + MultiLineCaption.Frame.Width; } private set { } }
		public float TotalWidth { get { return Frame.Width; } private set { } }
		private UIFont _captionFont;
		private UIFont _contentFont;
		private int _maxCharacter;
		public string Value { get { return TextView.Text; } set { TextView.Text = value; } }
		private bool _multiLineCaption;

		public RoundedMultiLineEntryElement (RectangleF frame, float captionWidth, string caption, bool editable, int maxCharacter = 0, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone, bool fitString = false, string content = "", bool multiLineCaption = true)
		{
			_maxCharacter = maxCharacter;
			_multiLineCaption = multiLineCaption;
			_captionFont = UIFont.FromName("Helvetica", 20f);
			_contentFont = UIFont.FromName("Helvetica", 20f);
			_captionBackgroundColor = SharedColors.ViewCellBackground;
			_contentBackgroundColor = UIColor.White;
			Frame = GenerateFrame(frame, captionWidth, content, fitString);
			CreateCaption (Frame, captionWidth, caption, _captionBackgroundColor, _captionFont, multiLineCaption);
			CreateContent (Frame, captionWidth, editable, _contentFont);
			UpdateMask (captionRectCorner, textFieldRectCorner);
			if (multiLineCaption)
				AddSubview(MultiLineCaption);
			else
				AddSubview (Caption);
			AddSubview (TextView);
			this.BackgroundColor = UIColor.Clear;
		}
		private RectangleF GenerateFrame (RectangleF frame, float captionWidth, string content, bool fitString)
		{
			if (!fitString)
				return frame;
			var size = StringHandler.MeasureTextHeight (content, frame.Width - captionWidth, 20, "Helvetica");
			Row = Convert.ToInt32 (Math.Ceiling (size.Height / frame.Height));
			return new RectangleF (frame.X, frame.Y, frame.Width, frame.Height * Row);
		}
		public void SetCaptionFont(float size, bool bold)
		{
			var fontName = bold ? "Helvetica-Bold" : "Helvetica";
			if (_multiLineCaption)
				MultiLineCaption.Font = UIFont.FromName (fontName, size);
			else
				Caption.Font = UIFont.FromName (fontName, size);
		}
		private void UpdateMask(UIRectCorner captionRectCorner, UIRectCorner textFieldRectCorner)
		{
			if (captionRectCorner != RoundedRectangle.RoundedNone)
				MultiLineCaption.UpdateMask (captionRectCorner);
			if (textFieldRectCorner != RoundedRectangle.RoundedNone)
				TextView.UpdateMask (textFieldRectCorner);
		}
		private void CreateCaption (RectangleF frame, float captionWidth, string caption, UIColor captionBackgroundColor, UIFont font, bool multiLineCaption)
		{
			BackgroundColor = captionBackgroundColor;
			if (multiLineCaption) {
				MultiLineCaption = new UITextView () {
					BackgroundColor = captionBackgroundColor,
					Frame = new RectangleF (0, 0, captionWidth, frame.Height),
					Text = caption,
					Font = font,
					Editable = false
				};
				MultiLineCaption.TextContainerInset = new UIEdgeInsets (0, Padding - 5f, 0, Padding - 5f);
			} else {
				Caption = new PaddingLabel () {
					EdgeInsets = new UIEdgeInsets(0, Padding, 0, Padding),
					BackgroundColor = captionBackgroundColor,
					Frame = new RectangleF (0, 0, captionWidth, frame.Height),
					Text = caption,
					Font = font,
					Enabled = false
				};
			}
		}
		private void CreateContent (RectangleF frame, float captionWidth, bool editable, UIFont font)
		{
			var left = _multiLineCaption ? MultiLineCaption.Frame.Left + captionWidth : Caption.Frame.Width;
			var contentFrame = new RectangleF (left, 0, frame.Width - left, frame.Height);
			TextView = new LimitedTextView(contentFrame, _maxCharacter) {
				BackgroundColor = _contentBackgroundColor,
				Font = font,
			};
			TextView.TextContainerInset = new UIEdgeInsets (0, Padding - 5f, 0, Padding - 5f);
			if (editable) {
//				TextView.ClearButtonMode = UITextFieldViewMode.Always;
			} else {
				TextView.UserInteractionEnabled = false;
//				TextView.ClearButtonMode = UITextFieldViewMode.Never;
			}
		}

		private bool _enabled;
		public bool Enabled {
			get { return _enabled; }
			set {
				_enabled = value;
				TextView.UserInteractionEnabled = value;
				if (value) {
					BackgroundColor = UIColor.White;
					TextView.BackgroundColor = _contentBackgroundColor;
					MultiLineCaption.BackgroundColor = _captionBackgroundColor;
					this.BackgroundColor = _captionBackgroundColor;
				} else {
					TextView.BackgroundColor = SharedColors.DarkViewCellBackground;
					MultiLineCaption.BackgroundColor = SharedColors.DarkViewCellBackground;
					this.BackgroundColor = SharedColors.DarkViewCellBackground;
				}
			}
		}
	}
}

