﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using ios;
using System.Drawing;
using System.Linq;

namespace emos_ios
{
	public class DropDownHandler : ICallback<KeyValuePair<string, string>>
	{
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OnDone;
		public const float NavigationBarHeight = 50f;
		private IBaseContext _baseContext;
		private TableAndHeaderView<KeyValuePair<string, string>, KeyValuePairViewCell> _dropDownView;
		private BaseViewCellSetting _baseViewCellSetting;
		private KeyValuePairViewCellHandler _keyValuePairViewCellHandler;
		private string _dropDownTitle;
		private UIView _dropDownText;
		public ModalViewController _modalViewController;
		public List<KeyValuePair<string, string>> _items { get; set; }
		public bool ModalParent { get; set; }
		private float _rowHeight;
		private string _selectedKey;
		public string SelectedKey {
			get { return _selectedKey; }
			set {
				_selectedKey = value;
				SelectByKey (value);
			}
		}
		private KeyValuePair<string,string> _selected;
		public KeyValuePair<string,string> Selected {
			get { return _selected; }
			set {
				_selected = value;
				SelectByKey (value.Key);
			}
		}
		public DropDownHandler (IBaseContext baseContext, UIView dropDownText, string initialValue, string dropDownTitle, List<KeyValuePair<string, string>> items, float rowHeight = 44f)
		{
			_baseContext = baseContext;
			_dropDownTitle = dropDownTitle;
			_items = items;
			_rowHeight = rowHeight;
			_baseViewCellSetting = new BaseViewCellSetting {
				BackgroundColor = UIColor.White
			};
			_dropDownText = dropDownText;
			_keyValuePairViewCellHandler = new KeyValuePairViewCellHandler (this, _baseViewCellSetting);
			InitiatDropDownView (dropDownText);
			_modalViewController = new ModalViewController ();
			_modalViewController.View.AddSubview (_dropDownView);
			_modalViewController.ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen;
		}
		private void HandleOnSelectionDone (object sender, EventArgs e)
		{
			HideDropdown ();
			_selectedKey = _dropDownView.Selected.Key;
			_selected = _dropDownView.Selected;
			OnDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ().Initiate (_dropDownView.Selected));
		}
		private void HandleOnSelectionCancelled (object sender, EventArgs e)
		{
			HideDropdown ();
		}
		private void HideDropdown ()
		{
			_modalViewController.View.RemoveFromSuperview ();
		}
		public void ShowDropDown(string initialValue = "")
		{
			SetLayout ();
			UIApplication.SharedApplication.KeyWindow.AddSubview (_modalViewController.View);
			UIApplication.SharedApplication.KeyWindow.BringSubviewToFront (_modalViewController.View);
			_dropDownView.CheckmarkVisible = true;
			SelectByValue (initialValue);
			_dropDownView.ExitOnSelection = true;
		}
		private void InitiatDropDownView (UIView dropDownText)
		{
			_dropDownView = new TableAndHeaderView<KeyValuePair<string, string>, KeyValuePairViewCell> (_baseContext, _keyValuePairViewCellHandler, _dropDownTitle, false);
			_dropDownView.OnDone += HandleOnSelectionDone;
			_dropDownView.OnCancelled += HandleOnSelectionCancelled;
			_dropDownView.TableView.SetItems (_items);
			_dropDownView.TableView.RowHeight = _rowHeight;
			_dropDownView.Selected = new KeyValuePair<string, string> ("", "");
		}
		private void SetLayout ()
		{
			var dropDownTopLeft = _dropDownText.ConvertPointToView(new PointF(0,0), UIApplication.SharedApplication.KeyWindow);
			var top = dropDownTopLeft.Y + _dropDownText.Frame.Height;
			var maxDropDownHeight = UIScreen.MainScreen.Bounds.Bottom - top - 3;
			var dropDownFullHeight = NavigationBarHeight + (_rowHeight * _items.Count);
			var dropDownHeight = dropDownFullHeight > maxDropDownHeight ? maxDropDownHeight : dropDownFullHeight;
			_dropDownView.SetLayout (new RectangleF (dropDownTopLeft.X, top, _dropDownText.Frame.Width, dropDownHeight));
		}
		private PointF GetParentTopF (UIView view)
		{
			if (view.Superview != null) {
				var parentTopLeft = GetParentTopF (view.Superview);
				return new PointF (parentTopLeft.X + view.Frame.Left, parentTopLeft.Y + view.Frame.Top);
			}
			return new PointF (view.Frame.Left, view.Frame.Top);
		}
		private void SelectByValue (string value)
		{
			var selectedIndex = GetIndexByValue (value);
			if (selectedIndex > -1) {
				_dropDownView.TableView.SelectRow (selectedIndex);
				OnDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ().Initiate (_dropDownView.Selected));
			}
		}
		private int GetIndexByValue (string value)
		{
			for (int i = 0; i < _items.Count; i++) {
				if (_items [i].Value == value)
					return i;
			}
			return -1;
		}
		private void SelectByKey (string key)
		{
			_dropDownView.Selected = new KeyValuePair<string, string> ("", "");
			var selectedIndex = GetIndexByKey (key);
			if (selectedIndex > -1) {
				_dropDownView.TableView.SelectRow (selectedIndex);
				OnDone.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ().Initiate (_dropDownView.Selected));
			}
		}
		private int GetIndexByKey (string key)
		{
			for (int i = 0; i < _items.Count; i++) {
				if (_items [i].Key == key)
					return i;
			}
			return -1;
		}
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OnItemSelected;
		#region ICallback implementation
		public void ItemSelected (KeyValuePair<string, string> selected, int selectedIndex)
		{
			OnItemSelected.SafeInvoke (this, new BaseEventArgs<KeyValuePair<string, string>> ().Initiate (selected));
		}
		public IBaseContext GetBaseContext()
		{
			return _baseContext;
		}
		#endregion
	}
}

