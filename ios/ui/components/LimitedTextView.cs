﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace emos_ios
{
	public class LimitedTextView : UITextView
	{
		public int MaxCharacter { get; set; }

		private void Initialize (int maxCharacter = 255)
		{
			MaxCharacter = maxCharacter > 0 ? maxCharacter : 255;
			ShouldChangeText = ShouldLimit;
		}
		private static bool ShouldLimit (UITextView view, NSRange range, string text)
		{
			var textView = (LimitedTextView)view;
			var limit = textView.MaxCharacter;

			int newLength = (view.Text.Length - range.Length) + text.Length;
			if (newLength <= limit)
				return true;

			var emptySpace = Math.Max (0, limit - (view.Text.Length - range.Length));
			var beforeCaret = view.Text.Substring (0, range.Location) + text.Substring (0, emptySpace);
			var afterCaret = view.Text.Substring (range.Location + range.Length);

			view.Text = beforeCaret + afterCaret;
			view.SelectedRange = new NSRange (beforeCaret.Length, 0);

			return false;
		}

		public LimitedTextView (IntPtr handle) : base (handle) { Initialize (); }
		public LimitedTextView (RectangleF frame, int maxCharacter) : base (frame) { Initialize (maxCharacter); }
	}
}