﻿using System;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace ios
{
	public class FormattedDateElement : DateElement
	{
		public FormattedDateElement (string caption, DateTime date, string dateFormat = "dd-MMM-yyyy") : base(caption, date)
		{
			fmt = new NSDateFormatter {
				DateFormat = dateFormat,
			};
		}
	}
}

