﻿using System;
using MonoTouch.Dialog;
using MonoTouch.Foundation;

namespace ios
{
	public class FormattedDateTimeElement : DateTimeElement
	{
		public FormattedDateTimeElement (string caption, DateTime date, string dateFormat = "dd-MMM-yyyy") : base(caption, date)
		{
			fmt = new NSDateFormatter {
				DateFormat = dateFormat,
			};
		}
	}
}

