﻿using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace emos_ios
{
	public class ButtonDVC : StyledStringElement
	{
		public ButtonDVC (string caption, NSAction action) : base(caption, action)
		{
			Alignment = UITextAlignment.Center;
			BackgroundColor = SharedColors.ButtonDVCColor;
			TextColor = UIColor.White;
		}
	}
}

