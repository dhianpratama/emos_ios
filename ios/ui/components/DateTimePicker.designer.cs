// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("DateTimePicker")]
	partial class DateTimePicker
	{
		[Outlet]
		MonoTouch.UIKit.UIDatePicker DatePicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton doneButton { get; set; }

		[Action ("DoneButton_TouchUpInside:")]
		partial void DoneButton_TouchUpInside (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DatePicker != null) {
				DatePicker.Dispose ();
				DatePicker = null;
			}

			if (doneButton != null) {
				doneButton.Dispose ();
				doneButton = null;
			}
		}
	}
}
