﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using ios;
using System.Collections.Generic;

namespace emos_ios
{
	public class RoundedDropDown : UIView
	{
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OnDone;
		public event EventHandler OnListNotInitiated;
		private const float Padding = 20f;
		private IBaseContext _appContext;
		private UIView _parentView;
		public string identifier;
		public PaddingLabel Caption { get; set; }
		public DropDownButton DropDownButton { get; set; }
		private UIColor _captionBackgroundColor;
		private UIColor _contentBackgroundColor;
		private UIFont _textViewFont;
		public UIFont TextViewFont {
			get { return _textViewFont; }
			set {
				_textViewFont = value;
				DropDownButton.TextButton.Font = value;
			}
		}
		public float TotalWidth { get { return Frame.Width; } private set { } }
		private UIFont _captionFont;
		private UIFont _contentFont;
		public string SelectedKey {
			get { return DropDownButton.SelectedKey; }
			set { DropDownButton.SelectedKey = value; }
		}
		public KeyValuePair<string,string> Selected {
			get { return DropDownButton.Selected; }
			set { DropDownButton.Selected = value; }
		}
		public RoundedDropDown (IBaseContext appContext, UIView parentView, RectangleF frame, float captionWidth, string caption, List<KeyValuePair<string, string>> items, bool editable, UIRectCorner captionRectCorner = RoundedRectangle.RoundedNone, UIRectCorner textFieldRectCorner = RoundedRectangle.RoundedNone, bool hideArrow = false)
		{
			_appContext = appContext;
			_parentView = parentView;
			_captionFont = UIFont.FromName("Helvetica", 20f);
			_contentFont = UIFont.FromName("Helvetica", 20f);
			_captionBackgroundColor = SharedColors.ViewCellBackground;
			_contentBackgroundColor = UIColor.White;
			this.AutosizesSubviews = false;
			Frame = frame;
			CreateCaption (frame, captionWidth, caption, _captionBackgroundColor, _captionFont);
			CreateDropDown (frame, caption, captionWidth, editable, _contentFont, items, hideArrow);
			UpdateMask (captionRectCorner, textFieldRectCorner);
			AddSubview (Caption);
			AddSubview (DropDownButton);
			this.BackgroundColor = UIColor.Clear;
		}
		public void SetCaptionFont(float size, bool bold)
		{
			var fontName = bold ? "Helvetica-Bold" : "Helvetica";
			Caption.Font = UIFont.FromName (fontName, size);
		}
		private void UpdateMask(UIRectCorner captionRectCorner, UIRectCorner textFieldRectCorner)
		{
			if (captionRectCorner != RoundedRectangle.RoundedNone)
				Caption.UpdateMask (captionRectCorner);
			if (textFieldRectCorner != RoundedRectangle.RoundedNone)
				DropDownButton.UpdateMask (textFieldRectCorner);
		}
		private void CreateCaption (RectangleF frame, float captionWidth, string caption, UIColor captionBackgroundColor, UIFont font)
		{
			BackgroundColor = captionBackgroundColor;
			Caption = new PaddingLabel () {
				EdgeInsets = new UIEdgeInsets(0, Padding, 0, Padding),
				BackgroundColor = captionBackgroundColor,
				Frame = new RectangleF (0, 0, captionWidth, frame.Height),
				Text = caption,
				Font = font
			};
		}
		private void CreateDropDown (RectangleF frame, string title, float captionWidth, bool editable, UIFont font, List<KeyValuePair<string,string>> items, bool hideArrow = false)
		{
			DropDownButton = new DropDownButton ("", false, false, frame.Height, hideArrow);
			var left = Caption.Frame.Left + captionWidth;
			DropDownButton.SetFrame (left, 0, frame.Width - left);
			DropDownButton.BackgroundColor = _contentBackgroundColor;
			DropDownButton.TextButton.Font = font;
			DropDownButton.UserInteractionEnabled = editable;
			DropDownButton.OnDone += (o, e) => OnDone.SafeInvoke (o, e);
			DropDownButton.OnListNotInititated += (o, e) => OnListNotInitiated.SafeInvoke (this);
			DropDownButton.Load (_appContext, _parentView, title, items);
		}
		public void Load(List<KeyValuePair<string,string>> items)
		{
			DropDownButton.Load (_appContext, _parentView, Caption.Text, items);
		}
		private bool _enabled;
		public bool Enabled {
			get { return _enabled; }
			set {
				_enabled = value;
				DropDownButton.UserInteractionEnabled = value;
				if (value) {
					BackgroundColor = UIColor.White;
					DropDownButton.TextButton.BackgroundColor = _contentBackgroundColor;
					Caption.BackgroundColor = _captionBackgroundColor;
					this.BackgroundColor = _captionBackgroundColor;
				} else {
					DropDownButton.TextButton.BackgroundColor = SharedColors.DarkViewCellBackground;
					Caption.BackgroundColor = SharedColors.DarkViewCellBackground;
					this.BackgroundColor = SharedColors.DarkViewCellBackground;
				}
			}
		}
		public void SetElementFrameBelowTo (UIView aboveView, float? x = null, float? width = null, float? height = null, float distanceToAbove = 20)
		{
			this.SetFrameBelowTo (aboveView, x, width, height, distanceToAbove);
		}
		public void SetElementFrame (float? x = null, float? y = null, float? width = null, float? height = null)
		{
			this.SetFrame (x, y, width, height);
		}
		public void ShowDropDown ()
		{
			DropDownButton.ShowDropDown ();
		}
	}
}

