﻿using System;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;
using System.Drawing;

namespace emos_ios
{
	public static class RoundedRectangle
	{
		private const float fCornerRadius = 12.0f;
		public static UIRectCorner RoundedTopCorners = UIRectCorner.TopLeft | UIRectCorner.TopRight;
		public static UIRectCorner RoundedBottomCorners = UIRectCorner.BottomLeft | UIRectCorner.BottomRight;
		public static UIRectCorner RoundedLeftCorners = UIRectCorner.TopLeft | UIRectCorner.BottomLeft;
		public static UIRectCorner RoundedRightCorners = UIRectCorner.TopRight | UIRectCorner.BottomRight;
		public const UIRectCorner RoundedNone = 0;

		public static void UpdateMask(this UIView view, UIRectCorner rectCorner)
		{
			// Add a layer that holds the rounded corners.
			UIBezierPath oMaskPath = UIBezierPath.FromRoundedRect (view.Bounds, rectCorner, new SizeF (fCornerRadius, fCornerRadius));

			CAShapeLayer oMaskLayer = new CAShapeLayer ();
			oMaskLayer.Frame = view.Bounds;
			oMaskLayer.Path = oMaskPath.CGPath;

			// Set the newly created shape layer as the mask for the image view's layer
			view.Layer.Mask = oMaskLayer;
		}

	}
}

