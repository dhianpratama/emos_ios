﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace emos_ios
{
	public class PaddingLabel : UILabel
	{
		public UIEdgeInsets EdgeInsets { get; set; }

		public PaddingLabel()
		{
			EdgeInsets = UIEdgeInsets.Zero;
		}
		public PaddingLabel(IntPtr intPtr) : base(intPtr)
		{
			EdgeInsets = UIEdgeInsets.Zero;
		}

		public override void DrawText(RectangleF rect)
		{
			base.DrawText(PaddingTextField.InsetRect(rect, EdgeInsets));
		}
	}
}

