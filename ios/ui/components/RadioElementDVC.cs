﻿using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace emos_ios
{
	public class MyRadioElement : RadioElement {
		// Pass the caption through to the base constructor.
		public MyRadioElement (string pCaption) : base(pCaption) {
		}

		// Fire an event when the selection changes.
		// I use this to flip a "dirty flag" further up stream.
		public override void Selected (
			DialogViewController pDialogViewController, 
			UITableView pTableView, NSIndexPath pIndexPath) {
			// Checking to see if the current cell is already "checked"
			// prevent the event from firing if the item is already selected.  
			if (GetActiveCell().Accessory.ToString().Equals(
				"Checkmark",StringComparison.InvariantCultureIgnoreCase)) {
				return;
			}



			base.Selected (pDialogViewController, pTableView, pIndexPath);

			// Is there an event mapped to our OnSelected event handler?
			var selected = OnSelected;

			// If yes, fire it.
			if (selected != null) {
				selected (this, EventArgs.Empty);
			}

			// Close the dialog.
			pDialogViewController.DeactivateController(true);
		}

		static public event EventHandler<EventArgs> OnSelected;
	}
}

