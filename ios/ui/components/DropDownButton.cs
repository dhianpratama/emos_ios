﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using ios;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public class DropDownButton : UIView
	{
		public event EventHandler<BaseEventArgs<KeyValuePair<string, string>>> OnDone;
		public event EventHandler OnListNotInititated;
		public const float ArrowButtonWidth = 40f;
		public float Height { get; set; }
		public UIButton TextButton { get; set; }
		public UIButton ArrowButton { get; set; }
		public string Text {
			get { return TextButton.Title (UIControlState.Normal); }
			set { TextButton.SetTitle (value, UIControlState.Normal); }
		}
		public DropDownHandler _dropDownHandler;
		public string SelectedKey {
			get { return _dropDownHandler.SelectedKey; }
			set { _dropDownHandler.SelectedKey = value; }
		}
		public KeyValuePair<string,string> Selected {
			get { return _dropDownHandler.Selected; }
			set { _dropDownHandler.Selected = value; }
		}
		private bool _roundedLeft;
		private bool _roundedRight;

		public DropDownButton (string text, bool roundedLeft, bool roundedRight, float? height = null, bool hideArrow = false)
		{
			_roundedLeft = roundedLeft;
			_roundedRight = roundedRight;
			var image = UIImage.FromFile ("Images/drop_down_arrow.png");
			SetBackground (height);
			TextButton = CreateTextButton (text);
			AddSubview (TextButton);
			ArrowButton = CreateArrowButton (image, hideArrow);
			AddSubview (ArrowButton);
		}
		public void Load(IBaseContext baseContext, UIView parentView, string dropDownTitle, List<KeyValuePair<string, string>> items, float rowHeight = 44f)
		{
			_dropDownHandler = new DropDownHandler (baseContext, this, "", dropDownTitle, items, rowHeight);
			_dropDownHandler.OnDone += HandleOnDone;
		}
		private void HandleOnDone (object sender, BaseEventArgs<KeyValuePair<string, string>> e)
		{
			SetTitle (e.Value.Value);
			OnDone.SafeInvoke (sender, e);
		}
		public void SetBackground(float? height)
		{
			var backgroundColor = UIColor.LightGray;
			AutosizesSubviews = false;
			Height = height ?? ArrowButtonWidth;
			BackgroundColor = backgroundColor;
		}
		private UIButton CreateTextButton (string text)
		{
			var result = new UIButton ();
			result.BackgroundColor = UIColor.White;
			result.SetTitle (text, UIControlState.Normal);
			result.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			result.TouchUpInside += HandleTouchUpInside;
			result.SetTitleColor (UIColor.Black, UIControlState.Normal);
			result.TitleEdgeInsets = new UIEdgeInsets (0, 20, 0, 0);
			return result;
		}
		private UIButton CreateArrowButton (UIImage image, bool hideArrow)
		{
			var result = new UIButton ();
			if (!hideArrow)
				result.SetImage (image, UIControlState.Normal);
			result.TouchUpInside += HandleTouchUpInside;
			result.BackgroundColor = UIColor.White;
			return result;
		}
		public void SetFrame(float? x = null, float? y = null, float? width = null)
		{
			this.SetFrame (x, y, width, Height);
			AdjustLayout (width);
		}
		public void SetFrameBelowTo(UIView view, float? x = null, float? width = null, float distanceToAbove = 20)
		{
			this.SetFrameBelowTo (view, x, width, Height, distanceToAbove);
			AdjustLayout (width);
		}
		public void AddToParentView (UIView parent)
		{
			parent.AddSubview (this);
			parent.BringSubviewToFront (this);
		}
		public void SetTitle (string value)
		{
			TextButton.SetTitle (value, UIControlState.Normal);
		}
		public void SetTitleByKey (string value)
		{
			_dropDownHandler.SelectedKey = value;
		}
		private void AdjustLayout (float? parentWidth)
		{
			var margin = 1f;
			var contentHeight = Height - (2 * margin);
			TextButton.SetFrame (margin, margin, parentWidth - margin - ArrowButtonWidth, contentHeight);
			ArrowButton.SetFrame (TextButton.Frame.Left + TextButton.Frame.Width - margin, TextButton.Frame.Top, ArrowButtonWidth, contentHeight);
			UpdateViewMask ();
			UpdateContentMask ();
		}
		private void UpdateViewMask ()
		{
			if (_roundedLeft && _roundedRight)
				this.UpdateMask (UIRectCorner.AllCorners);
			else
				if (_roundedRight)
					this.UpdateMask (UIRectCorner.BottomRight | UIRectCorner.TopRight);
		}
		private void UpdateContentMask ()
		{
			if (_roundedLeft)
				TextButton.UpdateMask (UIRectCorner.BottomLeft | UIRectCorner.TopLeft);
			if (_roundedRight)
				ArrowButton.UpdateMask (UIRectCorner.BottomRight | UIRectCorner.TopRight);
		}
		private void HandleTouchUpInside (object sender, EventArgs e)
		{
			if (_dropDownHandler._items == null || !_dropDownHandler._items.Any ())
				OnListNotInititated.SafeInvoke (this);
			else
				ShowDropDown ();
		}
		public void ShowDropDown ()
		{
			_dropDownHandler.ShowDropDown ("");
		}
	}
}
