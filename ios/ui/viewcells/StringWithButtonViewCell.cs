﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using System.Collections.Generic;

namespace ios
{
	public partial class StringWithButtonViewCell : BaseViewCell<KeyValuePair<int, string>>
	{
		public const string CellIdentifier = "StringWithButtonViewCell";
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		public IStringWithButtonCallback _callback;
		private KeyValuePair<int, string> _item;
		private BaseViewCellHandler<KeyValuePair<int, string>> _handler;

		public StringWithButtonViewCell (IntPtr handle) : base (handle)
		{
		}
		public static StringWithButtonViewCell Create ()
		{
			return (StringWithButtonViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetCellContent (KeyValuePair<int, string> item)
		{
			_item = item;
			DescriptionLabel.Text = _item.Value;
			Frame = new RectangleF (0, 0, 400, _handler.BaseViewCellSetting.CellHeight);
			DescriptionLabel.SetFrame (height: _handler.BaseViewCellSetting.CellHeight);
			ActionButton.Hidden = _callback.EnableAction ();
		}
		protected override void SetViewCellHandler(IViewCellHandler<KeyValuePair<int,string>> handler)
		{
			_handler = (BaseViewCellHandler<KeyValuePair<int,string>>)handler;
			_callback = (IStringWithButtonCallback) _handler.Callback;
		}
		partial void ActionButton_TouchDown (MonoTouch.Foundation.NSObject sender)
		{
			_callback.ActionButtonClicked(_item);
		}
	}
}

