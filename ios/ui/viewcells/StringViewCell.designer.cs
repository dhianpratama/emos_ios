// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("StringViewCell")]
	partial class StringViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel StringViewLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (StringViewLabel != null) {
				StringViewLabel.Dispose ();
				StringViewLabel = null;
			}
		}
	}
}
