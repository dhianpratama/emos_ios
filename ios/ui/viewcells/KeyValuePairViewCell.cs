﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

namespace emos_ios
{
	public partial class KeyValuePairViewCell : BaseViewCell<KeyValuePair<string, string>>
	{
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		public const string CellIdentifier = "KeyValuePairViewCell";

		public KeyValuePairViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (KeyValuePair<string, string> item)
		{
			KeyValuePairLabel.Text = item.Value;
		}
		public static KeyValuePairViewCell Create ()
		{
			return (KeyValuePairViewCell)Nib.Instantiate (null, null) [0];
		}
	}
}

