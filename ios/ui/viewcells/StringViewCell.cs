﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public partial class StringViewCell : BaseViewCell<string>
	{
		public static readonly UINib Nib = UINib.FromName (CellIdentifier, NSBundle.MainBundle);
		public static readonly NSString Key = new NSString (CellIdentifier);
		public const string CellIdentifier = "StringViewCell";
		public StringViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent (string item)
		{
			StringViewLabel.Text = item;
		}
		public static StringViewCell Create ()
		{
			return (StringViewCell)Nib.Instantiate (null, null) [0];
		}
	}
}

