﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace emos_ios
{
	public abstract class BaseViewCell<T> : UITableViewCell
	{
		public BaseViewCell (IntPtr handle) : base (handle)
		{
		}
		public virtual void Initialize(IViewCellHandler<T> handler, T item)
		{
			SetViewCellHandler (handler);
			SetCellContent (item);
			SetLayout ();
		}
		public virtual void HandleRowSelected(T item)
		{
		}
		public virtual void HandleRowDeselected(T item)
		{
		}
		protected virtual void SetViewCellHandler(IViewCellHandler<T> handler)
		{
		}
		protected virtual void SetCellContent(T item)
		{
		}
		protected virtual void SetLayout()
		{
		}
	}
}

