﻿using System;
using MonoTouch.UIKit;

namespace emos_ios
{
	public static class SharedColors
	{
		public static UIColor DarkViewCellBackground = UIColor.LightGray;
		public static UIColor ViewCellBackground = UIColor.FromRGB(230, 230, 230);
		public static UIColor ButtonDVCColor = UIColor.FromRGB(214, 30, 52);
	}
}

