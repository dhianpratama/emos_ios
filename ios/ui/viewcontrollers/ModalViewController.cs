﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ios
{
	public partial class ModalViewController : UIViewController
	{
		public ModalViewController () : base ("ModalViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		private UIView _backgroundView;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			SetView ();
			_backgroundView = CreateBackground ();
			View.AddSubview (_backgroundView);
		}
		private UIView CreateBackground ()
		{
			var result = new UIView (new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
			result.Alpha = 0.5f;
			result.BackgroundColor = UIColor.DarkTextColor;
			result.AutosizesSubviews = false;
			return result;
		}
		private void SetView()
		{
			View.Frame = new RectangleF (0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height);
			View.AutosizesSubviews = false;
			View.Opaque = false;
			View.BackgroundColor = UIColor.Clear;
		}
	}
}

