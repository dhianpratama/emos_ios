﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using System.Collections.Generic;

namespace emos_ios
{
	public class TableAndHeaderView<T, TCell> : UIView, ICallback<T> where TCell : BaseViewCell<T>
	{
		public event EventHandler OnDone;
		public event EventHandler OnCancelled;
		public GenericTableView<T, TCell> TableView { get; set; }
		public string Title { get; set; }
		private UINavigationBar _header;
		public T Selected { get; set; }
		public int SelectedIndex { get; private set; }

		public TableAndHeaderView (IViewCellHandler<T> cellHandler, string title, bool showDone = true, bool showCancel = true)
		{
			Title = title;
			cellHandler.Callback = this;
			TableView = new GenericTableView<T, TCell> (cellHandler);
			AutosizesSubviews = false;
			_header = new UINavigationBar ();
			_header.PushNavigationItem (CreateNavigationItem (showDone, showCancel), true);
			AddSubview (_header);
			AddSubview (TableView);
			TableView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
			TableView.Layer.BorderWidth = 1.0f;
			TableView.RowHeight = 40;
		}
		public void SetLayout(RectangleF frame)
		{
			_header.Frame = new RectangleF (0, 0, frame.Width, 50);
			TableView.Frame = new RectangleF (0, _header.Frame.Height, frame.Width, frame.Height - _header.Frame.Height);
			Frame = frame;
		}
		private UINavigationItem CreateNavigationItem (bool showDone = true, bool showCancel = true)
		{
			var navigationItem = new UINavigationItem ();
			AddButtons (navigationItem, showDone, showCancel);
			navigationItem.Title = Title;
			return navigationItem;
		}
		private void AddButtons (UINavigationItem navigationItem, bool showDone = true, bool showCancel = true)
		{
			if (showDone) {
				string doneText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Done");
				var doneButton = new UIBarButtonItem (doneText, UIBarButtonItemStyle.Plain, (sender, args) => {
					OnDone.SafeInvoke (this);
				});
				navigationItem.SetRightBarButtonItem (doneButton, true);
			}
			if (showCancel) {
				var cancelText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Cancel");
				var cancelButton = new UIBarButtonItem (cancelText, UIBarButtonItemStyle.Plain, (sender, args) => {
					OnCancelled.SafeInvoke (this);
				});
				navigationItem.SetLeftBarButtonItem (cancelButton, true);
			}
		}
		public void Reload ()
		{
			TableView.ReloadData ();
		}
		#region ICallback implementation
		public void ItemSelected (T selected, int selectedIndex)
		{
			Selected = selected;
			SelectedIndex = selectedIndex;
		}
		#endregion
	}
}

