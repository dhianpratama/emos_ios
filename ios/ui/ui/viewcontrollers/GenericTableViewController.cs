﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using SidebarNavigation;

namespace emos_ios
{
	public class GenericTableViewController<T, TCell> : UITableViewController where TCell : BaseViewCell<T>
	{
		public GenericTableView<T, TCell> GenericTableView;
		public event EventHandler DoneClicked;

		public GenericTableViewController (IViewCellHandler<T> cellHandler)
		{
			GenericTableView = new GenericTableView<T, TCell> (cellHandler);
			var done = new UIBarButtonItem (UIBarButtonSystemItem.Done);
			done.Clicked += (object sender, EventArgs e) =>  {
				EventHandler handler = DoneClicked;
				if (handler != null)
					handler (this, new EventArgs ());
			};
			NavigationItem.SetRightBarButtonItem (done, true);
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			TableView = GenericTableView;
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}
	}
}