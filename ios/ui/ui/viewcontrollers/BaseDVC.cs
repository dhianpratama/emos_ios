﻿using System;
using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.ObjCRuntime;

using emos_ios.tools;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace emos_ios
{
	public class BaseDVC : DialogViewController
	{
		private LoadingOverlay _loadingOverlay { get; set; }

		public BaseDVC () : base (UITableViewStyle.Grouped, null, true)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public virtual void CreateRoot()
		{
		}

		public virtual void GetData ()
		{
		}

		public void SendRequest(Action request, string loadingText = "Loading")
		{
			if (loadingText == "Loading")
				loadingText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Loading");

			_loadingOverlay = new LoadingOverlay (UIScreen.MainScreen.Bounds, loadingText);
			View.AddSubview (_loadingOverlay);
			if (String.IsNullOrEmpty (AppDelegate.Instance.ServerConfig.BaseUrl)) {
				OnRequestStopped ();
				this.ShowAlert (AppDelegate.Instance.LanguageHandler.GetLocalizedString("Pleasecontactadministratortosetupthisdevice"));
				return;
			}
			if (InternetConnected ()) {
				AppDelegate.Instance.SetExpiredSession ();
				request ();
			}
		}
		protected virtual void OnRequestStopped()
		{
			if (_loadingOverlay != null) {
				_loadingOverlay.Hide ();
			}
		}
		protected bool InternetConnected()
		{
			if (Runtime.Arch != Arch.DEVICE)
				return true;
			if (Reachability.InternetConnectionStatus () == NetworkStatus.NotReachable) {
				var alertMsg = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Youareoffline");
				this.ShowAlert (alertMsg);
				return false;
			}
			return true;
		}
		protected virtual void OnRequestCompleted(bool success, string failedTitle = "", bool showFailedTitle = true)
		{
			OnRequestStopped ();
			if (!success && showFailedTitle)
				this.ShowAlert (failedTitle, AppDelegate.Instance.LanguageHandler.GetLocalizedString ("RequestFailed"));
		}
	}
}

