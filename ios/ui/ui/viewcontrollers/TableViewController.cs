﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace emos_ios
{
	public partial class TableViewController : UIViewController, ICallback<KeyValuePair<string, string>>
	{
		private List<KeyValuePair<string, string>> _items;
		private KeyValuePairViewCellHandler _cellHandler;
		private DynamicTableSource<KeyValuePair<string, string>, KeyValuePairViewCell> _source;
//		private KeyValuePair<string, string> Selected;
		public event EventHandler DoneClicked;

		public TableViewController ()
		{
			_cellHandler = new KeyValuePairViewCellHandler (this, null);
			_source = new DynamicTableSource<KeyValuePair<string, string>, KeyValuePairViewCell> (_cellHandler);
			_items = new List<KeyValuePair<string, string>> ();
			_source.Items = new List<KeyValuePair<string, string>> ();
			var done = new UIBarButtonItem (UIBarButtonSystemItem.Done);
			done.Clicked += (object sender, EventArgs e) =>  {
				EventHandler handler = DoneClicked;
				if (handler != null)
					handler (this, new EventArgs ());
			};
			NavigationItem.SetRightBarButtonItem (done, true);
		}
		public string NavItemTitle { get { return NavigationItem.Title; } set { NavigationItem.Title = value; }}
		private void SetItems(IEnumerable<KeyValuePair<string, string>> items)
		{
			_items = items.ToList ();
			_source.Items = _items;
		}
		public void ItemSelected(KeyValuePair<string, string> selected, int selectedIndex)
		{
//			Selected = selected;
		}
		public void SetItems(IEnumerable<string> items)
		{
			var keyValues = new List<KeyValuePair<string, string>> ();
			foreach (var item in items) {
				var keyValuePair = new KeyValuePair<string,string> (item, item);
				keyValues.Add (keyValuePair);
			}
			SetItems (keyValues);
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			MainTable.Source = _source;
		}
	}
}

