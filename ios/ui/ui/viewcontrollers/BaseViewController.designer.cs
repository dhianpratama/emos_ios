// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace emos_ios
{
	[Register ("BaseViewController")]
	partial class BaseViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView LogoImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView LogoView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UINavigationItem NavItem { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NavItem != null) {
				NavItem.Dispose ();
				NavItem = null;
			}

			if (LogoImage != null) {
				LogoImage.Dispose ();
				LogoImage = null;
			}

			if (LogoView != null) {
				LogoView.Dispose ();
				LogoView = null;
			}
		}
	}
}
