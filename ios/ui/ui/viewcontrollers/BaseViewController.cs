﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.ObjCRuntime;
using emos_ios.tools;
using emos_ios;

namespace emos_ios
{
	public partial class BaseViewController : UIViewController, ICallback<KeyValuePair<string, string>>
	{
		public const float BottomBarHeight = 124;
		public GenericTableViewController<KeyValuePair<string, string>, KeyValuePairViewCell> TableViewController;
		public KeyValuePair<string, string> SelectedKeyValue;
		private KeyValuePairViewCellHandler _stringViewCellHandler;
		private LoadingOverlay _loadingOverlay { get; set; }
		public BaseViewController (string nibName, NSBundle bundle) : base (nibName, bundle)
		{
			var baseViewCellSetting = new BaseViewCellSetting {
				CellHeight = 50
			};
			_stringViewCellHandler = new KeyValuePairViewCellHandler(this, baseViewCellSetting);
		}			
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			AppDelegate.Instance.OnConnectionProblem += ConnectionProblemHandler;
			ShowBottomLogo ();
			SetTextsByLanguage ();
		}
		protected void ShowTableViewController(IEnumerable<string> items, string title, bool showModal=false)
		{
			var keyValueItems = ToKeyValuePair (items);
			ShowTableViewController (keyValueItems, title, showModal);
		}
		protected void ShowTableViewController(IEnumerable<KeyValuePair<string,string>> items, string title, bool showModal=false)
		{
			InitTableViewController (items, title);
			if (showModal)
				NavigationController.PresentViewController (TableViewController, true, null);
			else
				NavigationController.PushViewController (TableViewController, true);
		}
		protected void InitTableViewController(IEnumerable<string> items, string title)
		{
			var keyValueItems = ToKeyValuePair (items);
			InitTableViewController (keyValueItems, title);
		}
		protected void InitTableViewController (IEnumerable<KeyValuePair<string, string>> items, string title)
		{
			SelectedKeyValue = new KeyValuePair<string, string> ();
			TableViewController = new GenericTableViewController<KeyValuePair<string, string>, KeyValuePairViewCell> (_stringViewCellHandler);
			TableViewController.GenericTableView.SetItems (items);
			TableViewController.GenericTableView.RowHeight = 44;
			TableViewController.GenericTableView.Layer.BorderColor = Colors.ViewCellBackground.CGColor;
			TableViewController.GenericTableView.Layer.BorderWidth = 1.0f;
			TableViewController.Title = title;
		}
		private IEnumerable<KeyValuePair<string, string>> ToKeyValuePair(IEnumerable<string> items)
		{
			var keyValues = new List<KeyValuePair<string, string>> ();
			foreach (var item in items) {
				var keyValuePair = new KeyValuePair<string,string> (item, item);
				keyValues.Add (keyValuePair);
			}
			return (keyValues);
		}
		protected virtual void TableViewController_DoneClicked ()
		{
			AppDelegate.Instance.Nav.PopViewControllerAnimated (true);
		}
		protected bool InternetConnected()
		{
			if (Runtime.Arch != Arch.DEVICE)
				return true;
			if (Reachability.InternetConnectionStatus () == NetworkStatus.NotReachable) {
				var alertMsg = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Youareoffline");
				this.ShowAlert (alertMsg);
				return false;
			}
			return true;
		}
		protected void SendRequest(Action request, string loadingText = "Loading")
		{
		if (loadingText == "Loading")
				loadingText = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Loading");

			AppDelegate.Instance.SetExpiredSession ();
			_loadingOverlay = new LoadingOverlay (UIScreen.MainScreen.Bounds, loadingText);
			View.AddSubview (_loadingOverlay);
			if (String.IsNullOrEmpty (AppDelegate.Instance.ServerConfig.BaseUrl)) {
				var alertMsg = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Pleasecontactadministratortosetupthisdevice");
				OnRequestStopped ();
				this.ShowAlert (alertMsg);
				return;
			}
			if (InternetConnected ()) {
				request ();
			}
		}
		protected void SendBackgroundRequest(Action request)
		{
			if (String.IsNullOrEmpty (AppDelegate.Instance.ServerConfig.BaseUrl)) {
				var alertMsg = AppDelegate.Instance.LanguageHandler.GetLocalizedString ("Pleasecontactadministratortosetupthisdevice");
				OnRequestStopped ();
				this.ShowAlert (alertMsg);
				return;
			}
			if (InternetConnected ()) {
				AppDelegate.Instance.SetExpiredSession ();
				request ();
			}
		}
		protected virtual void OnRequestCompleted(bool success, string failedTitle = "", bool showFailedTitle = true)
		{
			OnRequestStopped ();
			if (!success && showFailedTitle)
				this.ShowAlert (failedTitle, AppDelegate.Instance.LanguageHandler.GetLocalizedString ("RequestFailed"));
		}
		protected virtual void OnRequestStopped()
		{
			if (_loadingOverlay != null) {
				_loadingOverlay.Hide ();
			}
		}
		private void ConnectionProblemHandler(object sender, EventArgs e)
		{
			OnRequestStopped ();
		}
		#region ICallback implementation
		public void ItemSelected (KeyValuePair<string, string> selected, int selectedIndex)
		{
			SelectedKeyValue = selected;
		}
		#endregion
		public virtual void ShowBottomLogo()
		{
			var logoView = new LogoView {
				Frame = new RectangleF (0, 920, 768, BottomBarHeight) 
			};
			View.AddSubview (logoView);
		}
		public virtual void SetTextsByLanguage()
		{
		}
	}
}
