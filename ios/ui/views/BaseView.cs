﻿using System;
using MonoTouch.UIKit;

namespace ios
{
	public class BaseView : UIView
	{
		protected IBaseContext AppContext { get; private set; }

		public BaseView (IntPtr h) : base (h)
		{
		}
		public BaseView (IBaseContext appContext)
		{
			AppContext = appContext;
		}
		public IBaseContext GetBaseContext()
		{
			return AppContext;
		}
	}
}

