﻿using System;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public class GenericTableView<T, TCell> : UITableView where TCell : BaseViewCell<T>
	{
		private List<T> _items;
		private IViewCellHandler<T> _cellHandler;
		private DynamicTableSource<T, TCell> _source;
		public bool CheckmarkVisible { get { return _source.CheckmarkVisible; } set { _source.CheckmarkVisible = value; } }

		public GenericTableView (IViewCellHandler<T> cellHandler)
		{
			_cellHandler = cellHandler;
			_source = new DynamicTableSource<T, TCell> (_cellHandler);
			_items = new List<T> ();
			_source.Items = new List<T> ();
			Source = _source;
		}
		public void SetItems(IEnumerable<T> items)
		{
			_items = items.ToList ();
			_source.Items = _items;
		}
		public void SelectRow(int index)
		{
			_source.SelectRow (index, this);
		}
	}
}

