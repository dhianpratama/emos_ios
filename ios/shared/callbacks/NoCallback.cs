﻿using System;
using emos_ios;

namespace ios
{
	public class NoCallback<T> : ICallback <T>
	{
		private IBaseContext _baseContext;
		public NoCallback (IBaseContext baseContext)
		{
			_baseContext = baseContext;
		}

		#region ICallback implementation

		public IBaseContext GetBaseContext ()
		{
			return _baseContext;
		}

		public void ItemSelected (T selected, int selectedIndex)
		{
//			throw new NotImplementedException ();
		}

		#endregion
	}
}

