﻿using System;
using emos_ios;
using System.Collections.Generic;

namespace ios
{
	public interface IStringWithButtonCallback : ICallback<KeyValuePair<int, string>>
	{
		void ActionButtonClicked(KeyValuePair<int, string> selected);
		bool EnableAction ();
	}
}

