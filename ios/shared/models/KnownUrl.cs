﻿using System;
using System.Net.Http;

namespace ios
{
	public class KnownUrl
	{
		public string Url { get; private set; }
		public HttpMethod Method { get; private set; }

		public KnownUrl(string url, HttpMethod method)
		{
			Url = url;
			Method = method;
		}
	}
}

