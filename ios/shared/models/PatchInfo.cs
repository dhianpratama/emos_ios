﻿using System;

namespace ios
{
	public class PatchInfo
	{
		public string Filename { get; set; }
		public string Url { get; set; }
	}
}

