﻿using System;

namespace emos_ios
{
	public class OperationInfo
	{
		public bool exceptionOccurred { get; set; }
		public bool executed { get; set; }
		public bool success { get; set; }
	}
}

