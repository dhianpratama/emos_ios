﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;

namespace emos_ios
{
	public class BaseUser
	{
		public string Id { get; set; }
		public bool IsAdmin { get; set; }
		public bool IsDemoUser { get; set; }
		public CookieContainer AspxAuth { get; set; }
		public List<string> UserAccessControl { get; set; }

		public BaseUser ()
		{
			Id = null;
			IsAdmin = false;
			IsDemoUser = false;
			AspxAuth = new CookieContainer ();
			UserAccessControl = new List<string> ();
		}

		public bool Authorized(string aclIdentifier)
		{
			return UserAccessControl.Any (e => e == aclIdentifier);
		}
	}
}