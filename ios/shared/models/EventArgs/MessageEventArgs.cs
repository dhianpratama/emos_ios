﻿using System;

namespace ios
{
	public class MessageEventArgs : EventArgs
	{
		public string MessageCode { get; set; }
		public string TitleCode { get; set; }
		public string Ok { get; set; }

		public MessageEventArgs (string localizationId, string titleCode = "")
		{
			MessageCode = localizationId;
			TitleCode = titleCode;
			Ok = "OK";
		}
	}
}

