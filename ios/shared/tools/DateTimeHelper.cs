﻿using System;
using MonoTouch.Foundation;

namespace emos_ios
{
	public static class DateTimeHelper
	{
		public static DateTime FromNSDate(NSDate date)
		{
			var reference = new DateTime(2001, 1, 1, 0, 0, 0);
			var currentDate = reference.AddSeconds(date.SecondsSinceReferenceDate);
			return currentDate.ToLocalTime ();
		}
	}
}

