﻿using System;
using MonoTouch.Dialog.Utilities;
using MonoTouch.UIKit;
using ios;
using emos_ios;
using System.Net;
using System.IO;

namespace core_emos
{
	public class ImageDownloader
	{
		public const string ImageNotAvailable = "Images/image-not-available.png";
		public event EventHandler<StringEventArgs> OnImageNotFound;
		public event EventHandler<StringEventArgs> OnImageFound;
		public string ImagePath { get; set; }
		public IBaseContext BaseContext { get; set; }
		public string ImageUrl { get; set; }

		public async void Download(IBaseContext baseContext, IImageUpdated caller, string path, int width = 154, int height = 154)
		{
			BaseContext = baseContext;
			if (String.IsNullOrEmpty (path)) {
				ImagePath = "";
				ImageUrl = "";
//				OnImageNotFound.SafeInvoke (this, new StringEventArgs (ImagePath));
				return;
			}
			ImagePath = path;
			ImageUrl = ResizerUrl (BaseContext.ServerConfig.BaseUrl, path, width, height);
			var content = ios.KnownUrls.CheckImageQueryString (path);
			await BaseContext.HttpSender.Request<Object> ()
				.From (ios.KnownUrls.CheckImage)
				.WithQueryString (content)
				.WhenSuccess (result => OnRequestImageCompleted (result))
				.WhenFail (result => OnFail (result))
				.Go ();
		}
		private void OnRequestImageCompleted (Object result)
		{
			OnImageFound.SafeInvoke (this, new StringEventArgs (ImagePath));
		}
		public static UIImage LazyLoad(string url, IImageUpdated caller)
		{
			if (String.IsNullOrEmpty (url))
				return ShowNotAvailable ();
			return ImageLoader.DefaultRequestImage (new Uri (url), caller);// ?? ShowNotAvailable ();
		}
		public static UIImage GetNotAvailableImage()
		{
			return ShowNotAvailable ();
		}
		private void OnFail (HttpStatusCode result)
		{
			OnImageNotFound.SafeInvoke (this, new StringEventArgs (ImagePath));
		}
		public static UIImage ShowNotAvailable()
		{
			return UIImage.FromBundle (ImageNotAvailable);
		}
		public static UIImage ShowLoading()
		{
			return UIImage.FromBundle ("Images/loading.png");
		}
		public static string ResizerUrl(string baseUrl, string url, int width = 154, int height = 154)
		{
			if (String.IsNullOrEmpty (url))
				return "";
			return QueryStringBuilder.BuildUrl (baseUrl, KnownUrls.ImageResize, KnownUrls.ImageResizeQueryString (url, width, height));
		}
	}
}

