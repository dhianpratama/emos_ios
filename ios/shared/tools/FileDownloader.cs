﻿using System;
using System.Net;
using emos_ios;
using System.IO;
using System.ComponentModel;

namespace ios
{
	public class FileDownloader
	{
		public event EventHandler OnDownloadCompleted;

		public FileDownloader ()
		{
		}
		public void Download(PatchInfo patchInfo)
		{
			var documentsFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var destinationPath = Path.Combine(documentsFolderPath, patchInfo.Filename);

			var client = new WebClient ();
			client.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0)");

			client.DownloadFileAsync(new Uri(patchInfo.Url), destinationPath);
			client.DownloadFileCompleted += DownloadFileCompleted();
			// wait for the current thread to complete, since the an async action will be on a new thread.
			//while (client.IsBusy) { }

			/*
			client.DownloadFileAsync(new Uri(patchInfo.Url), destinationPath);
			client.DownloadFileCompleted += DownloadFileCompleted();*/
		}
		private void Downloader_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			// print progress of download.
			Console.WriteLine(e.BytesReceived + " " + e.ProgressPercentage);
		}

		private void Downloader_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			// display completion status.
			if (e.Error != null)
				Console.WriteLine (e.Error.Message);
			else {
				Console.WriteLine("Download Completed!!!");
				OnDownloadCompleted.SafeInvoke (this);
			}
		}

		private AsyncCompletedEventHandler DownloadFileCompleted()
		{
			Action<object, AsyncCompletedEventArgs> action = (sender, e) => {
				OnDownloadCompleted.SafeInvoke (this);
			};
			return new AsyncCompletedEventHandler (action);
		}
	}
}

