﻿using System;
using System.Threading.Tasks;
using emos_ios;
using System.Threading;

namespace ios
{
	public class TimerHandler
	{
		public event EventHandler OnTick;
		private CancellationTokenSource _cancellationTokenSource;

		public void Restart (DateTime startTime, int interval)
		{
			Stop ();
			Start (startTime, interval);
		}
		private void Start(DateTime start, int intervalSecond)
		{
			var targetTime = start.AddSeconds (intervalSecond);
			Run (targetTime);
		}
		private async void Run(DateTime targetTime)
		{
			_cancellationTokenSource = new CancellationTokenSource ();
			_cancellationTokenSource.Token.Register(CancelNotification);
			var task = Task.Run (() => CheckTime (targetTime, _cancellationTokenSource.Token), _cancellationTokenSource.Token);
			try {
				await task;
				if (task.IsCompleted)
					OnTick.SafeInvoke (this);
			} catch (TaskCanceledException) {
			}
		}
		private void CancelNotification ()
		{
		}
		private void CheckTime(DateTime targetTime, CancellationToken cancellationToken)
		{
			while (true) {
				Thread.Sleep (2000);
				cancellationToken.ThrowIfCancellationRequested();
				var currentTime = DateTime.UtcNow;
				if (DateTime.Compare (currentTime, targetTime) >= 0)
					return;
			}
		}
		public void Stop ()
		{
			if (_cancellationTokenSource == null)
				return;
			try {
				_cancellationTokenSource.Cancel ();
			} catch (ObjectDisposedException) {
			}
		}
	}
}