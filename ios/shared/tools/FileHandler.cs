﻿using System;
using System.IO;
using MonoTouch.Foundation;

namespace ios
{
	public class FileHandler
	{
		public FileHandler ()
		{
		}
		public static void CopyFileInBundleToDocumentsFolder(String filename)
		{
			//---path to Documents folder---
			var documentsFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

			//---destination path for file in the Documents
			// folder---
			var destinationPath = Path.Combine(documentsFolderPath, filename);

			//---path of source file---
			var sourcePath = Path.Combine(NSBundle.MainBundle.BundlePath, filename);

			//---print for verfications---
			Console.WriteLine(destinationPath);
			Console.WriteLine(sourcePath);

			if (!File.Exists (sourcePath)) {
				Console.WriteLine ("{0} doesn't exist", sourcePath);
				return;
			}

			try {
				File.Copy(sourcePath, destinationPath, true);
			} catch (Exception e) {
				Console.WriteLine (e.Message);
			}
		}
	}
}

