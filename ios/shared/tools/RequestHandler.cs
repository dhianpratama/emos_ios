﻿using System;
using emos_ios;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using emos_ios.tools;

namespace ios
{
	public class RequestHandler
	{
		private IBaseContext _appContext;
		private UIViewController _controller;
		private event EventHandler OnRequestStop;
		public event EventHandler<RequestLoadingEventArgs> OnRequestStart;
		private event EventHandler<MessageEventArgs> OnRequestFailed;
		public RequestHandler (IBaseContext context, UIViewController controller)
		{
			_appContext = context;
			_controller = controller;
			OnRequestStart += _appContext.StartRequest;
			OnRequestStop += _appContext.HideLoadingOverlay;
			OnRequestFailed += _appContext.ShowAlert;
		}
		public void SendRequest (UIView view, Action request, string loadingText = "Loading")
		{
			if (_appContext.ServerConfig.IsDemo)
				return;

			OnRequestStart.SafeInvoke (this, new RequestLoadingEventArgs (view));
			EnableRightButtons (false);
			if (String.IsNullOrEmpty (_appContext.ServerConfig.BaseUrl)) {
				StopRequest ();
				OnRequestFailed.SafeInvoke (this, new MessageEventArgs ("Pleasecontactadministratortosetupthisdevice"));
				return;
			}
			if (InternetConnected ()) {
				request ();
			}
		}
		private void EnableRightButtons (bool enabled)
		{
			if (_controller.NavigationItem == null || _controller.NavigationItem.RightBarButtonItems == null)
				return;
			foreach(var item in _controller.NavigationItem.RightBarButtonItems)
				item.Enabled = enabled;
		}			
		public virtual void StopRequest()
		{
			EnableRightButtons (true);
			OnRequestStop.SafeInvoke (this);
		}
		protected bool InternetConnected()
		{
			if (Runtime.Arch != Arch.DEVICE)
				return true;
			if (Reachability.InternetConnectionStatus () == NetworkStatus.NotReachable) {
				CompletedRequest (false, "Youareoffline");
				return false;
			}
			return true;
		}
		public virtual void CompletedRequest(bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			StopRequest ();
			if (!success && showFailedTitle)
				OnRequestFailed.SafeInvoke (this, new MessageEventArgs ("RequestFailed", failedTitleCode));
		}
	}
}

