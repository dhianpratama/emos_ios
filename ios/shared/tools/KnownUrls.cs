﻿using System;
using System.Net.Http;

namespace ios
{
	public class KnownUrls
	{
		public static readonly KnownUrl ImageResize = new KnownUrl("Media/ResizeImage", HttpMethod.Post);
		public static string ImageResizeQueryString(string path, int targetWidth = 154, int targetHeight = 154)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString["path"] = path;
			queryString["targetWidth"] = targetWidth.ToString();
			queryString["targetHeight"] = targetHeight.ToString();
			return queryString.ToString ();
		}

		public static readonly KnownUrl CheckImage = new KnownUrl("Media/CheckImage", HttpMethod.Post);
		public static string CheckImageQueryString(string path)
		{
			var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
			queryString["path"] = path;
			return queryString.ToString ();
		}
	}
}

