﻿using System;

namespace emos_ios
{
	//Gleneagles
	public static class Settings
	{
		public static bool UseDateAsDashboardTitle = false;
		public static bool HideNurseDashboardTab = true;
		public static bool ShowCompanionLabelOnDashboard = false;
		public static bool ShowBedNameOnDashboard = false;
		public static bool HideMealOnHold = false;
		public static bool HideStayForMeal = false;
		public static bool HideEarlyDelivery = false;
		public static long DefaultNewExtraOrderQuantity = 0;
		public static bool IgnoreIncompleteOrder = true;
		public static bool UseMealPeriodAsBirthdayCakeSegment = false;
		public static bool ChangeCompanionLabelToLodger = true;
		public static bool SimpleTrialOrder = true;
		public static bool DoubleTapNurseDashboardTableView = false;
		public static bool UseDietOrderText = false;
		public static bool UseExtraOrdersText = false;
		public static bool MultipleCompanionOrder = true;
		public static bool MandatoryMainDish = false;
		public static bool HideMealPeriodGroupInSetting = true;
		public static bool EnableDeclineMealOrder = true;
		public static bool HideSaveDraft = true;
		public static bool FoodAllergiesRemarkSpecialQuery = false;
		public static bool ShowOtherDietsCommentField = false;
		public static bool ShowActiveDietInformation = false;
		public static bool ShowPatientPreference = true;
		public static bool CheckLock = true;
		public static bool ShowDVCTitle = false;
		public static bool ShowAdditionalPatientMenuLinks = true;
		public static bool UseRegistrationNumberInsteadOfId = true;
		public static int StartDateToEndDateDefaultDays = 90;
		public static bool DisableExtraOrderEdit = false;
		public static bool OneDishMeal = false;
		public static bool LoadExistingOrderOnEdit = true;
		public static bool AllowDashboardCutoffToggle = true;
		public static bool PatientDailyOrderMode = true;
		public static bool QueryMealTypeWithFeeds = true;
		public static bool FoodTextureIsCompulsory = true;
		public static bool ShowYesNoImageButtonOnMealOrderDetail = false;
		public static bool UseSmallCap = true;
		public static bool ShowOrderRoleSelection = false;
		public static bool ShowMealOrderTitle = true;
		public static bool ShowGotoPatientModeButtonOnMealOrder = false;
		public static bool ShowImageAsHeader = false;
		public static bool DietsEndToday = true;
		public static bool CommonDietsVersion2 = true;
		public static bool HideLoginNavigationBar = false;
		public static bool ShowWardSelectionInLogin = false;
		public static bool ReplaceMealOrderBackButton = false;
		public static bool EnableTheme = true;
		public static bool ShowOnGoingRemark = true;
		public static bool ShowDetailOnImageView = true;
		public static bool UseDateForDiets = true;
		public static bool HospitalBasedWards = true;
		public static bool GradientMenuView = false;
		public static bool ShowInfoMsgOnMealOrdering = false;
		public static bool MealOrderBasedPatientInfo = true;
		public static bool HideNurseDashboardIcons = false;
		public static bool HideAvailability = false;
		public static bool HideDateSettingOnPatientMeal = false;
		public static bool ShowTherapeuticComment = false;
		public static bool CompanionMealChargeableOption = false;
		public static bool CurrentAndNextMealPeriodExtraOrder = false;
		public static bool ExtraOrderQuantityInMealOrderHidden = true;
		public static int ExtraOrderMinimumQuantity = 0;
		public static int ExtraOrderMaximumQuantity = 3;
		public static int MaximumRemarkLengthInMealOrder = 150;
		public static bool CheckLoginAcl = false;
		public static bool HideSkippedStep = true;
		public static bool ShowAvailabilityInMealOrderTabs = false;
		public static bool AllowToAddOnlyOneTherapeutic = false;
		public static bool HideMenuOfTheDay = true;
		public static bool HideManualAdmissionOnSapUp = false;
	}
}