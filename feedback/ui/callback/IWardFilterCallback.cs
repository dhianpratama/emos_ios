﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;

namespace feedback
{
	public interface IWardFilterCallback: ICallback<WardFilteringModel>
	{
		void ExitButtonClicked();
		void RefreshTableView(WardFilteringModel detail);
	}
}

