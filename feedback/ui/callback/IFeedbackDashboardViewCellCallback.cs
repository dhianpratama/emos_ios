﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;

namespace feedback
{
	public interface IFeedbackDashboardViewCellCallback: ICallback<LocationForFeedbackSimple>
	{
		//void Click (LocationForFeedbackSimple item);
	}
	public interface IFeedbackSelectionCallBack: ICallback<List<AnswerModelSimple>>
	{
	}
	public interface IFeedbackTextCallback
	{
		void TextReturned(AnswerModelSimple answer);
	}
	public interface ITextViewTextCallback
	{
		void GetFreeText (String text);
	}
}

