﻿using System;
using System.Collections.Generic;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios;

namespace feedback
{
	public class FeedbackDashboardCellHandler : BaseViewCellHandler<LocationForFeedbackSimple>
	{
		public FeedbackDashboardCellHandler (string cellIdentifier, IFeedbackDashboardViewCellCallback callback, BaseViewCellSetting setting) : base(cellIdentifier, callback, setting)
		{
		}
	}
}

