﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using emos_ios.tools;
using MonoTouch.Dialog.Utilities;
using System.Linq;
using core_emos;
using emos_ios;

namespace feedback
{
	public partial class feedbackDashboardViewCell : BaseViewCell<LocationForFeedbackSimple>
	{
		public static readonly UINib Nib = UINib.FromName ("feedbackDashboardViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("feedbackDashboardViewCell");
		public static NSString CellIdentifier = Key;

		private IFeedbackDashboardViewCellCallback _callback;
		private LocationForFeedbackSimple _location;

		public feedbackDashboardViewCell (IntPtr handle) : base (handle)
		{
		}
		protected override void SetCellContent(LocationForFeedbackSimple item)
		{
			_location = item;
			noLabelCell.Text = item.order.ToString();
			idNumberCell.Text = item.registrations.Count > 0 ? item.registrations.FirstOrDefault ().registration_number?? "" : "";
			nameCellLabel.Text = item.registrations.Count > 0 ? item.registrations.FirstOrDefault ().patient_name : "";
			wardCellLabel.Text = item.wardCode;
			bedCellLabel.Text = item.code;

			if (item.registrations.Count () > 0) {
				if (item.registrations.FirstOrDefault ().manual_registration)
					this.BackgroundColor = UIColor.FromRGBA (147, 118, 32, 30);
				else
					this.BackgroundColor = UIColor.FromRGBA (49, 136, 204, 48);
			} else {
				this.BackgroundColor = UIColor.FromRGBA (255, 255, 255, 70);
			}

			this.SelectedBackgroundView.BackgroundColor = UIColor.FromRGBA (49, 136, 204, 78);
		}
		public static feedbackDashboardViewCell Create ()
		{
			return (feedbackDashboardViewCell)Nib.Instantiate (null, null) [0];
		}
		public override void HandleRowSelected(LocationForFeedbackSimple item)
		{
			if (item.registrations.Count > 0 && _location.registrations.Count > 0) 
			if( item.registrations.FirstOrDefault().registration_id
				!= _location.registrations.FirstOrDefault().registration_id)
				return;
			
			_callback.ItemSelected (item, 0);
		}
	}
}

