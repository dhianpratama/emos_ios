﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;

namespace feedback
{
	public partial class WardViewCell : BaseViewCell<WardFilteringModel>
	{
		public const string CellIdentifier = "WardViewCell";
		public static readonly UINib Nib = UINib.FromName ("WardViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("WardViewCell");
		public WardFilteringModel _item;
		private IWardFilterCallback _callback;

		public WardViewCell (IntPtr handle) : base (handle)
		{
		}

		public static WardViewCell Create ()
		{
			return (WardViewCell)Nib.Instantiate (null, null) [0];
		}
		protected override void SetViewCellHandler(IViewCellHandler<WardFilteringModel> handler)
		{
			_callback = (IWardFilterCallback) handler.Callback;
		}
		protected override void SetCellContent (WardFilteringModel item)
		{
			_item = item;
			this.Layer.BorderColor = UIColor.Black.CGColor;
			this.Layer.BorderWidth = 0.5f;
			this.contentLabel.Text = item.label;
			if (item.active_data)
				this.SetSelected (true, false);
		}
		public override void HandleRowSelected(WardFilteringModel item)
		{
			if (item.id != _item.id)
				return;
			_callback.RefreshTableView (_item);
		}
		public override void HandleRowDeselected(WardFilteringModel item)
		{
			if (item.id != _item.id)
				return;
			_callback.RefreshTableView (_item);
		}
	}
}

