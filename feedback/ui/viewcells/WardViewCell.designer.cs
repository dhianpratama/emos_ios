// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("WardViewCell")]
	partial class WardViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel contentLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contentLabel != null) {
				contentLabel.Dispose ();
				contentLabel = null;
			}
		}
	}
}
