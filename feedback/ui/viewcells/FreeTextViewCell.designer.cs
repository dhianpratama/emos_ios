// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("FreeTextViewCell")]
	partial class FreeTextViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITextView freeTextTextView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (freeTextTextView != null) {
				freeTextTextView.Dispose ();
				freeTextTextView = null;
			}
		}
	}
}
