// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("SelectionFreeTextViewCell")]
	partial class SelectionFreeTextViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UITextView answerShowTextTextView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel infoLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel questionLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (answerShowTextTextView != null) {
				answerShowTextTextView.Dispose ();
				answerShowTextTextView = null;
			}

			if (questionLabel != null) {
				questionLabel.Dispose ();
				questionLabel = null;
			}

			if (infoLabel != null) {
				infoLabel.Dispose ();
				infoLabel = null;
			}
		}
	}
}
