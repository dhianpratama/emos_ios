// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("feedbackDashboardViewCell")]
	partial class feedbackDashboardViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel bedCellLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel idNumberCell { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel nameCellLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel noLabelCell { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel wardCellLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (noLabelCell != null) {
				noLabelCell.Dispose ();
				noLabelCell = null;
			}

			if (idNumberCell != null) {
				idNumberCell.Dispose ();
				idNumberCell = null;
			}

			if (nameCellLabel != null) {
				nameCellLabel.Dispose ();
				nameCellLabel = null;
			}

			if (wardCellLabel != null) {
				wardCellLabel.Dispose ();
				wardCellLabel = null;
			}

			if (bedCellLabel != null) {
				bedCellLabel.Dispose ();
				bedCellLabel = null;
			}
		}
	}
}
