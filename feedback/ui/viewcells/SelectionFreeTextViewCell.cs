﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace feedback
{
	public partial class SelectionFreeTextViewCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("SelectionFreeTextViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("SelectionFreeTextViewCell");
		public String QuestionAnswer { get; set; }
		public String Model { get; set; }
		public ITextViewTextCallback callback;

		public SelectionFreeTextViewCell (IntPtr handle) : base (handle)
		{
		}

		public static SelectionFreeTextViewCell Create ()
		{
			return (SelectionFreeTextViewCell)Nib.Instantiate (null, null) [0];
		}
		public override void LayoutSubviews ()  
		{
			base.LayoutSubviews ();
			this.questionLabel.Text = QuestionAnswer;
			this.infoLabel.Text = String.Format ("If {0}, please tell us why", QuestionAnswer);
			this.answerShowTextTextView.Text = Model;
			this.answerShowTextTextView.Delegate = new FeedbackTextViewTableSource (callback);
		}
	}
}

