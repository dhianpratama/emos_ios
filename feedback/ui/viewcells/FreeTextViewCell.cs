﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace feedback
{
	public partial class FreeTextViewCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("FreeTextViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("FreeTextViewCell");
		public String Model { get; set; }
		public ITextViewTextCallback callback;

		public FreeTextViewCell (IntPtr handle) : base (handle)
		{
			
		}

		public static FreeTextViewCell Create ()
		{
			return (FreeTextViewCell)Nib.Instantiate (null, null) [0];
		}
		public override void LayoutSubviews ()  
		{
			base.LayoutSubviews ();
			this.freeTextTextView.Text = Model;
			this.freeTextTextView.Delegate = new FeedbackTextViewTableSource (callback);
		}
	}
}

