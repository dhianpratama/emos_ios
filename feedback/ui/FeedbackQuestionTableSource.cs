﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using VMS_IRIS.Areas.EmosIpad.Models;

namespace feedback
{
	public class FeedbackQuestionTableSource : UITableViewSource, ITextViewTextCallback{
		QuestionModelSimple _tableItems;
		List<AnswerModelSimple> _answers;
		string feedbackSelectionCellIdentifier = "SelectionViewCell";
		string feedbackSelectionFreeTextCellIdentifier = "SelectionFreeTextViewCell";
		string feedbackFreetextCellIdentifier = "FreeTextViewCell";
		IFeedbackSelectionCallBack _callback;

		public FeedbackQuestionTableSource (QuestionModelSimple items, List<AnswerModelSimple> answers,
			IFeedbackSelectionCallBack callback)
		{
			_tableItems = items;
			_answers = answers;
			_callback = callback;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (_tableItems != null){
				if (_tableItems.questionType.label == "Freetext")
					return 1;
				else
					return _tableItems.answers.Count ();
			}else
				return 0;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			if (_tableItems != null) {
				switch (_tableItems.questionType.label) {
				case "Freetext":
					{
						var freeTextCell = (FreeTextViewCell)tableView.DequeueReusableCell (FreeTextViewCell.Key);
						if (freeTextCell == null) {
							freeTextCell = FreeTextViewCell.Create ();
						}
						freeTextCell.callback = this;

						var text = "";
						if (_tableItems.answers.Count () > indexPath.Row)
							text = _tableItems.answers [indexPath.Row].label;

						freeTextCell.Model = text;
						return freeTextCell;
					}
				case "Single Choice":
					{
						var selected = false;
						var textview = false;
						var freetext = "";
						var answer = "";

						if (_answers.FirstOrDefault (e => e.question_id == _tableItems.question_id) != null) {
							var list = _answers.Where (e => e.question_id == _tableItems.question_id).ToList ();
							var _ans = list.Where (e => e.answer == _tableItems.answers [indexPath.Row].label).ToList();
							if (_ans != null && _ans.Count > 0) {
								selected = true;
								if (_tableItems.answers [indexPath.Row].show_text && _ans.FirstOrDefault ().freetext != null) {
									textview = true;
									freetext = _ans.FirstOrDefault ().freetext;
									answer = _ans.FirstOrDefault ().answer;
								}
							}
						}

						if (textview) {
							var selectionFreeTextCell = (SelectionFreeTextViewCell)tableView.DequeueReusableCell (SelectionFreeTextViewCell.Key);
							if (selectionFreeTextCell == null) {
								selectionFreeTextCell = SelectionFreeTextViewCell.Create ();
							}

							selectionFreeTextCell.callback = this;
							selectionFreeTextCell.Model = freetext;
							selectionFreeTextCell.QuestionAnswer = answer;

							if (selected)
								selectionFreeTextCell.Accessory = UITableViewCellAccessory.Checkmark;
							else
								selectionFreeTextCell.Accessory = UITableViewCellAccessory.None;
							
							return selectionFreeTextCell;

						} else {
							UITableViewCell cell = tableView.DequeueReusableCell (feedbackSelectionCellIdentifier);
							if (cell == null)
								cell = new UITableViewCell (UITableViewCellStyle.Default, feedbackSelectionCellIdentifier);
							cell.TextLabel.Text = _tableItems.answers [indexPath.Row].label;
							cell.SelectionStyle = UITableViewCellSelectionStyle.None;
							if (selected)
								cell.Accessory = UITableViewCellAccessory.Checkmark;
							else
								cell.Accessory = UITableViewCellAccessory.None;
							return cell;
						}
					}
				case "Multi Choice":
					{
						UITableViewCell cell1 = tableView.DequeueReusableCell (feedbackSelectionCellIdentifier);
						if (cell1 == null)
							cell1 = new UITableViewCell (UITableViewCellStyle.Default, feedbackSelectionCellIdentifier);
						cell1.TextLabel.Text = _tableItems.answers [indexPath.Row].label;

						if (_answers.FirstOrDefault (e => e.question_id == _tableItems.question_id) != null) {
							var list = _answers.Where (e => e.question_id == _tableItems.question_id).ToList ();
							if (list.Any (e => e.answer == _tableItems.answers [indexPath.Row].label))
								cell1.Accessory = UITableViewCellAccessory.Checkmark;
							else
								cell1.Accessory = UITableViewCellAccessory.None;
						} else {
							cell1.Accessory = UITableViewCellAccessory.None;
						}

						cell1.SelectionStyle = UITableViewCellSelectionStyle.None;

						return cell1;
					}
				}
			}
			UITableViewCell retrun = new UITableViewCell ();
			return retrun;
		}
		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			if (_tableItems != null) {
				switch (_tableItems.questionType.label) {
				case "Freetext":
					{
						return 150f;
					}
				case "Single Choice":
					{
						var selected = false;
						var textview = false;

						if (_answers.FirstOrDefault (e => e.question_id == _tableItems.question_id) != null) {
							var list = _answers.Where (e => e.question_id == _tableItems.question_id).ToList ();
							var _ans = list.Where (e => e.answer == _tableItems.answers [indexPath.Row].label).ToList ();
							if (_ans != null && _ans.Count > 0) {
								selected = true;
								if (_tableItems.answers [indexPath.Row].show_text && _ans.FirstOrDefault ().freetext != null) {
									textview = true;
								}
							}
						}

						if (selected && textview)
							return 200f;
						else
							return 50f;
					}
				}
			}
			return 50f;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (_tableItems != null) {
				var lists = new List<AnswerModelSimple> ();

				switch (_tableItems.questionType.label) {
				case "Freetext":

					break;

				case "Single Choice":
					var an = _answers.FirstOrDefault (e => e.question_id == _tableItems.question_id);

					if (an != null && indexPath.Row > _tableItems.answers.Count()) {
						an.answer = _tableItems.answers [indexPath.Row].label;
						lists.Add (an);
					} else {
						var _temp = new AnswerModelSimple ();
						_temp.question_id = _tableItems.question_id;
						_temp.question_set_id = (long)_tableItems.question_set_id;
						_temp.question_type_label = _tableItems.questionType.label;
						_temp.answer = _tableItems.answers [indexPath.Row].label;
						_temp.freetext = "";
						lists.Add (_temp);
					}
					_callback.ItemSelected (lists, indexPath.Row);

					break;


				case "Multi Choice":
					var ans = _answers.Where (e => e.question_id == _tableItems.question_id).ToList ();
					bool valid = false;

					if (ans.Count () > 0 && indexPath.Row > _tableItems.answers.Count ()) {
						if (!ans.Any (e => e.answer == _tableItems.answers [indexPath.Row].label)) {
							valid = true;
						}
					} else {
						valid = true;
					}

					if (valid) {
						var _temp = new AnswerModelSimple ();
						_temp.question_id = _tableItems.question_id;
						_temp.question_set_id = (long)_tableItems.question_set_id;
						_temp.question_type_label = _tableItems.questionType.label;
						_temp.answer = _tableItems.answers [indexPath.Row].label;
						_temp.freetext = "";
						lists.Add (_temp);
					}

					_callback.ItemSelected (lists, indexPath.Row);
					break;
				}
			}
		}

		public void GetFreeText(String text)
		{
			var an = _answers.FirstOrDefault (e => e.question_id == _tableItems.question_id);

			var lists = new List<AnswerModelSimple> ();

			if (_tableItems != null) {

				if(_tableItems.questionType.label == "Freetext"){
					if (an != null) {
						an.answer = text;
						an.freetext = "";
					} else {
						var _temp = new AnswerModelSimple ();
						_temp.question_id = _tableItems.question_id;
						_temp.question_set_id = (long)_tableItems.question_set_id;
						_temp.question_type_label = _tableItems.questionType.label;
						_temp.answer =text;
						_temp.freetext = "";
						lists.Add (_temp);
					}
				}else{
					if (an != null) {
						an.freetext = text;
					} else {
						var _temp = new AnswerModelSimple ();
						_temp.question_id = _tableItems.question_id;
						_temp.question_set_id = (long)_tableItems.question_set_id;
						_temp.question_type_label = _tableItems.questionType.label;
						_temp.freetext =text;
						_temp.answer = "";
						lists.Add (_temp);
					}
				}
			}

			_callback.ItemSelected (lists, 0);

		}
	}
}
