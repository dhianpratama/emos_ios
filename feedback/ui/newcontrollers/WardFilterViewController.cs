﻿
using System;
using System.Drawing;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;

namespace feedback
{
	public partial class WardFilterViewController : FeedbackBaseViewController
	{		
		private IWardFilterCallback _callback;
		private List<WardFilteringModel> _model;
		private MultipleSelectionDataSource<WardFilteringModel, WardViewCell> _dataSource;

		public WardFilterViewController (IApplicationContext appContext, IWardFilterCallback callback, List<WardFilteringModel> model) : base (appContext, "WardFilterViewController", null)
		{
			_callback = callback;
			_model = model;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			InitiateExtraOrderDataSource ();
			siTableView.AllowsMultipleSelection = false;
		}
		private void InitiateExtraOrderDataSource ()
		{
			var setting = new BaseViewCellSetting ();
			var handler = new BaseViewCellHandler<WardFilteringModel> (WardViewCell.CellIdentifier, _callback, setting) {
				GetHeightForRow = GetHeightForRow,
				EpicIsUp = false
			};

			_dataSource = new MultipleSelectionDataSource<WardFilteringModel, WardViewCell> (handler) {
				ReloadOnSelectionChanged = true
			};
			_dataSource.OnItemDeselected += OnExtraOrderDeselected;
			_dataSource.OnItemSelected += OnExtraOrderSelected;
			_dataSource.Items = _model ;
				
			siTableView.Source = _dataSource;

			int i = 0;
			var selectedRows = new List<int> ();

			_model.ForEach (e => {
				if(e.active_data)
					selectedRows.Add(i);
				i++;
			});

			_dataSource.SelectRows (selectedRows, siTableView);
		}

		#region MultipleSelectionDataSource Callback
		private void OnExtraOrderSelected (object sender, RowSelectionEventArgs<WardFilteringModel> e)
		{
		}
		private void OnExtraOrderDeselected (object sender, RowSelectionEventArgs<WardFilteringModel> e)
		{
		}
		private float GetHeightForRow (WardFilteringModel item)
		{
			return 40.0f;
		}
		#endregion



		partial void siExitButtonClicked (MonoTouch.Foundation.NSObject sender){
			_callback.ExitButtonClicked();
		}

		partial void submitButtonClicked (MonoTouch.Foundation.NSObject sender){
			//_callback.UpdateTableView(_extraOrder.allSpecialInstructions);
		}
	}
}

