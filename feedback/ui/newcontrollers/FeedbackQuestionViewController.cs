﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Collections.Generic;
using System.Linq;
using RaEngine.Service;
using core_emos;

namespace feedback
{
	public partial class FeedbackQuestionViewController : FeedbackBaseViewController, IFeedbackSelectionCallBack
	{
		private IApplicationContext _appContext;
		private LocationForFeedbackSimple _location;
		private FeedbackModelSimple _feedbackModel;
		private List<AnswerModelSimple> _answers;
		private bool _isFirstPage;
		private int _order;
		private UIBarButtonItem _rightButton;
		private QuestionModelSimple _questionModel;
		private bool _startOver;

		public FeedbackQuestionViewController (IApplicationContext appContext,
			LocationForFeedbackSimple location, FeedbackModelSimple feedbackModel,
			List<AnswerModelSimple> answers, bool isFirstPage=false, int order=0, bool startOver = false)
			: 
		base (appContext, "FeedbackQuestionViewController", null)
		{
			_appContext = appContext;
			_location = location;
			_feedbackModel = feedbackModel;
			_answers = answers;
			_order = order;
			_isFirstPage = isFirstPage;
			_startOver = startOver;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			bgImageView.Image = UIImage.FromBundle ("Images/feedback_bg.png");

			statement1.Hidden = true;
			statement2.Hidden = true;
			statement3.Hidden = true;
			statement4.Hidden = true;

			if (_isFirstPage) {
				introView.Hidden = false;
				questionView.Hidden = true;
				AnswerTableVIew.Hidden = true;
			} else {
				introView.Hidden = true;
				questionView.Hidden = false;
				AnswerTableVIew.Hidden = false;
				questionLabel.Text = "";
				dynamicQuestionLabel.Text = "";
				AnswerTableVIew.Source = new FeedbackQuestionTableSource(_questionModel, _answers, this);
				noOfQuestionsLabel.Text = String.Format ("{0} / {1}", _order.ToString (), _feedbackModel.questionCollection.Count ());
			}

			this.patientNameLabel.Text = String.Format("Hello {0}", _location.registrations.FirstOrDefault().patient_name);
			this.NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);

			SetupQuestionView ();
		}
		private void SetupQuestionView()
		{
			if (_isFirstPage) {
				RequestCheckIfDraftExist ();
				return;
			}

			if (_order == 1)
				RequestFeedbackQuestionModel ();
			else {
				if (_feedbackModel.questionCollection.Count () > _order - 1) {
					_questionModel = _feedbackModel.questionCollection.ElementAt (_order - 1);
					ReloadQuestionAnswerView ();	
				}
			}
		}
		private void RequestCheckIfDraftExist(){
			if (_location.registrations.Count == 0) {
				_requestHandler.StopRequest ();
				return;
			}

			var param = KnownUrls.GetFeedbackDraftByRegistration 
				(_location.registrations.FirstOrDefault().registration_id);

			_requestHandler.SendRequest (View, () => RequestIfDraftExist (param));
		}
		private async void RequestIfDraftExist(string paran){

			var requestFailedText = _appContext.LanguageHandler.GetLocalizedString ("RequestFailed");

			await _appContext.HttpSender.Request<OperationInfo> ()
				.From (KnownUrls.CheckIfDraftExist)
				.WithQueryString (paran)
				.WithContent (_appContext.OperationDate)
				.WhenSuccess (result => OnRequestFeedbackDraftSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false, requestFailedText))
				.Go ();
		}
		private void OnRequestFeedbackDraftSuccessful (OperationInfo result)
		{
			OnRequestCompleted (true);
			if (result.success) { 
				// draft exist
				statement1.Hidden = false;
				statement2.Hidden = false;
				statement3.Hidden = false;
				statement4.Hidden = false;

				this.NavigationItem.SetRightBarButtonItems (GetContinueRightButton ().ToArray (), true);
			} 
		}
		public IEnumerable<UIBarButtonItem> GetContinueRightButton(){
			var rightButtons = new List<UIBarButtonItem> ();
			rightButtons.AddRange (NavigationItem.RightBarButtonItems);

			var _rightButton = new UIBarButtonItem ("Continue", UIBarButtonItemStyle.Done, (sender, args) => {
				Continue();
			});;

			rightButtons.Add (NavigationBarHelper.CreateSeparatorBarButton ());

			rightButtons.Add (_rightButton);

			return rightButtons;
		}
		private void Continue(){
			if(_isFirstPage){
				var _newFeedbackQuestionVC = new FeedbackQuestionViewController (_appContext, _location, _feedbackModel, _answers, false, _order + 1);
				_appContext.Nav.PushViewController (_newFeedbackQuestionVC, true);
			}
		}
		private void RequestFeedbackQuestionModel()
		{
			if (_location.registrations.Count() == 0) {
				_requestHandler.StopRequest ();
				return;
			}

			var param = KnownUrls.GetFeedbackQuestionByRegistration 
				(_location.registrations.FirstOrDefault().registration_id, _startOver);
			
			_requestHandler.SendRequest (View, () => RequestFeedbackQuestion (param));
		}
		private async void RequestFeedbackQuestion(string paran){

			var requestFailedText = _appContext.LanguageHandler.GetLocalizedString ("RequestFailed");

			await _appContext.HttpSender.Request<FeedbackModelSimple> ()
				.From (KnownUrls.GetFeedbackQuestionModel)
				.WithQueryString (paran)
				.WithContent (_appContext.OperationDate)
				.WhenSuccess (result => OnRequestFeedbackQuestionSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false, requestFailedText))
				.Go ();
		}
		private void OnRequestFeedbackQuestionSuccessful (FeedbackModelSimple result)
		{
			OnRequestCompleted (true);

			if (result != null) {
				_feedbackModel = result;

				bool valid = false;
				if (_feedbackModel != null) {
					if (_feedbackModel.questionCollection != null) {
						if (_feedbackModel.questionCollection.Count > 0) {
							_questionModel = _feedbackModel.questionCollection.First ();
							valid = true;
						} else {
							this.ShowAlert (AppContext, "Nofeedbackavailable");
							NavigationController.PopToRootViewController (true);
						}
					}
				}
				if (valid)
					SetupQuestionAnswerTableView ();
			} else {
				this.ShowAlert (AppContext, "Nofeedbackavailable");
			}
		}

		private void SetupQuestionAnswerTableView()
		{
			_feedbackModel.questionCollection.ForEach (question => {
				question.answers.ForEach(ans => {
					if(question.draftAnswers.Any(e => e.label == ans.label))
					{
						var _temp = new AnswerModelSimple();
						_temp.question_id = question.question_id;
						_temp.question_set_id = (long)question.question_set_id;
						_temp.question_type_label = question.questionType.label;
						_temp.answer = question.draftAnswers.FirstOrDefault(d => d.label == ans.label).label;
						_temp.freetext = question.draftAnswers.FirstOrDefault(d => d.label == ans.label).freetext;
						_answers.Add(_temp);
					}	
				});
			});
			noOfQuestionsLabel.Text = String.Format ("{0} / {1}", _order.ToString (), _feedbackModel.questionCollection.Count ());

			/*
			if (_answers != null && _answers.Count () > 0) 
			{
				foreach (var question in _feedbackModel.questionCollection) {
					if (question.draftAnswers.Any ())
						_order += 1;
				}
			}
			*/

			ReloadQuestionAnswerView ();
		}

		private void ReloadQuestionAnswerView()
		{
			questionLabel.Text = String.Format ("Question {0}", _order.ToString());
			dynamicQuestionLabel.Text = String.Format ("{0}", _questionModel.freetext.ToString());

			if (_questionModel.questionType.label == "Multi Choice")
				AnswerTableVIew.AllowsMultipleSelection = true;
			else
				AnswerTableVIew.AllowsMultipleSelection = false;

			AnswerTableVIew.Source = new FeedbackQuestionTableSource (_questionModel, _answers, this);
			AnswerTableVIew.ReloadData ();
		}

		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var title = "";

			if (_isFirstPage)
				title = "Start";
			else if (_order == _feedbackModel.questionCollection.Count)
				title = "Submit";
			else
				title = "Next";
			

			_rightButton = new UIBarButtonItem (title, UIBarButtonItemStyle.Done, (sender, args) => {
				Next();
			});
			var buttons = new List<UIBarButtonItem> {
				_rightButton
			};
			return buttons;
		}

		private void Next(){
			if ( _order != _feedbackModel.questionCollection.Count) {
				SubmitDraft();
			} else if(_isFirstPage){
				var _newFeedbackQuestionVC = new FeedbackQuestionViewController (_appContext, _location, new FeedbackModelSimple(), new List<AnswerModelSimple>(), false, _order + 1, true);
				_appContext.Nav.PushViewController (_newFeedbackQuestionVC, true);
			}else {
				SubmitFeedback ();
			}
		}

		public void ItemSelected(List<AnswerModelSimple> ans, int path)
		{
			if (ans.Count <= 0)
				return;

			if (_questionModel.questionType.label == "Single Choice") {
				_answers.RemoveAll (e => e.question_id == ans.FirstOrDefault ().question_id);
				_answers.Add (ans.FirstOrDefault ());

				AnswerTableVIew.Source = new FeedbackQuestionTableSource (_questionModel, _answers, this);
				AnswerTableVIew.ReloadData ();

				if (!_questionModel.answers.Any (e => e.label == ans.FirstOrDefault ().answer
				   && e.question_id == ans.FirstOrDefault ().question_id
				   && e.show_text))
					Next ();

			} else if (_questionModel.questionType.label == "Multi Choice") {
				var _ans = _answers
					.FirstOrDefault (a => a.question_id == ans.FirstOrDefault ().question_id
				           && a.answer == ans.FirstOrDefault ().answer);
				
				if (_ans != null) {
					_answers.RemoveAll (e => e.question_id == ans.FirstOrDefault ().question_id
					&& e.answer == ans.FirstOrDefault ().answer);
				} else {
					_answers.Add (ans.FirstOrDefault ());
				}

				AnswerTableVIew.Source = new FeedbackQuestionTableSource (_questionModel, _answers, this);
				AnswerTableVIew.ReloadData ();
			} else {
				_answers.RemoveAll (e => e.question_id == ans.FirstOrDefault ().question_id);
				_answers.Add (ans.FirstOrDefault ());
			}
		}

		private void SubmitFeedback(){
			var saveModel = new SaveFeedbackModelSimple () {
				profile_id = _location.registrations.First().profile_id,
				registration_id = _location.registrations.FirstOrDefault().registration_id,
				answers = _answers
			};

			_requestHandler.SendRequest (View, () => SaveAnswers (saveModel));
		}
		private async void SaveAnswers(SaveFeedbackModelSimple model)
		{
			await _appContext.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveFeedbackAnswerV2)
				.WithContent (model)
				.WhenSuccess (result => OnSaveCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
			
		private void OnSaveCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success){	
				this.ShowAlert (AppContext, "Savedsuccessful");
				NavigationController.PopToRootViewController (true);
			}else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
		}
		private void SubmitDraft(){
			var _answer = _answers.Where(e => e.question_id == _questionModel.question_id).ToList();

			if (Valid (_answer)) {
				var saveModel = new SaveFeedbackModelSimple () {
					profile_id = _location.registrations.First ().profile_id,
					registration_id = _location.registrations.FirstOrDefault ().registration_id,
					answers = _answer
				};
				_requestHandler.SendRequest (View, () => SaveDraft (saveModel));
			}
		}
		private bool Valid(List<AnswerModelSimple> answers)
		{
			bool valid = true;

			switch (_questionModel.questionType.label) {
			case "Freetext":
				if (answers.Count <= 0) {
					this.ShowAlert (AppContext, "Please enter your answer");
					valid = false;
				}

				if (answers.Count >0 && String.IsNullOrEmpty (answers.FirstOrDefault ().answer)) {
					this.ShowAlert (AppContext, "Please enter your answer");
					valid = false;
				}
				break;
			case "Single Choice":
				if (answers.Count != 1) {
					valid = false;
					this.ShowAlert (AppContext, "Please choose one of the answers");
				}break;
			case "Multi Choice":
				if (answers.Count <= 0) {
					valid = false;
					this.ShowAlert (AppContext, "Please choose at least one of the answers");
				}break;
			}

			return valid;
		}
		private async void SaveDraft(SaveFeedbackModelSimple model)
		{
			await _appContext.HttpSender.Request<OperationInfoIpad> ()
				.From (KnownUrls.SaveFeedbackDraft)
				.WithContent (model)
				.WhenSuccess (result => OnSaveDraftCompleted(result))
				.WhenFail (result=> OnRequestCompleted(false))
				.Go ();
		}
		private void OnSaveDraftCompleted(OperationInfoIpad result)
		{
			OnRequestCompleted (true);
			if (result.success){	
				ProceedToNextQuestion ();
			}else {
				if (result.validations.Any ()) {
					var validation = (ValidationInfo) result.validations.First ();
					this.ShowAlert (AppContext, validation.message, null, false);
				} else {
					this.ShowAlert (AppContext, "RequestFailed");
				}
			}
		}
		private void ProceedToNextQuestion()
		{
			var _newFeedbackQuestionVC = new FeedbackQuestionViewController (_appContext, _location, _feedbackModel, _answers, false, _order + 1);
			_appContext.Nav.PushViewController (_newFeedbackQuestionVC, true);
		}
	}
}

