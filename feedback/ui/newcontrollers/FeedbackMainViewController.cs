﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using emos_ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using core_emos;
using System.Collections.Generic;
using System.Linq;

namespace feedback
{
	public partial class FeedbackMainViewController : FeedbackBaseViewController, IFeedbackDashboardViewCellCallback, IWardFilterCallback
	{		
		private IApplicationContext _appContext;
		private DynamicTableSource<LocationForFeedbackSimple, feedbackDashboardViewCell> _dataSource;
		private BaseViewCellSetting _baseViewCellSetting;
		public static NSString MyCellId = new NSString("MyCellId");
		private UIBarButtonItem _wardSelectionButton;
		private WardFilterViewController _wardFilterView;

		public FeedbackMainViewController (IApplicationContext appContext) 
			: base (appContext, "FeedbackMainViewController", null)
		{
			_appContext = appContext;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			InitiateNavigationBarButton();

			_baseViewCellSetting = new BaseViewCellSetting { CellHeight = 44 };
			var cellHandler = new FeedbackDashboardCellHandler (feedbackDashboardViewCell.Key, this, _baseViewCellSetting);
			_dataSource = new DynamicTableSource<LocationForFeedbackSimple, feedbackDashboardViewCell> (cellHandler);

			bgImageView.Image = UIImage.FromBundle ("Images/feedback_bg.png");
			_requestHandler.SendRequest (View, RequestFeedbackDashboardModel);
		}

		public void InitiateNavigationBarButton(){
			
			this.NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);

			if (!_appContext.CurrentUser.Authorized ("ACCESS_NURSE_DASHBOARD")) {
				string signOutText = AppContext.LanguageHandler.GetLocalizedString ("Signout");
				var signOutButton = new UIBarButtonItem (signOutText, UIBarButtonItemStyle.Done, (sender, args) => {
					_appContext.SignOut();
				});

				this.NavigationItem.SetLeftBarButtonItem (signOutButton, true);
			}
		}

		public IEnumerable<UIBarButtonItem> RightButtons ()
		{
			var title = "";

			if (_appContext.WardId == null || _appContext.WardId == 0)
				title = "Select Ward";
			else {
				if (_appContext.HospitalId != null && _appContext.HospitalId != 0
				    && _appContext.CurrentUser.AssignedLocation.hospitals.Any ()) {
					var hospital = _appContext.CurrentUser.AssignedLocation.hospitals.FirstOrDefault (e => e.id == _appContext.HospitalId);
					if (hospital != null) {
						title = hospital.wards.FirstOrDefault (e => e.id == _appContext.WardId).label;
					}
				}
			}

			_wardSelectionButton = new UIBarButtonItem (title, UIBarButtonItemStyle.Done, (sender, args) => {
				Refresh();
			});
			var buttons = new List<UIBarButtonItem> {
				_wardSelectionButton
			};
			return buttons;
		}
		public void Refresh(){
			InitiateWardFilerViewController();
			UIApplication.SharedApplication.KeyWindow.Add(_wardFilterView.View);
		}
		private void InitiateWardFilerViewController(){

			var list = new List<WardFilteringModel>();

			if (_appContext.HospitalId != null && _appContext.HospitalId != 0
			    && _appContext.CurrentUser.AssignedLocation.hospitals.Any ()) {

				_appContext.CurrentUser.AssignedLocation.hospitals.ForEach (e => {
					e.wards.ForEach(ward => {
						var _temp = new WardFilteringModel();
						_temp.id = ward.id;
						_temp.label = ward.label;
						_temp.hospital_id = e.id;
						_temp.hospital_label = e.label;
						_temp.active_data = (_appContext.WardId != null && _appContext.WardId == ward.id)? true: false;
						list.Add(_temp);
					});
				});


				/*
				var hospital = _appContext.CurrentUser.AssignedLocation.hospitals.FirstOrDefault (e => e.id == _appContext.HospitalId);
				if (hospital != null) {
					hospital.wards.ForEach (e => {
						var _temp = new WardFilteringModel();
						_temp.id = e.id;
						_temp.label = e.label;
						_temp.active_data = (_appContext.WardId != null && _appContext.WardId == e.id)? true: false;
						list.Add(_temp);
					});
				}
				*/
			}
			_wardFilterView = new WardFilterViewController(_appContext, this, list);

		}
		private void RequestFeedbackDashboardModel()
		{
			if (_appContext.WardId == null || _appContext.WardId == 0) {
				_requestHandler.StopRequest ();
				return;
			}

			var param = KnownUrls.GetFeedbackDashboardQueryString 
				(_appContext.InstitutionId, _appContext.WardId, 
					_appContext.OperationDate, 
					_appContext.HospitalId);
			_requestHandler.SendRequest (View, () => RequestFeedbackDashboard (param));
		}

		private async void RequestFeedbackDashboard(string paran){
			
			var requestdashboardText = _appContext.LanguageHandler.GetLocalizedString ("Requestdashboard");

			await _appContext.HttpSender.Request<FeedbackDashboardModel> ()
				.From (KnownUrls.GetFeedbackDashboardModel)
				.WithQueryString (paran)
				.WithContent (_appContext.OperationDate)
				.WhenSuccess (result => OnRequestFeedbackDashboardSuccessful (result))
				.WhenFail (result => OnRequestCompleted (false, requestdashboardText))
				.Go ();
		}
		private void OnRequestFeedbackDashboardSuccessful (FeedbackDashboardModel result)
		{
			OnRequestCompleted (true);
			_appContext.WardLabel = result.wardLabel;

			_dataSource.Items = result.regs;
			feedbackTableView.RowHeight = 60.0f;
			feedbackTableView.Source = _dataSource;
			feedbackTableView.ReloadData ();
		}
		public void ItemSelected (LocationForFeedbackSimple selected, int selectedIndex)
		{
			var _questionVC = new FeedbackQuestionViewController (_appContext, selected, new FeedbackModelSimple(), new List<AnswerModelSimple>(), true);
			_appContext.Nav.PushViewController (_questionVC, true);
		}
		public void ExitButtonClicked(){
			_wardFilterView.View.RemoveFromSuperview ();
			_wardFilterView.View = null;
			_wardFilterView = null;
		}
		public void RefreshTableView(WardFilteringModel detail){

			if (_appContext.WardId != null) {
				if (detail.id == _appContext.WardId)
					return;
			}
			_appContext.WardId = detail.id;
			_appContext.HospitalId = detail.hospital_id;

			this.NavigationItem.SetRightBarButtonItems (RightButtons ().ToArray (), true);

			ExitButtonClicked ();
			RequestFeedbackDashboardModel ();
		}
		public void ItemSelected(WardFilteringModel selected, int selectedIndex)
		{
		}
	}
}

