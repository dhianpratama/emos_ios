// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("FeedbackQuestionViewController")]
	partial class FeedbackQuestionViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView AnswerTableVIew { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView bgImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel dynamicQuestionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView introView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel noOfQuestionsLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel patientNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel questionLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView questionView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel statement1 { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel statement2 { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel statement3 { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel statement4 { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel welcomeNoteLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AnswerTableVIew != null) {
				AnswerTableVIew.Dispose ();
				AnswerTableVIew = null;
			}

			if (bgImageView != null) {
				bgImageView.Dispose ();
				bgImageView = null;
			}

			if (dynamicQuestionLabel != null) {
				dynamicQuestionLabel.Dispose ();
				dynamicQuestionLabel = null;
			}

			if (introView != null) {
				introView.Dispose ();
				introView = null;
			}

			if (patientNameLabel != null) {
				patientNameLabel.Dispose ();
				patientNameLabel = null;
			}

			if (questionLabel != null) {
				questionLabel.Dispose ();
				questionLabel = null;
			}

			if (questionView != null) {
				questionView.Dispose ();
				questionView = null;
			}

			if (statement1 != null) {
				statement1.Dispose ();
				statement1 = null;
			}

			if (statement2 != null) {
				statement2.Dispose ();
				statement2 = null;
			}

			if (statement3 != null) {
				statement3.Dispose ();
				statement3 = null;
			}

			if (statement4 != null) {
				statement4.Dispose ();
				statement4 = null;
			}

			if (welcomeNoteLabel != null) {
				welcomeNoteLabel.Dispose ();
				welcomeNoteLabel = null;
			}

			if (noOfQuestionsLabel != null) {
				noOfQuestionsLabel.Dispose ();
				noOfQuestionsLabel = null;
			}
		}
	}
}
