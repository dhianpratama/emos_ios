// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace feedback
{
	[Register ("FeedbackMainViewController")]
	partial class FeedbackMainViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel bedLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView bgImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView feedbackTableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView headerContainerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel idNumberLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel nameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel noLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel wardLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (bgImageView != null) {
				bgImageView.Dispose ();
				bgImageView = null;
			}

			if (feedbackTableView != null) {
				feedbackTableView.Dispose ();
				feedbackTableView = null;
			}

			if (headerContainerView != null) {
				headerContainerView.Dispose ();
				headerContainerView = null;
			}

			if (noLabel != null) {
				noLabel.Dispose ();
				noLabel = null;
			}

			if (idNumberLabel != null) {
				idNumberLabel.Dispose ();
				idNumberLabel = null;
			}

			if (nameLabel != null) {
				nameLabel.Dispose ();
				nameLabel = null;
			}

			if (wardLabel != null) {
				wardLabel.Dispose ();
				wardLabel = null;
			}

			if (bedLabel != null) {
				bedLabel.Dispose ();
				bedLabel = null;
			}
		}
	}
}
