﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using MonoTouch.ObjCRuntime;
using emos_ios.tools;
using emos_ios;
using ios;
using MonoTouch.CoreGraphics;

namespace feedback
{
	public partial class FeedbackBaseViewController : UIViewController, ICallback<KeyValuePair<string, string>>
	{
		public IApplicationContext AppContext;
		public static string Nib = "FeedbackBaseViewController";
		public GenericTableViewController<KeyValuePair<string, string>, KeyValuePairViewCell> TableViewController;
		public KeyValuePair<string, string> SelectedKeyValue;
		public CGColor TableBorderColor { get; set; }
		public bool HiddenLogo { get; set; }
		private KeyValuePairViewCellHandler _stringViewCellHandler;
		protected RequestHandler _requestHandler;

		public FeedbackBaseViewController (IApplicationContext appContext, string nibName, NSBundle bundle, IBaseDVC baseDVC = null) : base (nibName, bundle)
		{
			AppContext = appContext;
			_requestHandler = new RequestHandler (appContext, this);
			var baseViewCellSetting = new BaseViewCellSetting {
				CellHeight = 50
			};
			_stringViewCellHandler = new KeyValuePairViewCellHandler(this, baseViewCellSetting);
			ShowDVC (baseDVC);
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			AppContext.OnConnectionProblem += ConnectionProblemHandler;
			ShowBottomLogo ();
			SetTextsByLanguage ();
		}
		private void ShowDVC (IBaseDVC baseDVC)
		{
			if (baseDVC == null)
				return;
			View.AddSubview (baseDVC.GetDvcView ());
			baseDVC.GetDvcView ().Frame = new RectangleF (0, 64, 768, 1024-64);
		}
		protected void ShowTableViewController(IEnumerable<string> items, string title, bool showModal=false)
		{
			var keyValueItems = ToKeyValuePair (items);
			ShowTableViewController (keyValueItems, title, showModal);
		}
		protected void ShowTableViewController(IEnumerable<KeyValuePair<string,string>> items, string title, bool showModal=false)
		{
			InitTableViewController (items, title);
			if (showModal)
				NavigationController.PresentViewController (TableViewController, true, null);
			else
				NavigationController.PushViewController (TableViewController, true);
		}
		protected void InitTableViewController(IEnumerable<string> items, string title)
		{
			var keyValueItems = ToKeyValuePair (items);
			InitTableViewController (keyValueItems, title);
		}
		protected void InitTableViewController (IEnumerable<KeyValuePair<string, string>> items, string title)
		{
			SelectedKeyValue = new KeyValuePair<string, string> ();
			TableViewController = new GenericTableViewController<KeyValuePair<string, string>, KeyValuePairViewCell> (_stringViewCellHandler);
			TableViewController.GenericTableView.SetItems (items);
			TableViewController.GenericTableView.RowHeight = 44;
			TableViewController.GenericTableView.Layer.BorderColor = TableBorderColor ?? SharedColors.ViewCellBackground.CGColor;
			TableViewController.GenericTableView.Layer.BorderWidth = 1.0f;
			TableViewController.Title = title;
		}
		private IEnumerable<KeyValuePair<string, string>> ToKeyValuePair(IEnumerable<string> items)
		{
			var keyValues = new List<KeyValuePair<string, string>> ();
			foreach (var item in items) {
				var keyValuePair = new KeyValuePair<string,string> (item, item);
				keyValues.Add (keyValuePair);
			}
			return (keyValues);
		}
		protected virtual void TableViewController_DoneClicked ()
		{
			AppContext.Nav.PopViewControllerAnimated (true);
		}

		private void ConnectionProblemHandler(object sender, EventArgs e)
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}
		#region ICallback implementation
		public void ItemSelected (KeyValuePair<string, string> selected, int selectedIndex)
		{
			SelectedKeyValue = selected;
		}
		#endregion
		public virtual void ShowBottomLogo(float x=0, float y=-1, float width=768, float height=-1)
		{
			if (HiddenLogo)
				return;
			return;
		}
		public virtual void SetTextsByLanguage ()
		{
		}
		protected virtual void OnRequestCompleted(bool success, string failedTitleCode = "", bool showFailedTitle = true)
		{
			_requestHandler.CompletedRequest (success, failedTitleCode, showFailedTitle);
		}
		public IBaseContext GetBaseContext()
		{
			return AppContext;
		}
	}
}
