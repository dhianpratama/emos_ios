﻿
using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace feedback
{
	public class FeedbackTextViewTableSource : UITextViewDelegate
	{
		ITextViewTextCallback _callback;

		public FeedbackTextViewTableSource(ITextViewTextCallback callback)
		{
			_callback = callback;
		}

		public override void Changed(UITextView textView)
		{
			_callback.GetFreeText (textView.Text);
		}
	}
}
