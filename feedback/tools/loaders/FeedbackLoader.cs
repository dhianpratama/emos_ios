﻿using System;
using ios;
using VMS_IRIS.Areas.EmosIpad.Models;
using System.Linq;
using emos_ios;

namespace feedback
{
	public class FeedbackLoader : IFeedbackLoader
	{
		public void LoadFeedback (IApplicationContext appContext){
			var controller = new FeedbackMainViewController (appContext);

			bool showLeftMenu = false;
			if (appContext.CurrentUser.Authorized ("ACCESS_NURSE_DASHBOARD")) {
				showLeftMenu = true;
			}
			appContext.LoadController (true, showLeftMenu, controller);
		}
	}
}

